using System.Reflection;

[assembly: AssemblyCompany("OutSystems")]
[assembly: AssemblyProduct("OutSystems Platform")]
[assembly: AssemblyCopyright("Copyright © OutSystems 2019")]
[assembly: AssemblyTrademark("")]

[assembly: AssemblyVersion("11.0.614.0")]
[assembly: AssemblyFileVersion("11.0.614.0")]

#if (RUNNING_ON_NET_4_6_1 || NET461)
[assembly: AssemblyInformationalVersion("11.0.614.0+net461")]
#elif (RUNNING_ON_NET_4_7_2 || NET472)
[assembly: AssemblyInformationalVersion("11.0.614.0+net472")]
#elif (NETSTANDARD2_0)
[assembly: AssemblyInformationalVersion("11.0.614.0+netstandard2.0")]
#else
[assembly: AssemblyInformationalVersion("11.0.614.0")]
#endif
