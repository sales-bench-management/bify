﻿<%@ Page Language="c#" Codebehind="ChangeImagePopup.aspx.cs" AutoEventWireup="false" Inherits="ssBIFY.Flows.FlowLoggedIn.ScrnChangeImagePopup" %>
<%@ Register TagPrefix="osweb" Namespace="OutSystems.HubEdition.WebWidgets" Assembly="OutSystems.HubEdition.WebWidgets" %>
<%@ Import namespace="ssBIFY" %>
<%@ Import namespace="OutSystems.HubEdition.RuntimePlatform" %>
<%@ Assembly Name="OutSystems.WidgetsRuntimeAPI" %>
<%@ Register TagPrefix="RichWidgets_widgets" TagName="Kq8LrKI8_nU2pGd0T74Vmcw" Src="Blocks\BIFY\LayoutsOther\Layout_Popup.ascx" %>
<%@ Register TagPrefix="Cropit_widgets" TagName="KB0AuxptP40KkISgE0XFGOg" Src="Blocks\BIFY\Cropit\Cropit.ascx" %>

<%= BIFY_Properties.DocType %>
<html xmlns="http://www.w3.org/1999/xhtml" >
  <head runat="server"><%= GetHeadTopJavaScript() %>
	<title><%= HttpUtility.HtmlEncode (Title) %></title>
    <meta http-equiv="Content-Type" content="<%= "text/html; charset=" + Response.ContentEncoding.WebName %>" />
    <meta http-equiv="Content-Script-Type" content="text/javascript" />
    <meta http-equiv="Content-Style-Type" content="text/css" />
<%= "\n" + GetStyleSheetIncludes() %><%= GetRequestInfoJavaScript() + GetJavaScriptIncludes() + GetHeadBottomJavaScript() %>
  </head>
  <osweb:Body runat="server"><%= GetBodyTopJavaScript() %>
    <osweb:Form id="WebForm1" method="post"  action="<%# GetFormAction() %>" runat="server">
<RichWidgets_widgets:Kq8LrKI8_nU2pGd0T74Vmcw runat="server" id="RichWidgets_wt_WebBlockInstance2" OnEvaluateParameters="RichWidgets_webBlck_WebBlockInstance2_onDataBinding" InstanceID="_WebBlockInstance2"><phMainContent><Cropit_widgets:KB0AuxptP40KkISgE0XFGOg runat="server" id="Cropit_wt_WebBlockInstance3" onAjaxNotify="Cropit_webBlck_WebBlockInstance3_AjaxNotify" OnEvaluateParameters="Cropit_webBlck_WebBlockInstance3_onDataBinding" OnBindDelegates="Cropit_webBlck_WebBlockInstance3_BindDelegates" InstanceID="_WebBlockInstance3"></Cropit_widgets:KB0AuxptP40KkISgE0XFGOg></phMainContent></RichWidgets_widgets:Kq8LrKI8_nU2pGd0T74Vmcw><osweb:DummySubmitLink runat="server" id="Dummy_Submit_Link"/>
    <%= OutSystems.HubEdition.RuntimePlatform.AppInfo.GetAppInfo().GetWatermark() %><%= GetCallbackDebug()	
%><%= GetVisitCode() %></osweb:Form><%= GetEndPageJavaScript() + GetBodyBottomJavaScript() %>
  </osweb:Body>
</html>
