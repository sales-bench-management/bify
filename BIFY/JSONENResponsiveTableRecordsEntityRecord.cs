﻿/* 
 This source code (the "Generated Software") is generated by the OutSystems Platform 
 and is licensed by OutSystems (http://www.outsystems.com) to You solely for testing and evaluation 
 purposes, unless You and OutSystems have executed a specific agreement covering the use terms and 
 conditions of the Generated Software, in which case such agreement shall apply. 
*/

using System;
using System.Linq;
using OutSystems.HubEdition.RuntimePlatform;
using OutSystems.HubEdition.RuntimePlatform.Db;
using OutSystems.Internal.Db;
using OutSystems.RuntimeCommon;
using OutSystems.RuntimeCommon.Caching;

using System.Text;
using System.Data;
using System.Collections.Generic;
using Newtonsoft.Json;
using OutSystems.RESTService;
using OutSystems.RESTService.Behaviors;
using OutSystems.RESTService.Controllers;
using OutSystems.ObjectKeys;
using OutSystems.HubEdition.RuntimePlatform.NewRuntime;


namespace ssBIFY.RestRecords {

	public class JSONENResponsiveTableRecordsEntityRecord: AbstractRESTStructure<ENResponsiveTableRecordsEntityRecord> {
		[JsonProperty("ResponsiveTableRecords")]
		public string AttrResponsiveTableRecords;

		public JSONENResponsiveTableRecordsEntityRecord() {}

		public JSONENResponsiveTableRecordsEntityRecord(ENResponsiveTableRecordsEntityRecord s, IBehaviorsConfiguration config) {
			if (config.DefaultValuesBehavior == DefaultValuesBehavior.DontSend) {
				AttrResponsiveTableRecords = s.ssResponsiveTableRecords;
			} else {
				AttrResponsiveTableRecords = s.ssResponsiveTableRecords;
			}
		}

		public static Func<ssBIFY.RestRecords.JSONENResponsiveTableRecordsEntityRecord, ENResponsiveTableRecordsEntityRecord> ToStructureDelegate(IBehaviorsConfiguration config) {
			return (ssBIFY.RestRecords.JSONENResponsiveTableRecordsEntityRecord s) => ToStructure(s, config);
		}
		public static ENResponsiveTableRecordsEntityRecord ToStructure(ssBIFY.RestRecords.JSONENResponsiveTableRecordsEntityRecord obj, IBehaviorsConfiguration config) {
			ENResponsiveTableRecordsEntityRecord s = new ENResponsiveTableRecordsEntityRecord(null);
			if (obj != null) {
				s.ssResponsiveTableRecords = obj.AttrResponsiveTableRecords == null ? "": obj.AttrResponsiveTableRecords;
			}
			return s;
		}

		public static Func<ENResponsiveTableRecordsEntityRecord, ssBIFY.RestRecords.JSONENResponsiveTableRecordsEntityRecord> FromStructureDelegate(IBehaviorsConfiguration config) {
			return (ENResponsiveTableRecordsEntityRecord s) => FromStructure(s, config);
		}
		public static ssBIFY.RestRecords.JSONENResponsiveTableRecordsEntityRecord FromStructure(ENResponsiveTableRecordsEntityRecord s, IBehaviorsConfiguration config) {
			return new ssBIFY.RestRecords.JSONENResponsiveTableRecordsEntityRecord(s, config);
		}

	}



}