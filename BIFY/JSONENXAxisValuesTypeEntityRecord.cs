﻿/* 
 This source code (the "Generated Software") is generated by the OutSystems Platform 
 and is licensed by OutSystems (http://www.outsystems.com) to You solely for testing and evaluation 
 purposes, unless You and OutSystems have executed a specific agreement covering the use terms and 
 conditions of the Generated Software, in which case such agreement shall apply. 
*/

using System;
using System.Linq;
using OutSystems.HubEdition.RuntimePlatform;
using OutSystems.HubEdition.RuntimePlatform.Db;
using OutSystems.Internal.Db;
using OutSystems.RuntimeCommon;
using OutSystems.RuntimeCommon.Caching;

using System.Text;
using System.Data;
using System.Collections.Generic;
using Newtonsoft.Json;
using OutSystems.RESTService;
using OutSystems.RESTService.Behaviors;
using OutSystems.RESTService.Controllers;
using OutSystems.ObjectKeys;
using OutSystems.HubEdition.RuntimePlatform.NewRuntime;


namespace ssBIFY.RestRecords {

	public class JSONENXAxisValuesTypeEntityRecord: AbstractRESTStructure<ENXAxisValuesTypeEntityRecord> {
		[JsonProperty("Id")]
		public int? AttrId;

		[JsonProperty("Label")]
		public string AttrLabel;

		[JsonProperty("Order")]
		public int? AttrOrder;

		public JSONENXAxisValuesTypeEntityRecord() {}

		public JSONENXAxisValuesTypeEntityRecord(ENXAxisValuesTypeEntityRecord s, IBehaviorsConfiguration config) {
			if (config.DefaultValuesBehavior == DefaultValuesBehavior.DontSend) {
				AttrId = (int?) s.ssId;
				AttrLabel = s.ssLabel;
				AttrOrder = ConvertToRestWithoutDefaults(s.ssOrder, 0);
			} else {
				AttrId = (int?) s.ssId;
				AttrLabel = s.ssLabel;
				AttrOrder = (int?) s.ssOrder;
			}
		}

		public static Func<ssBIFY.RestRecords.JSONENXAxisValuesTypeEntityRecord, ENXAxisValuesTypeEntityRecord> ToStructureDelegate(IBehaviorsConfiguration config) {
			return (ssBIFY.RestRecords.JSONENXAxisValuesTypeEntityRecord s) => ToStructure(s, config);
		}
		public static ENXAxisValuesTypeEntityRecord ToStructure(ssBIFY.RestRecords.JSONENXAxisValuesTypeEntityRecord obj, IBehaviorsConfiguration config) {
			ENXAxisValuesTypeEntityRecord s = new ENXAxisValuesTypeEntityRecord(null);
			if (obj != null) {
				s.ssId = obj.AttrId == null ? 0: obj.AttrId.Value;
				s.ssLabel = obj.AttrLabel == null ? "": obj.AttrLabel;
				s.ssOrder = obj.AttrOrder == null ? 0: obj.AttrOrder.Value;
			}
			return s;
		}

		public static Func<ENXAxisValuesTypeEntityRecord, ssBIFY.RestRecords.JSONENXAxisValuesTypeEntityRecord> FromStructureDelegate(IBehaviorsConfiguration config) {
			return (ENXAxisValuesTypeEntityRecord s) => FromStructure(s, config);
		}
		public static ssBIFY.RestRecords.JSONENXAxisValuesTypeEntityRecord FromStructure(ENXAxisValuesTypeEntityRecord s, IBehaviorsConfiguration config) {
			return new ssBIFY.RestRecords.JSONENXAxisValuesTypeEntityRecord(s, config);
		}

	}



}