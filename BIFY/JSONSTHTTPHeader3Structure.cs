﻿/* 
 This source code (the "Generated Software") is generated by the OutSystems Platform 
 and is licensed by OutSystems (http://www.outsystems.com) to You solely for testing and evaluation 
 purposes, unless You and OutSystems have executed a specific agreement covering the use terms and 
 conditions of the Generated Software, in which case such agreement shall apply. 
*/

using System;
using System.Linq;
using OutSystems.HubEdition.RuntimePlatform;
using OutSystems.HubEdition.RuntimePlatform.Db;
using OutSystems.Internal.Db;
using OutSystems.RuntimeCommon;
using OutSystems.RuntimeCommon.Caching;

using System.Text;
using System.Data;
using System.Collections.Generic;
using Newtonsoft.Json;
using OutSystems.RESTService;
using OutSystems.RESTService.Behaviors;
using OutSystems.RESTService.Controllers;
using OutSystems.ObjectKeys;
using OutSystems.HubEdition.RuntimePlatform.NewRuntime;


namespace ssBIFY.RestRecords {

	public class JSONSTHTTPHeader3Structure: AbstractRESTStructure<STHTTPHeader3Structure> {
		[JsonProperty("Name")]
		public string AttrName;

		[JsonProperty("Value")]
		public string AttrValue;

		public JSONSTHTTPHeader3Structure() {}

		public JSONSTHTTPHeader3Structure(STHTTPHeader3Structure s, IBehaviorsConfiguration config) {
			if (config.DefaultValuesBehavior == DefaultValuesBehavior.DontSend) {
				AttrName = ConvertToRestWithoutDefaults(s.ssName, "");
				AttrValue = ConvertToRestWithoutDefaults(s.ssValue, "");
			} else {
				AttrName = s.ssName;
				AttrValue = s.ssValue;
			}
		}

		public static Func<ssBIFY.RestRecords.JSONSTHTTPHeader3Structure, STHTTPHeader3Structure> ToStructureDelegate(IBehaviorsConfiguration config) {
			return (ssBIFY.RestRecords.JSONSTHTTPHeader3Structure s) => ToStructure(s, config);
		}
		public static STHTTPHeader3Structure ToStructure(ssBIFY.RestRecords.JSONSTHTTPHeader3Structure obj, IBehaviorsConfiguration config) {
			STHTTPHeader3Structure s = new STHTTPHeader3Structure(null);
			if (obj != null) {
				s.ssName = obj.AttrName == null ? "": obj.AttrName;
				s.ssValue = obj.AttrValue == null ? "": obj.AttrValue;
			}
			return s;
		}

		public static Func<STHTTPHeader3Structure, ssBIFY.RestRecords.JSONSTHTTPHeader3Structure> FromStructureDelegate(IBehaviorsConfiguration config) {
			return (STHTTPHeader3Structure s) => FromStructure(s, config);
		}
		public static ssBIFY.RestRecords.JSONSTHTTPHeader3Structure FromStructure(STHTTPHeader3Structure s, IBehaviorsConfiguration config) {
			return new ssBIFY.RestRecords.JSONSTHTTPHeader3Structure(s, config);
		}

	}



}