﻿/* 
 This source code (the "Generated Software") is generated by the OutSystems Platform 
 and is licensed by OutSystems (http://www.outsystems.com) to You solely for testing and evaluation 
 purposes, unless You and OutSystems have executed a specific agreement covering the use terms and 
 conditions of the Generated Software, in which case such agreement shall apply. 
*/

using System;
using System.Linq;
using OutSystems.HubEdition.RuntimePlatform;
using OutSystems.HubEdition.RuntimePlatform.Db;
using OutSystems.Internal.Db;
using OutSystems.RuntimeCommon;
using OutSystems.RuntimeCommon.Caching;

using System.Text;
using System.Data;
using System.Collections.Generic;
using Newtonsoft.Json;
using OutSystems.RESTService;
using OutSystems.RESTService.Behaviors;
using OutSystems.RESTService.Controllers;
using OutSystems.ObjectKeys;
using OutSystems.HubEdition.RuntimePlatform.NewRuntime;


namespace ssBIFY.RestRecords {

	public class JSONENMenuSubItem3EntityRecord: AbstractRESTStructure<ENMenuSubItem3EntityRecord> {
		[JsonProperty("Id")]
		public int? AttrId;

		[JsonProperty("Order")]
		public int? AttrOrder;

		[JsonProperty("Caption")]
		public string AttrCaption;

		[JsonProperty("MenuItemId")]
		public int? AttrMenuItemId;

		public JSONENMenuSubItem3EntityRecord() {}

		public JSONENMenuSubItem3EntityRecord(ENMenuSubItem3EntityRecord s, IBehaviorsConfiguration config) {
			if (config.DefaultValuesBehavior == DefaultValuesBehavior.DontSend) {
				AttrId = (int?) s.ssId;
				AttrOrder = (int?) s.ssOrder;
				AttrCaption = s.ssCaption;
				AttrMenuItemId = ConvertToRestWithoutDefaults(s.ssMenuItemId, 0);
			} else {
				AttrId = (int?) s.ssId;
				AttrOrder = (int?) s.ssOrder;
				AttrCaption = s.ssCaption;
				AttrMenuItemId = (int?) s.ssMenuItemId;
			}
		}

		public static Func<ssBIFY.RestRecords.JSONENMenuSubItem3EntityRecord, ENMenuSubItem3EntityRecord> ToStructureDelegate(IBehaviorsConfiguration config) {
			return (ssBIFY.RestRecords.JSONENMenuSubItem3EntityRecord s) => ToStructure(s, config);
		}
		public static ENMenuSubItem3EntityRecord ToStructure(ssBIFY.RestRecords.JSONENMenuSubItem3EntityRecord obj, IBehaviorsConfiguration config) {
			ENMenuSubItem3EntityRecord s = new ENMenuSubItem3EntityRecord(null);
			if (obj != null) {
				s.ssId = obj.AttrId == null ? 0: obj.AttrId.Value;
				s.ssOrder = obj.AttrOrder == null ? 0: obj.AttrOrder.Value;
				s.ssCaption = obj.AttrCaption == null ? "": obj.AttrCaption;
				s.ssMenuItemId = obj.AttrMenuItemId == null ? 0: obj.AttrMenuItemId.Value;
			}
			return s;
		}

		public static Func<ENMenuSubItem3EntityRecord, ssBIFY.RestRecords.JSONENMenuSubItem3EntityRecord> FromStructureDelegate(IBehaviorsConfiguration config) {
			return (ENMenuSubItem3EntityRecord s) => FromStructure(s, config);
		}
		public static ssBIFY.RestRecords.JSONENMenuSubItem3EntityRecord FromStructure(ENMenuSubItem3EntityRecord s, IBehaviorsConfiguration config) {
			return new ssBIFY.RestRecords.JSONENMenuSubItem3EntityRecord(s, config);
		}

	}



}