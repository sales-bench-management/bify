﻿/* 
 This source code (the "Generated Software") is generated by the OutSystems Platform 
 and is licensed by OutSystems (http://www.outsystems.com) to You solely for testing and evaluation 
 purposes, unless You and OutSystems have executed a specific agreement covering the use terms and 
 conditions of the Generated Software, in which case such agreement shall apply. 
*/

using System;
using System.Linq;
using OutSystems.HubEdition.RuntimePlatform;
using OutSystems.HubEdition.RuntimePlatform.Db;
using OutSystems.Internal.Db;
using OutSystems.RuntimeCommon;
using OutSystems.RuntimeCommon.Caching;

using System.Text;
using System.Data;
using System.Collections.Generic;
using Newtonsoft.Json;
using OutSystems.RESTService;
using OutSystems.RESTService.Behaviors;
using OutSystems.RESTService.Controllers;
using OutSystems.ObjectKeys;
using OutSystems.HubEdition.RuntimePlatform.NewRuntime;


namespace ssBIFY.RestRecords {

	public class JSONENFundTypeEntityRecord: AbstractRESTStructure<ENFundTypeEntityRecord> {
		[JsonProperty("Id")]
		public int? AttrId;

		[JsonProperty("Label")]
		public string AttrLabel;

		[JsonProperty("Order")]
		public int? AttrOrder;

		[JsonProperty("Is_Active")]
		public bool? AttrIs_Active;

		public JSONENFundTypeEntityRecord() {}

		public JSONENFundTypeEntityRecord(ENFundTypeEntityRecord s, IBehaviorsConfiguration config) {
			if (config.DefaultValuesBehavior == DefaultValuesBehavior.DontSend) {
				AttrId = (int?) s.ssId;
				AttrLabel = s.ssLabel;
				AttrOrder = (int?) s.ssOrder;
				AttrIs_Active = (bool?) s.ssIs_Active;
			} else {
				AttrId = (int?) s.ssId;
				AttrLabel = s.ssLabel;
				AttrOrder = (int?) s.ssOrder;
				AttrIs_Active = (bool?) s.ssIs_Active;
			}
		}

		public static Func<ssBIFY.RestRecords.JSONENFundTypeEntityRecord, ENFundTypeEntityRecord> ToStructureDelegate(IBehaviorsConfiguration config) {
			return (ssBIFY.RestRecords.JSONENFundTypeEntityRecord s) => ToStructure(s, config);
		}
		public static ENFundTypeEntityRecord ToStructure(ssBIFY.RestRecords.JSONENFundTypeEntityRecord obj, IBehaviorsConfiguration config) {
			ENFundTypeEntityRecord s = new ENFundTypeEntityRecord(null);
			if (obj != null) {
				s.ssId = obj.AttrId == null ? 0: obj.AttrId.Value;
				s.ssLabel = obj.AttrLabel == null ? "": obj.AttrLabel;
				s.ssOrder = obj.AttrOrder == null ? 0: obj.AttrOrder.Value;
				s.ssIs_Active = obj.AttrIs_Active == null ? false: obj.AttrIs_Active.Value;
			}
			return s;
		}

		public static Func<ENFundTypeEntityRecord, ssBIFY.RestRecords.JSONENFundTypeEntityRecord> FromStructureDelegate(IBehaviorsConfiguration config) {
			return (ENFundTypeEntityRecord s) => FromStructure(s, config);
		}
		public static ssBIFY.RestRecords.JSONENFundTypeEntityRecord FromStructure(ENFundTypeEntityRecord s, IBehaviorsConfiguration config) {
			return new ssBIFY.RestRecords.JSONENFundTypeEntityRecord(s, config);
		}

	}



}