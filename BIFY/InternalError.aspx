﻿<%@ Page Language="c#" Codebehind="InternalError.aspx.cs" AutoEventWireup="false" Inherits="ssBIFY.Flows.FlowCommon.ScrnInternalError" %>
<%@ Register TagPrefix="osweb" Namespace="OutSystems.HubEdition.WebWidgets" Assembly="OutSystems.HubEdition.WebWidgets" %>
<%@ Import namespace="ssBIFY" %>
<%@ Import namespace="OutSystems.HubEdition.RuntimePlatform" %>
<%@ Assembly Name="OutSystems.WidgetsRuntimeAPI" %>
<%@ Register TagPrefix="widgets" TagName="Kvm5_RC0mw0KuAAMxuiwtpQ" Src="Blocks\BIFY\Common\Layout.ascx" %>
<%@ Register TagPrefix="widgets" TagName="KXIDBGmiH30CQls7epjSjoQ" Src="Blocks\BIFY\Common\ApplicationTitle.ascx" %>
<%@ Register TagPrefix="widgets" TagName="KAotDlTFMDkS99aVrmrPR8g" Src="Blocks\BIFY\Weblocks\MeFriendsBlock.ascx" %>
<%@ Register TagPrefix="OutSystemsUIWeb_widgets" TagName="K0OMmaHyl9E68I_pv5pITnw" Src="Blocks\BIFY\Content\Card.ascx" %>
<%@ Register TagPrefix="RichWidgets_widgets" TagName="KmbfKJ2gWQUq1Gwk_0SjV4w" Src="Blocks\BIFY\RichWidgets\Icon.ascx" %>

<%= BIFY_Properties.DocType %>
<html xmlns="http://www.w3.org/1999/xhtml" >
  <head runat="server"><%= GetHeadTopJavaScript() %>
	<title><%= HttpUtility.HtmlEncode (Title) %></title>
    <meta http-equiv="Content-Type" content="<%= "text/html; charset=" + Response.ContentEncoding.WebName %>" />
    <meta http-equiv="Content-Script-Type" content="text/javascript" />
    <meta http-equiv="Content-Style-Type" content="text/css" />
<%= "\n" + GetStyleSheetIncludes() %><%= GetRequestInfoJavaScript() + GetJavaScriptIncludes() + GetHeadBottomJavaScript() %>
  </head>
  <osweb:Body runat="server"><%= GetBodyTopJavaScript() %>
    <osweb:Form id="WebForm1" method="post"  action="<%# GetFormAction() %>" runat="server">
<widgets:Kvm5_RC0mw0KuAAMxuiwtpQ runat="server" id="wt_WebBlockInstance15" OnEvaluateParameters="webBlck_WebBlockInstance15_onDataBinding" InstanceID="_WebBlockInstance15"><phLeft><widgets:KXIDBGmiH30CQls7epjSjoQ runat="server" id="wt_WebBlockInstance22" OnEvaluateParameters="webBlck_WebBlockInstance22_onDataBinding" InstanceID="_WebBlockInstance22"></widgets:KXIDBGmiH30CQls7epjSjoQ></phLeft><phCenter></phCenter><phRight><widgets:KAotDlTFMDkS99aVrmrPR8g runat="server" id="wt_WebBlockInstance17" OnEvaluateParameters="webBlck_WebBlockInstance17_onDataBinding" InstanceID="_WebBlockInstance17"></widgets:KAotDlTFMDkS99aVrmrPR8g></phRight><phHeaderContent></phHeaderContent><phBreadcrumbs></phBreadcrumbs><phTitle></phTitle><phActions></phActions><phMainContent><OutSystemsUIWeb_widgets:K0OMmaHyl9E68I_pv5pITnw runat="server" id="OutSystemsUIWeb_wt_WebBlockInstance1" OnEvaluateParameters="OutSystemsUIWeb_webBlck_WebBlockInstance1_onDataBinding" InstanceID="_WebBlockInstance1"><phContent><osweb:Container runat="server" id="wtTitle" onDataBinding="cntTitle_onDataBinding" cssClass="text-error heading1"><RichWidgets_widgets:KmbfKJ2gWQUq1Gwk_0SjV4w runat="server" id="RichWidgets_wt_WebBlockInstance7" OnEvaluateParameters="RichWidgets_webBlck_WebBlockInstance7_onDataBinding" InstanceID="_WebBlockInstance7"></RichWidgets_widgets:KmbfKJ2gWQUq1Gwk_0SjV4w><osweb:PlaceHolder runat="server"><%# "Oops" %></osweb:PlaceHolder></osweb:Container><osweb:Container runat="server" id="wtMessage" onDataBinding="cntMessage_onDataBinding" align="center"><osweb:PlaceHolder runat="server"><%# "Something has gone wrong. We&#39;re really sorry. " %></osweb:PlaceHolder></osweb:Container><osweb:Container runat="server" id="wtLink" onDataBinding="cntLink_onDataBinding" align="center"><osweb:HyperLink runat="server" id="wt_Link11" Visible="<%# lnk_Link11_isVisible() %>" Enabled="<%# lnk_Link11_isEnabled() %>" NavigateUrl="<%# lnk_Link11_NavigateUrl() %>"><osweb:PlaceHolder runat="server"><%# "Click here to continue" %></osweb:PlaceHolder></osweb:HyperLink></osweb:Container></phContent></OutSystemsUIWeb_widgets:K0OMmaHyl9E68I_pv5pITnw></phMainContent><phFooter></phFooter></widgets:Kvm5_RC0mw0KuAAMxuiwtpQ><osweb:DummySubmitLink runat="server" id="Dummy_Submit_Link"/>
    <%= OutSystems.HubEdition.RuntimePlatform.AppInfo.GetAppInfo().GetWatermark() %><%= GetCallbackDebug()	
%><%= GetVisitCode() %></osweb:Form><%= GetEndPageJavaScript() + GetBodyBottomJavaScript() %>
  </osweb:Body>
</html>
