﻿/* 
 This source code (the "Generated Software") is generated by the OutSystems Platform 
 and is licensed by OutSystems (http://www.outsystems.com) to You solely for testing and evaluation 
 purposes, unless You and OutSystems have executed a specific agreement covering the use terms and 
 conditions of the Generated Software, in which case such agreement shall apply. 
*/

using System;
using System.Linq;
using OutSystems.HubEdition.RuntimePlatform;
using OutSystems.HubEdition.RuntimePlatform.Db;
using OutSystems.Internal.Db;
using OutSystems.RuntimeCommon;
using OutSystems.RuntimeCommon.Caching;

using System.Text;
using System.Data;
using System.Collections.Generic;
using Newtonsoft.Json;
using OutSystems.RESTService;
using OutSystems.RESTService.Behaviors;
using OutSystems.RESTService.Controllers;
using OutSystems.ObjectKeys;
using OutSystems.HubEdition.RuntimePlatform.NewRuntime;


namespace ssBIFY.RestRecords {

	public class JSONSTErrorItemStructure: AbstractRESTStructure<STErrorItemStructure> {
		[JsonProperty("message")]
		public string AttrMessage;

		[JsonProperty("field")]
		public string AttrField;

		[JsonProperty("help")]
		public string AttrHelp;

		[JsonProperty("error_indices")]
		public long[] AttrError_indices;

		public JSONSTErrorItemStructure() {}

		public JSONSTErrorItemStructure(STErrorItemStructure s, IBehaviorsConfiguration config) {
			if (config.DefaultValuesBehavior == DefaultValuesBehavior.DontSend) {
				AttrMessage = ConvertToRestWithoutDefaults(s.ssMessage, "");
				AttrField = ConvertToRestWithoutDefaults(s.ssField, "");
				AttrHelp = ConvertToRestWithoutDefaults(s.ssHelp, "");
				AttrError_indices = s.ssError_indices.Length == 0 ? null: s.ssError_indices.ToArray();
			} else {
				AttrMessage = s.ssMessage;
				AttrField = s.ssField;
				AttrHelp = s.ssHelp;
				AttrError_indices = s.ssError_indices.ToArray();
			}
		}

		public static Func<ssBIFY.RestRecords.JSONSTErrorItemStructure, STErrorItemStructure> ToStructureDelegate(IBehaviorsConfiguration config) {
			return (ssBIFY.RestRecords.JSONSTErrorItemStructure s) => ToStructure(s, config);
		}
		public static STErrorItemStructure ToStructure(ssBIFY.RestRecords.JSONSTErrorItemStructure obj, IBehaviorsConfiguration config) {
			STErrorItemStructure s = new STErrorItemStructure(null);
			if (obj != null) {
				s.ssMessage = obj.AttrMessage == null ? "": obj.AttrMessage;
				s.ssField = obj.AttrField == null ? "": obj.AttrField;
				s.ssHelp = obj.AttrHelp == null ? "": obj.AttrHelp;
				s.ssError_indices = BasicTypeList<long>.ToList(obj.AttrError_indices);
			}
			return s;
		}

		public static Func<STErrorItemStructure, ssBIFY.RestRecords.JSONSTErrorItemStructure> FromStructureDelegate(IBehaviorsConfiguration config) {
			return (STErrorItemStructure s) => FromStructure(s, config);
		}
		public static ssBIFY.RestRecords.JSONSTErrorItemStructure FromStructure(STErrorItemStructure s, IBehaviorsConfiguration config) {
			return new ssBIFY.RestRecords.JSONSTErrorItemStructure(s, config);
		}

	}



}