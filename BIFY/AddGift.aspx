﻿<%@ Page Language="c#" Codebehind="AddGift.aspx.cs" AutoEventWireup="false" Inherits="ssBIFY.Flows.FlowLoggedIn.ScrnAddGift" %>
<%@ Register TagPrefix="osweb" Namespace="OutSystems.HubEdition.WebWidgets" Assembly="OutSystems.HubEdition.WebWidgets" %>
<%@ Import namespace="ssBIFY" %>
<%@ Import namespace="OutSystems.HubEdition.RuntimePlatform" %>
<%@ Assembly Name="OutSystems.WidgetsRuntimeAPI" %>
<%@ Register TagPrefix="widgets" TagName="Kvm5_RC0mw0KuAAMxuiwtpQ" Src="Blocks\BIFY\Common\Layout.ascx" %>
<%@ Register TagPrefix="widgets" TagName="KXIDBGmiH30CQls7epjSjoQ" Src="Blocks\BIFY\Common\ApplicationTitle.ascx" %>
<%@ Register TagPrefix="widgets" TagName="K_5sqIAe8Z0mLMNBss5hGzg" Src="Blocks\BIFY\Common\Menu.ascx" %>
<%@ Register TagPrefix="widgets" TagName="KAotDlTFMDkS99aVrmrPR8g" Src="Blocks\BIFY\Weblocks\MeFriendsBlock.ascx" %>
<%@ Register TagPrefix="widgets" TagName="KlboP3Mo1qU6C2FuyPN_r2w" Src="Blocks\BIFY\Weblocks\SearchBoxHeader.ascx" %>
<%@ Register TagPrefix="RichWidgets_widgets" TagName="KmbfKJ2gWQUq1Gwk_0SjV4w" Src="Blocks\BIFY\RichWidgets\Icon.ascx" %>
<%@ Register TagPrefix="widgets" TagName="K5k62cdt0FUeYKSRz_54MJg" Src="Blocks\BIFY\ModalContent\AddANewGiftContent.ascx" %>
<%@ Register TagPrefix="widgets" TagName="KcGiUZvJCtEiF3fde7a3MaQ" Src="Blocks\BIFY\Weblocks\FooterLoggedIn.ascx" %>

<%= BIFY_Properties.DocType %>
<html xmlns="http://www.w3.org/1999/xhtml" >
  <head runat="server"><%= GetHeadTopJavaScript() %>
	<title><%= HttpUtility.HtmlEncode (Title) %></title>
    <meta http-equiv="Content-Type" content="<%= "text/html; charset=" + Response.ContentEncoding.WebName %>" />
    <meta http-equiv="Content-Script-Type" content="text/javascript" />
    <meta http-equiv="Content-Style-Type" content="text/css" />
<%= "\n" + GetStyleSheetIncludes() %><%= GetRequestInfoJavaScript() + GetJavaScriptIncludes() + GetHeadBottomJavaScript() %>
  </head>
  <osweb:Body runat="server"><%= GetBodyTopJavaScript() %>
    <osweb:Form id="WebForm1" method="post"  action="<%# GetFormAction() %>" runat="server">
<widgets:Kvm5_RC0mw0KuAAMxuiwtpQ runat="server" id="wt_WebBlockInstance9" OnEvaluateParameters="webBlck_WebBlockInstance9_onDataBinding" InstanceID="_WebBlockInstance9"><phLeft><widgets:KXIDBGmiH30CQls7epjSjoQ runat="server" id="wt_WebBlockInstance16" OnEvaluateParameters="webBlck_WebBlockInstance16_onDataBinding" InstanceID="_WebBlockInstance16"></widgets:KXIDBGmiH30CQls7epjSjoQ></phLeft><phCenter><widgets:K_5sqIAe8Z0mLMNBss5hGzg runat="server" id="wt_WebBlockInstance8" OnEvaluateParameters="webBlck_WebBlockInstance8_onDataBinding" InstanceID="_WebBlockInstance8"></widgets:K_5sqIAe8Z0mLMNBss5hGzg></phCenter><phRight><widgets:KAotDlTFMDkS99aVrmrPR8g runat="server" id="wt_WebBlockInstance13" OnEvaluateParameters="webBlck_WebBlockInstance13_onDataBinding" InstanceID="_WebBlockInstance13"></widgets:KAotDlTFMDkS99aVrmrPR8g><widgets:KlboP3Mo1qU6C2FuyPN_r2w runat="server" id="wt_WebBlockInstance3" OnEvaluateParameters="webBlck_WebBlockInstance3_onDataBinding" InstanceID="_WebBlockInstance3"></widgets:KlboP3Mo1qU6C2FuyPN_r2w></phRight><phHeaderContent></phHeaderContent><phBreadcrumbs><osweb:Container runat="server" id="wt_Container6" anonymous="true" onDataBinding="cnt_Container6_onDataBinding"><osweb:HyperLink runat="server" id="wt_Link18" Visible="<%# lnk_Link18_isVisible() %>" Enabled="<%# lnk_Link18_isEnabled() %>" NavigateUrl="<%# lnk_Link18_NavigateUrl() %>"><osweb:Container runat="server" id="wt_Container20" anonymous="true" onDataBinding="cnt_Container20_onDataBinding" GridCssClasses="OSInline"><RichWidgets_widgets:KmbfKJ2gWQUq1Gwk_0SjV4w runat="server" id="RichWidgets_wt_WebBlockInstance12" OnEvaluateParameters="RichWidgets_webBlck_WebBlockInstance12_onDataBinding" InstanceID="_WebBlockInstance12"></RichWidgets_widgets:KmbfKJ2gWQUq1Gwk_0SjV4w><osweb:PlaceHolder runat="server"><%# "Back" %></osweb:PlaceHolder></osweb:Container></osweb:HyperLink></osweb:Container></phBreadcrumbs><phTitle></phTitle><phActions></phActions><phMainContent><widgets:K5k62cdt0FUeYKSRz_54MJg runat="server" id="wt_WebBlockInstance14" OnEvaluateParameters="webBlck_WebBlockInstance14_onDataBinding" OnBindDelegates="webBlck_WebBlockInstance14_BindDelegates" InstanceID="_WebBlockInstance14"></widgets:K5k62cdt0FUeYKSRz_54MJg></phMainContent><phFooter><widgets:KcGiUZvJCtEiF3fde7a3MaQ runat="server" id="wt_WebBlockInstance21" OnEvaluateParameters="webBlck_WebBlockInstance21_onDataBinding" InstanceID="_WebBlockInstance21"></widgets:KcGiUZvJCtEiF3fde7a3MaQ></phFooter></widgets:Kvm5_RC0mw0KuAAMxuiwtpQ><osweb:DummySubmitLink runat="server" id="Dummy_Submit_Link"/>
    <%= OutSystems.HubEdition.RuntimePlatform.AppInfo.GetAppInfo().GetWatermark() %><%= GetCallbackDebug()	
%><%= GetVisitCode() %></osweb:Form><%= GetEndPageJavaScript() + GetBodyBottomJavaScript() %>
  </osweb:Body>
</html>
