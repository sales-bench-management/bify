﻿/* 
 This source code (the "Generated Software") is generated by the OutSystems Platform 
 and is licensed by OutSystems (http://www.outsystems.com) to You solely for testing and evaluation 
 purposes, unless You and OutSystems have executed a specific agreement covering the use terms and 
 conditions of the Generated Software, in which case such agreement shall apply. 
*/

using System;
using System.Linq;
using OutSystems.HubEdition.RuntimePlatform;
using OutSystems.HubEdition.RuntimePlatform.Db;
using OutSystems.Internal.Db;
using OutSystems.RuntimeCommon;
using OutSystems.RuntimeCommon.Caching;

using System.Text;
using System.Data;
using System.Collections.Generic;
using Newtonsoft.Json;
using OutSystems.RESTService;
using OutSystems.RESTService.Behaviors;
using OutSystems.RESTService.Controllers;
using OutSystems.ObjectKeys;
using OutSystems.HubEdition.RuntimePlatform.NewRuntime;


namespace ssBIFY.RestRecords {

	public class JSONENNotificationEntityRecord: AbstractRESTStructure<ENNotificationEntityRecord> {
		[JsonProperty("Id")]
		public long? AttrId;

		[JsonProperty("UserId")]
		public int? AttrUserId;

		[JsonProperty("Title")]
		public string AttrTitle;

		[JsonProperty("Detail")]
		public string AttrDetail;

		[JsonProperty("ButtonLabel")]
		public string AttrButtonLabel;

		[JsonProperty("ButtonURL")]
		public string AttrButtonURL;

		[JsonProperty("CreatedOn")]
		public String AttrCreatedOn;

		[JsonProperty("Seen")]
		public bool? AttrSeen;

		[JsonProperty("SeenOn")]
		public String AttrSeenOn;

		[JsonProperty("Cleared")]
		public bool? AttrCleared;

		[JsonProperty("ClearedOn")]
		public String AttrClearedOn;

		public JSONENNotificationEntityRecord() {}

		public JSONENNotificationEntityRecord(ENNotificationEntityRecord s, IBehaviorsConfiguration config) {
			if (config.DefaultValuesBehavior == DefaultValuesBehavior.DontSend) {
				AttrId = (long?) s.ssId;
				AttrUserId = ConvertToRestWithoutDefaults(s.ssUserId, 0);
				AttrTitle = ConvertToRestWithoutDefaults(s.ssTitle, "");
				AttrDetail = ConvertToRestWithoutDefaults(s.ssDetail, "");
				AttrButtonLabel = ConvertToRestWithoutDefaults(s.ssButtonLabel, "");
				AttrButtonURL = ConvertToRestWithoutDefaults(s.ssButtonURL, "");
				AttrCreatedOn = ConvertDateTimeToRestWithoutDefaults(s.ssCreatedOn, new DateTime(1900, 1, 1, 0, 0, 0), config.DateTimeFormat);
				AttrSeen = ConvertToRestWithoutDefaults(s.ssSeen, false);
				AttrSeenOn = ConvertDateTimeToRestWithoutDefaults(s.ssSeenOn, new DateTime(1900, 1, 1, 0, 0, 0), config.DateTimeFormat);
				AttrCleared = ConvertToRestWithoutDefaults(s.ssCleared, false);
				AttrClearedOn = ConvertDateTimeToRestWithoutDefaults(s.ssClearedOn, new DateTime(1900, 1, 1, 0, 0, 0), config.DateTimeFormat);
			} else {
				AttrId = (long?) s.ssId;
				AttrUserId = (int?) s.ssUserId;
				AttrTitle = s.ssTitle;
				AttrDetail = s.ssDetail;
				AttrButtonLabel = s.ssButtonLabel;
				AttrButtonURL = s.ssButtonURL;
				AttrCreatedOn = OutSystems.RESTService.Conversions.DateTimeToRestType(s.ssCreatedOn, config.DateTimeFormat);
				AttrSeen = (bool?) s.ssSeen;
				AttrSeenOn = OutSystems.RESTService.Conversions.DateTimeToRestType(s.ssSeenOn, config.DateTimeFormat);
				AttrCleared = (bool?) s.ssCleared;
				AttrClearedOn = OutSystems.RESTService.Conversions.DateTimeToRestType(s.ssClearedOn, config.DateTimeFormat);
			}
		}

		public static Func<ssBIFY.RestRecords.JSONENNotificationEntityRecord, ENNotificationEntityRecord> ToStructureDelegate(IBehaviorsConfiguration config) {
			return (ssBIFY.RestRecords.JSONENNotificationEntityRecord s) => ToStructure(s, config);
		}
		public static ENNotificationEntityRecord ToStructure(ssBIFY.RestRecords.JSONENNotificationEntityRecord obj, IBehaviorsConfiguration config) {
			ENNotificationEntityRecord s = new ENNotificationEntityRecord(null);
			if (obj != null) {
				s.ssId = obj.AttrId == null ? 0L: obj.AttrId.Value;
				s.ssUserId = obj.AttrUserId == null ? 0: obj.AttrUserId.Value;
				s.ssTitle = obj.AttrTitle == null ? "": obj.AttrTitle;
				s.ssDetail = obj.AttrDetail == null ? "": obj.AttrDetail;
				s.ssButtonLabel = obj.AttrButtonLabel == null ? "": obj.AttrButtonLabel;
				s.ssButtonURL = obj.AttrButtonURL == null ? "": obj.AttrButtonURL;
				s.ssCreatedOn = obj.AttrCreatedOn == null ? new DateTime(1900, 1, 1, 0, 0, 0): OutSystems.RESTService.Conversions.TextToDateTime(obj.AttrCreatedOn, config.DateTimeFormat);
				s.ssSeen = obj.AttrSeen == null ? false: obj.AttrSeen.Value;
				s.ssSeenOn = obj.AttrSeenOn == null ? new DateTime(1900, 1, 1, 0, 0, 0): OutSystems.RESTService.Conversions.TextToDateTime(obj.AttrSeenOn, config.DateTimeFormat);
				s.ssCleared = obj.AttrCleared == null ? false: obj.AttrCleared.Value;
				s.ssClearedOn = obj.AttrClearedOn == null ? new DateTime(1900, 1, 1, 0, 0, 0): OutSystems.RESTService.Conversions.TextToDateTime(obj.AttrClearedOn, config.DateTimeFormat);
			}
			return s;
		}

		public static Func<ENNotificationEntityRecord, ssBIFY.RestRecords.JSONENNotificationEntityRecord> FromStructureDelegate(IBehaviorsConfiguration config) {
			return (ENNotificationEntityRecord s) => FromStructure(s, config);
		}
		public static ssBIFY.RestRecords.JSONENNotificationEntityRecord FromStructure(ENNotificationEntityRecord s, IBehaviorsConfiguration config) {
			return new ssBIFY.RestRecords.JSONENNotificationEntityRecord(s, config);
		}

	}



}