﻿<%@ Page Language="c#" Codebehind="MyInfo.aspx.cs" AutoEventWireup="false" Inherits="ssBIFY.Flows.FlowCommon.ScrnMyInfo" %>
<%@ Register TagPrefix="osweb" Namespace="OutSystems.HubEdition.WebWidgets" Assembly="OutSystems.HubEdition.WebWidgets" %>
<%@ Import namespace="ssBIFY" %>
<%@ Import namespace="OutSystems.HubEdition.RuntimePlatform" %>
<%@ Assembly Name="OutSystems.WidgetsRuntimeAPI" %>
<%@ Register TagPrefix="widgets" TagName="Kvm5_RC0mw0KuAAMxuiwtpQ" Src="Blocks\BIFY\Common\Layout.ascx" %>
<%@ Register TagPrefix="widgets" TagName="KXIDBGmiH30CQls7epjSjoQ" Src="Blocks\BIFY\Common\ApplicationTitle.ascx" %>
<%@ Register TagPrefix="widgets" TagName="K_5sqIAe8Z0mLMNBss5hGzg" Src="Blocks\BIFY\Common\Menu.ascx" %>
<%@ Register TagPrefix="widgets" TagName="KeTXYsEiyT0iz699EGQA9pw" Src="Blocks\BIFY\Common\LoginInfo.ascx" %>
<%@ Register TagPrefix="OutSystemsUIWeb_widgets" TagName="K0OMmaHyl9E68I_pv5pITnw" Src="Blocks\BIFY\Content\Card.ascx" %>
<%@ Register TagPrefix="Users_widgets" TagName="KctWZ_esj_kuK_h4cO5oIqA" Src="Blocks\BIFY\UserManagement\EditMyInfo.ascx" %>
<%@ Register TagPrefix="Users_widgets" TagName="KQWN_E9RQQkSYKKIat9bWkw" Src="Blocks\BIFY\UserManagement\ChangePassword.ascx" %>

<%= BIFY_Properties.DocType %>
<html xmlns="http://www.w3.org/1999/xhtml" >
  <head runat="server"><%= GetHeadTopJavaScript() %>
	<title><%= HttpUtility.HtmlEncode (Title) %></title>
    <meta http-equiv="Content-Type" content="<%= "text/html; charset=" + Response.ContentEncoding.WebName %>" />
    <meta http-equiv="Content-Script-Type" content="text/javascript" />
    <meta http-equiv="Content-Style-Type" content="text/css" />
<%= "\n" + GetStyleSheetIncludes() %><%= GetRequestInfoJavaScript() + GetJavaScriptIncludes() + GetHeadBottomJavaScript() %>
  </head>
  <osweb:Body runat="server"><%= GetBodyTopJavaScript() %>
    <osweb:Form id="WebForm1" method="post"  action="<%# GetFormAction() %>" runat="server">
<widgets:Kvm5_RC0mw0KuAAMxuiwtpQ runat="server" id="wt_WebBlockInstance24" OnEvaluateParameters="webBlck_WebBlockInstance24_onDataBinding" InstanceID="_WebBlockInstance24"><phLeft><widgets:KXIDBGmiH30CQls7epjSjoQ runat="server" id="wt_WebBlockInstance2" OnEvaluateParameters="webBlck_WebBlockInstance2_onDataBinding" InstanceID="_WebBlockInstance2"></widgets:KXIDBGmiH30CQls7epjSjoQ></phLeft><phCenter><widgets:K_5sqIAe8Z0mLMNBss5hGzg runat="server" id="wt_WebBlockInstance20" OnEvaluateParameters="webBlck_WebBlockInstance20_onDataBinding" InstanceID="_WebBlockInstance20"></widgets:K_5sqIAe8Z0mLMNBss5hGzg></phCenter><phRight><widgets:KeTXYsEiyT0iz699EGQA9pw runat="server" id="wt_WebBlockInstance15" OnEvaluateParameters="webBlck_WebBlockInstance15_onDataBinding" InstanceID="_WebBlockInstance15"></widgets:KeTXYsEiyT0iz699EGQA9pw></phRight><phHeaderContent></phHeaderContent><phBreadcrumbs></phBreadcrumbs><phTitle><osweb:PlaceHolder runat="server"><%# "My Info" %></osweb:PlaceHolder></phTitle><phActions><osweb:LinkButton runat="server" id="wt_Link16" onDataBinding="lnk_Link16_onDataBinding" Visible="<%# lnk_Link16_isVisible() %>" Enabled="<%# lnk_Link16_isEnabled() %>" CausesValidation="false"><osweb:PlaceHolder runat="server"><%# "Change Password" %></osweb:PlaceHolder></osweb:LinkButton></phActions><phMainContent><OutSystemsUIWeb_widgets:K0OMmaHyl9E68I_pv5pITnw runat="server" id="OutSystemsUIWeb_wt_WebBlockInstance19" OnEvaluateParameters="OutSystemsUIWeb_webBlck_WebBlockInstance19_onDataBinding" InstanceID="_WebBlockInstance19"><phContent><osweb:Container runat="server" id="wtDivRefresh" onDataBinding="cntDivRefresh_onDataBinding"><osweb:IfPlaceHolder runat="server"><osweb:If runat="server" visible="<%# if_wt_If11() %>"><Users_widgets:KctWZ_esj_kuK_h4cO5oIqA runat="server" id="Users_wt_WebBlockInstance5" onAjaxNotify="Users_webBlck_WebBlockInstance5_AjaxNotify" OnEvaluateParameters="Users_webBlck_WebBlockInstance5_onDataBinding" OnBindDelegates="Users_webBlck_WebBlockInstance5_BindDelegates" InstanceID="_WebBlockInstance5"></Users_widgets:KctWZ_esj_kuK_h4cO5oIqA></osweb:If><osweb:If runat="server" visible="<%# !if_wt_If11() %>"><Users_widgets:KQWN_E9RQQkSYKKIat9bWkw runat="server" id="Users_wt_WebBlockInstance3" onAjaxNotify="Users_webBlck_WebBlockInstance3_AjaxNotify" OnEvaluateParameters="Users_webBlck_WebBlockInstance3_onDataBinding" OnBindDelegates="Users_webBlck_WebBlockInstance3_BindDelegates" InstanceID="_WebBlockInstance3"></Users_widgets:KQWN_E9RQQkSYKKIat9bWkw></osweb:If></osweb:IfPlaceHolder></osweb:Container></phContent></OutSystemsUIWeb_widgets:K0OMmaHyl9E68I_pv5pITnw></phMainContent><phFooter></phFooter></widgets:Kvm5_RC0mw0KuAAMxuiwtpQ><osweb:DummySubmitLink runat="server" id="Dummy_Submit_Link"/>
    <%= OutSystems.HubEdition.RuntimePlatform.AppInfo.GetAppInfo().GetWatermark() %><%= GetCallbackDebug()	
%><%= GetVisitCode() %></osweb:Form><%= GetEndPageJavaScript() + GetBodyBottomJavaScript() %>
  </osweb:Body>
</html>
