﻿/* 
 This source code (the "Generated Software") is generated by the OutSystems Platform 
 and is licensed by OutSystems (http://www.outsystems.com) to You solely for testing and evaluation 
 purposes, unless You and OutSystems have executed a specific agreement covering the use terms and 
 conditions of the Generated Software, in which case such agreement shall apply. 
*/

using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Runtime.Serialization;
using System.Reflection;
using System.Text;
using System.Text.RegularExpressions;
using OutSystems.HubEdition.RuntimePlatform;
using OutSystems.HubEdition.RuntimePlatform.Log;
using OutSystems.HubEdition.RuntimePlatform.Db;
using OutSystems.Internal.Db;
using OutSystems.HubEdition.RuntimePlatform.Email;
using OutSystems.HubEdition.RuntimePlatform.Web;
using OutSystems.HubEdition.RuntimePlatform.NewRuntime;
using OutSystems.RuntimeCommon;
using OutSystems.RuntimeCommon.Caching;
using OutSystems.ObjectKeys;
using System.Resources;

namespace ssBIFY {

	public partial class Actions {
		public class lcvParseFixDoubleBracket: VarsBag {
			public string inParamOriginalResponse;
			public lcvParseFixDoubleBracket(string inParamOriginalResponse) {
				this.inParamOriginalResponse = inParamOriginalResponse;
			}
		}
		public class lcoParseFixDoubleBracket: VarsBag {
			public string outParamNewResponse = "";

			public lcoParseFixDoubleBracket() {
			}
		}
		/// <summary>
		/// Action <code>ActionParseFixDoubleBracket</code> that represents the Service Studio user action
		///  <code>ParseFixDoubleBracket</code> <p> Description: </p>
		/// </summary>
		public static void ActionParseFixDoubleBracket(HeContext heContext, string inParamOriginalResponse, out string outParamNewResponse) {
			lcoParseFixDoubleBracket result = new lcoParseFixDoubleBracket();
			lcvParseFixDoubleBracket localVars = new lcvParseFixDoubleBracket(inParamOriginalResponse);
			if (heContext != null && heContext.RequestTracer != null) {
				heContext.RequestTracer.RegisterInternalCall("iNDKVoeXNEyIgG5rKNaJXA", "ParseFixDoubleBracket", "1WMl+O7_wkOQDYQl1Ck5zQ", "BIFY");
			}
			try {
				if (heContext != null) {
					heContext.RequestCancelation.ThrowIfCanceled();
				}
				// NewResponse = Replace
				result.outParamNewResponse = BuiltInFunction.Replace(BuiltInFunction.Replace(localVars.inParamOriginalResponse, "[[", "["), "]]", "]");
			} // try

			finally {
				outParamNewResponse = result.outParamNewResponse;
			}
		}

		public static class FuncActionParseFixDoubleBracket {



		}


	}

}