﻿/* 
 This source code (the "Generated Software") is generated by the OutSystems Platform 
 and is licensed by OutSystems (http://www.outsystems.com) to You solely for testing and evaluation 
 purposes, unless You and OutSystems have executed a specific agreement covering the use terms and 
 conditions of the Generated Software, in which case such agreement shall apply. 
*/

using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Runtime.Serialization;
using System.Reflection;
using System.Text;
using System.Text.RegularExpressions;
using OutSystems.HubEdition.RuntimePlatform;
using OutSystems.HubEdition.RuntimePlatform.Log;
using OutSystems.HubEdition.RuntimePlatform.Db;
using OutSystems.Internal.Db;
using OutSystems.HubEdition.RuntimePlatform.Email;
using OutSystems.HubEdition.RuntimePlatform.Web;
using OutSystems.HubEdition.RuntimePlatform.NewRuntime;
using OutSystems.RuntimeCommon;
using OutSystems.RuntimeCommon.Caching;
using OutSystems.ObjectKeys;
using System.Resources;

namespace ssBIFY {

	public partial class Actions {
		public class lcvGetEventBackground: VarsBag {
			public long inParamEventId;
			public RLEventRecordList queryResGetEventById_outParamList = new RLEventRecordList();
			public long queryResGetEventById_outParamCount = 0L;

			public lcvGetEventBackground(long inParamEventId) {
				this.inParamEventId = inParamEventId;
			}
		}
		public class lcoGetEventBackground: VarsBag {
			public string outParamImageUrl = "";

			public lcoGetEventBackground() {
			}
		}
		/// <summary>
		/// Action <code>ActionGetEventBackground</code> that represents the Service Studio user action
		///  <code>GetEventBackground</code> <p> Description: </p>
		/// </summary>
		public static void ActionGetEventBackground(HeContext heContext, long inParamEventId, out string outParamImageUrl) {
			lcoGetEventBackground result = new lcoGetEventBackground();
			lcvGetEventBackground localVars = new lcvGetEventBackground(inParamEventId);
			if (heContext != null && heContext.RequestTracer != null) {
				heContext.RequestTracer.RegisterInternalCall("Kau4U4GC_kiIdZgWBO4uwg", "GetEventBackground", "1WMl+O7_wkOQDYQl1Ck5zQ", "BIFY");
			}
			try {
				if (heContext != null) {
					heContext.RequestCancelation.ThrowIfCanceled();
				}
				// Query datasetGetEventById
				if (heContext != null) {
					heContext.RequestCancelation.ThrowIfCanceled();
				}
				int datasetGetEventById_maxRecords = 0;
				localVars.queryResGetEventById_outParamList = FuncActionGetEventBackground.datasetGetEventById(heContext, datasetGetEventById_maxRecords, IterationMultiplicity.Never, out localVars.queryResGetEventById_outParamCount, localVars.inParamEventId
				);

				// Birthday
				if ((((localVars.queryResGetEventById_outParamList.CurrentRec.ssENEvent.ssEventTypeId==ENEventTypeEntity.GetRecordByKey(ObjectKey.Parse("+NkxjR6JGEypomF6q2WVJw")).ssId) || (localVars.queryResGetEventById_outParamList.CurrentRec.ssENEvent.ssEventTypeId==ENEventTypeEntity.GetRecordByKey(ObjectKey.Parse("+NkxjR6JGEypomF6q2WVJw")).ssId)))) {
					// ImageUrl = SiteURL + "/img/birthday.jpg"
					result.outParamImageUrl = (((((string) Global.SiteProperties["SiteURL"]))) + "/img/birthday.jpg");
				} else {
					if ((localVars.queryResGetEventById_outParamList.CurrentRec.ssENEvent.ssEventTypeId==ENEventTypeEntity.GetRecordByKey(ObjectKey.Parse("cl+ZOq7_pUagr0haOk_otg")).ssId)) {
						// ImageUrl = SiteURL + "/img/anniversary.jpg"
						result.outParamImageUrl = (((((string) Global.SiteProperties["SiteURL"]))) + "/img/anniversary.jpg");
					} else {
						if ((localVars.queryResGetEventById_outParamList.CurrentRec.ssENEvent.ssEventTypeId==ENEventTypeEntity.GetRecordByKey(ObjectKey.Parse("jdMnhb1K3UKFExutBGEi8Q")).ssId)) {
							// ImageUrl = SiteURL + "/img/specialdate.jpg"
							result.outParamImageUrl = (((((string) Global.SiteProperties["SiteURL"]))) + "/img/specialdate.jpg");
						}

					}

				}

			} // try

			finally {
				outParamImageUrl = result.outParamImageUrl;
			}
		}

		public static class FuncActionGetEventBackground {

			/// <summary>
			/// Query Function "GetEventById" of Action "GetEventBackground"
			/// </summary>
			public static RLEventRecordList datasetGetEventById(HeContext heContext, int maxRecords, IterationMultiplicity multiplicity, out long outParamCount, long qploId) {
				// Query Iterations: Never {-unbound-}
				// Uses binary data: False
				if (multiplicity == IterationMultiplicity.Never) {
					maxRecords = 1;
				}
				outParamCount = -1;
				bool useMainTransaction = (multiplicity != IterationMultiplicity.Single || (maxRecords > 0 && maxRecords <= 150));
				Transaction trans = useMainTransaction? DatabaseAccess.ForCurrentDatabase.GetRequestTransaction(): DatabaseAccess.ForCurrentDatabase.GetReadOnlyTransaction();
				using(var sqlCmd = trans.CreateCommand()) {
					string sql = "";
					StringBuilder selectBuilder = new StringBuilder();
					StringBuilder fromBuilder = new StringBuilder();
					StringBuilder whereBuilder = new StringBuilder();
					StringBuilder orderByBuilder = new StringBuilder();
					StringBuilder groupByBuilder = new StringBuilder();
					StringBuilder havingBuilder = new StringBuilder();
					selectBuilder.Append("SELECT ");
					if (maxRecords > 0) {
						selectBuilder.Append("TOP (");
						selectBuilder.Append(maxRecords);
						selectBuilder.Append(") ");
					}
					selectBuilder.Append("NULL o0, NULL o1, NULL o2, NULL o3, [ENEVENT].[EVENTTYPEID] o4, NULL o5, NULL o6");
					fromBuilder.Append(" FROM {Event} [ENEVENT]");
					whereBuilder.Append(" WHERE ");
					if (qploId != 0) {
						whereBuilder.Append("(([ENEVENT].[ID] = @qploId) AND ([ENEVENT].[ID] IS NOT NULL))");
						sqlCmd.CreateParameterWithoutReplacements("@qploId", DbType.Int64, qploId);
					} else {
						whereBuilder.Append("([ENEVENT].[ID] IS NULL)");
					}

					sql = selectBuilder.ToString() + fromBuilder.ToString() + whereBuilder.ToString() + groupByBuilder.ToString() + havingBuilder.ToString() + orderByBuilder.ToString();
					string advSql = sql;
					sql = AppUtils.Instance.ReplaceEntityReferences(heContext, advSql);
					sqlCmd.CommandText = sql;
					try {
						RLEventRecordList outParamList = new RLEventRecordList();
						outParamList.Transaction = trans;
						BitArray[] opt = new BitArray[1];
						opt[0] = new BitArray(new bool[] {
							true, true, true, true, false, true, true});
						outParamList.AllOptimizedAttributes = opt;
						if (multiplicity == IterationMultiplicity.Multiple) {
							RLEventRecordList _tmp = new RLEventRecordList();
							_tmp.Transaction = trans;
							_tmp.AllOptimizedAttributes = opt;
							_tmp.MaxRecords = maxRecords;
							try {
								DateTime startTime = DateTime.Now;
								DatabaseAccess.ForCurrentDatabase.ExecuteQuery(sqlCmd, _tmp, "Query GetEventBackground.GetEventById.List");
								int queryExecutionTime = Convert.ToInt32(DateTime.Now.Subtract(startTime).TotalMilliseconds
								);
								RequestTracer reqTracer = RuntimePlatformUtils.GetRequestTracer();
								if (reqTracer != null) {
									reqTracer.RegisterQueryExecuted("ec9ff5fd-f64c-4b15-90fa-68384190d9c9", "GetEventBackground.GetEventById", "f82563d5-ffee-43c2-900d-8425d42939cd", "BIFY", queryExecutionTime, DateTime.Now);
								}
								outParamList = (RLEventRecordList) _tmp.Duplicate();
								return outParamList;
							} finally {
								_tmp.CloseDataReader();
							}
						} else if (multiplicity == IterationMultiplicity.Never) {
							try {
								DateTime startTime = DateTime.Now;
								DatabaseAccess.ForCurrentDatabase.ExecuteQuery(sqlCmd, outParamList, "Query GetEventBackground.GetEventById.List");
								int queryExecutionTime = Convert.ToInt32(DateTime.Now.Subtract(startTime).TotalMilliseconds
								);
								RequestTracer reqTracer = RuntimePlatformUtils.GetRequestTracer();
								if (reqTracer != null) {
									reqTracer.RegisterQueryExecuted("ec9ff5fd-f64c-4b15-90fa-68384190d9c9", "GetEventBackground.GetEventById", "f82563d5-ffee-43c2-900d-8425d42939cd", "BIFY", queryExecutionTime, DateTime.Now);
								}
								outParamList.MaxRecords = maxRecords;
								return outParamList;
							} finally {
								outParamList.CloseDataReader();
							}
						} else {
							if (maxRecords > 0 && maxRecords <= 150) {
								RLEventRecordList _tmp = new RLEventRecordList();
								_tmp.Transaction = trans;
								_tmp.AllOptimizedAttributes = opt;
								try {
									DateTime startTime = DateTime.Now;
									DatabaseAccess.ForCurrentDatabase.ExecuteQuery(sqlCmd, _tmp, "Query GetEventBackground.GetEventById.List");
									int queryExecutionTime = Convert.ToInt32(DateTime.Now.Subtract(startTime).TotalMilliseconds
									);
									RequestTracer reqTracer = RuntimePlatformUtils.GetRequestTracer();
									if (reqTracer != null) {
										reqTracer.RegisterQueryExecuted("ec9ff5fd-f64c-4b15-90fa-68384190d9c9", "GetEventBackground.GetEventById", "f82563d5-ffee-43c2-900d-8425d42939cd", "BIFY", queryExecutionTime, DateTime.Now);
									}
									outParamList = (RLEventRecordList) _tmp.Duplicate();
									outParamList.AllOptimizedAttributes = opt;
									return outParamList;
								} finally {
									_tmp.CloseDataReader();
								}
							} else {
								try {
									DateTime startTime = DateTime.Now;
									DatabaseAccess.ForCurrentDatabase.ExecuteQuery(sqlCmd, outParamList, "Query GetEventBackground.GetEventById.List");
									int queryExecutionTime = Convert.ToInt32(DateTime.Now.Subtract(startTime).TotalMilliseconds
									);
									RequestTracer reqTracer = RuntimePlatformUtils.GetRequestTracer();
									if (reqTracer != null) {
										reqTracer.RegisterQueryExecuted("ec9ff5fd-f64c-4b15-90fa-68384190d9c9", "GetEventBackground.GetEventById", "f82563d5-ffee-43c2-900d-8425d42939cd", "BIFY", queryExecutionTime, DateTime.Now);
									}
									outParamList.MaxRecords = maxRecords;
									return outParamList;
								} finally {
									if (maxRecords == 1) {
										outParamList.CloseDataReader();
									}
								}
							}
						}
					} catch (Exception e) {
						throw new DataBaseException("Error executing query.", e);
					}
				}
			}



		}


	}

}