﻿/* 
 This source code (the "Generated Software") is generated by the OutSystems Platform 
 and is licensed by OutSystems (http://www.outsystems.com) to You solely for testing and evaluation 
 purposes, unless You and OutSystems have executed a specific agreement covering the use terms and 
 conditions of the Generated Software, in which case such agreement shall apply. 
*/

using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Runtime.Serialization;
using System.Reflection;
using System.Text;
using System.Text.RegularExpressions;
using OutSystems.HubEdition.RuntimePlatform;
using OutSystems.HubEdition.RuntimePlatform.Log;
using OutSystems.HubEdition.RuntimePlatform.Db;
using OutSystems.Internal.Db;
using OutSystems.HubEdition.RuntimePlatform.Email;
using OutSystems.HubEdition.RuntimePlatform.Web;
using OutSystems.HubEdition.RuntimePlatform.NewRuntime;
using OutSystems.RuntimeCommon;
using OutSystems.RuntimeCommon.Caching;
using OutSystems.ObjectKeys;
using System.Resources;

namespace ssBIFY {

	public partial class Actions {
		public class lcvCleansePrice: VarsBag {
			public string inParamPriceText;
			public RLTextRecordList queryResSQL1_outParamList = new RLTextRecordList();
			public long queryResSQL1_outParamCount = 0L;

			public lcvCleansePrice(string inParamPriceText) {
				this.inParamPriceText = inParamPriceText;
			}
		}
		public class lcoCleansePrice: VarsBag {
			public decimal outParamCleansedPrice = 0.0M;

			public lcoCleansePrice() {
			}
		}
		/// <summary>
		/// Action <code>ActionCleansePrice</code> that represents the Service Studio user action
		///  <code>CleansePrice</code> <p> Description: </p>
		/// </summary>
		public static void ActionCleansePrice(HeContext heContext, string inParamPriceText, out decimal outParamCleansedPrice) {
			lcoCleansePrice result = new lcoCleansePrice();
			lcvCleansePrice localVars = new lcvCleansePrice(inParamPriceText);
			if (heContext != null && heContext.RequestTracer != null) {
				heContext.RequestTracer.RegisterInternalCall("KYXJeWi190WhWAiypP+FZw", "CleansePrice", "1WMl+O7_wkOQDYQl1Ck5zQ", "BIFY");
			}
			try {
				if (heContext != null) {
					heContext.RequestCancelation.ThrowIfCanceled();
				}
				// Query QuerySQL1
				if (heContext != null) {
					heContext.RequestCancelation.ThrowIfCanceled();
				}
				int QuerySQL1_maxRecords = 0;
				localVars.queryResSQL1_outParamList = FuncActionCleansePrice.QuerySQL1(heContext, QuerySQL1_maxRecords, IterationMultiplicity.Never, out localVars.queryResSQL1_outParamCount, localVars.inParamPriceText
				);

				// CleansedPrice = TextToDecimal
				result.outParamCleansedPrice = BuiltInFunction.TextToDecimal(localVars.queryResSQL1_outParamList.CurrentRec.ssSTText.ssValue);
			} // try

			finally {
				outParamCleansedPrice = result.outParamCleansedPrice;
			}
		}

		public static class FuncActionCleansePrice {

			/// <summary>
			/// Query Function "SQL1" of Action "CleansePrice"
			/// </summary>
			public static RLTextRecordList QuerySQL1(HeContext heContext, int maxRecords, IterationMultiplicity multiplicity, out long outParamCount, string qpstPrice) {
				// Query Iterations: Never {-unbound-}
				// Uses binary data: False
				if (multiplicity == IterationMultiplicity.Never) {
					maxRecords = 1;
				}
				outParamCount = -1;
				bool useMainTransaction = (multiplicity != IterationMultiplicity.Single || (maxRecords == 1));
				Transaction trans = useMainTransaction? DatabaseAccess.ForCurrentDatabase.GetRequestTransaction(): DatabaseAccess.ForCurrentDatabase.GetReadOnlyTransaction();
				using(var sqlCmd = trans.CreateCommand()) {
					sqlCmd.CreateParameter("@qpstPrice", DbType.String, qpstPrice);
					string advSql = "SELECT  \r\ncase when \r\ncharindex(' ',@qpstPrice)>0 \r\nthen \r\nreplace(replace(substring(@qpstPrice,0,charindex(' ',@qpstPrice)),',',''),'£','') \r\nelse \r\nreplace(replace(@qpstPrice,',',''),'£','') \r\nend";
					string sql = AppUtils.Instance.ReplaceEntityReferences(heContext, advSql);
					AppUtils.Instance.CheckReadOnlyEntityReferences(advSql);
					sqlCmd.CommandText = sql;
					try {
						RLTextRecordList outParamList = new RLTextRecordList();
						outParamList.Transaction = trans;
						BitArray[] opt = new BitArray[1];
						opt[0] = new BitArray(new bool[] {
							false});
						outParamList.AllOptimizedAttributes = opt;
						if (multiplicity == IterationMultiplicity.Multiple) {
							RLTextRecordList _tmp = new RLTextRecordList();
							_tmp.Transaction = trans;
							_tmp.AllOptimizedAttributes = opt;
							_tmp.MaxRecords = maxRecords;
							try {
								DateTime startTime = DateTime.Now;
								DatabaseAccess.ForCurrentDatabase.ExecuteQuery(sqlCmd, _tmp, "Query CleansePrice.SQL1.List");
								int queryExecutionTime = Convert.ToInt32(DateTime.Now.Subtract(startTime).TotalMilliseconds
								);
								RequestTracer reqTracer = RuntimePlatformUtils.GetRequestTracer();
								if (reqTracer != null) {
									reqTracer.RegisterQueryExecuted("99ade48a-05a3-4940-a1fe-387a152a74ed", "CleansePrice.SQL1", "f82563d5-ffee-43c2-900d-8425d42939cd", "BIFY", queryExecutionTime, DateTime.Now);
								}
								outParamList = (RLTextRecordList) _tmp.Duplicate();
								return outParamList;
							} finally {
								_tmp.CloseDataReader();
							}
						} else if (multiplicity == IterationMultiplicity.Never) {
							try {
								DateTime startTime = DateTime.Now;
								DatabaseAccess.ForCurrentDatabase.ExecuteQuery(sqlCmd, outParamList, "Query CleansePrice.SQL1.List");
								int queryExecutionTime = Convert.ToInt32(DateTime.Now.Subtract(startTime).TotalMilliseconds
								);
								RequestTracer reqTracer = RuntimePlatformUtils.GetRequestTracer();
								if (reqTracer != null) {
									reqTracer.RegisterQueryExecuted("99ade48a-05a3-4940-a1fe-387a152a74ed", "CleansePrice.SQL1", "f82563d5-ffee-43c2-900d-8425d42939cd", "BIFY", queryExecutionTime, DateTime.Now);
								}
								outParamList.MaxRecords = maxRecords;
								return outParamList;
							} finally {
								outParamList.CloseDataReader();
							}
						} else {
							try {
								DateTime startTime = DateTime.Now;
								DatabaseAccess.ForCurrentDatabase.ExecuteQuery(sqlCmd, outParamList, "Query CleansePrice.SQL1.List");
								int queryExecutionTime = Convert.ToInt32(DateTime.Now.Subtract(startTime).TotalMilliseconds
								);
								RequestTracer reqTracer = RuntimePlatformUtils.GetRequestTracer();
								if (reqTracer != null) {
									reqTracer.RegisterQueryExecuted("99ade48a-05a3-4940-a1fe-387a152a74ed", "CleansePrice.SQL1", "f82563d5-ffee-43c2-900d-8425d42939cd", "BIFY", queryExecutionTime, DateTime.Now);
								}
								outParamList.MaxRecords = maxRecords;
								return outParamList;
							} finally {
								if (maxRecords == 1) {
									outParamList.CloseDataReader();
								}
							}
						}
					} catch (System.InvalidOperationException aqExcep) {
						if (aqExcep.Message.StartsWith("Command parameter[")) {
							throw new DataBaseException("Error executing query.", new DataBaseException("Problems accessing parameters in advanced query SQL1 in CleansePrice in BIFY (SELECT   case when  charindex(' ',@Price)>0  then  replace(replace(substring(@Price,0,charindex(' ',@Price)),',',''),'£','')  else  replace(replace(@Price,',',''),'£','')  end): " + aqExcep.Message));
						}
						throw new DataBaseException("Error executing query.", new DataBaseException("Error in advanced query SQL1 in CleansePrice in BIFY (SELECT   case when  charindex(' ',@Price)>0  then  replace(replace(substring(@Price,0,charindex(' ',@Price)),',',''),'£','')  else  replace(replace(@Price,',',''),'£','')  end): " + aqExcep.Message));
					} catch (Exception aqExcep) {
						throw new DataBaseException("Error executing query.", new DataBaseException("Error in advanced query SQL1 in CleansePrice in BIFY (SELECT   case when  charindex(' ',@Price)>0  then  replace(replace(substring(@Price,0,charindex(' ',@Price)),',',''),'£','')  else  replace(replace(@Price,',',''),'£','')  end): " + aqExcep.Message));
					}
				}
			}



		}


	}

}