﻿/* 
 This source code (the "Generated Software") is generated by the OutSystems Platform 
 and is licensed by OutSystems (http://www.outsystems.com) to You solely for testing and evaluation 
 purposes, unless You and OutSystems have executed a specific agreement covering the use terms and 
 conditions of the Generated Software, in which case such agreement shall apply. 
*/

using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Runtime.Serialization;
using System.Reflection;
using System.Text;
using System.Text.RegularExpressions;
using OutSystems.HubEdition.RuntimePlatform;
using OutSystems.HubEdition.RuntimePlatform.Log;
using OutSystems.HubEdition.RuntimePlatform.Db;
using OutSystems.Internal.Db;
using OutSystems.HubEdition.RuntimePlatform.Email;
using OutSystems.HubEdition.RuntimePlatform.Web;
using OutSystems.HubEdition.RuntimePlatform.NewRuntime;
using OutSystems.RuntimeCommon;
using OutSystems.RuntimeCommon.Caching;
using OutSystems.ObjectKeys;
using System.Resources;

namespace ssBIFY {

	public partial class Actions {
		public class lcvGetGiftsForUser: VarsBag {
			public RLSample_ProductRecordList queryResGetProducts_outParamList = new RLSample_ProductRecordList();
			public long queryResGetProducts_outParamCount = 0L;

			public lcvGetGiftsForUser() {
			}
		}
		public class lcoGetGiftsForUser: VarsBag {
			public RLSample_ProductList outParamListOfProducts = new RLSample_ProductList();

			public lcoGetGiftsForUser() {
			}
		}
		/// <summary>
		/// Action <code>ActionGetGiftsForUser</code> that represents the Service Studio user action
		///  <code>GetGiftsForUser</code> <p> Description: </p>
		/// </summary>
		public static void ActionGetGiftsForUser(HeContext heContext, out RLSample_ProductList outParamListOfProducts) {
			lcoGetGiftsForUser result = new lcoGetGiftsForUser();
			lcvGetGiftsForUser localVars = new lcvGetGiftsForUser();
			if (heContext != null && heContext.RequestTracer != null) {
				heContext.RequestTracer.RegisterInternalCall("H+y2e0Bq9kCymKMbrDNelA", "GetGiftsForUser", "1WMl+O7_wkOQDYQl1Ck5zQ", "BIFY");
			}
			try {
				if (heContext != null) {
					heContext.RequestCancelation.ThrowIfCanceled();
				}
				// Query datasetGetProducts
				if (heContext != null) {
					heContext.RequestCancelation.ThrowIfCanceled();
				}
				int datasetGetProducts_maxRecords = 0;
				localVars.queryResGetProducts_outParamList = FuncActionGetGiftsForUser.datasetGetProducts(heContext, datasetGetProducts_maxRecords, IterationMultiplicity.Multiple, out localVars.queryResGetProducts_outParamCount);

				// ListOfProducts = GetProducts.List
				result.outParamListOfProducts = RLSample_ProductList.Convert(localVars.queryResGetProducts_outParamList, new RLSample_ProductList(), (ref RCSample_ProductRecord source, ref ENSample_ProductEntityRecord target) => {
					target = source;
				});
			} // try

			finally {
				outParamListOfProducts = result.outParamListOfProducts;
			}
		}

		public static class FuncActionGetGiftsForUser {

			/// <summary>
			/// Query Function "GetProducts" of Action "GetGiftsForUser"
			/// </summary>
			public static RLSample_ProductRecordList datasetGetProducts(HeContext heContext, int maxRecords, IterationMultiplicity multiplicity, out long outParamCount) {
				// Query Iterations: Multiple {-unbound-}
				// Uses binary data: True
				if (multiplicity == IterationMultiplicity.Never) {
					maxRecords = 1;
				}
				outParamCount = -1;
				bool useMainTransaction = (multiplicity != IterationMultiplicity.Single || (maxRecords > 0 && maxRecords <= 3));
				Transaction trans = useMainTransaction? DatabaseAccess.ForCurrentDatabase.GetRequestTransaction(): DatabaseAccess.ForCurrentDatabase.GetReadOnlyTransaction();
				using(var sqlCmd = trans.CreateCommand()) {
					string sql = "";
					StringBuilder selectBuilder = new StringBuilder();
					StringBuilder fromBuilder = new StringBuilder();
					StringBuilder whereBuilder = new StringBuilder();
					StringBuilder orderByBuilder = new StringBuilder();
					StringBuilder groupByBuilder = new StringBuilder();
					StringBuilder havingBuilder = new StringBuilder();
					selectBuilder.Append("SELECT ");
					if (maxRecords > 0) {
						selectBuilder.Append("TOP (");
						selectBuilder.Append(maxRecords);
						selectBuilder.Append(") ");
					}
					selectBuilder.Append("[ENSAMPLE_PRODUCT].[ID] o0, [ENSAMPLE_PRODUCT].[NAME] o1, [ENSAMPLE_PRODUCT].[DESCRIPTION] o2, [ENSAMPLE_PRODUCT].[IMAGE] o3, [ENSAMPLE_PRODUCT].[FILENAME] o4, [ENSAMPLE_PRODUCT].[PRICE] o5, [ENSAMPLE_PRODUCT].[STOCK] o6, [ENSAMPLE_PRODUCT].[MAXSTOCK] o7, [ENSAMPLE_PRODUCT].[STOCKTHRESHOLD] o8, [ENSAMPLE_PRODUCT].[CATEGORY] o9, [ENSAMPLE_PRODUCT].[CREATEDON] o10");
					fromBuilder.Append(" FROM {Sample_Product} [ENSAMPLE_PRODUCT]");
					orderByBuilder.Append(" ORDER BY [ENSAMPLE_PRODUCT].[NAME] ASC ");

					sql = selectBuilder.ToString() + fromBuilder.ToString() + whereBuilder.ToString() + groupByBuilder.ToString() + havingBuilder.ToString() + orderByBuilder.ToString();
					string advSql = sql;
					sql = AppUtils.Instance.ReplaceEntityReferences(heContext, advSql);
					sqlCmd.CommandText = sql;
					try {
						RLSample_ProductRecordList outParamList = new RLSample_ProductRecordList();
						outParamList.Transaction = trans;
						BitArray[] opt = new BitArray[1];
						opt[0] = new BitArray(new bool[] {
							false, false, false, false, false, false, false, false, false, false, false});
						outParamList.AllOptimizedAttributes = opt;
						if (multiplicity == IterationMultiplicity.Multiple) {
							RLSample_ProductRecordList _tmp = new RLSample_ProductRecordList();
							_tmp.Transaction = trans;
							_tmp.AllOptimizedAttributes = opt;
							_tmp.MaxRecords = maxRecords;
							try {
								DateTime startTime = DateTime.Now;
								DatabaseAccess.ForCurrentDatabase.ExecuteQuery(sqlCmd, _tmp, "Query GetGiftsForUser.GetProducts.List");
								int queryExecutionTime = Convert.ToInt32(DateTime.Now.Subtract(startTime).TotalMilliseconds
								);
								RequestTracer reqTracer = RuntimePlatformUtils.GetRequestTracer();
								if (reqTracer != null) {
									reqTracer.RegisterQueryExecuted("4c86bc18-79b9-4cbe-82ca-22a782985b74", "GetGiftsForUser.GetProducts", "f82563d5-ffee-43c2-900d-8425d42939cd", "BIFY", queryExecutionTime, DateTime.Now);
								}
								outParamList = (RLSample_ProductRecordList) _tmp.Duplicate();
								return outParamList;
							} finally {
								_tmp.CloseDataReader();
							}
						} else if (multiplicity == IterationMultiplicity.Never) {
							try {
								DateTime startTime = DateTime.Now;
								DatabaseAccess.ForCurrentDatabase.ExecuteQuery(sqlCmd, outParamList, "Query GetGiftsForUser.GetProducts.List");
								int queryExecutionTime = Convert.ToInt32(DateTime.Now.Subtract(startTime).TotalMilliseconds
								);
								RequestTracer reqTracer = RuntimePlatformUtils.GetRequestTracer();
								if (reqTracer != null) {
									reqTracer.RegisterQueryExecuted("4c86bc18-79b9-4cbe-82ca-22a782985b74", "GetGiftsForUser.GetProducts", "f82563d5-ffee-43c2-900d-8425d42939cd", "BIFY", queryExecutionTime, DateTime.Now);
								}
								outParamList.MaxRecords = maxRecords;
								return outParamList;
							} finally {
								outParamList.CloseDataReader();
							}
						} else {
							if (maxRecords > 0 && maxRecords <= 3) {
								RLSample_ProductRecordList _tmp = new RLSample_ProductRecordList();
								_tmp.Transaction = trans;
								_tmp.AllOptimizedAttributes = opt;
								try {
									DateTime startTime = DateTime.Now;
									DatabaseAccess.ForCurrentDatabase.ExecuteQuery(sqlCmd, _tmp, "Query GetGiftsForUser.GetProducts.List");
									int queryExecutionTime = Convert.ToInt32(DateTime.Now.Subtract(startTime).TotalMilliseconds
									);
									RequestTracer reqTracer = RuntimePlatformUtils.GetRequestTracer();
									if (reqTracer != null) {
										reqTracer.RegisterQueryExecuted("4c86bc18-79b9-4cbe-82ca-22a782985b74", "GetGiftsForUser.GetProducts", "f82563d5-ffee-43c2-900d-8425d42939cd", "BIFY", queryExecutionTime, DateTime.Now);
									}
									outParamList = (RLSample_ProductRecordList) _tmp.Duplicate();
									outParamList.AllOptimizedAttributes = opt;
									return outParamList;
								} finally {
									_tmp.CloseDataReader();
								}
							} else {
								try {
									DateTime startTime = DateTime.Now;
									DatabaseAccess.ForCurrentDatabase.ExecuteQuery(sqlCmd, outParamList, "Query GetGiftsForUser.GetProducts.List");
									int queryExecutionTime = Convert.ToInt32(DateTime.Now.Subtract(startTime).TotalMilliseconds
									);
									RequestTracer reqTracer = RuntimePlatformUtils.GetRequestTracer();
									if (reqTracer != null) {
										reqTracer.RegisterQueryExecuted("4c86bc18-79b9-4cbe-82ca-22a782985b74", "GetGiftsForUser.GetProducts", "f82563d5-ffee-43c2-900d-8425d42939cd", "BIFY", queryExecutionTime, DateTime.Now);
									}
									outParamList.MaxRecords = maxRecords;
									return outParamList;
								} finally {
									if (maxRecords == 1) {
										outParamList.CloseDataReader();
									}
								}
							}
						}
					} catch (Exception e) {
						throw new DataBaseException("Error executing query.", e);
					}
				}
			}



		}


	}

}