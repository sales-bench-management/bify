﻿/* 
 This source code (the "Generated Software") is generated by the OutSystems Platform 
 and is licensed by OutSystems (http://www.outsystems.com) to You solely for testing and evaluation 
 purposes, unless You and OutSystems have executed a specific agreement covering the use terms and 
 conditions of the Generated Software, in which case such agreement shall apply. 
*/

using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Runtime.Serialization;
using System.Reflection;
using System.Text;
using System.Text.RegularExpressions;
using OutSystems.HubEdition.RuntimePlatform;
using OutSystems.HubEdition.RuntimePlatform.Log;
using OutSystems.HubEdition.RuntimePlatform.Db;
using OutSystems.Internal.Db;
using OutSystems.HubEdition.RuntimePlatform.Email;
using OutSystems.HubEdition.RuntimePlatform.Web;
using OutSystems.HubEdition.RuntimePlatform.NewRuntime;
using OutSystems.RuntimeCommon;
using OutSystems.RuntimeCommon.Caching;
using OutSystems.ObjectKeys;
using System.Resources;

namespace ssBIFY {

	public partial class Actions {
		public class lcvParseImageArrayBug: VarsBag {
			public string inParamOriginalResponse;
			/// <summary>
			/// Variable <code>varLcFirstBit</code> that represents the Service Studio local variable
			///  <code>FirstBit</code> <p>Description: </p>
			/// </summary>
			public string varLcFirstBit = "";

			/// <summary>
			/// Variable <code>varLcSecondBit</code> that represents the Service Studio local variable
			///  <code>SecondBit</code> <p>Description: </p>
			/// </summary>
			public string varLcSecondBit = "";

			/// <summary>
			/// Variable <code>varLcSecondBitTrimmed</code> that represents the Service Studio local variable
			///  <code>SecondBitTrimmed</code> <p>Description: </p>
			/// </summary>
			public string varLcSecondBitTrimmed = "";

			/// <summary>
			/// Variable <code>varLcThirdBit</code> that represents the Service Studio local variable
			///  <code>ThirdBit</code> <p>Description: </p>
			/// </summary>
			public string varLcThirdBit = "";

			/// <summary>
			/// Variable <code>varLcCompleteBit</code> that represents the Service Studio local variable
			///  <code>CompleteBit</code> <p>Description: </p>
			/// </summary>
			public string varLcCompleteBit = "";

			public lcvParseImageArrayBug(string inParamOriginalResponse) {
				this.inParamOriginalResponse = inParamOriginalResponse;
			}
		}
		public class lcoParseImageArrayBug: VarsBag {
			public string outParamNewResponse = "";

			public lcoParseImageArrayBug() {
			}
		}
		/// <summary>
		/// Action <code>ActionParseImageArrayBug</code> that represents the Service Studio user action
		///  <code>ParseImageArrayBug</code> <p> Description: </p>
		/// </summary>
		public static void ActionParseImageArrayBug(HeContext heContext, string inParamOriginalResponse, out string outParamNewResponse) {
			lcoParseImageArrayBug result = new lcoParseImageArrayBug();
			lcvParseImageArrayBug localVars = new lcvParseImageArrayBug(inParamOriginalResponse);
			if (heContext != null && heContext.RequestTracer != null) {
				heContext.RequestTracer.RegisterInternalCall("7ISO00BV_UmwIvKJF9IP5g", "ParseImageArrayBug", "1WMl+O7_wkOQDYQl1Ck5zQ", "BIFY");
			}
			try {
				if (heContext != null) {
					heContext.RequestCancelation.ThrowIfCanceled();
				}
				// Found a problem string?
				while (((BuiltInFunction.IndexSC(localVars.inParamOriginalResponse, "images\":\"", 0, false, false) != (-1)))) {
					// FIX
					// FirstBit = Substr + "["
					localVars.varLcFirstBit = (BuiltInFunction.SubstrSC(localVars.inParamOriginalResponse, 0, (BuiltInFunction.IndexSC(localVars.inParamOriginalResponse, "images\":\"", 0, false, false) +8)) + "[");
					// SecondBit = Substr
					localVars.varLcSecondBit = BuiltInFunction.SubstrSC(localVars.inParamOriginalResponse, (BuiltInFunction.IndexSC(localVars.inParamOriginalResponse, "images\":\"", 0, false, false) +9), 1000);
					// SecondBitTrimmed = """" + Substr + """" + "]"
					localVars.varLcSecondBitTrimmed = ((("\"" +BuiltInFunction.SubstrSC(localVars.varLcSecondBit, 0, BuiltInFunction.IndexSC(localVars.varLcSecondBit, "\"", 0, false, false))) + "\"") + "]");
					// ThirdBit = Trim
					localVars.varLcThirdBit = BuiltInFunction.Trim(BuiltInFunction.SubstrSC(localVars.inParamOriginalResponse, (BuiltInFunction.LengthSC((localVars.varLcFirstBit+localVars.varLcSecondBitTrimmed)) -2), 10000));
					// CompleteBit = FirstBit + SecondBitTrimmed + ThirdBit
					localVars.varLcCompleteBit = ((localVars.varLcFirstBit+localVars.varLcSecondBitTrimmed) +localVars.varLcThirdBit);
					// NewResponse = CompleteBit
					result.outParamNewResponse = localVars.varLcCompleteBit;
					// OriginalResponse = NewResponse
					localVars.inParamOriginalResponse = result.outParamNewResponse;
				}

				// OK
				// NewResponse = OriginalResponse
				result.outParamNewResponse = localVars.inParamOriginalResponse;
			} // try

			finally {
				outParamNewResponse = result.outParamNewResponse;
			}
		}

		public static class FuncActionParseImageArrayBug {



		}


	}

}