﻿/* 
 This source code (the "Generated Software") is generated by the OutSystems Platform 
 and is licensed by OutSystems (http://www.outsystems.com) to You solely for testing and evaluation 
 purposes, unless You and OutSystems have executed a specific agreement covering the use terms and 
 conditions of the Generated Software, in which case such agreement shall apply. 
*/

using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Runtime.Serialization;
using System.Reflection;
using System.Text;
using System.Text.RegularExpressions;
using OutSystems.HubEdition.RuntimePlatform;
using OutSystems.HubEdition.RuntimePlatform.Log;
using OutSystems.HubEdition.RuntimePlatform.Db;
using OutSystems.Internal.Db;
using OutSystems.HubEdition.RuntimePlatform.Email;
using OutSystems.HubEdition.RuntimePlatform.Web;
using OutSystems.HubEdition.RuntimePlatform.NewRuntime;
using OutSystems.RuntimeCommon;
using OutSystems.RuntimeCommon.Caching;
using OutSystems.ObjectKeys;
using System.Resources;

namespace ssBIFY {

	public partial class Actions {
		public class lcvNotification: VarsBag {
			public string inParamTitle;
			public string inParamMessage;
			public string inParamButtonLabel;
			public string inParamButtonURL;
			/// <summary>
			/// Variable <code>varLcNotification</code> that represents the Service Studio local variable
			///  <code>Notification</code> <p>Description: </p>
			/// </summary>
			public ENNotificationEntityRecord varLcNotification = new ENNotificationEntityRecord(null);

			public long resCreateNotification_outParamId = 0L;

			public lcvNotification(string inParamTitle, string inParamMessage, string inParamButtonLabel, string inParamButtonURL) {
				this.inParamTitle = inParamTitle;
				this.inParamMessage = inParamMessage;
				this.inParamButtonLabel = inParamButtonLabel;
				this.inParamButtonURL = inParamButtonURL;
			}
		}
		/// <summary>
		/// Action <code>ActionNotification</code> that represents the Service Studio user action
		///  <code>Notification</code> <p> Description: </p>
		/// </summary>
		public static void ActionNotification(HeContext heContext, string inParamTitle, string inParamMessage, string inParamButtonLabel, string inParamButtonURL) {
			lcvNotification localVars = new lcvNotification(inParamTitle, inParamMessage, inParamButtonLabel, inParamButtonURL);
			if (heContext != null && heContext.RequestTracer != null) {
				heContext.RequestTracer.RegisterInternalCall("bpxHBZS+DE6qZVmzXtGHXw", "Notification", "1WMl+O7_wkOQDYQl1Ck5zQ", "BIFY");
			}
			if (heContext != null) {
				heContext.RequestCancelation.ThrowIfCanceled();
			}
			// Notification.UserId = GetUserId
			localVars.varLcNotification.ssUserId = BuiltInFunction.GetUserId();
			// Notification.Title = Title
			localVars.varLcNotification.ssTitle = localVars.inParamTitle;
			// Notification.Detail = Message
			localVars.varLcNotification.ssDetail = localVars.inParamMessage;
			// Notification.ButtonLabel = ButtonLabel
			localVars.varLcNotification.ssButtonLabel = localVars.inParamButtonLabel;
			// Notification.ButtonURL = ButtonURL
			localVars.varLcNotification.ssButtonURL = localVars.inParamButtonURL;
			// CreateNotification
			if (heContext != null) {
				heContext.RequestCancelation.ThrowIfCanceled();
			}
			ExtendedActions.CreateNotification(heContext, (((RCNotificationRecord) localVars.varLcNotification)), out localVars.resCreateNotification_outParamId);

		}

		public static class FuncActionNotification {



		}


	}

}