﻿/* 
 This source code (the "Generated Software") is generated by the OutSystems Platform 
 and is licensed by OutSystems (http://www.outsystems.com) to You solely for testing and evaluation 
 purposes, unless You and OutSystems have executed a specific agreement covering the use terms and 
 conditions of the Generated Software, in which case such agreement shall apply. 
*/

using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Runtime.Serialization;
using System.Reflection;
using System.Text;
using System.Text.RegularExpressions;
using OutSystems.HubEdition.RuntimePlatform;
using OutSystems.HubEdition.RuntimePlatform.Log;
using OutSystems.HubEdition.RuntimePlatform.Db;
using OutSystems.Internal.Db;
using OutSystems.HubEdition.RuntimePlatform.Email;
using OutSystems.HubEdition.RuntimePlatform.Web;
using OutSystems.HubEdition.RuntimePlatform.NewRuntime;
using OutSystems.RuntimeCommon;
using OutSystems.RuntimeCommon.Caching;
using OutSystems.ObjectKeys;
using System.Resources;

namespace ssBIFY {

	public partial class Actions {
		public class lcvCreateFundHash_s: VarsBag {
			public RLFundRecordList queryResGetFunds_outParamList = new RLFundRecordList();
			public long queryResGetFunds_outParamCount = 0L;

			public lcvCreateFundHash_s() {
			}
		}
		/// <summary>
		/// Action <code>ActionCreateFundHash_s</code> that represents the Service Studio user action
		///  <code>CreateFundHash_s</code> <p> Description: </p>
		/// </summary>
		public static void ActionCreateFundHash_s(HeContext heContext) {
			lcvCreateFundHash_s localVars = new lcvCreateFundHash_s();
			if (heContext != null && heContext.RequestTracer != null) {
				heContext.RequestTracer.RegisterInternalCall("SOlNXL62T0+Lztj+dHic5Q", "CreateFundHash_s", "1WMl+O7_wkOQDYQl1Ck5zQ", "BIFY");
			}
			if (heContext != null) {
				heContext.RequestCancelation.ThrowIfCanceled();
			}
			// Query datasetGetFunds
			if (heContext != null) {
				heContext.RequestCancelation.ThrowIfCanceled();
			}
			int datasetGetFunds_maxRecords = 0;
			localVars.queryResGetFunds_outParamList = FuncActionCreateFundHash_s.datasetGetFunds(heContext, datasetGetFunds_maxRecords, IterationMultiplicity.Single, out localVars.queryResGetFunds_outParamCount);

			// Foreach GetFunds.List
			localVars.queryResGetFunds_outParamList.StartIteration();
			try {
				while (!((localVars.queryResGetFunds_outParamList.Eof))) {
					if (((localVars.queryResGetFunds_outParamList.CurrentRec.ssENFund.ssHash== ""))) {
						// createhash
						// GetFunds.List.Current.Fund.Hash = GeneratePassword
						localVars.queryResGetFunds_outParamList.CurrentRec.ssENFund.ssHash = BuiltInFunction.GeneratePassword(35, true);
						// UpdateFund
						if (heContext != null) {
							heContext.RequestCancelation.ThrowIfCanceled();
						}
						ExtendedActions.UpdateFund(heContext, localVars.queryResGetFunds_outParamList.CurrentRec.ChangedAttributes, localVars.queryResGetFunds_outParamList.CurrentRec);

					}

					localVars.queryResGetFunds_outParamList.Advance();
				}

			} finally {
				localVars.queryResGetFunds_outParamList.EndIteration();
			}

		}

		public static class FuncActionCreateFundHash_s {

			/// <summary>
			/// Query Function "GetFunds" of Action "CreateFundHash_s"
			/// </summary>
			public static RLFundRecordList datasetGetFunds(HeContext heContext, int maxRecords, IterationMultiplicity multiplicity, out long outParamCount) {
				// Query Iterations: Single {-unbound-}
				// Uses binary data: False
				if (multiplicity == IterationMultiplicity.Never) {
					maxRecords = 1;
				}
				outParamCount = -1;
				bool useMainTransaction = (multiplicity != IterationMultiplicity.Single || (maxRecords > 0 && maxRecords <= 150));
				Transaction trans = useMainTransaction? DatabaseAccess.ForCurrentDatabase.GetRequestTransaction(): DatabaseAccess.ForCurrentDatabase.GetReadOnlyTransaction();
				using(var sqlCmd = trans.CreateCommand()) {
					string sql = "";
					StringBuilder selectBuilder = new StringBuilder();
					StringBuilder fromBuilder = new StringBuilder();
					StringBuilder whereBuilder = new StringBuilder();
					StringBuilder orderByBuilder = new StringBuilder();
					StringBuilder groupByBuilder = new StringBuilder();
					StringBuilder havingBuilder = new StringBuilder();
					selectBuilder.Append("SELECT ");
					if (maxRecords > 0) {
						selectBuilder.Append("TOP (");
						selectBuilder.Append(maxRecords);
						selectBuilder.Append(") ");
					}
					selectBuilder.Append("[ENFUND].[ID] o0, NULL o1, NULL o2, NULL o3, NULL o4, NULL o5, NULL o6, NULL o7, NULL o8, NULL o9, NULL o10, NULL o11, [ENFUND].[HASH] o12");
					fromBuilder.Append(" FROM {Fund} [ENFUND]");
					orderByBuilder.Append(" ORDER BY [ENFUND].[FUNDNAME] ASC ");

					sql = selectBuilder.ToString() + fromBuilder.ToString() + whereBuilder.ToString() + groupByBuilder.ToString() + havingBuilder.ToString() + orderByBuilder.ToString();
					string advSql = sql;
					sql = AppUtils.Instance.ReplaceEntityReferences(heContext, advSql);
					sqlCmd.CommandText = sql;
					try {
						RLFundRecordList outParamList = new RLFundRecordList();
						outParamList.Transaction = trans;
						BitArray[] opt = new BitArray[1];
						opt[0] = new BitArray(new bool[] {
							false, true, true, true, true, true, true, true, true, true, true, true, false});
						outParamList.AllOptimizedAttributes = opt;
						if (multiplicity == IterationMultiplicity.Multiple) {
							RLFundRecordList _tmp = new RLFundRecordList();
							_tmp.Transaction = trans;
							_tmp.AllOptimizedAttributes = opt;
							_tmp.MaxRecords = maxRecords;
							try {
								DateTime startTime = DateTime.Now;
								DatabaseAccess.ForCurrentDatabase.ExecuteQuery(sqlCmd, _tmp, "Query CreateFundHash_s.GetFunds.List");
								int queryExecutionTime = Convert.ToInt32(DateTime.Now.Subtract(startTime).TotalMilliseconds
								);
								RequestTracer reqTracer = RuntimePlatformUtils.GetRequestTracer();
								if (reqTracer != null) {
									reqTracer.RegisterQueryExecuted("4988f6e7-aff3-4487-8230-7808eac109fb", "CreateFundHash_s.GetFunds", "f82563d5-ffee-43c2-900d-8425d42939cd", "BIFY", queryExecutionTime, DateTime.Now);
								}
								outParamList = (RLFundRecordList) _tmp.Duplicate();
								return outParamList;
							} finally {
								_tmp.CloseDataReader();
							}
						} else if (multiplicity == IterationMultiplicity.Never) {
							try {
								DateTime startTime = DateTime.Now;
								DatabaseAccess.ForCurrentDatabase.ExecuteQuery(sqlCmd, outParamList, "Query CreateFundHash_s.GetFunds.List");
								int queryExecutionTime = Convert.ToInt32(DateTime.Now.Subtract(startTime).TotalMilliseconds
								);
								RequestTracer reqTracer = RuntimePlatformUtils.GetRequestTracer();
								if (reqTracer != null) {
									reqTracer.RegisterQueryExecuted("4988f6e7-aff3-4487-8230-7808eac109fb", "CreateFundHash_s.GetFunds", "f82563d5-ffee-43c2-900d-8425d42939cd", "BIFY", queryExecutionTime, DateTime.Now);
								}
								outParamList.MaxRecords = maxRecords;
								return outParamList;
							} finally {
								outParamList.CloseDataReader();
							}
						} else {
							if (maxRecords > 0 && maxRecords <= 150) {
								RLFundRecordList _tmp = new RLFundRecordList();
								_tmp.Transaction = trans;
								_tmp.AllOptimizedAttributes = opt;
								try {
									DateTime startTime = DateTime.Now;
									DatabaseAccess.ForCurrentDatabase.ExecuteQuery(sqlCmd, _tmp, "Query CreateFundHash_s.GetFunds.List");
									int queryExecutionTime = Convert.ToInt32(DateTime.Now.Subtract(startTime).TotalMilliseconds
									);
									RequestTracer reqTracer = RuntimePlatformUtils.GetRequestTracer();
									if (reqTracer != null) {
										reqTracer.RegisterQueryExecuted("4988f6e7-aff3-4487-8230-7808eac109fb", "CreateFundHash_s.GetFunds", "f82563d5-ffee-43c2-900d-8425d42939cd", "BIFY", queryExecutionTime, DateTime.Now);
									}
									outParamList = (RLFundRecordList) _tmp.Duplicate();
									outParamList.AllOptimizedAttributes = opt;
									return outParamList;
								} finally {
									_tmp.CloseDataReader();
								}
							} else {
								try {
									DateTime startTime = DateTime.Now;
									DatabaseAccess.ForCurrentDatabase.ExecuteQuery(sqlCmd, outParamList, "Query CreateFundHash_s.GetFunds.List");
									int queryExecutionTime = Convert.ToInt32(DateTime.Now.Subtract(startTime).TotalMilliseconds
									);
									RequestTracer reqTracer = RuntimePlatformUtils.GetRequestTracer();
									if (reqTracer != null) {
										reqTracer.RegisterQueryExecuted("4988f6e7-aff3-4487-8230-7808eac109fb", "CreateFundHash_s.GetFunds", "f82563d5-ffee-43c2-900d-8425d42939cd", "BIFY", queryExecutionTime, DateTime.Now);
									}
									outParamList.MaxRecords = maxRecords;
									return outParamList;
								} finally {
									if (maxRecords == 1) {
										outParamList.CloseDataReader();
									}
								}
							}
						}
					} catch (Exception e) {
						throw new DataBaseException("Error executing query.", e);
					}
				}
			}



		}


	}

}