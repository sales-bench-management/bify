﻿/* 
 This source code (the "Generated Software") is generated by the OutSystems Platform 
 and is licensed by OutSystems (http://www.outsystems.com) to You solely for testing and evaluation 
 purposes, unless You and OutSystems have executed a specific agreement covering the use terms and 
 conditions of the Generated Software, in which case such agreement shall apply. 
*/

using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Runtime.Serialization;
using System.Reflection;
using System.Text;
using System.Text.RegularExpressions;
using OutSystems.HubEdition.RuntimePlatform;
using OutSystems.HubEdition.RuntimePlatform.Log;
using OutSystems.HubEdition.RuntimePlatform.Db;
using OutSystems.Internal.Db;
using OutSystems.HubEdition.RuntimePlatform.Email;
using OutSystems.HubEdition.RuntimePlatform.Web;
using OutSystems.HubEdition.RuntimePlatform.NewRuntime;
using OutSystems.RuntimeCommon;
using OutSystems.RuntimeCommon.Caching;
using OutSystems.ObjectKeys;
using System.Resources;

namespace ssBIFY {

	public partial class Actions {
		public class lcvGetCounts: VarsBag {
			public int inParamUserId;
			public RLDateEventEventTypeRecordList queryResGetEventByUserId_outParamList = new RLDateEventEventTypeRecordList();
			public long queryResGetEventByUserId_outParamCount = 0L;

			public RLGiftTypeGiftsRecordList queryResGetGiftsByUserId_outParamList = new RLGiftTypeGiftsRecordList();
			public long queryResGetGiftsByUserId_outParamCount = 0L;

			public RLFundFundTypeRecordList queryResGetFundsByUserId_outParamList = new RLFundFundTypeRecordList();
			public long queryResGetFundsByUserId_outParamCount = 0L;

			public lcvGetCounts(int inParamUserId) {
				this.inParamUserId = inParamUserId;
			}
		}
		public class lcoGetCounts: VarsBag {
			public long outParamGiftCount = Convert.ToInt64(0);

			public long outParamFundCount = Convert.ToInt64(0);

			public long outParamEventCount = Convert.ToInt64(0);

			public lcoGetCounts() {
			}
		}
		/// <summary>
		/// Action <code>ActionGetCounts</code> that represents the Service Studio user action
		///  <code>GetCounts</code> <p> Description: </p>
		/// </summary>
		public static void ActionGetCounts(HeContext heContext, int inParamUserId, out long outParamGiftCount, out long outParamFundCount, out long outParamEventCount) {
			lcoGetCounts result = new lcoGetCounts();
			lcvGetCounts localVars = new lcvGetCounts(inParamUserId);
			if (heContext != null && heContext.RequestTracer != null) {
				heContext.RequestTracer.RegisterInternalCall("h7BpARe4Vk+FMdHLQKwDRQ", "GetCounts", "1WMl+O7_wkOQDYQl1Ck5zQ", "BIFY");
			}
			try {
				if (heContext != null) {
					heContext.RequestCancelation.ThrowIfCanceled();
				}
				// Query datasetGetEventByUserId
				if (heContext != null) {
					heContext.RequestCancelation.ThrowIfCanceled();
				}
				int datasetGetEventByUserId_maxRecords = 0;
				localVars.queryResGetEventByUserId_outParamList = FuncActionGetCounts.datasetGetEventByUserId(heContext, datasetGetEventByUserId_maxRecords, IterationMultiplicity.Never, out localVars.queryResGetEventByUserId_outParamCount, localVars.inParamUserId
				);

				// Query datasetGetFundsByUserId
				if (heContext != null) {
					heContext.RequestCancelation.ThrowIfCanceled();
				}
				int datasetGetFundsByUserId_maxRecords = 0;
				localVars.queryResGetFundsByUserId_outParamList = FuncActionGetCounts.datasetGetFundsByUserId(heContext, datasetGetFundsByUserId_maxRecords, IterationMultiplicity.Never, out localVars.queryResGetFundsByUserId_outParamCount, localVars.inParamUserId
				);

				// Query datasetGetGiftsByUserId
				if (heContext != null) {
					heContext.RequestCancelation.ThrowIfCanceled();
				}
				int datasetGetGiftsByUserId_maxRecords = 0;
				localVars.queryResGetGiftsByUserId_outParamList = FuncActionGetCounts.datasetGetGiftsByUserId(heContext, datasetGetGiftsByUserId_maxRecords, IterationMultiplicity.Never, out localVars.queryResGetGiftsByUserId_outParamCount, localVars.inParamUserId
				);

				// GiftCount = GetGiftsByUserId.Count
				result.outParamGiftCount = localVars.queryResGetGiftsByUserId_outParamCount;
				// FundCount = GetFundsByUserId.Count
				result.outParamFundCount = localVars.queryResGetFundsByUserId_outParamCount;
				// EventCount = GetEventByUserId.Count
				result.outParamEventCount = localVars.queryResGetEventByUserId_outParamCount;
			} // try

			finally {
				outParamGiftCount = result.outParamGiftCount;
				outParamFundCount = result.outParamFundCount;
				outParamEventCount = result.outParamEventCount;
			}
		}

		public static class FuncActionGetCounts {

			private static void datasetGetEventByUserIdReadDB(ref RCDateEventEventTypeRecord rec, IDataReader r) {
				int index = 0;
				rec.ssENEvent.Read(r, ref index);
				rec.ssENEventType.Read(r, ref index);
				rec.ssDateToUse = r.ReadDate(index++, "DateToUseEventEventTypeRecord.DateToUse", new DateTime(1900, 1, 1, 0, 0, 0));
			}
			/// <summary>
			/// Query Function "GetEventByUserId" of Action "GetCounts"
			/// </summary>
			public static RLDateEventEventTypeRecordList datasetGetEventByUserId(HeContext heContext, int maxRecords, IterationMultiplicity multiplicity, out long outParamCount, int qpinEvent_UserId) {
				// Query Iterations: Never {-unbound-}
				// Uses binary data: False
				if (multiplicity == IterationMultiplicity.Never) {
					maxRecords = 1;
				}
				bool useMainTransaction = (multiplicity != IterationMultiplicity.Single || (maxRecords > 0 && maxRecords <= 150));
				Transaction trans = useMainTransaction? DatabaseAccess.ForCurrentDatabase.GetRequestTransaction(): DatabaseAccess.ForCurrentDatabase.GetReadOnlyTransaction();
				using(var sqlCmd = trans.CreateCommand()) {
					using(var sqlCountCmd = trans.CreateCommand()) {
						string sql = "";
						string sqlCount = "";
						StringBuilder selectBuilder = new StringBuilder();
						StringBuilder fromBuilder = new StringBuilder();
						StringBuilder whereBuilder = new StringBuilder();
						StringBuilder orderByBuilder = new StringBuilder();
						StringBuilder groupByBuilder = new StringBuilder();
						StringBuilder havingBuilder = new StringBuilder();
						selectBuilder.Append("SELECT ");
						if (maxRecords > 0) {
							selectBuilder.Append("TOP (");
							selectBuilder.Append(maxRecords);
							selectBuilder.Append(") ");
						}
						selectBuilder.Append("NULL o0, NULL o1, NULL o2, NULL o3, NULL o4, NULL o5, NULL o6, NULL o7, NULL o8, NULL o9, NULL o10, (CASE WHEN ([ENEVENT].[RECURRING] = 0) THEN [ENEVENT].[EVENTDATE] ELSE (convert(datetime, convert(varchar(4), (datepart(year, (convert(datetime, substring(@qepCurrdate, 1, 10), 120))))) + '-' + convert(varchar(2), (datepart(month, [ENEVENT].[EVENTDATE]))) + '-' + convert(varchar(2), (datepart(day, [ENEVENT].[EVENTDATE]))) + ' 00:00:00', 120)) END) [DATETOUSE]");
						fromBuilder.Append(" FROM ({Event} [ENEVENT] Left JOIN {EventType} [ENEVENTTYPE] ON ([ENEVENT].[EVENTTYPEID] = [ENEVENTTYPE].[ID])) ");
						whereBuilder.Append(" WHERE ");
						sqlCmd.CreateParameterWithoutReplacements("@qepCurrdate", DbType.String, System.DateTime.Now.ToString("yyyy-MM-dd"));
						sqlCountCmd.CreateParameterWithoutReplacements("@qepCurrdate", DbType.String, System.DateTime.Now.ToString("yyyy-MM-dd"));
						sqlCmd.CreateParameterWithoutReplacements("@qepCurrdate", DbType.String, System.DateTime.Now.ToString("yyyy-MM-dd"));
						sqlCountCmd.CreateParameterWithoutReplacements("@qepCurrdate", DbType.String, System.DateTime.Now.ToString("yyyy-MM-dd"));
						if (qpinEvent_UserId != 0) {
							whereBuilder.Append("(([ENEVENT].[USERID] = @qpinEvent_UserId) AND ([ENEVENT].[USERID] IS NOT NULL))");
							sqlCmd.CreateParameterWithoutReplacements("@qpinEvent_UserId", DbType.Int32, qpinEvent_UserId);
							sqlCountCmd.CreateParameterWithoutReplacements("@qpinEvent_UserId", DbType.Int32, qpinEvent_UserId);
						} else {
							whereBuilder.Append("([ENEVENT].[USERID] IS NULL)");
						}
						whereBuilder.Append(" AND ((((datediff(day, (convert(datetime, substring(@qepCurrdate, 1, 10), 120)), [ENEVENT].[EVENTDATE])) >= 0) AND ([ENEVENT].[RECURRING] = 0)) OR (([ENEVENT].[RECURRING] = 1) AND ((datediff(day, (convert(datetime, substring(@qepCurrdate, 1, 10), 120)), (convert(datetime, convert(varchar(4), ((datepart(year, (convert(datetime, substring(@qepCurrdate, 1, 10), 120)))) + 1)) + '-' + convert(varchar(2), (datepart(month, [ENEVENT].[EVENTDATE]))) + '-' + convert(varchar(2), (datepart(day, [ENEVENT].[EVENTDATE]))) + ' 00:00:00', 120)))) >= 0)))");
						sqlCmd.CreateParameterWithoutReplacements("@qepCurrdate", DbType.String, System.DateTime.Now.ToString("yyyy-MM-dd"));
						sqlCountCmd.CreateParameterWithoutReplacements("@qepCurrdate", DbType.String, System.DateTime.Now.ToString("yyyy-MM-dd"));
						sqlCmd.CreateParameterWithoutReplacements("@qepCurrdate", DbType.String, System.DateTime.Now.ToString("yyyy-MM-dd"));
						sqlCountCmd.CreateParameterWithoutReplacements("@qepCurrdate", DbType.String, System.DateTime.Now.ToString("yyyy-MM-dd"));

						sql = selectBuilder.ToString() + fromBuilder.ToString() + whereBuilder.ToString() + groupByBuilder.ToString() + havingBuilder.ToString() + orderByBuilder.ToString();
						string advSql = sql;
						sql = AppUtils.Instance.ReplaceEntityReferences(heContext, advSql);
						sqlCount = "SELECT COUNT(1) " + fromBuilder.ToString() + whereBuilder.ToString() + groupByBuilder.ToString() + havingBuilder.ToString();
						sqlCount = AppUtils.Instance.ReplaceEntityReferences(heContext, sqlCount);
						sqlCmd.CommandText = sql;
						sqlCountCmd.CommandText = sqlCount;
						try {
							RLDateEventEventTypeRecordList outParamList = new RLDateEventEventTypeRecordList();
							if (multiplicity != IterationMultiplicity.Multiple) {
								outParamList.AlternateReadDBMethod = datasetGetEventByUserIdReadDB;
							}
							outParamList.Transaction = trans;
							BitArray[] opt = new BitArray[2];
							opt[0] = new BitArray(new bool[] {
								true, true, true, true, true, true, true});
							opt[1] = new BitArray(new bool[] {
								true, true, true, true});
							outParamList.AllOptimizedAttributes = opt;
							if (multiplicity == IterationMultiplicity.Multiple) {
								RLDateEventEventTypeRecordList _tmp = new RLDateEventEventTypeRecordList();
								_tmp.AlternateReadDBMethod = datasetGetEventByUserIdReadDB;
								_tmp.Transaction = trans;
								_tmp.AllOptimizedAttributes = opt;
								_tmp.MaxRecords = maxRecords;
								try {
									DateTime startTime = DateTime.Now;
									DatabaseAccess.ForCurrentDatabase.ExecuteQuery(sqlCmd, _tmp, "Query GetCounts.GetEventByUserId.List");
									int queryExecutionTime = Convert.ToInt32(DateTime.Now.Subtract(startTime).TotalMilliseconds
									);
									RequestTracer reqTracer = RuntimePlatformUtils.GetRequestTracer();
									if (reqTracer != null) {
										reqTracer.RegisterQueryExecuted("42347353-2484-4b15-9857-282f3328a492", "GetCounts.GetEventByUserId", "f82563d5-ffee-43c2-900d-8425d42939cd", "BIFY", queryExecutionTime, DateTime.Now);
									}
									outParamList = (RLDateEventEventTypeRecordList) _tmp.Duplicate();
									if (maxRecords > 0 && outParamList.Length == maxRecords)
									outParamCount = Convert.ToInt64(sqlCountCmd.ExecuteScalarWithoutTransformParametersSyntax("Query GetCounts.GetEventByUserId.Count", false).ToString());
									else
									outParamCount = outParamList.Length;
									return outParamList;
								} finally {
									_tmp.CloseDataReader();
								}
							} else if (multiplicity == IterationMultiplicity.Never) {
								try {
									outParamCount = Convert.ToInt64(sqlCountCmd.ExecuteScalarWithoutTransformParametersSyntax("Query GetCounts.GetEventByUserId.Count", false).ToString());
									DateTime startTime = DateTime.Now;
									DatabaseAccess.ForCurrentDatabase.ExecuteQuery(sqlCmd, outParamList, "Query GetCounts.GetEventByUserId.List");
									int queryExecutionTime = Convert.ToInt32(DateTime.Now.Subtract(startTime).TotalMilliseconds
									);
									RequestTracer reqTracer = RuntimePlatformUtils.GetRequestTracer();
									if (reqTracer != null) {
										reqTracer.RegisterQueryExecuted("42347353-2484-4b15-9857-282f3328a492", "GetCounts.GetEventByUserId", "f82563d5-ffee-43c2-900d-8425d42939cd", "BIFY", queryExecutionTime, DateTime.Now);
									}
									outParamList.MaxRecords = maxRecords;
									return outParamList;
								} finally {
									outParamList.CloseDataReader();
								}
							} else {
								if (maxRecords > 0 && maxRecords <= 150) {
									RLDateEventEventTypeRecordList _tmp = new RLDateEventEventTypeRecordList();
									_tmp.AlternateReadDBMethod = datasetGetEventByUserIdReadDB;
									_tmp.Transaction = trans;
									_tmp.AllOptimizedAttributes = opt;
									try {
										DateTime startTime = DateTime.Now;
										DatabaseAccess.ForCurrentDatabase.ExecuteQuery(sqlCmd, _tmp, "Query GetCounts.GetEventByUserId.List");
										int queryExecutionTime = Convert.ToInt32(DateTime.Now.Subtract(startTime).TotalMilliseconds
										);
										RequestTracer reqTracer = RuntimePlatformUtils.GetRequestTracer();
										if (reqTracer != null) {
											reqTracer.RegisterQueryExecuted("42347353-2484-4b15-9857-282f3328a492", "GetCounts.GetEventByUserId", "f82563d5-ffee-43c2-900d-8425d42939cd", "BIFY", queryExecutionTime, DateTime.Now);
										}
										outParamList = (RLDateEventEventTypeRecordList) _tmp.Duplicate();
										outParamList.AllOptimizedAttributes = opt;
										if (maxRecords > 0 && outParamList.Length == maxRecords)
										outParamCount = Convert.ToInt64(sqlCountCmd.ExecuteScalarWithoutTransformParametersSyntax("Query GetCounts.GetEventByUserId.Count", false).ToString());
										else
										outParamCount = outParamList.Length;
										return outParamList;
									} finally {
										_tmp.CloseDataReader();
									}
								} else {
									try {
										outParamCount = Convert.ToInt64(sqlCountCmd.ExecuteScalarWithoutTransformParametersSyntax("Query GetCounts.GetEventByUserId.Count", false).ToString());
										DateTime startTime = DateTime.Now;
										DatabaseAccess.ForCurrentDatabase.ExecuteQuery(sqlCmd, outParamList, "Query GetCounts.GetEventByUserId.List");
										int queryExecutionTime = Convert.ToInt32(DateTime.Now.Subtract(startTime).TotalMilliseconds
										);
										RequestTracer reqTracer = RuntimePlatformUtils.GetRequestTracer();
										if (reqTracer != null) {
											reqTracer.RegisterQueryExecuted("42347353-2484-4b15-9857-282f3328a492", "GetCounts.GetEventByUserId", "f82563d5-ffee-43c2-900d-8425d42939cd", "BIFY", queryExecutionTime, DateTime.Now);
										}
										outParamList.MaxRecords = maxRecords;
										return outParamList;
									} finally {
										if (maxRecords == 1) {
											outParamList.CloseDataReader();
										}
									}
								}
							}
						} catch (Exception e) {
							throw new DataBaseException("Error executing query.", e);
						}
					}
				}
			}

			private static void datasetGetGiftsByUserIdReadDB(ref RCGiftTypeGiftsRecord rec, IDataReader r) {
				int index = 0;
				rec.ssENGifts.Read(r, ref index);
				rec.ssENGiftType.Read(r, ref index);
			}
			/// <summary>
			/// Query Function "GetGiftsByUserId" of Action "GetCounts"
			/// </summary>
			public static RLGiftTypeGiftsRecordList datasetGetGiftsByUserId(HeContext heContext, int maxRecords, IterationMultiplicity multiplicity, out long outParamCount, int qpinGifts_UserId) {
				// Query Iterations: Never {-unbound-}
				// Uses binary data: False
				if (multiplicity == IterationMultiplicity.Never) {
					maxRecords = 1;
				}
				bool useMainTransaction = (multiplicity != IterationMultiplicity.Single || (maxRecords > 0 && maxRecords <= 150));
				Transaction trans = useMainTransaction? DatabaseAccess.ForCurrentDatabase.GetRequestTransaction(): DatabaseAccess.ForCurrentDatabase.GetReadOnlyTransaction();
				using(var sqlCmd = trans.CreateCommand()) {
					using(var sqlCountCmd = trans.CreateCommand()) {
						string sql = "";
						string sqlCount = "";
						StringBuilder selectBuilder = new StringBuilder();
						StringBuilder fromBuilder = new StringBuilder();
						StringBuilder whereBuilder = new StringBuilder();
						StringBuilder orderByBuilder = new StringBuilder();
						StringBuilder groupByBuilder = new StringBuilder();
						StringBuilder havingBuilder = new StringBuilder();
						selectBuilder.Append("SELECT ");
						if (maxRecords > 0) {
							selectBuilder.Append("TOP (");
							selectBuilder.Append(maxRecords);
							selectBuilder.Append(") ");
						}
						selectBuilder.Append("NULL o0, NULL o1, NULL o2, NULL o3, NULL o4, NULL o5, NULL o6, NULL o7, NULL o8, NULL o9, NULL o10, NULL o11, NULL o12, NULL o13, NULL o14, NULL o15, NULL o16, NULL o17, NULL o18, NULL o19, NULL o20, NULL o21");
						fromBuilder.Append(" FROM ({Gifts} [ENGIFTS] Left JOIN {GiftType} [ENGIFTTYPE] ON ([ENGIFTS].[GIFTTYPEID] = [ENGIFTTYPE].[ID])) ");
						whereBuilder.Append(" WHERE ");
						if (qpinGifts_UserId != 0) {
							whereBuilder.Append("(([ENGIFTS].[USERID] = @qpinGifts_UserId) AND ([ENGIFTS].[USERID] IS NOT NULL))");
							sqlCmd.CreateParameterWithoutReplacements("@qpinGifts_UserId", DbType.Int32, qpinGifts_UserId);
							sqlCountCmd.CreateParameterWithoutReplacements("@qpinGifts_UserId", DbType.Int32, qpinGifts_UserId);
						} else {
							whereBuilder.Append("([ENGIFTS].[USERID] IS NULL)");
						}

						sql = selectBuilder.ToString() + fromBuilder.ToString() + whereBuilder.ToString() + groupByBuilder.ToString() + havingBuilder.ToString() + orderByBuilder.ToString();
						string advSql = sql;
						sql = AppUtils.Instance.ReplaceEntityReferences(heContext, advSql);
						sqlCount = "SELECT COUNT(1) " + fromBuilder.ToString() + whereBuilder.ToString() + groupByBuilder.ToString() + havingBuilder.ToString();
						sqlCount = AppUtils.Instance.ReplaceEntityReferences(heContext, sqlCount);
						sqlCmd.CommandText = sql;
						sqlCountCmd.CommandText = sqlCount;
						try {
							RLGiftTypeGiftsRecordList outParamList = new RLGiftTypeGiftsRecordList();
							if (multiplicity != IterationMultiplicity.Multiple) {
								outParamList.AlternateReadDBMethod = datasetGetGiftsByUserIdReadDB;
							}
							outParamList.Transaction = trans;
							BitArray[] opt = new BitArray[2];
							opt[0] = new BitArray(new bool[] {
								true, true, true, true});
							opt[1] = new BitArray(new bool[] {
								true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true});
							outParamList.AllOptimizedAttributes = opt;
							if (multiplicity == IterationMultiplicity.Multiple) {
								RLGiftTypeGiftsRecordList _tmp = new RLGiftTypeGiftsRecordList();
								_tmp.AlternateReadDBMethod = datasetGetGiftsByUserIdReadDB;
								_tmp.Transaction = trans;
								_tmp.AllOptimizedAttributes = opt;
								_tmp.MaxRecords = maxRecords;
								try {
									DateTime startTime = DateTime.Now;
									DatabaseAccess.ForCurrentDatabase.ExecuteQuery(sqlCmd, _tmp, "Query GetCounts.GetGiftsByUserId.List");
									int queryExecutionTime = Convert.ToInt32(DateTime.Now.Subtract(startTime).TotalMilliseconds
									);
									RequestTracer reqTracer = RuntimePlatformUtils.GetRequestTracer();
									if (reqTracer != null) {
										reqTracer.RegisterQueryExecuted("7ca5a320-3a79-4c4f-8ce5-1cdab495a02a", "GetCounts.GetGiftsByUserId", "f82563d5-ffee-43c2-900d-8425d42939cd", "BIFY", queryExecutionTime, DateTime.Now);
									}
									outParamList = (RLGiftTypeGiftsRecordList) _tmp.Duplicate();
									if (maxRecords > 0 && outParamList.Length == maxRecords)
									outParamCount = Convert.ToInt64(sqlCountCmd.ExecuteScalarWithoutTransformParametersSyntax("Query GetCounts.GetGiftsByUserId.Count", false).ToString());
									else
									outParamCount = outParamList.Length;
									return outParamList;
								} finally {
									_tmp.CloseDataReader();
								}
							} else if (multiplicity == IterationMultiplicity.Never) {
								try {
									outParamCount = Convert.ToInt64(sqlCountCmd.ExecuteScalarWithoutTransformParametersSyntax("Query GetCounts.GetGiftsByUserId.Count", false).ToString());
									DateTime startTime = DateTime.Now;
									DatabaseAccess.ForCurrentDatabase.ExecuteQuery(sqlCmd, outParamList, "Query GetCounts.GetGiftsByUserId.List");
									int queryExecutionTime = Convert.ToInt32(DateTime.Now.Subtract(startTime).TotalMilliseconds
									);
									RequestTracer reqTracer = RuntimePlatformUtils.GetRequestTracer();
									if (reqTracer != null) {
										reqTracer.RegisterQueryExecuted("7ca5a320-3a79-4c4f-8ce5-1cdab495a02a", "GetCounts.GetGiftsByUserId", "f82563d5-ffee-43c2-900d-8425d42939cd", "BIFY", queryExecutionTime, DateTime.Now);
									}
									outParamList.MaxRecords = maxRecords;
									return outParamList;
								} finally {
									outParamList.CloseDataReader();
								}
							} else {
								if (maxRecords > 0 && maxRecords <= 150) {
									RLGiftTypeGiftsRecordList _tmp = new RLGiftTypeGiftsRecordList();
									_tmp.AlternateReadDBMethod = datasetGetGiftsByUserIdReadDB;
									_tmp.Transaction = trans;
									_tmp.AllOptimizedAttributes = opt;
									try {
										DateTime startTime = DateTime.Now;
										DatabaseAccess.ForCurrentDatabase.ExecuteQuery(sqlCmd, _tmp, "Query GetCounts.GetGiftsByUserId.List");
										int queryExecutionTime = Convert.ToInt32(DateTime.Now.Subtract(startTime).TotalMilliseconds
										);
										RequestTracer reqTracer = RuntimePlatformUtils.GetRequestTracer();
										if (reqTracer != null) {
											reqTracer.RegisterQueryExecuted("7ca5a320-3a79-4c4f-8ce5-1cdab495a02a", "GetCounts.GetGiftsByUserId", "f82563d5-ffee-43c2-900d-8425d42939cd", "BIFY", queryExecutionTime, DateTime.Now);
										}
										outParamList = (RLGiftTypeGiftsRecordList) _tmp.Duplicate();
										outParamList.AllOptimizedAttributes = opt;
										if (maxRecords > 0 && outParamList.Length == maxRecords)
										outParamCount = Convert.ToInt64(sqlCountCmd.ExecuteScalarWithoutTransformParametersSyntax("Query GetCounts.GetGiftsByUserId.Count", false).ToString());
										else
										outParamCount = outParamList.Length;
										return outParamList;
									} finally {
										_tmp.CloseDataReader();
									}
								} else {
									try {
										outParamCount = Convert.ToInt64(sqlCountCmd.ExecuteScalarWithoutTransformParametersSyntax("Query GetCounts.GetGiftsByUserId.Count", false).ToString());
										DateTime startTime = DateTime.Now;
										DatabaseAccess.ForCurrentDatabase.ExecuteQuery(sqlCmd, outParamList, "Query GetCounts.GetGiftsByUserId.List");
										int queryExecutionTime = Convert.ToInt32(DateTime.Now.Subtract(startTime).TotalMilliseconds
										);
										RequestTracer reqTracer = RuntimePlatformUtils.GetRequestTracer();
										if (reqTracer != null) {
											reqTracer.RegisterQueryExecuted("7ca5a320-3a79-4c4f-8ce5-1cdab495a02a", "GetCounts.GetGiftsByUserId", "f82563d5-ffee-43c2-900d-8425d42939cd", "BIFY", queryExecutionTime, DateTime.Now);
										}
										outParamList.MaxRecords = maxRecords;
										return outParamList;
									} finally {
										if (maxRecords == 1) {
											outParamList.CloseDataReader();
										}
									}
								}
							}
						} catch (Exception e) {
							throw new DataBaseException("Error executing query.", e);
						}
					}
				}
			}

			private static void datasetGetFundsByUserIdReadDB(ref RCFundFundTypeRecord rec, IDataReader r) {
				int index = 0;
				rec.ssENFund.Read(r, ref index);
				rec.ssENFundType.Read(r, ref index);
			}
			/// <summary>
			/// Query Function "GetFundsByUserId" of Action "GetCounts"
			/// </summary>
			public static RLFundFundTypeRecordList datasetGetFundsByUserId(HeContext heContext, int maxRecords, IterationMultiplicity multiplicity, out long outParamCount, int qpinFund_UserId) {
				// Query Iterations: Never {-unbound-}
				// Uses binary data: False
				if (multiplicity == IterationMultiplicity.Never) {
					maxRecords = 1;
				}
				bool useMainTransaction = (multiplicity != IterationMultiplicity.Single || (maxRecords > 0 && maxRecords <= 150));
				Transaction trans = useMainTransaction? DatabaseAccess.ForCurrentDatabase.GetRequestTransaction(): DatabaseAccess.ForCurrentDatabase.GetReadOnlyTransaction();
				using(var sqlCmd = trans.CreateCommand()) {
					using(var sqlCountCmd = trans.CreateCommand()) {
						string sql = "";
						string sqlCount = "";
						StringBuilder selectBuilder = new StringBuilder();
						StringBuilder fromBuilder = new StringBuilder();
						StringBuilder whereBuilder = new StringBuilder();
						StringBuilder orderByBuilder = new StringBuilder();
						StringBuilder groupByBuilder = new StringBuilder();
						StringBuilder havingBuilder = new StringBuilder();
						selectBuilder.Append("SELECT ");
						if (maxRecords > 0) {
							selectBuilder.Append("TOP (");
							selectBuilder.Append(maxRecords);
							selectBuilder.Append(") ");
						}
						selectBuilder.Append("NULL o0, NULL o1, NULL o2, NULL o3, NULL o4, NULL o5, NULL o6, NULL o7, NULL o8, NULL o9, NULL o10, NULL o11, NULL o12, NULL o13, NULL o14, NULL o15, NULL o16");
						fromBuilder.Append(" FROM ({Fund} [ENFUND] Left JOIN {FundType} [ENFUNDTYPE] ON ([ENFUND].[FUNDTYPEID] = [ENFUNDTYPE].[ID])) ");
						whereBuilder.Append(" WHERE ");
						if (qpinFund_UserId != 0) {
							whereBuilder.Append("(([ENFUND].[USERID] = @qpinFund_UserId) AND ([ENFUND].[USERID] IS NOT NULL))");
							sqlCmd.CreateParameterWithoutReplacements("@qpinFund_UserId", DbType.Int32, qpinFund_UserId);
							sqlCountCmd.CreateParameterWithoutReplacements("@qpinFund_UserId", DbType.Int32, qpinFund_UserId);
						} else {
							whereBuilder.Append("([ENFUND].[USERID] IS NULL)");
						}

						sql = selectBuilder.ToString() + fromBuilder.ToString() + whereBuilder.ToString() + groupByBuilder.ToString() + havingBuilder.ToString() + orderByBuilder.ToString();
						string advSql = sql;
						sql = AppUtils.Instance.ReplaceEntityReferences(heContext, advSql);
						sqlCount = "SELECT COUNT(1) " + fromBuilder.ToString() + whereBuilder.ToString() + groupByBuilder.ToString() + havingBuilder.ToString();
						sqlCount = AppUtils.Instance.ReplaceEntityReferences(heContext, sqlCount);
						sqlCmd.CommandText = sql;
						sqlCountCmd.CommandText = sqlCount;
						try {
							RLFundFundTypeRecordList outParamList = new RLFundFundTypeRecordList();
							if (multiplicity != IterationMultiplicity.Multiple) {
								outParamList.AlternateReadDBMethod = datasetGetFundsByUserIdReadDB;
							}
							outParamList.Transaction = trans;
							BitArray[] opt = new BitArray[2];
							opt[0] = new BitArray(new bool[] {
								true, true, true, true, true, true, true, true, true, true, true, true, true});
							opt[1] = new BitArray(new bool[] {
								true, true, true, true});
							outParamList.AllOptimizedAttributes = opt;
							if (multiplicity == IterationMultiplicity.Multiple) {
								RLFundFundTypeRecordList _tmp = new RLFundFundTypeRecordList();
								_tmp.AlternateReadDBMethod = datasetGetFundsByUserIdReadDB;
								_tmp.Transaction = trans;
								_tmp.AllOptimizedAttributes = opt;
								_tmp.MaxRecords = maxRecords;
								try {
									DateTime startTime = DateTime.Now;
									DatabaseAccess.ForCurrentDatabase.ExecuteQuery(sqlCmd, _tmp, "Query GetCounts.GetFundsByUserId.List");
									int queryExecutionTime = Convert.ToInt32(DateTime.Now.Subtract(startTime).TotalMilliseconds
									);
									RequestTracer reqTracer = RuntimePlatformUtils.GetRequestTracer();
									if (reqTracer != null) {
										reqTracer.RegisterQueryExecuted("b3374749-75de-4fdc-9697-e3fa8e4343e6", "GetCounts.GetFundsByUserId", "f82563d5-ffee-43c2-900d-8425d42939cd", "BIFY", queryExecutionTime, DateTime.Now);
									}
									outParamList = (RLFundFundTypeRecordList) _tmp.Duplicate();
									if (maxRecords > 0 && outParamList.Length == maxRecords)
									outParamCount = Convert.ToInt64(sqlCountCmd.ExecuteScalarWithoutTransformParametersSyntax("Query GetCounts.GetFundsByUserId.Count", false).ToString());
									else
									outParamCount = outParamList.Length;
									return outParamList;
								} finally {
									_tmp.CloseDataReader();
								}
							} else if (multiplicity == IterationMultiplicity.Never) {
								try {
									outParamCount = Convert.ToInt64(sqlCountCmd.ExecuteScalarWithoutTransformParametersSyntax("Query GetCounts.GetFundsByUserId.Count", false).ToString());
									DateTime startTime = DateTime.Now;
									DatabaseAccess.ForCurrentDatabase.ExecuteQuery(sqlCmd, outParamList, "Query GetCounts.GetFundsByUserId.List");
									int queryExecutionTime = Convert.ToInt32(DateTime.Now.Subtract(startTime).TotalMilliseconds
									);
									RequestTracer reqTracer = RuntimePlatformUtils.GetRequestTracer();
									if (reqTracer != null) {
										reqTracer.RegisterQueryExecuted("b3374749-75de-4fdc-9697-e3fa8e4343e6", "GetCounts.GetFundsByUserId", "f82563d5-ffee-43c2-900d-8425d42939cd", "BIFY", queryExecutionTime, DateTime.Now);
									}
									outParamList.MaxRecords = maxRecords;
									return outParamList;
								} finally {
									outParamList.CloseDataReader();
								}
							} else {
								if (maxRecords > 0 && maxRecords <= 150) {
									RLFundFundTypeRecordList _tmp = new RLFundFundTypeRecordList();
									_tmp.AlternateReadDBMethod = datasetGetFundsByUserIdReadDB;
									_tmp.Transaction = trans;
									_tmp.AllOptimizedAttributes = opt;
									try {
										DateTime startTime = DateTime.Now;
										DatabaseAccess.ForCurrentDatabase.ExecuteQuery(sqlCmd, _tmp, "Query GetCounts.GetFundsByUserId.List");
										int queryExecutionTime = Convert.ToInt32(DateTime.Now.Subtract(startTime).TotalMilliseconds
										);
										RequestTracer reqTracer = RuntimePlatformUtils.GetRequestTracer();
										if (reqTracer != null) {
											reqTracer.RegisterQueryExecuted("b3374749-75de-4fdc-9697-e3fa8e4343e6", "GetCounts.GetFundsByUserId", "f82563d5-ffee-43c2-900d-8425d42939cd", "BIFY", queryExecutionTime, DateTime.Now);
										}
										outParamList = (RLFundFundTypeRecordList) _tmp.Duplicate();
										outParamList.AllOptimizedAttributes = opt;
										if (maxRecords > 0 && outParamList.Length == maxRecords)
										outParamCount = Convert.ToInt64(sqlCountCmd.ExecuteScalarWithoutTransformParametersSyntax("Query GetCounts.GetFundsByUserId.Count", false).ToString());
										else
										outParamCount = outParamList.Length;
										return outParamList;
									} finally {
										_tmp.CloseDataReader();
									}
								} else {
									try {
										outParamCount = Convert.ToInt64(sqlCountCmd.ExecuteScalarWithoutTransformParametersSyntax("Query GetCounts.GetFundsByUserId.Count", false).ToString());
										DateTime startTime = DateTime.Now;
										DatabaseAccess.ForCurrentDatabase.ExecuteQuery(sqlCmd, outParamList, "Query GetCounts.GetFundsByUserId.List");
										int queryExecutionTime = Convert.ToInt32(DateTime.Now.Subtract(startTime).TotalMilliseconds
										);
										RequestTracer reqTracer = RuntimePlatformUtils.GetRequestTracer();
										if (reqTracer != null) {
											reqTracer.RegisterQueryExecuted("b3374749-75de-4fdc-9697-e3fa8e4343e6", "GetCounts.GetFundsByUserId", "f82563d5-ffee-43c2-900d-8425d42939cd", "BIFY", queryExecutionTime, DateTime.Now);
										}
										outParamList.MaxRecords = maxRecords;
										return outParamList;
									} finally {
										if (maxRecords == 1) {
											outParamList.CloseDataReader();
										}
									}
								}
							}
						} catch (Exception e) {
							throw new DataBaseException("Error executing query.", e);
						}
					}
				}
			}



		}


	}

}