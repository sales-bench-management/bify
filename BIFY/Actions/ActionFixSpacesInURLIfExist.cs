﻿/* 
 This source code (the "Generated Software") is generated by the OutSystems Platform 
 and is licensed by OutSystems (http://www.outsystems.com) to You solely for testing and evaluation 
 purposes, unless You and OutSystems have executed a specific agreement covering the use terms and 
 conditions of the Generated Software, in which case such agreement shall apply. 
*/

using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Runtime.Serialization;
using System.Reflection;
using System.Text;
using System.Text.RegularExpressions;
using OutSystems.HubEdition.RuntimePlatform;
using OutSystems.HubEdition.RuntimePlatform.Log;
using OutSystems.HubEdition.RuntimePlatform.Db;
using OutSystems.Internal.Db;
using OutSystems.HubEdition.RuntimePlatform.Email;
using OutSystems.HubEdition.RuntimePlatform.Web;
using OutSystems.HubEdition.RuntimePlatform.NewRuntime;
using OutSystems.RuntimeCommon;
using OutSystems.RuntimeCommon.Caching;
using OutSystems.ObjectKeys;
using System.Resources;

namespace ssBIFY {

	public partial class Actions {
		public class lcvFixSpacesInURLIfExist: VarsBag {
			public string inParamImageURL;
			public lcvFixSpacesInURLIfExist(string inParamImageURL) {
				this.inParamImageURL = inParamImageURL;
			}
		}
		public class lcoFixSpacesInURLIfExist: VarsBag {
			public string outParamFixedURL = "";

			public lcoFixSpacesInURLIfExist() {
			}
		}
		/// <summary>
		/// Action <code>ActionFixSpacesInURLIfExist</code> that represents the Service Studio user action
		///  <code>FixSpacesInURLIfExist</code> <p> Description: </p>
		/// </summary>
		public static void ActionFixSpacesInURLIfExist(HeContext heContext, string inParamImageURL, out string outParamFixedURL) {
			lcoFixSpacesInURLIfExist result = new lcoFixSpacesInURLIfExist();
			lcvFixSpacesInURLIfExist localVars = new lcvFixSpacesInURLIfExist(inParamImageURL);
			if (heContext != null && heContext.RequestTracer != null) {
				heContext.RequestTracer.RegisterInternalCall("aD91rlarRkibRJhEV2kprg", "FixSpacesInURLIfExist", "1WMl+O7_wkOQDYQl1Ck5zQ", "BIFY");
			}
			try {
				if (heContext != null) {
					heContext.RequestCancelation.ThrowIfCanceled();
				}
				// HasSpace
				if (((BuiltInFunction.IndexSC(" ", localVars.inParamImageURL, 0, false, false) !=0))) {
					// FixedURL = Replace
					result.outParamFixedURL = BuiltInFunction.Replace(localVars.inParamImageURL, " ", "%20");
				} else {
					// FixedURL = ImageURL
					result.outParamFixedURL = localVars.inParamImageURL;
				}

			} // try

			finally {
				outParamFixedURL = result.outParamFixedURL;
			}
		}

		public static class FuncActionFixSpacesInURLIfExist {



		}


	}

}