﻿/* 
 This source code (the "Generated Software") is generated by the OutSystems Platform 
 and is licensed by OutSystems (http://www.outsystems.com) to You solely for testing and evaluation 
 purposes, unless You and OutSystems have executed a specific agreement covering the use terms and 
 conditions of the Generated Software, in which case such agreement shall apply. 
*/

using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Runtime.Serialization;
using System.Reflection;
using System.Text;
using System.Text.RegularExpressions;
using OutSystems.HubEdition.RuntimePlatform;
using OutSystems.HubEdition.RuntimePlatform.Log;
using OutSystems.HubEdition.RuntimePlatform.Db;
using OutSystems.Internal.Db;
using OutSystems.HubEdition.RuntimePlatform.Email;
using OutSystems.HubEdition.RuntimePlatform.Web;
using OutSystems.HubEdition.RuntimePlatform.NewRuntime;
using OutSystems.RuntimeCommon;
using OutSystems.RuntimeCommon.Caching;
using OutSystems.ObjectKeys;
using System.Resources;

namespace ssBIFY {

	public partial class Actions {
		public class lcvDodgySiteImageNumber: VarsBag {
			public string inParamURL;
			public RLTextRecordList queryResGetmageIdToUse_outParamList = new RLTextRecordList();
			public long queryResGetmageIdToUse_outParamCount = 0L;

			public RLRetailerFixesRecordList queryResGetRetailerFixes_outParamList = new RLRetailerFixesRecordList();
			public long queryResGetRetailerFixes_outParamCount = 0L;

			public lcvDodgySiteImageNumber(string inParamURL) {
				this.inParamURL = inParamURL;
			}
		}
		public class lcoDodgySiteImageNumber: VarsBag {
			public int outParamImageIdToUse = 0;

			public bool outParamLoopImages = false;

			public bool outParamIncludeHybridProduct = false;

			public bool outParamIncludeInferProduct = false;

			public bool outParamIncludeInfer = false;

			public int outParamImageThreshold = 0;

			public bool outParamIndexRelatesToProductImages = true;

			public bool outParamJPG = false;

			public bool outParamPNG = false;

			public bool outParamSVG = false;

			public string outParamImageURLTemplate = "";

			public string outParamImageURLSQL = "";

			public bool outParamUseResize = true;

			public lcoDodgySiteImageNumber() {
			}
		}
		/// <summary>
		/// Action <code>ActionDodgySiteImageNumber</code> that represents the Service Studio user action
		///  <code>DodgySiteImageNumber</code> <p> Description: Some sites seem to return rubbish images - w
		/// e can override the returned image</p>
		/// </summary>
		public static void ActionDodgySiteImageNumber(HeContext heContext, string inParamURL, out int outParamImageIdToUse, out bool outParamLoopImages, out bool outParamIncludeHybridProduct, out bool outParamIncludeInferProduct, out bool outParamIncludeInfer, out int outParamImageThreshold, out bool outParamIndexRelatesToProductImages, out bool outParamJPG, out bool outParamPNG, out bool outParamSVG, out string outParamImageURLTemplate, out string outParamImageURLSQL, out bool outParamUseResize) {
			lcoDodgySiteImageNumber result = new lcoDodgySiteImageNumber();
			lcvDodgySiteImageNumber localVars = new lcvDodgySiteImageNumber(inParamURL);
			if (heContext != null && heContext.RequestTracer != null) {
				heContext.RequestTracer.RegisterInternalCall("u5ddJbu1JUOnj0CmRj_c7A", "DodgySiteImageNumber", "1WMl+O7_wkOQDYQl1Ck5zQ", "BIFY");
			}
			try {
				if (heContext != null) {
					heContext.RequestCancelation.ThrowIfCanceled();
				}
				// ImageIdToUse = 0
				result.outParamImageIdToUse = 0;
				// Query datasetGetRetailerFixes
				if (heContext != null) {
					heContext.RequestCancelation.ThrowIfCanceled();
				}
				int datasetGetRetailerFixes_maxRecords = 0;
				localVars.queryResGetRetailerFixes_outParamList = FuncActionDodgySiteImageNumber.datasetGetRetailerFixes(heContext, datasetGetRetailerFixes_maxRecords, IterationMultiplicity.Single, out localVars.queryResGetRetailerFixes_outParamCount);

				// Foreach GetRetailerFixes.List
				localVars.queryResGetRetailerFixes_outParamList.StartIteration();
				try {
					while (!((localVars.queryResGetRetailerFixes_outParamList.Eof))) {
						// Query QueryGetmageIdToUse
						if (heContext != null) {
							heContext.RequestCancelation.ThrowIfCanceled();
						}
						int QueryGetmageIdToUse_maxRecords = 0;
						localVars.queryResGetmageIdToUse_outParamList = FuncActionDodgySiteImageNumber.QueryGetmageIdToUse(heContext, QueryGetmageIdToUse_maxRecords, IterationMultiplicity.Never, out localVars.queryResGetmageIdToUse_outParamCount, localVars.inParamURL
						, localVars.queryResGetRetailerFixes_outParamList.CurrentRec.ssENRetailerFixes.ssDomain
						, Convert.ToString(localVars.queryResGetRetailerFixes_outParamList.CurrentRec.ssENRetailerFixes.ssImageIndex)
						);

						// OnTheDodgyList
						if (((localVars.queryResGetmageIdToUse_outParamList.CurrentRec.ssSTText.ssValue!=Convert.ToString(0)))) {
							// ImageIdToUse = TextToInteger
							result.outParamImageIdToUse = BuiltInFunction.TextToInteger(localVars.queryResGetmageIdToUse_outParamList.CurrentRec.ssSTText.ssValue);
							// LoopImages = GetRetailerFixes.List.Current.RetailerFixes.LoopImages
							result.outParamLoopImages = localVars.queryResGetRetailerFixes_outParamList.CurrentRec.ssENRetailerFixes.ssLoopImages;
							// IncludeHybridProduct = GetRetailerFixes.List.Current.RetailerFixes.IncludeHybridProduct
							result.outParamIncludeHybridProduct = localVars.queryResGetRetailerFixes_outParamList.CurrentRec.ssENRetailerFixes.ssIncludeHybridProduct;
							// IncludeInferProduct = GetRetailerFixes.List.Current.RetailerFixes.IncludeInferProduct
							result.outParamIncludeInferProduct = localVars.queryResGetRetailerFixes_outParamList.CurrentRec.ssENRetailerFixes.ssIncludeInferProduct;
							// IncludeInfer = GetRetailerFixes.List.Current.RetailerFixes.IncludeInfer
							result.outParamIncludeInfer = localVars.queryResGetRetailerFixes_outParamList.CurrentRec.ssENRetailerFixes.ssIncludeInfer;
							// ImageThreshold = GetRetailerFixes.List.Current.RetailerFixes.MinImgWidthHeight
							result.outParamImageThreshold = localVars.queryResGetRetailerFixes_outParamList.CurrentRec.ssENRetailerFixes.ssMinImgWidthHeight;
							// IndexRelatesToProductImages = GetRetailerFixes.List.Current.RetailerFixes.IndexRelatesToProductImages
							result.outParamIndexRelatesToProductImages = localVars.queryResGetRetailerFixes_outParamList.CurrentRec.ssENRetailerFixes.ssIndexRelatesToProductImages;
							// JPG = GetRetailerFixes.List.Current.RetailerFixes.AllowJPG
							result.outParamJPG = localVars.queryResGetRetailerFixes_outParamList.CurrentRec.ssENRetailerFixes.ssAllowJPG;
							// PNG = GetRetailerFixes.List.Current.RetailerFixes.AllowPNG
							result.outParamPNG = localVars.queryResGetRetailerFixes_outParamList.CurrentRec.ssENRetailerFixes.ssAllowPNG;
							// SVG = GetRetailerFixes.List.Current.RetailerFixes.AllowSVG
							result.outParamSVG = localVars.queryResGetRetailerFixes_outParamList.CurrentRec.ssENRetailerFixes.ssAllowSVG;
							// ImageURLTemplate = GetRetailerFixes.List.Current.RetailerFixes.ImageURLTemplate
							result.outParamImageURLTemplate = localVars.queryResGetRetailerFixes_outParamList.CurrentRec.ssENRetailerFixes.ssImageURLTemplate;
							// ImageURLSQL = GetRetailerFixes.List.Current.RetailerFixes.SQLCodeToGetID
							result.outParamImageURLSQL = localVars.queryResGetRetailerFixes_outParamList.CurrentRec.ssENRetailerFixes.ssSQLCodeToGetID;
							// UseResize = GetRetailerFixes.List.Current.RetailerFixes.FilterSizes
							result.outParamUseResize = localVars.queryResGetRetailerFixes_outParamList.CurrentRec.ssENRetailerFixes.ssFilterSizes;
						}

						localVars.queryResGetRetailerFixes_outParamList.Advance();
					}

				} finally {
					localVars.queryResGetRetailerFixes_outParamList.EndIteration();
				}

			} // try

			finally {
				outParamImageIdToUse = result.outParamImageIdToUse;
				outParamLoopImages = result.outParamLoopImages;
				outParamIncludeHybridProduct = result.outParamIncludeHybridProduct;
				outParamIncludeInferProduct = result.outParamIncludeInferProduct;
				outParamIncludeInfer = result.outParamIncludeInfer;
				outParamImageThreshold = result.outParamImageThreshold;
				outParamIndexRelatesToProductImages = result.outParamIndexRelatesToProductImages;
				outParamJPG = result.outParamJPG;
				outParamPNG = result.outParamPNG;
				outParamSVG = result.outParamSVG;
				outParamImageURLTemplate = result.outParamImageURLTemplate;
				outParamImageURLSQL = result.outParamImageURLSQL;
				outParamUseResize = result.outParamUseResize;
			}
		}

		public static class FuncActionDodgySiteImageNumber {

			/// <summary>
			/// Query Function "GetmageIdToUse" of Action "DodgySiteImageNumber"
			/// </summary>
			public static RLTextRecordList QueryGetmageIdToUse(HeContext heContext, int maxRecords, IterationMultiplicity multiplicity, out long outParamCount, string qpstURL, string qpstCurrentDodgyRetailer, string qpstAlternativeImageIndex) {
				// Query Iterations: Never {-unbound-}
				// Uses binary data: False
				if (multiplicity == IterationMultiplicity.Never) {
					maxRecords = 1;
				}
				outParamCount = -1;
				bool useMainTransaction = (multiplicity != IterationMultiplicity.Single || (maxRecords == 1));
				Transaction trans = useMainTransaction? DatabaseAccess.ForCurrentDatabase.GetRequestTransaction(): DatabaseAccess.ForCurrentDatabase.GetReadOnlyTransaction();
				using(var sqlCmd = trans.CreateCommand()) {
					sqlCmd.CreateParameter("@qpstCurrentDodgyRetailer", DbType.String, qpstCurrentDodgyRetailer);
					sqlCmd.CreateParameter("@qpstURL", DbType.String, qpstURL);
					sqlCmd.CreateParameter("@qpstAlternativeImageIndex", DbType.String, qpstAlternativeImageIndex);
					string advSql = "SELECT case when charindex(@qpstCurrentDodgyRetailer,@qpstURL) > 0 \r\nthen @qpstAlternativeImageIndex else 0 \r\nend";
					string sql = AppUtils.Instance.ReplaceEntityReferences(heContext, advSql);
					AppUtils.Instance.CheckReadOnlyEntityReferences(advSql);
					sqlCmd.CommandText = sql;
					try {
						RLTextRecordList outParamList = new RLTextRecordList();
						outParamList.Transaction = trans;
						BitArray[] opt = new BitArray[1];
						opt[0] = new BitArray(new bool[] {
							false});
						outParamList.AllOptimizedAttributes = opt;
						if (multiplicity == IterationMultiplicity.Multiple) {
							RLTextRecordList _tmp = new RLTextRecordList();
							_tmp.Transaction = trans;
							_tmp.AllOptimizedAttributes = opt;
							_tmp.MaxRecords = maxRecords;
							try {
								DateTime startTime = DateTime.Now;
								DatabaseAccess.ForCurrentDatabase.ExecuteQuery(sqlCmd, _tmp, "Query DodgySiteImageNumber.GetmageIdToUse.List");
								int queryExecutionTime = Convert.ToInt32(DateTime.Now.Subtract(startTime).TotalMilliseconds
								);
								RequestTracer reqTracer = RuntimePlatformUtils.GetRequestTracer();
								if (reqTracer != null) {
									reqTracer.RegisterQueryExecuted("3b8c8415-1317-411e-b77e-b9860a427ed6", "DodgySiteImageNumber.GetmageIdToUse", "f82563d5-ffee-43c2-900d-8425d42939cd", "BIFY", queryExecutionTime, DateTime.Now);
								}
								outParamList = (RLTextRecordList) _tmp.Duplicate();
								return outParamList;
							} finally {
								_tmp.CloseDataReader();
							}
						} else if (multiplicity == IterationMultiplicity.Never) {
							try {
								DateTime startTime = DateTime.Now;
								DatabaseAccess.ForCurrentDatabase.ExecuteQuery(sqlCmd, outParamList, "Query DodgySiteImageNumber.GetmageIdToUse.List");
								int queryExecutionTime = Convert.ToInt32(DateTime.Now.Subtract(startTime).TotalMilliseconds
								);
								RequestTracer reqTracer = RuntimePlatformUtils.GetRequestTracer();
								if (reqTracer != null) {
									reqTracer.RegisterQueryExecuted("3b8c8415-1317-411e-b77e-b9860a427ed6", "DodgySiteImageNumber.GetmageIdToUse", "f82563d5-ffee-43c2-900d-8425d42939cd", "BIFY", queryExecutionTime, DateTime.Now);
								}
								outParamList.MaxRecords = maxRecords;
								return outParamList;
							} finally {
								outParamList.CloseDataReader();
							}
						} else {
							try {
								DateTime startTime = DateTime.Now;
								DatabaseAccess.ForCurrentDatabase.ExecuteQuery(sqlCmd, outParamList, "Query DodgySiteImageNumber.GetmageIdToUse.List");
								int queryExecutionTime = Convert.ToInt32(DateTime.Now.Subtract(startTime).TotalMilliseconds
								);
								RequestTracer reqTracer = RuntimePlatformUtils.GetRequestTracer();
								if (reqTracer != null) {
									reqTracer.RegisterQueryExecuted("3b8c8415-1317-411e-b77e-b9860a427ed6", "DodgySiteImageNumber.GetmageIdToUse", "f82563d5-ffee-43c2-900d-8425d42939cd", "BIFY", queryExecutionTime, DateTime.Now);
								}
								outParamList.MaxRecords = maxRecords;
								return outParamList;
							} finally {
								if (maxRecords == 1) {
									outParamList.CloseDataReader();
								}
							}
						}
					} catch (System.InvalidOperationException aqExcep) {
						if (aqExcep.Message.StartsWith("Command parameter[")) {
							throw new DataBaseException("Error executing query.", new DataBaseException("Problems accessing parameters in advanced query GetmageIdToUse in DodgySiteImageNumber in BIFY (SELECT case when charindex(@CurrentDodgyRetailer,@URL) > 0  then @AlternativeImageIndex else 0  end): " + aqExcep.Message));
						}
						throw new DataBaseException("Error executing query.", new DataBaseException("Error in advanced query GetmageIdToUse in DodgySiteImageNumber in BIFY (SELECT case when charindex(@CurrentDodgyRetailer,@URL) > 0  then @AlternativeImageIndex else 0  end): " + aqExcep.Message));
					} catch (Exception aqExcep) {
						throw new DataBaseException("Error executing query.", new DataBaseException("Error in advanced query GetmageIdToUse in DodgySiteImageNumber in BIFY (SELECT case when charindex(@CurrentDodgyRetailer,@URL) > 0  then @AlternativeImageIndex else 0  end): " + aqExcep.Message));
					}
				}
			}

			/// <summary>
			/// Query Function "GetRetailerFixes" of Action "DodgySiteImageNumber"
			/// </summary>
			public static RLRetailerFixesRecordList datasetGetRetailerFixes(HeContext heContext, int maxRecords, IterationMultiplicity multiplicity, out long outParamCount) {
				// Query Iterations: Single {-unbound-}
				// Uses binary data: False
				if (multiplicity == IterationMultiplicity.Never) {
					maxRecords = 1;
				}
				outParamCount = -1;
				bool useMainTransaction = (multiplicity != IterationMultiplicity.Single || (maxRecords > 0 && maxRecords <= 150));
				Transaction trans = useMainTransaction? DatabaseAccess.ForCurrentDatabase.GetRequestTransaction(): DatabaseAccess.ForCurrentDatabase.GetReadOnlyTransaction();
				using(var sqlCmd = trans.CreateCommand()) {
					string sql = "";
					StringBuilder selectBuilder = new StringBuilder();
					StringBuilder fromBuilder = new StringBuilder();
					StringBuilder whereBuilder = new StringBuilder();
					StringBuilder orderByBuilder = new StringBuilder();
					StringBuilder groupByBuilder = new StringBuilder();
					StringBuilder havingBuilder = new StringBuilder();
					selectBuilder.Append("SELECT ");
					if (maxRecords > 0) {
						selectBuilder.Append("TOP (");
						selectBuilder.Append(maxRecords);
						selectBuilder.Append(") ");
					}
					selectBuilder.Append("NULL o0, [ENRETAILERFIXES].[DOMAIN] o1, [ENRETAILERFIXES].[IMAGEINDEX] o2, [ENRETAILERFIXES].[LOOPIMAGES] o3, [ENRETAILERFIXES].[INCLUDEHYBRIDPRODUCT] o4, [ENRETAILERFIXES].[INCLUDEINFER] o5, [ENRETAILERFIXES].[INCLUDEINFERPRODUCT] o6, [ENRETAILERFIXES].[MINIMGWIDTHHEIGHT] o7, [ENRETAILERFIXES].[FILTERSIZES] o8, [ENRETAILERFIXES].[ALLOWPNG] o9, [ENRETAILERFIXES].[ALLOWJPG] o10, [ENRETAILERFIXES].[ALLOWSVG] o11, NULL o12, [ENRETAILERFIXES].[INDEXRELATESTOPRODUCTIMAGES] o13, [ENRETAILERFIXES].[IMAGEURLTEMPLATE] o14, [ENRETAILERFIXES].[SQLCODETOGETID] o15, NULL o16");
					fromBuilder.Append(" FROM {RetailerFixes} [ENRETAILERFIXES]");
					whereBuilder.Append(" WHERE ([ENRETAILERFIXES].[ISACTIVE] = 1)");
					orderByBuilder.Append(" ORDER BY [ENRETAILERFIXES].[DOMAIN] ASC ");

					sql = selectBuilder.ToString() + fromBuilder.ToString() + whereBuilder.ToString() + groupByBuilder.ToString() + havingBuilder.ToString() + orderByBuilder.ToString();
					string advSql = sql;
					sql = AppUtils.Instance.ReplaceEntityReferences(heContext, advSql);
					sqlCmd.CommandText = sql;
					try {
						RLRetailerFixesRecordList outParamList = new RLRetailerFixesRecordList();
						outParamList.Transaction = trans;
						BitArray[] opt = new BitArray[1];
						opt[0] = new BitArray(new bool[] {
							true, false, false, false, false, false, false, false, false, false, false, false, true, false, false, false, true});
						outParamList.AllOptimizedAttributes = opt;
						if (multiplicity == IterationMultiplicity.Multiple) {
							RLRetailerFixesRecordList _tmp = new RLRetailerFixesRecordList();
							_tmp.Transaction = trans;
							_tmp.AllOptimizedAttributes = opt;
							_tmp.MaxRecords = maxRecords;
							try {
								DateTime startTime = DateTime.Now;
								DatabaseAccess.ForCurrentDatabase.ExecuteQuery(sqlCmd, _tmp, "Query DodgySiteImageNumber.GetRetailerFixes.List");
								int queryExecutionTime = Convert.ToInt32(DateTime.Now.Subtract(startTime).TotalMilliseconds
								);
								RequestTracer reqTracer = RuntimePlatformUtils.GetRequestTracer();
								if (reqTracer != null) {
									reqTracer.RegisterQueryExecuted("c47740d9-3c41-40b2-ba4f-b489821e376b", "DodgySiteImageNumber.GetRetailerFixes", "f82563d5-ffee-43c2-900d-8425d42939cd", "BIFY", queryExecutionTime, DateTime.Now);
								}
								outParamList = (RLRetailerFixesRecordList) _tmp.Duplicate();
								return outParamList;
							} finally {
								_tmp.CloseDataReader();
							}
						} else if (multiplicity == IterationMultiplicity.Never) {
							try {
								DateTime startTime = DateTime.Now;
								DatabaseAccess.ForCurrentDatabase.ExecuteQuery(sqlCmd, outParamList, "Query DodgySiteImageNumber.GetRetailerFixes.List");
								int queryExecutionTime = Convert.ToInt32(DateTime.Now.Subtract(startTime).TotalMilliseconds
								);
								RequestTracer reqTracer = RuntimePlatformUtils.GetRequestTracer();
								if (reqTracer != null) {
									reqTracer.RegisterQueryExecuted("c47740d9-3c41-40b2-ba4f-b489821e376b", "DodgySiteImageNumber.GetRetailerFixes", "f82563d5-ffee-43c2-900d-8425d42939cd", "BIFY", queryExecutionTime, DateTime.Now);
								}
								outParamList.MaxRecords = maxRecords;
								return outParamList;
							} finally {
								outParamList.CloseDataReader();
							}
						} else {
							if (maxRecords > 0 && maxRecords <= 150) {
								RLRetailerFixesRecordList _tmp = new RLRetailerFixesRecordList();
								_tmp.Transaction = trans;
								_tmp.AllOptimizedAttributes = opt;
								try {
									DateTime startTime = DateTime.Now;
									DatabaseAccess.ForCurrentDatabase.ExecuteQuery(sqlCmd, _tmp, "Query DodgySiteImageNumber.GetRetailerFixes.List");
									int queryExecutionTime = Convert.ToInt32(DateTime.Now.Subtract(startTime).TotalMilliseconds
									);
									RequestTracer reqTracer = RuntimePlatformUtils.GetRequestTracer();
									if (reqTracer != null) {
										reqTracer.RegisterQueryExecuted("c47740d9-3c41-40b2-ba4f-b489821e376b", "DodgySiteImageNumber.GetRetailerFixes", "f82563d5-ffee-43c2-900d-8425d42939cd", "BIFY", queryExecutionTime, DateTime.Now);
									}
									outParamList = (RLRetailerFixesRecordList) _tmp.Duplicate();
									outParamList.AllOptimizedAttributes = opt;
									return outParamList;
								} finally {
									_tmp.CloseDataReader();
								}
							} else {
								try {
									DateTime startTime = DateTime.Now;
									DatabaseAccess.ForCurrentDatabase.ExecuteQuery(sqlCmd, outParamList, "Query DodgySiteImageNumber.GetRetailerFixes.List");
									int queryExecutionTime = Convert.ToInt32(DateTime.Now.Subtract(startTime).TotalMilliseconds
									);
									RequestTracer reqTracer = RuntimePlatformUtils.GetRequestTracer();
									if (reqTracer != null) {
										reqTracer.RegisterQueryExecuted("c47740d9-3c41-40b2-ba4f-b489821e376b", "DodgySiteImageNumber.GetRetailerFixes", "f82563d5-ffee-43c2-900d-8425d42939cd", "BIFY", queryExecutionTime, DateTime.Now);
									}
									outParamList.MaxRecords = maxRecords;
									return outParamList;
								} finally {
									if (maxRecords == 1) {
										outParamList.CloseDataReader();
									}
								}
							}
						}
					} catch (Exception e) {
						throw new DataBaseException("Error executing query.", e);
					}
				}
			}



		}


	}

}