﻿/* 
 This source code (the "Generated Software") is generated by the OutSystems Platform 
 and is licensed by OutSystems (http://www.outsystems.com) to You solely for testing and evaluation 
 purposes, unless You and OutSystems have executed a specific agreement covering the use terms and 
 conditions of the Generated Software, in which case such agreement shall apply. 
*/

using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Runtime.Serialization;
using System.Reflection;
using System.Text;
using System.Text.RegularExpressions;
using OutSystems.HubEdition.RuntimePlatform;
using OutSystems.HubEdition.RuntimePlatform.Log;
using OutSystems.HubEdition.RuntimePlatform.Db;
using OutSystems.Internal.Db;
using OutSystems.HubEdition.RuntimePlatform.Email;
using OutSystems.HubEdition.RuntimePlatform.Web;
using OutSystems.HubEdition.RuntimePlatform.NewRuntime;
using OutSystems.RuntimeCommon;
using OutSystems.RuntimeCommon.Caching;
using OutSystems.ObjectKeys;
using System.Resources;

namespace ssBIFY {

	public partial class Actions {
		/// <summary>
		/// Action <code>ActionGetMetaFromURL</code> that represents the Service Studio reference action
		///  <code>GetMetaFromURL</code> <p> Description: </p>
		/// </summary>
		public static void ActionGetMetaFromURL(HeContext heContext, string inParamURL, string inParamHTML, bool inParamTryToFixImageURLS, out string outParamTitle, out string outParamDescription, out string outParamPrice, out string outParamImageURL, out string outParamImageURLs, out string outParamAllImageUrls, out string outParamDomain, out bool outParamBlocked) {
			RssExtensionGetMeta.MssGetMetaFromURL(heContext, inParamURL, inParamHTML, inParamTryToFixImageURLS, out outParamTitle, out outParamDescription, out outParamPrice, out outParamImageURL, out outParamImageURLs, out outParamAllImageUrls, out outParamDomain, out outParamBlocked);
		}

	}

}