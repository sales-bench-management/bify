﻿/* 
 This source code (the "Generated Software") is generated by the OutSystems Platform 
 and is licensed by OutSystems (http://www.outsystems.com) to You solely for testing and evaluation 
 purposes, unless You and OutSystems have executed a specific agreement covering the use terms and 
 conditions of the Generated Software, in which case such agreement shall apply. 
*/

using System;
using System.Linq;
using OutSystems.HubEdition.RuntimePlatform;
using OutSystems.HubEdition.RuntimePlatform.Db;
using OutSystems.Internal.Db;
using OutSystems.RuntimeCommon;
using OutSystems.RuntimeCommon.Caching;

using System.Text;
using System.Data;
using System.Collections.Generic;
using Newtonsoft.Json;
using OutSystems.RESTService;
using OutSystems.RESTService.Behaviors;
using OutSystems.RESTService.Controllers;
using OutSystems.ObjectKeys;
using OutSystems.HubEdition.RuntimePlatform.NewRuntime;


namespace ssBIFY.RestRecords {

	public class JSONENEventEntityRecord: AbstractRESTStructure<ENEventEntityRecord> {
		[JsonProperty("Id")]
		public long? AttrId;

		[JsonProperty("EventName")]
		public string AttrEventName;

		[JsonProperty("EventDate")]
		public String AttrEventDate;

		[JsonProperty("UserId")]
		public int? AttrUserId;

		[JsonProperty("EventTypeId")]
		public int? AttrEventTypeId;

		[JsonProperty("Public")]
		public bool? AttrPublic;

		[JsonProperty("Recurring")]
		public bool? AttrRecurring;

		public JSONENEventEntityRecord() {}

		public JSONENEventEntityRecord(ENEventEntityRecord s, IBehaviorsConfiguration config) {
			if (config.DefaultValuesBehavior == DefaultValuesBehavior.DontSend) {
				AttrId = (long?) s.ssId;
				AttrEventName = ConvertToRestWithoutDefaults(s.ssEventName, "");
				AttrEventDate = ConvertDateToRestWithoutDefaults(s.ssEventDate, new DateTime(1900, 1, 1, 0, 0, 0));
				AttrUserId = ConvertToRestWithoutDefaults(s.ssUserId, 0);
				AttrEventTypeId = ConvertToRestWithoutDefaults(s.ssEventTypeId, 0);
				AttrPublic = ConvertToRestWithoutDefaults(s.ssPublic, true);
				AttrRecurring = ConvertToRestWithoutDefaults(s.ssRecurring, false);
			} else {
				AttrId = (long?) s.ssId;
				AttrEventName = s.ssEventName;
				AttrEventDate = OutSystems.RESTService.Conversions.DateToText(s.ssEventDate);
				AttrUserId = (int?) s.ssUserId;
				AttrEventTypeId = (int?) s.ssEventTypeId;
				AttrPublic = (bool?) s.ssPublic;
				AttrRecurring = (bool?) s.ssRecurring;
			}
		}

		public static Func<ssBIFY.RestRecords.JSONENEventEntityRecord, ENEventEntityRecord> ToStructureDelegate(IBehaviorsConfiguration config) {
			return (ssBIFY.RestRecords.JSONENEventEntityRecord s) => ToStructure(s, config);
		}
		public static ENEventEntityRecord ToStructure(ssBIFY.RestRecords.JSONENEventEntityRecord obj, IBehaviorsConfiguration config) {
			ENEventEntityRecord s = new ENEventEntityRecord(null);
			if (obj != null) {
				s.ssId = obj.AttrId == null ? 0L: obj.AttrId.Value;
				s.ssEventName = obj.AttrEventName == null ? "": obj.AttrEventName;
				s.ssEventDate = obj.AttrEventDate == null ? new DateTime(1900, 1, 1, 0, 0, 0): OutSystems.RESTService.Conversions.TextToDate(obj.AttrEventDate);
				s.ssUserId = obj.AttrUserId == null ? 0: obj.AttrUserId.Value;
				s.ssEventTypeId = obj.AttrEventTypeId == null ? 0: obj.AttrEventTypeId.Value;
				s.ssPublic = obj.AttrPublic == null ? true: obj.AttrPublic.Value;
				s.ssRecurring = obj.AttrRecurring == null ? false: obj.AttrRecurring.Value;
			}
			return s;
		}

		public static Func<ENEventEntityRecord, ssBIFY.RestRecords.JSONENEventEntityRecord> FromStructureDelegate(IBehaviorsConfiguration config) {
			return (ENEventEntityRecord s) => FromStructure(s, config);
		}
		public static ssBIFY.RestRecords.JSONENEventEntityRecord FromStructure(ENEventEntityRecord s, IBehaviorsConfiguration config) {
			return new ssBIFY.RestRecords.JSONENEventEntityRecord(s, config);
		}

	}



}