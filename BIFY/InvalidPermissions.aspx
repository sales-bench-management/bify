﻿<%@ Page Language="c#" Codebehind="InvalidPermissions.aspx.cs" AutoEventWireup="false" Inherits="ssBIFY.Flows.FlowCommon.ScrnInvalidPermissions" %>
<%@ Register TagPrefix="osweb" Namespace="OutSystems.HubEdition.WebWidgets" Assembly="OutSystems.HubEdition.WebWidgets" %>
<%@ Import namespace="ssBIFY" %>
<%@ Import namespace="OutSystems.HubEdition.RuntimePlatform" %>
<%@ Assembly Name="OutSystems.WidgetsRuntimeAPI" %>
<%@ Register TagPrefix="widgets" TagName="Kvm5_RC0mw0KuAAMxuiwtpQ" Src="Blocks\BIFY\Common\Layout.ascx" %>
<%@ Register TagPrefix="widgets" TagName="KXIDBGmiH30CQls7epjSjoQ" Src="Blocks\BIFY\Common\ApplicationTitle.ascx" %>
<%@ Register TagPrefix="widgets" TagName="K_5sqIAe8Z0mLMNBss5hGzg" Src="Blocks\BIFY\Common\Menu.ascx" %>
<%@ Register TagPrefix="widgets" TagName="KeTXYsEiyT0iz699EGQA9pw" Src="Blocks\BIFY\Common\LoginInfo.ascx" %>
<%@ Register TagPrefix="OutSystemsUIWeb_widgets" TagName="K0OMmaHyl9E68I_pv5pITnw" Src="Blocks\BIFY\Content\Card.ascx" %>
<%@ Register TagPrefix="RichWidgets_widgets" TagName="KmbfKJ2gWQUq1Gwk_0SjV4w" Src="Blocks\BIFY\RichWidgets\Icon.ascx" %>

<%= BIFY_Properties.DocType %>
<html xmlns="http://www.w3.org/1999/xhtml" >
  <head runat="server"><%= GetHeadTopJavaScript() %>
	<title><%= HttpUtility.HtmlEncode (Title) %></title>
    <meta http-equiv="Content-Type" content="<%= "text/html; charset=" + Response.ContentEncoding.WebName %>" />
    <meta http-equiv="Content-Script-Type" content="text/javascript" />
    <meta http-equiv="Content-Style-Type" content="text/css" />
<%= "\n" + GetStyleSheetIncludes() %><%= GetRequestInfoJavaScript() + GetJavaScriptIncludes() + GetHeadBottomJavaScript() %>
  </head>
  <osweb:Body runat="server"><%= GetBodyTopJavaScript() %>
    <osweb:Form id="WebForm1" method="post"  action="<%# GetFormAction() %>" runat="server">
<widgets:Kvm5_RC0mw0KuAAMxuiwtpQ runat="server" id="wt_WebBlockInstance3" OnEvaluateParameters="webBlck_WebBlockInstance3_onDataBinding" InstanceID="_WebBlockInstance3"><phLeft><widgets:KXIDBGmiH30CQls7epjSjoQ runat="server" id="wt_WebBlockInstance19" OnEvaluateParameters="webBlck_WebBlockInstance19_onDataBinding" InstanceID="_WebBlockInstance19"></widgets:KXIDBGmiH30CQls7epjSjoQ></phLeft><phCenter><widgets:K_5sqIAe8Z0mLMNBss5hGzg runat="server" id="wt_WebBlockInstance12" OnEvaluateParameters="webBlck_WebBlockInstance12_onDataBinding" InstanceID="_WebBlockInstance12"></widgets:K_5sqIAe8Z0mLMNBss5hGzg></phCenter><phRight><widgets:KeTXYsEiyT0iz699EGQA9pw runat="server" id="wt_WebBlockInstance14" OnEvaluateParameters="webBlck_WebBlockInstance14_onDataBinding" InstanceID="_WebBlockInstance14"></widgets:KeTXYsEiyT0iz699EGQA9pw></phRight><phHeaderContent></phHeaderContent><phBreadcrumbs></phBreadcrumbs><phTitle></phTitle><phActions></phActions><phMainContent><OutSystemsUIWeb_widgets:K0OMmaHyl9E68I_pv5pITnw runat="server" id="OutSystemsUIWeb_wt_WebBlockInstance6" OnEvaluateParameters="OutSystemsUIWeb_webBlck_WebBlockInstance6_onDataBinding" InstanceID="_WebBlockInstance6"><phContent><osweb:Container runat="server" id="wtTitle" onDataBinding="cntTitle_onDataBinding" cssClass="text-info heading1"><RichWidgets_widgets:KmbfKJ2gWQUq1Gwk_0SjV4w runat="server" id="RichWidgets_wt_WebBlockInstance23" OnEvaluateParameters="RichWidgets_webBlck_WebBlockInstance23_onDataBinding" InstanceID="_WebBlockInstance23"></RichWidgets_widgets:KmbfKJ2gWQUq1Gwk_0SjV4w><osweb:PlaceHolder runat="server"><%# "Invalid Permissions" %></osweb:PlaceHolder></osweb:Container><osweb:Container runat="server" id="wtMessage" onDataBinding="cntMessage_onDataBinding" align="center"><osweb:PlaceHolder runat="server"><%# "You don&#39;t have permissions to access this screen. <br/>Please contact the administration team." %></osweb:PlaceHolder></osweb:Container><osweb:Container runat="server" id="wtLink" onDataBinding="cntLink_onDataBinding" align="center"><osweb:HyperLink runat="server" id="wt_Link1" Visible="<%# lnk_Link1_isVisible() %>" Enabled="<%# lnk_Link1_isEnabled() %>" NavigateUrl="<%# lnk_Link1_NavigateUrl() %>"><osweb:PlaceHolder runat="server"><%# "Click here to continue" %></osweb:PlaceHolder></osweb:HyperLink></osweb:Container></phContent></OutSystemsUIWeb_widgets:K0OMmaHyl9E68I_pv5pITnw></phMainContent><phFooter></phFooter></widgets:Kvm5_RC0mw0KuAAMxuiwtpQ><osweb:DummySubmitLink runat="server" id="Dummy_Submit_Link"/>
    <%= OutSystems.HubEdition.RuntimePlatform.AppInfo.GetAppInfo().GetWatermark() %><%= GetCallbackDebug()	
%><%= GetVisitCode() %></osweb:Form><%= GetEndPageJavaScript() + GetBodyBottomJavaScript() %>
  </osweb:Body>
</html>
