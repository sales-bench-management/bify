﻿/* 
 This source code (the "Generated Software") is generated by the OutSystems Platform 
 and is licensed by OutSystems (http://www.outsystems.com) to You solely for testing and evaluation 
 purposes, unless You and OutSystems have executed a specific agreement covering the use terms and 
 conditions of the Generated Software, in which case such agreement shall apply. 
*/

using System;
using System.Data;
using System.Runtime.Serialization;
using System.Reflection;
using System.Xml;
using OutSystems.ObjectKeys;
using OutSystems.HubEdition.RuntimePlatform;
using OutSystems.HubEdition.RuntimePlatform.Db;
using OutSystems.Internal.Db;
using System.Xml.Serialization;
using System.Collections;
using System.Collections.Generic;
using OutSystems.RuntimeCommon;
using OutSystems.HubEdition.RuntimePlatform.NewRuntime;

namespace ssBIFY {

	public sealed partial class ENStepEntity {
		public static string LocalViewName(int? tenant, string locale) {
			return ViewName(null, locale);
		}
		public static string ViewName(int? tenant, string locale) {
			return RsseSpaceOutSystemsUIWeb.ENStepEntity.ViewName(tenant, locale);
		}
		public static readonly ObjectKey eSpaceKey = RsseSpaceOutSystemsUIWeb.ENStepEntity.eSpaceKey;
		public static System.Collections.Generic.Dictionary<string, string> AttributesToDatabaseNamesMap() {
			return new System.Collections.Generic.Dictionary<string, string>() {
				{ "steps", "Steps"
				}
			};
		}
	} // ENStepEntity

	/// <summary>
	/// Entity <code>ENStepEntityRecord</code> that represents the Service Studio entity <code>Step</code>
	///  <p> Description: Different types of status.</p>
	/// </summary>
	[OutSystems.HubEdition.RuntimePlatform.MetaInformation.EntityRecordDetails("Step", "Ms_yQwOvVEmcUY_YgZfl1g", "ftsuvongDUa8WwDHf790vg", 0, "OSUSR_41o_Step", "", false)]
	[Serializable()]
	public partial struct ENStepEntityRecord: ISerializable, ITypedRecord<ENStepEntityRecord>, ISimpleRecord {
		internal static readonly GlobalObjectKey IdSteps = GlobalObjectKey.Parse("ftsuvongDUa8WwDHf790vg*xB6h6v3_00yvcuTzpUQHUw");

		public static void EnsureInitialized() {}
		[OutSystems.HubEdition.RuntimePlatform.MetaInformation.EntityAttributeDetails("STEPS", 50, false, true, false, true)]
		[System.Xml.Serialization.XmlElement("Steps")]
		private string _ssSteps;
		public string ssSteps {
			get {
				return _ssSteps;
			}
			set {
				if ((_ssSteps!=value) || OptimizedAttributes[0]) {
					ChangedAttributes = new BitArray(1, true);
					_ssSteps = value;
				}
			}
		}


		public BitArray ChangedAttributes;

		public BitArray OptimizedAttributes;

		public ENStepEntityRecord(params string[] dummy) {
			ChangedAttributes = new BitArray(1, true);
			OptimizedAttributes = new BitArray(1, false);
			_ssSteps = "";
		}

		public BitArray[] GetDefaultOptimizedValues() {
			BitArray[] all = new BitArray[0];
			return all;
		}

		public BitArray[] AllOptimizedAttributes {
			set {
				if (value == null) {
				} else {
				}
			}
			get {
				BitArray[] all = new BitArray[0];
				return all;
			}
		}

		/// <summary>
		/// Read a record from database
		/// </summary>
		/// <param name="r"> Data base reader</param>
		/// <param name="index"> index</param>
		public void Read(IDataReader r, ref int index) {
			ssSteps = r.ReadText(index++, "Step.Steps", "");
			ChangedAttributes = new BitArray(1, false);
			OptimizedAttributes = new BitArray(1, false);
		}
		/// <summary>
		/// Read from database
		/// </summary>
		/// <param name="r"> Data reader</param>
		public void ReadDB(IDataReader r) {
			int index = 0;
			Read(r, ref index);
		}

		/// <summary>
		/// Read from record
		/// </summary>
		/// <param name="r"> Record</param>
		public void ReadIM(ENStepEntityRecord r) {
			this = r;
		}


		public static bool operator == (ENStepEntityRecord a, ENStepEntityRecord b) {
			if (a.ssSteps != b.ssSteps) return false;
			return true;
		}

		public static bool operator != (ENStepEntityRecord a, ENStepEntityRecord b) {
			return !(a==b);
		}

		public override bool Equals(object o) {
			if (o.GetType() != typeof(ENStepEntityRecord)) return false;
			return (this == (ENStepEntityRecord) o);
		}

		public override int GetHashCode() {
			try {
				return base.GetHashCode()
				^ ssSteps.GetHashCode()
				;
			} catch {
				return base.GetHashCode();
			}
		}

		public void GetObjectData(SerializationInfo info, StreamingContext context) {
			Type objInfo = this.GetType();
			FieldInfo[] fields;
			fields = objInfo.GetFields(BindingFlags.NonPublic | BindingFlags.Instance | BindingFlags.Public);
			for (int i = 0; i < fields.Length; i++)
			if (fields[i] .FieldType.IsSerializable)
			info.AddValue(fields[i] .Name, fields[i] .GetValue(this));
		}

		public ENStepEntityRecord(SerializationInfo info, StreamingContext context) {
			ChangedAttributes = new BitArray(1, true);
			OptimizedAttributes = new BitArray(1, false);
			_ssSteps = "";
			Type objInfo = this.GetType();
			FieldInfo fieldInfo = null;
			fieldInfo = objInfo.GetField("_ssSteps", BindingFlags.NonPublic | BindingFlags.Instance | BindingFlags.Public);
			if (fieldInfo == null) {
				throw new Exception("The field named '_ssSteps' was not found.");
			}
			if (fieldInfo.FieldType.IsSerializable) {
				_ssSteps = (string) info.GetValue(fieldInfo.Name, fieldInfo.FieldType);
			}
		}

		public void RecursiveReset() {
		}

		public void InternalRecursiveSave() {
		}


		public ENStepEntityRecord Duplicate() {
			ENStepEntityRecord t;
			t._ssSteps = this._ssSteps;
			t.ChangedAttributes = new BitArray(1);
			t.OptimizedAttributes = new BitArray(1);
			for (int i = 0; i < 1; i++) {
				t.ChangedAttributes[i] = ChangedAttributes[i];
				t.OptimizedAttributes[i] = OptimizedAttributes[i];
			}
			return t;
		}

		IRecord IRecord.Duplicate() {
			return Duplicate();
		}

		public void ToXml(Object parent, System.Xml.XmlElement baseElem, String fieldName, int detailLevel) {
			System.Xml.XmlElement recordElem = VarValue.AppendChild(baseElem, "Entity");
			if (fieldName != null) {
				VarValue.AppendAttribute(recordElem, "debug.field", fieldName);
				fieldName = fieldName.ToLowerInvariant();
			}
			if (detailLevel > 0) {
				if (!VarValue.FieldIsOptimized(parent, fieldName + ".Steps")) VarValue.AppendAttribute(recordElem, "Steps", ssSteps, detailLevel, TypeKind.Text); else VarValue.AppendOptimizedAttribute(recordElem, "Steps");
			} else {
				VarValue.AppendDeferredEvaluationElement(recordElem);
			}
		}

		public void EvaluateFields(VarValue variable, Object parent, String baseName, String fields) {
			String head = VarValue.GetHead(fields);
			String tail = VarValue.GetTail(fields);
			variable.Found = false;
			if (head == "steps") {
				if (!VarValue.FieldIsOptimized(parent, baseName + ".Steps")) variable.Value = ssSteps; else variable.Optimized = true;
			}
			if (variable.Found && tail != null) variable.EvaluateFields(this, head, tail);
		}

		public bool ChangedAttributeGet(GlobalObjectKey key) {
			if (key.Equals(IdSteps)) {
				return ChangedAttributes[0];
			} else {
				throw new Exception("Invalid key");
			}
		}

		public bool OptimizedAttributeGet(GlobalObjectKey key) {
			if (key.Equals(IdSteps)) {
				return OptimizedAttributes[0];
			} else {
				throw new Exception("Invalid key");
			}
		}

		public object AttributeGet(GlobalObjectKey key) {
			if (key == IdSteps) {
				return ssSteps;
			} else {
				throw new Exception("Invalid key");
			}
		}
		public void FillFromOther(IRecord other) {
			ChangedAttributes = new BitArray(1);
			OptimizedAttributes = new BitArray(1);
			if (other == null) return;
			ssSteps = (string) other.AttributeGet(IdSteps);
			ChangedAttributes[0] = other.ChangedAttributeGet(IdSteps);
			OptimizedAttributes[0] = other.OptimizedAttributeGet(IdSteps);
		}
	} // ENStepEntityRecord
	/// <summary>
	/// Static Entity <code>ENStepEntity</code> gets the values for this static entity records in runtime
	///  <code>Step</code>
	/// </summary>
	partial class ENStepEntity {


		static ENStepEntity() {
			ENStepEntityRecordTypeFactoryImpl.InitializeFactory();
		}

		public static ENStepEntityRecord GetRecordById(string id) {
			return (ENStepEntityRecord) RsseSpaceOutSystemsUIWeb.ENStepEntity.GetRecordById(id);
		}

		public static ENStepEntityRecord GetRecordByName(string name) {
			return (ENStepEntityRecord) RsseSpaceOutSystemsUIWeb.ENStepEntity.GetRecordByName(name);
		}

		public static ENStepEntityRecord GetRecordByKey(ObjectKey key) {
			return (ENStepEntityRecord) RsseSpaceOutSystemsUIWeb.ENStepEntity.GetRecordByKey(key);
		}
	} // ENStepEntity;
	/// <summary>
	/// Structure <code>RCStepRecord</code>
	/// </summary>
	[Serializable()]
	public partial struct RCStepRecord: ISerializable, ITypedRecord<RCStepRecord> {
		internal static readonly GlobalObjectKey IdStep = GlobalObjectKey.Parse("2UmDmepsh0WSfJ_D1JexCA*7g0YGA4zwUqEpY9B9XJWCw");

		public static void EnsureInitialized() {}
		[System.Xml.Serialization.XmlElement("Step")]
		public ENStepEntityRecord ssENStep;


		public static implicit operator ENStepEntityRecord(RCStepRecord r) {
			return r.ssENStep;
		}

		public static implicit operator RCStepRecord(ENStepEntityRecord r) {
			RCStepRecord res = new RCStepRecord(null);
			res.ssENStep = r;
			return res;
		}

		public BitArray ChangedAttributes {
			set {
				ssENStep.ChangedAttributes = value;
			}
			get {
				return ssENStep.ChangedAttributes;
			}
		}
		public BitArray OptimizedAttributes;

		public RCStepRecord(params string[] dummy) {
			OptimizedAttributes = null;
			ssENStep = new ENStepEntityRecord(null);
		}

		public BitArray[] GetDefaultOptimizedValues() {
			BitArray[] all = new BitArray[1];
			all[0] = new BitArray(1, false);
			return all;
		}

		public BitArray[] AllOptimizedAttributes {
			set {
				if (value == null) {
					ssENStep.OptimizedAttributes = GetDefaultOptimizedValues()[0];
				} else {
					ssENStep.OptimizedAttributes = value[0];
				}
			}
			get {
				BitArray[] all = new BitArray[1];
				all[0] = ssENStep.OptimizedAttributes;
				return all;
			}
		}

		/// <summary>
		/// Read a record from database
		/// </summary>
		/// <param name="r"> Data base reader</param>
		/// <param name="index"> index</param>
		public void Read(IDataReader r, ref int index) {
			ssENStep.Read(r, ref index);
		}
		/// <summary>
		/// Read from database
		/// </summary>
		/// <param name="r"> Data reader</param>
		public void ReadDB(IDataReader r) {
			int index = 0;
			Read(r, ref index);
		}

		/// <summary>
		/// Read from record
		/// </summary>
		/// <param name="r"> Record</param>
		public void ReadIM(RCStepRecord r) {
			this = r;
		}


		public static bool operator == (RCStepRecord a, RCStepRecord b) {
			if (a.ssENStep != b.ssENStep) return false;
			return true;
		}

		public static bool operator != (RCStepRecord a, RCStepRecord b) {
			return !(a==b);
		}

		public override bool Equals(object o) {
			if (o.GetType() != typeof(RCStepRecord)) return false;
			return (this == (RCStepRecord) o);
		}

		public override int GetHashCode() {
			try {
				return base.GetHashCode()
				^ ssENStep.GetHashCode()
				;
			} catch {
				return base.GetHashCode();
			}
		}

		public void GetObjectData(SerializationInfo info, StreamingContext context) {
			Type objInfo = this.GetType();
			FieldInfo[] fields;
			fields = objInfo.GetFields(BindingFlags.NonPublic | BindingFlags.Instance | BindingFlags.Public);
			for (int i = 0; i < fields.Length; i++)
			if (fields[i] .FieldType.IsSerializable)
			info.AddValue(fields[i] .Name, fields[i] .GetValue(this));
		}

		public RCStepRecord(SerializationInfo info, StreamingContext context) {
			OptimizedAttributes = null;
			ssENStep = new ENStepEntityRecord(null);
			Type objInfo = this.GetType();
			FieldInfo fieldInfo = null;
			fieldInfo = objInfo.GetField("ssENStep", BindingFlags.NonPublic | BindingFlags.Instance | BindingFlags.Public);
			if (fieldInfo == null) {
				throw new Exception("The field named 'ssENStep' was not found.");
			}
			if (fieldInfo.FieldType.IsSerializable) {
				ssENStep = (ENStepEntityRecord) info.GetValue(fieldInfo.Name, fieldInfo.FieldType);
			}
		}

		public void RecursiveReset() {
			ssENStep.RecursiveReset();
		}

		public void InternalRecursiveSave() {
			ssENStep.InternalRecursiveSave();
		}


		public RCStepRecord Duplicate() {
			RCStepRecord t;
			t.ssENStep = (ENStepEntityRecord) this.ssENStep.Duplicate();
			t.OptimizedAttributes = null;
			return t;
		}

		IRecord IRecord.Duplicate() {
			return Duplicate();
		}

		public void ToXml(Object parent, System.Xml.XmlElement baseElem, String fieldName, int detailLevel) {
			System.Xml.XmlElement recordElem = VarValue.AppendChild(baseElem, "Record");
			if (fieldName != null) {
				VarValue.AppendAttribute(recordElem, "debug.field", fieldName);
			}
			if (detailLevel > 0) {
				ssENStep.ToXml(this, recordElem, "Step", detailLevel - 1);
			} else {
				VarValue.AppendDeferredEvaluationElement(recordElem);
			}
		}

		public void EvaluateFields(VarValue variable, Object parent, String baseName, String fields) {
			String head = VarValue.GetHead(fields);
			String tail = VarValue.GetTail(fields);
			variable.Found = false;
			if (head == "step") {
				if (!VarValue.FieldIsOptimized(parent, baseName + ".Step")) variable.Value = ssENStep; else variable.Optimized = true;
				variable.SetFieldName("step");
			}
			if (variable.Found && tail != null) variable.EvaluateFields(this, head, tail);
		}

		public bool ChangedAttributeGet(GlobalObjectKey key) {
			return ssENStep.ChangedAttributeGet(key);
		}

		public bool OptimizedAttributeGet(GlobalObjectKey key) {
			return ssENStep.OptimizedAttributeGet(key);
		}

		public object AttributeGet(GlobalObjectKey key) {
			if (key == IdStep) {
				return ssENStep;
			} else {
				throw new Exception("Invalid key");
			}
		}
		public void FillFromOther(IRecord other) {
			if (other == null) return;
			ssENStep.FillFromOther((IRecord) other.AttributeGet(IdStep));
		}
	} // RCStepRecord
	/// <summary>
	/// RecordList type <code>RLStepRecordList</code> that represents a record list of <code>Step</code>
	/// </summary>
	[Serializable()]
	public partial class RLStepRecordList: GenericRecordList<RCStepRecord>, IEnumerable, IEnumerator, ISerializable {
		public static void EnsureInitialized() {}

		protected override RCStepRecord GetElementDefaultValue() {
			return new RCStepRecord("");
		}

		public T[] ToArray<T>(Func<RCStepRecord, T> converter) {
			return ToArray(this, converter);
		}

		public static T[] ToArray<T>(RLStepRecordList recordlist, Func<RCStepRecord, T> converter) {
			return InnerToArray(recordlist, converter);
		}
		public static implicit operator RLStepRecordList(RCStepRecord[] array) {
			RLStepRecordList result = new RLStepRecordList();
			result.InnerFromArray(array);
			return result;
		}

		public static RLStepRecordList ToList<T>(T[] array, Func <T, RCStepRecord> converter) {
			RLStepRecordList result = new RLStepRecordList();
			result.InnerFromArray(array, converter);
			return result;
		}

		public static RLStepRecordList FromRestList<T>(RestList<T> restList, Func <T, RCStepRecord> converter) {
			RLStepRecordList result = new RLStepRecordList();
			result.InnerFromRestList(restList, converter);
			return result;
		}
		/// <summary>
		/// Default Constructor
		/// </summary>
		public RLStepRecordList(): base() {
		}

		/// <summary>
		/// Constructor with transaction parameter
		/// </summary>
		/// <param name="trans"> IDbTransaction Parameter</param>
		[Obsolete("Use the Default Constructor and set the Transaction afterwards.")]
		public RLStepRecordList(IDbTransaction trans): base(trans) {
		}

		/// <summary>
		/// Constructor with transaction parameter and alternate read method
		/// </summary>
		/// <param name="trans"> IDbTransaction Parameter</param>
		/// <param name="alternateReadDBMethod"> Alternate Read Method</param>
		[Obsolete("Use the Default Constructor and set the Transaction afterwards.")]
		public RLStepRecordList(IDbTransaction trans, ReadDBMethodDelegate alternateReadDBMethod): this(trans) {
			this.alternateReadDBMethod = alternateReadDBMethod;
		}

		/// <summary>
		/// Constructor declaration for serialization
		/// </summary>
		/// <param name="info"> SerializationInfo</param>
		/// <param name="context"> StreamingContext</param>
		public RLStepRecordList(SerializationInfo info, StreamingContext context): base(info, context) {
		}

		public override BitArray[] GetDefaultOptimizedValues() {
			BitArray[] def = new BitArray[1];
			def[0] = new BitArray(1, false);
			return def;
		}
		/// <summary>
		/// Create as new list
		/// </summary>
		/// <returns>The new record list</returns>
		protected override OSList<RCStepRecord> NewList() {
			return new RLStepRecordList();
		}


	} // RLStepRecordList

	/// <summary>
	/// RecordList type <code>RLStepList</code> that represents a record list of <code>Step</code>
	/// </summary>
	[Serializable()]
	public partial class RLStepList: GenericRecordList<ENStepEntityRecord>, IEnumerable, IEnumerator, ISerializable {
		public static void EnsureInitialized() {}

		protected override ENStepEntityRecord GetElementDefaultValue() {
			return new ENStepEntityRecord("");
		}

		public T[] ToArray<T>(Func<ENStepEntityRecord, T> converter) {
			return ToArray(this, converter);
		}

		public static T[] ToArray<T>(RLStepList recordlist, Func<ENStepEntityRecord, T> converter) {
			return InnerToArray(recordlist, converter);
		}
		public static implicit operator RLStepList(ENStepEntityRecord[] array) {
			RLStepList result = new RLStepList();
			result.InnerFromArray(array);
			return result;
		}

		public static RLStepList ToList<T>(T[] array, Func <T, ENStepEntityRecord> converter) {
			RLStepList result = new RLStepList();
			result.InnerFromArray(array, converter);
			return result;
		}

		public static RLStepList FromRestList<T>(RestList<T> restList, Func <T, ENStepEntityRecord> converter) {
			RLStepList result = new RLStepList();
			result.InnerFromRestList(restList, converter);
			return result;
		}
		/// <summary>
		/// Default Constructor
		/// </summary>
		public RLStepList(): base() {
		}

		/// <summary>
		/// Constructor with transaction parameter
		/// </summary>
		/// <param name="trans"> IDbTransaction Parameter</param>
		[Obsolete("Use the Default Constructor and set the Transaction afterwards.")]
		public RLStepList(IDbTransaction trans): base(trans) {
		}

		/// <summary>
		/// Constructor with transaction parameter and alternate read method
		/// </summary>
		/// <param name="trans"> IDbTransaction Parameter</param>
		/// <param name="alternateReadDBMethod"> Alternate Read Method</param>
		[Obsolete("Use the Default Constructor and set the Transaction afterwards.")]
		public RLStepList(IDbTransaction trans, ReadDBMethodDelegate alternateReadDBMethod): this(trans) {
			this.alternateReadDBMethod = alternateReadDBMethod;
		}

		/// <summary>
		/// Constructor declaration for serialization
		/// </summary>
		/// <param name="info"> SerializationInfo</param>
		/// <param name="context"> StreamingContext</param>
		public RLStepList(SerializationInfo info, StreamingContext context): base(info, context) {
		}

		public override BitArray[] GetDefaultOptimizedValues() {
			BitArray[] def = new BitArray[0];
			return def;
		}
		/// <summary>
		/// Create as new list
		/// </summary>
		/// <returns>The new record list</returns>
		protected override OSList<ENStepEntityRecord> NewList() {
			return new RLStepList();
		}


	} // RLStepList
	public partial class ExtendedActions {
		/// <summary>
		/// Action: GetStep
		/// </summary>

		public static void GetStep(HeContext heContext, string inParamId, out RCStepRecord outParamRecord) {
			outParamRecord = new RCStepRecord(null);

			ObjectKey oldCurrentESpaceKey = heContext.CurrentESpaceKey;
			try {
				heContext.CurrentESpaceKey = ENStepEntity.eSpaceKey;
				try {
					outParamRecord = ENStepEntity.GetRecordById(inParamId);
				} catch {
					using(Transaction trans = DatabaseAccess.ForCurrentDatabase.GetRequestTransaction()) {
						string sql =
						 "SELECT " +
						 " [STEPS]" +
						 " FROM " + ENStepEntity.LocalViewName(heContext.AppInfo.Tenant.Id, BuiltInFunction.GetCurrentLocale()) + " " +
						 "WHERE [STEPS] = @inParamId";
						using(var sqlCmd = trans.CreateCommand(sql)) {
							sqlCmd.CreateParameter("@inParamId", DbType.String, inParamId);
							using(IDataReader reader = sqlCmd.ExecuteReader("Entity Action GetStep", true, false, false)) {
								if (reader.Read()) {
									outParamRecord.ReadDB(reader);
								} else {
									throw new DataBaseException(ENStepEntity.LocalViewName(heContext.AppInfo.Tenant.Id, BuiltInFunction.GetCurrentLocale()) + " with key " + inParamId + " was not found"); 
								}
							}
						}
					}
				}
			} finally {
				heContext.CurrentESpaceKey = oldCurrentESpaceKey;
			}
		}

	}
}

namespace ssBIFY {
	[XmlType("Step")]
	public class WORCStepRecord {
		[System.Xml.Serialization.XmlElement("Steps")]
		public string varWSSteps;

		public WORCStepRecord() {
			varWSSteps = (string) "";
		}

		public WORCStepRecord(ENStepEntityRecord r) {
			varWSSteps = BaseAppUtils.RemoveControlChars(r.ssSteps);
		}

		public static RLStepList ToRecordList(WORCStepRecord[] array) {
			RLStepList rl = new RLStepList();
			if (array != null) {
				foreach(WORCStepRecord val in array) {
					rl.Append(val);
				}
			}
			return rl;
		}

		public static WORCStepRecord[] FromRecordList(RLStepList rl) {
			WORCStepRecord[] array = new WORCStepRecord[rl == null ? 0: rl.Length];
			for (int i = 0; i < array.Length; i++) {
				array[i] = rl.Data[i];
			}
			return array;
		}
	}
}

namespace ssBIFY {
	partial struct RCStepRecord {
		public static implicit operator WORCStepRecord(RCStepRecord r) {
			return new WORCStepRecord(r.ssENStep);
		}

		public static implicit operator RCStepRecord(WORCStepRecord w) {
			RCStepRecord r = new RCStepRecord("");
			if (w != null) {
				r.ssENStep = w;
			}
			return r;
		}

	}

	partial struct ENStepEntityRecord {
		public static implicit operator WORCStepRecord(ENStepEntityRecord r) {
			return new WORCStepRecord(r);
		}

		public static implicit operator ENStepEntityRecord(WORCStepRecord w) {
			ENStepEntityRecord r = new ENStepEntityRecord("");
			if (w != null) {
				r.ssSteps = ((string) w.varWSSteps ?? "");
			}
			return r;
		}

	}
}


namespace ssBIFY {
	[Serializable()]
	public partial class WORLStepRecordList {
		public WORCStepRecord[] Array;


		public WORLStepRecordList(WORCStepRecord[] r) {
			if (r == null)
			Array = new WORCStepRecord[0];
			else
			Array = r;
		}
		public WORLStepRecordList() {
			Array = new WORCStepRecord[0];
		}

		public WORLStepRecordList(RLStepRecordList rl) {
			rl=(RLStepRecordList) rl.Duplicate();
			Array = new WORCStepRecord[rl.Length];
			while (!rl.Eof) {
				Array[rl.CurrentRowNumber] = new WORCStepRecord(rl.CurrentRec);
				rl.Advance();
			}
		}

	}
}

namespace ssBIFY {
	partial class RLStepRecordList {
		public static implicit operator RLStepRecordList(WORCStepRecord[] array) {
			RLStepRecordList rl = new RLStepRecordList();
			if (array != null) {
				foreach(WORCStepRecord val in array) {
					rl.Append((RCStepRecord) val);
				}
			}
			return rl;
		}
		public static implicit operator WORCStepRecord[](RLStepRecordList rl) {
			WORCStepRecord[] array = new WORCStepRecord[rl == null ? 0: rl.Length];
			for (int i = 0; i < array.Length; i++) {
				array[i] = (RCStepRecord) rl.Data[i];
			}
			return array;
		}
	}
}

namespace ssBIFY {
	partial class WORLStepRecordList {
		public static implicit operator RLStepRecordList(WORLStepRecordList w) {
			return w.Array;
		}
		public static implicit operator WORLStepRecordList(RLStepRecordList rl) {
			return new WORLStepRecordList(rl);
		}
		public static implicit operator WORCStepRecord[](WORLStepRecordList w) {
			if (w != null) {
				return w.Array;
			}
			return null;
		}
		public static implicit operator WORLStepRecordList(WORCStepRecord[] array) {
			return new WORLStepRecordList(array);
		}
	}
}

