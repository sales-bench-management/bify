﻿/* 
 This source code (the "Generated Software") is generated by the OutSystems Platform 
 and is licensed by OutSystems (http://www.outsystems.com) to You solely for testing and evaluation 
 purposes, unless You and OutSystems have executed a specific agreement covering the use terms and 
 conditions of the Generated Software, in which case such agreement shall apply. 
*/

using System;
using System.Data;
using System.Runtime.Serialization;
using System.Reflection;
using System.Xml;
using OutSystems.ObjectKeys;
using OutSystems.HubEdition.RuntimePlatform;
using OutSystems.HubEdition.RuntimePlatform.Db;
using OutSystems.Internal.Db;
using System.Xml.Serialization;
using System.Collections;
using System.Collections.Generic;
using OutSystems.RuntimeCommon;
using OutSystems.HubEdition.RuntimePlatform.NewRuntime;

namespace ssBIFY {

	public sealed partial class ENSample_TransactionTypeEntity {
		public static string LocalViewName(int? tenant, string locale) {
			return ViewName(null, locale);
		}
		public static string ViewName(int? tenant, string locale) {
			return RsseSpaceOutSystemsSampleDataDB.ENSample_TransactionTypeEntity.ViewName(tenant, locale);
		}
		public static readonly ObjectKey eSpaceKey = RsseSpaceOutSystemsSampleDataDB.ENSample_TransactionTypeEntity.eSpaceKey;
		public static System.Collections.Generic.Dictionary<string, string> AttributesToDatabaseNamesMap() {
			return new System.Collections.Generic.Dictionary<string, string>() {
				{ "id", "Id"
				}
				, { "label", "Label"
				}
			};
		}
	} // ENSample_TransactionTypeEntity

	/// <summary>
	/// Entity <code>ENSample_TransactionTypeEntityRecord</code> that represents the Service Studio entity
	///  <code>Sample_TransactionType</code> <p> Description: </p>
	/// </summary>
	[OutSystems.HubEdition.RuntimePlatform.MetaInformation.EntityRecordDetails("Sample_TransactionType", "pGttnAXblEiSPOYOu_NIVg", "N5WnRxvbq02_ujVPlZNa0A", 0, "OSUSR_mle_Sample_TransactionType", "", false)]
	[Serializable()]
	public partial struct ENSample_TransactionTypeEntityRecord: ISerializable, ITypedRecord<ENSample_TransactionTypeEntityRecord>, ISimpleRecord {
		internal static readonly GlobalObjectKey IdId = GlobalObjectKey.Parse("N5WnRxvbq02_ujVPlZNa0A*E5EIfKiCSkiE5zpBkiHMGw");
		internal static readonly GlobalObjectKey IdLabel = GlobalObjectKey.Parse("N5WnRxvbq02_ujVPlZNa0A*aqZ+pKEb10uRjOQJZJaf+A");

		public static void EnsureInitialized() {}
		[OutSystems.HubEdition.RuntimePlatform.MetaInformation.EntityAttributeDetails("ID", 0, true, true, false, true)]
		[System.Xml.Serialization.XmlElement("Id")]
		private int _ssId;
		public int ssId {
			get {
				return _ssId;
			}
			set {
				if ((_ssId!=value) || OptimizedAttributes[0]) {
					ChangedAttributes = new BitArray(2, true);
					_ssId = value;
				}
			}
		}

		[OutSystems.HubEdition.RuntimePlatform.MetaInformation.EntityAttributeDetails("LABEL", 50, false, false, false, true)]
		[System.Xml.Serialization.XmlElement("Label")]
		private string _ssLabel;
		public string ssLabel {
			get {
				return _ssLabel;
			}
			set {
				if ((_ssLabel!=value) || OptimizedAttributes[1]) {
					ChangedAttributes[1] = true;
					_ssLabel = value;
				}
			}
		}


		public BitArray ChangedAttributes;

		public BitArray OptimizedAttributes;

		public ENSample_TransactionTypeEntityRecord(params string[] dummy) {
			ChangedAttributes = new BitArray(2, true);
			OptimizedAttributes = new BitArray(2, false);
			_ssId = 0;
			_ssLabel = "";
		}

		public BitArray[] GetDefaultOptimizedValues() {
			BitArray[] all = new BitArray[0];
			return all;
		}

		public BitArray[] AllOptimizedAttributes {
			set {
				if (value == null) {
				} else {
				}
			}
			get {
				BitArray[] all = new BitArray[0];
				return all;
			}
		}

		/// <summary>
		/// Read a record from database
		/// </summary>
		/// <param name="r"> Data base reader</param>
		/// <param name="index"> index</param>
		public void Read(IDataReader r, ref int index) {
			ssId = r.ReadInteger(index++, "Sample_TransactionType.Id", 0);
			ssLabel = r.ReadText(index++, "Sample_TransactionType.Label", "");
			ChangedAttributes = new BitArray(2, false);
			OptimizedAttributes = new BitArray(2, false);
		}
		/// <summary>
		/// Read from database
		/// </summary>
		/// <param name="r"> Data reader</param>
		public void ReadDB(IDataReader r) {
			int index = 0;
			Read(r, ref index);
		}

		/// <summary>
		/// Read from record
		/// </summary>
		/// <param name="r"> Record</param>
		public void ReadIM(ENSample_TransactionTypeEntityRecord r) {
			this = r;
		}


		public static bool operator == (ENSample_TransactionTypeEntityRecord a, ENSample_TransactionTypeEntityRecord b) {
			if (a.ssId != b.ssId) return false;
			if (a.ssLabel != b.ssLabel) return false;
			return true;
		}

		public static bool operator != (ENSample_TransactionTypeEntityRecord a, ENSample_TransactionTypeEntityRecord b) {
			return !(a==b);
		}

		public override bool Equals(object o) {
			if (o.GetType() != typeof(ENSample_TransactionTypeEntityRecord)) return false;
			return (this == (ENSample_TransactionTypeEntityRecord) o);
		}

		public override int GetHashCode() {
			try {
				return base.GetHashCode()
				^ ssId.GetHashCode()
				^ ssLabel.GetHashCode()
				;
			} catch {
				return base.GetHashCode();
			}
		}

		public void GetObjectData(SerializationInfo info, StreamingContext context) {
			Type objInfo = this.GetType();
			FieldInfo[] fields;
			fields = objInfo.GetFields(BindingFlags.NonPublic | BindingFlags.Instance | BindingFlags.Public);
			for (int i = 0; i < fields.Length; i++)
			if (fields[i] .FieldType.IsSerializable)
			info.AddValue(fields[i] .Name, fields[i] .GetValue(this));
		}

		public ENSample_TransactionTypeEntityRecord(SerializationInfo info, StreamingContext context) {
			ChangedAttributes = new BitArray(2, true);
			OptimizedAttributes = new BitArray(2, false);
			_ssId = 0;
			_ssLabel = "";
			Type objInfo = this.GetType();
			FieldInfo fieldInfo = null;
			fieldInfo = objInfo.GetField("_ssId", BindingFlags.NonPublic | BindingFlags.Instance | BindingFlags.Public);
			if (fieldInfo == null) {
				throw new Exception("The field named '_ssId' was not found.");
			}
			if (fieldInfo.FieldType.IsSerializable) {
				_ssId = (int) info.GetValue(fieldInfo.Name, fieldInfo.FieldType);
			}
			fieldInfo = objInfo.GetField("_ssLabel", BindingFlags.NonPublic | BindingFlags.Instance | BindingFlags.Public);
			if (fieldInfo == null) {
				throw new Exception("The field named '_ssLabel' was not found.");
			}
			if (fieldInfo.FieldType.IsSerializable) {
				_ssLabel = (string) info.GetValue(fieldInfo.Name, fieldInfo.FieldType);
			}
		}

		public void RecursiveReset() {
		}

		public void InternalRecursiveSave() {
		}


		public ENSample_TransactionTypeEntityRecord Duplicate() {
			ENSample_TransactionTypeEntityRecord t;
			t._ssId = this._ssId;
			t._ssLabel = this._ssLabel;
			t.ChangedAttributes = new BitArray(2);
			t.OptimizedAttributes = new BitArray(2);
			for (int i = 0; i < 2; i++) {
				t.ChangedAttributes[i] = ChangedAttributes[i];
				t.OptimizedAttributes[i] = OptimizedAttributes[i];
			}
			return t;
		}

		IRecord IRecord.Duplicate() {
			return Duplicate();
		}

		public void ToXml(Object parent, System.Xml.XmlElement baseElem, String fieldName, int detailLevel) {
			System.Xml.XmlElement recordElem = VarValue.AppendChild(baseElem, "Entity");
			if (fieldName != null) {
				VarValue.AppendAttribute(recordElem, "debug.field", fieldName);
				fieldName = fieldName.ToLowerInvariant();
			}
			if (detailLevel > 0) {
				if (!VarValue.FieldIsOptimized(parent, fieldName + ".Id")) VarValue.AppendAttribute(recordElem, "Id", ssId, detailLevel, TypeKind.Integer); else VarValue.AppendOptimizedAttribute(recordElem, "Id");
				if (!VarValue.FieldIsOptimized(parent, fieldName + ".Label")) VarValue.AppendAttribute(recordElem, "Label", ssLabel, detailLevel, TypeKind.Text); else VarValue.AppendOptimizedAttribute(recordElem, "Label");
			} else {
				VarValue.AppendDeferredEvaluationElement(recordElem);
			}
		}

		public void EvaluateFields(VarValue variable, Object parent, String baseName, String fields) {
			String head = VarValue.GetHead(fields);
			String tail = VarValue.GetTail(fields);
			variable.Found = false;
			if (head == "id") {
				if (!VarValue.FieldIsOptimized(parent, baseName + ".Id")) variable.Value = ssId; else variable.Optimized = true;
			} else if (head == "label") {
				if (!VarValue.FieldIsOptimized(parent, baseName + ".Label")) variable.Value = ssLabel; else variable.Optimized = true;
			}
			if (variable.Found && tail != null) variable.EvaluateFields(this, head, tail);
		}

		public bool ChangedAttributeGet(GlobalObjectKey key) {
			if (key.Equals(IdId)) {
				return ChangedAttributes[0];
			} else if (key.Equals(IdLabel)) {
				return ChangedAttributes[1];
			} else {
				throw new Exception("Invalid key");
			}
		}

		public bool OptimizedAttributeGet(GlobalObjectKey key) {
			if (key.Equals(IdId)) {
				return OptimizedAttributes[0];
			} else if (key.Equals(IdLabel)) {
				return OptimizedAttributes[1];
			} else {
				throw new Exception("Invalid key");
			}
		}

		public object AttributeGet(GlobalObjectKey key) {
			if (key == IdId) {
				return ssId;
			} else if (key == IdLabel) {
				return ssLabel;
			} else {
				throw new Exception("Invalid key");
			}
		}
		public void FillFromOther(IRecord other) {
			ChangedAttributes = new BitArray(2);
			OptimizedAttributes = new BitArray(2);
			if (other == null) return;
			ssId = (int) other.AttributeGet(IdId);
			ChangedAttributes[0] = other.ChangedAttributeGet(IdId);
			OptimizedAttributes[0] = other.OptimizedAttributeGet(IdId);
			ssLabel = (string) other.AttributeGet(IdLabel);
			ChangedAttributes[1] = other.ChangedAttributeGet(IdLabel);
			OptimizedAttributes[1] = other.OptimizedAttributeGet(IdLabel);
		}
	} // ENSample_TransactionTypeEntityRecord
	/// <summary>
	/// Static Entity <code>ENSample_TransactionTypeEntity</code> gets the values for this static entity
	///  records in runtime <code>Sample_TransactionType</code>
	/// </summary>
	partial class ENSample_TransactionTypeEntity {


		static ENSample_TransactionTypeEntity() {
			ENSample_TransactionTypeEntityRecordTypeFactoryImpl.InitializeFactory();
		}

		public static ENSample_TransactionTypeEntityRecord GetRecordById(int id) {
			return (ENSample_TransactionTypeEntityRecord) RsseSpaceOutSystemsSampleDataDB.ENSample_TransactionTypeEntity.GetRecordById(id);
		}

		public static ENSample_TransactionTypeEntityRecord GetRecordByName(string name) {
			return (ENSample_TransactionTypeEntityRecord) RsseSpaceOutSystemsSampleDataDB.ENSample_TransactionTypeEntity.GetRecordByName(name);
		}

		public static ENSample_TransactionTypeEntityRecord GetRecordByKey(ObjectKey key) {
			return (ENSample_TransactionTypeEntityRecord) RsseSpaceOutSystemsSampleDataDB.ENSample_TransactionTypeEntity.GetRecordByKey(key);
		}
	} // ENSample_TransactionTypeEntity;
	/// <summary>
	/// Structure <code>RCSample_TransactionTypeRecord</code>
	/// </summary>
	[Serializable()]
	public partial struct RCSample_TransactionTypeRecord: ISerializable, ITypedRecord<RCSample_TransactionTypeRecord> {
		internal static readonly GlobalObjectKey IdSample_TransactionType = GlobalObjectKey.Parse("2UmDmepsh0WSfJ_D1JexCA*4yU83HLCVI_qZ6QL3Wd3eA");

		public static void EnsureInitialized() {}
		[System.Xml.Serialization.XmlElement("Sample_TransactionType")]
		public ENSample_TransactionTypeEntityRecord ssENSample_TransactionType;


		public static implicit operator ENSample_TransactionTypeEntityRecord(RCSample_TransactionTypeRecord r) {
			return r.ssENSample_TransactionType;
		}

		public static implicit operator RCSample_TransactionTypeRecord(ENSample_TransactionTypeEntityRecord r) {
			RCSample_TransactionTypeRecord res = new RCSample_TransactionTypeRecord(null);
			res.ssENSample_TransactionType = r;
			return res;
		}

		public BitArray ChangedAttributes {
			set {
				ssENSample_TransactionType.ChangedAttributes = value;
			}
			get {
				return ssENSample_TransactionType.ChangedAttributes;
			}
		}
		public BitArray OptimizedAttributes;

		public RCSample_TransactionTypeRecord(params string[] dummy) {
			OptimizedAttributes = null;
			ssENSample_TransactionType = new ENSample_TransactionTypeEntityRecord(null);
		}

		public BitArray[] GetDefaultOptimizedValues() {
			BitArray[] all = new BitArray[1];
			all[0] = new BitArray(2, false);
			return all;
		}

		public BitArray[] AllOptimizedAttributes {
			set {
				if (value == null) {
					ssENSample_TransactionType.OptimizedAttributes = GetDefaultOptimizedValues()[0];
				} else {
					ssENSample_TransactionType.OptimizedAttributes = value[0];
				}
			}
			get {
				BitArray[] all = new BitArray[1];
				all[0] = ssENSample_TransactionType.OptimizedAttributes;
				return all;
			}
		}

		/// <summary>
		/// Read a record from database
		/// </summary>
		/// <param name="r"> Data base reader</param>
		/// <param name="index"> index</param>
		public void Read(IDataReader r, ref int index) {
			ssENSample_TransactionType.Read(r, ref index);
		}
		/// <summary>
		/// Read from database
		/// </summary>
		/// <param name="r"> Data reader</param>
		public void ReadDB(IDataReader r) {
			int index = 0;
			Read(r, ref index);
		}

		/// <summary>
		/// Read from record
		/// </summary>
		/// <param name="r"> Record</param>
		public void ReadIM(RCSample_TransactionTypeRecord r) {
			this = r;
		}


		public static bool operator == (RCSample_TransactionTypeRecord a, RCSample_TransactionTypeRecord b) {
			if (a.ssENSample_TransactionType != b.ssENSample_TransactionType) return false;
			return true;
		}

		public static bool operator != (RCSample_TransactionTypeRecord a, RCSample_TransactionTypeRecord b) {
			return !(a==b);
		}

		public override bool Equals(object o) {
			if (o.GetType() != typeof(RCSample_TransactionTypeRecord)) return false;
			return (this == (RCSample_TransactionTypeRecord) o);
		}

		public override int GetHashCode() {
			try {
				return base.GetHashCode()
				^ ssENSample_TransactionType.GetHashCode()
				;
			} catch {
				return base.GetHashCode();
			}
		}

		public void GetObjectData(SerializationInfo info, StreamingContext context) {
			Type objInfo = this.GetType();
			FieldInfo[] fields;
			fields = objInfo.GetFields(BindingFlags.NonPublic | BindingFlags.Instance | BindingFlags.Public);
			for (int i = 0; i < fields.Length; i++)
			if (fields[i] .FieldType.IsSerializable)
			info.AddValue(fields[i] .Name, fields[i] .GetValue(this));
		}

		public RCSample_TransactionTypeRecord(SerializationInfo info, StreamingContext context) {
			OptimizedAttributes = null;
			ssENSample_TransactionType = new ENSample_TransactionTypeEntityRecord(null);
			Type objInfo = this.GetType();
			FieldInfo fieldInfo = null;
			fieldInfo = objInfo.GetField("ssENSample_TransactionType", BindingFlags.NonPublic | BindingFlags.Instance | BindingFlags.Public);
			if (fieldInfo == null) {
				throw new Exception("The field named 'ssENSample_TransactionType' was not found.");
			}
			if (fieldInfo.FieldType.IsSerializable) {
				ssENSample_TransactionType = (ENSample_TransactionTypeEntityRecord) info.GetValue(fieldInfo.Name, fieldInfo.FieldType);
			}
		}

		public void RecursiveReset() {
			ssENSample_TransactionType.RecursiveReset();
		}

		public void InternalRecursiveSave() {
			ssENSample_TransactionType.InternalRecursiveSave();
		}


		public RCSample_TransactionTypeRecord Duplicate() {
			RCSample_TransactionTypeRecord t;
			t.ssENSample_TransactionType = (ENSample_TransactionTypeEntityRecord) this.ssENSample_TransactionType.Duplicate();
			t.OptimizedAttributes = null;
			return t;
		}

		IRecord IRecord.Duplicate() {
			return Duplicate();
		}

		public void ToXml(Object parent, System.Xml.XmlElement baseElem, String fieldName, int detailLevel) {
			System.Xml.XmlElement recordElem = VarValue.AppendChild(baseElem, "Record");
			if (fieldName != null) {
				VarValue.AppendAttribute(recordElem, "debug.field", fieldName);
			}
			if (detailLevel > 0) {
				ssENSample_TransactionType.ToXml(this, recordElem, "Sample_TransactionType", detailLevel - 1);
			} else {
				VarValue.AppendDeferredEvaluationElement(recordElem);
			}
		}

		public void EvaluateFields(VarValue variable, Object parent, String baseName, String fields) {
			String head = VarValue.GetHead(fields);
			String tail = VarValue.GetTail(fields);
			variable.Found = false;
			if (head == "sample_transactiontype") {
				if (!VarValue.FieldIsOptimized(parent, baseName + ".Sample_TransactionType")) variable.Value = ssENSample_TransactionType; else variable.Optimized = true;
				variable.SetFieldName("sample_transactiontype");
			}
			if (variable.Found && tail != null) variable.EvaluateFields(this, head, tail);
		}

		public bool ChangedAttributeGet(GlobalObjectKey key) {
			return ssENSample_TransactionType.ChangedAttributeGet(key);
		}

		public bool OptimizedAttributeGet(GlobalObjectKey key) {
			return ssENSample_TransactionType.OptimizedAttributeGet(key);
		}

		public object AttributeGet(GlobalObjectKey key) {
			if (key == IdSample_TransactionType) {
				return ssENSample_TransactionType;
			} else {
				throw new Exception("Invalid key");
			}
		}
		public void FillFromOther(IRecord other) {
			if (other == null) return;
			ssENSample_TransactionType.FillFromOther((IRecord) other.AttributeGet(IdSample_TransactionType));
		}
	} // RCSample_TransactionTypeRecord
	/// <summary>
	/// RecordList type <code>RLSample_TransactionTypeRecordList</code> that represents a record list of
	///  <code>Sample_TransactionType</code>
	/// </summary>
	[Serializable()]
	public partial class RLSample_TransactionTypeRecordList: GenericRecordList<RCSample_TransactionTypeRecord>, IEnumerable, IEnumerator, ISerializable {
		public static void EnsureInitialized() {}

		protected override RCSample_TransactionTypeRecord GetElementDefaultValue() {
			return new RCSample_TransactionTypeRecord("");
		}

		public T[] ToArray<T>(Func<RCSample_TransactionTypeRecord, T> converter) {
			return ToArray(this, converter);
		}

		public static T[] ToArray<T>(RLSample_TransactionTypeRecordList recordlist, Func<RCSample_TransactionTypeRecord, T> converter) {
			return InnerToArray(recordlist, converter);
		}
		public static implicit operator RLSample_TransactionTypeRecordList(RCSample_TransactionTypeRecord[] array) {
			RLSample_TransactionTypeRecordList result = new RLSample_TransactionTypeRecordList();
			result.InnerFromArray(array);
			return result;
		}

		public static RLSample_TransactionTypeRecordList ToList<T>(T[] array, Func <T, RCSample_TransactionTypeRecord> converter) {
			RLSample_TransactionTypeRecordList result = new RLSample_TransactionTypeRecordList();
			result.InnerFromArray(array, converter);
			return result;
		}

		public static RLSample_TransactionTypeRecordList FromRestList<T>(RestList<T> restList, Func <T, RCSample_TransactionTypeRecord> converter) {
			RLSample_TransactionTypeRecordList result = new RLSample_TransactionTypeRecordList();
			result.InnerFromRestList(restList, converter);
			return result;
		}
		/// <summary>
		/// Default Constructor
		/// </summary>
		public RLSample_TransactionTypeRecordList(): base() {
		}

		/// <summary>
		/// Constructor with transaction parameter
		/// </summary>
		/// <param name="trans"> IDbTransaction Parameter</param>
		[Obsolete("Use the Default Constructor and set the Transaction afterwards.")]
		public RLSample_TransactionTypeRecordList(IDbTransaction trans): base(trans) {
		}

		/// <summary>
		/// Constructor with transaction parameter and alternate read method
		/// </summary>
		/// <param name="trans"> IDbTransaction Parameter</param>
		/// <param name="alternateReadDBMethod"> Alternate Read Method</param>
		[Obsolete("Use the Default Constructor and set the Transaction afterwards.")]
		public RLSample_TransactionTypeRecordList(IDbTransaction trans, ReadDBMethodDelegate alternateReadDBMethod): this(trans) {
			this.alternateReadDBMethod = alternateReadDBMethod;
		}

		/// <summary>
		/// Constructor declaration for serialization
		/// </summary>
		/// <param name="info"> SerializationInfo</param>
		/// <param name="context"> StreamingContext</param>
		public RLSample_TransactionTypeRecordList(SerializationInfo info, StreamingContext context): base(info, context) {
		}

		public override BitArray[] GetDefaultOptimizedValues() {
			BitArray[] def = new BitArray[1];
			def[0] = new BitArray(2, false);
			return def;
		}
		/// <summary>
		/// Create as new list
		/// </summary>
		/// <returns>The new record list</returns>
		protected override OSList<RCSample_TransactionTypeRecord> NewList() {
			return new RLSample_TransactionTypeRecordList();
		}


	} // RLSample_TransactionTypeRecordList

	/// <summary>
	/// RecordList type <code>RLSample_TransactionTypeList</code> that represents a record list of
	///  <code>Sample_TransactionType</code>
	/// </summary>
	[Serializable()]
	public partial class RLSample_TransactionTypeList: GenericRecordList<ENSample_TransactionTypeEntityRecord>, IEnumerable, IEnumerator, ISerializable {
		public static void EnsureInitialized() {}

		protected override ENSample_TransactionTypeEntityRecord GetElementDefaultValue() {
			return new ENSample_TransactionTypeEntityRecord("");
		}

		public T[] ToArray<T>(Func<ENSample_TransactionTypeEntityRecord, T> converter) {
			return ToArray(this, converter);
		}

		public static T[] ToArray<T>(RLSample_TransactionTypeList recordlist, Func<ENSample_TransactionTypeEntityRecord, T> converter) {
			return InnerToArray(recordlist, converter);
		}
		public static implicit operator RLSample_TransactionTypeList(ENSample_TransactionTypeEntityRecord[] array) {
			RLSample_TransactionTypeList result = new RLSample_TransactionTypeList();
			result.InnerFromArray(array);
			return result;
		}

		public static RLSample_TransactionTypeList ToList<T>(T[] array, Func <T, ENSample_TransactionTypeEntityRecord> converter) {
			RLSample_TransactionTypeList result = new RLSample_TransactionTypeList();
			result.InnerFromArray(array, converter);
			return result;
		}

		public static RLSample_TransactionTypeList FromRestList<T>(RestList<T> restList, Func <T, ENSample_TransactionTypeEntityRecord> converter) {
			RLSample_TransactionTypeList result = new RLSample_TransactionTypeList();
			result.InnerFromRestList(restList, converter);
			return result;
		}
		/// <summary>
		/// Default Constructor
		/// </summary>
		public RLSample_TransactionTypeList(): base() {
		}

		/// <summary>
		/// Constructor with transaction parameter
		/// </summary>
		/// <param name="trans"> IDbTransaction Parameter</param>
		[Obsolete("Use the Default Constructor and set the Transaction afterwards.")]
		public RLSample_TransactionTypeList(IDbTransaction trans): base(trans) {
		}

		/// <summary>
		/// Constructor with transaction parameter and alternate read method
		/// </summary>
		/// <param name="trans"> IDbTransaction Parameter</param>
		/// <param name="alternateReadDBMethod"> Alternate Read Method</param>
		[Obsolete("Use the Default Constructor and set the Transaction afterwards.")]
		public RLSample_TransactionTypeList(IDbTransaction trans, ReadDBMethodDelegate alternateReadDBMethod): this(trans) {
			this.alternateReadDBMethod = alternateReadDBMethod;
		}

		/// <summary>
		/// Constructor declaration for serialization
		/// </summary>
		/// <param name="info"> SerializationInfo</param>
		/// <param name="context"> StreamingContext</param>
		public RLSample_TransactionTypeList(SerializationInfo info, StreamingContext context): base(info, context) {
		}

		public override BitArray[] GetDefaultOptimizedValues() {
			BitArray[] def = new BitArray[0];
			return def;
		}
		/// <summary>
		/// Create as new list
		/// </summary>
		/// <returns>The new record list</returns>
		protected override OSList<ENSample_TransactionTypeEntityRecord> NewList() {
			return new RLSample_TransactionTypeList();
		}


	} // RLSample_TransactionTypeList
	public partial class ExtendedActions {
		/// <summary>
		/// Action: GetSample_TransactionType
		/// </summary>

		public static void GetSample_TransactionType(HeContext heContext, int inParamId, out RCSample_TransactionTypeRecord outParamRecord) {
			outParamRecord = new RCSample_TransactionTypeRecord(null);

			ObjectKey oldCurrentESpaceKey = heContext.CurrentESpaceKey;
			try {
				heContext.CurrentESpaceKey = ENSample_TransactionTypeEntity.eSpaceKey;
				try {
					outParamRecord = ENSample_TransactionTypeEntity.GetRecordById(inParamId);
				} catch {
					using(Transaction trans = DatabaseAccess.ForCurrentDatabase.GetRequestTransaction()) {
						string sql =
						 "SELECT " +
						 " [ID]" +
						 ", [LABEL]" +
						 " FROM " + ENSample_TransactionTypeEntity.LocalViewName(heContext.AppInfo.Tenant.Id, BuiltInFunction.GetCurrentLocale()) + " " +
						 "WHERE [ID] = @inParamId";
						using(var sqlCmd = trans.CreateCommand(sql)) {
							sqlCmd.CreateParameter("@inParamId", DbType.Int32, inParamId);
							using(IDataReader reader = sqlCmd.ExecuteReader("Entity Action GetSample_TransactionType", true, false, false)) {
								if (reader.Read()) {
									outParamRecord.ReadDB(reader);
								} else {
									throw new DataBaseException(ENSample_TransactionTypeEntity.LocalViewName(heContext.AppInfo.Tenant.Id, BuiltInFunction.GetCurrentLocale()) + " with key " + inParamId + " was not found"); 
								}
							}
						}
					}
				}
			} finally {
				heContext.CurrentESpaceKey = oldCurrentESpaceKey;
			}
		}

	}
}

namespace ssBIFY {
	[XmlType("Sample_TransactionType")]
	public class WORCSample_TransactionTypeRecord {
		[System.Xml.Serialization.XmlElement("Id")]
		public int varWSId;

		[System.Xml.Serialization.XmlElement("Label")]
		public string varWSLabel;

		public WORCSample_TransactionTypeRecord() {
			varWSId = (int) 0;
			varWSLabel = (string) "";
		}

		public WORCSample_TransactionTypeRecord(ENSample_TransactionTypeEntityRecord r) {
			varWSId = (int) r.ssId;
			varWSLabel = BaseAppUtils.RemoveControlChars(r.ssLabel);
		}

		public static RLSample_TransactionTypeList ToRecordList(WORCSample_TransactionTypeRecord[] array) {
			RLSample_TransactionTypeList rl = new RLSample_TransactionTypeList();
			if (array != null) {
				foreach(WORCSample_TransactionTypeRecord val in array) {
					rl.Append(val);
				}
			}
			return rl;
		}

		public static WORCSample_TransactionTypeRecord[] FromRecordList(RLSample_TransactionTypeList rl) {
			WORCSample_TransactionTypeRecord[] array = new WORCSample_TransactionTypeRecord[rl == null ? 0: rl.Length];
			for (int i = 0; i < array.Length; i++) {
				array[i] = rl.Data[i];
			}
			return array;
		}
	}
}

namespace ssBIFY {
	partial struct RCSample_TransactionTypeRecord {
		public static implicit operator WORCSample_TransactionTypeRecord(RCSample_TransactionTypeRecord r) {
			return new WORCSample_TransactionTypeRecord(r.ssENSample_TransactionType);
		}

		public static implicit operator RCSample_TransactionTypeRecord(WORCSample_TransactionTypeRecord w) {
			RCSample_TransactionTypeRecord r = new RCSample_TransactionTypeRecord("");
			if (w != null) {
				r.ssENSample_TransactionType = w;
			}
			return r;
		}

	}

	partial struct ENSample_TransactionTypeEntityRecord {
		public static implicit operator WORCSample_TransactionTypeRecord(ENSample_TransactionTypeEntityRecord r) {
			return new WORCSample_TransactionTypeRecord(r);
		}

		public static implicit operator ENSample_TransactionTypeEntityRecord(WORCSample_TransactionTypeRecord w) {
			ENSample_TransactionTypeEntityRecord r = new ENSample_TransactionTypeEntityRecord("");
			if (w != null) {
				r.ssId = (int) w.varWSId;
				r.ssLabel = ((string) w.varWSLabel ?? "");
			}
			return r;
		}

	}
}


namespace ssBIFY {
	[Serializable()]
	public partial class WORLSample_TransactionTypeRecordList {
		public WORCSample_TransactionTypeRecord[] Array;


		public WORLSample_TransactionTypeRecordList(WORCSample_TransactionTypeRecord[] r) {
			if (r == null)
			Array = new WORCSample_TransactionTypeRecord[0];
			else
			Array = r;
		}
		public WORLSample_TransactionTypeRecordList() {
			Array = new WORCSample_TransactionTypeRecord[0];
		}

		public WORLSample_TransactionTypeRecordList(RLSample_TransactionTypeRecordList rl) {
			rl=(RLSample_TransactionTypeRecordList) rl.Duplicate();
			Array = new WORCSample_TransactionTypeRecord[rl.Length];
			while (!rl.Eof) {
				Array[rl.CurrentRowNumber] = new WORCSample_TransactionTypeRecord(rl.CurrentRec);
				rl.Advance();
			}
		}

	}
}

namespace ssBIFY {
	partial class RLSample_TransactionTypeRecordList {
		public static implicit operator RLSample_TransactionTypeRecordList(WORCSample_TransactionTypeRecord[] array) {
			RLSample_TransactionTypeRecordList rl = new RLSample_TransactionTypeRecordList();
			if (array != null) {
				foreach(WORCSample_TransactionTypeRecord val in array) {
					rl.Append((RCSample_TransactionTypeRecord) val);
				}
			}
			return rl;
		}
		public static implicit operator WORCSample_TransactionTypeRecord[](RLSample_TransactionTypeRecordList rl) {
			WORCSample_TransactionTypeRecord[] array = new WORCSample_TransactionTypeRecord[rl == null ? 0: rl.Length];
			for (int i = 0; i < array.Length; i++) {
				array[i] = (RCSample_TransactionTypeRecord) rl.Data[i];
			}
			return array;
		}
	}
}

namespace ssBIFY {
	partial class WORLSample_TransactionTypeRecordList {
		public static implicit operator RLSample_TransactionTypeRecordList(WORLSample_TransactionTypeRecordList w) {
			return w.Array;
		}
		public static implicit operator WORLSample_TransactionTypeRecordList(RLSample_TransactionTypeRecordList rl) {
			return new WORLSample_TransactionTypeRecordList(rl);
		}
		public static implicit operator WORCSample_TransactionTypeRecord[](WORLSample_TransactionTypeRecordList w) {
			if (w != null) {
				return w.Array;
			}
			return null;
		}
		public static implicit operator WORLSample_TransactionTypeRecordList(WORCSample_TransactionTypeRecord[] array) {
			return new WORLSample_TransactionTypeRecordList(array);
		}
	}
}

