﻿/* 
 This source code (the "Generated Software") is generated by the OutSystems Platform 
 and is licensed by OutSystems (http://www.outsystems.com) to You solely for testing and evaluation 
 purposes, unless You and OutSystems have executed a specific agreement covering the use terms and 
 conditions of the Generated Software, in which case such agreement shall apply. 
*/

using System;
using System.Data;
using System.Runtime.Serialization;
using System.Reflection;
using System.Xml;
using OutSystems.ObjectKeys;
using OutSystems.HubEdition.RuntimePlatform;
using OutSystems.HubEdition.RuntimePlatform.Db;
using OutSystems.Internal.Db;
using System.Xml.Serialization;
using System.Collections;
using System.Collections.Generic;
using OutSystems.RuntimeCommon;
using OutSystems.HubEdition.RuntimePlatform.NewRuntime;

namespace ssBIFY {

	public sealed partial class ENResponsiveTableRecordsEntity {
		public static string LocalViewName(int? tenant, string locale) {
			return ViewName(null, locale);
		}
		public static string ViewName(int? tenant, string locale) {
			return RsseSpaceOutSystemsUIWeb.ENResponsiveTableRecordsEntity.ViewName(tenant, locale);
		}
		public static readonly ObjectKey eSpaceKey = RsseSpaceOutSystemsUIWeb.ENResponsiveTableRecordsEntity.eSpaceKey;
		public static System.Collections.Generic.Dictionary<string, string> AttributesToDatabaseNamesMap() {
			return new System.Collections.Generic.Dictionary<string, string>() {
				{ "responsivetablerecords", "ResponsiveTableRecords"
				}
			};
		}
	} // ENResponsiveTableRecordsEntity

	/// <summary>
	/// Entity <code>ENResponsiveTableRecordsEntityRecord</code> that represents the Service Studio entity
	///  <code>ResponsiveTableRecords</code> <p> Description: Different options for responsive for th
	/// e responsive table.</p>
	/// </summary>
	[OutSystems.HubEdition.RuntimePlatform.MetaInformation.EntityRecordDetails("ResponsiveTableRecords", "eJrNEETLa0KQQkv4XLNM5w", "ftsuvongDUa8WwDHf790vg", 0, "OSUSR_41o_ResponsiveTableRecords", "", false)]
	[Serializable()]
	public partial struct ENResponsiveTableRecordsEntityRecord: ISerializable, ITypedRecord<ENResponsiveTableRecordsEntityRecord>, ISimpleRecord {
		internal static readonly GlobalObjectKey IdResponsiveTableRecords = GlobalObjectKey.Parse("ftsuvongDUa8WwDHf790vg*TmSVil5TDEOM0YKLu9naXA");

		public static void EnsureInitialized() {}
		[OutSystems.HubEdition.RuntimePlatform.MetaInformation.EntityAttributeDetails("RESPONSIVETABLERECORDS", 50, false, true, false, true)]
		[System.Xml.Serialization.XmlElement("ResponsiveTableRecords")]
		private string _ssResponsiveTableRecords;
		public string ssResponsiveTableRecords {
			get {
				return _ssResponsiveTableRecords;
			}
			set {
				if ((_ssResponsiveTableRecords!=value) || OptimizedAttributes[0]) {
					ChangedAttributes = new BitArray(1, true);
					_ssResponsiveTableRecords = value;
				}
			}
		}


		public BitArray ChangedAttributes;

		public BitArray OptimizedAttributes;

		public ENResponsiveTableRecordsEntityRecord(params string[] dummy) {
			ChangedAttributes = new BitArray(1, true);
			OptimizedAttributes = new BitArray(1, false);
			_ssResponsiveTableRecords = "";
		}

		public BitArray[] GetDefaultOptimizedValues() {
			BitArray[] all = new BitArray[0];
			return all;
		}

		public BitArray[] AllOptimizedAttributes {
			set {
				if (value == null) {
				} else {
				}
			}
			get {
				BitArray[] all = new BitArray[0];
				return all;
			}
		}

		/// <summary>
		/// Read a record from database
		/// </summary>
		/// <param name="r"> Data base reader</param>
		/// <param name="index"> index</param>
		public void Read(IDataReader r, ref int index) {
			ssResponsiveTableRecords = r.ReadText(index++, "ResponsiveTableRecords.ResponsiveTableRecords", "");
			ChangedAttributes = new BitArray(1, false);
			OptimizedAttributes = new BitArray(1, false);
		}
		/// <summary>
		/// Read from database
		/// </summary>
		/// <param name="r"> Data reader</param>
		public void ReadDB(IDataReader r) {
			int index = 0;
			Read(r, ref index);
		}

		/// <summary>
		/// Read from record
		/// </summary>
		/// <param name="r"> Record</param>
		public void ReadIM(ENResponsiveTableRecordsEntityRecord r) {
			this = r;
		}


		public static bool operator == (ENResponsiveTableRecordsEntityRecord a, ENResponsiveTableRecordsEntityRecord b) {
			if (a.ssResponsiveTableRecords != b.ssResponsiveTableRecords) return false;
			return true;
		}

		public static bool operator != (ENResponsiveTableRecordsEntityRecord a, ENResponsiveTableRecordsEntityRecord b) {
			return !(a==b);
		}

		public override bool Equals(object o) {
			if (o.GetType() != typeof(ENResponsiveTableRecordsEntityRecord)) return false;
			return (this == (ENResponsiveTableRecordsEntityRecord) o);
		}

		public override int GetHashCode() {
			try {
				return base.GetHashCode()
				^ ssResponsiveTableRecords.GetHashCode()
				;
			} catch {
				return base.GetHashCode();
			}
		}

		public void GetObjectData(SerializationInfo info, StreamingContext context) {
			Type objInfo = this.GetType();
			FieldInfo[] fields;
			fields = objInfo.GetFields(BindingFlags.NonPublic | BindingFlags.Instance | BindingFlags.Public);
			for (int i = 0; i < fields.Length; i++)
			if (fields[i] .FieldType.IsSerializable)
			info.AddValue(fields[i] .Name, fields[i] .GetValue(this));
		}

		public ENResponsiveTableRecordsEntityRecord(SerializationInfo info, StreamingContext context) {
			ChangedAttributes = new BitArray(1, true);
			OptimizedAttributes = new BitArray(1, false);
			_ssResponsiveTableRecords = "";
			Type objInfo = this.GetType();
			FieldInfo fieldInfo = null;
			fieldInfo = objInfo.GetField("_ssResponsiveTableRecords", BindingFlags.NonPublic | BindingFlags.Instance | BindingFlags.Public);
			if (fieldInfo == null) {
				throw new Exception("The field named '_ssResponsiveTableRecords' was not found.");
			}
			if (fieldInfo.FieldType.IsSerializable) {
				_ssResponsiveTableRecords = (string) info.GetValue(fieldInfo.Name, fieldInfo.FieldType);
			}
		}

		public void RecursiveReset() {
		}

		public void InternalRecursiveSave() {
		}


		public ENResponsiveTableRecordsEntityRecord Duplicate() {
			ENResponsiveTableRecordsEntityRecord t;
			t._ssResponsiveTableRecords = this._ssResponsiveTableRecords;
			t.ChangedAttributes = new BitArray(1);
			t.OptimizedAttributes = new BitArray(1);
			for (int i = 0; i < 1; i++) {
				t.ChangedAttributes[i] = ChangedAttributes[i];
				t.OptimizedAttributes[i] = OptimizedAttributes[i];
			}
			return t;
		}

		IRecord IRecord.Duplicate() {
			return Duplicate();
		}

		public void ToXml(Object parent, System.Xml.XmlElement baseElem, String fieldName, int detailLevel) {
			System.Xml.XmlElement recordElem = VarValue.AppendChild(baseElem, "Entity");
			if (fieldName != null) {
				VarValue.AppendAttribute(recordElem, "debug.field", fieldName);
				fieldName = fieldName.ToLowerInvariant();
			}
			if (detailLevel > 0) {
				if (!VarValue.FieldIsOptimized(parent, fieldName + ".ResponsiveTableRecords")) VarValue.AppendAttribute(recordElem, "ResponsiveTableRecords", ssResponsiveTableRecords, detailLevel, TypeKind.Text); else VarValue.AppendOptimizedAttribute(recordElem, "ResponsiveTableRecords");
			} else {
				VarValue.AppendDeferredEvaluationElement(recordElem);
			}
		}

		public void EvaluateFields(VarValue variable, Object parent, String baseName, String fields) {
			String head = VarValue.GetHead(fields);
			String tail = VarValue.GetTail(fields);
			variable.Found = false;
			if (head == "responsivetablerecords") {
				if (!VarValue.FieldIsOptimized(parent, baseName + ".ResponsiveTableRecords")) variable.Value = ssResponsiveTableRecords; else variable.Optimized = true;
			}
			if (variable.Found && tail != null) variable.EvaluateFields(this, head, tail);
		}

		public bool ChangedAttributeGet(GlobalObjectKey key) {
			if (key.Equals(IdResponsiveTableRecords)) {
				return ChangedAttributes[0];
			} else {
				throw new Exception("Invalid key");
			}
		}

		public bool OptimizedAttributeGet(GlobalObjectKey key) {
			if (key.Equals(IdResponsiveTableRecords)) {
				return OptimizedAttributes[0];
			} else {
				throw new Exception("Invalid key");
			}
		}

		public object AttributeGet(GlobalObjectKey key) {
			if (key == IdResponsiveTableRecords) {
				return ssResponsiveTableRecords;
			} else {
				throw new Exception("Invalid key");
			}
		}
		public void FillFromOther(IRecord other) {
			ChangedAttributes = new BitArray(1);
			OptimizedAttributes = new BitArray(1);
			if (other == null) return;
			ssResponsiveTableRecords = (string) other.AttributeGet(IdResponsiveTableRecords);
			ChangedAttributes[0] = other.ChangedAttributeGet(IdResponsiveTableRecords);
			OptimizedAttributes[0] = other.OptimizedAttributeGet(IdResponsiveTableRecords);
		}
	} // ENResponsiveTableRecordsEntityRecord
	/// <summary>
	/// Static Entity <code>ENResponsiveTableRecordsEntity</code> gets the values for this static entity
	///  records in runtime <code>ResponsiveTableRecords</code>
	/// </summary>
	partial class ENResponsiveTableRecordsEntity {


		static ENResponsiveTableRecordsEntity() {
			ENResponsiveTableRecordsEntityRecordTypeFactoryImpl.InitializeFactory();
		}

		public static ENResponsiveTableRecordsEntityRecord GetRecordById(string id) {
			return (ENResponsiveTableRecordsEntityRecord) RsseSpaceOutSystemsUIWeb.ENResponsiveTableRecordsEntity.GetRecordById(id);
		}

		public static ENResponsiveTableRecordsEntityRecord GetRecordByName(string name) {
			return (ENResponsiveTableRecordsEntityRecord) RsseSpaceOutSystemsUIWeb.ENResponsiveTableRecordsEntity.GetRecordByName(name);
		}

		public static ENResponsiveTableRecordsEntityRecord GetRecordByKey(ObjectKey key) {
			return (ENResponsiveTableRecordsEntityRecord) RsseSpaceOutSystemsUIWeb.ENResponsiveTableRecordsEntity.GetRecordByKey(key);
		}
	} // ENResponsiveTableRecordsEntity;
	/// <summary>
	/// Structure <code>RCResponsiveTableRecordsRecord</code>
	/// </summary>
	[Serializable()]
	public partial struct RCResponsiveTableRecordsRecord: ISerializable, ITypedRecord<RCResponsiveTableRecordsRecord> {
		internal static readonly GlobalObjectKey IdResponsiveTableRecords = GlobalObjectKey.Parse("2UmDmepsh0WSfJ_D1JexCA*MPw8XtqKIimtrpAtjWxD+Q");

		public static void EnsureInitialized() {}
		[System.Xml.Serialization.XmlElement("ResponsiveTableRecords")]
		public ENResponsiveTableRecordsEntityRecord ssENResponsiveTableRecords;


		public static implicit operator ENResponsiveTableRecordsEntityRecord(RCResponsiveTableRecordsRecord r) {
			return r.ssENResponsiveTableRecords;
		}

		public static implicit operator RCResponsiveTableRecordsRecord(ENResponsiveTableRecordsEntityRecord r) {
			RCResponsiveTableRecordsRecord res = new RCResponsiveTableRecordsRecord(null);
			res.ssENResponsiveTableRecords = r;
			return res;
		}

		public BitArray ChangedAttributes {
			set {
				ssENResponsiveTableRecords.ChangedAttributes = value;
			}
			get {
				return ssENResponsiveTableRecords.ChangedAttributes;
			}
		}
		public BitArray OptimizedAttributes;

		public RCResponsiveTableRecordsRecord(params string[] dummy) {
			OptimizedAttributes = null;
			ssENResponsiveTableRecords = new ENResponsiveTableRecordsEntityRecord(null);
		}

		public BitArray[] GetDefaultOptimizedValues() {
			BitArray[] all = new BitArray[1];
			all[0] = new BitArray(1, false);
			return all;
		}

		public BitArray[] AllOptimizedAttributes {
			set {
				if (value == null) {
					ssENResponsiveTableRecords.OptimizedAttributes = GetDefaultOptimizedValues()[0];
				} else {
					ssENResponsiveTableRecords.OptimizedAttributes = value[0];
				}
			}
			get {
				BitArray[] all = new BitArray[1];
				all[0] = ssENResponsiveTableRecords.OptimizedAttributes;
				return all;
			}
		}

		/// <summary>
		/// Read a record from database
		/// </summary>
		/// <param name="r"> Data base reader</param>
		/// <param name="index"> index</param>
		public void Read(IDataReader r, ref int index) {
			ssENResponsiveTableRecords.Read(r, ref index);
		}
		/// <summary>
		/// Read from database
		/// </summary>
		/// <param name="r"> Data reader</param>
		public void ReadDB(IDataReader r) {
			int index = 0;
			Read(r, ref index);
		}

		/// <summary>
		/// Read from record
		/// </summary>
		/// <param name="r"> Record</param>
		public void ReadIM(RCResponsiveTableRecordsRecord r) {
			this = r;
		}


		public static bool operator == (RCResponsiveTableRecordsRecord a, RCResponsiveTableRecordsRecord b) {
			if (a.ssENResponsiveTableRecords != b.ssENResponsiveTableRecords) return false;
			return true;
		}

		public static bool operator != (RCResponsiveTableRecordsRecord a, RCResponsiveTableRecordsRecord b) {
			return !(a==b);
		}

		public override bool Equals(object o) {
			if (o.GetType() != typeof(RCResponsiveTableRecordsRecord)) return false;
			return (this == (RCResponsiveTableRecordsRecord) o);
		}

		public override int GetHashCode() {
			try {
				return base.GetHashCode()
				^ ssENResponsiveTableRecords.GetHashCode()
				;
			} catch {
				return base.GetHashCode();
			}
		}

		public void GetObjectData(SerializationInfo info, StreamingContext context) {
			Type objInfo = this.GetType();
			FieldInfo[] fields;
			fields = objInfo.GetFields(BindingFlags.NonPublic | BindingFlags.Instance | BindingFlags.Public);
			for (int i = 0; i < fields.Length; i++)
			if (fields[i] .FieldType.IsSerializable)
			info.AddValue(fields[i] .Name, fields[i] .GetValue(this));
		}

		public RCResponsiveTableRecordsRecord(SerializationInfo info, StreamingContext context) {
			OptimizedAttributes = null;
			ssENResponsiveTableRecords = new ENResponsiveTableRecordsEntityRecord(null);
			Type objInfo = this.GetType();
			FieldInfo fieldInfo = null;
			fieldInfo = objInfo.GetField("ssENResponsiveTableRecords", BindingFlags.NonPublic | BindingFlags.Instance | BindingFlags.Public);
			if (fieldInfo == null) {
				throw new Exception("The field named 'ssENResponsiveTableRecords' was not found.");
			}
			if (fieldInfo.FieldType.IsSerializable) {
				ssENResponsiveTableRecords = (ENResponsiveTableRecordsEntityRecord) info.GetValue(fieldInfo.Name, fieldInfo.FieldType);
			}
		}

		public void RecursiveReset() {
			ssENResponsiveTableRecords.RecursiveReset();
		}

		public void InternalRecursiveSave() {
			ssENResponsiveTableRecords.InternalRecursiveSave();
		}


		public RCResponsiveTableRecordsRecord Duplicate() {
			RCResponsiveTableRecordsRecord t;
			t.ssENResponsiveTableRecords = (ENResponsiveTableRecordsEntityRecord) this.ssENResponsiveTableRecords.Duplicate();
			t.OptimizedAttributes = null;
			return t;
		}

		IRecord IRecord.Duplicate() {
			return Duplicate();
		}

		public void ToXml(Object parent, System.Xml.XmlElement baseElem, String fieldName, int detailLevel) {
			System.Xml.XmlElement recordElem = VarValue.AppendChild(baseElem, "Record");
			if (fieldName != null) {
				VarValue.AppendAttribute(recordElem, "debug.field", fieldName);
			}
			if (detailLevel > 0) {
				ssENResponsiveTableRecords.ToXml(this, recordElem, "ResponsiveTableRecords", detailLevel - 1);
			} else {
				VarValue.AppendDeferredEvaluationElement(recordElem);
			}
		}

		public void EvaluateFields(VarValue variable, Object parent, String baseName, String fields) {
			String head = VarValue.GetHead(fields);
			String tail = VarValue.GetTail(fields);
			variable.Found = false;
			if (head == "responsivetablerecords") {
				if (!VarValue.FieldIsOptimized(parent, baseName + ".ResponsiveTableRecords")) variable.Value = ssENResponsiveTableRecords; else variable.Optimized = true;
				variable.SetFieldName("responsivetablerecords");
			}
			if (variable.Found && tail != null) variable.EvaluateFields(this, head, tail);
		}

		public bool ChangedAttributeGet(GlobalObjectKey key) {
			return ssENResponsiveTableRecords.ChangedAttributeGet(key);
		}

		public bool OptimizedAttributeGet(GlobalObjectKey key) {
			return ssENResponsiveTableRecords.OptimizedAttributeGet(key);
		}

		public object AttributeGet(GlobalObjectKey key) {
			if (key == IdResponsiveTableRecords) {
				return ssENResponsiveTableRecords;
			} else {
				throw new Exception("Invalid key");
			}
		}
		public void FillFromOther(IRecord other) {
			if (other == null) return;
			ssENResponsiveTableRecords.FillFromOther((IRecord) other.AttributeGet(IdResponsiveTableRecords));
		}
	} // RCResponsiveTableRecordsRecord
	/// <summary>
	/// RecordList type <code>RLResponsiveTableRecordsRecordList</code> that represents a record list of
	///  <code>ResponsiveTableRecords</code>
	/// </summary>
	[Serializable()]
	public partial class RLResponsiveTableRecordsRecordList: GenericRecordList<RCResponsiveTableRecordsRecord>, IEnumerable, IEnumerator, ISerializable {
		public static void EnsureInitialized() {}

		protected override RCResponsiveTableRecordsRecord GetElementDefaultValue() {
			return new RCResponsiveTableRecordsRecord("");
		}

		public T[] ToArray<T>(Func<RCResponsiveTableRecordsRecord, T> converter) {
			return ToArray(this, converter);
		}

		public static T[] ToArray<T>(RLResponsiveTableRecordsRecordList recordlist, Func<RCResponsiveTableRecordsRecord, T> converter) {
			return InnerToArray(recordlist, converter);
		}
		public static implicit operator RLResponsiveTableRecordsRecordList(RCResponsiveTableRecordsRecord[] array) {
			RLResponsiveTableRecordsRecordList result = new RLResponsiveTableRecordsRecordList();
			result.InnerFromArray(array);
			return result;
		}

		public static RLResponsiveTableRecordsRecordList ToList<T>(T[] array, Func <T, RCResponsiveTableRecordsRecord> converter) {
			RLResponsiveTableRecordsRecordList result = new RLResponsiveTableRecordsRecordList();
			result.InnerFromArray(array, converter);
			return result;
		}

		public static RLResponsiveTableRecordsRecordList FromRestList<T>(RestList<T> restList, Func <T, RCResponsiveTableRecordsRecord> converter) {
			RLResponsiveTableRecordsRecordList result = new RLResponsiveTableRecordsRecordList();
			result.InnerFromRestList(restList, converter);
			return result;
		}
		/// <summary>
		/// Default Constructor
		/// </summary>
		public RLResponsiveTableRecordsRecordList(): base() {
		}

		/// <summary>
		/// Constructor with transaction parameter
		/// </summary>
		/// <param name="trans"> IDbTransaction Parameter</param>
		[Obsolete("Use the Default Constructor and set the Transaction afterwards.")]
		public RLResponsiveTableRecordsRecordList(IDbTransaction trans): base(trans) {
		}

		/// <summary>
		/// Constructor with transaction parameter and alternate read method
		/// </summary>
		/// <param name="trans"> IDbTransaction Parameter</param>
		/// <param name="alternateReadDBMethod"> Alternate Read Method</param>
		[Obsolete("Use the Default Constructor and set the Transaction afterwards.")]
		public RLResponsiveTableRecordsRecordList(IDbTransaction trans, ReadDBMethodDelegate alternateReadDBMethod): this(trans) {
			this.alternateReadDBMethod = alternateReadDBMethod;
		}

		/// <summary>
		/// Constructor declaration for serialization
		/// </summary>
		/// <param name="info"> SerializationInfo</param>
		/// <param name="context"> StreamingContext</param>
		public RLResponsiveTableRecordsRecordList(SerializationInfo info, StreamingContext context): base(info, context) {
		}

		public override BitArray[] GetDefaultOptimizedValues() {
			BitArray[] def = new BitArray[1];
			def[0] = new BitArray(1, false);
			return def;
		}
		/// <summary>
		/// Create as new list
		/// </summary>
		/// <returns>The new record list</returns>
		protected override OSList<RCResponsiveTableRecordsRecord> NewList() {
			return new RLResponsiveTableRecordsRecordList();
		}


	} // RLResponsiveTableRecordsRecordList

	/// <summary>
	/// RecordList type <code>RLResponsiveTableRecordsList</code> that represents a record list of
	///  <code>ResponsiveTableRecords</code>
	/// </summary>
	[Serializable()]
	public partial class RLResponsiveTableRecordsList: GenericRecordList<ENResponsiveTableRecordsEntityRecord>, IEnumerable, IEnumerator, ISerializable {
		public static void EnsureInitialized() {}

		protected override ENResponsiveTableRecordsEntityRecord GetElementDefaultValue() {
			return new ENResponsiveTableRecordsEntityRecord("");
		}

		public T[] ToArray<T>(Func<ENResponsiveTableRecordsEntityRecord, T> converter) {
			return ToArray(this, converter);
		}

		public static T[] ToArray<T>(RLResponsiveTableRecordsList recordlist, Func<ENResponsiveTableRecordsEntityRecord, T> converter) {
			return InnerToArray(recordlist, converter);
		}
		public static implicit operator RLResponsiveTableRecordsList(ENResponsiveTableRecordsEntityRecord[] array) {
			RLResponsiveTableRecordsList result = new RLResponsiveTableRecordsList();
			result.InnerFromArray(array);
			return result;
		}

		public static RLResponsiveTableRecordsList ToList<T>(T[] array, Func <T, ENResponsiveTableRecordsEntityRecord> converter) {
			RLResponsiveTableRecordsList result = new RLResponsiveTableRecordsList();
			result.InnerFromArray(array, converter);
			return result;
		}

		public static RLResponsiveTableRecordsList FromRestList<T>(RestList<T> restList, Func <T, ENResponsiveTableRecordsEntityRecord> converter) {
			RLResponsiveTableRecordsList result = new RLResponsiveTableRecordsList();
			result.InnerFromRestList(restList, converter);
			return result;
		}
		/// <summary>
		/// Default Constructor
		/// </summary>
		public RLResponsiveTableRecordsList(): base() {
		}

		/// <summary>
		/// Constructor with transaction parameter
		/// </summary>
		/// <param name="trans"> IDbTransaction Parameter</param>
		[Obsolete("Use the Default Constructor and set the Transaction afterwards.")]
		public RLResponsiveTableRecordsList(IDbTransaction trans): base(trans) {
		}

		/// <summary>
		/// Constructor with transaction parameter and alternate read method
		/// </summary>
		/// <param name="trans"> IDbTransaction Parameter</param>
		/// <param name="alternateReadDBMethod"> Alternate Read Method</param>
		[Obsolete("Use the Default Constructor and set the Transaction afterwards.")]
		public RLResponsiveTableRecordsList(IDbTransaction trans, ReadDBMethodDelegate alternateReadDBMethod): this(trans) {
			this.alternateReadDBMethod = alternateReadDBMethod;
		}

		/// <summary>
		/// Constructor declaration for serialization
		/// </summary>
		/// <param name="info"> SerializationInfo</param>
		/// <param name="context"> StreamingContext</param>
		public RLResponsiveTableRecordsList(SerializationInfo info, StreamingContext context): base(info, context) {
		}

		public override BitArray[] GetDefaultOptimizedValues() {
			BitArray[] def = new BitArray[0];
			return def;
		}
		/// <summary>
		/// Create as new list
		/// </summary>
		/// <returns>The new record list</returns>
		protected override OSList<ENResponsiveTableRecordsEntityRecord> NewList() {
			return new RLResponsiveTableRecordsList();
		}


	} // RLResponsiveTableRecordsList
	public partial class ExtendedActions {
		/// <summary>
		/// Action: GetResponsiveTableRecords
		/// </summary>

		public static void GetResponsiveTableRecords(HeContext heContext, string inParamId, out RCResponsiveTableRecordsRecord outParamRecord) {
			outParamRecord = new RCResponsiveTableRecordsRecord(null);

			ObjectKey oldCurrentESpaceKey = heContext.CurrentESpaceKey;
			try {
				heContext.CurrentESpaceKey = ENResponsiveTableRecordsEntity.eSpaceKey;
				try {
					outParamRecord = ENResponsiveTableRecordsEntity.GetRecordById(inParamId);
				} catch {
					using(Transaction trans = DatabaseAccess.ForCurrentDatabase.GetRequestTransaction()) {
						string sql =
						 "SELECT " +
						 " [RESPONSIVETABLERECORDS]" +
						 " FROM " + ENResponsiveTableRecordsEntity.LocalViewName(heContext.AppInfo.Tenant.Id, BuiltInFunction.GetCurrentLocale()) + " " +
						 "WHERE [RESPONSIVETABLERECORDS] = @inParamId";
						using(var sqlCmd = trans.CreateCommand(sql)) {
							sqlCmd.CreateParameter("@inParamId", DbType.String, inParamId);
							using(IDataReader reader = sqlCmd.ExecuteReader("Entity Action GetResponsiveTableRecords", true, false, false)) {
								if (reader.Read()) {
									outParamRecord.ReadDB(reader);
								} else {
									throw new DataBaseException(ENResponsiveTableRecordsEntity.LocalViewName(heContext.AppInfo.Tenant.Id, BuiltInFunction.GetCurrentLocale()) + " with key " + inParamId + " was not found"); 
								}
							}
						}
					}
				}
			} finally {
				heContext.CurrentESpaceKey = oldCurrentESpaceKey;
			}
		}

	}
}

namespace ssBIFY {
	[XmlType("ResponsiveTableRecords")]
	public class WORCResponsiveTableRecordsRecord {
		[System.Xml.Serialization.XmlElement("ResponsiveTableRecords")]
		public string varWSResponsiveTableRecords;

		public WORCResponsiveTableRecordsRecord() {
			varWSResponsiveTableRecords = (string) "";
		}

		public WORCResponsiveTableRecordsRecord(ENResponsiveTableRecordsEntityRecord r) {
			varWSResponsiveTableRecords = BaseAppUtils.RemoveControlChars(r.ssResponsiveTableRecords);
		}

		public static RLResponsiveTableRecordsList ToRecordList(WORCResponsiveTableRecordsRecord[] array) {
			RLResponsiveTableRecordsList rl = new RLResponsiveTableRecordsList();
			if (array != null) {
				foreach(WORCResponsiveTableRecordsRecord val in array) {
					rl.Append(val);
				}
			}
			return rl;
		}

		public static WORCResponsiveTableRecordsRecord[] FromRecordList(RLResponsiveTableRecordsList rl) {
			WORCResponsiveTableRecordsRecord[] array = new WORCResponsiveTableRecordsRecord[rl == null ? 0: rl.Length];
			for (int i = 0; i < array.Length; i++) {
				array[i] = rl.Data[i];
			}
			return array;
		}
	}
}

namespace ssBIFY {
	partial struct RCResponsiveTableRecordsRecord {
		public static implicit operator WORCResponsiveTableRecordsRecord(RCResponsiveTableRecordsRecord r) {
			return new WORCResponsiveTableRecordsRecord(r.ssENResponsiveTableRecords);
		}

		public static implicit operator RCResponsiveTableRecordsRecord(WORCResponsiveTableRecordsRecord w) {
			RCResponsiveTableRecordsRecord r = new RCResponsiveTableRecordsRecord("");
			if (w != null) {
				r.ssENResponsiveTableRecords = w;
			}
			return r;
		}

	}

	partial struct ENResponsiveTableRecordsEntityRecord {
		public static implicit operator WORCResponsiveTableRecordsRecord(ENResponsiveTableRecordsEntityRecord r) {
			return new WORCResponsiveTableRecordsRecord(r);
		}

		public static implicit operator ENResponsiveTableRecordsEntityRecord(WORCResponsiveTableRecordsRecord w) {
			ENResponsiveTableRecordsEntityRecord r = new ENResponsiveTableRecordsEntityRecord("");
			if (w != null) {
				r.ssResponsiveTableRecords = ((string) w.varWSResponsiveTableRecords ?? "");
			}
			return r;
		}

	}
}


namespace ssBIFY {
	[Serializable()]
	public partial class WORLResponsiveTableRecordsRecordList {
		public WORCResponsiveTableRecordsRecord[] Array;


		public WORLResponsiveTableRecordsRecordList(WORCResponsiveTableRecordsRecord[] r) {
			if (r == null)
			Array = new WORCResponsiveTableRecordsRecord[0];
			else
			Array = r;
		}
		public WORLResponsiveTableRecordsRecordList() {
			Array = new WORCResponsiveTableRecordsRecord[0];
		}

		public WORLResponsiveTableRecordsRecordList(RLResponsiveTableRecordsRecordList rl) {
			rl=(RLResponsiveTableRecordsRecordList) rl.Duplicate();
			Array = new WORCResponsiveTableRecordsRecord[rl.Length];
			while (!rl.Eof) {
				Array[rl.CurrentRowNumber] = new WORCResponsiveTableRecordsRecord(rl.CurrentRec);
				rl.Advance();
			}
		}

	}
}

namespace ssBIFY {
	partial class RLResponsiveTableRecordsRecordList {
		public static implicit operator RLResponsiveTableRecordsRecordList(WORCResponsiveTableRecordsRecord[] array) {
			RLResponsiveTableRecordsRecordList rl = new RLResponsiveTableRecordsRecordList();
			if (array != null) {
				foreach(WORCResponsiveTableRecordsRecord val in array) {
					rl.Append((RCResponsiveTableRecordsRecord) val);
				}
			}
			return rl;
		}
		public static implicit operator WORCResponsiveTableRecordsRecord[](RLResponsiveTableRecordsRecordList rl) {
			WORCResponsiveTableRecordsRecord[] array = new WORCResponsiveTableRecordsRecord[rl == null ? 0: rl.Length];
			for (int i = 0; i < array.Length; i++) {
				array[i] = (RCResponsiveTableRecordsRecord) rl.Data[i];
			}
			return array;
		}
	}
}

namespace ssBIFY {
	partial class WORLResponsiveTableRecordsRecordList {
		public static implicit operator RLResponsiveTableRecordsRecordList(WORLResponsiveTableRecordsRecordList w) {
			return w.Array;
		}
		public static implicit operator WORLResponsiveTableRecordsRecordList(RLResponsiveTableRecordsRecordList rl) {
			return new WORLResponsiveTableRecordsRecordList(rl);
		}
		public static implicit operator WORCResponsiveTableRecordsRecord[](WORLResponsiveTableRecordsRecordList w) {
			if (w != null) {
				return w.Array;
			}
			return null;
		}
		public static implicit operator WORLResponsiveTableRecordsRecordList(WORCResponsiveTableRecordsRecord[] array) {
			return new WORLResponsiveTableRecordsRecordList(array);
		}
	}
}

