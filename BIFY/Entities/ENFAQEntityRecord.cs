﻿/* 
 This source code (the "Generated Software") is generated by the OutSystems Platform 
 and is licensed by OutSystems (http://www.outsystems.com) to You solely for testing and evaluation 
 purposes, unless You and OutSystems have executed a specific agreement covering the use terms and 
 conditions of the Generated Software, in which case such agreement shall apply. 
*/

using System;
using System.Data;
using System.Runtime.Serialization;
using System.Reflection;
using System.Xml;
using OutSystems.ObjectKeys;
using OutSystems.HubEdition.RuntimePlatform;
using OutSystems.HubEdition.RuntimePlatform.Db;
using OutSystems.Internal.Db;
using System.Xml.Serialization;
using System.Collections;
using System.Collections.Generic;
using OutSystems.RuntimeCommon;
using OutSystems.HubEdition.RuntimePlatform.NewRuntime;

namespace ssBIFY {

	public sealed partial class ENFAQEntity {
		public static string LocalViewName(int? tenant, string locale) {
			return ViewName(null, locale);
		}
		public static string ViewName(int? tenant, string locale) {
			return BaseAppUtils.EscapeAndQualifySqlIdentifier(DatabaseAccess.ForEspaceDatabase("f82563d5-ffee-43c2-900d-8425d42939cd"), "OSUSR_kaz_FAQ");
		}
		public static System.Collections.Generic.Dictionary<string, string> AttributesToDatabaseNamesMap() {
			return new System.Collections.Generic.Dictionary<string, string>() {
				{ "id", "Id"
				}
				, { "title", "Title"
				}
				, { "detail", "Detail"
				}
			};
		}
	} // ENFAQEntity

	/// <summary>
	/// Entity <code>ENFAQEntityRecord</code> that represents the Service Studio entity <code>FAQ</code>
	///  <p> Description: </p>
	/// </summary>
	[OutSystems.HubEdition.RuntimePlatform.MetaInformation.EntityRecordDetails("FAQ", "TL925HEeDE24SfaMwMVXPw", "1WMl+O7_wkOQDYQl1Ck5zQ", 0, "OSUSR_kaz_FAQ", null, false)]
	[Serializable()]
	public partial struct ENFAQEntityRecord: ISerializable, ITypedRecord<ENFAQEntityRecord>, ISimpleRecord {
		internal static readonly GlobalObjectKey IdId = GlobalObjectKey.Parse("1WMl+O7_wkOQDYQl1Ck5zQ*PqIlBsr1BU+JBP7Onj7K7Q");
		internal static readonly GlobalObjectKey IdTitle = GlobalObjectKey.Parse("1WMl+O7_wkOQDYQl1Ck5zQ*aGKpcY90sEmnP645yJVo8Q");
		internal static readonly GlobalObjectKey IdDetail = GlobalObjectKey.Parse("1WMl+O7_wkOQDYQl1Ck5zQ*ESrFYPLgK06MVFi2f2ao8Q");

		public static void EnsureInitialized() {}
		[OutSystems.HubEdition.RuntimePlatform.MetaInformation.EntityAttributeDetails("ID", 0, true, true, false, true)]
		[System.Xml.Serialization.XmlElement("Id")]
		private long _ssId;
		public long ssId {
			get {
				return _ssId;
			}
			set {
				if ((_ssId!=value) || OptimizedAttributes[0]) {
					ChangedAttributes = new BitArray(3, true);
					_ssId = value;
				}
			}
		}

		[OutSystems.HubEdition.RuntimePlatform.MetaInformation.EntityAttributeDetails("TITLE", 50, false, false, false, false)]
		[System.Xml.Serialization.XmlElement("Title")]
		private string _ssTitle;
		public string ssTitle {
			get {
				return _ssTitle;
			}
			set {
				if ((_ssTitle!=value) || OptimizedAttributes[1]) {
					ChangedAttributes[1] = true;
					_ssTitle = value;
				}
			}
		}

		[OutSystems.HubEdition.RuntimePlatform.MetaInformation.EntityAttributeDetails("DETAIL", 500, false, false, false, false)]
		[System.Xml.Serialization.XmlElement("Detail")]
		private string _ssDetail;
		public string ssDetail {
			get {
				return _ssDetail;
			}
			set {
				if ((_ssDetail!=value) || OptimizedAttributes[2]) {
					ChangedAttributes[2] = true;
					_ssDetail = value;
				}
			}
		}


		public BitArray ChangedAttributes;

		public BitArray OptimizedAttributes;

		public ENFAQEntityRecord(params string[] dummy) {
			ChangedAttributes = new BitArray(3, true);
			OptimizedAttributes = new BitArray(3, false);
			_ssId = 0L;
			_ssTitle = "";
			_ssDetail = "";
		}

		public BitArray[] GetDefaultOptimizedValues() {
			BitArray[] all = new BitArray[0];
			return all;
		}

		public BitArray[] AllOptimizedAttributes {
			set {
				if (value == null) {
				} else {
				}
			}
			get {
				BitArray[] all = new BitArray[0];
				return all;
			}
		}

		/// <summary>
		/// Read a record from database
		/// </summary>
		/// <param name="r"> Data base reader</param>
		/// <param name="index"> index</param>
		public void Read(IDataReader r, ref int index) {
			ssId = r.ReadLongInteger(index++, "FAQ.Id", 0L);
			ssTitle = r.ReadText(index++, "FAQ.Title", "");
			ssDetail = r.ReadText(index++, "FAQ.Detail", "");
			ChangedAttributes = new BitArray(3, false);
			OptimizedAttributes = new BitArray(3, false);
		}
		/// <summary>
		/// Read from database
		/// </summary>
		/// <param name="r"> Data reader</param>
		public void ReadDB(IDataReader r) {
			int index = 0;
			Read(r, ref index);
		}

		/// <summary>
		/// Read from record
		/// </summary>
		/// <param name="r"> Record</param>
		public void ReadIM(ENFAQEntityRecord r) {
			this = r;
		}


		public static bool operator == (ENFAQEntityRecord a, ENFAQEntityRecord b) {
			if (a.ssId != b.ssId) return false;
			if (a.ssTitle != b.ssTitle) return false;
			if (a.ssDetail != b.ssDetail) return false;
			return true;
		}

		public static bool operator != (ENFAQEntityRecord a, ENFAQEntityRecord b) {
			return !(a==b);
		}

		public override bool Equals(object o) {
			if (o.GetType() != typeof(ENFAQEntityRecord)) return false;
			return (this == (ENFAQEntityRecord) o);
		}

		public override int GetHashCode() {
			try {
				return base.GetHashCode()
				^ ssId.GetHashCode()
				^ ssTitle.GetHashCode()
				^ ssDetail.GetHashCode()
				;
			} catch {
				return base.GetHashCode();
			}
		}

		public void GetObjectData(SerializationInfo info, StreamingContext context) {
			Type objInfo = this.GetType();
			FieldInfo[] fields;
			fields = objInfo.GetFields(BindingFlags.NonPublic | BindingFlags.Instance | BindingFlags.Public);
			for (int i = 0; i < fields.Length; i++)
			if (fields[i] .FieldType.IsSerializable)
			info.AddValue(fields[i] .Name, fields[i] .GetValue(this));
		}

		public ENFAQEntityRecord(SerializationInfo info, StreamingContext context) {
			ChangedAttributes = new BitArray(3, true);
			OptimizedAttributes = new BitArray(3, false);
			_ssId = 0L;
			_ssTitle = "";
			_ssDetail = "";
			Type objInfo = this.GetType();
			FieldInfo fieldInfo = null;
			fieldInfo = objInfo.GetField("_ssId", BindingFlags.NonPublic | BindingFlags.Instance | BindingFlags.Public);
			if (fieldInfo == null) {
				throw new Exception("The field named '_ssId' was not found.");
			}
			if (fieldInfo.FieldType.IsSerializable) {
				_ssId = (long) info.GetValue(fieldInfo.Name, fieldInfo.FieldType);
			}
			fieldInfo = objInfo.GetField("_ssTitle", BindingFlags.NonPublic | BindingFlags.Instance | BindingFlags.Public);
			if (fieldInfo == null) {
				throw new Exception("The field named '_ssTitle' was not found.");
			}
			if (fieldInfo.FieldType.IsSerializable) {
				_ssTitle = (string) info.GetValue(fieldInfo.Name, fieldInfo.FieldType);
			}
			fieldInfo = objInfo.GetField("_ssDetail", BindingFlags.NonPublic | BindingFlags.Instance | BindingFlags.Public);
			if (fieldInfo == null) {
				throw new Exception("The field named '_ssDetail' was not found.");
			}
			if (fieldInfo.FieldType.IsSerializable) {
				_ssDetail = (string) info.GetValue(fieldInfo.Name, fieldInfo.FieldType);
			}
		}

		public void RecursiveReset() {
		}

		public void InternalRecursiveSave() {
		}


		public ENFAQEntityRecord Duplicate() {
			ENFAQEntityRecord t;
			t._ssId = this._ssId;
			t._ssTitle = this._ssTitle;
			t._ssDetail = this._ssDetail;
			t.ChangedAttributes = new BitArray(3);
			t.OptimizedAttributes = new BitArray(3);
			for (int i = 0; i < 3; i++) {
				t.ChangedAttributes[i] = ChangedAttributes[i];
				t.OptimizedAttributes[i] = OptimizedAttributes[i];
			}
			return t;
		}

		IRecord IRecord.Duplicate() {
			return Duplicate();
		}

		public void ToXml(Object parent, System.Xml.XmlElement baseElem, String fieldName, int detailLevel) {
			System.Xml.XmlElement recordElem = VarValue.AppendChild(baseElem, "Entity");
			if (fieldName != null) {
				VarValue.AppendAttribute(recordElem, "debug.field", fieldName);
				fieldName = fieldName.ToLowerInvariant();
			}
			if (detailLevel > 0) {
				if (!VarValue.FieldIsOptimized(parent, fieldName + ".Id")) VarValue.AppendAttribute(recordElem, "Id", ssId, detailLevel, TypeKind.LongInteger); else VarValue.AppendOptimizedAttribute(recordElem, "Id");
				if (!VarValue.FieldIsOptimized(parent, fieldName + ".Title")) VarValue.AppendAttribute(recordElem, "Title", ssTitle, detailLevel, TypeKind.Text); else VarValue.AppendOptimizedAttribute(recordElem, "Title");
				if (!VarValue.FieldIsOptimized(parent, fieldName + ".Detail")) VarValue.AppendAttribute(recordElem, "Detail", ssDetail, detailLevel, TypeKind.Text); else VarValue.AppendOptimizedAttribute(recordElem, "Detail");
			} else {
				VarValue.AppendDeferredEvaluationElement(recordElem);
			}
		}

		public void EvaluateFields(VarValue variable, Object parent, String baseName, String fields) {
			String head = VarValue.GetHead(fields);
			String tail = VarValue.GetTail(fields);
			variable.Found = false;
			if (head == "id") {
				if (!VarValue.FieldIsOptimized(parent, baseName + ".Id")) variable.Value = ssId; else variable.Optimized = true;
			} else if (head == "title") {
				if (!VarValue.FieldIsOptimized(parent, baseName + ".Title")) variable.Value = ssTitle; else variable.Optimized = true;
			} else if (head == "detail") {
				if (!VarValue.FieldIsOptimized(parent, baseName + ".Detail")) variable.Value = ssDetail; else variable.Optimized = true;
			}
			if (variable.Found && tail != null) variable.EvaluateFields(this, head, tail);
		}

		public bool ChangedAttributeGet(GlobalObjectKey key) {
			if (key.Equals(IdId)) {
				return ChangedAttributes[0];
			} else if (key.Equals(IdTitle)) {
				return ChangedAttributes[1];
			} else if (key.Equals(IdDetail)) {
				return ChangedAttributes[2];
			} else {
				throw new Exception("Invalid key");
			}
		}

		public bool OptimizedAttributeGet(GlobalObjectKey key) {
			if (key.Equals(IdId)) {
				return OptimizedAttributes[0];
			} else if (key.Equals(IdTitle)) {
				return OptimizedAttributes[1];
			} else if (key.Equals(IdDetail)) {
				return OptimizedAttributes[2];
			} else {
				throw new Exception("Invalid key");
			}
		}

		public object AttributeGet(GlobalObjectKey key) {
			if (key == IdId) {
				return ssId;
			} else if (key == IdTitle) {
				return ssTitle;
			} else if (key == IdDetail) {
				return ssDetail;
			} else {
				throw new Exception("Invalid key");
			}
		}
		public void FillFromOther(IRecord other) {
			ChangedAttributes = new BitArray(3);
			OptimizedAttributes = new BitArray(3);
			if (other == null) return;
			ssId = (long) other.AttributeGet(IdId);
			ChangedAttributes[0] = other.ChangedAttributeGet(IdId);
			OptimizedAttributes[0] = other.OptimizedAttributeGet(IdId);
			ssTitle = (string) other.AttributeGet(IdTitle);
			ChangedAttributes[1] = other.ChangedAttributeGet(IdTitle);
			OptimizedAttributes[1] = other.OptimizedAttributeGet(IdTitle);
			ssDetail = (string) other.AttributeGet(IdDetail);
			ChangedAttributes[2] = other.ChangedAttributeGet(IdDetail);
			OptimizedAttributes[2] = other.OptimizedAttributeGet(IdDetail);
		}
	} // ENFAQEntityRecord
	/// <summary>
	/// Structure <code>RCFAQRecord</code>
	/// </summary>
	[Serializable()]
	public partial struct RCFAQRecord: ISerializable, ITypedRecord<RCFAQRecord> {
		internal static readonly GlobalObjectKey IdFAQ = GlobalObjectKey.Parse("2UmDmepsh0WSfJ_D1JexCA*KfTSFQ7SHZ+I1QuRcyKn_Q");

		public static void EnsureInitialized() {}
		[System.Xml.Serialization.XmlElement("FAQ")]
		public ENFAQEntityRecord ssENFAQ;


		public static implicit operator ENFAQEntityRecord(RCFAQRecord r) {
			return r.ssENFAQ;
		}

		public static implicit operator RCFAQRecord(ENFAQEntityRecord r) {
			RCFAQRecord res = new RCFAQRecord(null);
			res.ssENFAQ = r;
			return res;
		}

		public BitArray ChangedAttributes {
			set {
				ssENFAQ.ChangedAttributes = value;
			}
			get {
				return ssENFAQ.ChangedAttributes;
			}
		}
		public BitArray OptimizedAttributes;

		public RCFAQRecord(params string[] dummy) {
			OptimizedAttributes = null;
			ssENFAQ = new ENFAQEntityRecord(null);
		}

		public BitArray[] GetDefaultOptimizedValues() {
			BitArray[] all = new BitArray[1];
			all[0] = new BitArray(3, false);
			return all;
		}

		public BitArray[] AllOptimizedAttributes {
			set {
				if (value == null) {
					ssENFAQ.OptimizedAttributes = GetDefaultOptimizedValues()[0];
				} else {
					ssENFAQ.OptimizedAttributes = value[0];
				}
			}
			get {
				BitArray[] all = new BitArray[1];
				all[0] = ssENFAQ.OptimizedAttributes;
				return all;
			}
		}

		/// <summary>
		/// Read a record from database
		/// </summary>
		/// <param name="r"> Data base reader</param>
		/// <param name="index"> index</param>
		public void Read(IDataReader r, ref int index) {
			ssENFAQ.Read(r, ref index);
		}
		/// <summary>
		/// Read from database
		/// </summary>
		/// <param name="r"> Data reader</param>
		public void ReadDB(IDataReader r) {
			int index = 0;
			Read(r, ref index);
		}

		/// <summary>
		/// Read from record
		/// </summary>
		/// <param name="r"> Record</param>
		public void ReadIM(RCFAQRecord r) {
			this = r;
		}


		public static bool operator == (RCFAQRecord a, RCFAQRecord b) {
			if (a.ssENFAQ != b.ssENFAQ) return false;
			return true;
		}

		public static bool operator != (RCFAQRecord a, RCFAQRecord b) {
			return !(a==b);
		}

		public override bool Equals(object o) {
			if (o.GetType() != typeof(RCFAQRecord)) return false;
			return (this == (RCFAQRecord) o);
		}

		public override int GetHashCode() {
			try {
				return base.GetHashCode()
				^ ssENFAQ.GetHashCode()
				;
			} catch {
				return base.GetHashCode();
			}
		}

		public void GetObjectData(SerializationInfo info, StreamingContext context) {
			Type objInfo = this.GetType();
			FieldInfo[] fields;
			fields = objInfo.GetFields(BindingFlags.NonPublic | BindingFlags.Instance | BindingFlags.Public);
			for (int i = 0; i < fields.Length; i++)
			if (fields[i] .FieldType.IsSerializable)
			info.AddValue(fields[i] .Name, fields[i] .GetValue(this));
		}

		public RCFAQRecord(SerializationInfo info, StreamingContext context) {
			OptimizedAttributes = null;
			ssENFAQ = new ENFAQEntityRecord(null);
			Type objInfo = this.GetType();
			FieldInfo fieldInfo = null;
			fieldInfo = objInfo.GetField("ssENFAQ", BindingFlags.NonPublic | BindingFlags.Instance | BindingFlags.Public);
			if (fieldInfo == null) {
				throw new Exception("The field named 'ssENFAQ' was not found.");
			}
			if (fieldInfo.FieldType.IsSerializable) {
				ssENFAQ = (ENFAQEntityRecord) info.GetValue(fieldInfo.Name, fieldInfo.FieldType);
			}
		}

		public void RecursiveReset() {
			ssENFAQ.RecursiveReset();
		}

		public void InternalRecursiveSave() {
			ssENFAQ.InternalRecursiveSave();
		}


		public RCFAQRecord Duplicate() {
			RCFAQRecord t;
			t.ssENFAQ = (ENFAQEntityRecord) this.ssENFAQ.Duplicate();
			t.OptimizedAttributes = null;
			return t;
		}

		IRecord IRecord.Duplicate() {
			return Duplicate();
		}

		public void ToXml(Object parent, System.Xml.XmlElement baseElem, String fieldName, int detailLevel) {
			System.Xml.XmlElement recordElem = VarValue.AppendChild(baseElem, "Record");
			if (fieldName != null) {
				VarValue.AppendAttribute(recordElem, "debug.field", fieldName);
			}
			if (detailLevel > 0) {
				ssENFAQ.ToXml(this, recordElem, "FAQ", detailLevel - 1);
			} else {
				VarValue.AppendDeferredEvaluationElement(recordElem);
			}
		}

		public void EvaluateFields(VarValue variable, Object parent, String baseName, String fields) {
			String head = VarValue.GetHead(fields);
			String tail = VarValue.GetTail(fields);
			variable.Found = false;
			if (head == "faq") {
				if (!VarValue.FieldIsOptimized(parent, baseName + ".FAQ")) variable.Value = ssENFAQ; else variable.Optimized = true;
				variable.SetFieldName("faq");
			}
			if (variable.Found && tail != null) variable.EvaluateFields(this, head, tail);
		}

		public bool ChangedAttributeGet(GlobalObjectKey key) {
			return ssENFAQ.ChangedAttributeGet(key);
		}

		public bool OptimizedAttributeGet(GlobalObjectKey key) {
			return ssENFAQ.OptimizedAttributeGet(key);
		}

		public object AttributeGet(GlobalObjectKey key) {
			if (key == IdFAQ) {
				return ssENFAQ;
			} else {
				throw new Exception("Invalid key");
			}
		}
		public void FillFromOther(IRecord other) {
			if (other == null) return;
			ssENFAQ.FillFromOther((IRecord) other.AttributeGet(IdFAQ));
		}
	} // RCFAQRecord
	/// <summary>
	/// RecordList type <code>RLFAQRecordList</code> that represents a record list of <code>FAQ</code>
	/// </summary>
	[Serializable()]
	public partial class RLFAQRecordList: GenericRecordList<RCFAQRecord>, IEnumerable, IEnumerator, ISerializable {
		public static void EnsureInitialized() {}

		protected override RCFAQRecord GetElementDefaultValue() {
			return new RCFAQRecord("");
		}

		public T[] ToArray<T>(Func<RCFAQRecord, T> converter) {
			return ToArray(this, converter);
		}

		public static T[] ToArray<T>(RLFAQRecordList recordlist, Func<RCFAQRecord, T> converter) {
			return InnerToArray(recordlist, converter);
		}
		public static implicit operator RLFAQRecordList(RCFAQRecord[] array) {
			RLFAQRecordList result = new RLFAQRecordList();
			result.InnerFromArray(array);
			return result;
		}

		public static RLFAQRecordList ToList<T>(T[] array, Func <T, RCFAQRecord> converter) {
			RLFAQRecordList result = new RLFAQRecordList();
			result.InnerFromArray(array, converter);
			return result;
		}

		public static RLFAQRecordList FromRestList<T>(RestList<T> restList, Func <T, RCFAQRecord> converter) {
			RLFAQRecordList result = new RLFAQRecordList();
			result.InnerFromRestList(restList, converter);
			return result;
		}
		/// <summary>
		/// Default Constructor
		/// </summary>
		public RLFAQRecordList(): base() {
		}

		/// <summary>
		/// Constructor with transaction parameter
		/// </summary>
		/// <param name="trans"> IDbTransaction Parameter</param>
		[Obsolete("Use the Default Constructor and set the Transaction afterwards.")]
		public RLFAQRecordList(IDbTransaction trans): base(trans) {
		}

		/// <summary>
		/// Constructor with transaction parameter and alternate read method
		/// </summary>
		/// <param name="trans"> IDbTransaction Parameter</param>
		/// <param name="alternateReadDBMethod"> Alternate Read Method</param>
		[Obsolete("Use the Default Constructor and set the Transaction afterwards.")]
		public RLFAQRecordList(IDbTransaction trans, ReadDBMethodDelegate alternateReadDBMethod): this(trans) {
			this.alternateReadDBMethod = alternateReadDBMethod;
		}

		/// <summary>
		/// Constructor declaration for serialization
		/// </summary>
		/// <param name="info"> SerializationInfo</param>
		/// <param name="context"> StreamingContext</param>
		public RLFAQRecordList(SerializationInfo info, StreamingContext context): base(info, context) {
		}

		public override BitArray[] GetDefaultOptimizedValues() {
			BitArray[] def = new BitArray[1];
			def[0] = new BitArray(3, false);
			return def;
		}
		/// <summary>
		/// Create as new list
		/// </summary>
		/// <returns>The new record list</returns>
		protected override OSList<RCFAQRecord> NewList() {
			return new RLFAQRecordList();
		}


	} // RLFAQRecordList

	/// <summary>
	/// RecordList type <code>RLFAQList</code> that represents a record list of <code>FAQ</code>
	/// </summary>
	[Serializable()]
	public partial class RLFAQList: GenericRecordList<ENFAQEntityRecord>, IEnumerable, IEnumerator, ISerializable {
		public static void EnsureInitialized() {}

		protected override ENFAQEntityRecord GetElementDefaultValue() {
			return new ENFAQEntityRecord("");
		}

		public T[] ToArray<T>(Func<ENFAQEntityRecord, T> converter) {
			return ToArray(this, converter);
		}

		public static T[] ToArray<T>(RLFAQList recordlist, Func<ENFAQEntityRecord, T> converter) {
			return InnerToArray(recordlist, converter);
		}
		public static implicit operator RLFAQList(ENFAQEntityRecord[] array) {
			RLFAQList result = new RLFAQList();
			result.InnerFromArray(array);
			return result;
		}

		public static RLFAQList ToList<T>(T[] array, Func <T, ENFAQEntityRecord> converter) {
			RLFAQList result = new RLFAQList();
			result.InnerFromArray(array, converter);
			return result;
		}

		public static RLFAQList FromRestList<T>(RestList<T> restList, Func <T, ENFAQEntityRecord> converter) {
			RLFAQList result = new RLFAQList();
			result.InnerFromRestList(restList, converter);
			return result;
		}
		/// <summary>
		/// Default Constructor
		/// </summary>
		public RLFAQList(): base() {
		}

		/// <summary>
		/// Constructor with transaction parameter
		/// </summary>
		/// <param name="trans"> IDbTransaction Parameter</param>
		[Obsolete("Use the Default Constructor and set the Transaction afterwards.")]
		public RLFAQList(IDbTransaction trans): base(trans) {
		}

		/// <summary>
		/// Constructor with transaction parameter and alternate read method
		/// </summary>
		/// <param name="trans"> IDbTransaction Parameter</param>
		/// <param name="alternateReadDBMethod"> Alternate Read Method</param>
		[Obsolete("Use the Default Constructor and set the Transaction afterwards.")]
		public RLFAQList(IDbTransaction trans, ReadDBMethodDelegate alternateReadDBMethod): this(trans) {
			this.alternateReadDBMethod = alternateReadDBMethod;
		}

		/// <summary>
		/// Constructor declaration for serialization
		/// </summary>
		/// <param name="info"> SerializationInfo</param>
		/// <param name="context"> StreamingContext</param>
		public RLFAQList(SerializationInfo info, StreamingContext context): base(info, context) {
		}

		public override BitArray[] GetDefaultOptimizedValues() {
			BitArray[] def = new BitArray[0];
			return def;
		}
		/// <summary>
		/// Create as new list
		/// </summary>
		/// <returns>The new record list</returns>
		protected override OSList<ENFAQEntityRecord> NewList() {
			return new RLFAQList();
		}


	} // RLFAQList
	public partial class ExtendedActions {
		/// <summary>
		/// Action: CreateFAQ
		/// </summary>

		public static void CreateFAQ(HeContext heContext, RCFAQRecord inParamSource, out long outParamId) {
			outParamId = 0L;

			ENFAQEntityRecord ssENFAQ = inParamSource;
			using(Transaction trans = DatabaseAccess.ForCurrentDatabase.GetRequestTransaction()) {
				string insertSql =
				 "SET NOCOUNT ON; INSERT INTO " + ENFAQEntity.LocalViewName(null, null) + " (" +
				 " [TITLE]" +
				 ", [DETAIL]" +
				 " ) VALUES (" +
				 " @ssTitle" +
				 ", @ssDetail" +
				 " ) ; SELECT SCOPE_IDENTITY();";
				using(var insertSqlCmd = trans.CreateCommand(insertSql)) {
					insertSqlCmd.CreateParameter("@ssTitle", DbType.String, ssENFAQ.ssTitle);
					insertSqlCmd.CreateParameter("@ssDetail", DbType.String, ssENFAQ.ssDetail);
					outParamId = (long) Convert.ChangeType(insertSqlCmd.ExecuteScalar("Entity Action CreateFAQ", true), typeof(long));
				}
			}
		}

		/// <summary>
		/// Action: CreateOrUpdateFAQ
		/// </summary>

		public static void CreateOrUpdateFAQ(HeContext heContext, BitArray usedFields, RCFAQRecord inParamSource, out long outParamId) {
			outParamId = 0L;

			ENFAQEntityRecord ssENFAQ = inParamSource;
			using(Transaction trans = DatabaseAccess.ForCurrentDatabase.GetRequestTransaction()) {
				if (usedFields == null) {
					usedFields = new BitArray(3, true);
				}
				string updateSet = "SET NOCOUNT OFF; UPDATE " + ENFAQEntity.LocalViewName(heContext.AppInfo.Tenant.Id, BuiltInFunction.GetCurrentLocale()) + " SET ";
				string parameters = "";
				parameters = (usedFields[1] ?(StringUtils.SuffixIfNotEmpty(parameters, ", ") + "[TITLE] = @ssTitle"): parameters);
				parameters = (usedFields[2] ?(StringUtils.SuffixIfNotEmpty(parameters, ", ") + "[DETAIL] = @ssDetail"): parameters);
				string whereCondition = " WHERE [ID] = @ssENFAQssId";
				var sql = updateSet + parameters + whereCondition;
				if (string.IsNullOrEmpty(parameters)) {
					string dummyUpdate = "[TITLE] = [TITLE]";
					sql = updateSet + dummyUpdate + whereCondition;
				}
				using(var sqlCmd = trans.CreateCommand(sql)) {
					if (usedFields[1]) {
						sqlCmd.CreateParameter("@ssTitle", DbType.String, ssENFAQ.ssTitle);
					}
					if (usedFields[2]) {
						sqlCmd.CreateParameter("@ssDetail", DbType.String, ssENFAQ.ssDetail);
					}
					sqlCmd.CreateParameter("@ssENFAQssId", DbType.Int64, ssENFAQ.ssId);
					int counter = sqlCmd.ExecuteNonQueryWithoutTransformParametersSyntax("Entity Action CreateOrUpdateFAQ (update)", true);
					if (counter == 0) {
						string insertSql =
						 "SET NOCOUNT ON; INSERT INTO " + ENFAQEntity.LocalViewName(null, null) + " (" +
						 " [TITLE]" +
						 ", [DETAIL]" +
						 " ) VALUES (" +
						 " @ssTitle" +
						 ", @ssDetail" +
						 " ) ; SELECT SCOPE_IDENTITY();";
						using(var insertSqlCmd = trans.CreateCommand(insertSql)) {
							insertSqlCmd.CreateParameter("@ssTitle", DbType.String, ssENFAQ.ssTitle);
							insertSqlCmd.CreateParameter("@ssDetail", DbType.String, ssENFAQ.ssDetail);
							outParamId = (long) Convert.ChangeType(insertSqlCmd.ExecuteScalar("Entity Action CreateOrUpdateFAQ", true), typeof(long));
						}
					} else {
						outParamId = ssENFAQ.ssId;
					}
				}
			}
		}

		/// <summary>
		/// Action: DeleteFAQ
		/// </summary>

		public static void DeleteFAQ(HeContext heContext, long inParamId) {
			using(Transaction trans = DatabaseAccess.ForCurrentDatabase.GetRequestTransaction()) {
				string sql =
				 "DELETE " +
				 "FROM " + ENFAQEntity.LocalViewName(heContext.AppInfo.Tenant.Id, BuiltInFunction.GetCurrentLocale()) + " " +
				 "WHERE [ID] = @inParamId";
				using(var sqlCmd = trans.CreateCommand(sql)) {
					sqlCmd.CreateParameter("@inParamId", DbType.Int64, inParamId);
					sqlCmd.ExecuteNonQueryWithoutTransformParametersSyntax("Entity Action DeleteFAQ", true);
				}
			}
		}

		/// <summary>
		/// Action: GetFAQ
		/// </summary>

		public static void GetFAQ(HeContext heContext, long inParamId, out RCFAQRecord outParamRecord) {
			outParamRecord = new RCFAQRecord(null);

			using(Transaction trans = DatabaseAccess.ForCurrentDatabase.GetRequestTransaction()) {
				string sql =
				 "SELECT " +
				 " [ID]" +
				 ", [TITLE]" +
				 ", [DETAIL]" +
				 " FROM " + ENFAQEntity.LocalViewName(heContext.AppInfo.Tenant.Id, BuiltInFunction.GetCurrentLocale()) + " " +
				 "WHERE [ID] = @inParamId";
				using(var sqlCmd = trans.CreateCommand(sql)) {
					sqlCmd.CreateParameter("@inParamId", DbType.Int64, inParamId);
					using(IDataReader reader = sqlCmd.ExecuteReader("Entity Action GetFAQ", true, false, false)) {
						if (reader.Read()) {
							outParamRecord.ReadDB(reader);
							outParamRecord.AllOptimizedAttributes = new BitArray[] {
								new BitArray(3, false)
							};
						} else {
							throw new DataBaseException(ENFAQEntity.LocalViewName(heContext.AppInfo.Tenant.Id, BuiltInFunction.GetCurrentLocale()) + " with key " + inParamId + " was not found"); 
						}
					}
				}
			}
		}

		/// <summary>
		/// Action: GetFAQForUpdate
		/// </summary>

		public static void GetFAQForUpdate(HeContext heContext, long inParamId, out RCFAQRecord outParamRecord) {
			outParamRecord = new RCFAQRecord(null);

			using(Transaction trans = DatabaseAccess.ForCurrentDatabase.GetRequestTransaction()) {
				string sql =
				 "SELECT " +
				 " [ID]" +
				 ", [TITLE]" +
				 ", [DETAIL]" +
				 "FROM " + ENFAQEntity.LocalViewName(heContext.AppInfo.Tenant.Id, BuiltInFunction.GetCurrentLocale()) + " WITH ( UPDLOCK ) " +
				 "WHERE [ID] = @inParamId ";
				using(var sqlCmd = trans.CreateCommand(sql)) {
					sqlCmd.CreateParameter("@inParamId", DbType.Int64, inParamId);
					using(IDataReader reader = sqlCmd.ExecuteReader("Entity Action GetFAQForUpdate", true, false, false)) {
						if (reader.Read()) {
							outParamRecord.ReadDB(reader);
							outParamRecord.AllOptimizedAttributes = new BitArray[] {
								new BitArray(3, false)
							};
						} else {
							throw new DataBaseException(ENFAQEntity.LocalViewName(heContext.AppInfo.Tenant.Id, BuiltInFunction.GetCurrentLocale()) + " with key " + inParamId + " was not found"); 
						}
					}
				}
			}
		}

		/// <summary>
		/// Action: UpdateFAQ
		/// </summary>

		public static void UpdateFAQ(HeContext heContext, BitArray usedFields, RCFAQRecord inParamSource) {
			ENFAQEntityRecord ssENFAQ = inParamSource;
			using(Transaction trans = DatabaseAccess.ForCurrentDatabase.GetRequestTransaction()) {
				if (usedFields == null) {
					usedFields = new BitArray(3, true);
				}
				string updateSet = "SET NOCOUNT OFF; UPDATE " + ENFAQEntity.LocalViewName(heContext.AppInfo.Tenant.Id, BuiltInFunction.GetCurrentLocale()) + " SET ";
				string parameters = "";
				parameters = (usedFields[1] ?(StringUtils.SuffixIfNotEmpty(parameters, ", ") + "[TITLE] = @ssTitle"): parameters);
				parameters = (usedFields[2] ?(StringUtils.SuffixIfNotEmpty(parameters, ", ") + "[DETAIL] = @ssDetail"): parameters);
				string whereCondition = " WHERE [ID] = @ssENFAQssId";
				var sql = updateSet + parameters + whereCondition;
				if (string.IsNullOrEmpty(parameters)) {
					string dummyUpdate = "[TITLE] = [TITLE]";
					sql = updateSet + dummyUpdate + whereCondition;
				}
				using(var sqlCmd = trans.CreateCommand(sql)) {
					if (usedFields[1]) {
						sqlCmd.CreateParameter("@ssTitle", DbType.String, ssENFAQ.ssTitle);
					}
					if (usedFields[2]) {
						sqlCmd.CreateParameter("@ssDetail", DbType.String, ssENFAQ.ssDetail);
					}
					sqlCmd.CreateParameter("@ssENFAQssId", DbType.Int64, ssENFAQ.ssId);
					if (sqlCmd.ExecuteNonQueryWithoutTransformParametersSyntax("Entity Action UpdateFAQ", true) <= 0) {
						throw new DataBaseException(ENFAQEntity.LocalViewName(heContext.AppInfo.Tenant.Id, BuiltInFunction.GetCurrentLocale()) + " with key " + ssENFAQ.ssId + " was not updated"); 
					}
				}
			}
		}

	}
}

namespace ssBIFY {
	[XmlType("FAQ")]
	public class WORCFAQRecord {
		[System.Xml.Serialization.XmlElement("Id")]
		public long varWSId;

		[System.Xml.Serialization.XmlElement("Title")]
		public string varWSTitle;

		[System.Xml.Serialization.XmlElement("Detail")]
		public string varWSDetail;

		public WORCFAQRecord() {
			varWSId = (long) 0L;
			varWSTitle = (string) "";
			varWSDetail = (string) "";
		}

		public WORCFAQRecord(ENFAQEntityRecord r) {
			varWSId = (long) r.ssId;
			varWSTitle = BaseAppUtils.RemoveControlChars(r.ssTitle);
			varWSDetail = BaseAppUtils.RemoveControlChars(r.ssDetail);
		}

		public static RLFAQList ToRecordList(WORCFAQRecord[] array) {
			RLFAQList rl = new RLFAQList();
			if (array != null) {
				foreach(WORCFAQRecord val in array) {
					rl.Append(val);
				}
			}
			return rl;
		}

		public static WORCFAQRecord[] FromRecordList(RLFAQList rl) {
			WORCFAQRecord[] array = new WORCFAQRecord[rl == null ? 0: rl.Length];
			for (int i = 0; i < array.Length; i++) {
				array[i] = rl.Data[i];
			}
			return array;
		}
	}
}

namespace ssBIFY {
	partial struct RCFAQRecord {
		public static implicit operator WORCFAQRecord(RCFAQRecord r) {
			return new WORCFAQRecord(r.ssENFAQ);
		}

		public static implicit operator RCFAQRecord(WORCFAQRecord w) {
			RCFAQRecord r = new RCFAQRecord("");
			if (w != null) {
				r.ssENFAQ = w;
			}
			return r;
		}

	}

	partial struct ENFAQEntityRecord {
		public static implicit operator WORCFAQRecord(ENFAQEntityRecord r) {
			return new WORCFAQRecord(r);
		}

		public static implicit operator ENFAQEntityRecord(WORCFAQRecord w) {
			ENFAQEntityRecord r = new ENFAQEntityRecord("");
			if (w != null) {
				r.ssId = (long) w.varWSId;
				r.ssTitle = ((string) w.varWSTitle ?? "");
				r.ssDetail = ((string) w.varWSDetail ?? "");
			}
			return r;
		}

	}
}


namespace ssBIFY {
	[Serializable()]
	public partial class WORLFAQRecordList {
		public WORCFAQRecord[] Array;


		public WORLFAQRecordList(WORCFAQRecord[] r) {
			if (r == null)
			Array = new WORCFAQRecord[0];
			else
			Array = r;
		}
		public WORLFAQRecordList() {
			Array = new WORCFAQRecord[0];
		}

		public WORLFAQRecordList(RLFAQRecordList rl) {
			rl=(RLFAQRecordList) rl.Duplicate();
			Array = new WORCFAQRecord[rl.Length];
			while (!rl.Eof) {
				Array[rl.CurrentRowNumber] = new WORCFAQRecord(rl.CurrentRec);
				rl.Advance();
			}
		}

	}
}

namespace ssBIFY {
	partial class RLFAQRecordList {
		public static implicit operator RLFAQRecordList(WORCFAQRecord[] array) {
			RLFAQRecordList rl = new RLFAQRecordList();
			if (array != null) {
				foreach(WORCFAQRecord val in array) {
					rl.Append((RCFAQRecord) val);
				}
			}
			return rl;
		}
		public static implicit operator WORCFAQRecord[](RLFAQRecordList rl) {
			WORCFAQRecord[] array = new WORCFAQRecord[rl == null ? 0: rl.Length];
			for (int i = 0; i < array.Length; i++) {
				array[i] = (RCFAQRecord) rl.Data[i];
			}
			return array;
		}
	}
}

namespace ssBIFY {
	partial class WORLFAQRecordList {
		public static implicit operator RLFAQRecordList(WORLFAQRecordList w) {
			return w.Array;
		}
		public static implicit operator WORLFAQRecordList(RLFAQRecordList rl) {
			return new WORLFAQRecordList(rl);
		}
		public static implicit operator WORCFAQRecord[](WORLFAQRecordList w) {
			if (w != null) {
				return w.Array;
			}
			return null;
		}
		public static implicit operator WORLFAQRecordList(WORCFAQRecord[] array) {
			return new WORLFAQRecordList(array);
		}
	}
}

