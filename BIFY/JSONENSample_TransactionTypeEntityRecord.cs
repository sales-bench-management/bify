﻿/* 
 This source code (the "Generated Software") is generated by the OutSystems Platform 
 and is licensed by OutSystems (http://www.outsystems.com) to You solely for testing and evaluation 
 purposes, unless You and OutSystems have executed a specific agreement covering the use terms and 
 conditions of the Generated Software, in which case such agreement shall apply. 
*/

using System;
using System.Linq;
using OutSystems.HubEdition.RuntimePlatform;
using OutSystems.HubEdition.RuntimePlatform.Db;
using OutSystems.Internal.Db;
using OutSystems.RuntimeCommon;
using OutSystems.RuntimeCommon.Caching;

using System.Text;
using System.Data;
using System.Collections.Generic;
using Newtonsoft.Json;
using OutSystems.RESTService;
using OutSystems.RESTService.Behaviors;
using OutSystems.RESTService.Controllers;
using OutSystems.ObjectKeys;
using OutSystems.HubEdition.RuntimePlatform.NewRuntime;


namespace ssBIFY.RestRecords {

	public class JSONENSample_TransactionTypeEntityRecord: AbstractRESTStructure<ENSample_TransactionTypeEntityRecord> {
		[JsonProperty("Id")]
		public int? AttrId;

		[JsonProperty("Label")]
		public string AttrLabel;

		public JSONENSample_TransactionTypeEntityRecord() {}

		public JSONENSample_TransactionTypeEntityRecord(ENSample_TransactionTypeEntityRecord s, IBehaviorsConfiguration config) {
			if (config.DefaultValuesBehavior == DefaultValuesBehavior.DontSend) {
				AttrId = (int?) s.ssId;
				AttrLabel = s.ssLabel;
			} else {
				AttrId = (int?) s.ssId;
				AttrLabel = s.ssLabel;
			}
		}

		public static Func<ssBIFY.RestRecords.JSONENSample_TransactionTypeEntityRecord, ENSample_TransactionTypeEntityRecord> ToStructureDelegate(IBehaviorsConfiguration config) {
			return (ssBIFY.RestRecords.JSONENSample_TransactionTypeEntityRecord s) => ToStructure(s, config);
		}
		public static ENSample_TransactionTypeEntityRecord ToStructure(ssBIFY.RestRecords.JSONENSample_TransactionTypeEntityRecord obj, IBehaviorsConfiguration config) {
			ENSample_TransactionTypeEntityRecord s = new ENSample_TransactionTypeEntityRecord(null);
			if (obj != null) {
				s.ssId = obj.AttrId == null ? 0: obj.AttrId.Value;
				s.ssLabel = obj.AttrLabel == null ? "": obj.AttrLabel;
			}
			return s;
		}

		public static Func<ENSample_TransactionTypeEntityRecord, ssBIFY.RestRecords.JSONENSample_TransactionTypeEntityRecord> FromStructureDelegate(IBehaviorsConfiguration config) {
			return (ENSample_TransactionTypeEntityRecord s) => FromStructure(s, config);
		}
		public static ssBIFY.RestRecords.JSONENSample_TransactionTypeEntityRecord FromStructure(ENSample_TransactionTypeEntityRecord s, IBehaviorsConfiguration config) {
			return new ssBIFY.RestRecords.JSONENSample_TransactionTypeEntityRecord(s, config);
		}

	}



}