﻿<%@ Control Language="c#" AutoEventWireup="false" Codebehind="Blocks\SocialLoginSamples\Common\Menu.ascx.cs" Inherits="ssSocialLoginSamples.Flows.FlowCommon.WBlkMenu,SocialLoginSamples" TargetSchema="http://schemas.microsoft.com/intellisense/ie5"%>
<%@ Register TagPrefix="osweb" Namespace="OutSystems.HubEdition.WebWidgets" Assembly="OutSystems.HubEdition.WebWidgets" %>
<%@ Assembly Name="OutSystems.WidgetsRuntimeAPI" %>
<%@ Import namespace="ssSocialLoginSamples" %>
<%# PageStartHook() %><osweb:Container runat="server" id="wt3" anonymous="true" onDataBinding="cnt3_onDataBinding" cssClass="Application_Menu"><osweb:Container runat="server" id="wt2" anonymous="true" onDataBinding="cnt2_onDataBinding" cssClass="Menu_TopMenus"></osweb:Container></osweb:Container><%# PageEndHook() %>