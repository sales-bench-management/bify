﻿<%@ Control Language="c#" AutoEventWireup="false" Codebehind="Blocks\OutSystemsUIWeb\Controls\ButtonGroup.ascx.cs" Inherits="ssOutSystemsUIWeb.Flows.FlowControls.WBlkButtonGroup,OutSystemsUIWeb" TargetSchema="http://schemas.microsoft.com/intellisense/ie5"%>
<%@ Register TagPrefix="osweb" Namespace="OutSystems.HubEdition.WebWidgets" Assembly="OutSystems.HubEdition.WebWidgets" %>
<%@ Assembly Name="OutSystems.WidgetsRuntimeAPI" %>
<%@ Import namespace="ssOutSystemsUIWeb" %>
<%# PageStartHook() %><osweb:PlaceholderContainer runat="server" id="wtContent" onDataBinding="wtContent_onDataBinding" cssClass="button-group" GridCssClasses="OSInline"></osweb:PlaceholderContainer><%# PageEndHook() %>