﻿/* Manually triggers the balloon according to the value of the search */
var SearchBalloon = function() {
    
    // Initial variables
    var input;
    var balloon;
    var searchFocus;
    var hasListeners = false;
    
    var openBalloon = function(searchFocus) {
        searchFocus._tippy.show();
    };
    
    var closeBalloon = function(searchFocus) {
        searchFocus._tippy.hide();
    };
    
    var addEventListeners = function() {
        if(input) {
            var isActive = searchFocus.classList.contains('tippy-active');
            hasListeners = true;
            
            input.addEventListener('focus', function() {
                if(input.value.length > 0 && !isActive) {
                    openBalloon(searchFocus);
                }
            });
            
            input.addEventListener('input', function() {
                if(input.value.length > 0 && !isActive) {
                    openBalloon(searchFocus);
                } else if(input.value.length === 0) {
                    closeBalloon(searchFocus);
                }
            });
            
            input.addEventListener('blur', function() {
                if(isActive) {
                    closeBalloon(searchFocus);
                }
            });
        }
    };
    
    // Starts the Balloon
    var start = function(widgetId, balloonId, search) {
        // Sets objects and variables
        input = document.querySelector('#' + widgetId + ' input');
        balloon = document.querySelector('#' + balloonId);
        searchFocus = document.querySelector('#' + search);
        
        if(!hasListeners) {
            // Add event listeners
            addEventListeners();
        }
        
        // Disables auto complete
        if(input) {
            input.setAttribute('autocomplete', 'off');
        }
    };
    
    return {
        init: function(widgetId, balloonId, search) {
            start(widgetId, balloonId, search);
            osAjaxBackend.BindAfterAjaxRequest(function() {
                start(widgetId, balloonId, search);
            });
        }
    };
}
