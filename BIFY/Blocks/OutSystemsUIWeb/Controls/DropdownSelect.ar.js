﻿var newDropdownSelect = function() {
    
    // Objects
    var elementId;
    var elementClass;
    var extendedOptions;
    var choices;
    var options;
    
    var start = function(widgetId, classParam, optionsParam, advancedFormat) {
        elementId = document.getElementById(widgetId);
        
        if(classParam.length > 0) {
            elementClass = classParam;
        }
          
        extendedOptions = advancedFormat;
        options = {
            noResultsText: optionsParam.noResultsText,
            searchResultLimit: optionsParam.searchResultLimit,
            searchEnabled: optionsParam.searchEnabled,
            shouldSort: false,
            callbackOnInit: onInit,
        };
        
        if(extendedOptions.length > 0) {
            var advancedObj = eval('(' + extendedOptions + ')');
            options = mergeJSON(options, advancedObj);
        }
        
        if(elementId) {
            choices = new Choices('#' + widgetId, options);
        };
        
        // AdvancedFormat function
        function mergeJSON(target, add) {
            function isObject(obj) {
                if (typeof obj == 'object') {
                    for (var key in obj) {
                        if (obj.hasOwnProperty(key)) {
                            return true; // search for first object prop
                        }
                    }
                }
                return false;
            }
            for (var key in add) {
                if (add.hasOwnProperty(key)) {
                    if (target[key] && isObject(target[key]) && isObject(add[key])) {
                        mergeJSON(target[key], add[key]);
                    } else {
                        target[key] = add[key];
                    }
                }
            }
            return target;
        };
        
        // Recursive Function for closest() in IE
        var closestFn = function(element, classToSearch) {
            if(element && element.classList.contains(classToSearch)) {
                return element;
            } else if(!element || element && element.classList.contains('layout')) {
                return false;
            } else {
                return closestFn(element.parentElement, classToSearch);
            }
        };
        
        function isEmpty(str) {
            return (!str || 0 === str.length);
        }
        
       // Function to create div around search input (necessary for css search icon)
       var createEl = function(query, tag, className, innerHTML) {
           var queryList = document.querySelectorAll(query);
           
           for(var i = 0; i < queryList.length; i++) {
                var elem = queryList[i];
                var wrapper = document.createElement(tag);
                if(!isEmpty(innerHTML)) { 
                    wrapper.innerHTML = innerHTML 
                }
                
                if(!closestFn(elem, className)){
                    elem.parentElement.insertBefore(wrapper, elem);
                    wrapper.appendChild(elem);
                    wrapper.classList.add(className);
                    
                }
           }
        };
        
        // Creates div around search input
        createEl('.choices__input--cloned', 'div', 'search--wrapper', '');
        
        // Add title do input
        elementId.setAttribute('title', 'Search');
        
        // Get all elements with class .choices
        var choicesClass = document.querySelectorAll('.choices');
        var choicesListClass = document.querySelectorAll('.choices__list');
        
        // Check for rtl and set correct attribute on every element
        if(document.body.classList.contains('is-rtl')) {
            for(var i = 0; i < choicesClass.length; i++) {
                choicesClass[i].setAttribute('dir', 'rtl');
            }
            for(var i = 0; i < choicesListClass.length; i++) {
                choicesListClass[i].setAttribute('dir', 'rtl');
            }
        };
        
        // Verify if select has the attribute disabled        
        var choicesSelect = document.querySelectorAll('.choices');
        
        if(choicesSelect) {
            for(var i = 0; i < choicesSelect.length; i++) {
                var choicesDisabled = choicesSelect[i].querySelector('.select');
                
                if(choicesDisabled.hasAttribute('disabled')) {
                    choicesSelect[i].classList.add('choices-disabled');
                };       
            };
        };                
    };

    var onInit = function() {
        
        // Get select input
        var selectElement = document.querySelector(this.element);
        var choicesElement = this.containerOuter;
        
        // Apply the same CSS classes from the select element to the Choices wrapper
        var selectClasses = selectElement.classList.value;
        var choicesClasses = choicesElement.classList.value;        
        choicesElement.classList.value = choicesClasses + " " + selectClasses + " " + elementClass;
        
        // Remove unwanted CSS classes from the Choices wrapper
        choicesElement.classList.remove("select");
        choicesElement.classList.remove("Mandatory");
        choicesElement.classList.remove("choices__input");
        choicesElement.classList.remove("is-hidden");
        
        // Display the select input in order to have OutSystems client side validations
        selectElement.style.display = "block";
        
        // Check for validation changes using the CSS class
        var observer = new MutationObserver(function (event) {
            if(event[0].target.classList.contains("Not_Valid")) {                
                choicesElement.classList.add("Not_Valid");
            } else {
                choicesElement.classList.remove("Not_Valid");
            }
        })

        observer.observe(selectElement, {
            attributes: true, 
            attributeFilter: ['class'],
            childList: false, 
            characterData: false
        })
    }

    return {
        init: function(widgetId, classParam, optionsParam, advancedFormat) {
            start(widgetId, classParam, optionsParam, advancedFormat);
        }
    };
};