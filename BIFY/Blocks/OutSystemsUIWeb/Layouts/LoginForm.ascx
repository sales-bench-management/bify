﻿<%@ Control Language="c#" AutoEventWireup="false" Codebehind="Blocks\OutSystemsUIWeb\Layouts\LoginForm.ascx.cs" Inherits="ssOutSystemsUIWeb.Flows.FlowLayouts.WBlkLoginForm,OutSystemsUIWeb" TargetSchema="http://schemas.microsoft.com/intellisense/ie5"%>
<%@ Register TagPrefix="osweb" Namespace="OutSystems.HubEdition.WebWidgets" Assembly="OutSystems.HubEdition.WebWidgets" %>
<%@ Assembly Name="OutSystems.WidgetsRuntimeAPI" %>
<%@ Import namespace="ssOutSystemsUIWeb" %>
<%# PageStartHook() %><osweb:PlaceholderContainer runat="server" id="wtLogin" onDataBinding="wtLogin_onDataBinding" cssClass="layout-login-form"></osweb:PlaceholderContainer><%# PageEndHook() %>