﻿<%@ Control Language="c#" AutoEventWireup="false" Codebehind="Blocks\OutSystemsUIWeb\Structure\AlignCenter.ascx.cs" Inherits="ssOutSystemsUIWeb.Flows.FlowStructure.WBlkAlignCenter,OutSystemsUIWeb" TargetSchema="http://schemas.microsoft.com/intellisense/ie5"%>
<%@ Register TagPrefix="osweb" Namespace="OutSystems.HubEdition.WebWidgets" Assembly="OutSystems.HubEdition.WebWidgets" %>
<%@ Assembly Name="OutSystems.WidgetsRuntimeAPI" %>
<%@ Import namespace="ssOutSystemsUIWeb" %>
<%# PageStartHook() %><osweb:PlaceholderContainer runat="server" id="wtContent" onDataBinding="wtContent_onDataBinding" cssClass="center-align"></osweb:PlaceholderContainer><%# PageEndHook() %>