﻿var Tooltip = function() {
    
    /* Object variables */
    var widget;
    var content;
    var visible;
    var trigger;
    var position;
    var tippyObj;
    
    /* Starting functions */
    var start = function(widgetId, contentId, visibleParam, positionParam, triggerParam, onShowId, onHideId, advancedFormat) {
         // Sets objects and variables
        widget = document.getElementById(widgetId);
        content = document.getElementById(contentId);
        visible = (visibleParam === 'True');
        trigger = triggerParam === "hover" ? "mouseenter focus" : triggerParam;
        
        if(positionParam.indexOf("bottom") !== -1) {
            position = "bottom";
        } else if(positionParam.indexOf("top") !== -1) {
            position = "top";
        } else if(positionParam.indexOf("left") !== -1) {
            position = "left";
        } else if(positionParam.indexOf("right") !== -1) {
            position = "right";
        }
        
        var options = {
            content: content,
            arrow: true,
            placement: position,
            trigger: trigger,
            interactive: true,
            zIndex: 102,
            showOnInit: visible,
            appendTo: function(ref) {
                return ref.parentNode
            },
            onShow: function() { // done to add accessibility to the balloon the same way that we had on the previous version
                content.setAttribute('aria-hidden','false');
                content.setAttribute('aria-expanded','true');
                widget.setAttribute('aria-expanded','true');
                
                //Check if IE and addEventListener to close tooltip/ballon on focus out
                if(document.querySelector('.ie11') && trigger === "mouseenter focus"){
                    setTimeout(function(){
                        widget.parentElement.querySelector('.tippy-popper').addEventListener("blur", closeTippyObj);
                    }, 0);
                }
                
                TriggerEvents(onShowId);
            },
            onHide: function() { // done to add accessibility to the balloon the same way that we had on the previous version
                content.setAttribute('aria-hidden','true');
                content.setAttribute('aria-expanded','false');
                widget.setAttribute('aria-expanded','false');
                
                TriggerEvents(onHideId);
            }
        };
    
        if(advancedFormat.length > 0) {
            advancedFormat = eval('(' + advancedFormat + ')');
            options = mergeJSON(options, advancedFormat);
        }
        
        tippy(widget, options);
        tippyObj = tippy;
    };  
    
    //Action to close tooltips and balloon on focus out
    function closeTippyObj() {
        tippyObj.hideAllPoppers();
        widget.parentElement.querySelector('.tippy-popper').removeEventListener("blur", closeTippyObj);
    }; 
    
    function mergeJSON(target, add) {
        function isObject(obj) {
            if (typeof obj == "object") {
                for (var key in obj) {
                    if (obj.hasOwnProperty(key)) {
                        return true; // search for first object prop
                    }
                }
            }
            return false;
        }
        for (var key in add) {
            if (add.hasOwnProperty(key)) {
                if (target[key] && isObject(target[key]) && isObject(add[key])) {
                    mergeJSON(target[key], add[key]);
                } else {
                    target[key] = add[key];
                }
            }
        }
        return target;
    };
    
    return {
        init: function(widgetId, contentId, visibleParam, positionParam, triggerParam, onShowId, onHideId, advancedFormat) {
            start(widgetId, contentId, visibleParam, positionParam, triggerParam, onShowId, onHideId, advancedFormat);
        }
    };
}