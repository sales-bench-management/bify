﻿var CardBackground = function() {
    // Objects
    var isIE;
    var cardBackground;
    var cardBackgroundImage;
    var cardBackgroundImageURL;
    
    var start = function(widgetId) {
        cardBackground = document.getElementById(widgetId);

        // detect IE11
        isIE = document.body.classList.contains('ie11');
    
        if(isIE) { 
            cardBackground.classList.add('card-background-ie');       
            cardBackgroundImage = cardBackground.querySelector('.card-background-image');
            cardBackgroundImageURL = cardBackgroundImage.getElementsByTagName('img')[0].getAttribute('src');           
            cardBackgroundImage.style.backgroundImage = 'url('+ cardBackgroundImageURL +')';                   
        };
    };
         
    return {
        init: function(widgetId) {
            start(widgetId);
        }
    };
};