﻿/* Position, show and hide balloon */
var newBalloon = function() {
    
    /* Object variables */
    var widget;
    var balloonWrapper;
    var position;
    var trigger;
    var advancedFormat;
    var tippyObj;
    
    /* Starting functions */
    var start = function(widgetId, balloonId, positionParam, triggerParam, advancedFormatParam, onShowId, onHideId) {
        // Sets objects and variables
        widget = document.getElementById(widgetId);
        balloonWrapper = document.getElementById(balloonId);
        trigger = triggerParam === "hover" ? "mouseenter focus" : triggerParam;
        
        if(positionParam.indexOf("bottom") !== -1) {
            position = "bottom";
        } else if(positionParam.indexOf("top") !== -1) {
            position = "top";
        } else if(positionParam.indexOf("left") !== -1) {
            position = "left";
        } else if(positionParam.indexOf("right") !== -1) {
            position = "right";
        }
        
        var options = {
            content: balloonWrapper,
            arrow: true,
            theme: 'light',
            placement: position,
            trigger: trigger,
            interactive: true,
            zIndex: 102,
            appendTo: function(ref) {
                if(!ref.parentNode.classList.contains('search-balloon')) { // used to identify when it's being used inside of a search balloon
                    // or by itself
                    ref.parentNode.classList.add('only-balloon');
                }
            
                return ref.parentNode
            },
            onShow: function() { // done o add accessibility to the balloon the same way that we had on the previous version
                balloonWrapper.setAttribute('aria-hidden','false');
                balloonWrapper.setAttribute('aria-expanded','true');
                widget.setAttribute('aria-expanded','true');
                
                //Check if IE and addEventListener to close tooltip/ballon on focus out
                if(document.querySelector('.ie11') && trigger === "mouseenter focus"){
                    setTimeout(function(){
                        widget.parentElement.querySelector('.tippy-popper').addEventListener("blur", closeTippyObj);
                    }, 0);
                }
                
                TriggerEvents(onShowId);
            },
            onHide: function() { // done o add accessibility to the balloon the same way that we had on the previous version
                balloonWrapper.setAttribute('aria-hidden','true');
                balloonWrapper.setAttribute('aria-expanded','false');
                widget.setAttribute('aria-expanded','false');
                
                TriggerEvents(onHideId);
                    
            }
        };
        
        if(advancedFormatParam.length > 0) {
            advancedFormat = eval('(' + advancedFormatParam + ')');
            options = mergeJSON(options, advancedFormat);
        }
        
        tippy(widget, options);
        tippyObj = tippy;
    };
    
    //Action to close tooltips and balloon on focus out
    function closeTippyObj() {
        tippyObj.hideAllPoppers();
        widget.parentElement.querySelector('.tippy-popper').removeEventListener("blur", closeTippyObj);
    };
    
    function mergeJSON(target, add) {
        function isObject(obj) {
            if (typeof obj == "object") {
                for (var key in obj) {
                    if (obj.hasOwnProperty(key)) {
                        return true; // search for first object prop
                    }
                }
            }
            return false;
        }
        for (var key in add) {
            if (add.hasOwnProperty(key)) {
                if (target[key] && isObject(target[key]) && isObject(add[key])) {
                    mergeJSON(target[key], add[key]);
                } else {
                    target[key] = add[key];
                }
            }
        }
        return target;
    };

    return {
        init: function(widgetId, balloonId, positionParam, triggerParam, advancedFormatParam, onShowEvent, onHideEvent) {
            start(widgetId, balloonId, positionParam, triggerParam, advancedFormatParam, onShowEvent, onHideEvent);
        }
    };
}
