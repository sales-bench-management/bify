﻿function OpenConfirmationDialog(PopupMainContainer,ButtonId){    
    //OPEN CONFIRMATION DIALOG WINDOW
    $('.conf-dialog-holder'+ButtonId.replace(/:/g, "_")).show();
    $('.conf-dialog-background'+ButtonId.replace(/:/g, "_")).show();
    //CENTER CONFIRMATION DIALOG WINDOW
    $("#" + PopupMainContainer).css({
        'top': (Math.abs(($(window).height() / 2) - ($("#" + PopupMainContainer).height() / 2))),
        'left': (Math.abs(($(window).width() / 2) - ($("#" + PopupMainContainer).width() / 2)))
    });
    return false;
}


function ConfirmationDialog_Cancel(PopupMainContainer,ButtonId){
    //CLOSE CONFIRMATION DIALOG
    $('.conf-dialog-holder'+ButtonId.replace(/:/g, "_")).hide();
    $('.conf-dialog-background'+ButtonId.replace(/:/g, "_")).hide();
}


function ConfirmationDialog_Continue(PopupMainContainer,ButtonId,eventClick){
    //CLOSE CONFIRMATION DIALOG
   
    $('.conf-dialog-holder'+ButtonId.replace(/:/g, "_")).hide();
    $('.conf-dialog-background'+ButtonId.replace(/:/g, "_")).hide();
    
    //ReBind Button EventClick
    if (eventClick != "") {
        document.getElementById(ButtonId).setAttribute("onclick",eventClick);
    }
    
    //Click button to trigger original button event
    $('#' + ButtonId)[0].click();
    
    //Rebing button to confirm again 
    if (eventClick != "") {
       document.getElementById(ButtonId).setAttribute("onclick","return OpenConfirmationDialog('" + PopupMainContainer + "','" + ButtonId + "')");
    }
    return true;
}


function move(tagEvent, tag) {
    var Jx = 0;
    var Jy = 0;
    var JiTopo = 20;
    var JiBase = 20;
    $("#" + tagEvent).css({
        'cursor':'move'
    });
    $(tag).css({"left":"0px", "top":"0px"});
    
    cliq = 0;
    
    $("#" + tagEvent).mousedown(function(cxy){
        JiTopo = Math.abs($(window).scrollLeft() - $("#" + tag).position().left);
        JiBase = Math.abs($(window).scrollTop() -  $("#" + tag).position().top);
        
        cliq = 0;
        
        Jx = Math.abs($(window).scrollLeft() - cxy.pageX);
        Jy = Math.abs($(window).scrollTop() - cxy.pageY);
        
        $(document).mousemove(function(e){
            $('body').css('cursor','move');
            cliq = 1;
            
            if (Math.abs($(window).scrollLeft() - $("#" + tag).position().left) > 0)
                $("#" + tag).css({"left":(JiTopo + (Math.abs($(window).scrollLeft() - e.pageX) - Jx))});
            else
                $("#" + tag).css({"left": 1});
            if (Math.abs($(window).scrollTop() -  $("#" + tag).position().top) > 0)
                $("#" + tag).css({"top":(JiBase + (Math.abs($(window).scrollTop() - e.pageY) - Jy))});
            else
                $("#" + tag).css({"top": 1});
            
            return false; // To object is not selected.
        });
        
        return false; // Prevent selection on drag.
    });
    $(document).mouseup(function(c){
        $(this).unbind('mousemove');
        $('body').css('cursor','default');
    });
 }