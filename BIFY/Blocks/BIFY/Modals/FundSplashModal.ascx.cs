﻿/* 
 This source code (the "Generated Software") is generated by the OutSystems Platform 
 and is licensed by OutSystems (http://www.outsystems.com) to You solely for testing and evaluation 
 purposes, unless You and OutSystems have executed a specific agreement covering the use terms and 
 conditions of the Generated Software, in which case such agreement shall apply. 
*/

using System;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Web.SessionState;
using System.IO;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Xml;
using OutSystems.HubEdition.RuntimePlatform;
using OutSystems.HubEdition.RuntimePlatform.Internal;
using OutSystems.HubEdition.RuntimePlatform.Log;
using OutSystems.HubEdition.RuntimePlatform.Db;
using OutSystems.Internal.Db;
using OutSystems.HubEdition.RuntimePlatform.Web;
using OutSystems.HubEdition.RuntimePlatform.Email;
using OutSystems.HubEdition.RuntimePlatform.NewRuntime;
using OutSystems.HubEdition.WebWidgets;
using OutSystems.HubEdition.WebWidgets.Behaviors;
using OutSystems.RuntimeCommon;
using OutSystems.RuntimeCommon.Caching;
using OutSystems.ObjectKeys;
using System.Text.RegularExpressions;
using System.Configuration;
using System.Runtime.Serialization;
using System.Text;
using System.Linq;


namespace ssBIFY.Flows.FlowModals {
	public abstract class WBlkFundSplashModal: OSUserControl, IWebScreen, INegotiateTabIndexes, IAjaxNotifyEvent, INotifyTriggers, INotifySender {

		/// <summary>
		/// Delegate Definitions
		/// </summary>
		/// <summary>
		/// Parameters and Local Variables Definitions
		/// </summary>
		/// <summary>
		/// Screen Input Parameter inParamFundId. Description:
		/// </summary>
		public long inParamFundId = 0L;
		/// <summary>
		/// Screen Input Parameter inParamFundSplashTo. Description:
		/// </summary>
		public int inParamFundSplashTo = 0;
		/// <summary>
		/// Screen Input Parameter inParamReturn. Description: Used for Returning From a Fund Donation
		/// </summary>
		public bool inParamReturn = false;
		/// <summary>
		/// Screen Input Parameter inParamSuccess. Description: Used for Returning From a Fund Donation
		/// </summary>
		public bool inParamSuccess = false;
		/// <summary>
		/// Variable <code>varLcShowBuyingModal</code> that represents the Service Studio local variable
		///  <code>ShowBuyingModal</code> <p>Description: </p>
		/// </summary>
		bool varLcShowBuyingModal = false;

		protected OutSystems.HubEdition.WebWidgets.Container wt_Container4;
		/// <summary>
		/// Variable "True" if the Widget wt_If3
		/// </summary>
		protected OutSystems.HubEdition.WebWidgets.If wt_If3T;

		/// <summary>
		/// Variable "True" if the Widget wt_If3
		/// </summary>
		protected OutSystems.HubEdition.WebWidgets.If wt_If3F;
		/// <summary>
		/// Variable OutSystemsUIWeb_wtModal
		/// </summary>
		protected proxy_BIFY_OutSystemsUIWeb.Flows.FlowContent.WBlkModal OutSystemsUIWeb_wtModal;
		/// <summary>
		/// Variable (wt_Link12) with Link component
		/// </summary>
		protected OutSystems.HubEdition.WebWidgets.LinkButton wt_Link12;
		/// <summary>
		/// Variable "True" if the Widget wtbuyingmodal
		/// </summary>
		protected OutSystems.HubEdition.WebWidgets.If wtbuyingmodalT;

		/// <summary>
		/// Variable "True" if the Widget wtbuyingmodal
		/// </summary>
		protected OutSystems.HubEdition.WebWidgets.If wtbuyingmodalF;
		protected OutSystems.HubEdition.WebWidgets.Container wt_Container11;
		private List<object> explicitChangedVariables = new List<object>();
		private bool _isRendering = false;
		public HeContext heContext;
		private static Hashtable htTabIndexGroups = new Hashtable();
		private Hashtable htTabIndexGroupsTI = new Hashtable();
		public string InstanceID;
		public string RuntimeID= "";
		public event EventHandler NotifyTriggered;
		public event Action ssPassBackBuyTriggered;
		private BlocksJavascript.JavascriptNode _javascriptNode;
		/// <summary>
		/// Action <code>Preparation</code> that represents the Service Studio preparation
		///  <code>Preparation</code> <p> Description: </p>
		/// </summary>
		public void Preparation(HeContext heContext) {
			CheckPermissions(heContext);
			RequestTracer perfTracer = heContext.RequestTracer; perfTracer.RegisterAction("ac80e5c4-73fc-416a-bb6c-6b09934c1dff.#Preparation", "FundSplashModal.Preparation");
			try {
				if (heContext != null && heContext.RequestTracer != null) {
					heContext.RequestTracer.RegisterInternalCall("xOWArPxzakG7bGsJk0wd_w.#Preparation", "Preparation", "1WMl+O7_wkOQDYQl1Ck5zQ", "BIFY");
				}
				if (heContext != null) {
					heContext.RequestCancelation.ThrowIfCanceled();
				}
				if ((inParamReturn)) {
					// ShowBuyingModal = True
					varLcShowBuyingModal = true;
				} else {
					// ToggleModal
					if (heContext != null) {
						heContext.RequestCancelation.ThrowIfCanceled();
					}
					Actions.ActionToggleModal(heContext, OutSystemsUIWeb_wtModal.ClientID, "");

					// AddBodyFix
					if (heContext != null) {
						heContext.RequestCancelation.ThrowIfCanceled();
					}
					Actions.ActionAddBodyFix(heContext);

				}

			} catch (System.Threading.ThreadAbortException) {
				throw;
			}
		}
		static WBlkFundSplashModal() {
		}

		public void OnNotifyCalled(string message) {
			BindDelegatesIfNeeded();
			if (NotifyTriggered != null) {
				NotifyTriggered(this, new MsgEventArgs(message));
			}
		}

		override protected void OnInit(EventArgs e) {
			InitializeComponent();
			base.OnInit(e);
		}
		private T HandleCurrentEspaceKey<T>(Func<T> action) {
			ObjectKey oldCurrentESpaceKey = heContext.CurrentESpaceKey;
			try {
				heContext.CurrentESpaceKey = ssBIFY.Global.eSpaceKey;
				return action();
			} finally {
				heContext.CurrentESpaceKey = oldCurrentESpaceKey;
			}
		}
		private void InitializeComponent() {
			OutSystemsUIWeb_wtModal = (proxy_BIFY_OutSystemsUIWeb.Flows.FlowContent.WBlkModal) FindControl("OutSystemsUIWeb_wtModal");
			wt_Link12 = (OutSystems.HubEdition.WebWidgets.LinkButton) FindControl("wt_Link12");
			if (this.wt_Link12 != null) {
				this.wt_Link12.Click += new System.EventHandler(this.wt_Link12_Click);
			}
			this.Load += new System.EventHandler(this.Page_Load);
			this.Unload += new System.EventHandler(this.Page_Unload);
		}
		private void Page_Load(object sender, System.EventArgs e) {
			((OSPageViewState) Page).EnableResetViewState();
			heContext = Global.App.OsContext;
			RuntimeID = ClientID;
			if (!Visible) return;
			ObjectKey oldCurrentESpaceKey = heContext.CurrentESpaceKey;
			try {
				heContext.CurrentESpaceKey = ssBIFY.Global.eSpaceKey;
				if (!IsPostBack || _isRendering) {
					// register this block in the page so for later outputting the block javascript includes in their correct order
					((OSPage) Page).RegisterBlock(this, out _javascriptNode);
					if (_isRendering) {
						Preparation(heContext);
					}
					bool bindEditRecords = IsViewStateEmpty;
				} else {
					FetchViewState();
				}
			} finally {
				heContext.CurrentESpaceKey = oldCurrentESpaceKey;
			}

		}
		/// <summary>
		/// This method is called when there is a submit. So it should validate input values and call the
		///  correct system event user action if needed
		/// </summary>
		public void OnSubmit(string parentEditRecord, bool validate) {
			if (!WasRendered) {
				return;
			}
			CallOnSubmitOnChildren(Controls, parentEditRecord, validate);
		}
		public void CallOnSubmitOnChildren(ControlCollection children, string parentEditRecord, bool validate) {
			foreach(Control ctrl in children) {
				IWebScreen screen = ctrl as IWebScreen;

				if (screen != null) {
					screen.OnSubmit(parentEditRecord, validate);
				} else {
					CallOnSubmitOnChildren(ctrl.Controls, parentEditRecord, validate);
				}
			}
		}
		public short NegotiateTabIndexes(short tabindex, bool setTabIndex) {
			Control rootCtrl = this;
			if ((this.Controls.Count == 1) && (typeof(HtmlForm).IsInstanceOfType(this.Controls[0]))) {
				rootCtrl = this.Controls[0];
			} else {
				rootCtrl = this;
			}
			tabindex = NegotiateTabIndexesRecursively(tabindex, rootCtrl, setTabIndex);
			return tabindex;
		}

		public short NegotiateTabIndexesRecursively(short tabindex, Control rootCtrl, bool setTabIndex) {

			bool bAssignTabIndex = false;
			WebControl ctrl = null;
			HtmlControl htmlCtrl = null;
			foreach(Control child in rootCtrl.Controls) {
				if (child is INegotiateTabIndexes) {
					tabindex = ((INegotiateTabIndexes) child).NegotiateTabIndexes(tabindex, setTabIndex);
					continue;
				}
				if (typeof(WebControl).IsInstanceOfType(child)) {
					ctrl = (WebControl) child;
					bAssignTabIndex = false;
					if (ctrl is OutSystems.HubEdition.WebWidgets.TextBox | ctrl is OutSystems.HubEdition.WebWidgets.CheckBox | ctrl is OutSystems.HubEdition.WebWidgets.RadioButton | ctrl is OutSystems.HubEdition.WebWidgets.DropDownList) {

						bAssignTabIndex = true;
					} else if (ctrl is System.Web.UI.WebControls.LinkButton | ctrl is System.Web.UI.WebControls.Button | ctrl is System.Web.UI.WebControls.HyperLink | ctrl is System.Web.UI.WebControls.ListBox) {
						bAssignTabIndex = true;
					}
					else if (ctrl is PlaceholderContainer)
					{
						INegotiateTabIndexes placeholderOwner = (INegotiateTabIndexes) Utils.GetOwnerOfControl(ctrl);
						tabindex = placeholderOwner.NegotiateTabIndexesRecursively(tabindex, ctrl, setTabIndex);
						continue;
					}

					short settedTabIndex = 0;
					if (bAssignTabIndex && setTabIndex) {
						object b = htTabIndexGroups[ctrl.ID];
						if (b != null) {
							string groupid = b.ToString();
							if (htTabIndexGroupsTI[groupid] == null) {
								htTabIndexGroupsTI[groupid] = tabindex++;
							}
							ViewStateAttributes.SetTabIndex(ctrl, Convert.ToInt16(htTabIndexGroupsTI[groupid]), out settedTabIndex);
						} else {
							ViewStateAttributes.SetTabIndex(ctrl, tabindex, out settedTabIndex);
							// Increase tabindex if it was not overiden
							if (tabindex == settedTabIndex) {
								tabindex++;
							}
						}
					}
					tabindex = Math.Max(tabindex, ++settedTabIndex);
				} else if (child is HtmlControl && setTabIndex) {
					htmlCtrl = (HtmlControl) child;
					if (htmlCtrl is System.Web.UI.HtmlControls.HtmlInputFile) {
						htmlCtrl.Attributes.Add("tabIndex", Convert.ToString(tabindex++));
					}
				}
				if (child.Controls.Count > 0) {
					tabindex = NegotiateTabIndexesRecursively(tabindex, child, setTabIndex);
				}
			}
			return tabindex;
		}


		/// <summary>
		/// Store widget and variable data in the viewstate
		/// </summary>
		public override void StoreViewState() {
			base.StoreViewState();
			ViewStateAttributes.SetInViewState("inParamFundId", inParamFundId, 0L);
			ViewStateAttributes.SetInViewState("inParamReturn", inParamReturn, false);
			ViewStateAttributes.SetInViewState("inParamSuccess", inParamSuccess, false);
			ViewStateAttributes.SetInViewState("varLcShowBuyingModal", varLcShowBuyingModal, false);
			ViewStateAttributes.EnsureNotEmpty();
			RemoveStoreViewStateWebScreenStack(this);
		}
		/// <summary>
		/// Restore widget and variable data from the viewstate
		/// </summary>
		protected override void FetchViewState() {
			base.FetchViewState();
			if (IsViewStateEmpty) return;
			try {
				inParamFundId = (long) ViewStateAttributes.GetFromViewState("inParamFundId", 0L);
				inParamReturn = (bool) ViewStateAttributes.GetFromViewState("inParamReturn", false);
				inParamSuccess = (bool) ViewStateAttributes.GetFromViewState("inParamSuccess", false);
				varLcShowBuyingModal = (bool) ViewStateAttributes.GetFromViewState("varLcShowBuyingModal", false);
			} catch (Exception e) {
				throw new Exception("Error Deserializing ViewState", e); 
			}
		}

		/// <summary>
		/// Store visibility information of the web block and input widgets in the viewstate
		/// </summary>
		protected override void StoreInputsAndWebBlockVisibility() {
			ViewStateAttributes.EnsureNotEmpty();
		}
		/// <summary>
		/// Restore visibility information of the web block and input widgets from the viewstate
		/// </summary>
		protected override void RestoreInputsAndWebBlockVisibility() {
			WasRendered = true;
		}

		private void Page_Unload(object sender, System.EventArgs e) {
		}

		public LocalState PushStack() {
			throw new NotImplementedException();
		}

		public void doRefreshScreen(HeContext heContext) {
			((IWebScreen) this.Page).doRefreshScreen(heContext);
		}

		public void doAJAXRefreshScreen(HeContext heContext) {
			StoreViewState();
			((IWebScreen) this.Page).doAJAXRefreshScreen(heContext);
		}

		public static void GetCss(TextWriter writer, bool inline, HashSet<string> visited) {
			string blockId = "BIFY.KxOWArPxzakG7bGsJk0wd_w";
			if (visited.Contains(blockId)) {
				return; 
			}
			visited.Add(blockId);
			if (!inline) {
				GetCssIncludes(writer, visited);
			} else {
				GetInlineCss(writer, visited);
			}
		}

		private static void GetCssIncludes(TextWriter writer, HashSet<string> visited) {
			InnerGetCss(writer, false, visited);
			CssHelper.WriteCssInclude(writer, AppUtils.Instance.getImagePath() + "Blocks/BIFY/Modals/FundSplashModal.css" + AppUtils.Instance.CacheInvalidationSuffix);
		}

		private static void GetInlineCss(TextWriter writer, HashSet<string> visited) {
			StringWriter localCssWriter = new StringWriter();
			localCssWriter.NewLine = writer.NewLine;
			string localCss = Environment.NewLine;
			InnerGetCss(localCssWriter, true, visited);
			localCss += localCssWriter.ToString();
			HashSet<string> cssVisited = new HashSet<string>();
			string read;
			AppUtils.getResourceFileContent(out read, "Blocks\\BIFY\\Modals\\FundSplashModal.css");
			localCss += OutSystems.HubEdition.RuntimePlatform.Email.EmailHelper.FlattenCSSFile(read, "Blocks\\BIFY\\Modals\\FundSplashModal.css", cssVisited);
			writer.Write(localCss);
		}

		private static void InnerGetCss(TextWriter writer, bool inline, HashSet<string> visited) {
			proxy_BIFY_OutSystemsUIWeb.Flows.FlowContent.WBlkModal.GetCss(writer, inline, visited);
			proxy_BIFY_RichWidgets.Flows.FlowRichWidgets.WBlkIcon.GetCss(writer, inline, visited);
			ssBIFY.Flows.FlowModalContent.WBlkFundSplashContent.GetCss(writer, inline, visited);
			ssBIFY.Flows.FlowModals.WBlkContributeToAFundModal.GetCss(writer, inline, visited);
		}

		private void Page_Error(object sender, System.EventArgs e) {
		}
		public void CheckPermissions(HeContext heContext) {
			((IWebScreen) this.Page).CheckPermissions(heContext);
		}
		protected static string GetString(string key, string defaultValue) {
			return Global.GetStringResource(key, defaultValue);
		}

		public ObjectKey Key {
			get {
				return ObjectKey.Parse("xOWArPxzakG7bGsJk0wd_w"); 
			}
		}
		public bool isSecure {
			get {
				return ((IWebScreen) Page).isSecure; 
			}
		}
		/// <summary>
		/// Action <code>CommandCloseModal</code> that represents the Service Studio screen action
		///  <code>CloseModal</code> <p> Description: </p>
		/// </summary>
		private bool CommandCloseModal(HeContext heContext) {
			Global.App.Context.Items["osPassedOnAction"] = true;
			CheckPermissions(heContext);
			RequestTracer perfTracer = heContext.RequestTracer; if (perfTracer != null) {
				perfTracer.RegisterAction("2b9d7911-a052-4573-952f-e4845f177df9", "FundSplashModal.CloseModal"); 
			}
			try {
				if (heContext != null && heContext.RequestTracer != null) {
					heContext.RequestTracer.RegisterInternalCall("EXmdK1Kgc0WVL+SEXxd9+Q", "CloseModal", "1WMl+O7_wkOQDYQl1Ck5zQ", "BIFY");
				}
				try {
					if (heContext != null) {
						heContext.RequestCancelation.ThrowIfCanceled();
					}
					// ToggleModal2
					if (heContext != null) {
						heContext.RequestCancelation.ThrowIfCanceled();
					}
					Actions.ActionToggleModal(heContext, OutSystemsUIWeb_wtModal.ClientID, "");

					// RemoveFixBody
					if (heContext != null) {
						heContext.RequestCancelation.ThrowIfCanceled();
					}
					Actions.ActionRunJavaScript(heContext, "$(document.body).removeClass(\'fixBody\');");

					// Update Screen
					return true;

				} // try

				catch (System.Threading.ThreadAbortException) {
					return false;
				}
				catch (Exception ex) {
					heContext.RequestCancelation.Pause();
					try {
						ErrorLog.LogApplicationError(ex, heContext, "");
						heContext.LastException = ex;
						// Error Handler
						DatabaseAccess.RollbackAllTransactions();

						// ExceptionFeedbackMessage
						if (heContext != null) {
							heContext.RequestCancelation.ThrowIfCanceled();
						}
						Actions.ActionFeedback_Message(heContext, ex.Message, ENMessageTypeEntity.GetRecordByKey(ObjectKey.Parse("htKb+xw1b0eZ4Wacii2S8w")).ssId);

						// Update Screen
						return true;

					} finally {
						heContext.RequestCancelation.Resume();
					}
				} // Catch
			} catch (System.Threading.ThreadAbortException) {
				return false;
			}
		}
		/// <summary>
		/// Action <code>CommandClickedBuy</code> that represents the Service Studio screen action
		///  <code>ClickedBuy</code> <p> Description: </p>
		/// </summary>
		private bool CommandClickedBuy(HeContext heContext) {
			Global.App.Context.Items["osPassedOnAction"] = true;
			CheckPermissions(heContext);
			RequestTracer perfTracer = heContext.RequestTracer; if (perfTracer != null) {
				perfTracer.RegisterAction("36984c39-a2d0-40f2-879c-11dd22dbc9ed", "FundSplashModal.ClickedBuy"); 
			}
			try {
				if (heContext != null && heContext.RequestTracer != null) {
					heContext.RequestTracer.RegisterInternalCall("OUyYNtCi8kCHnBHdItvJ7Q", "ClickedBuy", "1WMl+O7_wkOQDYQl1Ck5zQ", "BIFY");
				}
				if (heContext != null) {
					heContext.RequestCancelation.ThrowIfCanceled();
				}
				// ToggleModal2
				if (heContext != null) {
					heContext.RequestCancelation.ThrowIfCanceled();
				}
				Actions.ActionToggleModal(heContext, OutSystemsUIWeb_wtModal.ClientID, "");

				// RemoveFixBody
				if (heContext != null) {
					heContext.RequestCancelation.ThrowIfCanceled();
				}
				Actions.ActionRunJavaScript(heContext, "$(document.body).removeClass(\'fixBody\');");

				// ShowBuyingModal = True
				varLcShowBuyingModal = true;
				explicitChangedVariables.Add(varLcShowBuyingModal);

				// Refresh Widget (wtbuyingmodal)
				((OSPageViewState) Page).AjaxRefresh(this.FindControl("wtbuyingmodal"), "None", StoreViewState);

				// Update Screen
				return true;

			} catch (System.Threading.ThreadAbortException) {
				return false;
			}
		}
		/// <summary>
		/// Action <code>CommandSendBifyGiftModalSavedGift</code> that represents the Service Studio screen
		///  action <code>SendBifyGiftModalSavedGift</code> <p> Description: </p>
		/// </summary>
		private bool CommandSendBifyGiftModalSavedGift(HeContext heContext) {
			Global.App.Context.Items["osPassedOnAction"] = true;
			CheckPermissions(heContext);
			RequestTracer perfTracer = heContext.RequestTracer; if (perfTracer != null) {
				perfTracer.RegisterAction("caf4ef40-5892-4bed-ba05-55257b332c49", "FundSplashModal.SendBifyGiftModalSavedGift"); 
			}
			try {
				if (heContext != null && heContext.RequestTracer != null) {
					heContext.RequestTracer.RegisterInternalCall("QO_0ypJY7Uu6BVUlezMsSQ", "SendBifyGiftModalSavedGift", "1WMl+O7_wkOQDYQl1Ck5zQ", "BIFY");
				}
				if (heContext != null) {
					heContext.RequestCancelation.ThrowIfCanceled();
				}
				BindDelegatesIfNeeded();
				if ((ssPassBackBuyTriggered != null)) {
					ssPassBackBuyTriggered();
				}

				// RemoveFixBody
				if (heContext != null) {
					heContext.RequestCancelation.ThrowIfCanceled();
				}
				Actions.ActionRunJavaScript(heContext, "$(document.body).removeClass(\'fixBody\');");

				// Update Screen
				return true;

			} catch (System.Threading.ThreadAbortException) {
				return false;
			}
		}
		public void cnt_Container4_onDataBinding(object sender, System.EventArgs e) {
			ObjectKey oldCurrentESpaceKey = heContext.CurrentESpaceKey;
			try {
				heContext.CurrentESpaceKey = ssBIFY.Global.eSpaceKey;
				cnt_Container4_setInlineAttributes(sender, e);
			} finally {
				heContext.CurrentESpaceKey = oldCurrentESpaceKey;
			}
		}
		public string cnt_Container4_setInlineAttributes(object sender, System.EventArgs e) {
			ObjectKey oldCurrentESpaceKey = heContext.CurrentESpaceKey;
			try {
				heContext.CurrentESpaceKey = ssBIFY.Global.eSpaceKey;
				if (!cnt_Container4_isVisible()) {
					string stylevalue = ((IAttributeAccessor) sender).GetAttribute("style");
					{
						string newstyledef;
						string oldstyledef;
						newstyledef = stylevalue + ((stylevalue!=null && !stylevalue.TrimEnd().EndsWith(";")) ? ";": "") + "display:none";
						oldstyledef = ((IAttributeAccessor) sender).GetAttribute("style");
						if (oldstyledef != null) {
							if (!oldstyledef.TrimEnd().EndsWith(";")) newstyledef = ";" + newstyledef;
							if (!oldstyledef.EndsWith(newstyledef)) {
								((IAttributeAccessor) sender).SetAttribute("style", oldstyledef + newstyledef.ToString());
							} else {
								((IAttributeAccessor) sender).SetAttribute("style", oldstyledef.ToString());
							}
						} else {
							((IAttributeAccessor) sender).SetAttribute("style", newstyledef.ToString());
						}
					}
				} else {
					string stylevalue = ((IAttributeAccessor) sender).GetAttribute("style");
					if (stylevalue != null) {
						((IAttributeAccessor) sender).SetAttribute("style", stylevalue.Replace("display:none;", "").Replace("display:none", "").ToString());
					}
				}
				return "";
			} finally {
				heContext.CurrentESpaceKey = oldCurrentESpaceKey;
			}
		}
		/// <summary>
		/// Gets the visible state of component (wt_Container4)
		/// </summary>
		/// <returns>The Visible State of wt_Container4</returns>
		public bool cnt_Container4_isVisible() {
			ObjectKey oldCurrentESpaceKey = heContext.CurrentESpaceKey;
			try {
				heContext.CurrentESpaceKey = ssBIFY.Global.eSpaceKey;
				return true;
			} finally {
				heContext.CurrentESpaceKey = oldCurrentESpaceKey;
			}
		}

		bool if_wt_If3_hasRun=false;
		bool if_wt_If3_evalResult;
		public bool if_wt_If3() {
			if (if_wt_If3_hasRun) {
				if_wt_If3_hasRun = false;
				return if_wt_If3_evalResult;
			}
			if_wt_If3_hasRun = true;
			ObjectKey oldCurrentESpaceKey = heContext.CurrentESpaceKey;
			try {
				heContext.CurrentESpaceKey = ssBIFY.Global.eSpaceKey;
				if_wt_If3_evalResult = varLcShowBuyingModal;
			} finally {
				heContext.CurrentESpaceKey = oldCurrentESpaceKey;
			}
			return if_wt_If3_evalResult;
		}

		public void OutSystemsUIWeb_webBlckModal_onDataBinding(object sender, System.EventArgs e) {
			ObjectKey oldCurrentESpaceKey = heContext.CurrentESpaceKey;
			try {
				heContext.CurrentESpaceKey = ssBIFY.Global.eSpaceKey;
				proxy_BIFY_OutSystemsUIWeb.Flows.FlowContent.WBlkModal widget = (proxy_BIFY_OutSystemsUIWeb.Flows.FlowContent.WBlkModal) sender;
				widget.inParamPosition = ENPositionExtendedEntity.GetRecordByKey(ObjectKey.Parse("n7Pc+umccEOemB+tTiVohg")).ssPositionExtended;
				widget.inParamHasOverlay = true;
				widget.inParamEnterAnimation = ENEnterAnimationEntity.GetRecordByKey(ObjectKey.Parse("dU8JiSRF2UGS0bdcqRrwGw")).ssEnterAnimation;
				widget.inParamLeaveAnimation = ENLeaveAnimationEntity.GetRecordByKey(ObjectKey.Parse("Ock5tiNPg0adznQQI2XCiA")).ssLeaveAnimation;
				widget.inParamExtendedClass = "";
			} finally {
				heContext.CurrentESpaceKey = oldCurrentESpaceKey;
			}
		}
		public void lnk_Link12_onDataBinding(object sender, System.EventArgs e) {
			ObjectKey oldCurrentESpaceKey = heContext.CurrentESpaceKey;
			try {
				heContext.CurrentESpaceKey = ssBIFY.Global.eSpaceKey;
				((IAjaxHandler) sender).RegisterAjaxEvent(AjaxEventType.onAjaxClick, null);
			} finally {
				heContext.CurrentESpaceKey = oldCurrentESpaceKey;
			}
		}
		/// <summary>
		/// Action to be taken at a Link submit action)
		/// </summary>
		/// <param name="sender"> The associated sender components</param>
		/// <param name="e"> The associated event arguments</param>
		public void wt_Link12_Click(object sender, System.EventArgs e) {
			ObjectKey oldCurrentESpaceKey = heContext.CurrentESpaceKey;
			try {
				heContext.CurrentESpaceKey = ssBIFY.Global.eSpaceKey;
				if (IsAjaxRequest && e.Equals(EventArgs.Empty) && ((OSPage) Page).TriggersBubbleUpEvents((Control) sender)) {
					return;
				}
				((IWebScreen) ((System.Web.UI.Control) sender).Page).OnSubmit(((IParentEditRecordProp) sender).GetParentEditRecordClientId(), false);
				if (CommandCloseModal(heContext)) {
					doAJAXRefreshScreen(heContext);
				}
			} finally {
				heContext.CurrentESpaceKey = oldCurrentESpaceKey;
			}
		}
		/// <summary>
		/// Gets the title of the link (wt_Link12)
		/// </summary>
		/// <returns>title of the Link (wt_Link12)</returns>
		public string
		lnk_Link12_getTitle() {
			return "";
		}
		/// <summary>
		/// Gets the visible state of component (wt_Link12)
		/// </summary>
		/// <returns>The Visible State of wt_Link12</returns>
		public bool lnk_Link12_isVisible() {
			ObjectKey oldCurrentESpaceKey = heContext.CurrentESpaceKey;
			try {
				heContext.CurrentESpaceKey = ssBIFY.Global.eSpaceKey;
				return true;
			} finally {
				heContext.CurrentESpaceKey = oldCurrentESpaceKey;
			}
		}

		/// <summary>
		/// Gets the enabled state of component (wt_Link12)
		/// </summary>
		/// <returns>The Enabled State of wt_Link12</returns>
		public bool lnk_Link12_isEnabled() {
			ObjectKey oldCurrentESpaceKey = heContext.CurrentESpaceKey;
			try {
				heContext.CurrentESpaceKey = ssBIFY.Global.eSpaceKey;
				return true; 
			} finally {
				heContext.CurrentESpaceKey = oldCurrentESpaceKey;
			}
		}
		public void RichWidgets_webBlck_WebBlockInstance8_onDataBinding(object sender, System.EventArgs e) {
			ObjectKey oldCurrentESpaceKey = heContext.CurrentESpaceKey;
			try {
				heContext.CurrentESpaceKey = ssBIFY.Global.eSpaceKey;
				proxy_BIFY_RichWidgets.Flows.FlowRichWidgets.WBlkIcon widget = (proxy_BIFY_RichWidgets.Flows.FlowRichWidgets.WBlkIcon) sender;
				widget.inParamName = ENIconNameEntity.GetRecordByKey(ObjectKey.Parse("G1Mahdk9CkCYt5z2Q3wtYw")).ssClass;
				widget.inParamSize = ENIconSizeEntity.GetRecordByKey(ObjectKey.Parse("oKKDWUUxF0u1zqd+FXTuhQ")).ssClass;
			} finally {
				heContext.CurrentESpaceKey = oldCurrentESpaceKey;
			}
		}
		public void webBlck_WebBlockInstance15_BindDelegates(object sender, BindDelegatesEventArgs e) {
			ssBIFY.Flows.FlowModalContent.WBlkFundSplashContent webBlock = (ssBIFY.Flows.FlowModalContent.WBlkFundSplashContent) e.UserControl;
			webBlock.ssBuyTriggered += webBlck_WebBlockInstance15_ssBuyTriggered;
		}
		public void webBlck_WebBlockInstance15_onDataBinding(object sender, System.EventArgs e) {
			ObjectKey oldCurrentESpaceKey = heContext.CurrentESpaceKey;
			try {
				heContext.CurrentESpaceKey = ssBIFY.Global.eSpaceKey;
				ssBIFY.Flows.FlowModalContent.WBlkFundSplashContent widget = (ssBIFY.Flows.FlowModalContent.WBlkFundSplashContent) sender;
				widget.inParamFundId = inParamFundId;
				widget.inParamUserId = inParamFundSplashTo;
			} finally {
				heContext.CurrentESpaceKey = oldCurrentESpaceKey;
			}
		}
		private void webBlck_WebBlockInstance15_ssBuyTriggered() {
			CommandClickedBuy(heContext);
			AddStoreViewStateWebScreenStack(this);
		}
		bool if_wtbuyingmodal_hasRun=false;
		bool if_wtbuyingmodal_evalResult;
		public bool if_wtbuyingmodal() {
			if (if_wtbuyingmodal_hasRun) {
				if_wtbuyingmodal_hasRun = false;
				return if_wtbuyingmodal_evalResult;
			}
			if_wtbuyingmodal_hasRun = true;
			ObjectKey oldCurrentESpaceKey = heContext.CurrentESpaceKey;
			try {
				heContext.CurrentESpaceKey = ssBIFY.Global.eSpaceKey;
				if_wtbuyingmodal_evalResult = varLcShowBuyingModal;
			} finally {
				heContext.CurrentESpaceKey = oldCurrentESpaceKey;
			}
			return if_wtbuyingmodal_evalResult;
		}

		public void cnt_Container11_onDataBinding(object sender, System.EventArgs e) {
			ObjectKey oldCurrentESpaceKey = heContext.CurrentESpaceKey;
			try {
				heContext.CurrentESpaceKey = ssBIFY.Global.eSpaceKey;
				cnt_Container11_setInlineAttributes(sender, e);
			} finally {
				heContext.CurrentESpaceKey = oldCurrentESpaceKey;
			}
		}
		public string cnt_Container11_setInlineAttributes(object sender, System.EventArgs e) {
			ObjectKey oldCurrentESpaceKey = heContext.CurrentESpaceKey;
			try {
				heContext.CurrentESpaceKey = ssBIFY.Global.eSpaceKey;
				if (!cnt_Container11_isVisible()) {
					string stylevalue = ((IAttributeAccessor) sender).GetAttribute("style");
					{
						string newstyledef;
						string oldstyledef;
						newstyledef = stylevalue + ((stylevalue!=null && !stylevalue.TrimEnd().EndsWith(";")) ? ";": "") + "display:none";
						oldstyledef = ((IAttributeAccessor) sender).GetAttribute("style");
						if (oldstyledef != null) {
							if (!oldstyledef.TrimEnd().EndsWith(";")) newstyledef = ";" + newstyledef;
							if (!oldstyledef.EndsWith(newstyledef)) {
								((IAttributeAccessor) sender).SetAttribute("style", oldstyledef + newstyledef.ToString());
							} else {
								((IAttributeAccessor) sender).SetAttribute("style", oldstyledef.ToString());
							}
						} else {
							((IAttributeAccessor) sender).SetAttribute("style", newstyledef.ToString());
						}
					}
				} else {
					string stylevalue = ((IAttributeAccessor) sender).GetAttribute("style");
					if (stylevalue != null) {
						((IAttributeAccessor) sender).SetAttribute("style", stylevalue.Replace("display:none;", "").Replace("display:none", "").ToString());
					}
				}
				return "";
			} finally {
				heContext.CurrentESpaceKey = oldCurrentESpaceKey;
			}
		}
		/// <summary>
		/// Gets the visible state of component (wt_Container11)
		/// </summary>
		/// <returns>The Visible State of wt_Container11</returns>
		public bool cnt_Container11_isVisible() {
			ObjectKey oldCurrentESpaceKey = heContext.CurrentESpaceKey;
			try {
				heContext.CurrentESpaceKey = ssBIFY.Global.eSpaceKey;
				return true;
			} finally {
				heContext.CurrentESpaceKey = oldCurrentESpaceKey;
			}
		}

		public void webBlck_WebBlockInstance7_BindDelegates(object sender, BindDelegatesEventArgs e) {
			ssBIFY.Flows.FlowModals.WBlkContributeToAFundModal webBlock = (ssBIFY.Flows.FlowModals.WBlkContributeToAFundModal) e.UserControl;
			webBlock.ssSavedGiftTriggered += webBlck_WebBlockInstance7_ssSavedGiftTriggered;
		}
		public void webBlck_WebBlockInstance7_onDataBinding(object sender, System.EventArgs e) {
			ObjectKey oldCurrentESpaceKey = heContext.CurrentESpaceKey;
			try {
				heContext.CurrentESpaceKey = ssBIFY.Global.eSpaceKey;
				ssBIFY.Flows.FlowModals.WBlkContributeToAFundModal widget = (ssBIFY.Flows.FlowModals.WBlkContributeToAFundModal) sender;
				widget.inParamFundId = inParamFundId;
				widget.inParamReturn = inParamReturn;
				widget.inParamSuccess = inParamSuccess;
			} finally {
				heContext.CurrentESpaceKey = oldCurrentESpaceKey;
			}
		}
		private void webBlck_WebBlockInstance7_ssSavedGiftTriggered() {
			CommandSendBifyGiftModalSavedGift(heContext);
			AddStoreViewStateWebScreenStack(this);
		}
		public static class FuncCommandCloseModal {
		}
		public static class FuncCommandClickedBuy {
		}
		public static class FuncCommandSendBifyGiftModalSavedGift {
		}
		public static class FuncssPreparation {
		}

		public override Control FindControl(string id) {
			if (id == "wt_Link12") {
				return OutSystemsUIWeb_wtModal.wtIcon.FindControl("wt_Link12");
			}
			if (id == "RichWidgets_wt_WebBlockInstance8") {
				return OutSystemsUIWeb_wtModal.wtIcon.FindControl("RichWidgets_wt_WebBlockInstance8");
			}
			if (id == "wt_WebBlockInstance15") {
				return OutSystemsUIWeb_wtModal.wtContent.FindControl("wt_WebBlockInstance15");
			}
			return base.FindControl(id);
		}
		public String BreakpointHook(String breakpointId) {
			return "";
		}

		public String BreakpointHook(String breakpointId, bool isExpressionlessWidget) {
			return "";
		}

		public String PageStartHook() {
			_isRendering = true;
			Page_Load(null, null); _isRendering = false;
			this.Load -= new System.EventHandler(this.Page_Load);
			return "";
		}
		public String PageEndHook() {
			return "";
		}
		public override string WebBlockIdentifier {
			get {
				return "BIFY.KxOWArPxzakG7bGsJk0wd_w";
			}
		}
	}
}
