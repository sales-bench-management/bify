﻿<%@ Page Language="c#" Codebehind="FacebookCallback.aspx.cs" AutoEventWireup="false" Inherits="ssBIFY.Flows.FlowCallbackFlow.ScrnFacebookCallback" %>
<%@ Register TagPrefix="osweb" Namespace="OutSystems.HubEdition.WebWidgets" Assembly="OutSystems.HubEdition.WebWidgets" %>
<%@ Import namespace="ssBIFY" %>
<%@ Import namespace="OutSystems.HubEdition.RuntimePlatform" %>
<%@ Assembly Name="OutSystems.WidgetsRuntimeAPI" %>
<%@ Register TagPrefix="RichWidgets_widgets" TagName="KH_KQJACwvEqFcQnMKeaEzA" Src="Blocks\BIFY\Layouts2\Layout_London.ascx" %>
<%@ Register TagPrefix="SocialLoginSamples_widgets" TagName="KgGqVjlDuvkKTxk7naaerTA" Src="Blocks\BIFY\Common2\Header.ascx" %>
<%@ Register TagPrefix="SocialLoginSamples_widgets" TagName="Ke77LnY3GBfWtQXGOyBElgw" Src="Blocks\BIFY\Common2\Menu.ascx" %>
<%@ Register TagPrefix="SocialLoginAPI_widgets" TagName="Kjlhx_cFF2kSwlfjEE5dxKg" Src="Blocks\BIFY\Callbacks\FacebookAuthorize.ascx" %>

<%= BIFY_Properties.DocType %>
<html xmlns="http://www.w3.org/1999/xhtml" >
  <head runat="server"><%= GetHeadTopJavaScript() %>
	<title><%= HttpUtility.HtmlEncode (Title) %></title>
    <meta http-equiv="Content-Type" content="<%= "text/html; charset=" + Response.ContentEncoding.WebName %>" />
    <meta http-equiv="Content-Script-Type" content="text/javascript" />
    <meta http-equiv="Content-Style-Type" content="text/css" />
<%= "\n" + GetStyleSheetIncludes() %><%= GetRequestInfoJavaScript() + GetJavaScriptIncludes() + GetHeadBottomJavaScript() %>
  </head>
  <osweb:Body runat="server"><%= GetBodyTopJavaScript() %>
    <osweb:Form id="WebForm1" method="post"  action="<%# GetFormAction() %>" runat="server">
<RichWidgets_widgets:KH_KQJACwvEqFcQnMKeaEzA runat="server" id="RichWidgets_wt_WebBlockInstance10" OnEvaluateParameters="RichWidgets_webBlck_WebBlockInstance10_onDataBinding" InstanceID="_WebBlockInstance10"><phHeader><SocialLoginSamples_widgets:KgGqVjlDuvkKTxk7naaerTA runat="server" id="SocialLoginSamples_wt_WebBlockInstance2" OnEvaluateParameters="SocialLoginSamples_webBlck_WebBlockInstance2_onDataBinding" InstanceID="_WebBlockInstance2"></SocialLoginSamples_widgets:KgGqVjlDuvkKTxk7naaerTA></phHeader><phMenu><SocialLoginSamples_widgets:Ke77LnY3GBfWtQXGOyBElgw runat="server" id="SocialLoginSamples_wt_WebBlockInstance13" OnEvaluateParameters="SocialLoginSamples_webBlck_WebBlockInstance13_onDataBinding" InstanceID="_WebBlockInstance13"></SocialLoginSamples_widgets:Ke77LnY3GBfWtQXGOyBElgw></phMenu><phTitle></phTitle><phActions></phActions><phMainContent><SocialLoginAPI_widgets:Kjlhx_cFF2kSwlfjEE5dxKg runat="server" id="SocialLoginAPI_wt_WebBlockInstance1" onAjaxNotify="SocialLoginAPI_webBlck_WebBlockInstance1_AjaxNotify" OnEvaluateParameters="SocialLoginAPI_webBlck_WebBlockInstance1_onDataBinding" OnBindDelegates="SocialLoginAPI_webBlck_WebBlockInstance1_BindDelegates" InstanceID="_WebBlockInstance1"></SocialLoginAPI_widgets:Kjlhx_cFF2kSwlfjEE5dxKg><osweb:Container runat="server" id="wtresultContainer" onDataBinding="cntresultContainer_onDataBinding" cssClass="OSAutoMarginTop"><osweb:TextBox runat="server" id="wt_Input6" GridCssClasses="OSFillParent" Visible="<%# inputwt_Input6_isVisible() %>" ReadOnly="<%# !inputwt_Input6_isEnabled() %>" maxlength="50" onchange="OsLimitInput(this,event,50);" onkeyup="OsLimitInput(this,event,50);" OnDefaultMandatoryValidationMessage="GetMandatoryValidatorMsg" OnDefaultTypeValidationMessage="GetTextValidatorMsg" textmode="MultiLine" rows="12" text="<%# inputwt_Input6_input_value() %>" onTextChanged="inputwt_Input6_input_onTextChanged" Mandatory="<%# inputwt_Input6_isMandatory() %>"/><osweb:RequiredFieldTextValidator  runat="server" display="Dynamic" id="wt_Input6ValidatorRequired" ErrorMessage="<%# BIFY_Properties.MandatoryValidatorMsg %>" ControlToValidate="wt_Input6"/><osweb:TextValidator runat="server" display="Dynamic" id="wt_Input6ValidatorType" ErrorMessage="<%# BIFY_Properties.TextValidatorMsg %>" ControlToValidate="wt_Input6" ClientValidationFunction="OsCustomValidatorText"/></osweb:Container></phMainContent><phFooter></phFooter></RichWidgets_widgets:KH_KQJACwvEqFcQnMKeaEzA><osweb:DummySubmitLink runat="server" id="Dummy_Submit_Link"/>
    <%= OutSystems.HubEdition.RuntimePlatform.AppInfo.GetAppInfo().GetWatermark() %><%= GetCallbackDebug()	
%><%= GetVisitCode() %></osweb:Form><%= GetEndPageJavaScript() + GetBodyBottomJavaScript() %>
  </osweb:Body>
</html>
