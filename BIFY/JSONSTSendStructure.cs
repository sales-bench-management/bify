﻿/* 
 This source code (the "Generated Software") is generated by the OutSystems Platform 
 and is licensed by OutSystems (http://www.outsystems.com) to You solely for testing and evaluation 
 purposes, unless You and OutSystems have executed a specific agreement covering the use terms and 
 conditions of the Generated Software, in which case such agreement shall apply. 
*/

using System;
using System.Linq;
using OutSystems.HubEdition.RuntimePlatform;
using OutSystems.HubEdition.RuntimePlatform.Db;
using OutSystems.Internal.Db;
using OutSystems.RuntimeCommon;
using OutSystems.RuntimeCommon.Caching;

using System.Text;
using System.Data;
using System.Collections.Generic;
using Newtonsoft.Json;
using OutSystems.RESTService;
using OutSystems.RESTService.Behaviors;
using OutSystems.RESTService.Controllers;
using OutSystems.ObjectKeys;
using OutSystems.HubEdition.RuntimePlatform.NewRuntime;


namespace ssBIFY.RestRecords {

	public class JSONSTSendStructure: AbstractRESTStructure<STSendStructure> {
		[JsonProperty("personalizations")]
		public ssBIFY.RestRecords.JSONSTPersonalizationItemStructure[] AttrPersonalizations;

		[JsonProperty("from")]
		public ssBIFY.RestRecords.JSONSTToItemStructure AttrFrom;

		[JsonProperty("subject")]
		public string AttrSubject;

		[JsonProperty("content")]
		public ssBIFY.RestRecords.JSONSTText_plainStructure[] AttrContent;

		[JsonProperty("template_id")]
		public string AttrTemplate_id;

		public JSONSTSendStructure() {}

		public JSONSTSendStructure(STSendStructure s, IBehaviorsConfiguration config) {
			if (config.DefaultValuesBehavior == DefaultValuesBehavior.DontSend) {
				AttrPersonalizations = s.ssPersonalizations.Length == 0 ? null: s.ssPersonalizations.ToArray<ssBIFY.RestRecords.JSONSTPersonalizationItemStructure>(ssBIFY.RestRecords.JSONSTPersonalizationItemStructure.FromStructureDelegate(config));
				AttrFrom = ConvertToRestWithoutDefaults(s.ssFrom, new STToItemStructure(null), ssBIFY.RestRecords.JSONSTToItemStructure.FromStructureDelegate(config));
				AttrSubject = ConvertToRestWithoutDefaults(s.ssSubject, "");
				AttrContent = s.ssContent.Length == 0 ? null: s.ssContent.ToArray<ssBIFY.RestRecords.JSONSTText_plainStructure>(ssBIFY.RestRecords.JSONSTText_plainStructure.FromStructureDelegate(config));
				AttrTemplate_id = ConvertToRestWithoutDefaults(s.ssTemplate_id, "");
			} else {
				AttrPersonalizations = s.ssPersonalizations.ToArray<ssBIFY.RestRecords.JSONSTPersonalizationItemStructure>(ssBIFY.RestRecords.JSONSTPersonalizationItemStructure.FromStructureDelegate(config));
				AttrFrom = ssBIFY.RestRecords.JSONSTToItemStructure.FromStructure(s.ssFrom, config);
				AttrSubject = s.ssSubject;
				AttrContent = s.ssContent.ToArray<ssBIFY.RestRecords.JSONSTText_plainStructure>(ssBIFY.RestRecords.JSONSTText_plainStructure.FromStructureDelegate(config));
				AttrTemplate_id = s.ssTemplate_id;
			}
		}

		public static Func<ssBIFY.RestRecords.JSONSTSendStructure, STSendStructure> ToStructureDelegate(IBehaviorsConfiguration config) {
			return (ssBIFY.RestRecords.JSONSTSendStructure s) => ToStructure(s, config);
		}
		public static STSendStructure ToStructure(ssBIFY.RestRecords.JSONSTSendStructure obj, IBehaviorsConfiguration config) {
			STSendStructure s = new STSendStructure(null);
			if (obj != null) {
				s.ssPersonalizations = RLPersonalizationItemList.ToList(obj.AttrPersonalizations, ssBIFY.RestRecords.JSONSTPersonalizationItemStructure.ToStructureDelegate(config));
				s.ssFrom = ssBIFY.RestRecords.JSONSTToItemStructure.ToStructure(obj.AttrFrom, config);
				s.ssSubject = obj.AttrSubject == null ? "": obj.AttrSubject;
				s.ssContent = RLText_plainList.ToList(obj.AttrContent, ssBIFY.RestRecords.JSONSTText_plainStructure.ToStructureDelegate(config));
				s.ssTemplate_id = obj.AttrTemplate_id == null ? "": obj.AttrTemplate_id;
			}
			return s;
		}

		public static Func<STSendStructure, ssBIFY.RestRecords.JSONSTSendStructure> FromStructureDelegate(IBehaviorsConfiguration config) {
			return (STSendStructure s) => FromStructure(s, config);
		}
		public static ssBIFY.RestRecords.JSONSTSendStructure FromStructure(STSendStructure s, IBehaviorsConfiguration config) {
			return new ssBIFY.RestRecords.JSONSTSendStructure(s, config);
		}

	}



}