﻿/* 
 This source code (the "Generated Software") is generated by the OutSystems Platform 
 and is licensed by OutSystems (http://www.outsystems.com) to You solely for testing and evaluation 
 purposes, unless You and OutSystems have executed a specific agreement covering the use terms and 
 conditions of the Generated Software, in which case such agreement shall apply. 
*/

// Proxy for reference eSpace with name OutSystemsSampleDataDB and key CZsu2iX9cUKODzAyYDBjXQ
using System;
using System.Data;
using System.Reflection;
using System.Collections;
using System.Runtime.Serialization;
using OutSystems.ObjectKeys;
using OutSystems.RuntimeCommon;
using OutSystems.HubEdition.RuntimePlatform;
using System.Collections.Generic;
using System.Xml;
using OutSystems.HubEdition.RuntimePlatform.Db;
using OutSystems.Internal.Db;
using OutSystems.HubEdition.RuntimePlatform.Log;
using System.Web.UI;
using OutSystems.HubEdition.WebWidgets;
using OutSystems.HubEdition.RuntimePlatform.Web;
using OutSystems.HubEdition.RuntimePlatform.NewRuntime;
using ssBIFY;
namespace ssBIFY {
	/// <summary>
	/// Class: RsseSpaceOutSystemsSampleDataDB
	/// </summary>
	public partial class RsseSpaceOutSystemsSampleDataDB {
		protected static int _maxExtensionLogsPerRequest = OutSystems.HubEdition.RuntimePlatform.RuntimePlatformSettings.Misc.MaxLogsPerRequestExtension.GetValue();
		public static int eSpaceId {
			get {
				return ssOutSystemsSampleDataDB.Global.eSpaceId;
			}
		}
		public static HashSet<StaticEntity> GetStaticEntities(HashSet<StaticEntity> staticEntities, HashSet<ObjectKey> producersKeys) {
			return ssOutSystemsSampleDataDB.Global.GetStaticEntities(staticEntities, producersKeys);
		}
		public static void MssOnSessionStart(HeContext heContext) {
			ObjectKey oldCurrentESpaceKey = heContext.CurrentESpaceKey;
			try {
				heContext.CurrentESpaceKey = ssOutSystemsSampleDataDB.Global.eSpaceKey;
				if (RuntimePlatformUtils.TestAndSetProducerSession("OutSystemsSampleDataDB")) {
					ssOutSystemsSampleDataDB.Actions.ActionOnSessionStart(heContext);
				}
			} finally {
				heContext.CurrentESpaceKey = oldCurrentESpaceKey;
			}
		}


		public sealed partial class ENSample_EmployeeEntity {
			public static string ViewName(int? tenant, string locale) {
				return ssOutSystemsSampleDataDB.ENSample_EmployeeEntity.ViewName(tenant, locale);
			}
			public static readonly ObjectKey eSpaceKey = ssOutSystemsSampleDataDB.Global.eSpaceKey;
		} // ENSample_EmployeeEntity
		public sealed partial class ENSample_DepartmentEntity {
			public static string ViewName(int? tenant, string locale) {
				return ssOutSystemsSampleDataDB.ENSample_DepartmentEntity.ViewName(tenant, locale);
			}
			public static readonly ObjectKey eSpaceKey = ssOutSystemsSampleDataDB.Global.eSpaceKey;
		} // ENSample_DepartmentEntity
		public sealed partial class ENSample_TransactionTypeEntity {
			public static string ViewName(int? tenant, string locale) {
				return ssOutSystemsSampleDataDB.ENSample_TransactionTypeEntity.ViewName(tenant, locale);
			}
			public static readonly ObjectKey eSpaceKey = ssOutSystemsSampleDataDB.Global.eSpaceKey;
		} // ENSample_TransactionTypeEntity
		partial class ENSample_TransactionTypeEntity {


			public static IRecord GetRecordById(int id) {
				IRecord rec = Factory.FactoryENSample_TransactionTypeEntityRecordSingleton.CreateRsseSpaceOutSystemsSampleDataDBENSample_TransactionTypeEntityRecord();
				rec.FillFromOther(ssOutSystemsSampleDataDB.ENSample_TransactionTypeEntity.GetRecordById(id));
				return rec;
			}

			public static IRecord GetRecordByName(string name) {
				IRecord rec = Factory.FactoryENSample_TransactionTypeEntityRecordSingleton.CreateRsseSpaceOutSystemsSampleDataDBENSample_TransactionTypeEntityRecord();
				rec.FillFromOther(ssOutSystemsSampleDataDB.ENSample_TransactionTypeEntity.GetRecordByName(name));
				return rec;
			}

			public static IRecord GetRecordByKey(ObjectKey key) {
				IRecord rec = Factory.FactoryENSample_TransactionTypeEntityRecordSingleton.CreateRsseSpaceOutSystemsSampleDataDBENSample_TransactionTypeEntityRecord();
				rec.FillFromOther(ssOutSystemsSampleDataDB.ENSample_TransactionTypeEntity.GetRecordByKey(key));
				return rec;
			}
		} // ENSample_TransactionTypeEntity;

		public sealed partial class ENSample_TransactionEntity {
			public static string ViewName(int? tenant, string locale) {
				return ssOutSystemsSampleDataDB.ENSample_TransactionEntity.ViewName(tenant, locale);
			}
			public static readonly ObjectKey eSpaceKey = ssOutSystemsSampleDataDB.Global.eSpaceKey;
		} // ENSample_TransactionEntity
		public sealed partial class ENSample_ProductCategoryEntity {
			public static string ViewName(int? tenant, string locale) {
				return ssOutSystemsSampleDataDB.ENSample_ProductCategoryEntity.ViewName(tenant, locale);
			}
			public static readonly ObjectKey eSpaceKey = ssOutSystemsSampleDataDB.Global.eSpaceKey;
		} // ENSample_ProductCategoryEntity
		partial class ENSample_ProductCategoryEntity {


			public static IRecord GetRecordById(int id) {
				IRecord rec = Factory.FactoryENSample_ProductCategoryEntityRecordSingleton.CreateRsseSpaceOutSystemsSampleDataDBENSample_ProductCategoryEntityRecord();
				rec.FillFromOther(ssOutSystemsSampleDataDB.ENSample_ProductCategoryEntity.GetRecordById(id));
				return rec;
			}

			public static IRecord GetRecordByName(string name) {
				IRecord rec = Factory.FactoryENSample_ProductCategoryEntityRecordSingleton.CreateRsseSpaceOutSystemsSampleDataDBENSample_ProductCategoryEntityRecord();
				rec.FillFromOther(ssOutSystemsSampleDataDB.ENSample_ProductCategoryEntity.GetRecordByName(name));
				return rec;
			}

			public static IRecord GetRecordByKey(ObjectKey key) {
				IRecord rec = Factory.FactoryENSample_ProductCategoryEntityRecordSingleton.CreateRsseSpaceOutSystemsSampleDataDBENSample_ProductCategoryEntityRecord();
				rec.FillFromOther(ssOutSystemsSampleDataDB.ENSample_ProductCategoryEntity.GetRecordByKey(key));
				return rec;
			}
		} // ENSample_ProductCategoryEntity;

		public sealed partial class ENSample_AccountsEntity {
			public static string ViewName(int? tenant, string locale) {
				return ssOutSystemsSampleDataDB.ENSample_AccountsEntity.ViewName(tenant, locale);
			}
			public static readonly ObjectKey eSpaceKey = ssOutSystemsSampleDataDB.Global.eSpaceKey;
		} // ENSample_AccountsEntity
		public sealed partial class ENSample_OfficeEntity {
			public static string ViewName(int? tenant, string locale) {
				return ssOutSystemsSampleDataDB.ENSample_OfficeEntity.ViewName(tenant, locale);
			}
			public static readonly ObjectKey eSpaceKey = ssOutSystemsSampleDataDB.Global.eSpaceKey;
		} // ENSample_OfficeEntity
		public sealed partial class ENSample_ProductEntity {
			public static string ViewName(int? tenant, string locale) {
				return ssOutSystemsSampleDataDB.ENSample_ProductEntity.ViewName(tenant, locale);
			}
			public static readonly ObjectKey eSpaceKey = ssOutSystemsSampleDataDB.Global.eSpaceKey;
		} // ENSample_ProductEntity

		public interface IENSample_TransactionTypeEntityRecordTypeFactory {
			IRecord CreateRsseSpaceOutSystemsSampleDataDBENSample_TransactionTypeEntityRecord();
		}
		public interface IENSample_ProductCategoryEntityRecordTypeFactory {
			IRecord CreateRsseSpaceOutSystemsSampleDataDBENSample_ProductCategoryEntityRecord();
		}

		public class Factory {
			public static IENSample_TransactionTypeEntityRecordTypeFactory FactoryENSample_TransactionTypeEntityRecordSingleton;
			public static IENSample_ProductCategoryEntityRecordTypeFactory FactoryENSample_ProductCategoryEntityRecordSingleton;
		}
		public class DefaultValues {
			public static int ReferenceEntity_Sample_Employee_ReferenceEntityAttribute_Office {
				get {
					return 0; 
				}
			}
			public static long ReferenceEntity_Sample_Employee_ReferenceEntityAttribute_Department {
				get {
					return 0L; 
				}
			}
			public static int ReferenceEntity_Sample_Employee_ReferenceEntityAttribute_Manager {
				get {
					return 0; 
				}
			}
			public static long ReferenceEntity_Sample_Transaction_ReferenceEntityAttribute_SourceAccount {
				get {
					return 0L; 
				}
			}
			public static long ReferenceEntity_Sample_Transaction_ReferenceEntityAttribute_DestinationAccount {
				get {
					return 0L; 
				}
			}
			public static int ReferenceEntity_Sample_Transaction_ReferenceEntityAttribute_Type {
				get {
					return 0; 
				}
			}
			public static int ReferenceEntity_Sample_Accounts_ReferenceEntityAttribute_CreatedBy {
				get {
					return 0; 
				}
			}
			public static int ReferenceEntity_Sample_Accounts_ReferenceEntityAttribute_Manager {
				get {
					return 0; 
				}
			}
			public static int ReferenceEntity_Sample_Accounts_ReferenceEntityAttribute_Owner {
				get {
					return 0; 
				}
			}
			public static int ReferenceEntity_Sample_Product_ReferenceEntityAttribute_Category {
				get {
					return 0; 
				}
			}
		}
		public static string CacheInvalidationSuffix {
			get {
				return ssOutSystemsSampleDataDB.AppUtils.Instance.CacheInvalidationSuffix;
			}
		}
	}
}
