﻿/* 
 This source code (the "Generated Software") is generated by the OutSystems Platform 
 and is licensed by OutSystems (http://www.outsystems.com) to You solely for testing and evaluation 
 purposes, unless You and OutSystems have executed a specific agreement covering the use terms and 
 conditions of the Generated Software, in which case such agreement shall apply. 
*/

// Proxy for reference eSpace with name ProgressCircle and key nBaveu6RTUCLC+2yMuxxwg
using System;
using System.Data;
using System.Reflection;
using System.Collections;
using System.Runtime.Serialization;
using OutSystems.ObjectKeys;
using OutSystems.RuntimeCommon;
using OutSystems.HubEdition.RuntimePlatform;
using System.Collections.Generic;
using System.Xml;
using OutSystems.HubEdition.RuntimePlatform.Db;
using OutSystems.Internal.Db;
using OutSystems.HubEdition.RuntimePlatform.Log;
using System.Web.UI;
using OutSystems.HubEdition.WebWidgets;
using OutSystems.HubEdition.RuntimePlatform.Web;
using OutSystems.HubEdition.RuntimePlatform.NewRuntime;
using ssBIFY;
namespace ssBIFY {
	/// <summary>
	/// Class: RsseSpaceProgressCircle
	/// </summary>
	public partial class RsseSpaceProgressCircle {
		protected static int _maxExtensionLogsPerRequest = OutSystems.HubEdition.RuntimePlatform.RuntimePlatformSettings.Misc.MaxLogsPerRequestExtension.GetValue();
		public static int eSpaceId {
			get {
				return ssProgressCircle.Global.eSpaceId;
			}
		}
		public static HashSet<StaticEntity> GetStaticEntities(HashSet<StaticEntity> staticEntities, HashSet<ObjectKey> producersKeys) {
			return ssProgressCircle.Global.GetStaticEntities(staticEntities, producersKeys);
		}
		public static void MssOnSessionStart(HeContext heContext) {
			ObjectKey oldCurrentESpaceKey = heContext.CurrentESpaceKey;
			try {
				heContext.CurrentESpaceKey = ssProgressCircle.Global.eSpaceKey;
				if (RuntimePlatformUtils.TestAndSetProducerSession("ProgressCircle")) {
					ssProgressCircle.Actions.ActionOnSessionStart(heContext);
				}
			} finally {
				heContext.CurrentESpaceKey = oldCurrentESpaceKey;
			}
		}


		public sealed partial class ENMenuSubItem3Entity {
			public static string ViewName(int? tenant, string locale) {
				return ssProgressCircle.ENMenuSubItemEntity.ViewName(tenant, locale);
			}
			public static readonly ObjectKey eSpaceKey = ssProgressCircle.Global.eSpaceKey;
		} // ENMenuSubItem3Entity
		partial class ENMenuSubItem3Entity {


			public static IRecord GetRecordById(int id) {
				IRecord rec = Factory.FactoryENMenuSubItem3EntityRecordSingleton.CreateRsseSpaceProgressCircleENMenuSubItem3EntityRecord();
				rec.FillFromOther(ssProgressCircle.ENMenuSubItemEntity.GetRecordById(id));
				return rec;
			}

			public static IRecord GetRecordByName(string name) {
				IRecord rec = Factory.FactoryENMenuSubItem3EntityRecordSingleton.CreateRsseSpaceProgressCircleENMenuSubItem3EntityRecord();
				rec.FillFromOther(ssProgressCircle.ENMenuSubItemEntity.GetRecordByName(name));
				return rec;
			}

			public static IRecord GetRecordByKey(ObjectKey key) {
				IRecord rec = Factory.FactoryENMenuSubItem3EntityRecordSingleton.CreateRsseSpaceProgressCircleENMenuSubItem3EntityRecord();
				rec.FillFromOther(ssProgressCircle.ENMenuSubItemEntity.GetRecordByKey(key));
				return rec;
			}
		} // ENMenuSubItem3Entity;

		public sealed partial class ENMenuItem3Entity {
			public static string ViewName(int? tenant, string locale) {
				return ssProgressCircle.ENMenuItemEntity.ViewName(tenant, locale);
			}
			public static readonly ObjectKey eSpaceKey = ssProgressCircle.Global.eSpaceKey;
		} // ENMenuItem3Entity
		partial class ENMenuItem3Entity {


			public static IRecord GetRecordById(int id) {
				IRecord rec = Factory.FactoryENMenuItem3EntityRecordSingleton.CreateRsseSpaceProgressCircleENMenuItem3EntityRecord();
				rec.FillFromOther(ssProgressCircle.ENMenuItemEntity.GetRecordById(id));
				return rec;
			}

			public static IRecord GetRecordByName(string name) {
				IRecord rec = Factory.FactoryENMenuItem3EntityRecordSingleton.CreateRsseSpaceProgressCircleENMenuItem3EntityRecord();
				rec.FillFromOther(ssProgressCircle.ENMenuItemEntity.GetRecordByName(name));
				return rec;
			}

			public static IRecord GetRecordByKey(ObjectKey key) {
				IRecord rec = Factory.FactoryENMenuItem3EntityRecordSingleton.CreateRsseSpaceProgressCircleENMenuItem3EntityRecord();
				rec.FillFromOther(ssProgressCircle.ENMenuItemEntity.GetRecordByKey(key));
				return rec;
			}
		} // ENMenuItem3Entity;


		public interface IENMenuSubItem3EntityRecordTypeFactory {
			IRecord CreateRsseSpaceProgressCircleENMenuSubItem3EntityRecord();
		}
		public interface IENMenuItem3EntityRecordTypeFactory {
			IRecord CreateRsseSpaceProgressCircleENMenuItem3EntityRecord();
		}

		public class Factory {
			public static IENMenuSubItem3EntityRecordTypeFactory FactoryENMenuSubItem3EntityRecordSingleton;
			public static IENMenuItem3EntityRecordTypeFactory FactoryENMenuItem3EntityRecordSingleton;
		}
		public class DefaultValues {
			public static int ReferenceEntity_MenuSubItem3_ReferenceEntityAttribute_MenuItemId {
				get {
					return 0; 
				}
			}
			public static int ReferenceWebFlow_Common3_ReferenceWebBlock_Menu_Variables_ReferenceSerializableInputParameter_ActiveMenuItemId {
				get {
					return 0; 
				}
			}
			public static int ReferenceWebFlow_Common3_ReferenceWebBlock_Menu_Variables_ReferenceSerializableInputParameter_ActiveSubMenuItemId {
				get {
					return 0; 
				}
			}
		}
		public static string CacheInvalidationSuffix {
			get {
				return ssProgressCircle.AppUtils.Instance.CacheInvalidationSuffix;
			}
		}
		public class Themes {
			public class ThemeProgressCircle {

				public static string ThemeCssUrl {
					get {
						return "/ProgressCircle/Theme.ProgressCircle.css"; 
					}
				}

				public static string ThemeExtraCssUrl {
					get {
						return "/ProgressCircle/Theme.ProgressCircle.extra.css"; 
					}
				}

				public class ExceptionHandler {

					private readonly OSPage page;
					private readonly bool isEmailScreen;

					public ExceptionHandler(OSPage page, bool isEmailScreen) {
						this.page = page;
						this.isEmailScreen = isEmailScreen;
					}

					private OSPage Page {
						get {
							return page; 
						}
					}

					public bool HandleException() {
						LocalState dummy = null;
						return HandleException(ref dummy);
					}

					public bool HandleException(ref LocalState flowState) {
						return new ssProgressCircle.Themes.ThemeProgressCircle.ExceptionHandler(Page, isEmailScreen).HandleException(ref flowState);

					}
				}
			}
		}

	}
}
namespace proxy_BIFY_ProgressCircle.Flows.FlowCommon {
	public class WBlkFooter: OSUserControl, INegotiateTabIndexes, IWebScreen, INotifyTriggers, INotifySender
	{
		protected ssProgressCircle.Flows.FlowCommon.WBlkFooter block;
		static WBlkFooter() {
			HeContext heContext = AppInfo.GetAppInfo().OsContext;
			ObjectKey oldCurrentESpaceKey = heContext.CurrentESpaceKey;
			try {
				heContext.CurrentESpaceKey = ssProgressCircle.Global.eSpaceKey;
				ssBIFY.RsseSpaceProgressCircle.MssOnSessionStart(AppInfo.GetAppInfo().OsContext);
			} finally {
				heContext.CurrentESpaceKey = oldCurrentESpaceKey;
			}
		}


		public event EventHandler NotifyTriggered {
			add {
				block.NotifyTriggered += value; 
			}
			remove {
				block.NotifyTriggered -= value; 
			}
		}

		public void OnNotifyCalled(string message) {
			block.OnNotifyCalled(message);
		}

		// override OSUserControl IsProxy()
		public override bool IsProxy() {
			return true;
		}

		// proxy implementation for WebScreen interface methods

		public override event EventHandler EvaluateParameters;

		public override string ClientID {
			get {
				return block.ClientID;
			}
		}


		private readonly ObjectKey eSpaceKey = ObjectKey.Parse("d13b7783-9664-44a6-aec7-55cd5b7155b4") ?? ObjectKey.Dummy;

		public LocalState PushStack() {
			return block.PushStack();
		}

		public void doRefreshScreen(HeContext context) {
			ObjectKey oldCurrentESpaceKey = heContext.CurrentESpaceKey;
			try {
				heContext.CurrentESpaceKey = eSpaceKey;
				block.doRefreshScreen(context);
			} finally {
				heContext.CurrentESpaceKey = oldCurrentESpaceKey;
			}
		}

		public void doAJAXRefreshScreen(HeContext context) {
			ObjectKey oldCurrentESpaceKey = heContext.CurrentESpaceKey;
			try {
				heContext.CurrentESpaceKey = eSpaceKey;
				block.doAJAXRefreshScreen(context);
			} finally {
				heContext.CurrentESpaceKey = oldCurrentESpaceKey;
			}
		}

		public void OnSubmit(String parentEditRecord, bool validate) {
			block.OnSubmit(parentEditRecord, validate);
		}

		public void CheckPermissions(HeContext context) {
			block.CheckPermissions(context);
		}

		public ObjectKey Key {
			get {
				return block.Key;
			}
		}
		public bool isSecure {
			get {
				return block.isSecure;
			}
		}

		public string InstanceID {
			get {
				return block.InstanceID;
			}
			set {
				block.InstanceID = value;
			}
		}

		public HeContext heContext {
			get {
				return block.heContext;
			}
			set {
				block.heContext = value;
			}
		}

		// proxy implementation INegociateTabs interface method

		public short NegotiateTabIndexes(short tabindex, bool setTabIndex) {
			return block.NegotiateTabIndexes(tabindex, setTabIndex);
		}

		public short NegotiateTabIndexesRecursively(short tabindex, System.Web.UI.Control rootControl, bool setTabIndex) {
			return block.NegotiateTabIndexesRecursively(tabindex, rootControl, setTabIndex);
		}

		public override void DataBind() {
			HandleBreakpoint();
			if (EvaluateParameters != null) {
				EvaluateParameters(this, null);
			}
			block.DataBind();
		}

		public override bool IncludeSpan {
			get {
				return block.IncludeSpan; 
			}
			set {
				block.IncludeSpan = value; 
			}
		}

		public void BindProxyDelegates(object sender, BindDelegatesEventArgs e) {
			BindDelegatesIfNeeded();
			ssProgressCircle.Flows.FlowCommon.WBlkFooter webBlock = (ssProgressCircle.Flows.FlowCommon.WBlkFooter) e.UserControl;

		}

		public override string ParentEditRecord {
			get {
				return block.ParentEditRecord;
			}
			set {
				block.ParentEditRecord = value;
			}
		}

		public static void GetCss(System.IO.TextWriter writer, bool inline, HashSet<string> visited) {
			ssProgressCircle.Flows.FlowCommon.WBlkFooter.GetCss(writer, inline, visited);
		}

		public override string GridCssClasses {
			get {
				return block.GridCssClasses;
			}
			set {
				block.GridCssClasses = value;
			}
		}

		public override string Style {
			get {
				return block.Style;
			}
			set {
				block.Style = value;
			}
		}

		public override string WebBlockIdentifier {
			get {
				return "Proxy." + block.WebBlockIdentifier;
			}
		}

		/// <summary>
		/// Delegate Definitions
		/// </summary>

		// web block public declarations

	}

}
namespace proxy_BIFY_ProgressCircle.Flows.FlowCommon {
	public class WBlkHeader: OSUserControl, INegotiateTabIndexes, IWebScreen, INotifyTriggers, INotifySender
	{
		protected ssProgressCircle.Flows.FlowCommon.WBlkHeader block;
		static WBlkHeader() {
			HeContext heContext = AppInfo.GetAppInfo().OsContext;
			ObjectKey oldCurrentESpaceKey = heContext.CurrentESpaceKey;
			try {
				heContext.CurrentESpaceKey = ssProgressCircle.Global.eSpaceKey;
				ssBIFY.RsseSpaceProgressCircle.MssOnSessionStart(AppInfo.GetAppInfo().OsContext);
			} finally {
				heContext.CurrentESpaceKey = oldCurrentESpaceKey;
			}
		}


		public event EventHandler NotifyTriggered {
			add {
				block.NotifyTriggered += value; 
			}
			remove {
				block.NotifyTriggered -= value; 
			}
		}

		public void OnNotifyCalled(string message) {
			block.OnNotifyCalled(message);
		}

		// override OSUserControl IsProxy()
		public override bool IsProxy() {
			return true;
		}

		// proxy implementation for WebScreen interface methods

		public override event EventHandler EvaluateParameters;

		public override string ClientID {
			get {
				return block.ClientID;
			}
		}


		private readonly ObjectKey eSpaceKey = ObjectKey.Parse("d13b7783-9664-44a6-aec7-55cd5b7155b4") ?? ObjectKey.Dummy;

		public LocalState PushStack() {
			return block.PushStack();
		}

		public void doRefreshScreen(HeContext context) {
			ObjectKey oldCurrentESpaceKey = heContext.CurrentESpaceKey;
			try {
				heContext.CurrentESpaceKey = eSpaceKey;
				block.doRefreshScreen(context);
			} finally {
				heContext.CurrentESpaceKey = oldCurrentESpaceKey;
			}
		}

		public void doAJAXRefreshScreen(HeContext context) {
			ObjectKey oldCurrentESpaceKey = heContext.CurrentESpaceKey;
			try {
				heContext.CurrentESpaceKey = eSpaceKey;
				block.doAJAXRefreshScreen(context);
			} finally {
				heContext.CurrentESpaceKey = oldCurrentESpaceKey;
			}
		}

		public void OnSubmit(String parentEditRecord, bool validate) {
			block.OnSubmit(parentEditRecord, validate);
		}

		public void CheckPermissions(HeContext context) {
			block.CheckPermissions(context);
		}

		public ObjectKey Key {
			get {
				return block.Key;
			}
		}
		public bool isSecure {
			get {
				return block.isSecure;
			}
		}

		public string InstanceID {
			get {
				return block.InstanceID;
			}
			set {
				block.InstanceID = value;
			}
		}

		public HeContext heContext {
			get {
				return block.heContext;
			}
			set {
				block.heContext = value;
			}
		}

		// proxy implementation INegociateTabs interface method

		public short NegotiateTabIndexes(short tabindex, bool setTabIndex) {
			return block.NegotiateTabIndexes(tabindex, setTabIndex);
		}

		public short NegotiateTabIndexesRecursively(short tabindex, System.Web.UI.Control rootControl, bool setTabIndex) {
			return block.NegotiateTabIndexesRecursively(tabindex, rootControl, setTabIndex);
		}

		public override void DataBind() {
			HandleBreakpoint();
			if (EvaluateParameters != null) {
				EvaluateParameters(this, null);
			}
			block.DataBind();
		}

		public override bool IncludeSpan {
			get {
				return block.IncludeSpan; 
			}
			set {
				block.IncludeSpan = value; 
			}
		}

		public void BindProxyDelegates(object sender, BindDelegatesEventArgs e) {
			BindDelegatesIfNeeded();
			ssProgressCircle.Flows.FlowCommon.WBlkHeader webBlock = (ssProgressCircle.Flows.FlowCommon.WBlkHeader) e.UserControl;

		}

		public override string ParentEditRecord {
			get {
				return block.ParentEditRecord;
			}
			set {
				block.ParentEditRecord = value;
			}
		}

		public static void GetCss(System.IO.TextWriter writer, bool inline, HashSet<string> visited) {
			ssProgressCircle.Flows.FlowCommon.WBlkHeader.GetCss(writer, inline, visited);
		}

		public override string GridCssClasses {
			get {
				return block.GridCssClasses;
			}
			set {
				block.GridCssClasses = value;
			}
		}

		public override string Style {
			get {
				return block.Style;
			}
			set {
				block.Style = value;
			}
		}

		public override string WebBlockIdentifier {
			get {
				return "Proxy." + block.WebBlockIdentifier;
			}
		}

		/// <summary>
		/// Delegate Definitions
		/// </summary>

		// web block public declarations

	}

}
namespace proxy_BIFY_ProgressCircle.Flows.FlowCommon {
	public class WBlkMenu: OSUserControl, INegotiateTabIndexes, IWebScreen, INotifyTriggers, INotifySender
	{
		protected ssProgressCircle.Flows.FlowCommon.WBlkMenu block;
		static WBlkMenu() {
			HeContext heContext = AppInfo.GetAppInfo().OsContext;
			ObjectKey oldCurrentESpaceKey = heContext.CurrentESpaceKey;
			try {
				heContext.CurrentESpaceKey = ssProgressCircle.Global.eSpaceKey;
				ssBIFY.RsseSpaceProgressCircle.MssOnSessionStart(AppInfo.GetAppInfo().OsContext);
			} finally {
				heContext.CurrentESpaceKey = oldCurrentESpaceKey;
			}
		}


		public event EventHandler NotifyTriggered {
			add {
				block.NotifyTriggered += value; 
			}
			remove {
				block.NotifyTriggered -= value; 
			}
		}

		public void OnNotifyCalled(string message) {
			block.OnNotifyCalled(message);
		}

		// override OSUserControl IsProxy()
		public override bool IsProxy() {
			return true;
		}

		// proxy implementation for WebScreen interface methods

		public override event EventHandler EvaluateParameters;

		public override string ClientID {
			get {
				return block.ClientID;
			}
		}


		private readonly ObjectKey eSpaceKey = ObjectKey.Parse("d13b7783-9664-44a6-aec7-55cd5b7155b4") ?? ObjectKey.Dummy;

		public LocalState PushStack() {
			return block.PushStack();
		}

		public void doRefreshScreen(HeContext context) {
			ObjectKey oldCurrentESpaceKey = heContext.CurrentESpaceKey;
			try {
				heContext.CurrentESpaceKey = eSpaceKey;
				block.doRefreshScreen(context);
			} finally {
				heContext.CurrentESpaceKey = oldCurrentESpaceKey;
			}
		}

		public void doAJAXRefreshScreen(HeContext context) {
			ObjectKey oldCurrentESpaceKey = heContext.CurrentESpaceKey;
			try {
				heContext.CurrentESpaceKey = eSpaceKey;
				block.doAJAXRefreshScreen(context);
			} finally {
				heContext.CurrentESpaceKey = oldCurrentESpaceKey;
			}
		}

		public void OnSubmit(String parentEditRecord, bool validate) {
			block.OnSubmit(parentEditRecord, validate);
		}

		public void CheckPermissions(HeContext context) {
			block.CheckPermissions(context);
		}

		public ObjectKey Key {
			get {
				return block.Key;
			}
		}
		public bool isSecure {
			get {
				return block.isSecure;
			}
		}

		public string InstanceID {
			get {
				return block.InstanceID;
			}
			set {
				block.InstanceID = value;
			}
		}

		public HeContext heContext {
			get {
				return block.heContext;
			}
			set {
				block.heContext = value;
			}
		}

		// proxy implementation INegociateTabs interface method

		public short NegotiateTabIndexes(short tabindex, bool setTabIndex) {
			return block.NegotiateTabIndexes(tabindex, setTabIndex);
		}

		public short NegotiateTabIndexesRecursively(short tabindex, System.Web.UI.Control rootControl, bool setTabIndex) {
			return block.NegotiateTabIndexesRecursively(tabindex, rootControl, setTabIndex);
		}

		public override void DataBind() {
			HandleBreakpoint();
			if (EvaluateParameters != null) {
				EvaluateParameters(this, null);
			}
			block.DataBind();
		}

		public override bool IncludeSpan {
			get {
				return block.IncludeSpan; 
			}
			set {
				block.IncludeSpan = value; 
			}
		}

		public void BindProxyDelegates(object sender, BindDelegatesEventArgs e) {
			BindDelegatesIfNeeded();
			ssProgressCircle.Flows.FlowCommon.WBlkMenu webBlock = (ssProgressCircle.Flows.FlowCommon.WBlkMenu) e.UserControl;

		}

		public override string ParentEditRecord {
			get {
				return block.ParentEditRecord;
			}
			set {
				block.ParentEditRecord = value;
			}
		}

		public static void GetCss(System.IO.TextWriter writer, bool inline, HashSet<string> visited) {
			ssProgressCircle.Flows.FlowCommon.WBlkMenu.GetCss(writer, inline, visited);
		}

		public override string GridCssClasses {
			get {
				return block.GridCssClasses;
			}
			set {
				block.GridCssClasses = value;
			}
		}

		public override string Style {
			get {
				return block.Style;
			}
			set {
				block.Style = value;
			}
		}

		public override string WebBlockIdentifier {
			get {
				return "Proxy." + block.WebBlockIdentifier;
			}
		}

		/// <summary>
		/// Delegate Definitions
		/// </summary>

		// web block public declarations

		// block input variable ActiveMenuItemId
		public int inParamActiveMenuItemId {
			set {
				block.inParamActiveMenuItemId = value;
			}
		}

		// block input variable ActiveSubMenuItemId
		public int inParamActiveSubMenuItemId {
			set {
				block.inParamActiveSubMenuItemId = value;
			}
		}

	}

}
