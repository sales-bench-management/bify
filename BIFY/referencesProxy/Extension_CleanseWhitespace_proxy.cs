﻿/* 
 This source code (the "Generated Software") is generated by the OutSystems Platform 
 and is licensed by OutSystems (http://www.outsystems.com) to You solely for testing and evaluation 
 purposes, unless You and OutSystems have executed a specific agreement covering the use terms and 
 conditions of the Generated Software, in which case such agreement shall apply. 
*/

// Proxy for reference Extension with name CleanseWhitespace and key 9L2nW8spsEa_Zo3zQdBDuA
using System;
using System.Data;
using System.Reflection;
using System.Collections;
using System.Runtime.Serialization;
using OutSystems.ObjectKeys;
using OutSystems.RuntimeCommon;
using OutSystems.HubEdition.RuntimePlatform;
using System.Collections.Generic;
using System.Xml;
using OutSystems.HubEdition.RuntimePlatform.Db;
using OutSystems.Internal.Db;
using OutSystems.HubEdition.RuntimePlatform.Log;
using System.Web.UI;
using OutSystems.HubEdition.WebWidgets;
using OutSystems.HubEdition.RuntimePlatform.Web;
using OutSystems.HubEdition.RuntimePlatform.NewRuntime;
using ssBIFY;
namespace ssBIFY {
	/// <summary>
	/// Class: RssExtensionCleanseWhitespace
	/// </summary>
	public partial class RssExtensionCleanseWhitespace {
		/// <summary>
		/// Extension Variable: issCleanseWhitespace
		/// </summary>
		protected static OutSystems.NssCleanseWhitespace.IssCleanseWhitespace issCleanseWhitespace = (OutSystems.NssCleanseWhitespace.IssCleanseWhitespace) new OutSystems.NssCleanseWhitespace.CssCleanseWhitespace();
		protected static int _maxExtensionLogsPerRequest = OutSystems.HubEdition.RuntimePlatform.RuntimePlatformSettings.Misc.MaxLogsPerRequestExtension.GetValue();
		public static void MssRemoveWhitespace(HeContext heContext, string inParamIn, out string outParamOut) {
			DateTime startTime = DateTime.Now;
			String errorLogId = "";
			try {
				issCleanseWhitespace.MssRemoveWhitespace(inParamIn, out outParamOut);
			} catch (Exception ex) {
				errorLogId = ErrorLog.LogApplicationError(ex, heContext, "Extension method execution: CleanseWhitespace.RemoveWhitespace");
				throw ex;
			} finally {
				if (errorLogId != string.Empty || (!heContext.AppInfo.SelectiveLoggingEnabled ||
				(heContext.AppInfo.ExtensionProperties.AllowLogging("17b416c4-12ec-4aa6-a88f-3f119d4e1e99") && heContext.AppInfo.Properties.AllowLogging))) {

					int extLogCount = heContext.ExtensionLogCount;
					if (extLogCount == _maxExtensionLogsPerRequest) {
						// issue warning
						GeneralLog.StaticWrite(
						DateTime.Now, heContext.Session.SessionGuid, heContext.AppInfo.eSpaceId, heContext.AppInfo.Tenant.Id,
						heContext.Session.UserId, "The maximum number (" + _maxExtensionLogsPerRequest + ") of allowed Extension Log entries per request has been exceeded. No more entries will be logged in this request.",
						 "WARNING", "SLOWEXTENSION", "");
						heContext.ExtensionLogCount = extLogCount + 1;
					} else if (extLogCount < _maxExtensionLogsPerRequest) {
						DateTime instant = DateTime.Now;
						int executionDuration = Convert.ToInt32(DateTime.Now.Subtract(startTime).TotalMilliseconds);
						ExtensionLog.StaticWrite(heContext.AppInfo, heContext.Session,
						instant, executionDuration, "RemoveWhitespace", errorLogId, 36, "CleanseWhitespace");
						heContext.ExtensionLogCount = extLogCount + 1;
						RequestTracer reqTracer = heContext.RequestTracer;
						if (reqTracer != null) {
							reqTracer.RegisterExtensionExecuted("a5009b81-bf1d-4193-b300-724d4e51fb5e", "RemoveWhitespace", "f82563d5-ffee-43c2-900d-8425d42939cd", "BIFY", executionDuration, instant); 
						}
					}
				}
				RuntimePlatformUtils.LogSlowExtensionCall(startTime, "CleanseWhitespace.RemoveWhitespace");
			}
		}




		public class Factory {
		}
		public class DefaultValues {
		}
	}
}
