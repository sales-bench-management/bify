﻿<%@ Page Language="c#" Codebehind="WelcomeToBify.aspx.cs" AutoEventWireup="false" Inherits="ssBIFY.Flows.FlowLoggedIn.ScrnWelcomeToBify" %>
<%@ Register TagPrefix="osweb" Namespace="OutSystems.HubEdition.WebWidgets" Assembly="OutSystems.HubEdition.WebWidgets" %>
<%@ Import namespace="ssBIFY" %>
<%@ Import namespace="OutSystems.HubEdition.RuntimePlatform" %>
<%@ Assembly Name="OutSystems.WidgetsRuntimeAPI" %>
<%@ Register TagPrefix="widgets" TagName="Kvm5_RC0mw0KuAAMxuiwtpQ" Src="Blocks\BIFY\Common\Layout.ascx" %>
<%@ Register TagPrefix="widgets" TagName="KXIDBGmiH30CQls7epjSjoQ" Src="Blocks\BIFY\Common\ApplicationTitle.ascx" %>
<%@ Register TagPrefix="widgets" TagName="K_5sqIAe8Z0mLMNBss5hGzg" Src="Blocks\BIFY\Common\Menu.ascx" %>
<%@ Register TagPrefix="widgets" TagName="KAotDlTFMDkS99aVrmrPR8g" Src="Blocks\BIFY\Weblocks\MeFriendsBlock.ascx" %>
<%@ Register TagPrefix="widgets" TagName="KlboP3Mo1qU6C2FuyPN_r2w" Src="Blocks\BIFY\Weblocks\SearchBoxHeader.ascx" %>
<%@ Register TagPrefix="RichWidgets_widgets" TagName="KmbfKJ2gWQUq1Gwk_0SjV4w" Src="Blocks\BIFY\RichWidgets\Icon.ascx" %>
<%@ Register TagPrefix="widgets" TagName="KLNiAvNNCSUGGITe8itt4jg" Src="Blocks\BIFY\ModalContent\WelcomeToBifyContent.ascx" %>
<%@ Register TagPrefix="widgets" TagName="KcGiUZvJCtEiF3fde7a3MaQ" Src="Blocks\BIFY\Weblocks\FooterLoggedIn.ascx" %>

<%= BIFY_Properties.DocType %>
<html xmlns="http://www.w3.org/1999/xhtml" >
  <head runat="server"><%= GetHeadTopJavaScript() %>
	<title><%= HttpUtility.HtmlEncode (Title) %></title>
    <meta http-equiv="Content-Type" content="<%= "text/html; charset=" + Response.ContentEncoding.WebName %>" />
    <meta http-equiv="Content-Script-Type" content="text/javascript" />
    <meta http-equiv="Content-Style-Type" content="text/css" />
<%= "\n" + GetStyleSheetIncludes() %><%= GetRequestInfoJavaScript() + GetJavaScriptIncludes() + GetHeadBottomJavaScript() %>
  </head>
  <osweb:Body runat="server"><%= GetBodyTopJavaScript() %>
    <osweb:Form id="WebForm1" method="post"  action="<%# GetFormAction() %>" runat="server">
<widgets:Kvm5_RC0mw0KuAAMxuiwtpQ runat="server" id="wt_WebBlockInstance19" OnEvaluateParameters="webBlck_WebBlockInstance19_onDataBinding" InstanceID="_WebBlockInstance19"><phLeft><widgets:KXIDBGmiH30CQls7epjSjoQ runat="server" id="wt_WebBlockInstance14" OnEvaluateParameters="webBlck_WebBlockInstance14_onDataBinding" InstanceID="_WebBlockInstance14"></widgets:KXIDBGmiH30CQls7epjSjoQ></phLeft><phCenter><widgets:K_5sqIAe8Z0mLMNBss5hGzg runat="server" id="wt_WebBlockInstance13" OnEvaluateParameters="webBlck_WebBlockInstance13_onDataBinding" InstanceID="_WebBlockInstance13"></widgets:K_5sqIAe8Z0mLMNBss5hGzg></phCenter><phRight><widgets:KAotDlTFMDkS99aVrmrPR8g runat="server" id="wt_WebBlockInstance12" OnEvaluateParameters="webBlck_WebBlockInstance12_onDataBinding" InstanceID="_WebBlockInstance12"></widgets:KAotDlTFMDkS99aVrmrPR8g><widgets:KlboP3Mo1qU6C2FuyPN_r2w runat="server" id="wt_WebBlockInstance7" OnEvaluateParameters="webBlck_WebBlockInstance7_onDataBinding" InstanceID="_WebBlockInstance7"></widgets:KlboP3Mo1qU6C2FuyPN_r2w></phRight><phHeaderContent></phHeaderContent><phBreadcrumbs><osweb:Container runat="server" id="wt_Container21" anonymous="true" onDataBinding="cnt_Container21_onDataBinding"><osweb:HyperLink runat="server" id="wt_Link10" Visible="<%# lnk_Link10_isVisible() %>" Enabled="<%# lnk_Link10_isEnabled() %>" NavigateUrl="<%# lnk_Link10_NavigateUrl() %>"><osweb:Container runat="server" id="wt_Container11" anonymous="true" onDataBinding="cnt_Container11_onDataBinding" GridCssClasses="OSInline"><RichWidgets_widgets:KmbfKJ2gWQUq1Gwk_0SjV4w runat="server" id="RichWidgets_wt_WebBlockInstance6" OnEvaluateParameters="RichWidgets_webBlck_WebBlockInstance6_onDataBinding" InstanceID="_WebBlockInstance6"></RichWidgets_widgets:KmbfKJ2gWQUq1Gwk_0SjV4w><osweb:PlaceHolder runat="server"><%# "Back" %></osweb:PlaceHolder></osweb:Container></osweb:HyperLink></osweb:Container></phBreadcrumbs><phTitle></phTitle><phActions></phActions><phMainContent><widgets:KLNiAvNNCSUGGITe8itt4jg runat="server" id="wt_WebBlockInstance3" OnEvaluateParameters="webBlck_WebBlockInstance3_onDataBinding" OnBindDelegates="webBlck_WebBlockInstance3_BindDelegates" InstanceID="_WebBlockInstance3"></widgets:KLNiAvNNCSUGGITe8itt4jg></phMainContent><phFooter><widgets:KcGiUZvJCtEiF3fde7a3MaQ runat="server" id="wt_WebBlockInstance5" OnEvaluateParameters="webBlck_WebBlockInstance5_onDataBinding" InstanceID="_WebBlockInstance5"></widgets:KcGiUZvJCtEiF3fde7a3MaQ></phFooter></widgets:Kvm5_RC0mw0KuAAMxuiwtpQ><osweb:DummySubmitLink runat="server" id="Dummy_Submit_Link"/>
    <%= OutSystems.HubEdition.RuntimePlatform.AppInfo.GetAppInfo().GetWatermark() %><%= GetCallbackDebug()	
%><%= GetVisitCode() %></osweb:Form><%= GetEndPageJavaScript() + GetBodyBottomJavaScript() %>
  </osweb:Body>
</html>
