﻿/* 
 This source code (the "Generated Software") is generated by the OutSystems Platform 
 and is licensed by OutSystems (http://www.outsystems.com) to You solely for testing and evaluation 
 purposes, unless You and OutSystems have executed a specific agreement covering the use terms and 
 conditions of the Generated Software, in which case such agreement shall apply. 
*/

using System;
using System.Linq;
using OutSystems.HubEdition.RuntimePlatform;
using OutSystems.HubEdition.RuntimePlatform.Db;
using OutSystems.Internal.Db;
using OutSystems.RuntimeCommon;
using OutSystems.RuntimeCommon.Caching;

using System.Text;
using System.Data;
using System.Collections.Generic;
using Newtonsoft.Json;
using OutSystems.RESTService;
using OutSystems.RESTService.Behaviors;
using OutSystems.RESTService.Controllers;
using OutSystems.ObjectKeys;
using OutSystems.HubEdition.RuntimePlatform.NewRuntime;


namespace ssBIFY.RestRecords {

	public class JSONENFAQEntityRecord: AbstractRESTStructure<ENFAQEntityRecord> {
		[JsonProperty("Id")]
		public long? AttrId;

		[JsonProperty("Title")]
		public string AttrTitle;

		[JsonProperty("Detail")]
		public string AttrDetail;

		public JSONENFAQEntityRecord() {}

		public JSONENFAQEntityRecord(ENFAQEntityRecord s, IBehaviorsConfiguration config) {
			if (config.DefaultValuesBehavior == DefaultValuesBehavior.DontSend) {
				AttrId = (long?) s.ssId;
				AttrTitle = ConvertToRestWithoutDefaults(s.ssTitle, "");
				AttrDetail = ConvertToRestWithoutDefaults(s.ssDetail, "");
			} else {
				AttrId = (long?) s.ssId;
				AttrTitle = s.ssTitle;
				AttrDetail = s.ssDetail;
			}
		}

		public static Func<ssBIFY.RestRecords.JSONENFAQEntityRecord, ENFAQEntityRecord> ToStructureDelegate(IBehaviorsConfiguration config) {
			return (ssBIFY.RestRecords.JSONENFAQEntityRecord s) => ToStructure(s, config);
		}
		public static ENFAQEntityRecord ToStructure(ssBIFY.RestRecords.JSONENFAQEntityRecord obj, IBehaviorsConfiguration config) {
			ENFAQEntityRecord s = new ENFAQEntityRecord(null);
			if (obj != null) {
				s.ssId = obj.AttrId == null ? 0L: obj.AttrId.Value;
				s.ssTitle = obj.AttrTitle == null ? "": obj.AttrTitle;
				s.ssDetail = obj.AttrDetail == null ? "": obj.AttrDetail;
			}
			return s;
		}

		public static Func<ENFAQEntityRecord, ssBIFY.RestRecords.JSONENFAQEntityRecord> FromStructureDelegate(IBehaviorsConfiguration config) {
			return (ENFAQEntityRecord s) => FromStructure(s, config);
		}
		public static ssBIFY.RestRecords.JSONENFAQEntityRecord FromStructure(ENFAQEntityRecord s, IBehaviorsConfiguration config) {
			return new ssBIFY.RestRecords.JSONENFAQEntityRecord(s, config);
		}

	}



}