﻿define("OutSystemsUIWeb.model$StepRec", ["exports", "OutSystems/ClientRuntime/Main", "OutSystemsUIWeb.model"], function (exports, OutSystems, OutSystemsUIWebModel) {
var OS = OutSystems.Internal;
var StepRec = (function (_super) {
__extends(StepRec, _super);
function StepRec(defaults) {
_super.apply(this, arguments);
}
StepRec.attributesToDeclare = function () {
return [
this.attr("Steps", "stepsAttr", "Steps", true, false, OS.Types.Text, function () {
return "";
}, true)
].concat(_super.attributesToDeclare.call(this));
};
StepRec.fromStructure = function (str) {
return new StepRec(new StepRec.RecordClass({
stepsAttr: OS.DataTypes.ImmutableBase.getData(str)
}));
};
StepRec.init();
return StepRec;
})(OS.DataTypes.GenericRecord);
OutSystemsUIWebModel.StepRec = StepRec;

});
define("OutSystemsUIWeb.model$SpeedRec", ["exports", "OutSystems/ClientRuntime/Main", "OutSystemsUIWeb.model"], function (exports, OutSystems, OutSystemsUIWebModel) {
var OS = OutSystems.Internal;
var SpeedRec = (function (_super) {
__extends(SpeedRec, _super);
function SpeedRec(defaults) {
_super.apply(this, arguments);
}
SpeedRec.attributesToDeclare = function () {
return [
this.attr("Speed", "speedAttr", "Speed", true, false, OS.Types.Text, function () {
return "";
}, true)
].concat(_super.attributesToDeclare.call(this));
};
SpeedRec.fromStructure = function (str) {
return new SpeedRec(new SpeedRec.RecordClass({
speedAttr: OS.DataTypes.ImmutableBase.getData(str)
}));
};
SpeedRec.init();
return SpeedRec;
})(OS.DataTypes.GenericRecord);
OutSystemsUIWebModel.SpeedRec = SpeedRec;

});
define("OutSystemsUIWeb.model$ProgressBarSizeRec", ["exports", "OutSystems/ClientRuntime/Main", "OutSystemsUIWeb.model"], function (exports, OutSystems, OutSystemsUIWebModel) {
var OS = OutSystems.Internal;
var ProgressBarSizeRec = (function (_super) {
__extends(ProgressBarSizeRec, _super);
function ProgressBarSizeRec(defaults) {
_super.apply(this, arguments);
}
ProgressBarSizeRec.attributesToDeclare = function () {
return [
this.attr("ProgressBarSize", "progressBarSizeAttr", "ProgressBarSize", true, false, OS.Types.Text, function () {
return "";
}, true), 
this.attr("Order", "orderAttr", "Order", true, false, OS.Types.Integer, function () {
return 0;
}, true)
].concat(_super.attributesToDeclare.call(this));
};
ProgressBarSizeRec.init();
return ProgressBarSizeRec;
})(OS.DataTypes.GenericRecord);
OutSystemsUIWebModel.ProgressBarSizeRec = ProgressBarSizeRec;

});
define("OutSystemsUIWeb.model$ColorRec", ["exports", "OutSystems/ClientRuntime/Main", "OutSystemsUIWeb.model"], function (exports, OutSystems, OutSystemsUIWebModel) {
var OS = OutSystems.Internal;
var ColorRec = (function (_super) {
__extends(ColorRec, _super);
function ColorRec(defaults) {
_super.apply(this, arguments);
}
ColorRec.attributesToDeclare = function () {
return [
this.attr("Color", "colorAttr", "Color", true, false, OS.Types.Text, function () {
return "";
}, true), 
this.attr("Order", "orderAttr", "Order", true, false, OS.Types.Integer, function () {
return 0;
}, true)
].concat(_super.attributesToDeclare.call(this));
};
ColorRec.init();
return ColorRec;
})(OS.DataTypes.GenericRecord);
OutSystemsUIWebModel.ColorRec = ColorRec;

});
define("OutSystemsUIWeb.model$DeviceConfigurationRec", ["exports", "OutSystems/ClientRuntime/Main", "OutSystemsUIWeb.model"], function (exports, OutSystems, OutSystemsUIWebModel) {
var OS = OutSystems.Internal;
var DeviceConfigurationRec = (function (_super) {
__extends(DeviceConfigurationRec, _super);
function DeviceConfigurationRec(defaults) {
_super.apply(this, arguments);
}
DeviceConfigurationRec.attributesToDeclare = function () {
return [
this.attr("PhoneWidth", "phoneWidthAttr", "PhoneWidth", false, false, OS.Types.Integer, function () {
return 420;
}, true), 
this.attr("TabletWidth", "tabletWidthAttr", "TabletWidth", false, false, OS.Types.Integer, function () {
return 1024;
}, true), 
this.attr("DesktopSmallWidth", "desktopSmallWidthAttr", "DesktopSmallWidth", false, false, OS.Types.Integer, function () {
return 1366;
}, true), 
this.attr("DesktopWidth", "desktopWidthAttr", "DesktopWidth", false, false, OS.Types.Integer, function () {
return 1600;
}, true), 
this.attr("DesktopBigWidth", "desktopBigWidthAttr", "DesktopBigWidth", false, false, OS.Types.Integer, function () {
return 1920;
}, true)
].concat(_super.attributesToDeclare.call(this));
};
DeviceConfigurationRec.init();
return DeviceConfigurationRec;
})(OS.DataTypes.GenericRecord);
OutSystemsUIWebModel.DeviceConfigurationRec = DeviceConfigurationRec;

});
define("OutSystemsUIWeb.model$LeaveAnimationRec", ["exports", "OutSystems/ClientRuntime/Main", "OutSystemsUIWeb.model"], function (exports, OutSystems, OutSystemsUIWebModel) {
var OS = OutSystems.Internal;
var LeaveAnimationRec = (function (_super) {
__extends(LeaveAnimationRec, _super);
function LeaveAnimationRec(defaults) {
_super.apply(this, arguments);
}
LeaveAnimationRec.attributesToDeclare = function () {
return [
this.attr("LeaveAnimation", "leaveAnimationAttr", "LeaveAnimation", true, false, OS.Types.Text, function () {
return "";
}, true)
].concat(_super.attributesToDeclare.call(this));
};
LeaveAnimationRec.fromStructure = function (str) {
return new LeaveAnimationRec(new LeaveAnimationRec.RecordClass({
leaveAnimationAttr: OS.DataTypes.ImmutableBase.getData(str)
}));
};
LeaveAnimationRec.init();
return LeaveAnimationRec;
})(OS.DataTypes.GenericRecord);
OutSystemsUIWebModel.LeaveAnimationRec = LeaveAnimationRec;

});
define("OutSystemsUIWeb.model$", ["exports", "OutSystems/ClientRuntime/Main", "OutSystemsUIWeb.model"], function (exports, OutSystems, OutSystemsUIWebModel) {
var OS = OutSystems.Internal;
var DateTimeList = (function (_super) {
__extends(DateTimeList, _super);
function DateTimeList(defaults) {
_super.apply(this, arguments);
}
DateTimeList.itemType = OS.Types.DateTime;
return DateTimeList;
})(OS.DataTypes.GenericRecordList);
OutSystemsUIWebModel.DateTimeList = DateTimeList;

});
define("OutSystemsUIWeb.model$DatePickerAdvancedFormatRec", ["exports", "OutSystems/ClientRuntime/Main", "OutSystemsUIWeb.model"], function (exports, OutSystems, OutSystemsUIWebModel) {
var OS = OutSystems.Internal;
var DatePickerAdvancedFormatRec = (function (_super) {
__extends(DatePickerAdvancedFormatRec, _super);
function DatePickerAdvancedFormatRec(defaults) {
_super.apply(this, arguments);
}
DatePickerAdvancedFormatRec.attributesToDeclare = function () {
return [
this.attr("EventsList", "eventsListAttr", "EventsList", false, false, OS.Types.RecordList, function () {
return OS.DataTypes.ImmutableBase.getData(new OS.DataTypes.DateTimeList());
}, true, OS.DataTypes.DateTimeList), 
this.attr("AdvancedFormatJSON", "advancedFormatJSONAttr", "AdvancedFormatJSON", false, false, OS.Types.Text, function () {
return "{}";
}, true)
].concat(_super.attributesToDeclare.call(this));
};
DatePickerAdvancedFormatRec.init();
return DatePickerAdvancedFormatRec;
})(OS.DataTypes.GenericRecord);
OutSystemsUIWebModel.DatePickerAdvancedFormatRec = DatePickerAdvancedFormatRec;

});
define("OutSystemsUIWeb.model$AlertRec", ["exports", "OutSystems/ClientRuntime/Main", "OutSystemsUIWeb.model"], function (exports, OutSystems, OutSystemsUIWebModel) {
var OS = OutSystems.Internal;
var AlertRec = (function (_super) {
__extends(AlertRec, _super);
function AlertRec(defaults) {
_super.apply(this, arguments);
}
AlertRec.attributesToDeclare = function () {
return [
this.attr("Alert", "alertAttr", "Alert", true, false, OS.Types.Text, function () {
return "";
}, true)
].concat(_super.attributesToDeclare.call(this));
};
AlertRec.fromStructure = function (str) {
return new AlertRec(new AlertRec.RecordClass({
alertAttr: OS.DataTypes.ImmutableBase.getData(str)
}));
};
AlertRec.init();
return AlertRec;
})(OS.DataTypes.GenericRecord);
OutSystemsUIWebModel.AlertRec = AlertRec;

});
define("OutSystemsUIWeb.model$SpaceRec", ["exports", "OutSystems/ClientRuntime/Main", "OutSystemsUIWeb.model"], function (exports, OutSystems, OutSystemsUIWebModel) {
var OS = OutSystems.Internal;
var SpaceRec = (function (_super) {
__extends(SpaceRec, _super);
function SpaceRec(defaults) {
_super.apply(this, arguments);
}
SpaceRec.attributesToDeclare = function () {
return [
this.attr("Space", "spaceAttr", "Space", true, false, OS.Types.Text, function () {
return "";
}, true), 
this.attr("Order", "orderAttr", "Order", true, false, OS.Types.Integer, function () {
return 0;
}, true)
].concat(_super.attributesToDeclare.call(this));
};
SpaceRec.init();
return SpaceRec;
})(OS.DataTypes.GenericRecord);
OutSystemsUIWebModel.SpaceRec = SpaceRec;

});
define("OutSystemsUIWeb.model$AccessibilityConfigurationRec", ["exports", "OutSystems/ClientRuntime/Main", "OutSystemsUIWeb.model"], function (exports, OutSystems, OutSystemsUIWebModel) {
var OS = OutSystems.Internal;
var AccessibilityConfigurationRec = (function (_super) {
__extends(AccessibilityConfigurationRec, _super);
function AccessibilityConfigurationRec(defaults) {
_super.apply(this, arguments);
}
AccessibilityConfigurationRec.attributesToDeclare = function () {
return [
this.attr("ResetTabIndex", "resetTabIndexAttr", "ResetTabIndex", false, false, OS.Types.Boolean, function () {
return false;
}, true), 
this.attr("ShowFocus", "showFocusAttr", "ShowFocus", false, false, OS.Types.Boolean, function () {
return false;
}, true), 
this.attr("ShowSkipToContent", "showSkipToContentAttr", "ShowSkipToContent", false, false, OS.Types.Boolean, function () {
return false;
}, true), 
this.attr("Lang", "langAttr", "Lang", false, false, OS.Types.Text, function () {
return "";
}, true)
].concat(_super.attributesToDeclare.call(this));
};
AccessibilityConfigurationRec.init();
return AccessibilityConfigurationRec;
})(OS.DataTypes.GenericRecord);
OutSystemsUIWebModel.AccessibilityConfigurationRec = AccessibilityConfigurationRec;

});
define("OutSystemsUIWeb.model$SizeRec", ["exports", "OutSystems/ClientRuntime/Main", "OutSystemsUIWeb.model"], function (exports, OutSystems, OutSystemsUIWebModel) {
var OS = OutSystems.Internal;
var SizeRec = (function (_super) {
__extends(SizeRec, _super);
function SizeRec(defaults) {
_super.apply(this, arguments);
}
SizeRec.attributesToDeclare = function () {
return [
this.attr("Size", "sizeAttr", "Size", true, false, OS.Types.Text, function () {
return "";
}, true)
].concat(_super.attributesToDeclare.call(this));
};
SizeRec.fromStructure = function (str) {
return new SizeRec(new SizeRec.RecordClass({
sizeAttr: OS.DataTypes.ImmutableBase.getData(str)
}));
};
SizeRec.init();
return SizeRec;
})(OS.DataTypes.GenericRecord);
OutSystemsUIWebModel.SizeRec = SizeRec;

});
define("OutSystemsUIWeb.model$ShapeRec", ["exports", "OutSystems/ClientRuntime/Main", "OutSystemsUIWeb.model"], function (exports, OutSystems, OutSystemsUIWebModel) {
var OS = OutSystems.Internal;
var ShapeRec = (function (_super) {
__extends(ShapeRec, _super);
function ShapeRec(defaults) {
_super.apply(this, arguments);
}
ShapeRec.attributesToDeclare = function () {
return [
this.attr("Shape", "shapeAttr", "Shape", true, false, OS.Types.Text, function () {
return "";
}, true), 
this.attr("Order", "orderAttr", "Order", false, false, OS.Types.Integer, function () {
return 0;
}, true)
].concat(_super.attributesToDeclare.call(this));
};
ShapeRec.init();
return ShapeRec;
})(OS.DataTypes.GenericRecord);
OutSystemsUIWebModel.ShapeRec = ShapeRec;

});
define("OutSystemsUIWeb.model$DeviceResponsiveRec", ["exports", "OutSystems/ClientRuntime/Main", "OutSystemsUIWeb.model"], function (exports, OutSystems, OutSystemsUIWebModel) {
var OS = OutSystems.Internal;
var DeviceResponsiveRec = (function (_super) {
__extends(DeviceResponsiveRec, _super);
function DeviceResponsiveRec(defaults) {
_super.apply(this, arguments);
}
DeviceResponsiveRec.attributesToDeclare = function () {
return [
this.attr("Type", "typeAttr", "Type", true, false, OS.Types.Text, function () {
return "";
}, true), 
this.attr("Order", "orderAttr", "Order", true, false, OS.Types.Integer, function () {
return 0;
}, true), 
this.attr("Is_Active", "is_ActiveAttr", "Is_Active", true, false, OS.Types.Boolean, function () {
return false;
}, true)
].concat(_super.attributesToDeclare.call(this));
};
DeviceResponsiveRec.init();
return DeviceResponsiveRec;
})(OS.DataTypes.GenericRecord);
OutSystemsUIWebModel.DeviceResponsiveRec = DeviceResponsiveRec;

});
define("OutSystemsUIWeb.model$EnterAnimationRec", ["exports", "OutSystems/ClientRuntime/Main", "OutSystemsUIWeb.model"], function (exports, OutSystems, OutSystemsUIWebModel) {
var OS = OutSystems.Internal;
var EnterAnimationRec = (function (_super) {
__extends(EnterAnimationRec, _super);
function EnterAnimationRec(defaults) {
_super.apply(this, arguments);
}
EnterAnimationRec.attributesToDeclare = function () {
return [
this.attr("EnterAnimation", "enterAnimationAttr", "EnterAnimation", true, false, OS.Types.Text, function () {
return "";
}, true)
].concat(_super.attributesToDeclare.call(this));
};
EnterAnimationRec.fromStructure = function (str) {
return new EnterAnimationRec(new EnterAnimationRec.RecordClass({
enterAnimationAttr: OS.DataTypes.ImmutableBase.getData(str)
}));
};
EnterAnimationRec.init();
return EnterAnimationRec;
})(OS.DataTypes.GenericRecord);
OutSystemsUIWebModel.EnterAnimationRec = EnterAnimationRec;

});
define("OutSystemsUIWeb.model$PositionExtendedRec", ["exports", "OutSystems/ClientRuntime/Main", "OutSystemsUIWeb.model"], function (exports, OutSystems, OutSystemsUIWebModel) {
var OS = OutSystems.Internal;
var PositionExtendedRec = (function (_super) {
__extends(PositionExtendedRec, _super);
function PositionExtendedRec(defaults) {
_super.apply(this, arguments);
}
PositionExtendedRec.attributesToDeclare = function () {
return [
this.attr("PositionExtended", "positionExtendedAttr", "PositionExtended", true, false, OS.Types.Text, function () {
return "";
}, true)
].concat(_super.attributesToDeclare.call(this));
};
PositionExtendedRec.fromStructure = function (str) {
return new PositionExtendedRec(new PositionExtendedRec.RecordClass({
positionExtendedAttr: OS.DataTypes.ImmutableBase.getData(str)
}));
};
PositionExtendedRec.init();
return PositionExtendedRec;
})(OS.DataTypes.GenericRecord);
OutSystemsUIWebModel.PositionExtendedRec = PositionExtendedRec;

});
define("OutSystemsUIWeb.model$PositionBaseRec", ["exports", "OutSystems/ClientRuntime/Main", "OutSystemsUIWeb.model"], function (exports, OutSystems, OutSystemsUIWebModel) {
var OS = OutSystems.Internal;
var PositionBaseRec = (function (_super) {
__extends(PositionBaseRec, _super);
function PositionBaseRec(defaults) {
_super.apply(this, arguments);
}
PositionBaseRec.attributesToDeclare = function () {
return [
this.attr("PositionBase", "positionBaseAttr", "PositionBase", true, false, OS.Types.Text, function () {
return "";
}, true)
].concat(_super.attributesToDeclare.call(this));
};
PositionBaseRec.fromStructure = function (str) {
return new PositionBaseRec(new PositionBaseRec.RecordClass({
positionBaseAttr: OS.DataTypes.ImmutableBase.getData(str)
}));
};
PositionBaseRec.init();
return PositionBaseRec;
})(OS.DataTypes.GenericRecord);
OutSystemsUIWebModel.PositionBaseRec = PositionBaseRec;

});
define("OutSystemsUIWeb.model$GutterSizeRec", ["exports", "OutSystems/ClientRuntime/Main", "OutSystemsUIWeb.model"], function (exports, OutSystems, OutSystemsUIWebModel) {
var OS = OutSystems.Internal;
var GutterSizeRec = (function (_super) {
__extends(GutterSizeRec, _super);
function GutterSizeRec(defaults) {
_super.apply(this, arguments);
}
GutterSizeRec.attributesToDeclare = function () {
return [
this.attr("GutterSize", "gutterSizeAttr", "GutterSize", true, false, OS.Types.Text, function () {
return "";
}, true), 
this.attr("Order", "orderAttr", "Order", true, false, OS.Types.Integer, function () {
return 0;
}, true)
].concat(_super.attributesToDeclare.call(this));
};
GutterSizeRec.init();
return GutterSizeRec;
})(OS.DataTypes.GenericRecord);
OutSystemsUIWebModel.GutterSizeRec = GutterSizeRec;

});
define("OutSystemsUIWeb.model$BreakColumnsRec", ["exports", "OutSystems/ClientRuntime/Main", "OutSystemsUIWeb.model"], function (exports, OutSystems, OutSystemsUIWebModel) {
var OS = OutSystems.Internal;
var BreakColumnsRec = (function (_super) {
__extends(BreakColumnsRec, _super);
function BreakColumnsRec(defaults) {
_super.apply(this, arguments);
}
BreakColumnsRec.attributesToDeclare = function () {
return [
this.attr("BreakColumns", "breakColumnsAttr", "BreakColumns", true, false, OS.Types.Text, function () {
return "";
}, true)
].concat(_super.attributesToDeclare.call(this));
};
BreakColumnsRec.fromStructure = function (str) {
return new BreakColumnsRec(new BreakColumnsRec.RecordClass({
breakColumnsAttr: OS.DataTypes.ImmutableBase.getData(str)
}));
};
BreakColumnsRec.init();
return BreakColumnsRec;
})(OS.DataTypes.GenericRecord);
OutSystemsUIWebModel.BreakColumnsRec = BreakColumnsRec;

});
define("OutSystemsUIWeb.model$ResponsiveTableRecordsRec", ["exports", "OutSystems/ClientRuntime/Main", "OutSystemsUIWeb.model"], function (exports, OutSystems, OutSystemsUIWebModel) {
var OS = OutSystems.Internal;
var ResponsiveTableRecordsRec = (function (_super) {
__extends(ResponsiveTableRecordsRec, _super);
function ResponsiveTableRecordsRec(defaults) {
_super.apply(this, arguments);
}
ResponsiveTableRecordsRec.attributesToDeclare = function () {
return [
this.attr("ResponsiveTableRecords", "responsiveTableRecordsAttr", "ResponsiveTableRecords", true, false, OS.Types.Text, function () {
return "";
}, true)
].concat(_super.attributesToDeclare.call(this));
};
ResponsiveTableRecordsRec.fromStructure = function (str) {
return new ResponsiveTableRecordsRec(new ResponsiveTableRecordsRec.RecordClass({
responsiveTableRecordsAttr: OS.DataTypes.ImmutableBase.getData(str)
}));
};
ResponsiveTableRecordsRec.init();
return ResponsiveTableRecordsRec;
})(OS.DataTypes.GenericRecord);
OutSystemsUIWebModel.ResponsiveTableRecordsRec = ResponsiveTableRecordsRec;

});
define("OutSystemsUIWeb.model$", ["exports", "OutSystems/ClientRuntime/Main", "OutSystemsUIWeb.model"], function (exports, OutSystems, OutSystemsUIWebModel) {
var OS = OutSystems.Internal;
var TimeList = (function (_super) {
__extends(TimeList, _super);
function TimeList(defaults) {
_super.apply(this, arguments);
}
TimeList.itemType = OS.Types.Time;
return TimeList;
})(OS.DataTypes.GenericRecordList);
OutSystemsUIWebModel.TimeList = TimeList;

});
define("OutSystemsUIWeb.model$TimePickerAdvancedFormatRec", ["exports", "OutSystems/ClientRuntime/Main", "OutSystemsUIWeb.model"], function (exports, OutSystems, OutSystemsUIWebModel) {
var OS = OutSystems.Internal;
var TimePickerAdvancedFormatRec = (function (_super) {
__extends(TimePickerAdvancedFormatRec, _super);
function TimePickerAdvancedFormatRec(defaults) {
_super.apply(this, arguments);
}
TimePickerAdvancedFormatRec.attributesToDeclare = function () {
return [
this.attr("DisabledTimes", "disabledTimesAttr", "DisabledTimes", false, false, OS.Types.RecordList, function () {
return OS.DataTypes.ImmutableBase.getData(new OS.DataTypes.TimeList());
}, true, OS.DataTypes.TimeList), 
this.attr("MinTime", "minTimeAttr", "MinTime", false, false, OS.Types.Time, function () {
return OS.DataTypes.DateTime.defaultValue;
}, true), 
this.attr("MaxTime", "maxTimeAttr", "MaxTime", false, false, OS.Types.Time, function () {
return OS.DataTypes.DateTime.defaultValue;
}, true), 
this.attr("StartEmpty", "startEmptyAttr", "StartEmpty", false, false, OS.Types.Boolean, function () {
return false;
}, true)
].concat(_super.attributesToDeclare.call(this));
};
TimePickerAdvancedFormatRec.init();
return TimePickerAdvancedFormatRec;
})(OS.DataTypes.GenericRecord);
OutSystemsUIWebModel.TimePickerAdvancedFormatRec = TimePickerAdvancedFormatRec;

});
define("OutSystemsUIWeb.model$TriggerRec", ["exports", "OutSystems/ClientRuntime/Main", "OutSystemsUIWeb.model"], function (exports, OutSystems, OutSystemsUIWebModel) {
var OS = OutSystems.Internal;
var TriggerRec = (function (_super) {
__extends(TriggerRec, _super);
function TriggerRec(defaults) {
_super.apply(this, arguments);
}
TriggerRec.attributesToDeclare = function () {
return [
this.attr("TriggerAction", "triggerActionAttr", "TriggerAction", true, false, OS.Types.Text, function () {
return "";
}, true), 
this.attr("Order", "orderAttr", "Order", true, false, OS.Types.Integer, function () {
return 0;
}, true)
].concat(_super.attributesToDeclare.call(this));
};
TriggerRec.init();
return TriggerRec;
})(OS.DataTypes.GenericRecord);
OutSystemsUIWebModel.TriggerRec = TriggerRec;

});
define("OutSystemsUIWeb.model$OrientationRec", ["exports", "OutSystems/ClientRuntime/Main", "OutSystemsUIWeb.model"], function (exports, OutSystems, OutSystemsUIWebModel) {
var OS = OutSystems.Internal;
var OrientationRec = (function (_super) {
__extends(OrientationRec, _super);
function OrientationRec(defaults) {
_super.apply(this, arguments);
}
OrientationRec.attributesToDeclare = function () {
return [
this.attr("Orientation", "orientationAttr", "Orientation", true, false, OS.Types.Text, function () {
return "";
}, true)
].concat(_super.attributesToDeclare.call(this));
};
OrientationRec.fromStructure = function (str) {
return new OrientationRec(new OrientationRec.RecordClass({
orientationAttr: OS.DataTypes.ImmutableBase.getData(str)
}));
};
OrientationRec.init();
return OrientationRec;
})(OS.DataTypes.GenericRecord);
OutSystemsUIWebModel.OrientationRec = OrientationRec;

});
define("OutSystemsUIWeb.model", ["exports", "OutSystems/ClientRuntime/Main"], function (exports, OutSystems) {
var OS = OutSystems.Internal;
var OutSystemsUIWebModel = exports;
});
