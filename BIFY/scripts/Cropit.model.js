﻿define("Cropit.model$CropitBuilderRec", ["exports", "OutSystems/ClientRuntime/Main", "Cropit.model"], function (exports, OutSystems, CropitModel) {
var OS = OutSystems.Internal;
var CropitBuilderRec = (function (_super) {
__extends(CropitBuilderRec, _super);
function CropitBuilderRec(defaults) {
_super.apply(this, arguments);
}
CropitBuilderRec.attributesToDeclare = function () {
return [
this.attr("divClass", "divClassAttr", "divClass", false, false, OS.Types.Text, function () {
return "cropit-default-container";
}, true), 
this.attr("width", "widthAttr", "width", false, false, OS.Types.Integer, function () {
return 200;
}, true), 
this.attr("height", "heightAttr", "height", false, false, OS.Types.Integer, function () {
return 200;
}, true), 
this.attr("imageBackground", "imageBackgroundAttr", "imageBackground", false, false, OS.Types.Text, function () {
return "false";
}, true), 
this.attr("imageBackgroundBorderWidth", "imageBackgroundBorderWidthAttr", "imageBackgroundBorderWidth", false, false, OS.Types.Text, function () {
return "[0,0,0,0]";
}, true), 
this.attr("allowDragNDrop", "allowDragNDropAttr", "allowDragNDrop", false, false, OS.Types.Text, function () {
return "true";
}, true), 
this.attr("exportZoom", "exportZoomAttr", "exportZoom", false, false, OS.Types.Text, function () {
return "1";
}, true), 
this.attr("exportOriginalSize", "exportOriginalSizeAttr", "exportOriginalSize", false, false, OS.Types.Text, function () {
return "false";
}, true), 
this.attr("minZoom", "minZoomAttr", "minZoom", false, false, OS.Types.Text, function () {
return "\'fill\'";
}, true), 
this.attr("maxZoom", "maxZoomAttr", "maxZoom", false, false, OS.Types.Text, function () {
return "1";
}, true), 
this.attr("initialZoom", "initialZoomAttr", "initialZoom", false, false, OS.Types.Text, function () {
return "\'min\'";
}, true), 
this.attr("freeMove", "freeMoveAttr", "freeMove", false, false, OS.Types.Text, function () {
return "false";
}, true), 
this.attr("smallImage", "smallImageAttr", "smallImage", false, false, OS.Types.Text, function () {
return "\'reject\'";
}, true), 
this.attr("showButtons", "showButtonsAttr", "showButtons", false, false, OS.Types.Boolean, function () {
return true;
}, true), 
this.attr("selectBtnText", "selectBtnTextAttr", "selectBtnText", false, false, OS.Types.Text, function () {
return "Select Image";
}, true), 
this.attr("selectBtnBackground", "selectBtnBackgroundAttr", "selectBtnBackground", false, false, OS.Types.Text, function () {
return "#52a0c7";
}, true), 
this.attr("saveBtnText", "saveBtnTextAttr", "saveBtnText", false, false, OS.Types.Text, function () {
return "Save Image";
}, true), 
this.attr("saveBtnBackground", "saveBtnBackgroundAttr", "saveBtnBackground", false, false, OS.Types.Text, function () {
return "#3b6e22";
}, true), 
this.attr("btnFontColor", "btnFontColorAttr", "btnFontColor", false, false, OS.Types.Text, function () {
return "#FFF";
}, true)
].concat(_super.attributesToDeclare.call(this));
};
CropitBuilderRec.init();
return CropitBuilderRec;
})(OS.DataTypes.GenericRecord);
CropitModel.CropitBuilderRec = CropitBuilderRec;

});
define("Cropit.model", ["exports", "OutSystems/ClientRuntime/Main"], function (exports, OutSystems) {
var OS = OutSystems.Internal;
var CropitModel = exports;
});
