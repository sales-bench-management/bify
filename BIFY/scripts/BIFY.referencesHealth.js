﻿define("BIFY.referencesHealth$BIFYBackOffice",[], function() {
	// Reference to producer 'BIFYBackOffice' is OK.
});
define("BIFY.referencesHealth$BinaryData",[], function() {
	// Reference to producer 'BinaryData' is OK.
});
define("BIFY.referencesHealth$Charts",[], function() {
	// Reference to producer 'Charts' is OK.
});
define("BIFY.referencesHealth$CheckURL",[], function() {
	// Reference to producer 'CheckURL' is OK.
});
define("BIFY.referencesHealth$CheckValidUrl",[], function() {
	// Reference to producer 'CheckValidUrl' is OK.
});
define("BIFY.referencesHealth$CleanseWhitespace",[], function() {
	// Reference to producer 'CleanseWhitespace' is OK.
});
define("BIFY.referencesHealth$ConfirmationDialog",[], function() {
	// Reference to producer 'ConfirmationDialog' is OK.
});
define("BIFY.referencesHealth$Cropit",[], function() {
	// Reference to producer 'Cropit' is OK.
});
define("BIFY.referencesHealth$DoddleBinary",[], function() {
	// Reference to producer 'DoddleBinary' is OK.
});
define("BIFY.referencesHealth$Extended_REST",[], function() {
	// Reference to producer 'Extended_REST' is OK.
});
define("BIFY.referencesHealth$FileUploadAndImgPreviewOnChooseFile",[], function() {
	// Reference to producer 'FileUploadAndImgPreviewOnChooseFile' is OK.
});
define("BIFY.referencesHealth$GetMeta",[], function() {
	// Reference to producer 'GetMeta' is OK.
});
define("BIFY.referencesHealth$HtmlRenderer",[], function() {
	// Reference to producer 'HtmlRenderer' is OK.
});
define("BIFY.referencesHealth$HTTPRequestHandler",[], function() {
	// Reference to producer 'HTTPRequestHandler' is OK.
});
define("BIFY.referencesHealth$ImageCompress",[], function() {
	// Reference to producer 'ImageCompress' is OK.
});
define("BIFY.referencesHealth$ImageConversion",[], function() {
	// Reference to producer 'ImageConversion' is OK.
});
define("BIFY.referencesHealth$ImageToolbox",[], function() {
	// Reference to producer 'ImageToolbox' is OK.
});
define("BIFY.referencesHealth$OutSystemsSampleDataDB",[], function() {
	// Reference to producer 'OutSystemsSampleDataDB' is OK.
});
define("BIFY.referencesHealth$OutSystemsUIWeb",[], function() {
	// Reference to producer 'OutSystemsUIWeb' is OK.
});
define("BIFY.referencesHealth$PlatformPasswordUtils",[], function() {
	// Reference to producer 'PlatformPasswordUtils' is OK.
});
define("BIFY.referencesHealth$ProgressCircle",[], function() {
	// Reference to producer 'ProgressCircle' is OK.
});
define("BIFY.referencesHealth$RestProxy_Lib",[], function() {
	// Reference to producer 'RestProxy_Lib' is OK.
});
define("BIFY.referencesHealth$RichWidgets",[], function() {
	// Reference to producer 'RichWidgets' is OK.
});
define("BIFY.referencesHealth$ServiceCenter",[], function() {
	// Reference to producer 'ServiceCenter' is OK.
});
define("BIFY.referencesHealth$Sleep",[], function() {
	// Reference to producer 'Sleep' is OK.
});
define("BIFY.referencesHealth$SocialLoginAPI",[], function() {
	// Reference to producer 'SocialLoginAPI' is OK.
});
define("BIFY.referencesHealth$SocialLoginSamples",[], function() {
	// Reference to producer 'SocialLoginSamples' is OK.
});
define("BIFY.referencesHealth$SortRecordList",[], function() {
	// Reference to producer 'SortRecordList' is OK.
});
define("BIFY.referencesHealth$Text",[], function() {
	// Reference to producer 'Text' is OK.
});
define("BIFY.referencesHealth$TomsImageCompress",[], function() {
	// Reference to producer 'TomsImageCompress' is OK.
});
define("BIFY.referencesHealth$URLUtilities",[], function() {
	// Reference to producer 'URLUtilities' is OK.
});
define("BIFY.referencesHealth$Users",[], function() {
	// Reference to producer 'Users' is OK.
});
define("BIFY.referencesHealth$WebPatterns",[], function() {
	// Reference to producer 'WebPatterns' is OK.
});
define("BIFY.referencesHealth",[], function() {
});
