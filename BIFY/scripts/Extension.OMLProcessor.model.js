﻿define("Extension.OMLProcessor.model$QueuedNativeBuildResultRec", ["exports", "OutSystems/ClientRuntime/Main", "Extension.OMLProcessor.model"], function (exports, OutSystems, Extension_OMLProcessorModel) {
var OS = OutSystems.Internal;
var QueuedNativeBuildResultRec = (function (_super) {
__extends(QueuedNativeBuildResultRec, _super);
function QueuedNativeBuildResultRec(defaults) {
_super.apply(this, arguments);
}
QueuedNativeBuildResultRec.attributesToDeclare = function () {
return [
this.attr("Success", "successAttr", "Success", true, false, OS.Types.Boolean, function () {
return false;
}, true), 
this.attr("MobilePlatformId", "mobilePlatformIdAttr", "MobilePlatformId", true, false, OS.Types.Integer, function () {
return 0;
}, true)
].concat(_super.attributesToDeclare.call(this));
};
QueuedNativeBuildResultRec.init();
return QueuedNativeBuildResultRec;
})(OS.DataTypes.GenericRecord);
Extension_OMLProcessorModel.QueuedNativeBuildResultRec = QueuedNativeBuildResultRec;

});
define("Extension.OMLProcessor.model$DecimalRec", ["exports", "OutSystems/ClientRuntime/Main", "Extension.OMLProcessor.model"], function (exports, OutSystems, Extension_OMLProcessorModel) {
var OS = OutSystems.Internal;
var DecimalRec = (function (_super) {
__extends(DecimalRec, _super);
function DecimalRec(defaults) {
_super.apply(this, arguments);
}
DecimalRec.attributesToDeclare = function () {
return [
this.attr("Decimal", "decimalAttr", "Decimal", false, false, OS.Types.Decimal, function () {
return OS.DataTypes.Decimal.defaultValue;
}, true)
].concat(_super.attributesToDeclare.call(this));
};
DecimalRec.fromStructure = function (str) {
return new DecimalRec(new DecimalRec.RecordClass({
decimalAttr: OS.DataTypes.ImmutableBase.getData(str)
}));
};
DecimalRec.init();
return DecimalRec;
})(OS.DataTypes.GenericRecord);
Extension_OMLProcessorModel.DecimalRec = DecimalRec;

});
define("Extension.OMLProcessor.model$DecimalRecord", ["exports", "OutSystems/ClientRuntime/Main", "Extension.OMLProcessor.model", "Extension.OMLProcessor.model$DecimalRec"], function (exports, OutSystems, Extension_OMLProcessorModel) {
var OS = OutSystems.Internal;
var DecimalRecord = (function (_super) {
__extends(DecimalRecord, _super);
function DecimalRecord(defaults) {
_super.apply(this, arguments);
}
DecimalRecord.attributesToDeclare = function () {
return [
this.attr("Decimal", "decimalAttr", "Decimal", false, false, OS.Types.Record, function () {
return OS.DataTypes.ImmutableBase.getData(new Extension_OMLProcessorModel.DecimalRec());
}, true, Extension_OMLProcessorModel.DecimalRec)
].concat(_super.attributesToDeclare.call(this));
};
DecimalRecord.fromStructure = function (str) {
return new DecimalRecord(new DecimalRecord.RecordClass({
decimalAttr: OS.DataTypes.ImmutableBase.getData(str)
}));
};
DecimalRecord._isAnonymousRecord = true;
DecimalRecord.UniqueId = "00739b0c-8c9b-e2ce-92d2-52c631f6442e";
DecimalRecord.init();
return DecimalRecord;
})(OS.DataTypes.GenericRecord);
Extension_OMLProcessorModel.DecimalRecord = DecimalRecord;

});
define("Extension.OMLProcessor.model$IntegerRec", ["exports", "OutSystems/ClientRuntime/Main", "Extension.OMLProcessor.model"], function (exports, OutSystems, Extension_OMLProcessorModel) {
var OS = OutSystems.Internal;
var IntegerRec = (function (_super) {
__extends(IntegerRec, _super);
function IntegerRec(defaults) {
_super.apply(this, arguments);
}
IntegerRec.attributesToDeclare = function () {
return [
this.attr("Integer", "integerAttr", "Integer", false, false, OS.Types.Integer, function () {
return 0;
}, true)
].concat(_super.attributesToDeclare.call(this));
};
IntegerRec.fromStructure = function (str) {
return new IntegerRec(new IntegerRec.RecordClass({
integerAttr: OS.DataTypes.ImmutableBase.getData(str)
}));
};
IntegerRec.init();
return IntegerRec;
})(OS.DataTypes.GenericRecord);
Extension_OMLProcessorModel.IntegerRec = IntegerRec;

});
define("Extension.OMLProcessor.model$IntegerRecord", ["exports", "OutSystems/ClientRuntime/Main", "Extension.OMLProcessor.model", "Extension.OMLProcessor.model$IntegerRec"], function (exports, OutSystems, Extension_OMLProcessorModel) {
var OS = OutSystems.Internal;
var IntegerRecord = (function (_super) {
__extends(IntegerRecord, _super);
function IntegerRecord(defaults) {
_super.apply(this, arguments);
}
IntegerRecord.attributesToDeclare = function () {
return [
this.attr("Integer", "integerAttr", "Integer", false, false, OS.Types.Record, function () {
return OS.DataTypes.ImmutableBase.getData(new Extension_OMLProcessorModel.IntegerRec());
}, true, Extension_OMLProcessorModel.IntegerRec)
].concat(_super.attributesToDeclare.call(this));
};
IntegerRecord.fromStructure = function (str) {
return new IntegerRecord(new IntegerRecord.RecordClass({
integerAttr: OS.DataTypes.ImmutableBase.getData(str)
}));
};
IntegerRecord._isAnonymousRecord = true;
IntegerRecord.UniqueId = "00e721f2-8a4e-c706-2026-7d37cbb82717";
IntegerRecord.init();
return IntegerRecord;
})(OS.DataTypes.GenericRecord);
Extension_OMLProcessorModel.IntegerRecord = IntegerRecord;

});
define("Extension.OMLProcessor.model$LinkRec", ["exports", "OutSystems/ClientRuntime/Main", "Extension.OMLProcessor.model"], function (exports, OutSystems, Extension_OMLProcessorModel) {
var OS = OutSystems.Internal;
var LinkRec = (function (_super) {
__extends(LinkRec, _super);
function LinkRec(defaults) {
_super.apply(this, arguments);
}
LinkRec.attributesToDeclare = function () {
return [
this.attr("LogicalName", "logicalNameAttr", "LogicalName", true, false, OS.Types.Text, function () {
return "";
}, true), 
this.attr("PhysicalName", "physicalNameAttr", "PhysicalName", true, false, OS.Types.Text, function () {
return "";
}, true)
].concat(_super.attributesToDeclare.call(this));
};
LinkRec.init();
return LinkRec;
})(OS.DataTypes.GenericRecord);
Extension_OMLProcessorModel.LinkRec = LinkRec;

});
define("Extension.OMLProcessor.model$LinkRecord", ["exports", "OutSystems/ClientRuntime/Main", "Extension.OMLProcessor.model", "Extension.OMLProcessor.model$LinkRec"], function (exports, OutSystems, Extension_OMLProcessorModel) {
var OS = OutSystems.Internal;
var LinkRecord = (function (_super) {
__extends(LinkRecord, _super);
function LinkRecord(defaults) {
_super.apply(this, arguments);
}
LinkRecord.attributesToDeclare = function () {
return [
this.attr("Link", "linkAttr", "Link", false, false, OS.Types.Record, function () {
return OS.DataTypes.ImmutableBase.getData(new Extension_OMLProcessorModel.LinkRec());
}, true, Extension_OMLProcessorModel.LinkRec)
].concat(_super.attributesToDeclare.call(this));
};
LinkRecord.fromStructure = function (str) {
return new LinkRecord(new LinkRecord.RecordClass({
linkAttr: OS.DataTypes.ImmutableBase.getData(str)
}));
};
LinkRecord._isAnonymousRecord = true;
LinkRecord.UniqueId = "01336b23-deb8-91aa-d10f-3c21e3bae982";
LinkRecord.init();
return LinkRecord;
})(OS.DataTypes.GenericRecord);
Extension_OMLProcessorModel.LinkRecord = LinkRecord;

});
define("Extension.OMLProcessor.model$EntityInfoRec", ["exports", "OutSystems/ClientRuntime/Main", "Extension.OMLProcessor.model"], function (exports, OutSystems, Extension_OMLProcessorModel) {
var OS = OutSystems.Internal;
var EntityInfoRec = (function (_super) {
__extends(EntityInfoRec, _super);
function EntityInfoRec(defaults) {
_super.apply(this, arguments);
}
EntityInfoRec.attributesToDeclare = function () {
return [
this.attr("Name", "nameAttr", "Name", false, false, OS.Types.Text, function () {
return "";
}, true), 
this.attr("ReferenceKey", "referenceKeyAttr", "ReferenceKey", false, false, OS.Types.Text, function () {
return "";
}, true), 
this.attr("Key", "keyAttr", "Key", false, false, OS.Types.Text, function () {
return "";
}, true), 
this.attr("ReadOnly", "readOnlyAttr", "ReadOnly", false, false, OS.Types.Boolean, function () {
return false;
}, true), 
this.attr("ShowTenantId", "showTenantIdAttr", "ShowTenantId", false, false, OS.Types.Boolean, function () {
return false;
}, true), 
this.attr("status", "statusAttr", "status", false, false, OS.Types.Text, function () {
return "";
}, true)
].concat(_super.attributesToDeclare.call(this));
};
EntityInfoRec.init();
return EntityInfoRec;
})(OS.DataTypes.GenericRecord);
Extension_OMLProcessorModel.EntityInfoRec = EntityInfoRec;

});
define("Extension.OMLProcessor.model$EntityInfoRecord", ["exports", "OutSystems/ClientRuntime/Main", "Extension.OMLProcessor.model", "Extension.OMLProcessor.model$EntityInfoRec"], function (exports, OutSystems, Extension_OMLProcessorModel) {
var OS = OutSystems.Internal;
var EntityInfoRecord = (function (_super) {
__extends(EntityInfoRecord, _super);
function EntityInfoRecord(defaults) {
_super.apply(this, arguments);
}
EntityInfoRecord.attributesToDeclare = function () {
return [
this.attr("EntityInfo", "entityInfoAttr", "EntityInfo", false, false, OS.Types.Record, function () {
return OS.DataTypes.ImmutableBase.getData(new Extension_OMLProcessorModel.EntityInfoRec());
}, true, Extension_OMLProcessorModel.EntityInfoRec)
].concat(_super.attributesToDeclare.call(this));
};
EntityInfoRecord.fromStructure = function (str) {
return new EntityInfoRecord(new EntityInfoRecord.RecordClass({
entityInfoAttr: OS.DataTypes.ImmutableBase.getData(str)
}));
};
EntityInfoRecord._isAnonymousRecord = true;
EntityInfoRecord.UniqueId = "01b35ef1-0955-228a-acd8-783a476f059e";
EntityInfoRecord.init();
return EntityInfoRecord;
})(OS.DataTypes.GenericRecord);
Extension_OMLProcessorModel.EntityInfoRecord = EntityInfoRecord;

});
define("Extension.OMLProcessor.model$ActionInfoRec", ["exports", "OutSystems/ClientRuntime/Main", "Extension.OMLProcessor.model"], function (exports, OutSystems, Extension_OMLProcessorModel) {
var OS = OutSystems.Internal;
var ActionInfoRec = (function (_super) {
__extends(ActionInfoRec, _super);
function ActionInfoRec(defaults) {
_super.apply(this, arguments);
}
ActionInfoRec.attributesToDeclare = function () {
return [
this.attr("Name", "nameAttr", "Name", false, false, OS.Types.Text, function () {
return "";
}, true), 
this.attr("Key", "keyAttr", "Key", false, false, OS.Types.Text, function () {
return "";
}, true), 
this.attr("Description", "descriptionAttr", "Description", false, false, OS.Types.Text, function () {
return "";
}, true)
].concat(_super.attributesToDeclare.call(this));
};
ActionInfoRec.init();
return ActionInfoRec;
})(OS.DataTypes.GenericRecord);
Extension_OMLProcessorModel.ActionInfoRec = ActionInfoRec;

});
define("Extension.OMLProcessor.model$ActionInfoRecord", ["exports", "OutSystems/ClientRuntime/Main", "Extension.OMLProcessor.model", "Extension.OMLProcessor.model$ActionInfoRec"], function (exports, OutSystems, Extension_OMLProcessorModel) {
var OS = OutSystems.Internal;
var ActionInfoRecord = (function (_super) {
__extends(ActionInfoRecord, _super);
function ActionInfoRecord(defaults) {
_super.apply(this, arguments);
}
ActionInfoRecord.attributesToDeclare = function () {
return [
this.attr("ActionInfo", "actionInfoAttr", "ActionInfo", false, false, OS.Types.Record, function () {
return OS.DataTypes.ImmutableBase.getData(new Extension_OMLProcessorModel.ActionInfoRec());
}, true, Extension_OMLProcessorModel.ActionInfoRec)
].concat(_super.attributesToDeclare.call(this));
};
ActionInfoRecord.fromStructure = function (str) {
return new ActionInfoRecord(new ActionInfoRecord.RecordClass({
actionInfoAttr: OS.DataTypes.ImmutableBase.getData(str)
}));
};
ActionInfoRecord._isAnonymousRecord = true;
ActionInfoRecord.UniqueId = "05b264be-b7ca-4d47-f77f-006bc1efc989";
ActionInfoRecord.init();
return ActionInfoRecord;
})(OS.DataTypes.GenericRecord);
Extension_OMLProcessorModel.ActionInfoRecord = ActionInfoRecord;

});
define("Extension.OMLProcessor.model$StructureInfoRec", ["exports", "OutSystems/ClientRuntime/Main", "Extension.OMLProcessor.model"], function (exports, OutSystems, Extension_OMLProcessorModel) {
var OS = OutSystems.Internal;
var StructureInfoRec = (function (_super) {
__extends(StructureInfoRec, _super);
function StructureInfoRec(defaults) {
_super.apply(this, arguments);
}
StructureInfoRec.attributesToDeclare = function () {
return [
this.attr("Name", "nameAttr", "Name", false, false, OS.Types.Text, function () {
return "";
}, true), 
this.attr("Key", "keyAttr", "Key", false, false, OS.Types.Text, function () {
return "";
}, true), 
this.attr("Description", "descriptionAttr", "Description", false, false, OS.Types.Text, function () {
return "";
}, true)
].concat(_super.attributesToDeclare.call(this));
};
StructureInfoRec.init();
return StructureInfoRec;
})(OS.DataTypes.GenericRecord);
Extension_OMLProcessorModel.StructureInfoRec = StructureInfoRec;

});
define("Extension.OMLProcessor.model$StructureInfoRecord", ["exports", "OutSystems/ClientRuntime/Main", "Extension.OMLProcessor.model", "Extension.OMLProcessor.model$StructureInfoRec"], function (exports, OutSystems, Extension_OMLProcessorModel) {
var OS = OutSystems.Internal;
var StructureInfoRecord = (function (_super) {
__extends(StructureInfoRecord, _super);
function StructureInfoRecord(defaults) {
_super.apply(this, arguments);
}
StructureInfoRecord.attributesToDeclare = function () {
return [
this.attr("StructureInfo", "structureInfoAttr", "StructureInfo", false, false, OS.Types.Record, function () {
return OS.DataTypes.ImmutableBase.getData(new Extension_OMLProcessorModel.StructureInfoRec());
}, true, Extension_OMLProcessorModel.StructureInfoRec)
].concat(_super.attributesToDeclare.call(this));
};
StructureInfoRecord.fromStructure = function (str) {
return new StructureInfoRecord(new StructureInfoRecord.RecordClass({
structureInfoAttr: OS.DataTypes.ImmutableBase.getData(str)
}));
};
StructureInfoRecord._isAnonymousRecord = true;
StructureInfoRecord.UniqueId = "ed7407f7-361d-d18b-5fe8-c1493e2b64a3";
StructureInfoRecord.init();
return StructureInfoRecord;
})(OS.DataTypes.GenericRecord);
Extension_OMLProcessorModel.StructureInfoRecord = StructureInfoRecord;

});
define("Extension.OMLProcessor.model$StructureInfoRecordList", ["exports", "OutSystems/ClientRuntime/Main", "Extension.OMLProcessor.model", "Extension.OMLProcessor.model$StructureInfoRecord"], function (exports, OutSystems, Extension_OMLProcessorModel) {
var OS = OutSystems.Internal;
var StructureInfoRecordList = (function (_super) {
__extends(StructureInfoRecordList, _super);
function StructureInfoRecordList(defaults) {
_super.apply(this, arguments);
}
StructureInfoRecordList.itemType = Extension_OMLProcessorModel.StructureInfoRecord;
return StructureInfoRecordList;
})(OS.DataTypes.GenericRecordList);
Extension_OMLProcessorModel.StructureInfoRecordList = StructureInfoRecordList;

});
define("Extension.OMLProcessor.model$SolutionPack_SolutionLogicalDatabaseRec", ["exports", "OutSystems/ClientRuntime/Main", "Extension.OMLProcessor.model"], function (exports, OutSystems, Extension_OMLProcessorModel) {
var OS = OutSystems.Internal;
var SolutionPack_SolutionLogicalDatabaseRec = (function (_super) {
__extends(SolutionPack_SolutionLogicalDatabaseRec, _super);
function SolutionPack_SolutionLogicalDatabaseRec(defaults) {
_super.apply(this, arguments);
}
SolutionPack_SolutionLogicalDatabaseRec.attributesToDeclare = function () {
return [
this.attr("LogicalDatabase", "logicalDatabaseAttr", "LogicalDatabase", true, false, OS.Types.Text, function () {
return "";
}, true)
].concat(_super.attributesToDeclare.call(this));
};
SolutionPack_SolutionLogicalDatabaseRec.fromStructure = function (str) {
return new SolutionPack_SolutionLogicalDatabaseRec(new SolutionPack_SolutionLogicalDatabaseRec.RecordClass({
logicalDatabaseAttr: OS.DataTypes.ImmutableBase.getData(str)
}));
};
SolutionPack_SolutionLogicalDatabaseRec.init();
return SolutionPack_SolutionLogicalDatabaseRec;
})(OS.DataTypes.GenericRecord);
Extension_OMLProcessorModel.SolutionPack_SolutionLogicalDatabaseRec = SolutionPack_SolutionLogicalDatabaseRec;

});
define("Extension.OMLProcessor.model$SolutionPack_SolutionLogicalDatabaseRecord", ["exports", "OutSystems/ClientRuntime/Main", "Extension.OMLProcessor.model", "Extension.OMLProcessor.model$SolutionPack_SolutionLogicalDatabaseRec"], function (exports, OutSystems, Extension_OMLProcessorModel) {
var OS = OutSystems.Internal;
var SolutionPack_SolutionLogicalDatabaseRecord = (function (_super) {
__extends(SolutionPack_SolutionLogicalDatabaseRecord, _super);
function SolutionPack_SolutionLogicalDatabaseRecord(defaults) {
_super.apply(this, arguments);
}
SolutionPack_SolutionLogicalDatabaseRecord.attributesToDeclare = function () {
return [
this.attr("SolutionPack_SolutionLogicalDatabase", "solutionPack_SolutionLogicalDatabaseAttr", "SolutionPack_SolutionLogicalDatabase", false, false, OS.Types.Record, function () {
return OS.DataTypes.ImmutableBase.getData(new Extension_OMLProcessorModel.SolutionPack_SolutionLogicalDatabaseRec());
}, true, Extension_OMLProcessorModel.SolutionPack_SolutionLogicalDatabaseRec)
].concat(_super.attributesToDeclare.call(this));
};
SolutionPack_SolutionLogicalDatabaseRecord.fromStructure = function (str) {
return new SolutionPack_SolutionLogicalDatabaseRecord(new SolutionPack_SolutionLogicalDatabaseRecord.RecordClass({
solutionPack_SolutionLogicalDatabaseAttr: OS.DataTypes.ImmutableBase.getData(str)
}));
};
SolutionPack_SolutionLogicalDatabaseRecord._isAnonymousRecord = true;
SolutionPack_SolutionLogicalDatabaseRecord.UniqueId = "6988ce96-e1fb-0e29-d135-8f34f57ab8d9";
SolutionPack_SolutionLogicalDatabaseRecord.init();
return SolutionPack_SolutionLogicalDatabaseRecord;
})(OS.DataTypes.GenericRecord);
Extension_OMLProcessorModel.SolutionPack_SolutionLogicalDatabaseRecord = SolutionPack_SolutionLogicalDatabaseRecord;

});
define("Extension.OMLProcessor.model$SolutionPack_SolutionLogicalDatabaseRecordList", ["exports", "OutSystems/ClientRuntime/Main", "Extension.OMLProcessor.model", "Extension.OMLProcessor.model$SolutionPack_SolutionLogicalDatabaseRecord"], function (exports, OutSystems, Extension_OMLProcessorModel) {
var OS = OutSystems.Internal;
var SolutionPack_SolutionLogicalDatabaseRecordList = (function (_super) {
__extends(SolutionPack_SolutionLogicalDatabaseRecordList, _super);
function SolutionPack_SolutionLogicalDatabaseRecordList(defaults) {
_super.apply(this, arguments);
}
SolutionPack_SolutionLogicalDatabaseRecordList.itemType = Extension_OMLProcessorModel.SolutionPack_SolutionLogicalDatabaseRecord;
return SolutionPack_SolutionLogicalDatabaseRecordList;
})(OS.DataTypes.GenericRecordList);
Extension_OMLProcessorModel.SolutionPack_SolutionLogicalDatabaseRecordList = SolutionPack_SolutionLogicalDatabaseRecordList;

});
define("Extension.OMLProcessor.model$SolutionPack_SolutionDBCatalogRec", ["exports", "OutSystems/ClientRuntime/Main", "Extension.OMLProcessor.model"], function (exports, OutSystems, Extension_OMLProcessorModel) {
var OS = OutSystems.Internal;
var SolutionPack_SolutionDBCatalogRec = (function (_super) {
__extends(SolutionPack_SolutionDBCatalogRec, _super);
function SolutionPack_SolutionDBCatalogRec(defaults) {
_super.apply(this, arguments);
}
SolutionPack_SolutionDBCatalogRec.attributesToDeclare = function () {
return [
this.attr("DbCatalog", "dbCatalogAttr", "DbCatalog", true, false, OS.Types.Text, function () {
return "";
}, true)
].concat(_super.attributesToDeclare.call(this));
};
SolutionPack_SolutionDBCatalogRec.fromStructure = function (str) {
return new SolutionPack_SolutionDBCatalogRec(new SolutionPack_SolutionDBCatalogRec.RecordClass({
dbCatalogAttr: OS.DataTypes.ImmutableBase.getData(str)
}));
};
SolutionPack_SolutionDBCatalogRec.init();
return SolutionPack_SolutionDBCatalogRec;
})(OS.DataTypes.GenericRecord);
Extension_OMLProcessorModel.SolutionPack_SolutionDBCatalogRec = SolutionPack_SolutionDBCatalogRec;

});
define("Extension.OMLProcessor.model$SolutionPack_SolutionDBCatalogRecord", ["exports", "OutSystems/ClientRuntime/Main", "Extension.OMLProcessor.model", "Extension.OMLProcessor.model$SolutionPack_SolutionDBCatalogRec"], function (exports, OutSystems, Extension_OMLProcessorModel) {
var OS = OutSystems.Internal;
var SolutionPack_SolutionDBCatalogRecord = (function (_super) {
__extends(SolutionPack_SolutionDBCatalogRecord, _super);
function SolutionPack_SolutionDBCatalogRecord(defaults) {
_super.apply(this, arguments);
}
SolutionPack_SolutionDBCatalogRecord.attributesToDeclare = function () {
return [
this.attr("SolutionPack_SolutionDBCatalog", "solutionPack_SolutionDBCatalogAttr", "SolutionPack_SolutionDBCatalog", false, false, OS.Types.Record, function () {
return OS.DataTypes.ImmutableBase.getData(new Extension_OMLProcessorModel.SolutionPack_SolutionDBCatalogRec());
}, true, Extension_OMLProcessorModel.SolutionPack_SolutionDBCatalogRec)
].concat(_super.attributesToDeclare.call(this));
};
SolutionPack_SolutionDBCatalogRecord.fromStructure = function (str) {
return new SolutionPack_SolutionDBCatalogRecord(new SolutionPack_SolutionDBCatalogRecord.RecordClass({
solutionPack_SolutionDBCatalogAttr: OS.DataTypes.ImmutableBase.getData(str)
}));
};
SolutionPack_SolutionDBCatalogRecord._isAnonymousRecord = true;
SolutionPack_SolutionDBCatalogRecord.UniqueId = "bb049b61-83cc-257b-8f55-4c07762480ff";
SolutionPack_SolutionDBCatalogRecord.init();
return SolutionPack_SolutionDBCatalogRecord;
})(OS.DataTypes.GenericRecord);
Extension_OMLProcessorModel.SolutionPack_SolutionDBCatalogRecord = SolutionPack_SolutionDBCatalogRecord;

});
define("Extension.OMLProcessor.model$SolutionPack_SolutionReferenceRec", ["exports", "OutSystems/ClientRuntime/Main", "Extension.OMLProcessor.model", "Extension.OMLProcessor.model$SolutionPack_SolutionLogicalDatabaseRecordList", "Extension.OMLProcessor.model$SolutionPack_SolutionDBCatalogRecord"], function (exports, OutSystems, Extension_OMLProcessorModel) {
var OS = OutSystems.Internal;
var SolutionPack_SolutionReferenceRec = (function (_super) {
__extends(SolutionPack_SolutionReferenceRec, _super);
function SolutionPack_SolutionReferenceRec(defaults) {
_super.apply(this, arguments);
}
SolutionPack_SolutionReferenceRec.attributesToDeclare = function () {
return [
this.attr("UID", "uIDAttr", "UID", true, false, OS.Types.Text, function () {
return "";
}, true), 
this.attr("Kind", "kindAttr", "Kind", true, false, OS.Types.Text, function () {
return "";
}, true), 
this.attr("Name", "nameAttr", "Name", true, false, OS.Types.Text, function () {
return "";
}, true), 
this.attr("LogicalDatabases", "logicalDatabasesAttr", "LogicalDatabases", true, false, OS.Types.RecordList, function () {
return OS.DataTypes.ImmutableBase.getData(new Extension_OMLProcessorModel.SolutionPack_SolutionLogicalDatabaseRecordList());
}, true, Extension_OMLProcessorModel.SolutionPack_SolutionLogicalDatabaseRecordList), 
this.attr("DBCatalog", "dBCatalogAttr", "DBCatalog", true, false, OS.Types.Record, function () {
return OS.DataTypes.ImmutableBase.getData(new Extension_OMLProcessorModel.SolutionPack_SolutionDBCatalogRecord());
}, true, Extension_OMLProcessorModel.SolutionPack_SolutionDBCatalogRecord), 
this.attr("Hash", "hashAttr", "Hash", false, false, OS.Types.Text, function () {
return "";
}, true), 
this.attr("IsComponent", "isComponentAttr", "IsComponent", false, false, OS.Types.Boolean, function () {
return false;
}, true), 
this.attr("CreatedBy", "createdByAttr", "CreatedBy", false, false, OS.Types.Text, function () {
return "";
}, true), 
this.attr("CreatedOn", "createdOnAttr", "CreatedOn", false, false, OS.Types.DateTime, function () {
return OS.DataTypes.DateTime.defaultValue;
}, true), 
this.attr("Description", "descriptionAttr", "Description", false, false, OS.Types.Text, function () {
return "";
}, true)
].concat(_super.attributesToDeclare.call(this));
};
SolutionPack_SolutionReferenceRec.init();
return SolutionPack_SolutionReferenceRec;
})(OS.DataTypes.GenericRecord);
Extension_OMLProcessorModel.SolutionPack_SolutionReferenceRec = SolutionPack_SolutionReferenceRec;

});
define("Extension.OMLProcessor.model$SolutionPack_SolutionReferenceRecord", ["exports", "OutSystems/ClientRuntime/Main", "Extension.OMLProcessor.model", "Extension.OMLProcessor.model$SolutionPack_SolutionReferenceRec"], function (exports, OutSystems, Extension_OMLProcessorModel) {
var OS = OutSystems.Internal;
var SolutionPack_SolutionReferenceRecord = (function (_super) {
__extends(SolutionPack_SolutionReferenceRecord, _super);
function SolutionPack_SolutionReferenceRecord(defaults) {
_super.apply(this, arguments);
}
SolutionPack_SolutionReferenceRecord.attributesToDeclare = function () {
return [
this.attr("SolutionPack_SolutionReference", "solutionPack_SolutionReferenceAttr", "SolutionPack_SolutionReference", false, false, OS.Types.Record, function () {
return OS.DataTypes.ImmutableBase.getData(new Extension_OMLProcessorModel.SolutionPack_SolutionReferenceRec());
}, true, Extension_OMLProcessorModel.SolutionPack_SolutionReferenceRec)
].concat(_super.attributesToDeclare.call(this));
};
SolutionPack_SolutionReferenceRecord.fromStructure = function (str) {
return new SolutionPack_SolutionReferenceRecord(new SolutionPack_SolutionReferenceRecord.RecordClass({
solutionPack_SolutionReferenceAttr: OS.DataTypes.ImmutableBase.getData(str)
}));
};
SolutionPack_SolutionReferenceRecord._isAnonymousRecord = true;
SolutionPack_SolutionReferenceRecord.UniqueId = "2d0540e1-542d-26db-8230-358425505ada";
SolutionPack_SolutionReferenceRecord.init();
return SolutionPack_SolutionReferenceRecord;
})(OS.DataTypes.GenericRecord);
Extension_OMLProcessorModel.SolutionPack_SolutionReferenceRecord = SolutionPack_SolutionReferenceRecord;

});
define("Extension.OMLProcessor.model$SolutionPack_SolutionReferenceRecordList", ["exports", "OutSystems/ClientRuntime/Main", "Extension.OMLProcessor.model", "Extension.OMLProcessor.model$SolutionPack_SolutionReferenceRecord"], function (exports, OutSystems, Extension_OMLProcessorModel) {
var OS = OutSystems.Internal;
var SolutionPack_SolutionReferenceRecordList = (function (_super) {
__extends(SolutionPack_SolutionReferenceRecordList, _super);
function SolutionPack_SolutionReferenceRecordList(defaults) {
_super.apply(this, arguments);
}
SolutionPack_SolutionReferenceRecordList.itemType = Extension_OMLProcessorModel.SolutionPack_SolutionReferenceRecord;
return SolutionPack_SolutionReferenceRecordList;
})(OS.DataTypes.GenericRecordList);
Extension_OMLProcessorModel.SolutionPack_SolutionReferenceRecordList = SolutionPack_SolutionReferenceRecordList;

});
define("Extension.OMLProcessor.model$SolutionPack_ApplicationModulePublicElementRec", ["exports", "OutSystems/ClientRuntime/Main", "Extension.OMLProcessor.model"], function (exports, OutSystems, Extension_OMLProcessorModel) {
var OS = OutSystems.Internal;
var SolutionPack_ApplicationModulePublicElementRec = (function (_super) {
__extends(SolutionPack_ApplicationModulePublicElementRec, _super);
function SolutionPack_ApplicationModulePublicElementRec(defaults) {
_super.apply(this, arguments);
}
SolutionPack_ApplicationModulePublicElementRec.attributesToDeclare = function () {
return [
this.attr("Name", "nameAttr", "Name", true, false, OS.Types.Text, function () {
return "";
}, true), 
this.attr("Kind", "kindAttr", "Kind", true, false, OS.Types.Text, function () {
return "";
}, true), 
this.attr("Key", "keyAttr", "Key", true, false, OS.Types.Text, function () {
return "";
}, true), 
this.attr("CompatibilitySignatureHash", "compatibilitySignatureHashAttr", "CompatibilitySignatureHash", true, false, OS.Types.Text, function () {
return "";
}, true), 
this.attr("FullSignatureHash", "fullSignatureHashAttr", "FullSignatureHash", true, false, OS.Types.Text, function () {
return "";
}, true)
].concat(_super.attributesToDeclare.call(this));
};
SolutionPack_ApplicationModulePublicElementRec.init();
return SolutionPack_ApplicationModulePublicElementRec;
})(OS.DataTypes.GenericRecord);
Extension_OMLProcessorModel.SolutionPack_ApplicationModulePublicElementRec = SolutionPack_ApplicationModulePublicElementRec;

});
define("Extension.OMLProcessor.model$SolutionPack_ApplicationModulePublicElementRecord", ["exports", "OutSystems/ClientRuntime/Main", "Extension.OMLProcessor.model", "Extension.OMLProcessor.model$SolutionPack_ApplicationModulePublicElementRec"], function (exports, OutSystems, Extension_OMLProcessorModel) {
var OS = OutSystems.Internal;
var SolutionPack_ApplicationModulePublicElementRecord = (function (_super) {
__extends(SolutionPack_ApplicationModulePublicElementRecord, _super);
function SolutionPack_ApplicationModulePublicElementRecord(defaults) {
_super.apply(this, arguments);
}
SolutionPack_ApplicationModulePublicElementRecord.attributesToDeclare = function () {
return [
this.attr("SolutionPack_ApplicationModulePublicElement", "solutionPack_ApplicationModulePublicElementAttr", "SolutionPack_ApplicationModulePublicElement", false, false, OS.Types.Record, function () {
return OS.DataTypes.ImmutableBase.getData(new Extension_OMLProcessorModel.SolutionPack_ApplicationModulePublicElementRec());
}, true, Extension_OMLProcessorModel.SolutionPack_ApplicationModulePublicElementRec)
].concat(_super.attributesToDeclare.call(this));
};
SolutionPack_ApplicationModulePublicElementRecord.fromStructure = function (str) {
return new SolutionPack_ApplicationModulePublicElementRecord(new SolutionPack_ApplicationModulePublicElementRecord.RecordClass({
solutionPack_ApplicationModulePublicElementAttr: OS.DataTypes.ImmutableBase.getData(str)
}));
};
SolutionPack_ApplicationModulePublicElementRecord._isAnonymousRecord = true;
SolutionPack_ApplicationModulePublicElementRecord.UniqueId = "e70d8c3a-c727-015b-a90d-dccd53c33e05";
SolutionPack_ApplicationModulePublicElementRecord.init();
return SolutionPack_ApplicationModulePublicElementRecord;
})(OS.DataTypes.GenericRecord);
Extension_OMLProcessorModel.SolutionPack_ApplicationModulePublicElementRecord = SolutionPack_ApplicationModulePublicElementRecord;

});
define("Extension.OMLProcessor.model$SolutionPack_ApplicationModulePublicElementRecordList", ["exports", "OutSystems/ClientRuntime/Main", "Extension.OMLProcessor.model", "Extension.OMLProcessor.model$SolutionPack_ApplicationModulePublicElementRecord"], function (exports, OutSystems, Extension_OMLProcessorModel) {
var OS = OutSystems.Internal;
var SolutionPack_ApplicationModulePublicElementRecordList = (function (_super) {
__extends(SolutionPack_ApplicationModulePublicElementRecordList, _super);
function SolutionPack_ApplicationModulePublicElementRecordList(defaults) {
_super.apply(this, arguments);
}
SolutionPack_ApplicationModulePublicElementRecordList.itemType = Extension_OMLProcessorModel.SolutionPack_ApplicationModulePublicElementRecord;
return SolutionPack_ApplicationModulePublicElementRecordList;
})(OS.DataTypes.GenericRecordList);
Extension_OMLProcessorModel.SolutionPack_ApplicationModulePublicElementRecordList = SolutionPack_ApplicationModulePublicElementRecordList;

});
define("Extension.OMLProcessor.model$SolutionPack_ApplicationModuleRec", ["exports", "OutSystems/ClientRuntime/Main", "Extension.OMLProcessor.model", "Extension.OMLProcessor.model$SolutionPack_ApplicationModulePublicElementRecordList"], function (exports, OutSystems, Extension_OMLProcessorModel) {
var OS = OutSystems.Internal;
var SolutionPack_ApplicationModuleRec = (function (_super) {
__extends(SolutionPack_ApplicationModuleRec, _super);
function SolutionPack_ApplicationModuleRec(defaults) {
_super.apply(this, arguments);
}
SolutionPack_ApplicationModuleRec.attributesToDeclare = function () {
return [
this.attr("UID", "uIDAttr", "UID", true, false, OS.Types.Text, function () {
return "";
}, true), 
this.attr("Kind", "kindAttr", "Kind", true, false, OS.Types.Text, function () {
return "";
}, true), 
this.attr("Signature", "signatureAttr", "Signature", true, false, OS.Types.BinaryData, function () {
return OS.DataTypes.BinaryData.defaultValue;
}, true), 
this.attr("API", "aPIAttr", "API", true, false, OS.Types.RecordList, function () {
return OS.DataTypes.ImmutableBase.getData(new Extension_OMLProcessorModel.SolutionPack_ApplicationModulePublicElementRecordList());
}, true, Extension_OMLProcessorModel.SolutionPack_ApplicationModulePublicElementRecordList), 
this.attr("Name", "nameAttr", "Name", true, false, OS.Types.Text, function () {
return "";
}, true), 
this.attr("ESpaceType", "eSpaceTypeAttr", "ESpaceType", true, false, OS.Types.Text, function () {
return "";
}, true)
].concat(_super.attributesToDeclare.call(this));
};
SolutionPack_ApplicationModuleRec.init();
return SolutionPack_ApplicationModuleRec;
})(OS.DataTypes.GenericRecord);
Extension_OMLProcessorModel.SolutionPack_ApplicationModuleRec = SolutionPack_ApplicationModuleRec;

});
define("Extension.OMLProcessor.model$SolutionPack_ApplicationModuleRecord", ["exports", "OutSystems/ClientRuntime/Main", "Extension.OMLProcessor.model", "Extension.OMLProcessor.model$SolutionPack_ApplicationModuleRec"], function (exports, OutSystems, Extension_OMLProcessorModel) {
var OS = OutSystems.Internal;
var SolutionPack_ApplicationModuleRecord = (function (_super) {
__extends(SolutionPack_ApplicationModuleRecord, _super);
function SolutionPack_ApplicationModuleRecord(defaults) {
_super.apply(this, arguments);
}
SolutionPack_ApplicationModuleRecord.attributesToDeclare = function () {
return [
this.attr("SolutionPack_ApplicationModule", "solutionPack_ApplicationModuleAttr", "SolutionPack_ApplicationModule", false, false, OS.Types.Record, function () {
return OS.DataTypes.ImmutableBase.getData(new Extension_OMLProcessorModel.SolutionPack_ApplicationModuleRec());
}, true, Extension_OMLProcessorModel.SolutionPack_ApplicationModuleRec)
].concat(_super.attributesToDeclare.call(this));
};
SolutionPack_ApplicationModuleRecord.fromStructure = function (str) {
return new SolutionPack_ApplicationModuleRecord(new SolutionPack_ApplicationModuleRecord.RecordClass({
solutionPack_ApplicationModuleAttr: OS.DataTypes.ImmutableBase.getData(str)
}));
};
SolutionPack_ApplicationModuleRecord._isAnonymousRecord = true;
SolutionPack_ApplicationModuleRecord.UniqueId = "0d1659cc-3ee8-6b25-b6c0-c1ee89a87389";
SolutionPack_ApplicationModuleRecord.init();
return SolutionPack_ApplicationModuleRecord;
})(OS.DataTypes.GenericRecord);
Extension_OMLProcessorModel.SolutionPack_ApplicationModuleRecord = SolutionPack_ApplicationModuleRecord;

});
define("Extension.OMLProcessor.model$SolutionPack_ApplicationModuleRecordList", ["exports", "OutSystems/ClientRuntime/Main", "Extension.OMLProcessor.model", "Extension.OMLProcessor.model$SolutionPack_ApplicationModuleRecord"], function (exports, OutSystems, Extension_OMLProcessorModel) {
var OS = OutSystems.Internal;
var SolutionPack_ApplicationModuleRecordList = (function (_super) {
__extends(SolutionPack_ApplicationModuleRecordList, _super);
function SolutionPack_ApplicationModuleRecordList(defaults) {
_super.apply(this, arguments);
}
SolutionPack_ApplicationModuleRecordList.itemType = Extension_OMLProcessorModel.SolutionPack_ApplicationModuleRecord;
return SolutionPack_ApplicationModuleRecordList;
})(OS.DataTypes.GenericRecordList);
Extension_OMLProcessorModel.SolutionPack_ApplicationModuleRecordList = SolutionPack_ApplicationModuleRecordList;

});
define("Extension.OMLProcessor.model$SolutionPack_ApplicationReferencedModuleRec", ["exports", "OutSystems/ClientRuntime/Main", "Extension.OMLProcessor.model", "Extension.OMLProcessor.model$SolutionPack_ApplicationModulePublicElementRecordList"], function (exports, OutSystems, Extension_OMLProcessorModel) {
var OS = OutSystems.Internal;
var SolutionPack_ApplicationReferencedModuleRec = (function (_super) {
__extends(SolutionPack_ApplicationReferencedModuleRec, _super);
function SolutionPack_ApplicationReferencedModuleRec(defaults) {
_super.apply(this, arguments);
}
SolutionPack_ApplicationReferencedModuleRec.attributesToDeclare = function () {
return [
this.attr("UID", "uIDAttr", "UID", true, false, OS.Types.Text, function () {
return "";
}, true), 
this.attr("Kind", "kindAttr", "Kind", true, false, OS.Types.Text, function () {
return "";
}, true), 
this.attr("ReferencedAPI", "referencedAPIAttr", "ReferencedAPI", true, false, OS.Types.RecordList, function () {
return OS.DataTypes.ImmutableBase.getData(new Extension_OMLProcessorModel.SolutionPack_ApplicationModulePublicElementRecordList());
}, true, Extension_OMLProcessorModel.SolutionPack_ApplicationModulePublicElementRecordList), 
this.attr("ESpaceType", "eSpaceTypeAttr", "ESpaceType", false, false, OS.Types.Text, function () {
return "";
}, true)
].concat(_super.attributesToDeclare.call(this));
};
SolutionPack_ApplicationReferencedModuleRec.init();
return SolutionPack_ApplicationReferencedModuleRec;
})(OS.DataTypes.GenericRecord);
Extension_OMLProcessorModel.SolutionPack_ApplicationReferencedModuleRec = SolutionPack_ApplicationReferencedModuleRec;

});
define("Extension.OMLProcessor.model$SolutionPack_ApplicationReferencedModuleRecord", ["exports", "OutSystems/ClientRuntime/Main", "Extension.OMLProcessor.model", "Extension.OMLProcessor.model$SolutionPack_ApplicationReferencedModuleRec"], function (exports, OutSystems, Extension_OMLProcessorModel) {
var OS = OutSystems.Internal;
var SolutionPack_ApplicationReferencedModuleRecord = (function (_super) {
__extends(SolutionPack_ApplicationReferencedModuleRecord, _super);
function SolutionPack_ApplicationReferencedModuleRecord(defaults) {
_super.apply(this, arguments);
}
SolutionPack_ApplicationReferencedModuleRecord.attributesToDeclare = function () {
return [
this.attr("SolutionPack_ApplicationReferencedModule", "solutionPack_ApplicationReferencedModuleAttr", "SolutionPack_ApplicationReferencedModule", false, false, OS.Types.Record, function () {
return OS.DataTypes.ImmutableBase.getData(new Extension_OMLProcessorModel.SolutionPack_ApplicationReferencedModuleRec());
}, true, Extension_OMLProcessorModel.SolutionPack_ApplicationReferencedModuleRec)
].concat(_super.attributesToDeclare.call(this));
};
SolutionPack_ApplicationReferencedModuleRecord.fromStructure = function (str) {
return new SolutionPack_ApplicationReferencedModuleRecord(new SolutionPack_ApplicationReferencedModuleRecord.RecordClass({
solutionPack_ApplicationReferencedModuleAttr: OS.DataTypes.ImmutableBase.getData(str)
}));
};
SolutionPack_ApplicationReferencedModuleRecord._isAnonymousRecord = true;
SolutionPack_ApplicationReferencedModuleRecord.UniqueId = "d619ba76-004a-4e47-8fd6-e6f8b314e322";
SolutionPack_ApplicationReferencedModuleRecord.init();
return SolutionPack_ApplicationReferencedModuleRecord;
})(OS.DataTypes.GenericRecord);
Extension_OMLProcessorModel.SolutionPack_ApplicationReferencedModuleRecord = SolutionPack_ApplicationReferencedModuleRecord;

});
define("Extension.OMLProcessor.model$SolutionPack_ApplicationReferencedModuleRecordList", ["exports", "OutSystems/ClientRuntime/Main", "Extension.OMLProcessor.model", "Extension.OMLProcessor.model$SolutionPack_ApplicationReferencedModuleRecord"], function (exports, OutSystems, Extension_OMLProcessorModel) {
var OS = OutSystems.Internal;
var SolutionPack_ApplicationReferencedModuleRecordList = (function (_super) {
__extends(SolutionPack_ApplicationReferencedModuleRecordList, _super);
function SolutionPack_ApplicationReferencedModuleRecordList(defaults) {
_super.apply(this, arguments);
}
SolutionPack_ApplicationReferencedModuleRecordList.itemType = Extension_OMLProcessorModel.SolutionPack_ApplicationReferencedModuleRecord;
return SolutionPack_ApplicationReferencedModuleRecordList;
})(OS.DataTypes.GenericRecordList);
Extension_OMLProcessorModel.SolutionPack_ApplicationReferencedModuleRecordList = SolutionPack_ApplicationReferencedModuleRecordList;

});
define("Extension.OMLProcessor.model$SolutionPack_ApplicationDependencyRec", ["exports", "OutSystems/ClientRuntime/Main", "Extension.OMLProcessor.model", "Extension.OMLProcessor.model$SolutionPack_ApplicationReferencedModuleRecordList"], function (exports, OutSystems, Extension_OMLProcessorModel) {
var OS = OutSystems.Internal;
var SolutionPack_ApplicationDependencyRec = (function (_super) {
__extends(SolutionPack_ApplicationDependencyRec, _super);
function SolutionPack_ApplicationDependencyRec(defaults) {
_super.apply(this, arguments);
}
SolutionPack_ApplicationDependencyRec.attributesToDeclare = function () {
return [
this.attr("Name", "nameAttr", "Name", true, false, OS.Types.Text, function () {
return "";
}, true), 
this.attr("Key", "keyAttr", "Key", true, false, OS.Types.Text, function () {
return "";
}, true), 
this.attr("ReferencedModules", "referencedModulesAttr", "ReferencedModules", true, false, OS.Types.RecordList, function () {
return OS.DataTypes.ImmutableBase.getData(new Extension_OMLProcessorModel.SolutionPack_ApplicationReferencedModuleRecordList());
}, true, Extension_OMLProcessorModel.SolutionPack_ApplicationReferencedModuleRecordList), 
this.attr("ForgeBaseVersion", "forgeBaseVersionAttr", "ForgeBaseVersion", true, false, OS.Types.Decimal, function () {
return OS.DataTypes.Decimal.defaultValue;
}, true), 
this.attr("HasLocalChanges", "hasLocalChangesAttr", "HasLocalChanges", true, false, OS.Types.Boolean, function () {
return false;
}, true), 
this.attr("ContentHash", "contentHashAttr", "ContentHash", true, false, OS.Types.Text, function () {
return "";
}, true), 
this.attr("APIHash", "aPIHashAttr", "APIHash", true, false, OS.Types.Text, function () {
return "";
}, true), 
this.attr("ApplicationKind", "applicationKindAttr", "ApplicationKind", true, false, OS.Types.Text, function () {
return "";
}, true)
].concat(_super.attributesToDeclare.call(this));
};
SolutionPack_ApplicationDependencyRec.init();
return SolutionPack_ApplicationDependencyRec;
})(OS.DataTypes.GenericRecord);
Extension_OMLProcessorModel.SolutionPack_ApplicationDependencyRec = SolutionPack_ApplicationDependencyRec;

});
define("Extension.OMLProcessor.model$SolutionPack_ApplicationDependencyRecord", ["exports", "OutSystems/ClientRuntime/Main", "Extension.OMLProcessor.model", "Extension.OMLProcessor.model$SolutionPack_ApplicationDependencyRec"], function (exports, OutSystems, Extension_OMLProcessorModel) {
var OS = OutSystems.Internal;
var SolutionPack_ApplicationDependencyRecord = (function (_super) {
__extends(SolutionPack_ApplicationDependencyRecord, _super);
function SolutionPack_ApplicationDependencyRecord(defaults) {
_super.apply(this, arguments);
}
SolutionPack_ApplicationDependencyRecord.attributesToDeclare = function () {
return [
this.attr("SolutionPack_ApplicationDependency", "solutionPack_ApplicationDependencyAttr", "SolutionPack_ApplicationDependency", false, false, OS.Types.Record, function () {
return OS.DataTypes.ImmutableBase.getData(new Extension_OMLProcessorModel.SolutionPack_ApplicationDependencyRec());
}, true, Extension_OMLProcessorModel.SolutionPack_ApplicationDependencyRec)
].concat(_super.attributesToDeclare.call(this));
};
SolutionPack_ApplicationDependencyRecord.fromStructure = function (str) {
return new SolutionPack_ApplicationDependencyRecord(new SolutionPack_ApplicationDependencyRecord.RecordClass({
solutionPack_ApplicationDependencyAttr: OS.DataTypes.ImmutableBase.getData(str)
}));
};
SolutionPack_ApplicationDependencyRecord._isAnonymousRecord = true;
SolutionPack_ApplicationDependencyRecord.UniqueId = "93317dc1-81d6-561d-9e1b-20bb771cfc43";
SolutionPack_ApplicationDependencyRecord.init();
return SolutionPack_ApplicationDependencyRecord;
})(OS.DataTypes.GenericRecord);
Extension_OMLProcessorModel.SolutionPack_ApplicationDependencyRecord = SolutionPack_ApplicationDependencyRecord;

});
define("Extension.OMLProcessor.model$SolutionPack_ApplicationDependencyRecordList", ["exports", "OutSystems/ClientRuntime/Main", "Extension.OMLProcessor.model", "Extension.OMLProcessor.model$SolutionPack_ApplicationDependencyRecord"], function (exports, OutSystems, Extension_OMLProcessorModel) {
var OS = OutSystems.Internal;
var SolutionPack_ApplicationDependencyRecordList = (function (_super) {
__extends(SolutionPack_ApplicationDependencyRecordList, _super);
function SolutionPack_ApplicationDependencyRecordList(defaults) {
_super.apply(this, arguments);
}
SolutionPack_ApplicationDependencyRecordList.itemType = Extension_OMLProcessorModel.SolutionPack_ApplicationDependencyRecord;
return SolutionPack_ApplicationDependencyRecordList;
})(OS.DataTypes.GenericRecordList);
Extension_OMLProcessorModel.SolutionPack_ApplicationDependencyRecordList = SolutionPack_ApplicationDependencyRecordList;

});
define("Extension.OMLProcessor.model$SolutionPack_ApplicationRec", ["exports", "OutSystems/ClientRuntime/Main", "Extension.OMLProcessor.model", "Extension.OMLProcessor.model$SolutionPack_ApplicationModuleRecordList", "Extension.OMLProcessor.model$SolutionPack_ApplicationDependencyRecordList"], function (exports, OutSystems, Extension_OMLProcessorModel) {
var OS = OutSystems.Internal;
var SolutionPack_ApplicationRec = (function (_super) {
__extends(SolutionPack_ApplicationRec, _super);
function SolutionPack_ApplicationRec(defaults) {
_super.apply(this, arguments);
}
SolutionPack_ApplicationRec.attributesToDeclare = function () {
return [
this.attr("Key", "keyAttr", "Key", true, false, OS.Types.Text, function () {
return "";
}, true), 
this.attr("Name", "nameAttr", "Name", true, false, OS.Types.Text, function () {
return "";
}, true), 
this.attr("Description", "descriptionAttr", "Description", false, false, OS.Types.Text, function () {
return "";
}, true), 
this.attr("DefaultThemeIsMobile", "defaultThemeIsMobileAttr", "DefaultThemeIsMobile", true, false, OS.Types.Boolean, function () {
return false;
}, true), 
this.attr("Icon", "iconAttr", "Icon", false, false, OS.Types.BinaryData, function () {
return OS.DataTypes.BinaryData.defaultValue;
}, true), 
this.attr("FrontOfficeEspaceKey", "frontOfficeEspaceKeyAttr", "FrontOfficeEspaceKey", false, false, OS.Types.Text, function () {
return "";
}, true), 
this.attr("FrontOfficeEspaceName", "frontOfficeEspaceNameAttr", "FrontOfficeEspaceName", false, false, OS.Types.Text, function () {
return "";
}, true), 
this.attr("BackOfficeEspaceKey", "backOfficeEspaceKeyAttr", "BackOfficeEspaceKey", false, false, OS.Types.Text, function () {
return "";
}, true), 
this.attr("BackOfficeEspaceName", "backOfficeEspaceNameAttr", "BackOfficeEspaceName", false, false, OS.Types.Text, function () {
return "";
}, true), 
this.attr("Modules", "modulesAttr", "Modules", true, false, OS.Types.RecordList, function () {
return OS.DataTypes.ImmutableBase.getData(new Extension_OMLProcessorModel.SolutionPack_ApplicationModuleRecordList());
}, true, Extension_OMLProcessorModel.SolutionPack_ApplicationModuleRecordList), 
this.attr("Dependencies", "dependenciesAttr", "Dependencies", true, false, OS.Types.RecordList, function () {
return OS.DataTypes.ImmutableBase.getData(new Extension_OMLProcessorModel.SolutionPack_ApplicationDependencyRecordList());
}, true, Extension_OMLProcessorModel.SolutionPack_ApplicationDependencyRecordList), 
this.attr("ForgeBaseVersion", "forgeBaseVersionAttr", "ForgeBaseVersion", true, false, OS.Types.Decimal, function () {
return OS.DataTypes.Decimal.defaultValue;
}, true), 
this.attr("ContentHash", "contentHashAttr", "ContentHash", true, false, OS.Types.Text, function () {
return "";
}, true), 
this.attr("APIHash", "aPIHashAttr", "APIHash", true, false, OS.Types.Text, function () {
return "";
}, true), 
this.attr("OriginEnvironmentCode", "originEnvironmentCodeAttr", "OriginEnvironmentCode", true, false, OS.Types.Text, function () {
return "";
}, true), 
this.attr("OutdatedReferences", "outdatedReferencesAttr", "OutdatedReferences", true, false, OS.Types.Boolean, function () {
return false;
}, true), 
this.attr("ApplicationKind", "applicationKindAttr", "ApplicationKind", true, false, OS.Types.Text, function () {
return "";
}, true), 
this.attr("TemplateKey", "templateKeyAttr", "TemplateKey", true, false, OS.Types.Text, function () {
return "";
}, true), 
this.attr("PrimaryColor", "primaryColorAttr", "PrimaryColor", true, false, OS.Types.Text, function () {
return "";
}, true)
].concat(_super.attributesToDeclare.call(this));
};
SolutionPack_ApplicationRec.init();
return SolutionPack_ApplicationRec;
})(OS.DataTypes.GenericRecord);
Extension_OMLProcessorModel.SolutionPack_ApplicationRec = SolutionPack_ApplicationRec;

});
define("Extension.OMLProcessor.model$SolutionPack_ApplicationRecord", ["exports", "OutSystems/ClientRuntime/Main", "Extension.OMLProcessor.model", "Extension.OMLProcessor.model$SolutionPack_ApplicationRec"], function (exports, OutSystems, Extension_OMLProcessorModel) {
var OS = OutSystems.Internal;
var SolutionPack_ApplicationRecord = (function (_super) {
__extends(SolutionPack_ApplicationRecord, _super);
function SolutionPack_ApplicationRecord(defaults) {
_super.apply(this, arguments);
}
SolutionPack_ApplicationRecord.attributesToDeclare = function () {
return [
this.attr("SolutionPack_Application", "solutionPack_ApplicationAttr", "SolutionPack_Application", false, false, OS.Types.Record, function () {
return OS.DataTypes.ImmutableBase.getData(new Extension_OMLProcessorModel.SolutionPack_ApplicationRec());
}, true, Extension_OMLProcessorModel.SolutionPack_ApplicationRec)
].concat(_super.attributesToDeclare.call(this));
};
SolutionPack_ApplicationRecord.fromStructure = function (str) {
return new SolutionPack_ApplicationRecord(new SolutionPack_ApplicationRecord.RecordClass({
solutionPack_ApplicationAttr: OS.DataTypes.ImmutableBase.getData(str)
}));
};
SolutionPack_ApplicationRecord._isAnonymousRecord = true;
SolutionPack_ApplicationRecord.UniqueId = "0d153080-ab04-192f-dcd0-f2a52244b779";
SolutionPack_ApplicationRecord.init();
return SolutionPack_ApplicationRecord;
})(OS.DataTypes.GenericRecord);
Extension_OMLProcessorModel.SolutionPack_ApplicationRecord = SolutionPack_ApplicationRecord;

});
define("Extension.OMLProcessor.model$SolutionPack_ApplicationRecordList", ["exports", "OutSystems/ClientRuntime/Main", "Extension.OMLProcessor.model", "Extension.OMLProcessor.model$SolutionPack_ApplicationRecord"], function (exports, OutSystems, Extension_OMLProcessorModel) {
var OS = OutSystems.Internal;
var SolutionPack_ApplicationRecordList = (function (_super) {
__extends(SolutionPack_ApplicationRecordList, _super);
function SolutionPack_ApplicationRecordList(defaults) {
_super.apply(this, arguments);
}
SolutionPack_ApplicationRecordList.itemType = Extension_OMLProcessorModel.SolutionPack_ApplicationRecord;
return SolutionPack_ApplicationRecordList;
})(OS.DataTypes.GenericRecordList);
Extension_OMLProcessorModel.SolutionPack_ApplicationRecordList = SolutionPack_ApplicationRecordList;

});
define("Extension.OMLProcessor.model$SolutionPack_SolutionVersionReferenceRec", ["exports", "OutSystems/ClientRuntime/Main", "Extension.OMLProcessor.model", "Extension.OMLProcessor.model$SolutionPack_SolutionLogicalDatabaseRecordList", "Extension.OMLProcessor.model$SolutionPack_SolutionDBCatalogRecord"], function (exports, OutSystems, Extension_OMLProcessorModel) {
var OS = OutSystems.Internal;
var SolutionPack_SolutionVersionReferenceRec = (function (_super) {
__extends(SolutionPack_SolutionVersionReferenceRec, _super);
function SolutionPack_SolutionVersionReferenceRec(defaults) {
_super.apply(this, arguments);
}
SolutionPack_SolutionVersionReferenceRec.attributesToDeclare = function () {
return [
this.attr("UID", "uIDAttr", "UID", true, false, OS.Types.Text, function () {
return "";
}, true), 
this.attr("Kind", "kindAttr", "Kind", true, false, OS.Types.Text, function () {
return "";
}, true), 
this.attr("Name", "nameAttr", "Name", true, false, OS.Types.Text, function () {
return "";
}, true), 
this.attr("MainReference", "mainReferenceAttr", "MainReference", true, false, OS.Types.Boolean, function () {
return false;
}, true), 
this.attr("Filename", "filenameAttr", "Filename", true, false, OS.Types.Text, function () {
return "";
}, true), 
this.attr("InPack", "inPackAttr", "InPack", true, false, OS.Types.Boolean, function () {
return false;
}, true), 
this.attr("Hash", "hashAttr", "Hash", true, false, OS.Types.Text, function () {
return "";
}, true), 
this.attr("Creation", "creationAttr", "Creation", true, false, OS.Types.DateTime, function () {
return OS.DataTypes.DateTime.defaultValue;
}, true), 
this.attr("CreationBy", "creationByAttr", "CreationBy", true, false, OS.Types.Text, function () {
return "";
}, true), 
this.attr("VersionId", "versionIdAttr", "VersionId", false, false, OS.Types.Integer, function () {
return 0;
}, true), 
this.attr("LogicalDatabases", "logicalDatabasesAttr", "LogicalDatabases", true, false, OS.Types.RecordList, function () {
return OS.DataTypes.ImmutableBase.getData(new Extension_OMLProcessorModel.SolutionPack_SolutionLogicalDatabaseRecordList());
}, true, Extension_OMLProcessorModel.SolutionPack_SolutionLogicalDatabaseRecordList), 
this.attr("DBCatalog", "dBCatalogAttr", "DBCatalog", true, false, OS.Types.Record, function () {
return OS.DataTypes.ImmutableBase.getData(new Extension_OMLProcessorModel.SolutionPack_SolutionDBCatalogRecord());
}, true, Extension_OMLProcessorModel.SolutionPack_SolutionDBCatalogRecord), 
this.attr("Description", "descriptionAttr", "Description", false, false, OS.Types.Text, function () {
return "";
}, true)
].concat(_super.attributesToDeclare.call(this));
};
SolutionPack_SolutionVersionReferenceRec.init();
return SolutionPack_SolutionVersionReferenceRec;
})(OS.DataTypes.GenericRecord);
Extension_OMLProcessorModel.SolutionPack_SolutionVersionReferenceRec = SolutionPack_SolutionVersionReferenceRec;

});
define("Extension.OMLProcessor.model$SolutionPack_SolutionVersionReferenceRecord", ["exports", "OutSystems/ClientRuntime/Main", "Extension.OMLProcessor.model", "Extension.OMLProcessor.model$SolutionPack_SolutionVersionReferenceRec"], function (exports, OutSystems, Extension_OMLProcessorModel) {
var OS = OutSystems.Internal;
var SolutionPack_SolutionVersionReferenceRecord = (function (_super) {
__extends(SolutionPack_SolutionVersionReferenceRecord, _super);
function SolutionPack_SolutionVersionReferenceRecord(defaults) {
_super.apply(this, arguments);
}
SolutionPack_SolutionVersionReferenceRecord.attributesToDeclare = function () {
return [
this.attr("SolutionPack_SolutionVersionReference", "solutionPack_SolutionVersionReferenceAttr", "SolutionPack_SolutionVersionReference", false, false, OS.Types.Record, function () {
return OS.DataTypes.ImmutableBase.getData(new Extension_OMLProcessorModel.SolutionPack_SolutionVersionReferenceRec());
}, true, Extension_OMLProcessorModel.SolutionPack_SolutionVersionReferenceRec)
].concat(_super.attributesToDeclare.call(this));
};
SolutionPack_SolutionVersionReferenceRecord.fromStructure = function (str) {
return new SolutionPack_SolutionVersionReferenceRecord(new SolutionPack_SolutionVersionReferenceRecord.RecordClass({
solutionPack_SolutionVersionReferenceAttr: OS.DataTypes.ImmutableBase.getData(str)
}));
};
SolutionPack_SolutionVersionReferenceRecord._isAnonymousRecord = true;
SolutionPack_SolutionVersionReferenceRecord.UniqueId = "e15b7d7c-b897-458b-2008-ac3e511299d7";
SolutionPack_SolutionVersionReferenceRecord.init();
return SolutionPack_SolutionVersionReferenceRecord;
})(OS.DataTypes.GenericRecord);
Extension_OMLProcessorModel.SolutionPack_SolutionVersionReferenceRecord = SolutionPack_SolutionVersionReferenceRecord;

});
define("Extension.OMLProcessor.model$SolutionPack_SolutionVersionReferenceRecordList", ["exports", "OutSystems/ClientRuntime/Main", "Extension.OMLProcessor.model", "Extension.OMLProcessor.model$SolutionPack_SolutionVersionReferenceRecord"], function (exports, OutSystems, Extension_OMLProcessorModel) {
var OS = OutSystems.Internal;
var SolutionPack_SolutionVersionReferenceRecordList = (function (_super) {
__extends(SolutionPack_SolutionVersionReferenceRecordList, _super);
function SolutionPack_SolutionVersionReferenceRecordList(defaults) {
_super.apply(this, arguments);
}
SolutionPack_SolutionVersionReferenceRecordList.itemType = Extension_OMLProcessorModel.SolutionPack_SolutionVersionReferenceRecord;
return SolutionPack_SolutionVersionReferenceRecordList;
})(OS.DataTypes.GenericRecordList);
Extension_OMLProcessorModel.SolutionPack_SolutionVersionReferenceRecordList = SolutionPack_SolutionVersionReferenceRecordList;

});
define("Extension.OMLProcessor.model$SolutionPack_SolutionVersionRec", ["exports", "OutSystems/ClientRuntime/Main", "Extension.OMLProcessor.model", "Extension.OMLProcessor.model$SolutionPack_SolutionVersionReferenceRecordList"], function (exports, OutSystems, Extension_OMLProcessorModel) {
var OS = OutSystems.Internal;
var SolutionPack_SolutionVersionRec = (function (_super) {
__extends(SolutionPack_SolutionVersionRec, _super);
function SolutionPack_SolutionVersionRec(defaults) {
_super.apply(this, arguments);
}
SolutionPack_SolutionVersionRec.attributesToDeclare = function () {
return [
this.attr("UID", "uIDAttr", "UID", true, false, OS.Types.Text, function () {
return "";
}, true), 
this.attr("Creation", "creationAttr", "Creation", true, false, OS.Types.DateTime, function () {
return OS.DataTypes.DateTime.defaultValue;
}, true), 
this.attr("Name", "nameAttr", "Name", true, false, OS.Types.Text, function () {
return "";
}, true), 
this.attr("Description", "descriptionAttr", "Description", false, false, OS.Types.Text, function () {
return "";
}, true), 
this.attr("References", "referencesAttr", "References", true, false, OS.Types.RecordList, function () {
return OS.DataTypes.ImmutableBase.getData(new Extension_OMLProcessorModel.SolutionPack_SolutionVersionReferenceRecordList());
}, true, Extension_OMLProcessorModel.SolutionPack_SolutionVersionReferenceRecordList)
].concat(_super.attributesToDeclare.call(this));
};
SolutionPack_SolutionVersionRec.init();
return SolutionPack_SolutionVersionRec;
})(OS.DataTypes.GenericRecord);
Extension_OMLProcessorModel.SolutionPack_SolutionVersionRec = SolutionPack_SolutionVersionRec;

});
define("Extension.OMLProcessor.model$SolutionPack_SolutionVersionRecord", ["exports", "OutSystems/ClientRuntime/Main", "Extension.OMLProcessor.model", "Extension.OMLProcessor.model$SolutionPack_SolutionVersionRec"], function (exports, OutSystems, Extension_OMLProcessorModel) {
var OS = OutSystems.Internal;
var SolutionPack_SolutionVersionRecord = (function (_super) {
__extends(SolutionPack_SolutionVersionRecord, _super);
function SolutionPack_SolutionVersionRecord(defaults) {
_super.apply(this, arguments);
}
SolutionPack_SolutionVersionRecord.attributesToDeclare = function () {
return [
this.attr("SolutionPack_SolutionVersion", "solutionPack_SolutionVersionAttr", "SolutionPack_SolutionVersion", false, false, OS.Types.Record, function () {
return OS.DataTypes.ImmutableBase.getData(new Extension_OMLProcessorModel.SolutionPack_SolutionVersionRec());
}, true, Extension_OMLProcessorModel.SolutionPack_SolutionVersionRec)
].concat(_super.attributesToDeclare.call(this));
};
SolutionPack_SolutionVersionRecord.fromStructure = function (str) {
return new SolutionPack_SolutionVersionRecord(new SolutionPack_SolutionVersionRecord.RecordClass({
solutionPack_SolutionVersionAttr: OS.DataTypes.ImmutableBase.getData(str)
}));
};
SolutionPack_SolutionVersionRecord._isAnonymousRecord = true;
SolutionPack_SolutionVersionRecord.UniqueId = "f3bf0aba-1cb0-fd7b-590d-e65b1e85914d";
SolutionPack_SolutionVersionRecord.init();
return SolutionPack_SolutionVersionRecord;
})(OS.DataTypes.GenericRecord);
Extension_OMLProcessorModel.SolutionPack_SolutionVersionRecord = SolutionPack_SolutionVersionRecord;

});
define("Extension.OMLProcessor.model$SolutionPack_SolutionRec", ["exports", "OutSystems/ClientRuntime/Main", "Extension.OMLProcessor.model", "Extension.OMLProcessor.model$SolutionPack_SolutionReferenceRecordList", "Extension.OMLProcessor.model$SolutionPack_ApplicationRecordList", "Extension.OMLProcessor.model$SolutionPack_SolutionVersionRecord"], function (exports, OutSystems, Extension_OMLProcessorModel) {
var OS = OutSystems.Internal;
var SolutionPack_SolutionRec = (function (_super) {
__extends(SolutionPack_SolutionRec, _super);
function SolutionPack_SolutionRec(defaults) {
_super.apply(this, arguments);
}
SolutionPack_SolutionRec.attributesToDeclare = function () {
return [
this.attr("Schema", "schemaAttr", "Schema", true, false, OS.Types.Integer, function () {
return 0;
}, true), 
this.attr("HubServerVersion", "hubServerVersionAttr", "HubServerVersion", true, false, OS.Types.Text, function () {
return "";
}, true), 
this.attr("LastUpgradeVersion", "lastUpgradeVersionAttr", "LastUpgradeVersion", true, false, OS.Types.Text, function () {
return "";
}, true), 
this.attr("UID", "uIDAttr", "UID", true, false, OS.Types.Text, function () {
return "";
}, true), 
this.attr("Creation", "creationAttr", "Creation", true, false, OS.Types.DateTime, function () {
return OS.DataTypes.DateTime.defaultValue;
}, true), 
this.attr("Name", "nameAttr", "Name", true, false, OS.Types.Text, function () {
return "";
}, true), 
this.attr("Description", "descriptionAttr", "Description", false, false, OS.Types.Text, function () {
return "";
}, true), 
this.attr("References", "referencesAttr", "References", true, false, OS.Types.RecordList, function () {
return OS.DataTypes.ImmutableBase.getData(new Extension_OMLProcessorModel.SolutionPack_SolutionReferenceRecordList());
}, true, Extension_OMLProcessorModel.SolutionPack_SolutionReferenceRecordList), 
this.attr("Applications", "applicationsAttr", "Applications", true, false, OS.Types.RecordList, function () {
return OS.DataTypes.ImmutableBase.getData(new Extension_OMLProcessorModel.SolutionPack_ApplicationRecordList());
}, true, Extension_OMLProcessorModel.SolutionPack_ApplicationRecordList), 
this.attr("Version", "versionAttr", "Version", true, false, OS.Types.Record, function () {
return OS.DataTypes.ImmutableBase.getData(new Extension_OMLProcessorModel.SolutionPack_SolutionVersionRecord());
}, true, Extension_OMLProcessorModel.SolutionPack_SolutionVersionRecord), 
this.attr("ActivationCode", "activationCodeAttr", "ActivationCode", true, false, OS.Types.Text, function () {
return "";
}, true), 
this.attr("IsTransient", "isTransientAttr", "IsTransient", true, false, OS.Types.Boolean, function () {
return false;
}, true)
].concat(_super.attributesToDeclare.call(this));
};
SolutionPack_SolutionRec.init();
return SolutionPack_SolutionRec;
})(OS.DataTypes.GenericRecord);
Extension_OMLProcessorModel.SolutionPack_SolutionRec = SolutionPack_SolutionRec;

});
define("Extension.OMLProcessor.model$SolutionPack_SolutionRecord", ["exports", "OutSystems/ClientRuntime/Main", "Extension.OMLProcessor.model", "Extension.OMLProcessor.model$SolutionPack_SolutionRec"], function (exports, OutSystems, Extension_OMLProcessorModel) {
var OS = OutSystems.Internal;
var SolutionPack_SolutionRecord = (function (_super) {
__extends(SolutionPack_SolutionRecord, _super);
function SolutionPack_SolutionRecord(defaults) {
_super.apply(this, arguments);
}
SolutionPack_SolutionRecord.attributesToDeclare = function () {
return [
this.attr("SolutionPack_Solution", "solutionPack_SolutionAttr", "SolutionPack_Solution", false, false, OS.Types.Record, function () {
return OS.DataTypes.ImmutableBase.getData(new Extension_OMLProcessorModel.SolutionPack_SolutionRec());
}, true, Extension_OMLProcessorModel.SolutionPack_SolutionRec)
].concat(_super.attributesToDeclare.call(this));
};
SolutionPack_SolutionRecord.fromStructure = function (str) {
return new SolutionPack_SolutionRecord(new SolutionPack_SolutionRecord.RecordClass({
solutionPack_SolutionAttr: OS.DataTypes.ImmutableBase.getData(str)
}));
};
SolutionPack_SolutionRecord._isAnonymousRecord = true;
SolutionPack_SolutionRecord.UniqueId = "f5176e25-2ba4-c1f9-56e6-64a3661d9ee8";
SolutionPack_SolutionRecord.init();
return SolutionPack_SolutionRecord;
})(OS.DataTypes.GenericRecord);
Extension_OMLProcessorModel.SolutionPack_SolutionRecord = SolutionPack_SolutionRecord;

});
define("Extension.OMLProcessor.model$SolutionPack_SolutionRecordList", ["exports", "OutSystems/ClientRuntime/Main", "Extension.OMLProcessor.model", "Extension.OMLProcessor.model$SolutionPack_SolutionRecord"], function (exports, OutSystems, Extension_OMLProcessorModel) {
var OS = OutSystems.Internal;
var SolutionPack_SolutionRecordList = (function (_super) {
__extends(SolutionPack_SolutionRecordList, _super);
function SolutionPack_SolutionRecordList(defaults) {
_super.apply(this, arguments);
}
SolutionPack_SolutionRecordList.itemType = Extension_OMLProcessorModel.SolutionPack_SolutionRecord;
return SolutionPack_SolutionRecordList;
})(OS.DataTypes.GenericRecordList);
Extension_OMLProcessorModel.SolutionPack_SolutionRecordList = SolutionPack_SolutionRecordList;

});
define("Extension.OMLProcessor.model$SolutionPackRec", ["exports", "OutSystems/ClientRuntime/Main", "Extension.OMLProcessor.model", "Extension.OMLProcessor.model$SolutionPack_SolutionRecordList"], function (exports, OutSystems, Extension_OMLProcessorModel) {
var OS = OutSystems.Internal;
var SolutionPackRec = (function (_super) {
__extends(SolutionPackRec, _super);
function SolutionPackRec(defaults) {
_super.apply(this, arguments);
}
SolutionPackRec.attributesToDeclare = function () {
return [
this.attr("Schema", "schemaAttr", "Schema", true, false, OS.Types.Integer, function () {
return 0;
}, true), 
this.attr("HubServerVersion", "hubServerVersionAttr", "HubServerVersion", true, false, OS.Types.Text, function () {
return "";
}, true), 
this.attr("LastUpgradeVersion", "lastUpgradeVersionAttr", "LastUpgradeVersion", true, false, OS.Types.Text, function () {
return "";
}, true), 
this.attr("UID", "uIDAttr", "UID", true, false, OS.Types.Text, function () {
return "";
}, true), 
this.attr("Creation", "creationAttr", "Creation", true, false, OS.Types.DateTime, function () {
return OS.DataTypes.DateTime.defaultValue;
}, true), 
this.attr("Solutions", "solutionsAttr", "Solutions", true, false, OS.Types.RecordList, function () {
return OS.DataTypes.ImmutableBase.getData(new Extension_OMLProcessorModel.SolutionPack_SolutionRecordList());
}, true, Extension_OMLProcessorModel.SolutionPack_SolutionRecordList), 
this.attr("IsApplicationPack", "isApplicationPackAttr", "IsApplicationPack", false, false, OS.Types.Boolean, function () {
return false;
}, true)
].concat(_super.attributesToDeclare.call(this));
};
SolutionPackRec.init();
return SolutionPackRec;
})(OS.DataTypes.GenericRecord);
Extension_OMLProcessorModel.SolutionPackRec = SolutionPackRec;

});
define("Extension.OMLProcessor.model$SolutionPackRecord", ["exports", "OutSystems/ClientRuntime/Main", "Extension.OMLProcessor.model", "Extension.OMLProcessor.model$SolutionPackRec"], function (exports, OutSystems, Extension_OMLProcessorModel) {
var OS = OutSystems.Internal;
var SolutionPackRecord = (function (_super) {
__extends(SolutionPackRecord, _super);
function SolutionPackRecord(defaults) {
_super.apply(this, arguments);
}
SolutionPackRecord.attributesToDeclare = function () {
return [
this.attr("SolutionPack", "solutionPackAttr", "SolutionPack", false, false, OS.Types.Record, function () {
return OS.DataTypes.ImmutableBase.getData(new Extension_OMLProcessorModel.SolutionPackRec());
}, true, Extension_OMLProcessorModel.SolutionPackRec)
].concat(_super.attributesToDeclare.call(this));
};
SolutionPackRecord.fromStructure = function (str) {
return new SolutionPackRecord(new SolutionPackRecord.RecordClass({
solutionPackAttr: OS.DataTypes.ImmutableBase.getData(str)
}));
};
SolutionPackRecord._isAnonymousRecord = true;
SolutionPackRecord.UniqueId = "68954c2b-71e6-781c-fc07-6daf6378260b";
SolutionPackRecord.init();
return SolutionPackRecord;
})(OS.DataTypes.GenericRecord);
Extension_OMLProcessorModel.SolutionPackRecord = SolutionPackRecord;

});
define("Extension.OMLProcessor.model$SolutionPackRecordList", ["exports", "OutSystems/ClientRuntime/Main", "Extension.OMLProcessor.model", "Extension.OMLProcessor.model$SolutionPackRecord"], function (exports, OutSystems, Extension_OMLProcessorModel) {
var OS = OutSystems.Internal;
var SolutionPackRecordList = (function (_super) {
__extends(SolutionPackRecordList, _super);
function SolutionPackRecordList(defaults) {
_super.apply(this, arguments);
}
SolutionPackRecordList.itemType = Extension_OMLProcessorModel.SolutionPackRecord;
return SolutionPackRecordList;
})(OS.DataTypes.GenericRecordList);
Extension_OMLProcessorModel.SolutionPackRecordList = SolutionPackRecordList;

});
define("Extension.OMLProcessor.model$EspaceToPublishRec", ["exports", "OutSystems/ClientRuntime/Main", "Extension.OMLProcessor.model"], function (exports, OutSystems, Extension_OMLProcessorModel) {
var OS = OutSystems.Internal;
var EspaceToPublishRec = (function (_super) {
__extends(EspaceToPublishRec, _super);
function EspaceToPublishRec(defaults) {
_super.apply(this, arguments);
}
EspaceToPublishRec.attributesToDeclare = function () {
return [
this.attr("Oml", "omlAttr", "Oml", false, false, OS.Types.BinaryData, function () {
return OS.DataTypes.BinaryData.defaultValue;
}, true), 
this.attr("EspaceVersionId", "espaceVersionIdAttr", "EspaceVersionId", false, false, OS.Types.Integer, function () {
return 0;
}, true)
].concat(_super.attributesToDeclare.call(this));
};
EspaceToPublishRec.init();
return EspaceToPublishRec;
})(OS.DataTypes.GenericRecord);
Extension_OMLProcessorModel.EspaceToPublishRec = EspaceToPublishRec;

});
define("Extension.OMLProcessor.model$EspaceToPublishRecord", ["exports", "OutSystems/ClientRuntime/Main", "Extension.OMLProcessor.model", "Extension.OMLProcessor.model$EspaceToPublishRec"], function (exports, OutSystems, Extension_OMLProcessorModel) {
var OS = OutSystems.Internal;
var EspaceToPublishRecord = (function (_super) {
__extends(EspaceToPublishRecord, _super);
function EspaceToPublishRecord(defaults) {
_super.apply(this, arguments);
}
EspaceToPublishRecord.attributesToDeclare = function () {
return [
this.attr("EspaceToPublish", "espaceToPublishAttr", "EspaceToPublish", false, false, OS.Types.Record, function () {
return OS.DataTypes.ImmutableBase.getData(new Extension_OMLProcessorModel.EspaceToPublishRec());
}, true, Extension_OMLProcessorModel.EspaceToPublishRec)
].concat(_super.attributesToDeclare.call(this));
};
EspaceToPublishRecord.fromStructure = function (str) {
return new EspaceToPublishRecord(new EspaceToPublishRecord.RecordClass({
espaceToPublishAttr: OS.DataTypes.ImmutableBase.getData(str)
}));
};
EspaceToPublishRecord._isAnonymousRecord = true;
EspaceToPublishRecord.UniqueId = "0a4c9dc4-7877-49a1-832e-e1d665aa6cfa";
EspaceToPublishRecord.init();
return EspaceToPublishRecord;
})(OS.DataTypes.GenericRecord);
Extension_OMLProcessorModel.EspaceToPublishRecord = EspaceToPublishRecord;

});
define("Extension.OMLProcessor.model$EspaceToPublishRecordList", ["exports", "OutSystems/ClientRuntime/Main", "Extension.OMLProcessor.model", "Extension.OMLProcessor.model$EspaceToPublishRecord"], function (exports, OutSystems, Extension_OMLProcessorModel) {
var OS = OutSystems.Internal;
var EspaceToPublishRecordList = (function (_super) {
__extends(EspaceToPublishRecordList, _super);
function EspaceToPublishRecordList(defaults) {
_super.apply(this, arguments);
}
EspaceToPublishRecordList.itemType = Extension_OMLProcessorModel.EspaceToPublishRecord;
return EspaceToPublishRecordList;
})(OS.DataTypes.GenericRecordList);
Extension_OMLProcessorModel.EspaceToPublishRecordList = EspaceToPublishRecordList;

});
define("Extension.OMLProcessor.model$GenericRecordDescriptionRec", ["exports", "OutSystems/ClientRuntime/Main", "Extension.OMLProcessor.model"], function (exports, OutSystems, Extension_OMLProcessorModel) {
var OS = OutSystems.Internal;
var GenericRecordDescriptionRec = (function (_super) {
__extends(GenericRecordDescriptionRec, _super);
function GenericRecordDescriptionRec(defaults) {
_super.apply(this, arguments);
}
GenericRecordDescriptionRec.attributesToDeclare = function () {
return [
this.attr("Key", "keyAttr", "Key", false, false, OS.Types.Text, function () {
return "";
}, true), 
this.attr("Name", "nameAttr", "Name", false, false, OS.Types.Text, function () {
return "";
}, true)
].concat(_super.attributesToDeclare.call(this));
};
GenericRecordDescriptionRec.init();
return GenericRecordDescriptionRec;
})(OS.DataTypes.GenericRecord);
Extension_OMLProcessorModel.GenericRecordDescriptionRec = GenericRecordDescriptionRec;

});
define("Extension.OMLProcessor.model$ModuleVersionFeatureRec", ["exports", "OutSystems/ClientRuntime/Main", "Extension.OMLProcessor.model"], function (exports, OutSystems, Extension_OMLProcessorModel) {
var OS = OutSystems.Internal;
var ModuleVersionFeatureRec = (function (_super) {
__extends(ModuleVersionFeatureRec, _super);
function ModuleVersionFeatureRec(defaults) {
_super.apply(this, arguments);
}
ModuleVersionFeatureRec.attributesToDeclare = function () {
return [
this.attr("Id", "idAttr", "Id", true, false, OS.Types.Text, function () {
return "";
}, true), 
this.attr("Name", "nameAttr", "Name", true, false, OS.Types.Text, function () {
return "";
}, true)
].concat(_super.attributesToDeclare.call(this));
};
ModuleVersionFeatureRec.init();
return ModuleVersionFeatureRec;
})(OS.DataTypes.GenericRecord);
Extension_OMLProcessorModel.ModuleVersionFeatureRec = ModuleVersionFeatureRec;

});
define("Extension.OMLProcessor.model$DeploymentZone_AmazonECSRec", ["exports", "OutSystems/ClientRuntime/Main", "Extension.OMLProcessor.model"], function (exports, OutSystems, Extension_OMLProcessorModel) {
var OS = OutSystems.Internal;
var DeploymentZone_AmazonECSRec = (function (_super) {
__extends(DeploymentZone_AmazonECSRec, _super);
function DeploymentZone_AmazonECSRec(defaults) {
_super.apply(this, arguments);
}
DeploymentZone_AmazonECSRec.attributesToDeclare = function () {
return [
this.attr("ResultPath", "resultPathAttr", "ResultPath", true, false, OS.Types.Text, function () {
return "";
}, true), 
this.attr("TargetPath", "targetPathAttr", "TargetPath", true, false, OS.Types.Text, function () {
return "";
}, true), 
this.attr("FromImageReference", "fromImageReferenceAttr", "FromImageReference", true, false, OS.Types.Text, function () {
return "";
}, true), 
this.attr("ConfigPath", "configPathAttr", "ConfigPath", true, false, OS.Types.Text, function () {
return "";
}, true), 
this.attr("OnPrepareDeployDone", "onPrepareDeployDoneAttr", "OnPrepareDeployDone", false, false, OS.Types.Text, function () {
return "";
}, true), 
this.attr("OnDeployDone", "onDeployDoneAttr", "OnDeployDone", false, false, OS.Types.Text, function () {
return "";
}, true), 
this.attr("OnUndeploy", "onUndeployAttr", "OnUndeploy", false, false, OS.Types.Text, function () {
return "";
}, true), 
this.attr("OnUpdateConfigurations", "onUpdateConfigurationsAttr", "OnUpdateConfigurations", false, false, OS.Types.Text, function () {
return "";
}, true), 
this.attr("ManualDeployTimeoutInMinutes", "manualDeployTimeoutInMinutesAttr", "ManualDeployTimeoutInMinutes", false, false, OS.Types.Integer, function () {
return 0;
}, true), 
this.attr("AutomaticDeployTimeoutInMinutes", "automaticDeployTimeoutInMinutesAttr", "AutomaticDeployTimeoutInMinutes", false, false, OS.Types.Integer, function () {
return 0;
}, true)
].concat(_super.attributesToDeclare.call(this));
};
DeploymentZone_AmazonECSRec.init();
return DeploymentZone_AmazonECSRec;
})(OS.DataTypes.GenericRecord);
Extension_OMLProcessorModel.DeploymentZone_AmazonECSRec = DeploymentZone_AmazonECSRec;

});
define("Extension.OMLProcessor.model$DeploymentZone_AmazonECSRecord", ["exports", "OutSystems/ClientRuntime/Main", "Extension.OMLProcessor.model", "Extension.OMLProcessor.model$DeploymentZone_AmazonECSRec"], function (exports, OutSystems, Extension_OMLProcessorModel) {
var OS = OutSystems.Internal;
var DeploymentZone_AmazonECSRecord = (function (_super) {
__extends(DeploymentZone_AmazonECSRecord, _super);
function DeploymentZone_AmazonECSRecord(defaults) {
_super.apply(this, arguments);
}
DeploymentZone_AmazonECSRecord.attributesToDeclare = function () {
return [
this.attr("DeploymentZone_AmazonECS", "deploymentZone_AmazonECSAttr", "DeploymentZone_AmazonECS", false, false, OS.Types.Record, function () {
return OS.DataTypes.ImmutableBase.getData(new Extension_OMLProcessorModel.DeploymentZone_AmazonECSRec());
}, true, Extension_OMLProcessorModel.DeploymentZone_AmazonECSRec)
].concat(_super.attributesToDeclare.call(this));
};
DeploymentZone_AmazonECSRecord.fromStructure = function (str) {
return new DeploymentZone_AmazonECSRecord(new DeploymentZone_AmazonECSRecord.RecordClass({
deploymentZone_AmazonECSAttr: OS.DataTypes.ImmutableBase.getData(str)
}));
};
DeploymentZone_AmazonECSRecord._isAnonymousRecord = true;
DeploymentZone_AmazonECSRecord.UniqueId = "98584ce8-162d-abb3-e298-84d63daf04c8";
DeploymentZone_AmazonECSRecord.init();
return DeploymentZone_AmazonECSRecord;
})(OS.DataTypes.GenericRecord);
Extension_OMLProcessorModel.DeploymentZone_AmazonECSRecord = DeploymentZone_AmazonECSRecord;

});
define("Extension.OMLProcessor.model$DeploymentZone_AmazonECSRecordList", ["exports", "OutSystems/ClientRuntime/Main", "Extension.OMLProcessor.model", "Extension.OMLProcessor.model$DeploymentZone_AmazonECSRecord"], function (exports, OutSystems, Extension_OMLProcessorModel) {
var OS = OutSystems.Internal;
var DeploymentZone_AmazonECSRecordList = (function (_super) {
__extends(DeploymentZone_AmazonECSRecordList, _super);
function DeploymentZone_AmazonECSRecordList(defaults) {
_super.apply(this, arguments);
}
DeploymentZone_AmazonECSRecordList.itemType = Extension_OMLProcessorModel.DeploymentZone_AmazonECSRecord;
return DeploymentZone_AmazonECSRecordList;
})(OS.DataTypes.GenericRecordList);
Extension_OMLProcessorModel.DeploymentZone_AmazonECSRecordList = DeploymentZone_AmazonECSRecordList;

});
define("Extension.OMLProcessor.model$DeploymentDetailsRec", ["exports", "OutSystems/ClientRuntime/Main", "Extension.OMLProcessor.model"], function (exports, OutSystems, Extension_OMLProcessorModel) {
var OS = OutSystems.Internal;
var DeploymentDetailsRec = (function (_super) {
__extends(DeploymentDetailsRec, _super);
function DeploymentDetailsRec(defaults) {
_super.apply(this, arguments);
}
DeploymentDetailsRec.attributesToDeclare = function () {
return [
this.attr("EspaceVersiondId", "espaceVersiondIdAttr", "EspaceVersiondId", true, false, OS.Types.Integer, function () {
return 0;
}, true), 
this.attr("DeploymentId", "deploymentIdAttr", "DeploymentId", true, false, OS.Types.Text, function () {
return "";
}, true)
].concat(_super.attributesToDeclare.call(this));
};
DeploymentDetailsRec.init();
return DeploymentDetailsRec;
})(OS.DataTypes.GenericRecord);
Extension_OMLProcessorModel.DeploymentDetailsRec = DeploymentDetailsRec;

});
define("Extension.OMLProcessor.model$DeploymentDetailsRecord", ["exports", "OutSystems/ClientRuntime/Main", "Extension.OMLProcessor.model", "Extension.OMLProcessor.model$DeploymentDetailsRec"], function (exports, OutSystems, Extension_OMLProcessorModel) {
var OS = OutSystems.Internal;
var DeploymentDetailsRecord = (function (_super) {
__extends(DeploymentDetailsRecord, _super);
function DeploymentDetailsRecord(defaults) {
_super.apply(this, arguments);
}
DeploymentDetailsRecord.attributesToDeclare = function () {
return [
this.attr("DeploymentDetails", "deploymentDetailsAttr", "DeploymentDetails", false, false, OS.Types.Record, function () {
return OS.DataTypes.ImmutableBase.getData(new Extension_OMLProcessorModel.DeploymentDetailsRec());
}, true, Extension_OMLProcessorModel.DeploymentDetailsRec)
].concat(_super.attributesToDeclare.call(this));
};
DeploymentDetailsRecord.fromStructure = function (str) {
return new DeploymentDetailsRecord(new DeploymentDetailsRecord.RecordClass({
deploymentDetailsAttr: OS.DataTypes.ImmutableBase.getData(str)
}));
};
DeploymentDetailsRecord._isAnonymousRecord = true;
DeploymentDetailsRecord.UniqueId = "b933966b-6c70-35bd-f3fe-0fa016ef29e7";
DeploymentDetailsRecord.init();
return DeploymentDetailsRecord;
})(OS.DataTypes.GenericRecord);
Extension_OMLProcessorModel.DeploymentDetailsRecord = DeploymentDetailsRecord;

});
define("Extension.OMLProcessor.model$DeploymentDetailsRecordList", ["exports", "OutSystems/ClientRuntime/Main", "Extension.OMLProcessor.model", "Extension.OMLProcessor.model$DeploymentDetailsRecord"], function (exports, OutSystems, Extension_OMLProcessorModel) {
var OS = OutSystems.Internal;
var DeploymentDetailsRecordList = (function (_super) {
__extends(DeploymentDetailsRecordList, _super);
function DeploymentDetailsRecordList(defaults) {
_super.apply(this, arguments);
}
DeploymentDetailsRecordList.itemType = Extension_OMLProcessorModel.DeploymentDetailsRecord;
return DeploymentDetailsRecordList;
})(OS.DataTypes.GenericRecordList);
Extension_OMLProcessorModel.DeploymentDetailsRecordList = DeploymentDetailsRecordList;

});
define("Extension.OMLProcessor.model$DBCatalogRec", ["exports", "OutSystems/ClientRuntime/Main", "Extension.OMLProcessor.model"], function (exports, OutSystems, Extension_OMLProcessorModel) {
var OS = OutSystems.Internal;
var DBCatalogRec = (function (_super) {
__extends(DBCatalogRec, _super);
function DBCatalogRec(defaults) {
_super.apply(this, arguments);
}
DBCatalogRec.attributesToDeclare = function () {
return [
this.attr("Id", "idAttr", "Id", true, false, OS.Types.Integer, function () {
return 0;
}, true), 
this.attr("Name", "nameAttr", "Name", true, false, OS.Types.Text, function () {
return "";
}, true), 
this.attr("LogicalName", "logicalNameAttr", "LogicalName", true, false, OS.Types.Text, function () {
return "";
}, true), 
this.attr("Description", "descriptionAttr", "Description", true, false, OS.Types.Text, function () {
return "";
}, true), 
this.attr("Username", "usernameAttr", "Username", true, false, OS.Types.Text, function () {
return "";
}, true), 
this.attr("Password", "passwordAttr", "Password", true, false, OS.Types.Text, function () {
return "";
}, true), 
this.attr("OwnerUsername", "ownerUsernameAttr", "OwnerUsername", true, false, OS.Types.Text, function () {
return "";
}, true), 
this.attr("OwnerPassword", "ownerPasswordAttr", "OwnerPassword", true, false, OS.Types.Text, function () {
return "";
}, true), 
this.attr("DataTablespace", "dataTablespaceAttr", "DataTablespace", true, false, OS.Types.Text, function () {
return "";
}, true), 
this.attr("IndexTablespace", "indexTablespaceAttr", "IndexTablespace", true, false, OS.Types.Text, function () {
return "";
}, true), 
this.attr("UseIntegratedAuth", "useIntegratedAuthAttr", "UseIntegratedAuth", true, false, OS.Types.Boolean, function () {
return false;
}, true)
].concat(_super.attributesToDeclare.call(this));
};
DBCatalogRec.init();
return DBCatalogRec;
})(OS.DataTypes.GenericRecord);
Extension_OMLProcessorModel.DBCatalogRec = DBCatalogRec;

});
define("Extension.OMLProcessor.model$DALEntityHighlightingHashRec", ["exports", "OutSystems/ClientRuntime/Main", "Extension.OMLProcessor.model"], function (exports, OutSystems, Extension_OMLProcessorModel) {
var OS = OutSystems.Internal;
var DALEntityHighlightingHashRec = (function (_super) {
__extends(DALEntityHighlightingHashRec, _super);
function DALEntityHighlightingHashRec(defaults) {
_super.apply(this, arguments);
}
DALEntityHighlightingHashRec.attributesToDeclare = function () {
return [
this.attr("ModuleKey", "moduleKeyAttr", "ModuleKey", true, false, OS.Types.Text, function () {
return "";
}, true), 
this.attr("EntityKey", "entityKeyAttr", "EntityKey", true, false, OS.Types.Text, function () {
return "";
}, true), 
this.attr("HighlightingHash", "highlightingHashAttr", "HighlightingHash", true, false, OS.Types.Text, function () {
return "";
}, true)
].concat(_super.attributesToDeclare.call(this));
};
DALEntityHighlightingHashRec.init();
return DALEntityHighlightingHashRec;
})(OS.DataTypes.GenericRecord);
Extension_OMLProcessorModel.DALEntityHighlightingHashRec = DALEntityHighlightingHashRec;

});
define("Extension.OMLProcessor.model$DALEntityHighlightingHashRecord", ["exports", "OutSystems/ClientRuntime/Main", "Extension.OMLProcessor.model", "Extension.OMLProcessor.model$DALEntityHighlightingHashRec"], function (exports, OutSystems, Extension_OMLProcessorModel) {
var OS = OutSystems.Internal;
var DALEntityHighlightingHashRecord = (function (_super) {
__extends(DALEntityHighlightingHashRecord, _super);
function DALEntityHighlightingHashRecord(defaults) {
_super.apply(this, arguments);
}
DALEntityHighlightingHashRecord.attributesToDeclare = function () {
return [
this.attr("DALEntityHighlightingHash", "dALEntityHighlightingHashAttr", "DALEntityHighlightingHash", false, false, OS.Types.Record, function () {
return OS.DataTypes.ImmutableBase.getData(new Extension_OMLProcessorModel.DALEntityHighlightingHashRec());
}, true, Extension_OMLProcessorModel.DALEntityHighlightingHashRec)
].concat(_super.attributesToDeclare.call(this));
};
DALEntityHighlightingHashRecord.fromStructure = function (str) {
return new DALEntityHighlightingHashRecord(new DALEntityHighlightingHashRecord.RecordClass({
dALEntityHighlightingHashAttr: OS.DataTypes.ImmutableBase.getData(str)
}));
};
DALEntityHighlightingHashRecord._isAnonymousRecord = true;
DALEntityHighlightingHashRecord.UniqueId = "c6bcd567-d074-4726-91c6-1bd6754c7a42";
DALEntityHighlightingHashRecord.init();
return DALEntityHighlightingHashRecord;
})(OS.DataTypes.GenericRecord);
Extension_OMLProcessorModel.DALEntityHighlightingHashRecord = DALEntityHighlightingHashRecord;

});
define("Extension.OMLProcessor.model$DALEntityHighlightingHashRecordList", ["exports", "OutSystems/ClientRuntime/Main", "Extension.OMLProcessor.model", "Extension.OMLProcessor.model$DALEntityHighlightingHashRecord"], function (exports, OutSystems, Extension_OMLProcessorModel) {
var OS = OutSystems.Internal;
var DALEntityHighlightingHashRecordList = (function (_super) {
__extends(DALEntityHighlightingHashRecordList, _super);
function DALEntityHighlightingHashRecordList(defaults) {
_super.apply(this, arguments);
}
DALEntityHighlightingHashRecordList.itemType = Extension_OMLProcessorModel.DALEntityHighlightingHashRecord;
return DALEntityHighlightingHashRecordList;
})(OS.DataTypes.GenericRecordList);
Extension_OMLProcessorModel.DALEntityHighlightingHashRecordList = DALEntityHighlightingHashRecordList;

});
define("Extension.OMLProcessor.model$ExtensionInfoRec", ["exports", "OutSystems/ClientRuntime/Main", "Extension.OMLProcessor.model"], function (exports, OutSystems, Extension_OMLProcessorModel) {
var OS = OutSystems.Internal;
var ExtensionInfoRec = (function (_super) {
__extends(ExtensionInfoRec, _super);
function ExtensionInfoRec(defaults) {
_super.apply(this, arguments);
}
ExtensionInfoRec.attributesToDeclare = function () {
return [
this.attr("Name", "nameAttr", "Name", false, false, OS.Types.Text, function () {
return "";
}, true), 
this.attr("Key", "keyAttr", "Key", false, false, OS.Types.Text, function () {
return "";
}, true), 
this.attr("status", "statusAttr", "status", false, false, OS.Types.Text, function () {
return "";
}, true)
].concat(_super.attributesToDeclare.call(this));
};
ExtensionInfoRec.init();
return ExtensionInfoRec;
})(OS.DataTypes.GenericRecord);
Extension_OMLProcessorModel.ExtensionInfoRec = ExtensionInfoRec;

});
define("Extension.OMLProcessor.model$ModuleDefinitionRec", ["exports", "OutSystems/ClientRuntime/Main", "Extension.OMLProcessor.model"], function (exports, OutSystems, Extension_OMLProcessorModel) {
var OS = OutSystems.Internal;
var ModuleDefinitionRec = (function (_super) {
__extends(ModuleDefinitionRec, _super);
function ModuleDefinitionRec(defaults) {
_super.apply(this, arguments);
}
ModuleDefinitionRec.attributesToDeclare = function () {
return [
this.attr("id", "idAttr", "id", true, false, OS.Types.Integer, function () {
return 0;
}, true), 
this.attr("name", "nameAttr", "name", true, false, OS.Types.Text, function () {
return "";
}, true), 
this.attr("isActive", "isActiveAttr", "isActive", true, false, OS.Types.Boolean, function () {
return false;
}, true), 
this.attr("moduleType", "moduleTypeAttr", "moduleType", true, false, OS.Types.Text, function () {
return "";
}, true)
].concat(_super.attributesToDeclare.call(this));
};
ModuleDefinitionRec.init();
return ModuleDefinitionRec;
})(OS.DataTypes.GenericRecord);
Extension_OMLProcessorModel.ModuleDefinitionRec = ModuleDefinitionRec;

});
define("Extension.OMLProcessor.model$ModuleDefinitionRecord", ["exports", "OutSystems/ClientRuntime/Main", "Extension.OMLProcessor.model", "Extension.OMLProcessor.model$ModuleDefinitionRec"], function (exports, OutSystems, Extension_OMLProcessorModel) {
var OS = OutSystems.Internal;
var ModuleDefinitionRecord = (function (_super) {
__extends(ModuleDefinitionRecord, _super);
function ModuleDefinitionRecord(defaults) {
_super.apply(this, arguments);
}
ModuleDefinitionRecord.attributesToDeclare = function () {
return [
this.attr("ModuleDefinition", "moduleDefinitionAttr", "ModuleDefinition", false, false, OS.Types.Record, function () {
return OS.DataTypes.ImmutableBase.getData(new Extension_OMLProcessorModel.ModuleDefinitionRec());
}, true, Extension_OMLProcessorModel.ModuleDefinitionRec)
].concat(_super.attributesToDeclare.call(this));
};
ModuleDefinitionRecord.fromStructure = function (str) {
return new ModuleDefinitionRecord(new ModuleDefinitionRecord.RecordClass({
moduleDefinitionAttr: OS.DataTypes.ImmutableBase.getData(str)
}));
};
ModuleDefinitionRecord._isAnonymousRecord = true;
ModuleDefinitionRecord.UniqueId = "bd562189-324a-f29d-bf3b-9d560dd0738a";
ModuleDefinitionRecord.init();
return ModuleDefinitionRecord;
})(OS.DataTypes.GenericRecord);
Extension_OMLProcessorModel.ModuleDefinitionRecord = ModuleDefinitionRecord;

});
define("Extension.OMLProcessor.model$EntityDefinitionRec", ["exports", "OutSystems/ClientRuntime/Main", "Extension.OMLProcessor.model", "Extension.OMLProcessor.model$ModuleDefinitionRecord"], function (exports, OutSystems, Extension_OMLProcessorModel) {
var OS = OutSystems.Internal;
var EntityDefinitionRec = (function (_super) {
__extends(EntityDefinitionRec, _super);
function EntityDefinitionRec(defaults) {
_super.apply(this, arguments);
}
EntityDefinitionRec.attributesToDeclare = function () {
return [
this.attr("id", "idAttr", "id", true, false, OS.Types.Integer, function () {
return 0;
}, true), 
this.attr("name", "nameAttr", "name", true, false, OS.Types.Text, function () {
return "";
}, true), 
this.attr("isActive", "isActiveAttr", "isActive", true, false, OS.Types.Boolean, function () {
return false;
}, true), 
this.attr("owner", "ownerAttr", "owner", true, false, OS.Types.Record, function () {
return OS.DataTypes.ImmutableBase.getData(new Extension_OMLProcessorModel.ModuleDefinitionRecord());
}, true, Extension_OMLProcessorModel.ModuleDefinitionRecord), 
this.attr("physicalTableName", "physicalTableNameAttr", "physicalTableName", true, false, OS.Types.Text, function () {
return "";
}, true)
].concat(_super.attributesToDeclare.call(this));
};
EntityDefinitionRec.init();
return EntityDefinitionRec;
})(OS.DataTypes.GenericRecord);
Extension_OMLProcessorModel.EntityDefinitionRec = EntityDefinitionRec;

});
define("Extension.OMLProcessor.model$EntityDefinitionRecord", ["exports", "OutSystems/ClientRuntime/Main", "Extension.OMLProcessor.model", "Extension.OMLProcessor.model$EntityDefinitionRec"], function (exports, OutSystems, Extension_OMLProcessorModel) {
var OS = OutSystems.Internal;
var EntityDefinitionRecord = (function (_super) {
__extends(EntityDefinitionRecord, _super);
function EntityDefinitionRecord(defaults) {
_super.apply(this, arguments);
}
EntityDefinitionRecord.attributesToDeclare = function () {
return [
this.attr("EntityDefinition", "entityDefinitionAttr", "EntityDefinition", false, false, OS.Types.Record, function () {
return OS.DataTypes.ImmutableBase.getData(new Extension_OMLProcessorModel.EntityDefinitionRec());
}, true, Extension_OMLProcessorModel.EntityDefinitionRec)
].concat(_super.attributesToDeclare.call(this));
};
EntityDefinitionRecord.fromStructure = function (str) {
return new EntityDefinitionRecord(new EntityDefinitionRecord.RecordClass({
entityDefinitionAttr: OS.DataTypes.ImmutableBase.getData(str)
}));
};
EntityDefinitionRecord._isAnonymousRecord = true;
EntityDefinitionRecord.UniqueId = "554d5d26-0bde-a5a7-c4f6-dcaf5bd7b6d9";
EntityDefinitionRecord.init();
return EntityDefinitionRecord;
})(OS.DataTypes.GenericRecord);
Extension_OMLProcessorModel.EntityDefinitionRecord = EntityDefinitionRecord;

});
define("Extension.OMLProcessor.model$AttributeDefinitionRec", ["exports", "OutSystems/ClientRuntime/Main", "Extension.OMLProcessor.model", "Extension.OMLProcessor.model$EntityDefinitionRecord"], function (exports, OutSystems, Extension_OMLProcessorModel) {
var OS = OutSystems.Internal;
var AttributeDefinitionRec = (function (_super) {
__extends(AttributeDefinitionRec, _super);
function AttributeDefinitionRec(defaults) {
_super.apply(this, arguments);
}
AttributeDefinitionRec.attributesToDeclare = function () {
return [
this.attr("id", "idAttr", "id", true, false, OS.Types.Integer, function () {
return 0;
}, true), 
this.attr("name", "nameAttr", "name", true, false, OS.Types.Text, function () {
return "";
}, true), 
this.attr("isActive", "isActiveAttr", "isActive", true, false, OS.Types.Boolean, function () {
return false;
}, true), 
this.attr("owner", "ownerAttr", "owner", true, false, OS.Types.Record, function () {
return OS.DataTypes.ImmutableBase.getData(new Extension_OMLProcessorModel.EntityDefinitionRecord());
}, true, Extension_OMLProcessorModel.EntityDefinitionRecord)
].concat(_super.attributesToDeclare.call(this));
};
AttributeDefinitionRec.init();
return AttributeDefinitionRec;
})(OS.DataTypes.GenericRecord);
Extension_OMLProcessorModel.AttributeDefinitionRec = AttributeDefinitionRec;

});
define("Extension.OMLProcessor.model$AttributeDefinitionRecord", ["exports", "OutSystems/ClientRuntime/Main", "Extension.OMLProcessor.model", "Extension.OMLProcessor.model$AttributeDefinitionRec"], function (exports, OutSystems, Extension_OMLProcessorModel) {
var OS = OutSystems.Internal;
var AttributeDefinitionRecord = (function (_super) {
__extends(AttributeDefinitionRecord, _super);
function AttributeDefinitionRecord(defaults) {
_super.apply(this, arguments);
}
AttributeDefinitionRecord.attributesToDeclare = function () {
return [
this.attr("AttributeDefinition", "attributeDefinitionAttr", "AttributeDefinition", false, false, OS.Types.Record, function () {
return OS.DataTypes.ImmutableBase.getData(new Extension_OMLProcessorModel.AttributeDefinitionRec());
}, true, Extension_OMLProcessorModel.AttributeDefinitionRec)
].concat(_super.attributesToDeclare.call(this));
};
AttributeDefinitionRecord.fromStructure = function (str) {
return new AttributeDefinitionRecord(new AttributeDefinitionRecord.RecordClass({
attributeDefinitionAttr: OS.DataTypes.ImmutableBase.getData(str)
}));
};
AttributeDefinitionRecord._isAnonymousRecord = true;
AttributeDefinitionRecord.UniqueId = "11dba769-7f74-1c3d-0e1f-739a0593f53d";
AttributeDefinitionRecord.init();
return AttributeDefinitionRecord;
})(OS.DataTypes.GenericRecord);
Extension_OMLProcessorModel.AttributeDefinitionRecord = AttributeDefinitionRecord;

});
define("Extension.OMLProcessor.model$PublicElementRec", ["exports", "OutSystems/ClientRuntime/Main", "Extension.OMLProcessor.model"], function (exports, OutSystems, Extension_OMLProcessorModel) {
var OS = OutSystems.Internal;
var PublicElementRec = (function (_super) {
__extends(PublicElementRec, _super);
function PublicElementRec(defaults) {
_super.apply(this, arguments);
}
PublicElementRec.attributesToDeclare = function () {
return [
this.attr("Name", "nameAttr", "Name", true, false, OS.Types.Text, function () {
return "";
}, true), 
this.attr("Kind", "kindAttr", "Kind", true, false, OS.Types.Text, function () {
return "";
}, true), 
this.attr("Key", "keyAttr", "Key", true, false, OS.Types.Text, function () {
return "";
}, true), 
this.attr("ProducerName", "producerNameAttr", "ProducerName", true, false, OS.Types.Text, function () {
return "";
}, true), 
this.attr("ProducerKind", "producerKindAttr", "ProducerKind", true, false, OS.Types.Text, function () {
return "";
}, true), 
this.attr("ProducerKey", "producerKeyAttr", "ProducerKey", true, false, OS.Types.Text, function () {
return "";
}, true), 
this.attr("PublishedProdVersionId", "publishedProdVersionIdAttr", "PublishedProdVersionId", true, false, OS.Types.Integer, function () {
return 0;
}, true), 
this.attr("CompatibilitySignatureHash", "compatibilitySignatureHashAttr", "CompatibilitySignatureHash", true, false, OS.Types.Text, function () {
return "";
}, true), 
this.attr("FullSignatureHash", "fullSignatureHashAttr", "FullSignatureHash", true, false, OS.Types.Text, function () {
return "";
}, true), 
this.attr("Description", "descriptionAttr", "Description", true, false, OS.Types.Text, function () {
return "";
}, true), 
this.attr("EspaceKind", "espaceKindAttr", "EspaceKind", true, false, OS.Types.Text, function () {
return "";
}, true)
].concat(_super.attributesToDeclare.call(this));
};
PublicElementRec.init();
return PublicElementRec;
})(OS.DataTypes.GenericRecord);
Extension_OMLProcessorModel.PublicElementRec = PublicElementRec;

});
define("Extension.OMLProcessor.model$PublicElementRecord", ["exports", "OutSystems/ClientRuntime/Main", "Extension.OMLProcessor.model", "Extension.OMLProcessor.model$PublicElementRec"], function (exports, OutSystems, Extension_OMLProcessorModel) {
var OS = OutSystems.Internal;
var PublicElementRecord = (function (_super) {
__extends(PublicElementRecord, _super);
function PublicElementRecord(defaults) {
_super.apply(this, arguments);
}
PublicElementRecord.attributesToDeclare = function () {
return [
this.attr("PublicElement", "publicElementAttr", "PublicElement", false, false, OS.Types.Record, function () {
return OS.DataTypes.ImmutableBase.getData(new Extension_OMLProcessorModel.PublicElementRec());
}, true, Extension_OMLProcessorModel.PublicElementRec)
].concat(_super.attributesToDeclare.call(this));
};
PublicElementRecord.fromStructure = function (str) {
return new PublicElementRecord(new PublicElementRecord.RecordClass({
publicElementAttr: OS.DataTypes.ImmutableBase.getData(str)
}));
};
PublicElementRecord._isAnonymousRecord = true;
PublicElementRecord.UniqueId = "11f1bddb-418c-a36a-103f-dceaa7a904e7";
PublicElementRecord.init();
return PublicElementRecord;
})(OS.DataTypes.GenericRecord);
Extension_OMLProcessorModel.PublicElementRecord = PublicElementRecord;

});
define("Extension.OMLProcessor.model$PTA_EspaceInfoRec", ["exports", "OutSystems/ClientRuntime/Main", "Extension.OMLProcessor.model"], function (exports, OutSystems, Extension_OMLProcessorModel) {
var OS = OutSystems.Internal;
var PTA_EspaceInfoRec = (function (_super) {
__extends(PTA_EspaceInfoRec, _super);
function PTA_EspaceInfoRec(defaults) {
_super.apply(this, arguments);
}
PTA_EspaceInfoRec.attributesToDeclare = function () {
return [
this.attr("Name", "nameAttr", "Name", true, false, OS.Types.Text, function () {
return "";
}, true), 
this.attr("Description", "descriptionAttr", "Description", true, false, OS.Types.Text, function () {
return "";
}, true), 
this.attr("Key", "keyAttr", "Key", true, false, OS.Types.Text, function () {
return "";
}, true), 
this.attr("Id", "idAttr", "Id", true, false, OS.Types.Integer, function () {
return 0;
}, true), 
this.attr("VersionId", "versionIdAttr", "VersionId", true, false, OS.Types.Integer, function () {
return 0;
}, true), 
this.attr("ApplicationKey", "applicationKeyAttr", "ApplicationKey", true, false, OS.Types.Text, function () {
return "";
}, true), 
this.attr("ApplicationName", "applicationNameAttr", "ApplicationName", true, false, OS.Types.Text, function () {
return "";
}, true)
].concat(_super.attributesToDeclare.call(this));
};
PTA_EspaceInfoRec.init();
return PTA_EspaceInfoRec;
})(OS.DataTypes.GenericRecord);
Extension_OMLProcessorModel.PTA_EspaceInfoRec = PTA_EspaceInfoRec;

});
define("Extension.OMLProcessor.model$PTA_EspaceInfoRecord", ["exports", "OutSystems/ClientRuntime/Main", "Extension.OMLProcessor.model", "Extension.OMLProcessor.model$PTA_EspaceInfoRec"], function (exports, OutSystems, Extension_OMLProcessorModel) {
var OS = OutSystems.Internal;
var PTA_EspaceInfoRecord = (function (_super) {
__extends(PTA_EspaceInfoRecord, _super);
function PTA_EspaceInfoRecord(defaults) {
_super.apply(this, arguments);
}
PTA_EspaceInfoRecord.attributesToDeclare = function () {
return [
this.attr("PTA_EspaceInfo", "pTA_EspaceInfoAttr", "PTA_EspaceInfo", false, false, OS.Types.Record, function () {
return OS.DataTypes.ImmutableBase.getData(new Extension_OMLProcessorModel.PTA_EspaceInfoRec());
}, true, Extension_OMLProcessorModel.PTA_EspaceInfoRec)
].concat(_super.attributesToDeclare.call(this));
};
PTA_EspaceInfoRecord.fromStructure = function (str) {
return new PTA_EspaceInfoRecord(new PTA_EspaceInfoRecord.RecordClass({
pTA_EspaceInfoAttr: OS.DataTypes.ImmutableBase.getData(str)
}));
};
PTA_EspaceInfoRecord._isAnonymousRecord = true;
PTA_EspaceInfoRecord.UniqueId = "136ef01b-72f4-ae60-c281-2f8c12cc3c6b";
PTA_EspaceInfoRecord.init();
return PTA_EspaceInfoRecord;
})(OS.DataTypes.GenericRecord);
Extension_OMLProcessorModel.PTA_EspaceInfoRecord = PTA_EspaceInfoRecord;

});
define("Extension.OMLProcessor.model$ExtensionEntityConfigRec", ["exports", "OutSystems/ClientRuntime/Main", "Extension.OMLProcessor.model"], function (exports, OutSystems, Extension_OMLProcessorModel) {
var OS = OutSystems.Internal;
var ExtensionEntityConfigRec = (function (_super) {
__extends(ExtensionEntityConfigRec, _super);
function ExtensionEntityConfigRec(defaults) {
_super.apply(this, arguments);
}
ExtensionEntityConfigRec.attributesToDeclare = function () {
return [
this.attr("Key", "keyAttr", "Key", true, false, OS.Types.Text, function () {
return "";
}, true), 
this.attr("EntityId", "entityIdAttr", "EntityId", true, false, OS.Types.Integer, function () {
return 0;
}, true), 
this.attr("Name", "nameAttr", "Name", true, false, OS.Types.Text, function () {
return "";
}, true), 
this.attr("EffectivePhysicalTableName", "effectivePhysicalTableNameAttr", "EffectivePhysicalTableName", true, false, OS.Types.Text, function () {
return "";
}, true), 
this.attr("DefaultPhysicalTableName", "defaultPhysicalTableNameAttr", "DefaultPhysicalTableName", true, false, OS.Types.Text, function () {
return "";
}, true), 
this.attr("IsPhysicalTableNameOverridden", "isPhysicalTableNameOverriddenAttr", "IsPhysicalTableNameOverridden", true, false, OS.Types.Boolean, function () {
return false;
}, true), 
this.attr("LogicalDatabase", "logicalDatabaseAttr", "LogicalDatabase", true, false, OS.Types.Text, function () {
return "";
}, true), 
this.attr("Description", "descriptionAttr", "Description", true, false, OS.Types.Text, function () {
return "";
}, true), 
this.attr("ExtensionKey", "extensionKeyAttr", "ExtensionKey", true, false, OS.Types.Text, function () {
return "";
}, true)
].concat(_super.attributesToDeclare.call(this));
};
ExtensionEntityConfigRec.init();
return ExtensionEntityConfigRec;
})(OS.DataTypes.GenericRecord);
Extension_OMLProcessorModel.ExtensionEntityConfigRec = ExtensionEntityConfigRec;

});
define("Extension.OMLProcessor.model$ExtensionEntityConfigRecord", ["exports", "OutSystems/ClientRuntime/Main", "Extension.OMLProcessor.model", "Extension.OMLProcessor.model$ExtensionEntityConfigRec"], function (exports, OutSystems, Extension_OMLProcessorModel) {
var OS = OutSystems.Internal;
var ExtensionEntityConfigRecord = (function (_super) {
__extends(ExtensionEntityConfigRecord, _super);
function ExtensionEntityConfigRecord(defaults) {
_super.apply(this, arguments);
}
ExtensionEntityConfigRecord.attributesToDeclare = function () {
return [
this.attr("ExtensionEntityConfig", "extensionEntityConfigAttr", "ExtensionEntityConfig", false, false, OS.Types.Record, function () {
return OS.DataTypes.ImmutableBase.getData(new Extension_OMLProcessorModel.ExtensionEntityConfigRec());
}, true, Extension_OMLProcessorModel.ExtensionEntityConfigRec)
].concat(_super.attributesToDeclare.call(this));
};
ExtensionEntityConfigRecord.fromStructure = function (str) {
return new ExtensionEntityConfigRecord(new ExtensionEntityConfigRecord.RecordClass({
extensionEntityConfigAttr: OS.DataTypes.ImmutableBase.getData(str)
}));
};
ExtensionEntityConfigRecord._isAnonymousRecord = true;
ExtensionEntityConfigRecord.UniqueId = "d69d9506-5ae8-dece-1f1c-1abfef62d649";
ExtensionEntityConfigRecord.init();
return ExtensionEntityConfigRecord;
})(OS.DataTypes.GenericRecord);
Extension_OMLProcessorModel.ExtensionEntityConfigRecord = ExtensionEntityConfigRecord;

});
define("Extension.OMLProcessor.model$ExtensionEntityConfigRecordList", ["exports", "OutSystems/ClientRuntime/Main", "Extension.OMLProcessor.model", "Extension.OMLProcessor.model$ExtensionEntityConfigRecord"], function (exports, OutSystems, Extension_OMLProcessorModel) {
var OS = OutSystems.Internal;
var ExtensionEntityConfigRecordList = (function (_super) {
__extends(ExtensionEntityConfigRecordList, _super);
function ExtensionEntityConfigRecordList(defaults) {
_super.apply(this, arguments);
}
ExtensionEntityConfigRecordList.itemType = Extension_OMLProcessorModel.ExtensionEntityConfigRecord;
return ExtensionEntityConfigRecordList;
})(OS.DataTypes.GenericRecordList);
Extension_OMLProcessorModel.ExtensionEntityConfigRecordList = ExtensionEntityConfigRecordList;

});
define("Extension.OMLProcessor.model$FeatureInfoRec", ["exports", "OutSystems/ClientRuntime/Main", "Extension.OMLProcessor.model"], function (exports, OutSystems, Extension_OMLProcessorModel) {
var OS = OutSystems.Internal;
var FeatureInfoRec = (function (_super) {
__extends(FeatureInfoRec, _super);
function FeatureInfoRec(defaults) {
_super.apply(this, arguments);
}
FeatureInfoRec.attributesToDeclare = function () {
return [
this.attr("Id", "idAttr", "Id", true, false, OS.Types.Text, function () {
return "";
}, true), 
this.attr("Name", "nameAttr", "Name", true, false, OS.Types.Text, function () {
return "";
}, true), 
this.attr("Description", "descriptionAttr", "Description", true, false, OS.Types.Text, function () {
return "";
}, true), 
this.attr("Status", "statusAttr", "Status", true, false, OS.Types.Text, function () {
return "";
}, true), 
this.attr("Enabled", "enabledAttr", "Enabled", true, false, OS.Types.Boolean, function () {
return false;
}, true)
].concat(_super.attributesToDeclare.call(this));
};
FeatureInfoRec.init();
return FeatureInfoRec;
})(OS.DataTypes.GenericRecord);
Extension_OMLProcessorModel.FeatureInfoRec = FeatureInfoRec;

});
define("Extension.OMLProcessor.model$FeatureInfoRecord", ["exports", "OutSystems/ClientRuntime/Main", "Extension.OMLProcessor.model", "Extension.OMLProcessor.model$FeatureInfoRec"], function (exports, OutSystems, Extension_OMLProcessorModel) {
var OS = OutSystems.Internal;
var FeatureInfoRecord = (function (_super) {
__extends(FeatureInfoRecord, _super);
function FeatureInfoRecord(defaults) {
_super.apply(this, arguments);
}
FeatureInfoRecord.attributesToDeclare = function () {
return [
this.attr("FeatureInfo", "featureInfoAttr", "FeatureInfo", false, false, OS.Types.Record, function () {
return OS.DataTypes.ImmutableBase.getData(new Extension_OMLProcessorModel.FeatureInfoRec());
}, true, Extension_OMLProcessorModel.FeatureInfoRec)
].concat(_super.attributesToDeclare.call(this));
};
FeatureInfoRecord.fromStructure = function (str) {
return new FeatureInfoRecord(new FeatureInfoRecord.RecordClass({
featureInfoAttr: OS.DataTypes.ImmutableBase.getData(str)
}));
};
FeatureInfoRecord._isAnonymousRecord = true;
FeatureInfoRecord.UniqueId = "14070977-5f36-0d71-d337-9058d89d1543";
FeatureInfoRecord.init();
return FeatureInfoRecord;
})(OS.DataTypes.GenericRecord);
Extension_OMLProcessorModel.FeatureInfoRecord = FeatureInfoRecord;

});
define("Extension.OMLProcessor.model$EspaceInfoRec", ["exports", "OutSystems/ClientRuntime/Main", "Extension.OMLProcessor.model"], function (exports, OutSystems, Extension_OMLProcessorModel) {
var OS = OutSystems.Internal;
var EspaceInfoRec = (function (_super) {
__extends(EspaceInfoRec, _super);
function EspaceInfoRec(defaults) {
_super.apply(this, arguments);
}
EspaceInfoRec.attributesToDeclare = function () {
return [
this.attr("Name", "nameAttr", "Name", false, false, OS.Types.Text, function () {
return "";
}, true), 
this.attr("Key", "keyAttr", "Key", false, false, OS.Types.Text, function () {
return "";
}, true), 
this.attr("status", "statusAttr", "status", false, false, OS.Types.Text, function () {
return "";
}, true)
].concat(_super.attributesToDeclare.call(this));
};
EspaceInfoRec.init();
return EspaceInfoRec;
})(OS.DataTypes.GenericRecord);
Extension_OMLProcessorModel.EspaceInfoRec = EspaceInfoRec;

});
define("Extension.OMLProcessor.model$OmlReportRec", ["exports", "OutSystems/ClientRuntime/Main", "Extension.OMLProcessor.model"], function (exports, OutSystems, Extension_OMLProcessorModel) {
var OS = OutSystems.Internal;
var OmlReportRec = (function (_super) {
__extends(OmlReportRec, _super);
function OmlReportRec(defaults) {
_super.apply(this, arguments);
}
OmlReportRec.attributesToDeclare = function () {
return [
this.attr("Name", "nameAttr", "Name", false, false, OS.Types.Text, function () {
return "";
}, true), 
this.attr("Value", "valueAttr", "Value", false, false, OS.Types.Integer, function () {
return 0;
}, true)
].concat(_super.attributesToDeclare.call(this));
};
OmlReportRec.init();
return OmlReportRec;
})(OS.DataTypes.GenericRecord);
Extension_OMLProcessorModel.OmlReportRec = OmlReportRec;

});
define("Extension.OMLProcessor.model$OmlReportRecord", ["exports", "OutSystems/ClientRuntime/Main", "Extension.OMLProcessor.model", "Extension.OMLProcessor.model$OmlReportRec"], function (exports, OutSystems, Extension_OMLProcessorModel) {
var OS = OutSystems.Internal;
var OmlReportRecord = (function (_super) {
__extends(OmlReportRecord, _super);
function OmlReportRecord(defaults) {
_super.apply(this, arguments);
}
OmlReportRecord.attributesToDeclare = function () {
return [
this.attr("OmlReport", "omlReportAttr", "OmlReport", false, false, OS.Types.Record, function () {
return OS.DataTypes.ImmutableBase.getData(new Extension_OMLProcessorModel.OmlReportRec());
}, true, Extension_OMLProcessorModel.OmlReportRec)
].concat(_super.attributesToDeclare.call(this));
};
OmlReportRecord.fromStructure = function (str) {
return new OmlReportRecord(new OmlReportRecord.RecordClass({
omlReportAttr: OS.DataTypes.ImmutableBase.getData(str)
}));
};
OmlReportRecord._isAnonymousRecord = true;
OmlReportRecord.UniqueId = "4dfe2bce-0d70-2f78-b254-25cc4dd284c9";
OmlReportRecord.init();
return OmlReportRecord;
})(OS.DataTypes.GenericRecord);
Extension_OMLProcessorModel.OmlReportRecord = OmlReportRecord;

});
define("Extension.OMLProcessor.model$OmlReportRecordList", ["exports", "OutSystems/ClientRuntime/Main", "Extension.OMLProcessor.model", "Extension.OMLProcessor.model$OmlReportRecord"], function (exports, OutSystems, Extension_OMLProcessorModel) {
var OS = OutSystems.Internal;
var OmlReportRecordList = (function (_super) {
__extends(OmlReportRecordList, _super);
function OmlReportRecordList(defaults) {
_super.apply(this, arguments);
}
OmlReportRecordList.itemType = Extension_OMLProcessorModel.OmlReportRecord;
return OmlReportRecordList;
})(OS.DataTypes.GenericRecordList);
Extension_OMLProcessorModel.OmlReportRecordList = OmlReportRecordList;

});
define("Extension.OMLProcessor.model$TextRec", ["exports", "OutSystems/ClientRuntime/Main", "Extension.OMLProcessor.model"], function (exports, OutSystems, Extension_OMLProcessorModel) {
var OS = OutSystems.Internal;
var TextRec = (function (_super) {
__extends(TextRec, _super);
function TextRec(defaults) {
_super.apply(this, arguments);
}
TextRec.attributesToDeclare = function () {
return [
this.attr("Text", "textAttr", "Text", false, false, OS.Types.Text, function () {
return "";
}, true)
].concat(_super.attributesToDeclare.call(this));
};
TextRec.fromStructure = function (str) {
return new TextRec(new TextRec.RecordClass({
textAttr: OS.DataTypes.ImmutableBase.getData(str)
}));
};
TextRec.init();
return TextRec;
})(OS.DataTypes.GenericRecord);
Extension_OMLProcessorModel.TextRec = TextRec;

});
define("Extension.OMLProcessor.model$TextRecord", ["exports", "OutSystems/ClientRuntime/Main", "Extension.OMLProcessor.model", "Extension.OMLProcessor.model$TextRec"], function (exports, OutSystems, Extension_OMLProcessorModel) {
var OS = OutSystems.Internal;
var TextRecord = (function (_super) {
__extends(TextRecord, _super);
function TextRecord(defaults) {
_super.apply(this, arguments);
}
TextRecord.attributesToDeclare = function () {
return [
this.attr("Text", "textAttr", "Text", false, false, OS.Types.Record, function () {
return OS.DataTypes.ImmutableBase.getData(new Extension_OMLProcessorModel.TextRec());
}, true, Extension_OMLProcessorModel.TextRec)
].concat(_super.attributesToDeclare.call(this));
};
TextRecord.fromStructure = function (str) {
return new TextRecord(new TextRecord.RecordClass({
textAttr: OS.DataTypes.ImmutableBase.getData(str)
}));
};
TextRecord._isAnonymousRecord = true;
TextRecord.UniqueId = "15c935ab-83b6-b91c-5867-7afdf899091e";
TextRecord.init();
return TextRecord;
})(OS.DataTypes.GenericRecord);
Extension_OMLProcessorModel.TextRecord = TextRecord;

});
define("Extension.OMLProcessor.model$ExtensionEntityInfoRec", ["exports", "OutSystems/ClientRuntime/Main", "Extension.OMLProcessor.model"], function (exports, OutSystems, Extension_OMLProcessorModel) {
var OS = OutSystems.Internal;
var ExtensionEntityInfoRec = (function (_super) {
__extends(ExtensionEntityInfoRec, _super);
function ExtensionEntityInfoRec(defaults) {
_super.apply(this, arguments);
}
ExtensionEntityInfoRec.attributesToDeclare = function () {
return [
this.attr("Name", "nameAttr", "Name", true, false, OS.Types.Text, function () {
return "";
}, true), 
this.attr("Key", "keyAttr", "Key", true, false, OS.Types.Text, function () {
return "";
}, true), 
this.attr("Description", "descriptionAttr", "Description", true, false, OS.Types.Text, function () {
return "";
}, true), 
this.attr("PhysicalTableName", "physicalTableNameAttr", "PhysicalTableName", true, false, OS.Types.Text, function () {
return "";
}, true), 
this.attr("LogicalDatabase", "logicalDatabaseAttr", "LogicalDatabase", true, false, OS.Types.Text, function () {
return "";
}, true)
].concat(_super.attributesToDeclare.call(this));
};
ExtensionEntityInfoRec.init();
return ExtensionEntityInfoRec;
})(OS.DataTypes.GenericRecord);
Extension_OMLProcessorModel.ExtensionEntityInfoRec = ExtensionEntityInfoRec;

});
define("Extension.OMLProcessor.model$ExtensionEntityInfoRecord", ["exports", "OutSystems/ClientRuntime/Main", "Extension.OMLProcessor.model", "Extension.OMLProcessor.model$ExtensionEntityInfoRec"], function (exports, OutSystems, Extension_OMLProcessorModel) {
var OS = OutSystems.Internal;
var ExtensionEntityInfoRecord = (function (_super) {
__extends(ExtensionEntityInfoRecord, _super);
function ExtensionEntityInfoRecord(defaults) {
_super.apply(this, arguments);
}
ExtensionEntityInfoRecord.attributesToDeclare = function () {
return [
this.attr("ExtensionEntityInfo", "extensionEntityInfoAttr", "ExtensionEntityInfo", false, false, OS.Types.Record, function () {
return OS.DataTypes.ImmutableBase.getData(new Extension_OMLProcessorModel.ExtensionEntityInfoRec());
}, true, Extension_OMLProcessorModel.ExtensionEntityInfoRec)
].concat(_super.attributesToDeclare.call(this));
};
ExtensionEntityInfoRecord.fromStructure = function (str) {
return new ExtensionEntityInfoRecord(new ExtensionEntityInfoRecord.RecordClass({
extensionEntityInfoAttr: OS.DataTypes.ImmutableBase.getData(str)
}));
};
ExtensionEntityInfoRecord._isAnonymousRecord = true;
ExtensionEntityInfoRecord.UniqueId = "1632ea52-62be-295e-acf4-de32b0d34e1b";
ExtensionEntityInfoRecord.init();
return ExtensionEntityInfoRecord;
})(OS.DataTypes.GenericRecord);
Extension_OMLProcessorModel.ExtensionEntityInfoRecord = ExtensionEntityInfoRecord;

});
define("Extension.OMLProcessor.model$EntityInfoRecordList", ["exports", "OutSystems/ClientRuntime/Main", "Extension.OMLProcessor.model", "Extension.OMLProcessor.model$EntityInfoRecord"], function (exports, OutSystems, Extension_OMLProcessorModel) {
var OS = OutSystems.Internal;
var EntityInfoRecordList = (function (_super) {
__extends(EntityInfoRecordList, _super);
function EntityInfoRecordList(defaults) {
_super.apply(this, arguments);
}
EntityInfoRecordList.itemType = Extension_OMLProcessorModel.EntityInfoRecord;
return EntityInfoRecordList;
})(OS.DataTypes.GenericRecordList);
Extension_OMLProcessorModel.EntityInfoRecordList = EntityInfoRecordList;

});
define("Extension.OMLProcessor.model$ExtensionInfoRecord", ["exports", "OutSystems/ClientRuntime/Main", "Extension.OMLProcessor.model", "Extension.OMLProcessor.model$ExtensionInfoRec"], function (exports, OutSystems, Extension_OMLProcessorModel) {
var OS = OutSystems.Internal;
var ExtensionInfoRecord = (function (_super) {
__extends(ExtensionInfoRecord, _super);
function ExtensionInfoRecord(defaults) {
_super.apply(this, arguments);
}
ExtensionInfoRecord.attributesToDeclare = function () {
return [
this.attr("ExtensionInfo", "extensionInfoAttr", "ExtensionInfo", false, false, OS.Types.Record, function () {
return OS.DataTypes.ImmutableBase.getData(new Extension_OMLProcessorModel.ExtensionInfoRec());
}, true, Extension_OMLProcessorModel.ExtensionInfoRec)
].concat(_super.attributesToDeclare.call(this));
};
ExtensionInfoRecord.fromStructure = function (str) {
return new ExtensionInfoRecord(new ExtensionInfoRecord.RecordClass({
extensionInfoAttr: OS.DataTypes.ImmutableBase.getData(str)
}));
};
ExtensionInfoRecord._isAnonymousRecord = true;
ExtensionInfoRecord.UniqueId = "b1b84af4-b090-3028-66ff-a47d21d58e78";
ExtensionInfoRecord.init();
return ExtensionInfoRecord;
})(OS.DataTypes.GenericRecord);
Extension_OMLProcessorModel.ExtensionInfoRecord = ExtensionInfoRecord;

});
define("Extension.OMLProcessor.model$ExtensionInfoRecordList", ["exports", "OutSystems/ClientRuntime/Main", "Extension.OMLProcessor.model", "Extension.OMLProcessor.model$ExtensionInfoRecord"], function (exports, OutSystems, Extension_OMLProcessorModel) {
var OS = OutSystems.Internal;
var ExtensionInfoRecordList = (function (_super) {
__extends(ExtensionInfoRecordList, _super);
function ExtensionInfoRecordList(defaults) {
_super.apply(this, arguments);
}
ExtensionInfoRecordList.itemType = Extension_OMLProcessorModel.ExtensionInfoRecord;
return ExtensionInfoRecordList;
})(OS.DataTypes.GenericRecordList);
Extension_OMLProcessorModel.ExtensionInfoRecordList = ExtensionInfoRecordList;

});
define("Extension.OMLProcessor.model$EspaceInfoRecord", ["exports", "OutSystems/ClientRuntime/Main", "Extension.OMLProcessor.model", "Extension.OMLProcessor.model$EspaceInfoRec"], function (exports, OutSystems, Extension_OMLProcessorModel) {
var OS = OutSystems.Internal;
var EspaceInfoRecord = (function (_super) {
__extends(EspaceInfoRecord, _super);
function EspaceInfoRecord(defaults) {
_super.apply(this, arguments);
}
EspaceInfoRecord.attributesToDeclare = function () {
return [
this.attr("EspaceInfo", "espaceInfoAttr", "EspaceInfo", false, false, OS.Types.Record, function () {
return OS.DataTypes.ImmutableBase.getData(new Extension_OMLProcessorModel.EspaceInfoRec());
}, true, Extension_OMLProcessorModel.EspaceInfoRec)
].concat(_super.attributesToDeclare.call(this));
};
EspaceInfoRecord.fromStructure = function (str) {
return new EspaceInfoRecord(new EspaceInfoRecord.RecordClass({
espaceInfoAttr: OS.DataTypes.ImmutableBase.getData(str)
}));
};
EspaceInfoRecord._isAnonymousRecord = true;
EspaceInfoRecord.UniqueId = "d0516316-ab5f-5b74-0a56-8a33ac0be213";
EspaceInfoRecord.init();
return EspaceInfoRecord;
})(OS.DataTypes.GenericRecord);
Extension_OMLProcessorModel.EspaceInfoRecord = EspaceInfoRecord;

});
define("Extension.OMLProcessor.model$EspaceInfoRecordList", ["exports", "OutSystems/ClientRuntime/Main", "Extension.OMLProcessor.model", "Extension.OMLProcessor.model$EspaceInfoRecord"], function (exports, OutSystems, Extension_OMLProcessorModel) {
var OS = OutSystems.Internal;
var EspaceInfoRecordList = (function (_super) {
__extends(EspaceInfoRecordList, _super);
function EspaceInfoRecordList(defaults) {
_super.apply(this, arguments);
}
EspaceInfoRecordList.itemType = Extension_OMLProcessorModel.EspaceInfoRecord;
return EspaceInfoRecordList;
})(OS.DataTypes.GenericRecordList);
Extension_OMLProcessorModel.EspaceInfoRecordList = EspaceInfoRecordList;

});
define("Extension.OMLProcessor.model$ESpaceDetailedInfoRec", ["exports", "OutSystems/ClientRuntime/Main", "Extension.OMLProcessor.model", "Extension.OMLProcessor.model$EntityInfoRecordList", "Extension.OMLProcessor.model$ExtensionInfoRecordList", "Extension.OMLProcessor.model$EspaceInfoRecordList"], function (exports, OutSystems, Extension_OMLProcessorModel) {
var OS = OutSystems.Internal;
var ESpaceDetailedInfoRec = (function (_super) {
__extends(ESpaceDetailedInfoRec, _super);
function ESpaceDetailedInfoRec(defaults) {
_super.apply(this, arguments);
}
ESpaceDetailedInfoRec.attributesToDeclare = function () {
return [
this.attr("eSpace_name", "eSpace_nameAttr", "eSpace_name", true, false, OS.Types.Text, function () {
return "";
}, true), 
this.attr("eSpace_UID", "eSpace_UIDAttr", "eSpace_UID", true, false, OS.Types.Text, function () {
return "";
}, true), 
this.attr("eSpace_system", "eSpace_systemAttr", "eSpace_system", true, false, OS.Types.Boolean, function () {
return false;
}, true), 
this.attr("eSpace_multitenant", "eSpace_multitenantAttr", "eSpace_multitenant", true, false, OS.Types.Boolean, function () {
return false;
}, true), 
this.attr("eSpace_versionHistory", "eSpace_versionHistoryAttr", "eSpace_versionHistory", true, false, OS.Types.BinaryData, function () {
return OS.DataTypes.BinaryData.defaultValue;
}, true), 
this.attr("eSpace_valid", "eSpace_validAttr", "eSpace_valid", true, false, OS.Types.Boolean, function () {
return false;
}, true), 
this.attr("eSpace_UserEntity", "eSpace_UserEntityAttr", "eSpace_UserEntity", true, false, OS.Types.Boolean, function () {
return false;
}, true), 
this.attr("eSpace_ServiceStudioVersion", "eSpace_ServiceStudioVersionAttr", "eSpace_ServiceStudioVersion", true, false, OS.Types.Text, function () {
return "";
}, true), 
this.attr("eSpace_LastUpgradeVersion", "eSpace_LastUpgradeVersionAttr", "eSpace_LastUpgradeVersion", true, false, OS.Types.Text, function () {
return "";
}, true), 
this.attr("eSpace_Description", "eSpace_DescriptionAttr", "eSpace_Description", true, false, OS.Types.Text, function () {
return "";
}, true), 
this.attr("eSpace_Hash", "eSpace_HashAttr", "eSpace_Hash", true, false, OS.Types.Text, function () {
return "";
}, true), 
this.attr("eSpace_LastModified", "eSpace_LastModifiedAttr", "eSpace_LastModified", true, false, OS.Types.DateTime, function () {
return OS.DataTypes.DateTime.defaultValue;
}, true), 
this.attr("ExternalEntities", "externalEntitiesAttr", "ExternalEntities", true, false, OS.Types.RecordList, function () {
return OS.DataTypes.ImmutableBase.getData(new Extension_OMLProcessorModel.EntityInfoRecordList());
}, true, Extension_OMLProcessorModel.EntityInfoRecordList), 
this.attr("Extensions", "extensionsAttr", "Extensions", true, false, OS.Types.RecordList, function () {
return OS.DataTypes.ImmutableBase.getData(new Extension_OMLProcessorModel.ExtensionInfoRecordList());
}, true, Extension_OMLProcessorModel.ExtensionInfoRecordList), 
this.attr("ESpaces", "eSpacesAttr", "ESpaces", true, false, OS.Types.RecordList, function () {
return OS.DataTypes.ImmutableBase.getData(new Extension_OMLProcessorModel.EspaceInfoRecordList());
}, true, Extension_OMLProcessorModel.EspaceInfoRecordList), 
this.attr("eSpace_Is_User_Provider", "eSpace_Is_User_ProviderAttr", "eSpace_Is_User_Provider", true, false, OS.Types.Boolean, function () {
return false;
}, true), 
this.attr("eSpace_User_Provider_Name", "eSpace_User_Provider_NameAttr", "eSpace_User_Provider_Name", true, false, OS.Types.Text, function () {
return "";
}, true), 
this.attr("eSpace_User_Provider_Key", "eSpace_User_Provider_KeyAttr", "eSpace_User_Provider_Key", true, false, OS.Types.Text, function () {
return "";
}, true), 
this.attr("Has_HTTPS", "has_HTTPSAttr", "Has_HTTPS", true, false, OS.Types.Boolean, function () {
return false;
}, true), 
this.attr("Has_HTTPSWithClientCertificates", "has_HTTPSWithClientCertificatesAttr", "Has_HTTPSWithClientCertificates", true, false, OS.Types.Boolean, function () {
return false;
}, true), 
this.attr("Has_IntegratedAuthentication", "has_IntegratedAuthenticationAttr", "Has_IntegratedAuthentication", true, false, OS.Types.Boolean, function () {
return false;
}, true), 
this.attr("Has_SMS", "has_SMSAttr", "Has_SMS", true, false, OS.Types.Boolean, function () {
return false;
}, true), 
this.attr("ActivationCode", "activationCodeAttr", "ActivationCode", true, false, OS.Types.Text, function () {
return "";
}, true), 
this.attr("ProductId", "productIdAttr", "ProductId", true, false, OS.Types.Text, function () {
return "";
}, true), 
this.attr("ProductName", "productNameAttr", "ProductName", true, false, OS.Types.Text, function () {
return "";
}, true), 
this.attr("Has_WebServices", "has_WebServicesAttr", "Has_WebServices", true, false, OS.Types.Boolean, function () {
return false;
}, true), 
this.attr("Has_PublicElements", "has_PublicElementsAttr", "Has_PublicElements", true, false, OS.Types.Boolean, function () {
return false;
}, true), 
this.attr("Default_Theme_Is_Mobile", "default_Theme_Is_MobileAttr", "Default_Theme_Is_Mobile", true, false, OS.Types.Boolean, function () {
return false;
}, true), 
this.attr("Default_Theme_Global_Key", "default_Theme_Global_KeyAttr", "Default_Theme_Global_Key", true, false, OS.Types.Text, function () {
return "";
}, true), 
this.attr("eSpace_DirectUpgradeFrom", "eSpace_DirectUpgradeFromAttr", "eSpace_DirectUpgradeFrom", true, false, OS.Types.Text, function () {
return "";
}, true), 
this.attr("eSpace_GeneralHash", "eSpace_GeneralHashAttr", "eSpace_GeneralHash", true, false, OS.Types.Text, function () {
return "";
}, true), 
this.attr("eSpace_jQueryVersion", "eSpace_jQueryVersionAttr", "eSpace_jQueryVersion", true, false, OS.Types.Text, function () {
return "";
}, true), 
this.attr("eSpace_Icon", "eSpace_IconAttr", "eSpace_Icon", true, false, OS.Types.BinaryData, function () {
return OS.DataTypes.BinaryData.defaultValue;
}, true), 
this.attr("eSpace_Kind", "eSpace_KindAttr", "eSpace_Kind", true, false, OS.Types.Text, function () {
return "";
}, true), 
this.attr("eSpace_Complexity", "eSpace_ComplexityAttr", "eSpace_Complexity", true, false, OS.Types.Integer, function () {
return 0;
}, true), 
this.attr("eSpace_Configurations", "eSpace_ConfigurationsAttr", "eSpace_Configurations", true, false, OS.Types.Text, function () {
return "";
}, true), 
this.attr("eSpace_IsTemplateBundle", "eSpace_IsTemplateBundleAttr", "eSpace_IsTemplateBundle", true, false, OS.Types.Boolean, function () {
return false;
}, true)
].concat(_super.attributesToDeclare.call(this));
};
ESpaceDetailedInfoRec.init();
return ESpaceDetailedInfoRec;
})(OS.DataTypes.GenericRecord);
Extension_OMLProcessorModel.ESpaceDetailedInfoRec = ESpaceDetailedInfoRec;

});
define("Extension.OMLProcessor.model$ESpaceDetailedInfoRecord", ["exports", "OutSystems/ClientRuntime/Main", "Extension.OMLProcessor.model", "Extension.OMLProcessor.model$ESpaceDetailedInfoRec"], function (exports, OutSystems, Extension_OMLProcessorModel) {
var OS = OutSystems.Internal;
var ESpaceDetailedInfoRecord = (function (_super) {
__extends(ESpaceDetailedInfoRecord, _super);
function ESpaceDetailedInfoRecord(defaults) {
_super.apply(this, arguments);
}
ESpaceDetailedInfoRecord.attributesToDeclare = function () {
return [
this.attr("ESpaceDetailedInfo", "eSpaceDetailedInfoAttr", "ESpaceDetailedInfo", false, false, OS.Types.Record, function () {
return OS.DataTypes.ImmutableBase.getData(new Extension_OMLProcessorModel.ESpaceDetailedInfoRec());
}, true, Extension_OMLProcessorModel.ESpaceDetailedInfoRec)
].concat(_super.attributesToDeclare.call(this));
};
ESpaceDetailedInfoRecord.fromStructure = function (str) {
return new ESpaceDetailedInfoRecord(new ESpaceDetailedInfoRecord.RecordClass({
eSpaceDetailedInfoAttr: OS.DataTypes.ImmutableBase.getData(str)
}));
};
ESpaceDetailedInfoRecord._isAnonymousRecord = true;
ESpaceDetailedInfoRecord.UniqueId = "17a228b5-a440-f742-5fda-aa2d8b70c424";
ESpaceDetailedInfoRecord.init();
return ESpaceDetailedInfoRecord;
})(OS.DataTypes.GenericRecord);
Extension_OMLProcessorModel.ESpaceDetailedInfoRecord = ESpaceDetailedInfoRecord;

});
define("Extension.OMLProcessor.model$KeyValuePairRec", ["exports", "OutSystems/ClientRuntime/Main", "Extension.OMLProcessor.model"], function (exports, OutSystems, Extension_OMLProcessorModel) {
var OS = OutSystems.Internal;
var KeyValuePairRec = (function (_super) {
__extends(KeyValuePairRec, _super);
function KeyValuePairRec(defaults) {
_super.apply(this, arguments);
}
KeyValuePairRec.attributesToDeclare = function () {
return [
this.attr("Key", "keyAttr", "Key", true, false, OS.Types.Text, function () {
return "";
}, true), 
this.attr("Value", "valueAttr", "Value", true, false, OS.Types.Text, function () {
return "";
}, true)
].concat(_super.attributesToDeclare.call(this));
};
KeyValuePairRec.init();
return KeyValuePairRec;
})(OS.DataTypes.GenericRecord);
Extension_OMLProcessorModel.KeyValuePairRec = KeyValuePairRec;

});
define("Extension.OMLProcessor.model$KeyValuePairRecord", ["exports", "OutSystems/ClientRuntime/Main", "Extension.OMLProcessor.model", "Extension.OMLProcessor.model$KeyValuePairRec"], function (exports, OutSystems, Extension_OMLProcessorModel) {
var OS = OutSystems.Internal;
var KeyValuePairRecord = (function (_super) {
__extends(KeyValuePairRecord, _super);
function KeyValuePairRecord(defaults) {
_super.apply(this, arguments);
}
KeyValuePairRecord.attributesToDeclare = function () {
return [
this.attr("KeyValuePair", "keyValuePairAttr", "KeyValuePair", false, false, OS.Types.Record, function () {
return OS.DataTypes.ImmutableBase.getData(new Extension_OMLProcessorModel.KeyValuePairRec());
}, true, Extension_OMLProcessorModel.KeyValuePairRec)
].concat(_super.attributesToDeclare.call(this));
};
KeyValuePairRecord.fromStructure = function (str) {
return new KeyValuePairRecord(new KeyValuePairRecord.RecordClass({
keyValuePairAttr: OS.DataTypes.ImmutableBase.getData(str)
}));
};
KeyValuePairRecord._isAnonymousRecord = true;
KeyValuePairRecord.UniqueId = "666cd95e-a129-5a4e-4b3e-57725b785b77";
KeyValuePairRecord.init();
return KeyValuePairRecord;
})(OS.DataTypes.GenericRecord);
Extension_OMLProcessorModel.KeyValuePairRecord = KeyValuePairRecord;

});
define("Extension.OMLProcessor.model$KeyValuePairRecordList", ["exports", "OutSystems/ClientRuntime/Main", "Extension.OMLProcessor.model", "Extension.OMLProcessor.model$KeyValuePairRecord"], function (exports, OutSystems, Extension_OMLProcessorModel) {
var OS = OutSystems.Internal;
var KeyValuePairRecordList = (function (_super) {
__extends(KeyValuePairRecordList, _super);
function KeyValuePairRecordList(defaults) {
_super.apply(this, arguments);
}
KeyValuePairRecordList.itemType = Extension_OMLProcessorModel.KeyValuePairRecord;
return KeyValuePairRecordList;
})(OS.DataTypes.GenericRecordList);
Extension_OMLProcessorModel.KeyValuePairRecordList = KeyValuePairRecordList;

});
define("Extension.OMLProcessor.model$HubNodeStatusRec", ["exports", "OutSystems/ClientRuntime/Main", "Extension.OMLProcessor.model"], function (exports, OutSystems, Extension_OMLProcessorModel) {
var OS = OutSystems.Internal;
var HubNodeStatusRec = (function (_super) {
__extends(HubNodeStatusRec, _super);
function HubNodeStatusRec(defaults) {
_super.apply(this, arguments);
}
HubNodeStatusRec.attributesToDeclare = function () {
return [
this.attr("Name", "nameAttr", "Name", true, false, OS.Types.Text, function () {
return "";
}, true), 
this.attr("IP", "iPAttr", "IP", true, false, OS.Types.Text, function () {
return "";
}, true), 
this.attr("IISStatus", "iISStatusAttr", "IISStatus", true, false, OS.Types.Boolean, function () {
return false;
}, true), 
this.attr("SchedulerStatus", "schedulerStatusAttr", "SchedulerStatus", true, false, OS.Types.Boolean, function () {
return false;
}, true), 
this.attr("SchedulerStatusDescription", "schedulerStatusDescriptionAttr", "SchedulerStatusDescription", false, false, OS.Types.Text, function () {
return "";
}, true), 
this.attr("DeployStatus", "deployStatusAttr", "DeployStatus", true, false, OS.Types.Boolean, function () {
return false;
}, true), 
this.attr("DeployStatusDescription", "deployStatusDescriptionAttr", "DeployStatusDescription", false, false, OS.Types.Text, function () {
return "";
}, true), 
this.attr("LogStatus", "logStatusAttr", "LogStatus", true, false, OS.Types.Boolean, function () {
return false;
}, true), 
this.attr("LogStatusDescription", "logStatusDescriptionAttr", "LogStatusDescription", false, false, OS.Types.Text, function () {
return "";
}, true), 
this.attr("SMSConnectorStatus", "sMSConnectorStatusAttr", "SMSConnectorStatus", true, false, OS.Types.Boolean, function () {
return false;
}, true), 
this.attr("SMSConnectorStatusDesc", "sMSConnectorStatusDescAttr", "SMSConnectorStatusDesc", false, false, OS.Types.Text, function () {
return "";
}, true)
].concat(_super.attributesToDeclare.call(this));
};
HubNodeStatusRec.init();
return HubNodeStatusRec;
})(OS.DataTypes.GenericRecord);
Extension_OMLProcessorModel.HubNodeStatusRec = HubNodeStatusRec;

});
define("Extension.OMLProcessor.model$HubNodeStatusRecord", ["exports", "OutSystems/ClientRuntime/Main", "Extension.OMLProcessor.model", "Extension.OMLProcessor.model$HubNodeStatusRec"], function (exports, OutSystems, Extension_OMLProcessorModel) {
var OS = OutSystems.Internal;
var HubNodeStatusRecord = (function (_super) {
__extends(HubNodeStatusRecord, _super);
function HubNodeStatusRecord(defaults) {
_super.apply(this, arguments);
}
HubNodeStatusRecord.attributesToDeclare = function () {
return [
this.attr("HubNodeStatus", "hubNodeStatusAttr", "HubNodeStatus", false, false, OS.Types.Record, function () {
return OS.DataTypes.ImmutableBase.getData(new Extension_OMLProcessorModel.HubNodeStatusRec());
}, true, Extension_OMLProcessorModel.HubNodeStatusRec)
].concat(_super.attributesToDeclare.call(this));
};
HubNodeStatusRecord.fromStructure = function (str) {
return new HubNodeStatusRecord(new HubNodeStatusRecord.RecordClass({
hubNodeStatusAttr: OS.DataTypes.ImmutableBase.getData(str)
}));
};
HubNodeStatusRecord._isAnonymousRecord = true;
HubNodeStatusRecord.UniqueId = "1868594c-cb5b-5694-b32e-bac3d3a5fd33";
HubNodeStatusRecord.init();
return HubNodeStatusRecord;
})(OS.DataTypes.GenericRecord);
Extension_OMLProcessorModel.HubNodeStatusRecord = HubNodeStatusRecord;

});
define("Extension.OMLProcessor.model$ModuleVersionFeatureRecord", ["exports", "OutSystems/ClientRuntime/Main", "Extension.OMLProcessor.model", "Extension.OMLProcessor.model$ModuleVersionFeatureRec"], function (exports, OutSystems, Extension_OMLProcessorModel) {
var OS = OutSystems.Internal;
var ModuleVersionFeatureRecord = (function (_super) {
__extends(ModuleVersionFeatureRecord, _super);
function ModuleVersionFeatureRecord(defaults) {
_super.apply(this, arguments);
}
ModuleVersionFeatureRecord.attributesToDeclare = function () {
return [
this.attr("ModuleVersionFeature", "moduleVersionFeatureAttr", "ModuleVersionFeature", false, false, OS.Types.Record, function () {
return OS.DataTypes.ImmutableBase.getData(new Extension_OMLProcessorModel.ModuleVersionFeatureRec());
}, true, Extension_OMLProcessorModel.ModuleVersionFeatureRec)
].concat(_super.attributesToDeclare.call(this));
};
ModuleVersionFeatureRecord.fromStructure = function (str) {
return new ModuleVersionFeatureRecord(new ModuleVersionFeatureRecord.RecordClass({
moduleVersionFeatureAttr: OS.DataTypes.ImmutableBase.getData(str)
}));
};
ModuleVersionFeatureRecord._isAnonymousRecord = true;
ModuleVersionFeatureRecord.UniqueId = "19cba8c1-9673-5734-2d9f-f88eb53ad0a3";
ModuleVersionFeatureRecord.init();
return ModuleVersionFeatureRecord;
})(OS.DataTypes.GenericRecord);
Extension_OMLProcessorModel.ModuleVersionFeatureRecord = ModuleVersionFeatureRecord;

});
define("Extension.OMLProcessor.model$ThreadStatusRec", ["exports", "OutSystems/ClientRuntime/Main", "Extension.OMLProcessor.model"], function (exports, OutSystems, Extension_OMLProcessorModel) {
var OS = OutSystems.Internal;
var ThreadStatusRec = (function (_super) {
__extends(ThreadStatusRec, _super);
function ThreadStatusRec(defaults) {
_super.apply(this, arguments);
}
ThreadStatusRec.attributesToDeclare = function () {
return [
this.attr("Name", "nameAttr", "Name", false, false, OS.Types.Text, function () {
return "";
}, true), 
this.attr("Status", "statusAttr", "Status", false, false, OS.Types.Text, function () {
return "";
}, true), 
this.attr("SecondsInStatus", "secondsInStatusAttr", "SecondsInStatus", false, false, OS.Types.Integer, function () {
return 0;
}, true), 
this.attr("Detail", "detailAttr", "Detail", false, false, OS.Types.Text, function () {
return "";
}, true), 
this.attr("MaxSecondsInStatus", "maxSecondsInStatusAttr", "MaxSecondsInStatus", false, false, OS.Types.Integer, function () {
return 0;
}, true)
].concat(_super.attributesToDeclare.call(this));
};
ThreadStatusRec.init();
return ThreadStatusRec;
})(OS.DataTypes.GenericRecord);
Extension_OMLProcessorModel.ThreadStatusRec = ThreadStatusRec;

});
define("Extension.OMLProcessor.model$ThreadStatusRecord", ["exports", "OutSystems/ClientRuntime/Main", "Extension.OMLProcessor.model", "Extension.OMLProcessor.model$ThreadStatusRec"], function (exports, OutSystems, Extension_OMLProcessorModel) {
var OS = OutSystems.Internal;
var ThreadStatusRecord = (function (_super) {
__extends(ThreadStatusRecord, _super);
function ThreadStatusRecord(defaults) {
_super.apply(this, arguments);
}
ThreadStatusRecord.attributesToDeclare = function () {
return [
this.attr("ThreadStatus", "threadStatusAttr", "ThreadStatus", false, false, OS.Types.Record, function () {
return OS.DataTypes.ImmutableBase.getData(new Extension_OMLProcessorModel.ThreadStatusRec());
}, true, Extension_OMLProcessorModel.ThreadStatusRec)
].concat(_super.attributesToDeclare.call(this));
};
ThreadStatusRecord.fromStructure = function (str) {
return new ThreadStatusRecord(new ThreadStatusRecord.RecordClass({
threadStatusAttr: OS.DataTypes.ImmutableBase.getData(str)
}));
};
ThreadStatusRecord._isAnonymousRecord = true;
ThreadStatusRecord.UniqueId = "99ac8159-53a0-d221-6853-1cf4572428f4";
ThreadStatusRecord.init();
return ThreadStatusRecord;
})(OS.DataTypes.GenericRecord);
Extension_OMLProcessorModel.ThreadStatusRecord = ThreadStatusRecord;

});
define("Extension.OMLProcessor.model$ThreadStatusRecordList", ["exports", "OutSystems/ClientRuntime/Main", "Extension.OMLProcessor.model", "Extension.OMLProcessor.model$ThreadStatusRecord"], function (exports, OutSystems, Extension_OMLProcessorModel) {
var OS = OutSystems.Internal;
var ThreadStatusRecordList = (function (_super) {
__extends(ThreadStatusRecordList, _super);
function ThreadStatusRecordList(defaults) {
_super.apply(this, arguments);
}
ThreadStatusRecordList.itemType = Extension_OMLProcessorModel.ThreadStatusRecord;
return ThreadStatusRecordList;
})(OS.DataTypes.GenericRecordList);
Extension_OMLProcessorModel.ThreadStatusRecordList = ThreadStatusRecordList;

});
define("Extension.OMLProcessor.model$UserSecurityGrantsRec", ["exports", "OutSystems/ClientRuntime/Main", "Extension.OMLProcessor.model"], function (exports, OutSystems, Extension_OMLProcessorModel) {
var OS = OutSystems.Internal;
var UserSecurityGrantsRec = (function (_super) {
__extends(UserSecurityGrantsRec, _super);
function UserSecurityGrantsRec(defaults) {
_super.apply(this, arguments);
}
UserSecurityGrantsRec.attributesToDeclare = function () {
return [
this.attr("UserId", "userIdAttr", "UserId", true, false, OS.Types.Integer, function () {
return 0;
}, true), 
this.attr("AllowSolutions", "allowSolutionsAttr", "AllowSolutions", false, false, OS.Types.Boolean, function () {
return false;
}, true), 
this.attr("AllowNewEspace", "allowNewEspaceAttr", "AllowNewEspace", false, false, OS.Types.Boolean, function () {
return false;
}, true), 
this.attr("AllowExtensions", "allowExtensionsAttr", "AllowExtensions", false, false, OS.Types.Boolean, function () {
return false;
}, true), 
this.attr("AllowExternalEntities", "allowExternalEntitiesAttr", "AllowExternalEntities", false, false, OS.Types.Boolean, function () {
return false;
}, true), 
this.attr("AllowSystemEntities", "allowSystemEntitiesAttr", "AllowSystemEntities", false, false, OS.Types.Boolean, function () {
return false;
}, true), 
this.attr("AllowDBConnections", "allowDBConnectionsAttr", "AllowDBConnections", false, false, OS.Types.Boolean, function () {
return false;
}, true), 
this.attr("AllowProcessManagement", "allowProcessManagementAttr", "AllowProcessManagement", false, false, OS.Types.Boolean, function () {
return false;
}, true), 
this.attr("AllowNewDBCatalog", "allowNewDBCatalogAttr", "AllowNewDBCatalog", false, false, OS.Types.Boolean, function () {
return false;
}, true), 
this.attr("AllowSEOManagement", "allowSEOManagementAttr", "AllowSEOManagement", false, false, OS.Types.Boolean, function () {
return false;
}, true), 
this.attr("ViewOnlineMonitoring", "viewOnlineMonitoringAttr", "ViewOnlineMonitoring", false, false, OS.Types.Boolean, function () {
return false;
}, true), 
this.attr("ViewLicensing", "viewLicensingAttr", "ViewLicensing", false, false, OS.Types.Boolean, function () {
return false;
}, true), 
this.attr("SolutionsSecurity", "solutionsSecurityAttr", "SolutionsSecurity", false, false, OS.Types.Integer, function () {
return 0;
}, true), 
this.attr("EspacesSecurity", "espacesSecurityAttr", "EspacesSecurity", false, false, OS.Types.Integer, function () {
return 0;
}, true), 
this.attr("ExtensionsSecurity", "extensionsSecurityAttr", "ExtensionsSecurity", false, false, OS.Types.Integer, function () {
return 0;
}, true), 
this.attr("UsersSecurity", "usersSecurityAttr", "UsersSecurity", false, false, OS.Types.Integer, function () {
return 0;
}, true), 
this.attr("PhonesSecurity", "phonesSecurityAttr", "PhonesSecurity", false, false, OS.Types.Integer, function () {
return 0;
}, true), 
this.attr("DBConnectionsSecurity", "dBConnectionsSecurityAttr", "DBConnectionsSecurity", false, false, OS.Types.Integer, function () {
return 0;
}, true), 
this.attr("CertificatesSecurity", "certificatesSecurityAttr", "CertificatesSecurity", false, false, OS.Types.Integer, function () {
return 0;
}, true), 
this.attr("HubConfigurationSecurity", "hubConfigurationSecurityAttr", "HubConfigurationSecurity", false, false, OS.Types.Integer, function () {
return 0;
}, true), 
this.attr("ReportsSecurity", "reportsSecurityAttr", "ReportsSecurity", false, false, OS.Types.Integer, function () {
return 0;
}, true), 
this.attr("DBCatalogSecurity", "dBCatalogSecurityAttr", "DBCatalogSecurity", false, false, OS.Types.Integer, function () {
return 0;
}, true), 
this.attr("ApplicationsSecurity", "applicationsSecurityAttr", "ApplicationsSecurity", false, false, OS.Types.Integer, function () {
return 0;
}, true)
].concat(_super.attributesToDeclare.call(this));
};
UserSecurityGrantsRec.init();
return UserSecurityGrantsRec;
})(OS.DataTypes.GenericRecord);
Extension_OMLProcessorModel.UserSecurityGrantsRec = UserSecurityGrantsRec;

});
define("Extension.OMLProcessor.model$UserSecurityGrantsRecord", ["exports", "OutSystems/ClientRuntime/Main", "Extension.OMLProcessor.model", "Extension.OMLProcessor.model$UserSecurityGrantsRec"], function (exports, OutSystems, Extension_OMLProcessorModel) {
var OS = OutSystems.Internal;
var UserSecurityGrantsRecord = (function (_super) {
__extends(UserSecurityGrantsRecord, _super);
function UserSecurityGrantsRecord(defaults) {
_super.apply(this, arguments);
}
UserSecurityGrantsRecord.attributesToDeclare = function () {
return [
this.attr("UserSecurityGrants", "userSecurityGrantsAttr", "UserSecurityGrants", false, false, OS.Types.Record, function () {
return OS.DataTypes.ImmutableBase.getData(new Extension_OMLProcessorModel.UserSecurityGrantsRec());
}, true, Extension_OMLProcessorModel.UserSecurityGrantsRec)
].concat(_super.attributesToDeclare.call(this));
};
UserSecurityGrantsRecord.fromStructure = function (str) {
return new UserSecurityGrantsRecord(new UserSecurityGrantsRecord.RecordClass({
userSecurityGrantsAttr: OS.DataTypes.ImmutableBase.getData(str)
}));
};
UserSecurityGrantsRecord._isAnonymousRecord = true;
UserSecurityGrantsRecord.UniqueId = "f38ec777-0910-20f9-656c-349af4f323c1";
UserSecurityGrantsRecord.init();
return UserSecurityGrantsRecord;
})(OS.DataTypes.GenericRecord);
Extension_OMLProcessorModel.UserSecurityGrantsRecord = UserSecurityGrantsRecord;

});
define("Extension.OMLProcessor.model$UserSecurityGrantsRecordList", ["exports", "OutSystems/ClientRuntime/Main", "Extension.OMLProcessor.model", "Extension.OMLProcessor.model$UserSecurityGrantsRecord"], function (exports, OutSystems, Extension_OMLProcessorModel) {
var OS = OutSystems.Internal;
var UserSecurityGrantsRecordList = (function (_super) {
__extends(UserSecurityGrantsRecordList, _super);
function UserSecurityGrantsRecordList(defaults) {
_super.apply(this, arguments);
}
UserSecurityGrantsRecordList.itemType = Extension_OMLProcessorModel.UserSecurityGrantsRecord;
return UserSecurityGrantsRecordList;
})(OS.DataTypes.GenericRecordList);
Extension_OMLProcessorModel.UserSecurityGrantsRecordList = UserSecurityGrantsRecordList;

});
define("Extension.OMLProcessor.model$DBCatalogRecord", ["exports", "OutSystems/ClientRuntime/Main", "Extension.OMLProcessor.model", "Extension.OMLProcessor.model$DBCatalogRec"], function (exports, OutSystems, Extension_OMLProcessorModel) {
var OS = OutSystems.Internal;
var DBCatalogRecord = (function (_super) {
__extends(DBCatalogRecord, _super);
function DBCatalogRecord(defaults) {
_super.apply(this, arguments);
}
DBCatalogRecord.attributesToDeclare = function () {
return [
this.attr("DBCatalog", "dBCatalogAttr", "DBCatalog", false, false, OS.Types.Record, function () {
return OS.DataTypes.ImmutableBase.getData(new Extension_OMLProcessorModel.DBCatalogRec());
}, true, Extension_OMLProcessorModel.DBCatalogRec)
].concat(_super.attributesToDeclare.call(this));
};
DBCatalogRecord.fromStructure = function (str) {
return new DBCatalogRecord(new DBCatalogRecord.RecordClass({
dBCatalogAttr: OS.DataTypes.ImmutableBase.getData(str)
}));
};
DBCatalogRecord._isAnonymousRecord = true;
DBCatalogRecord.UniqueId = "648696e2-bc3f-e882-a014-3ac043e6f3f8";
DBCatalogRecord.init();
return DBCatalogRecord;
})(OS.DataTypes.GenericRecord);
Extension_OMLProcessorModel.DBCatalogRecord = DBCatalogRecord;

});
define("Extension.OMLProcessor.model$DBCatalogRecordList", ["exports", "OutSystems/ClientRuntime/Main", "Extension.OMLProcessor.model", "Extension.OMLProcessor.model$DBCatalogRecord"], function (exports, OutSystems, Extension_OMLProcessorModel) {
var OS = OutSystems.Internal;
var DBCatalogRecordList = (function (_super) {
__extends(DBCatalogRecordList, _super);
function DBCatalogRecordList(defaults) {
_super.apply(this, arguments);
}
DBCatalogRecordList.itemType = Extension_OMLProcessorModel.DBCatalogRecord;
return DBCatalogRecordList;
})(OS.DataTypes.GenericRecordList);
Extension_OMLProcessorModel.DBCatalogRecordList = DBCatalogRecordList;

});
define("Extension.OMLProcessor.model$EntityRecordRec", ["exports", "OutSystems/ClientRuntime/Main", "Extension.OMLProcessor.model"], function (exports, OutSystems, Extension_OMLProcessorModel) {
var OS = OutSystems.Internal;
var EntityRecordRec = (function (_super) {
__extends(EntityRecordRec, _super);
function EntityRecordRec(defaults) {
_super.apply(this, arguments);
}
EntityRecordRec.attributesToDeclare = function () {
return [
this.attr("Id", "idAttr", "Id", true, false, OS.Types.Integer, function () {
return 0;
}, true), 
this.attr("Data_Id", "data_IdAttr", "Data_Id", true, false, OS.Types.Text, function () {
return "";
}, true), 
this.attr("Name", "nameAttr", "Name", true, false, OS.Types.Text, function () {
return "";
}, true), 
this.attr("SS_Key", "sS_KeyAttr", "SS_Key", true, false, OS.Types.Text, function () {
return "";
}, true), 
this.attr("Entity_SS_Key", "entity_SS_KeyAttr", "Entity_SS_Key", true, false, OS.Types.Text, function () {
return "";
}, true), 
this.attr("Espace_Id", "espace_IdAttr", "Espace_Id", true, false, OS.Types.Integer, function () {
return 0;
}, true), 
this.attr("Is_Active", "is_ActiveAttr", "Is_Active", true, false, OS.Types.Boolean, function () {
return false;
}, true)
].concat(_super.attributesToDeclare.call(this));
};
EntityRecordRec.init();
return EntityRecordRec;
})(OS.DataTypes.GenericRecord);
Extension_OMLProcessorModel.EntityRecordRec = EntityRecordRec;

});
define("Extension.OMLProcessor.model$DALDBConfigEnumValueRec", ["exports", "OutSystems/ClientRuntime/Main", "Extension.OMLProcessor.model"], function (exports, OutSystems, Extension_OMLProcessorModel) {
var OS = OutSystems.Internal;
var DALDBConfigEnumValueRec = (function (_super) {
__extends(DALDBConfigEnumValueRec, _super);
function DALDBConfigEnumValueRec(defaults) {
_super.apply(this, arguments);
}
DALDBConfigEnumValueRec.attributesToDeclare = function () {
return [
this.attr("Value", "valueAttr", "Value", true, false, OS.Types.Text, function () {
return "";
}, true)
].concat(_super.attributesToDeclare.call(this));
};
DALDBConfigEnumValueRec.fromStructure = function (str) {
return new DALDBConfigEnumValueRec(new DALDBConfigEnumValueRec.RecordClass({
valueAttr: OS.DataTypes.ImmutableBase.getData(str)
}));
};
DALDBConfigEnumValueRec.init();
return DALDBConfigEnumValueRec;
})(OS.DataTypes.GenericRecord);
Extension_OMLProcessorModel.DALDBConfigEnumValueRec = DALDBConfigEnumValueRec;

});
define("Extension.OMLProcessor.model$JWTClaimsRec", ["exports", "OutSystems/ClientRuntime/Main", "Extension.OMLProcessor.model"], function (exports, OutSystems, Extension_OMLProcessorModel) {
var OS = OutSystems.Internal;
var JWTClaimsRec = (function (_super) {
__extends(JWTClaimsRec, _super);
function JWTClaimsRec(defaults) {
_super.apply(this, arguments);
}
JWTClaimsRec.attributesToDeclare = function () {
return [
this.attr("Issuer", "issuerAttr", "Issuer", true, false, OS.Types.Text, function () {
return "";
}, true), 
this.attr("Subject", "subjectAttr", "Subject", true, false, OS.Types.Text, function () {
return "";
}, true), 
this.attr("Audience", "audienceAttr", "Audience", true, false, OS.Types.Text, function () {
return "";
}, true), 
this.attr("IssuedAt", "issuedAtAttr", "IssuedAt", true, false, OS.Types.LongInteger, function () {
return OS.DataTypes.LongInteger.defaultValue;
}, true), 
this.attr("JWTID", "jWTIDAttr", "JWTID", true, false, OS.Types.Text, function () {
return "";
}, true)
].concat(_super.attributesToDeclare.call(this));
};
JWTClaimsRec.init();
return JWTClaimsRec;
})(OS.DataTypes.GenericRecord);
Extension_OMLProcessorModel.JWTClaimsRec = JWTClaimsRec;

});
define("Extension.OMLProcessor.model$JWTClaimsRecord", ["exports", "OutSystems/ClientRuntime/Main", "Extension.OMLProcessor.model", "Extension.OMLProcessor.model$JWTClaimsRec"], function (exports, OutSystems, Extension_OMLProcessorModel) {
var OS = OutSystems.Internal;
var JWTClaimsRecord = (function (_super) {
__extends(JWTClaimsRecord, _super);
function JWTClaimsRecord(defaults) {
_super.apply(this, arguments);
}
JWTClaimsRecord.attributesToDeclare = function () {
return [
this.attr("JWTClaims", "jWTClaimsAttr", "JWTClaims", false, false, OS.Types.Record, function () {
return OS.DataTypes.ImmutableBase.getData(new Extension_OMLProcessorModel.JWTClaimsRec());
}, true, Extension_OMLProcessorModel.JWTClaimsRec)
].concat(_super.attributesToDeclare.call(this));
};
JWTClaimsRecord.fromStructure = function (str) {
return new JWTClaimsRecord(new JWTClaimsRecord.RecordClass({
jWTClaimsAttr: OS.DataTypes.ImmutableBase.getData(str)
}));
};
JWTClaimsRecord._isAnonymousRecord = true;
JWTClaimsRecord.UniqueId = "5c3a2039-61ad-c7da-5089-c3ce428f005d";
JWTClaimsRecord.init();
return JWTClaimsRecord;
})(OS.DataTypes.GenericRecord);
Extension_OMLProcessorModel.JWTClaimsRecord = JWTClaimsRecord;

});
define("Extension.OMLProcessor.model$JWTClaimsRecordList", ["exports", "OutSystems/ClientRuntime/Main", "Extension.OMLProcessor.model", "Extension.OMLProcessor.model$JWTClaimsRecord"], function (exports, OutSystems, Extension_OMLProcessorModel) {
var OS = OutSystems.Internal;
var JWTClaimsRecordList = (function (_super) {
__extends(JWTClaimsRecordList, _super);
function JWTClaimsRecordList(defaults) {
_super.apply(this, arguments);
}
JWTClaimsRecordList.itemType = Extension_OMLProcessorModel.JWTClaimsRecord;
return JWTClaimsRecordList;
})(OS.DataTypes.GenericRecordList);
Extension_OMLProcessorModel.JWTClaimsRecordList = JWTClaimsRecordList;

});
define("Extension.OMLProcessor.model$ReferenceImageRec", ["exports", "OutSystems/ClientRuntime/Main", "Extension.OMLProcessor.model"], function (exports, OutSystems, Extension_OMLProcessorModel) {
var OS = OutSystems.Internal;
var ReferenceImageRec = (function (_super) {
__extends(ReferenceImageRec, _super);
function ReferenceImageRec(defaults) {
_super.apply(this, arguments);
}
ReferenceImageRec.attributesToDeclare = function () {
return [
this.attr("Name", "nameAttr", "Name", true, false, OS.Types.Text, function () {
return "";
}, true), 
this.attr("Image", "imageAttr", "Image", true, false, OS.Types.BinaryData, function () {
return OS.DataTypes.BinaryData.defaultValue;
}, true)
].concat(_super.attributesToDeclare.call(this));
};
ReferenceImageRec.init();
return ReferenceImageRec;
})(OS.DataTypes.GenericRecord);
Extension_OMLProcessorModel.ReferenceImageRec = ReferenceImageRec;

});
define("Extension.OMLProcessor.model$ReferenceImageRecord", ["exports", "OutSystems/ClientRuntime/Main", "Extension.OMLProcessor.model", "Extension.OMLProcessor.model$ReferenceImageRec"], function (exports, OutSystems, Extension_OMLProcessorModel) {
var OS = OutSystems.Internal;
var ReferenceImageRecord = (function (_super) {
__extends(ReferenceImageRecord, _super);
function ReferenceImageRecord(defaults) {
_super.apply(this, arguments);
}
ReferenceImageRecord.attributesToDeclare = function () {
return [
this.attr("ReferenceImage", "referenceImageAttr", "ReferenceImage", false, false, OS.Types.Record, function () {
return OS.DataTypes.ImmutableBase.getData(new Extension_OMLProcessorModel.ReferenceImageRec());
}, true, Extension_OMLProcessorModel.ReferenceImageRec)
].concat(_super.attributesToDeclare.call(this));
};
ReferenceImageRecord.fromStructure = function (str) {
return new ReferenceImageRecord(new ReferenceImageRecord.RecordClass({
referenceImageAttr: OS.DataTypes.ImmutableBase.getData(str)
}));
};
ReferenceImageRecord._isAnonymousRecord = true;
ReferenceImageRecord.UniqueId = "22c191c5-1744-e4ac-7b3f-dba2add8e5b5";
ReferenceImageRecord.init();
return ReferenceImageRecord;
})(OS.DataTypes.GenericRecord);
Extension_OMLProcessorModel.ReferenceImageRecord = ReferenceImageRecord;

});
define("Extension.OMLProcessor.model$NativePluginConfigurationRec", ["exports", "OutSystems/ClientRuntime/Main", "Extension.OMLProcessor.model"], function (exports, OutSystems, Extension_OMLProcessorModel) {
var OS = OutSystems.Internal;
var NativePluginConfigurationRec = (function (_super) {
__extends(NativePluginConfigurationRec, _super);
function NativePluginConfigurationRec(defaults) {
_super.apply(this, arguments);
}
NativePluginConfigurationRec.attributesToDeclare = function () {
return [
this.attr("ApplicationKey", "applicationKeyAttr", "ApplicationKey", true, false, OS.Types.Text, function () {
return "";
}, true), 
this.attr("PluginModuleKey", "pluginModuleKeyAttr", "PluginModuleKey", true, false, OS.Types.Text, function () {
return "";
}, true)
].concat(_super.attributesToDeclare.call(this));
};
NativePluginConfigurationRec.init();
return NativePluginConfigurationRec;
})(OS.DataTypes.GenericRecord);
Extension_OMLProcessorModel.NativePluginConfigurationRec = NativePluginConfigurationRec;

});
define("Extension.OMLProcessor.model$DeploymentZone_AzureContainerServiceRec", ["exports", "OutSystems/ClientRuntime/Main", "Extension.OMLProcessor.model"], function (exports, OutSystems, Extension_OMLProcessorModel) {
var OS = OutSystems.Internal;
var DeploymentZone_AzureContainerServiceRec = (function (_super) {
__extends(DeploymentZone_AzureContainerServiceRec, _super);
function DeploymentZone_AzureContainerServiceRec(defaults) {
_super.apply(this, arguments);
}
DeploymentZone_AzureContainerServiceRec.attributesToDeclare = function () {
return [
this.attr("ResultPath", "resultPathAttr", "ResultPath", true, false, OS.Types.Text, function () {
return "";
}, true), 
this.attr("TargetPath", "targetPathAttr", "TargetPath", true, false, OS.Types.Text, function () {
return "";
}, true), 
this.attr("FromImageReference", "fromImageReferenceAttr", "FromImageReference", true, false, OS.Types.Text, function () {
return "";
}, true), 
this.attr("ConfigPath", "configPathAttr", "ConfigPath", true, false, OS.Types.Text, function () {
return "";
}, true), 
this.attr("OnPrepareDeployDone", "onPrepareDeployDoneAttr", "OnPrepareDeployDone", false, false, OS.Types.Text, function () {
return "";
}, true), 
this.attr("OnDeployDone", "onDeployDoneAttr", "OnDeployDone", false, false, OS.Types.Text, function () {
return "";
}, true), 
this.attr("OnUndeploy", "onUndeployAttr", "OnUndeploy", false, false, OS.Types.Text, function () {
return "";
}, true), 
this.attr("OnUpdateConfigurations", "onUpdateConfigurationsAttr", "OnUpdateConfigurations", false, false, OS.Types.Text, function () {
return "";
}, true), 
this.attr("ManualDeployTimeoutInMinutes", "manualDeployTimeoutInMinutesAttr", "ManualDeployTimeoutInMinutes", false, false, OS.Types.Integer, function () {
return 0;
}, true), 
this.attr("AutomaticDeployTimeoutInMinutes", "automaticDeployTimeoutInMinutesAttr", "AutomaticDeployTimeoutInMinutes", false, false, OS.Types.Integer, function () {
return 0;
}, true)
].concat(_super.attributesToDeclare.call(this));
};
DeploymentZone_AzureContainerServiceRec.init();
return DeploymentZone_AzureContainerServiceRec;
})(OS.DataTypes.GenericRecord);
Extension_OMLProcessorModel.DeploymentZone_AzureContainerServiceRec = DeploymentZone_AzureContainerServiceRec;

});
define("Extension.OMLProcessor.model$DeploymentZone_AzureContainerServiceRecord", ["exports", "OutSystems/ClientRuntime/Main", "Extension.OMLProcessor.model", "Extension.OMLProcessor.model$DeploymentZone_AzureContainerServiceRec"], function (exports, OutSystems, Extension_OMLProcessorModel) {
var OS = OutSystems.Internal;
var DeploymentZone_AzureContainerServiceRecord = (function (_super) {
__extends(DeploymentZone_AzureContainerServiceRecord, _super);
function DeploymentZone_AzureContainerServiceRecord(defaults) {
_super.apply(this, arguments);
}
DeploymentZone_AzureContainerServiceRecord.attributesToDeclare = function () {
return [
this.attr("DeploymentZone_AzureContainerService", "deploymentZone_AzureContainerServiceAttr", "DeploymentZone_AzureContainerService", false, false, OS.Types.Record, function () {
return OS.DataTypes.ImmutableBase.getData(new Extension_OMLProcessorModel.DeploymentZone_AzureContainerServiceRec());
}, true, Extension_OMLProcessorModel.DeploymentZone_AzureContainerServiceRec)
].concat(_super.attributesToDeclare.call(this));
};
DeploymentZone_AzureContainerServiceRecord.fromStructure = function (str) {
return new DeploymentZone_AzureContainerServiceRecord(new DeploymentZone_AzureContainerServiceRecord.RecordClass({
deploymentZone_AzureContainerServiceAttr: OS.DataTypes.ImmutableBase.getData(str)
}));
};
DeploymentZone_AzureContainerServiceRecord._isAnonymousRecord = true;
DeploymentZone_AzureContainerServiceRecord.UniqueId = "25448204-a389-6777-7726-71360c9a4ad9";
DeploymentZone_AzureContainerServiceRecord.init();
return DeploymentZone_AzureContainerServiceRecord;
})(OS.DataTypes.GenericRecord);
Extension_OMLProcessorModel.DeploymentZone_AzureContainerServiceRecord = DeploymentZone_AzureContainerServiceRecord;

});
define("Extension.OMLProcessor.model$LoginAttemptRec", ["exports", "OutSystems/ClientRuntime/Main", "Extension.OMLProcessor.model"], function (exports, OutSystems, Extension_OMLProcessorModel) {
var OS = OutSystems.Internal;
var LoginAttemptRec = (function (_super) {
__extends(LoginAttemptRec, _super);
function LoginAttemptRec(defaults) {
_super.apply(this, arguments);
}
LoginAttemptRec.attributesToDeclare = function () {
return [
this.attr("Id", "idAttr", "Id", true, false, OS.Types.LongInteger, function () {
return OS.DataTypes.LongInteger.defaultValue;
}, true), 
this.attr("UserId", "userIdAttr", "UserId", true, false, OS.Types.Integer, function () {
return 0;
}, true), 
this.attr("Username", "usernameAttr", "Username", true, false, OS.Types.Text, function () {
return "";
}, true), 
this.attr("Success", "successAttr", "Success", true, false, OS.Types.Boolean, function () {
return false;
}, true), 
this.attr("Instant", "instantAttr", "Instant", true, false, OS.Types.DateTime, function () {
return OS.DataTypes.DateTime.defaultValue;
}, true), 
this.attr("IPAddress", "iPAddressAttr", "IPAddress", true, false, OS.Types.Text, function () {
return "";
}, true), 
this.attr("UsernameFailureCount", "usernameFailureCountAttr", "UsernameFailureCount", true, false, OS.Types.Integer, function () {
return 0;
}, true), 
this.attr("OriginAddressFailureCount", "originAddressFailureCountAttr", "OriginAddressFailureCount", true, false, OS.Types.Integer, function () {
return 0;
}, true), 
this.attr("UserAgent", "userAgentAttr", "UserAgent", true, false, OS.Types.Text, function () {
return "";
}, true), 
this.attr("Visitor", "visitorAttr", "Visitor", true, false, OS.Types.Text, function () {
return "";
}, true), 
this.attr("RequestKey", "requestKeyAttr", "RequestKey", true, false, OS.Types.Text, function () {
return "";
}, true), 
this.attr("Result", "resultAttr", "Result", true, false, OS.Types.Text, function () {
return "";
}, true)
].concat(_super.attributesToDeclare.call(this));
};
LoginAttemptRec.init();
return LoginAttemptRec;
})(OS.DataTypes.GenericRecord);
Extension_OMLProcessorModel.LoginAttemptRec = LoginAttemptRec;

});
define("Extension.OMLProcessor.model$LoginAttemptRecord", ["exports", "OutSystems/ClientRuntime/Main", "Extension.OMLProcessor.model", "Extension.OMLProcessor.model$LoginAttemptRec"], function (exports, OutSystems, Extension_OMLProcessorModel) {
var OS = OutSystems.Internal;
var LoginAttemptRecord = (function (_super) {
__extends(LoginAttemptRecord, _super);
function LoginAttemptRecord(defaults) {
_super.apply(this, arguments);
}
LoginAttemptRecord.attributesToDeclare = function () {
return [
this.attr("LoginAttempt", "loginAttemptAttr", "LoginAttempt", false, false, OS.Types.Record, function () {
return OS.DataTypes.ImmutableBase.getData(new Extension_OMLProcessorModel.LoginAttemptRec());
}, true, Extension_OMLProcessorModel.LoginAttemptRec)
].concat(_super.attributesToDeclare.call(this));
};
LoginAttemptRecord.fromStructure = function (str) {
return new LoginAttemptRecord(new LoginAttemptRecord.RecordClass({
loginAttemptAttr: OS.DataTypes.ImmutableBase.getData(str)
}));
};
LoginAttemptRecord._isAnonymousRecord = true;
LoginAttemptRecord.UniqueId = "7b8cdc64-cd1b-e729-5cc0-7f3077c60d19";
LoginAttemptRecord.init();
return LoginAttemptRecord;
})(OS.DataTypes.GenericRecord);
Extension_OMLProcessorModel.LoginAttemptRecord = LoginAttemptRecord;

});
define("Extension.OMLProcessor.model$LoginAttemptRecordList", ["exports", "OutSystems/ClientRuntime/Main", "Extension.OMLProcessor.model", "Extension.OMLProcessor.model$LoginAttemptRecord"], function (exports, OutSystems, Extension_OMLProcessorModel) {
var OS = OutSystems.Internal;
var LoginAttemptRecordList = (function (_super) {
__extends(LoginAttemptRecordList, _super);
function LoginAttemptRecordList(defaults) {
_super.apply(this, arguments);
}
LoginAttemptRecordList.itemType = Extension_OMLProcessorModel.LoginAttemptRecord;
return LoginAttemptRecordList;
})(OS.DataTypes.GenericRecordList);
Extension_OMLProcessorModel.LoginAttemptRecordList = LoginAttemptRecordList;

});
define("Extension.OMLProcessor.model$DateTimeRec", ["exports", "OutSystems/ClientRuntime/Main", "Extension.OMLProcessor.model"], function (exports, OutSystems, Extension_OMLProcessorModel) {
var OS = OutSystems.Internal;
var DateTimeRec = (function (_super) {
__extends(DateTimeRec, _super);
function DateTimeRec(defaults) {
_super.apply(this, arguments);
}
DateTimeRec.attributesToDeclare = function () {
return [
this.attr("DateTime", "dateTimeAttr", "DateTime", false, false, OS.Types.DateTime, function () {
return OS.DataTypes.DateTime.defaultValue;
}, true)
].concat(_super.attributesToDeclare.call(this));
};
DateTimeRec.fromStructure = function (str) {
return new DateTimeRec(new DateTimeRec.RecordClass({
dateTimeAttr: OS.DataTypes.ImmutableBase.getData(str)
}));
};
DateTimeRec.init();
return DateTimeRec;
})(OS.DataTypes.GenericRecord);
Extension_OMLProcessorModel.DateTimeRec = DateTimeRec;

});
define("Extension.OMLProcessor.model$EntityEventRec", ["exports", "OutSystems/ClientRuntime/Main", "Extension.OMLProcessor.model"], function (exports, OutSystems, Extension_OMLProcessorModel) {
var OS = OutSystems.Internal;
var EntityEventRec = (function (_super) {
__extends(EntityEventRec, _super);
function EntityEventRec(defaults) {
_super.apply(this, arguments);
}
EntityEventRec.attributesToDeclare = function () {
return [
this.attr("ProcessId", "processIdAttr", "ProcessId", true, false, OS.Types.Integer, function () {
return 0;
}, true)
].concat(_super.attributesToDeclare.call(this));
};
EntityEventRec.fromStructure = function (str) {
return new EntityEventRec(new EntityEventRec.RecordClass({
processIdAttr: OS.DataTypes.ImmutableBase.getData(str)
}));
};
EntityEventRec.init();
return EntityEventRec;
})(OS.DataTypes.GenericRecord);
Extension_OMLProcessorModel.EntityEventRec = EntityEventRec;

});
define("Extension.OMLProcessor.model$EntityEventRecord", ["exports", "OutSystems/ClientRuntime/Main", "Extension.OMLProcessor.model", "Extension.OMLProcessor.model$EntityEventRec"], function (exports, OutSystems, Extension_OMLProcessorModel) {
var OS = OutSystems.Internal;
var EntityEventRecord = (function (_super) {
__extends(EntityEventRecord, _super);
function EntityEventRecord(defaults) {
_super.apply(this, arguments);
}
EntityEventRecord.attributesToDeclare = function () {
return [
this.attr("EntityEvent", "entityEventAttr", "EntityEvent", false, false, OS.Types.Record, function () {
return OS.DataTypes.ImmutableBase.getData(new Extension_OMLProcessorModel.EntityEventRec());
}, true, Extension_OMLProcessorModel.EntityEventRec)
].concat(_super.attributesToDeclare.call(this));
};
EntityEventRecord.fromStructure = function (str) {
return new EntityEventRecord(new EntityEventRecord.RecordClass({
entityEventAttr: OS.DataTypes.ImmutableBase.getData(str)
}));
};
EntityEventRecord._isAnonymousRecord = true;
EntityEventRecord.UniqueId = "ea547b84-7614-70fe-62e7-1483db29be56";
EntityEventRecord.init();
return EntityEventRecord;
})(OS.DataTypes.GenericRecord);
Extension_OMLProcessorModel.EntityEventRecord = EntityEventRecord;

});
define("Extension.OMLProcessor.model$EntityEventRecordList", ["exports", "OutSystems/ClientRuntime/Main", "Extension.OMLProcessor.model", "Extension.OMLProcessor.model$EntityEventRecord"], function (exports, OutSystems, Extension_OMLProcessorModel) {
var OS = OutSystems.Internal;
var EntityEventRecordList = (function (_super) {
__extends(EntityEventRecordList, _super);
function EntityEventRecordList(defaults) {
_super.apply(this, arguments);
}
EntityEventRecordList.itemType = Extension_OMLProcessorModel.EntityEventRecord;
return EntityEventRecordList;
})(OS.DataTypes.GenericRecordList);
Extension_OMLProcessorModel.EntityEventRecordList = EntityEventRecordList;

});
define("Extension.OMLProcessor.model$EntityDBProviderKeyRec", ["exports", "OutSystems/ClientRuntime/Main", "Extension.OMLProcessor.model"], function (exports, OutSystems, Extension_OMLProcessorModel) {
var OS = OutSystems.Internal;
var EntityDBProviderKeyRec = (function (_super) {
__extends(EntityDBProviderKeyRec, _super);
function EntityDBProviderKeyRec(defaults) {
_super.apply(this, arguments);
}
EntityDBProviderKeyRec.attributesToDeclare = function () {
return [
this.attr("EntityKey", "entityKeyAttr", "EntityKey", true, false, OS.Types.Text, function () {
return "";
}, true), 
this.attr("DBProviderKey", "dBProviderKeyAttr", "DBProviderKey", true, false, OS.Types.Text, function () {
return "";
}, true)
].concat(_super.attributesToDeclare.call(this));
};
EntityDBProviderKeyRec.init();
return EntityDBProviderKeyRec;
})(OS.DataTypes.GenericRecord);
Extension_OMLProcessorModel.EntityDBProviderKeyRec = EntityDBProviderKeyRec;

});
define("Extension.OMLProcessor.model$PTA_EspaceVersionInfoRec", ["exports", "OutSystems/ClientRuntime/Main", "Extension.OMLProcessor.model"], function (exports, OutSystems, Extension_OMLProcessorModel) {
var OS = OutSystems.Internal;
var PTA_EspaceVersionInfoRec = (function (_super) {
__extends(PTA_EspaceVersionInfoRec, _super);
function PTA_EspaceVersionInfoRec(defaults) {
_super.apply(this, arguments);
}
PTA_EspaceVersionInfoRec.attributesToDeclare = function () {
return [
this.attr("Name", "nameAttr", "Name", true, false, OS.Types.Text, function () {
return "";
}, true), 
this.attr("Description", "descriptionAttr", "Description", true, false, OS.Types.Text, function () {
return "";
}, true), 
this.attr("Key", "keyAttr", "Key", true, false, OS.Types.Text, function () {
return "";
}, true), 
this.attr("Id", "idAttr", "Id", true, false, OS.Types.Integer, function () {
return 0;
}, true), 
this.attr("VersionId", "versionIdAttr", "VersionId", true, false, OS.Types.Integer, function () {
return 0;
}, true), 
this.attr("Version", "versionAttr", "Version", true, false, OS.Types.Integer, function () {
return 0;
}, true), 
this.attr("UploadedDate", "uploadedDateAttr", "UploadedDate", true, false, OS.Types.DateTime, function () {
return OS.DataTypes.DateTime.defaultValue;
}, true), 
this.attr("UploadedBy", "uploadedByAttr", "UploadedBy", true, false, OS.Types.Text, function () {
return "";
}, true)
].concat(_super.attributesToDeclare.call(this));
};
PTA_EspaceVersionInfoRec.init();
return PTA_EspaceVersionInfoRec;
})(OS.DataTypes.GenericRecord);
Extension_OMLProcessorModel.PTA_EspaceVersionInfoRec = PTA_EspaceVersionInfoRec;

});
define("Extension.OMLProcessor.model$PTA_EspaceVersionInfoRecord", ["exports", "OutSystems/ClientRuntime/Main", "Extension.OMLProcessor.model", "Extension.OMLProcessor.model$PTA_EspaceVersionInfoRec"], function (exports, OutSystems, Extension_OMLProcessorModel) {
var OS = OutSystems.Internal;
var PTA_EspaceVersionInfoRecord = (function (_super) {
__extends(PTA_EspaceVersionInfoRecord, _super);
function PTA_EspaceVersionInfoRecord(defaults) {
_super.apply(this, arguments);
}
PTA_EspaceVersionInfoRecord.attributesToDeclare = function () {
return [
this.attr("PTA_EspaceVersionInfo", "pTA_EspaceVersionInfoAttr", "PTA_EspaceVersionInfo", false, false, OS.Types.Record, function () {
return OS.DataTypes.ImmutableBase.getData(new Extension_OMLProcessorModel.PTA_EspaceVersionInfoRec());
}, true, Extension_OMLProcessorModel.PTA_EspaceVersionInfoRec)
].concat(_super.attributesToDeclare.call(this));
};
PTA_EspaceVersionInfoRecord.fromStructure = function (str) {
return new PTA_EspaceVersionInfoRecord(new PTA_EspaceVersionInfoRecord.RecordClass({
pTA_EspaceVersionInfoAttr: OS.DataTypes.ImmutableBase.getData(str)
}));
};
PTA_EspaceVersionInfoRecord._isAnonymousRecord = true;
PTA_EspaceVersionInfoRecord.UniqueId = "294169a6-ad7c-a634-fe3e-15ee9645f4c2";
PTA_EspaceVersionInfoRecord.init();
return PTA_EspaceVersionInfoRecord;
})(OS.DataTypes.GenericRecord);
Extension_OMLProcessorModel.PTA_EspaceVersionInfoRecord = PTA_EspaceVersionInfoRecord;

});
define("Extension.OMLProcessor.model$DeploymentZone_AzureContainerServiceRecordList", ["exports", "OutSystems/ClientRuntime/Main", "Extension.OMLProcessor.model", "Extension.OMLProcessor.model$DeploymentZone_AzureContainerServiceRecord"], function (exports, OutSystems, Extension_OMLProcessorModel) {
var OS = OutSystems.Internal;
var DeploymentZone_AzureContainerServiceRecordList = (function (_super) {
__extends(DeploymentZone_AzureContainerServiceRecordList, _super);
function DeploymentZone_AzureContainerServiceRecordList(defaults) {
_super.apply(this, arguments);
}
DeploymentZone_AzureContainerServiceRecordList.itemType = Extension_OMLProcessorModel.DeploymentZone_AzureContainerServiceRecord;
return DeploymentZone_AzureContainerServiceRecordList;
})(OS.DataTypes.GenericRecordList);
Extension_OMLProcessorModel.DeploymentZone_AzureContainerServiceRecordList = DeploymentZone_AzureContainerServiceRecordList;

});
define("Extension.OMLProcessor.model$BinaryDataRec", ["exports", "OutSystems/ClientRuntime/Main", "Extension.OMLProcessor.model"], function (exports, OutSystems, Extension_OMLProcessorModel) {
var OS = OutSystems.Internal;
var BinaryDataRec = (function (_super) {
__extends(BinaryDataRec, _super);
function BinaryDataRec(defaults) {
_super.apply(this, arguments);
}
BinaryDataRec.attributesToDeclare = function () {
return [
this.attr("Bin", "binAttr", "Bin", true, false, OS.Types.BinaryData, function () {
return OS.DataTypes.BinaryData.defaultValue;
}, true)
].concat(_super.attributesToDeclare.call(this));
};
BinaryDataRec.fromStructure = function (str) {
return new BinaryDataRec(new BinaryDataRec.RecordClass({
binAttr: OS.DataTypes.ImmutableBase.getData(str)
}));
};
BinaryDataRec.init();
return BinaryDataRec;
})(OS.DataTypes.GenericRecord);
Extension_OMLProcessorModel.BinaryDataRec = BinaryDataRec;

});
define("Extension.OMLProcessor.model$FileRec", ["exports", "OutSystems/ClientRuntime/Main", "Extension.OMLProcessor.model"], function (exports, OutSystems, Extension_OMLProcessorModel) {
var OS = OutSystems.Internal;
var FileRec = (function (_super) {
__extends(FileRec, _super);
function FileRec(defaults) {
_super.apply(this, arguments);
}
FileRec.attributesToDeclare = function () {
return [
this.attr("Filename", "filenameAttr", "Filename", true, false, OS.Types.Text, function () {
return "";
}, true), 
this.attr("Content", "contentAttr", "Content", true, false, OS.Types.BinaryData, function () {
return OS.DataTypes.BinaryData.defaultValue;
}, true)
].concat(_super.attributesToDeclare.call(this));
};
FileRec.init();
return FileRec;
})(OS.DataTypes.GenericRecord);
Extension_OMLProcessorModel.FileRec = FileRec;

});
define("Extension.OMLProcessor.model$HubNodeStatusRecordList", ["exports", "OutSystems/ClientRuntime/Main", "Extension.OMLProcessor.model", "Extension.OMLProcessor.model$HubNodeStatusRecord"], function (exports, OutSystems, Extension_OMLProcessorModel) {
var OS = OutSystems.Internal;
var HubNodeStatusRecordList = (function (_super) {
__extends(HubNodeStatusRecordList, _super);
function HubNodeStatusRecordList(defaults) {
_super.apply(this, arguments);
}
HubNodeStatusRecordList.itemType = Extension_OMLProcessorModel.HubNodeStatusRecord;
return HubNodeStatusRecordList;
})(OS.DataTypes.GenericRecordList);
Extension_OMLProcessorModel.HubNodeStatusRecordList = HubNodeStatusRecordList;

});
define("Extension.OMLProcessor.model$ExternalAuthentication_PluginAPIStatusRec", ["exports", "OutSystems/ClientRuntime/Main", "Extension.OMLProcessor.model"], function (exports, OutSystems, Extension_OMLProcessorModel) {
var OS = OutSystems.Internal;
var ExternalAuthentication_PluginAPIStatusRec = (function (_super) {
__extends(ExternalAuthentication_PluginAPIStatusRec, _super);
function ExternalAuthentication_PluginAPIStatusRec(defaults) {
_super.apply(this, arguments);
}
ExternalAuthentication_PluginAPIStatusRec.attributesToDeclare = function () {
return [
this.attr("Success", "successAttr", "Success", true, false, OS.Types.Boolean, function () {
return false;
}, true), 
this.attr("ResponseId", "responseIdAttr", "ResponseId", true, false, OS.Types.Integer, function () {
return 0;
}, true), 
this.attr("ResponseMessage", "responseMessageAttr", "ResponseMessage", true, false, OS.Types.Text, function () {
return "";
}, true), 
this.attr("ResponseAdditionalInfo", "responseAdditionalInfoAttr", "ResponseAdditionalInfo", true, false, OS.Types.Text, function () {
return "";
}, true)
].concat(_super.attributesToDeclare.call(this));
};
ExternalAuthentication_PluginAPIStatusRec.init();
return ExternalAuthentication_PluginAPIStatusRec;
})(OS.DataTypes.GenericRecord);
Extension_OMLProcessorModel.ExternalAuthentication_PluginAPIStatusRec = ExternalAuthentication_PluginAPIStatusRec;

});
define("Extension.OMLProcessor.model$DateTimeRecord", ["exports", "OutSystems/ClientRuntime/Main", "Extension.OMLProcessor.model", "Extension.OMLProcessor.model$DateTimeRec"], function (exports, OutSystems, Extension_OMLProcessorModel) {
var OS = OutSystems.Internal;
var DateTimeRecord = (function (_super) {
__extends(DateTimeRecord, _super);
function DateTimeRecord(defaults) {
_super.apply(this, arguments);
}
DateTimeRecord.attributesToDeclare = function () {
return [
this.attr("DateTime", "dateTimeAttr", "DateTime", false, false, OS.Types.Record, function () {
return OS.DataTypes.ImmutableBase.getData(new Extension_OMLProcessorModel.DateTimeRec());
}, true, Extension_OMLProcessorModel.DateTimeRec)
].concat(_super.attributesToDeclare.call(this));
};
DateTimeRecord.fromStructure = function (str) {
return new DateTimeRecord(new DateTimeRecord.RecordClass({
dateTimeAttr: OS.DataTypes.ImmutableBase.getData(str)
}));
};
DateTimeRecord._isAnonymousRecord = true;
DateTimeRecord.UniqueId = "2fd2b54b-6af5-ff4d-c5da-f15b9d6ce77c";
DateTimeRecord.init();
return DateTimeRecord;
})(OS.DataTypes.GenericRecord);
Extension_OMLProcessorModel.DateTimeRecord = DateTimeRecord;

});
define("Extension.OMLProcessor.model$IntegrationPluginInfoRec", ["exports", "OutSystems/ClientRuntime/Main", "Extension.OMLProcessor.model"], function (exports, OutSystems, Extension_OMLProcessorModel) {
var OS = OutSystems.Internal;
var IntegrationPluginInfoRec = (function (_super) {
__extends(IntegrationPluginInfoRec, _super);
function IntegrationPluginInfoRec(defaults) {
_super.apply(this, arguments);
}
IntegrationPluginInfoRec.attributesToDeclare = function () {
return [
this.attr("Name", "nameAttr", "Name", true, false, OS.Types.Text, function () {
return "";
}, true), 
this.attr("Key", "keyAttr", "Key", true, false, OS.Types.Text, function () {
return "";
}, true), 
this.attr("IsExpose", "isExposeAttr", "IsExpose", true, false, OS.Types.Boolean, function () {
return false;
}, true)
].concat(_super.attributesToDeclare.call(this));
};
IntegrationPluginInfoRec.init();
return IntegrationPluginInfoRec;
})(OS.DataTypes.GenericRecord);
Extension_OMLProcessorModel.IntegrationPluginInfoRec = IntegrationPluginInfoRec;

});
define("Extension.OMLProcessor.model$IntegrationPluginInfoRecord", ["exports", "OutSystems/ClientRuntime/Main", "Extension.OMLProcessor.model", "Extension.OMLProcessor.model$IntegrationPluginInfoRec"], function (exports, OutSystems, Extension_OMLProcessorModel) {
var OS = OutSystems.Internal;
var IntegrationPluginInfoRecord = (function (_super) {
__extends(IntegrationPluginInfoRecord, _super);
function IntegrationPluginInfoRecord(defaults) {
_super.apply(this, arguments);
}
IntegrationPluginInfoRecord.attributesToDeclare = function () {
return [
this.attr("IntegrationPluginInfo", "integrationPluginInfoAttr", "IntegrationPluginInfo", false, false, OS.Types.Record, function () {
return OS.DataTypes.ImmutableBase.getData(new Extension_OMLProcessorModel.IntegrationPluginInfoRec());
}, true, Extension_OMLProcessorModel.IntegrationPluginInfoRec)
].concat(_super.attributesToDeclare.call(this));
};
IntegrationPluginInfoRecord.fromStructure = function (str) {
return new IntegrationPluginInfoRecord(new IntegrationPluginInfoRecord.RecordClass({
integrationPluginInfoAttr: OS.DataTypes.ImmutableBase.getData(str)
}));
};
IntegrationPluginInfoRecord._isAnonymousRecord = true;
IntegrationPluginInfoRecord.UniqueId = "e5205a8f-5e7d-6e9b-92be-55480bbdf15c";
IntegrationPluginInfoRecord.init();
return IntegrationPluginInfoRecord;
})(OS.DataTypes.GenericRecord);
Extension_OMLProcessorModel.IntegrationPluginInfoRecord = IntegrationPluginInfoRecord;

});
define("Extension.OMLProcessor.model$IntegrationPluginInfoRecordList", ["exports", "OutSystems/ClientRuntime/Main", "Extension.OMLProcessor.model", "Extension.OMLProcessor.model$IntegrationPluginInfoRecord"], function (exports, OutSystems, Extension_OMLProcessorModel) {
var OS = OutSystems.Internal;
var IntegrationPluginInfoRecordList = (function (_super) {
__extends(IntegrationPluginInfoRecordList, _super);
function IntegrationPluginInfoRecordList(defaults) {
_super.apply(this, arguments);
}
IntegrationPluginInfoRecordList.itemType = Extension_OMLProcessorModel.IntegrationPluginInfoRecord;
return IntegrationPluginInfoRecordList;
})(OS.DataTypes.GenericRecordList);
Extension_OMLProcessorModel.IntegrationPluginInfoRecordList = IntegrationPluginInfoRecordList;

});
define("Extension.OMLProcessor.model$HEMessageRec", ["exports", "OutSystems/ClientRuntime/Main", "Extension.OMLProcessor.model"], function (exports, OutSystems, Extension_OMLProcessorModel) {
var OS = OutSystems.Internal;
var HEMessageRec = (function (_super) {
__extends(HEMessageRec, _super);
function HEMessageRec(defaults) {
_super.apply(this, arguments);
}
HEMessageRec.attributesToDeclare = function () {
return [
this.attr("Id", "idAttr", "Id", true, false, OS.Types.Text, function () {
return "";
}, true), 
this.attr("Message", "messageAttr", "Message", true, false, OS.Types.Text, function () {
return "";
}, true), 
this.attr("Detail", "detailAttr", "Detail", true, false, OS.Types.Text, function () {
return "";
}, true), 
this.attr("HelpRef", "helpRefAttr", "HelpRef", false, false, OS.Types.Integer, function () {
return 0;
}, true), 
this.attr("ExtraInfo", "extraInfoAttr", "ExtraInfo", false, false, OS.Types.Text, function () {
return "";
}, true), 
this.attr("Type", "typeAttr", "Type", true, false, OS.Types.Text, function () {
return "";
}, true), 
this.attr("Submitable", "submitableAttr", "Submitable", true, false, OS.Types.Boolean, function () {
return false;
}, true)
].concat(_super.attributesToDeclare.call(this));
};
HEMessageRec.init();
return HEMessageRec;
})(OS.DataTypes.GenericRecord);
Extension_OMLProcessorModel.HEMessageRec = HEMessageRec;

});
define("Extension.OMLProcessor.model$HEMessageRecord", ["exports", "OutSystems/ClientRuntime/Main", "Extension.OMLProcessor.model", "Extension.OMLProcessor.model$HEMessageRec"], function (exports, OutSystems, Extension_OMLProcessorModel) {
var OS = OutSystems.Internal;
var HEMessageRecord = (function (_super) {
__extends(HEMessageRecord, _super);
function HEMessageRecord(defaults) {
_super.apply(this, arguments);
}
HEMessageRecord.attributesToDeclare = function () {
return [
this.attr("HEMessage", "hEMessageAttr", "HEMessage", false, false, OS.Types.Record, function () {
return OS.DataTypes.ImmutableBase.getData(new Extension_OMLProcessorModel.HEMessageRec());
}, true, Extension_OMLProcessorModel.HEMessageRec)
].concat(_super.attributesToDeclare.call(this));
};
HEMessageRecord.fromStructure = function (str) {
return new HEMessageRecord(new HEMessageRecord.RecordClass({
hEMessageAttr: OS.DataTypes.ImmutableBase.getData(str)
}));
};
HEMessageRecord._isAnonymousRecord = true;
HEMessageRecord.UniqueId = "d1f25c49-6027-f2c1-4bad-2eb74ab2052b";
HEMessageRecord.init();
return HEMessageRecord;
})(OS.DataTypes.GenericRecord);
Extension_OMLProcessorModel.HEMessageRecord = HEMessageRecord;

});
define("Extension.OMLProcessor.model$ActivationResponseRec", ["exports", "OutSystems/ClientRuntime/Main", "Extension.OMLProcessor.model", "Extension.OMLProcessor.model$HEMessageRecord"], function (exports, OutSystems, Extension_OMLProcessorModel) {
var OS = OutSystems.Internal;
var ActivationResponseRec = (function (_super) {
__extends(ActivationResponseRec, _super);
function ActivationResponseRec(defaults) {
_super.apply(this, arguments);
}
ActivationResponseRec.attributesToDeclare = function () {
return [
this.attr("Answer", "answerAttr", "Answer", true, false, OS.Types.Text, function () {
return "";
}, true), 
this.attr("Message", "messageAttr", "Message", true, false, OS.Types.Record, function () {
return OS.DataTypes.ImmutableBase.getData(new Extension_OMLProcessorModel.HEMessageRecord());
}, true, Extension_OMLProcessorModel.HEMessageRecord)
].concat(_super.attributesToDeclare.call(this));
};
ActivationResponseRec.init();
return ActivationResponseRec;
})(OS.DataTypes.GenericRecord);
Extension_OMLProcessorModel.ActivationResponseRec = ActivationResponseRec;

});
define("Extension.OMLProcessor.model$ActivationResponseRecord", ["exports", "OutSystems/ClientRuntime/Main", "Extension.OMLProcessor.model", "Extension.OMLProcessor.model$ActivationResponseRec"], function (exports, OutSystems, Extension_OMLProcessorModel) {
var OS = OutSystems.Internal;
var ActivationResponseRecord = (function (_super) {
__extends(ActivationResponseRecord, _super);
function ActivationResponseRecord(defaults) {
_super.apply(this, arguments);
}
ActivationResponseRecord.attributesToDeclare = function () {
return [
this.attr("ActivationResponse", "activationResponseAttr", "ActivationResponse", false, false, OS.Types.Record, function () {
return OS.DataTypes.ImmutableBase.getData(new Extension_OMLProcessorModel.ActivationResponseRec());
}, true, Extension_OMLProcessorModel.ActivationResponseRec)
].concat(_super.attributesToDeclare.call(this));
};
ActivationResponseRecord.fromStructure = function (str) {
return new ActivationResponseRecord(new ActivationResponseRecord.RecordClass({
activationResponseAttr: OS.DataTypes.ImmutableBase.getData(str)
}));
};
ActivationResponseRecord._isAnonymousRecord = true;
ActivationResponseRecord.UniqueId = "6c8d3a4e-491e-a3b5-10f5-105d76a9aa3c";
ActivationResponseRecord.init();
return ActivationResponseRecord;
})(OS.DataTypes.GenericRecord);
Extension_OMLProcessorModel.ActivationResponseRecord = ActivationResponseRecord;

});
define("Extension.OMLProcessor.model$ActivationResponseRecordList", ["exports", "OutSystems/ClientRuntime/Main", "Extension.OMLProcessor.model", "Extension.OMLProcessor.model$ActivationResponseRecord"], function (exports, OutSystems, Extension_OMLProcessorModel) {
var OS = OutSystems.Internal;
var ActivationResponseRecordList = (function (_super) {
__extends(ActivationResponseRecordList, _super);
function ActivationResponseRecordList(defaults) {
_super.apply(this, arguments);
}
ActivationResponseRecordList.itemType = Extension_OMLProcessorModel.ActivationResponseRecord;
return ActivationResponseRecordList;
})(OS.DataTypes.GenericRecordList);
Extension_OMLProcessorModel.ActivationResponseRecordList = ActivationResponseRecordList;

});
define("Extension.OMLProcessor.model$DALDBConfigParamRec", ["exports", "OutSystems/ClientRuntime/Main", "Extension.OMLProcessor.model"], function (exports, OutSystems, Extension_OMLProcessorModel) {
var OS = OutSystems.Internal;
var DALDBConfigParamRec = (function (_super) {
__extends(DALDBConfigParamRec, _super);
function DALDBConfigParamRec(defaults) {
_super.apply(this, arguments);
}
DALDBConfigParamRec.attributesToDeclare = function () {
return [
this.attr("Name", "nameAttr", "Name", true, false, OS.Types.Text, function () {
return "";
}, true), 
this.attr("Type", "typeAttr", "Type", true, false, OS.Types.Text, function () {
return "";
}, true), 
this.attr("Label", "labelAttr", "Label", true, false, OS.Types.Text, function () {
return "";
}, true), 
this.attr("Value", "valueAttr", "Value", true, false, OS.Types.Text, function () {
return "";
}, true), 
this.attr("Mandatory", "mandatoryAttr", "Mandatory", true, false, OS.Types.Boolean, function () {
return false;
}, true), 
this.attr("Example", "exampleAttr", "Example", true, false, OS.Types.Text, function () {
return "";
}, true), 
this.attr("AutoCompleteExpression", "autoCompleteExpressionAttr", "AutoCompleteExpression", true, false, OS.Types.Text, function () {
return "";
}, true)
].concat(_super.attributesToDeclare.call(this));
};
DALDBConfigParamRec.init();
return DALDBConfigParamRec;
})(OS.DataTypes.GenericRecord);
Extension_OMLProcessorModel.DALDBConfigParamRec = DALDBConfigParamRec;

});
define("Extension.OMLProcessor.model$DALDBConfigParamRecord", ["exports", "OutSystems/ClientRuntime/Main", "Extension.OMLProcessor.model", "Extension.OMLProcessor.model$DALDBConfigParamRec"], function (exports, OutSystems, Extension_OMLProcessorModel) {
var OS = OutSystems.Internal;
var DALDBConfigParamRecord = (function (_super) {
__extends(DALDBConfigParamRecord, _super);
function DALDBConfigParamRecord(defaults) {
_super.apply(this, arguments);
}
DALDBConfigParamRecord.attributesToDeclare = function () {
return [
this.attr("DALDBConfigParam", "dALDBConfigParamAttr", "DALDBConfigParam", false, false, OS.Types.Record, function () {
return OS.DataTypes.ImmutableBase.getData(new Extension_OMLProcessorModel.DALDBConfigParamRec());
}, true, Extension_OMLProcessorModel.DALDBConfigParamRec)
].concat(_super.attributesToDeclare.call(this));
};
DALDBConfigParamRecord.fromStructure = function (str) {
return new DALDBConfigParamRecord(new DALDBConfigParamRecord.RecordClass({
dALDBConfigParamAttr: OS.DataTypes.ImmutableBase.getData(str)
}));
};
DALDBConfigParamRecord._isAnonymousRecord = true;
DALDBConfigParamRecord.UniqueId = "3b61129c-7381-d326-3966-6271827f709c";
DALDBConfigParamRecord.init();
return DALDBConfigParamRecord;
})(OS.DataTypes.GenericRecord);
Extension_OMLProcessorModel.DALDBConfigParamRecord = DALDBConfigParamRecord;

});
define("Extension.OMLProcessor.model$DALDBConfigParamRecordList", ["exports", "OutSystems/ClientRuntime/Main", "Extension.OMLProcessor.model", "Extension.OMLProcessor.model$DALDBConfigParamRecord"], function (exports, OutSystems, Extension_OMLProcessorModel) {
var OS = OutSystems.Internal;
var DALDBConfigParamRecordList = (function (_super) {
__extends(DALDBConfigParamRecordList, _super);
function DALDBConfigParamRecordList(defaults) {
_super.apply(this, arguments);
}
DALDBConfigParamRecordList.itemType = Extension_OMLProcessorModel.DALDBConfigParamRecord;
return DALDBConfigParamRecordList;
})(OS.DataTypes.GenericRecordList);
Extension_OMLProcessorModel.DALDBConfigParamRecordList = DALDBConfigParamRecordList;

});
define("Extension.OMLProcessor.model$ProcessMemDataRec", ["exports", "OutSystems/ClientRuntime/Main", "Extension.OMLProcessor.model"], function (exports, OutSystems, Extension_OMLProcessorModel) {
var OS = OutSystems.Internal;
var ProcessMemDataRec = (function (_super) {
__extends(ProcessMemDataRec, _super);
function ProcessMemDataRec(defaults) {
_super.apply(this, arguments);
}
ProcessMemDataRec.attributesToDeclare = function () {
return [
this.attr("maxMem", "maxMemAttr", "maxMem", true, false, OS.Types.Decimal, function () {
return OS.DataTypes.Decimal.defaultValue;
}, true), 
this.attr("initMem", "initMemAttr", "initMem", true, false, OS.Types.Decimal, function () {
return OS.DataTypes.Decimal.defaultValue;
}, true), 
this.attr("endMem", "endMemAttr", "endMem", true, false, OS.Types.Decimal, function () {
return OS.DataTypes.Decimal.defaultValue;
}, true)
].concat(_super.attributesToDeclare.call(this));
};
ProcessMemDataRec.init();
return ProcessMemDataRec;
})(OS.DataTypes.GenericRecord);
Extension_OMLProcessorModel.ProcessMemDataRec = ProcessMemDataRec;

});
define("Extension.OMLProcessor.model$ProcessMemDataRecord", ["exports", "OutSystems/ClientRuntime/Main", "Extension.OMLProcessor.model", "Extension.OMLProcessor.model$ProcessMemDataRec"], function (exports, OutSystems, Extension_OMLProcessorModel) {
var OS = OutSystems.Internal;
var ProcessMemDataRecord = (function (_super) {
__extends(ProcessMemDataRecord, _super);
function ProcessMemDataRecord(defaults) {
_super.apply(this, arguments);
}
ProcessMemDataRecord.attributesToDeclare = function () {
return [
this.attr("ProcessMemData", "processMemDataAttr", "ProcessMemData", false, false, OS.Types.Record, function () {
return OS.DataTypes.ImmutableBase.getData(new Extension_OMLProcessorModel.ProcessMemDataRec());
}, true, Extension_OMLProcessorModel.ProcessMemDataRec)
].concat(_super.attributesToDeclare.call(this));
};
ProcessMemDataRecord.fromStructure = function (str) {
return new ProcessMemDataRecord(new ProcessMemDataRecord.RecordClass({
processMemDataAttr: OS.DataTypes.ImmutableBase.getData(str)
}));
};
ProcessMemDataRecord._isAnonymousRecord = true;
ProcessMemDataRecord.UniqueId = "3c003c30-57cd-3117-05c4-196d7339e7d8";
ProcessMemDataRecord.init();
return ProcessMemDataRecord;
})(OS.DataTypes.GenericRecord);
Extension_OMLProcessorModel.ProcessMemDataRecord = ProcessMemDataRecord;

});
define("Extension.OMLProcessor.model$LinkRecordList", ["exports", "OutSystems/ClientRuntime/Main", "Extension.OMLProcessor.model", "Extension.OMLProcessor.model$LinkRecord"], function (exports, OutSystems, Extension_OMLProcessorModel) {
var OS = OutSystems.Internal;
var LinkRecordList = (function (_super) {
__extends(LinkRecordList, _super);
function LinkRecordList(defaults) {
_super.apply(this, arguments);
}
LinkRecordList.itemType = Extension_OMLProcessorModel.LinkRecord;
return LinkRecordList;
})(OS.DataTypes.GenericRecordList);
Extension_OMLProcessorModel.LinkRecordList = LinkRecordList;

});
define("Extension.OMLProcessor.model$CacheInvalidationServiceStatusRec", ["exports", "OutSystems/ClientRuntime/Main", "Extension.OMLProcessor.model"], function (exports, OutSystems, Extension_OMLProcessorModel) {
var OS = OutSystems.Internal;
var CacheInvalidationServiceStatusRec = (function (_super) {
__extends(CacheInvalidationServiceStatusRec, _super);
function CacheInvalidationServiceStatusRec(defaults) {
_super.apply(this, arguments);
}
CacheInvalidationServiceStatusRec.attributesToDeclare = function () {
return [
this.attr("IsOk", "isOkAttr", "IsOk", true, false, OS.Types.Boolean, function () {
return false;
}, true), 
this.attr("Message", "messageAttr", "Message", true, false, OS.Types.Text, function () {
return "";
}, true)
].concat(_super.attributesToDeclare.call(this));
};
CacheInvalidationServiceStatusRec.init();
return CacheInvalidationServiceStatusRec;
})(OS.DataTypes.GenericRecord);
Extension_OMLProcessorModel.CacheInvalidationServiceStatusRec = CacheInvalidationServiceStatusRec;

});
define("Extension.OMLProcessor.model$CacheInvalidationServiceStatusRecord", ["exports", "OutSystems/ClientRuntime/Main", "Extension.OMLProcessor.model", "Extension.OMLProcessor.model$CacheInvalidationServiceStatusRec"], function (exports, OutSystems, Extension_OMLProcessorModel) {
var OS = OutSystems.Internal;
var CacheInvalidationServiceStatusRecord = (function (_super) {
__extends(CacheInvalidationServiceStatusRecord, _super);
function CacheInvalidationServiceStatusRecord(defaults) {
_super.apply(this, arguments);
}
CacheInvalidationServiceStatusRecord.attributesToDeclare = function () {
return [
this.attr("CacheInvalidationServiceStatus", "cacheInvalidationServiceStatusAttr", "CacheInvalidationServiceStatus", false, false, OS.Types.Record, function () {
return OS.DataTypes.ImmutableBase.getData(new Extension_OMLProcessorModel.CacheInvalidationServiceStatusRec());
}, true, Extension_OMLProcessorModel.CacheInvalidationServiceStatusRec)
].concat(_super.attributesToDeclare.call(this));
};
CacheInvalidationServiceStatusRecord.fromStructure = function (str) {
return new CacheInvalidationServiceStatusRecord(new CacheInvalidationServiceStatusRecord.RecordClass({
cacheInvalidationServiceStatusAttr: OS.DataTypes.ImmutableBase.getData(str)
}));
};
CacheInvalidationServiceStatusRecord._isAnonymousRecord = true;
CacheInvalidationServiceStatusRecord.UniqueId = "eda213e5-c151-5e35-aca1-7935098bf568";
CacheInvalidationServiceStatusRecord.init();
return CacheInvalidationServiceStatusRecord;
})(OS.DataTypes.GenericRecord);
Extension_OMLProcessorModel.CacheInvalidationServiceStatusRecord = CacheInvalidationServiceStatusRecord;

});
define("Extension.OMLProcessor.model$CacheInvalidationServiceStatusRecordList", ["exports", "OutSystems/ClientRuntime/Main", "Extension.OMLProcessor.model", "Extension.OMLProcessor.model$CacheInvalidationServiceStatusRecord"], function (exports, OutSystems, Extension_OMLProcessorModel) {
var OS = OutSystems.Internal;
var CacheInvalidationServiceStatusRecordList = (function (_super) {
__extends(CacheInvalidationServiceStatusRecordList, _super);
function CacheInvalidationServiceStatusRecordList(defaults) {
_super.apply(this, arguments);
}
CacheInvalidationServiceStatusRecordList.itemType = Extension_OMLProcessorModel.CacheInvalidationServiceStatusRecord;
return CacheInvalidationServiceStatusRecordList;
})(OS.DataTypes.GenericRecordList);
Extension_OMLProcessorModel.CacheInvalidationServiceStatusRecordList = CacheInvalidationServiceStatusRecordList;

});
define("Extension.OMLProcessor.model$TextRecordList", ["exports", "OutSystems/ClientRuntime/Main", "Extension.OMLProcessor.model", "Extension.OMLProcessor.model$TextRecord"], function (exports, OutSystems, Extension_OMLProcessorModel) {
var OS = OutSystems.Internal;
var TextRecordList = (function (_super) {
__extends(TextRecordList, _super);
function TextRecordList(defaults) {
_super.apply(this, arguments);
}
TextRecordList.itemType = Extension_OMLProcessorModel.TextRecord;
return TextRecordList;
})(OS.DataTypes.GenericRecordList);
Extension_OMLProcessorModel.TextRecordList = TextRecordList;

});
define("Extension.OMLProcessor.model$EspacePublishOptionsRec", ["exports", "OutSystems/ClientRuntime/Main", "Extension.OMLProcessor.model", "Extension.OMLProcessor.model$TextRecordList", "Extension.OMLProcessor.model$KeyValuePairRecordList"], function (exports, OutSystems, Extension_OMLProcessorModel) {
var OS = OutSystems.Internal;
var EspacePublishOptionsRec = (function (_super) {
__extends(EspacePublishOptionsRec, _super);
function EspacePublishOptionsRec(defaults) {
_super.apply(this, arguments);
}
EspacePublishOptionsRec.attributesToDeclare = function () {
return [
this.attr("ApplicationKey", "applicationKeyAttr", "ApplicationKey", false, false, OS.Types.Text, function () {
return "";
}, true), 
this.attr("CatalogId", "catalogIdAttr", "CatalogId", false, false, OS.Types.Integer, function () {
return 0;
}, true), 
this.attr("ReplaceExisting", "replaceExistingAttr", "ReplaceExisting", false, false, OS.Types.Boolean, function () {
return false;
}, true), 
this.attr("ReuseTables", "reuseTablesAttr", "ReuseTables", false, false, OS.Types.Boolean, function () {
return false;
}, true), 
this.attr("CommitMessage", "commitMessageAttr", "CommitMessage", false, false, OS.Types.Text, function () {
return "";
}, true), 
this.attr("CheckServerVersion", "checkServerVersionAttr", "CheckServerVersion", false, false, OS.Types.Boolean, function () {
return false;
}, true), 
this.attr("LastCheckedServerVersion", "lastCheckedServerVersionAttr", "LastCheckedServerVersion", false, false, OS.Types.Integer, function () {
return 0;
}, true), 
this.attr("AbortOnNewBrokenReferences", "abortOnNewBrokenReferencesAttr", "AbortOnNewBrokenReferences", false, false, OS.Types.Boolean, function () {
return false;
}, true), 
this.attr("LastCheckedBrokenReferences", "lastCheckedBrokenReferencesAttr", "LastCheckedBrokenReferences", false, false, OS.Types.RecordList, function () {
return OS.DataTypes.ImmutableBase.getData(new Extension_OMLProcessorModel.TextRecordList());
}, true, Extension_OMLProcessorModel.TextRecordList), 
this.attr("CompilerSettingsOverride", "compilerSettingsOverrideAttr", "CompilerSettingsOverride", false, false, OS.Types.RecordList, function () {
return OS.DataTypes.ImmutableBase.getData(new Extension_OMLProcessorModel.KeyValuePairRecordList());
}, true, Extension_OMLProcessorModel.KeyValuePairRecordList), 
this.attr("NativeBuildsEnabled", "nativeBuildsEnabledAttr", "NativeBuildsEnabled", false, false, OS.Types.Boolean, function () {
return false;
}, true), 
this.attr("FromIDE", "fromIDEAttr", "FromIDE", false, false, OS.Types.Boolean, function () {
return false;
}, true), 
this.attr("PublishId", "publishIdAttr", "PublishId", false, false, OS.Types.Text, function () {
return "";
}, true)
].concat(_super.attributesToDeclare.call(this));
};
EspacePublishOptionsRec.init();
return EspacePublishOptionsRec;
})(OS.DataTypes.GenericRecord);
Extension_OMLProcessorModel.EspacePublishOptionsRec = EspacePublishOptionsRec;

});
define("Extension.OMLProcessor.model$EspacePublishOptionsRecord", ["exports", "OutSystems/ClientRuntime/Main", "Extension.OMLProcessor.model", "Extension.OMLProcessor.model$EspacePublishOptionsRec"], function (exports, OutSystems, Extension_OMLProcessorModel) {
var OS = OutSystems.Internal;
var EspacePublishOptionsRecord = (function (_super) {
__extends(EspacePublishOptionsRecord, _super);
function EspacePublishOptionsRecord(defaults) {
_super.apply(this, arguments);
}
EspacePublishOptionsRecord.attributesToDeclare = function () {
return [
this.attr("EspacePublishOptions", "espacePublishOptionsAttr", "EspacePublishOptions", false, false, OS.Types.Record, function () {
return OS.DataTypes.ImmutableBase.getData(new Extension_OMLProcessorModel.EspacePublishOptionsRec());
}, true, Extension_OMLProcessorModel.EspacePublishOptionsRec)
].concat(_super.attributesToDeclare.call(this));
};
EspacePublishOptionsRecord.fromStructure = function (str) {
return new EspacePublishOptionsRecord(new EspacePublishOptionsRecord.RecordClass({
espacePublishOptionsAttr: OS.DataTypes.ImmutableBase.getData(str)
}));
};
EspacePublishOptionsRecord._isAnonymousRecord = true;
EspacePublishOptionsRecord.UniqueId = "487b1e5d-e11d-5c2f-9b0f-a1886a8e89a6";
EspacePublishOptionsRecord.init();
return EspacePublishOptionsRecord;
})(OS.DataTypes.GenericRecord);
Extension_OMLProcessorModel.EspacePublishOptionsRecord = EspacePublishOptionsRecord;

});
define("Extension.OMLProcessor.model$UserWithUserPoolRec", ["exports", "OutSystems/ClientRuntime/Main", "Extension.OMLProcessor.model"], function (exports, OutSystems, Extension_OMLProcessorModel) {
var OS = OutSystems.Internal;
var UserWithUserPoolRec = (function (_super) {
__extends(UserWithUserPoolRec, _super);
function UserWithUserPoolRec(defaults) {
_super.apply(this, arguments);
}
UserWithUserPoolRec.attributesToDeclare = function () {
return [
this.attr("UserId", "userIdAttr", "UserId", true, false, OS.Types.LongInteger, function () {
return OS.DataTypes.LongInteger.defaultValue;
}, true), 
this.attr("UserPoolKey", "userPoolKeyAttr", "UserPoolKey", true, false, OS.Types.Text, function () {
return "";
}, true)
].concat(_super.attributesToDeclare.call(this));
};
UserWithUserPoolRec.init();
return UserWithUserPoolRec;
})(OS.DataTypes.GenericRecord);
Extension_OMLProcessorModel.UserWithUserPoolRec = UserWithUserPoolRec;

});
define("Extension.OMLProcessor.model$UserWithUserPoolRecord", ["exports", "OutSystems/ClientRuntime/Main", "Extension.OMLProcessor.model", "Extension.OMLProcessor.model$UserWithUserPoolRec"], function (exports, OutSystems, Extension_OMLProcessorModel) {
var OS = OutSystems.Internal;
var UserWithUserPoolRecord = (function (_super) {
__extends(UserWithUserPoolRecord, _super);
function UserWithUserPoolRecord(defaults) {
_super.apply(this, arguments);
}
UserWithUserPoolRecord.attributesToDeclare = function () {
return [
this.attr("UserWithUserPool", "userWithUserPoolAttr", "UserWithUserPool", false, false, OS.Types.Record, function () {
return OS.DataTypes.ImmutableBase.getData(new Extension_OMLProcessorModel.UserWithUserPoolRec());
}, true, Extension_OMLProcessorModel.UserWithUserPoolRec)
].concat(_super.attributesToDeclare.call(this));
};
UserWithUserPoolRecord.fromStructure = function (str) {
return new UserWithUserPoolRecord(new UserWithUserPoolRecord.RecordClass({
userWithUserPoolAttr: OS.DataTypes.ImmutableBase.getData(str)
}));
};
UserWithUserPoolRecord._isAnonymousRecord = true;
UserWithUserPoolRecord.UniqueId = "48fd86b8-58de-d5f5-c5bb-cc0c0ff04613";
UserWithUserPoolRecord.init();
return UserWithUserPoolRecord;
})(OS.DataTypes.GenericRecord);
Extension_OMLProcessorModel.UserWithUserPoolRecord = UserWithUserPoolRecord;

});
define("Extension.OMLProcessor.model$ApplicationModuleDetailsRec", ["exports", "OutSystems/ClientRuntime/Main", "Extension.OMLProcessor.model"], function (exports, OutSystems, Extension_OMLProcessorModel) {
var OS = OutSystems.Internal;
var ApplicationModuleDetailsRec = (function (_super) {
__extends(ApplicationModuleDetailsRec, _super);
function ApplicationModuleDetailsRec(defaults) {
_super.apply(this, arguments);
}
ApplicationModuleDetailsRec.attributesToDeclare = function () {
return [
this.attr("ModuleKey", "moduleKeyAttr", "ModuleKey", true, false, OS.Types.Text, function () {
return "";
}, true), 
this.attr("ModuleName", "moduleNameAttr", "ModuleName", true, false, OS.Types.Text, function () {
return "";
}, true), 
this.attr("ApplicationName", "applicationNameAttr", "ApplicationName", true, false, OS.Types.Text, function () {
return "";
}, true), 
this.attr("ApplicationKey", "applicationKeyAttr", "ApplicationKey", true, false, OS.Types.Text, function () {
return "";
}, true)
].concat(_super.attributesToDeclare.call(this));
};
ApplicationModuleDetailsRec.init();
return ApplicationModuleDetailsRec;
})(OS.DataTypes.GenericRecord);
Extension_OMLProcessorModel.ApplicationModuleDetailsRec = ApplicationModuleDetailsRec;

});
define("Extension.OMLProcessor.model$ApplicationModuleDetailsRecord", ["exports", "OutSystems/ClientRuntime/Main", "Extension.OMLProcessor.model", "Extension.OMLProcessor.model$ApplicationModuleDetailsRec"], function (exports, OutSystems, Extension_OMLProcessorModel) {
var OS = OutSystems.Internal;
var ApplicationModuleDetailsRecord = (function (_super) {
__extends(ApplicationModuleDetailsRecord, _super);
function ApplicationModuleDetailsRecord(defaults) {
_super.apply(this, arguments);
}
ApplicationModuleDetailsRecord.attributesToDeclare = function () {
return [
this.attr("ApplicationModuleDetails", "applicationModuleDetailsAttr", "ApplicationModuleDetails", false, false, OS.Types.Record, function () {
return OS.DataTypes.ImmutableBase.getData(new Extension_OMLProcessorModel.ApplicationModuleDetailsRec());
}, true, Extension_OMLProcessorModel.ApplicationModuleDetailsRec)
].concat(_super.attributesToDeclare.call(this));
};
ApplicationModuleDetailsRecord.fromStructure = function (str) {
return new ApplicationModuleDetailsRecord(new ApplicationModuleDetailsRecord.RecordClass({
applicationModuleDetailsAttr: OS.DataTypes.ImmutableBase.getData(str)
}));
};
ApplicationModuleDetailsRecord._isAnonymousRecord = true;
ApplicationModuleDetailsRecord.UniqueId = "4c54ec35-2dcb-882f-0544-c674999fdd35";
ApplicationModuleDetailsRecord.init();
return ApplicationModuleDetailsRecord;
})(OS.DataTypes.GenericRecord);
Extension_OMLProcessorModel.ApplicationModuleDetailsRecord = ApplicationModuleDetailsRecord;

});
define("Extension.OMLProcessor.model$ExtensionEntityInfoRecordList", ["exports", "OutSystems/ClientRuntime/Main", "Extension.OMLProcessor.model", "Extension.OMLProcessor.model$ExtensionEntityInfoRecord"], function (exports, OutSystems, Extension_OMLProcessorModel) {
var OS = OutSystems.Internal;
var ExtensionEntityInfoRecordList = (function (_super) {
__extends(ExtensionEntityInfoRecordList, _super);
function ExtensionEntityInfoRecordList(defaults) {
_super.apply(this, arguments);
}
ExtensionEntityInfoRecordList.itemType = Extension_OMLProcessorModel.ExtensionEntityInfoRecord;
return ExtensionEntityInfoRecordList;
})(OS.DataTypes.GenericRecordList);
Extension_OMLProcessorModel.ExtensionEntityInfoRecordList = ExtensionEntityInfoRecordList;

});
define("Extension.OMLProcessor.model$HEMessageRecordList", ["exports", "OutSystems/ClientRuntime/Main", "Extension.OMLProcessor.model", "Extension.OMLProcessor.model$HEMessageRecord"], function (exports, OutSystems, Extension_OMLProcessorModel) {
var OS = OutSystems.Internal;
var HEMessageRecordList = (function (_super) {
__extends(HEMessageRecordList, _super);
function HEMessageRecordList(defaults) {
_super.apply(this, arguments);
}
HEMessageRecordList.itemType = Extension_OMLProcessorModel.HEMessageRecord;
return HEMessageRecordList;
})(OS.DataTypes.GenericRecordList);
Extension_OMLProcessorModel.HEMessageRecordList = HEMessageRecordList;

});
define("Extension.OMLProcessor.model$VersionComparisonRec", ["exports", "OutSystems/ClientRuntime/Main", "Extension.OMLProcessor.model"], function (exports, OutSystems, Extension_OMLProcessorModel) {
var OS = OutSystems.Internal;
var VersionComparisonRec = (function (_super) {
__extends(VersionComparisonRec, _super);
function VersionComparisonRec(defaults) {
_super.apply(this, arguments);
}
VersionComparisonRec.attributesToDeclare = function () {
return [
this.attr("Result", "resultAttr", "Result", true, false, OS.Types.Integer, function () {
return 0;
}, true), 
this.attr("LastServerVersion", "lastServerVersionAttr", "LastServerVersion", true, false, OS.Types.Integer, function () {
return 0;
}, true), 
this.attr("UploadUser", "uploadUserAttr", "UploadUser", true, false, OS.Types.Text, function () {
return "";
}, true), 
this.attr("UploadDate", "uploadDateAttr", "UploadDate", true, false, OS.Types.DateTime, function () {
return OS.DataTypes.DateTime.defaultValue;
}, true)
].concat(_super.attributesToDeclare.call(this));
};
VersionComparisonRec.init();
return VersionComparisonRec;
})(OS.DataTypes.GenericRecord);
Extension_OMLProcessorModel.VersionComparisonRec = VersionComparisonRec;

});
define("Extension.OMLProcessor.model$VersionComparisonRecord", ["exports", "OutSystems/ClientRuntime/Main", "Extension.OMLProcessor.model", "Extension.OMLProcessor.model$VersionComparisonRec"], function (exports, OutSystems, Extension_OMLProcessorModel) {
var OS = OutSystems.Internal;
var VersionComparisonRecord = (function (_super) {
__extends(VersionComparisonRecord, _super);
function VersionComparisonRecord(defaults) {
_super.apply(this, arguments);
}
VersionComparisonRecord.attributesToDeclare = function () {
return [
this.attr("VersionComparison", "versionComparisonAttr", "VersionComparison", false, false, OS.Types.Record, function () {
return OS.DataTypes.ImmutableBase.getData(new Extension_OMLProcessorModel.VersionComparisonRec());
}, true, Extension_OMLProcessorModel.VersionComparisonRec)
].concat(_super.attributesToDeclare.call(this));
};
VersionComparisonRecord.fromStructure = function (str) {
return new VersionComparisonRecord(new VersionComparisonRecord.RecordClass({
versionComparisonAttr: OS.DataTypes.ImmutableBase.getData(str)
}));
};
VersionComparisonRecord._isAnonymousRecord = true;
VersionComparisonRecord.UniqueId = "af5dde89-509f-c8a8-795b-2caa6fc36074";
VersionComparisonRecord.init();
return VersionComparisonRecord;
})(OS.DataTypes.GenericRecord);
Extension_OMLProcessorModel.VersionComparisonRecord = VersionComparisonRecord;

});
define("Extension.OMLProcessor.model$EspacePublishAsyncResultRec", ["exports", "OutSystems/ClientRuntime/Main", "Extension.OMLProcessor.model", "Extension.OMLProcessor.model$HEMessageRecordList", "Extension.OMLProcessor.model$VersionComparisonRecord"], function (exports, OutSystems, Extension_OMLProcessorModel) {
var OS = OutSystems.Internal;
var EspacePublishAsyncResultRec = (function (_super) {
__extends(EspacePublishAsyncResultRec, _super);
function EspacePublishAsyncResultRec(defaults) {
_super.apply(this, arguments);
}
EspacePublishAsyncResultRec.attributesToDeclare = function () {
return [
this.attr("PublishId", "publishIdAttr", "PublishId", true, false, OS.Types.Text, function () {
return "";
}, true), 
this.attr("HasErrors", "hasErrorsAttr", "HasErrors", true, false, OS.Types.Boolean, function () {
return false;
}, true), 
this.attr("Messages", "messagesAttr", "Messages", true, false, OS.Types.RecordList, function () {
return OS.DataTypes.ImmutableBase.getData(new Extension_OMLProcessorModel.HEMessageRecordList());
}, true, Extension_OMLProcessorModel.HEMessageRecordList), 
this.attr("EspaceId", "espaceIdAttr", "EspaceId", false, false, OS.Types.Integer, function () {
return 0;
}, true), 
this.attr("EspaceVersionId", "espaceVersionIdAttr", "EspaceVersionId", false, false, OS.Types.Integer, function () {
return 0;
}, true), 
this.attr("VersionComparison", "versionComparisonAttr", "VersionComparison", false, false, OS.Types.Record, function () {
return OS.DataTypes.ImmutableBase.getData(new Extension_OMLProcessorModel.VersionComparisonRecord());
}, true, Extension_OMLProcessorModel.VersionComparisonRecord), 
this.attr("NeedsCatalog", "needsCatalogAttr", "NeedsCatalog", false, false, OS.Types.Boolean, function () {
return false;
}, true), 
this.attr("NeedsReplace", "needsReplaceAttr", "NeedsReplace", false, false, OS.Types.Boolean, function () {
return false;
}, true)
].concat(_super.attributesToDeclare.call(this));
};
EspacePublishAsyncResultRec.init();
return EspacePublishAsyncResultRec;
})(OS.DataTypes.GenericRecord);
Extension_OMLProcessorModel.EspacePublishAsyncResultRec = EspacePublishAsyncResultRec;

});
define("Extension.OMLProcessor.model$ActivationLicenseInformationRec", ["exports", "OutSystems/ClientRuntime/Main", "Extension.OMLProcessor.model", "Extension.OMLProcessor.model$ActivationResponseRecord"], function (exports, OutSystems, Extension_OMLProcessorModel) {
var OS = OutSystems.Internal;
var ActivationLicenseInformationRec = (function (_super) {
__extends(ActivationLicenseInformationRec, _super);
function ActivationLicenseInformationRec(defaults) {
_super.apply(this, arguments);
}
ActivationLicenseInformationRec.attributesToDeclare = function () {
return [
this.attr("Schema", "schemaAttr", "Schema", true, false, OS.Types.Text, function () {
return "";
}, true), 
this.attr("UID", "uIDAttr", "UID", true, false, OS.Types.Text, function () {
return "";
}, true), 
this.attr("Creation", "creationAttr", "Creation", true, false, OS.Types.Text, function () {
return "";
}, true), 
this.attr("Term", "termAttr", "Term", true, false, OS.Types.Text, function () {
return "";
}, true), 
this.attr("Warn", "warnAttr", "Warn", true, false, OS.Types.Text, function () {
return "";
}, true), 
this.attr("Grace", "graceAttr", "Grace", true, false, OS.Types.Text, function () {
return "";
}, true), 
this.attr("Type", "typeAttr", "Type", true, false, OS.Types.Text, function () {
return "";
}, true), 
this.attr("Edition", "editionAttr", "Edition", true, false, OS.Types.Text, function () {
return "";
}, true), 
this.attr("Serial", "serialAttr", "Serial", true, false, OS.Types.Text, function () {
return "";
}, true), 
this.attr("Organization", "organizationAttr", "Organization", true, false, OS.Types.Text, function () {
return "";
}, true), 
this.attr("State", "stateAttr", "State", true, false, OS.Types.Record, function () {
return OS.DataTypes.ImmutableBase.getData(new Extension_OMLProcessorModel.ActivationResponseRecord());
}, true, Extension_OMLProcessorModel.ActivationResponseRecord), 
this.attr("IsOEM", "isOEMAttr", "IsOEM", true, false, OS.Types.Text, function () {
return "";
}, true)
].concat(_super.attributesToDeclare.call(this));
};
ActivationLicenseInformationRec.init();
return ActivationLicenseInformationRec;
})(OS.DataTypes.GenericRecord);
Extension_OMLProcessorModel.ActivationLicenseInformationRec = ActivationLicenseInformationRec;

});
define("Extension.OMLProcessor.model$ActivationLicenseInformationRecord", ["exports", "OutSystems/ClientRuntime/Main", "Extension.OMLProcessor.model", "Extension.OMLProcessor.model$ActivationLicenseInformationRec"], function (exports, OutSystems, Extension_OMLProcessorModel) {
var OS = OutSystems.Internal;
var ActivationLicenseInformationRecord = (function (_super) {
__extends(ActivationLicenseInformationRecord, _super);
function ActivationLicenseInformationRecord(defaults) {
_super.apply(this, arguments);
}
ActivationLicenseInformationRecord.attributesToDeclare = function () {
return [
this.attr("ActivationLicenseInformation", "activationLicenseInformationAttr", "ActivationLicenseInformation", false, false, OS.Types.Record, function () {
return OS.DataTypes.ImmutableBase.getData(new Extension_OMLProcessorModel.ActivationLicenseInformationRec());
}, true, Extension_OMLProcessorModel.ActivationLicenseInformationRec)
].concat(_super.attributesToDeclare.call(this));
};
ActivationLicenseInformationRecord.fromStructure = function (str) {
return new ActivationLicenseInformationRecord(new ActivationLicenseInformationRecord.RecordClass({
activationLicenseInformationAttr: OS.DataTypes.ImmutableBase.getData(str)
}));
};
ActivationLicenseInformationRecord._isAnonymousRecord = true;
ActivationLicenseInformationRecord.UniqueId = "54666c57-538f-aea8-309c-ef82993477a9";
ActivationLicenseInformationRecord.init();
return ActivationLicenseInformationRecord;
})(OS.DataTypes.GenericRecord);
Extension_OMLProcessorModel.ActivationLicenseInformationRecord = ActivationLicenseInformationRecord;

});
define("Extension.OMLProcessor.model$PTA_EspaceVersionInfoRecordList", ["exports", "OutSystems/ClientRuntime/Main", "Extension.OMLProcessor.model", "Extension.OMLProcessor.model$PTA_EspaceVersionInfoRecord"], function (exports, OutSystems, Extension_OMLProcessorModel) {
var OS = OutSystems.Internal;
var PTA_EspaceVersionInfoRecordList = (function (_super) {
__extends(PTA_EspaceVersionInfoRecordList, _super);
function PTA_EspaceVersionInfoRecordList(defaults) {
_super.apply(this, arguments);
}
PTA_EspaceVersionInfoRecordList.itemType = Extension_OMLProcessorModel.PTA_EspaceVersionInfoRecord;
return PTA_EspaceVersionInfoRecordList;
})(OS.DataTypes.GenericRecordList);
Extension_OMLProcessorModel.PTA_EspaceVersionInfoRecordList = PTA_EspaceVersionInfoRecordList;

});
define("Extension.OMLProcessor.model$ModuleVersionDefinitionRec", ["exports", "OutSystems/ClientRuntime/Main", "Extension.OMLProcessor.model", "Extension.OMLProcessor.model$ModuleDefinitionRecord"], function (exports, OutSystems, Extension_OMLProcessorModel) {
var OS = OutSystems.Internal;
var ModuleVersionDefinitionRec = (function (_super) {
__extends(ModuleVersionDefinitionRec, _super);
function ModuleVersionDefinitionRec(defaults) {
_super.apply(this, arguments);
}
ModuleVersionDefinitionRec.attributesToDeclare = function () {
return [
this.attr("id", "idAttr", "id", true, false, OS.Types.Integer, function () {
return 0;
}, true), 
this.attr("owner", "ownerAttr", "owner", true, false, OS.Types.Record, function () {
return OS.DataTypes.ImmutableBase.getData(new Extension_OMLProcessorModel.ModuleDefinitionRecord());
}, true, Extension_OMLProcessorModel.ModuleDefinitionRecord), 
this.attr("version", "versionAttr", "version", true, false, OS.Types.Integer, function () {
return 0;
}, true), 
this.attr("uploadedDate", "uploadedDateAttr", "uploadedDate", true, false, OS.Types.DateTime, function () {
return OS.DataTypes.DateTime.defaultValue;
}, true), 
this.attr("lastModified", "lastModifiedAttr", "lastModified", true, false, OS.Types.DateTime, function () {
return OS.DataTypes.DateTime.defaultValue;
}, true), 
this.attr("lastPublishedDate", "lastPublishedDateAttr", "lastPublishedDate", true, false, OS.Types.DateTime, function () {
return OS.DataTypes.DateTime.defaultValue;
}, true), 
this.attr("publishedBy", "publishedByAttr", "publishedBy", true, false, OS.Types.Text, function () {
return "";
}, true)
].concat(_super.attributesToDeclare.call(this));
};
ModuleVersionDefinitionRec.init();
return ModuleVersionDefinitionRec;
})(OS.DataTypes.GenericRecord);
Extension_OMLProcessorModel.ModuleVersionDefinitionRec = ModuleVersionDefinitionRec;

});
define("Extension.OMLProcessor.model$ModuleVersionDefinitionRecord", ["exports", "OutSystems/ClientRuntime/Main", "Extension.OMLProcessor.model", "Extension.OMLProcessor.model$ModuleVersionDefinitionRec"], function (exports, OutSystems, Extension_OMLProcessorModel) {
var OS = OutSystems.Internal;
var ModuleVersionDefinitionRecord = (function (_super) {
__extends(ModuleVersionDefinitionRecord, _super);
function ModuleVersionDefinitionRecord(defaults) {
_super.apply(this, arguments);
}
ModuleVersionDefinitionRecord.attributesToDeclare = function () {
return [
this.attr("ModuleVersionDefinition", "moduleVersionDefinitionAttr", "ModuleVersionDefinition", false, false, OS.Types.Record, function () {
return OS.DataTypes.ImmutableBase.getData(new Extension_OMLProcessorModel.ModuleVersionDefinitionRec());
}, true, Extension_OMLProcessorModel.ModuleVersionDefinitionRec)
].concat(_super.attributesToDeclare.call(this));
};
ModuleVersionDefinitionRecord.fromStructure = function (str) {
return new ModuleVersionDefinitionRecord(new ModuleVersionDefinitionRecord.RecordClass({
moduleVersionDefinitionAttr: OS.DataTypes.ImmutableBase.getData(str)
}));
};
ModuleVersionDefinitionRecord._isAnonymousRecord = true;
ModuleVersionDefinitionRecord.UniqueId = "bcd3d320-6446-5f5a-b461-88aa6ba4641b";
ModuleVersionDefinitionRecord.init();
return ModuleVersionDefinitionRecord;
})(OS.DataTypes.GenericRecord);
Extension_OMLProcessorModel.ModuleVersionDefinitionRecord = ModuleVersionDefinitionRecord;

});
define("Extension.OMLProcessor.model$ModuleVersionDefinitionRecordList", ["exports", "OutSystems/ClientRuntime/Main", "Extension.OMLProcessor.model", "Extension.OMLProcessor.model$ModuleVersionDefinitionRecord"], function (exports, OutSystems, Extension_OMLProcessorModel) {
var OS = OutSystems.Internal;
var ModuleVersionDefinitionRecordList = (function (_super) {
__extends(ModuleVersionDefinitionRecordList, _super);
function ModuleVersionDefinitionRecordList(defaults) {
_super.apply(this, arguments);
}
ModuleVersionDefinitionRecordList.itemType = Extension_OMLProcessorModel.ModuleVersionDefinitionRecord;
return ModuleVersionDefinitionRecordList;
})(OS.DataTypes.GenericRecordList);
Extension_OMLProcessorModel.ModuleVersionDefinitionRecordList = ModuleVersionDefinitionRecordList;

});
define("Extension.OMLProcessor.model$EspacePublishOptionsRecordList", ["exports", "OutSystems/ClientRuntime/Main", "Extension.OMLProcessor.model", "Extension.OMLProcessor.model$EspacePublishOptionsRecord"], function (exports, OutSystems, Extension_OMLProcessorModel) {
var OS = OutSystems.Internal;
var EspacePublishOptionsRecordList = (function (_super) {
__extends(EspacePublishOptionsRecordList, _super);
function EspacePublishOptionsRecordList(defaults) {
_super.apply(this, arguments);
}
EspacePublishOptionsRecordList.itemType = Extension_OMLProcessorModel.EspacePublishOptionsRecord;
return EspacePublishOptionsRecordList;
})(OS.DataTypes.GenericRecordList);
Extension_OMLProcessorModel.EspacePublishOptionsRecordList = EspacePublishOptionsRecordList;

});
define("Extension.OMLProcessor.model$DeploymentZone_PivotalRec", ["exports", "OutSystems/ClientRuntime/Main", "Extension.OMLProcessor.model"], function (exports, OutSystems, Extension_OMLProcessorModel) {
var OS = OutSystems.Internal;
var DeploymentZone_PivotalRec = (function (_super) {
__extends(DeploymentZone_PivotalRec, _super);
function DeploymentZone_PivotalRec(defaults) {
_super.apply(this, arguments);
}
DeploymentZone_PivotalRec.attributesToDeclare = function () {
return [
this.attr("ResultPath", "resultPathAttr", "ResultPath", true, false, OS.Types.Text, function () {
return "";
}, true), 
this.attr("TargetPath", "targetPathAttr", "TargetPath", true, false, OS.Types.Text, function () {
return "";
}, true), 
this.attr("ConfigPath", "configPathAttr", "ConfigPath", false, false, OS.Types.Text, function () {
return "";
}, true), 
this.attr("OnPrepareDeployDone", "onPrepareDeployDoneAttr", "OnPrepareDeployDone", false, false, OS.Types.Text, function () {
return "";
}, true), 
this.attr("OnDeployDone", "onDeployDoneAttr", "OnDeployDone", false, false, OS.Types.Text, function () {
return "";
}, true), 
this.attr("OnUndeploy", "onUndeployAttr", "OnUndeploy", false, false, OS.Types.Text, function () {
return "";
}, true), 
this.attr("OnUpdateConfigurations", "onUpdateConfigurationsAttr", "OnUpdateConfigurations", false, false, OS.Types.Text, function () {
return "";
}, true), 
this.attr("ManualDeployTimeoutInMinutes", "manualDeployTimeoutInMinutesAttr", "ManualDeployTimeoutInMinutes", false, false, OS.Types.Integer, function () {
return 0;
}, true), 
this.attr("AutomaticDeployTimeoutInMinutes", "automaticDeployTimeoutInMinutesAttr", "AutomaticDeployTimeoutInMinutes", false, false, OS.Types.Integer, function () {
return 0;
}, true)
].concat(_super.attributesToDeclare.call(this));
};
DeploymentZone_PivotalRec.init();
return DeploymentZone_PivotalRec;
})(OS.DataTypes.GenericRecord);
Extension_OMLProcessorModel.DeploymentZone_PivotalRec = DeploymentZone_PivotalRec;

});
define("Extension.OMLProcessor.model$DeploymentZone_PivotalRecord", ["exports", "OutSystems/ClientRuntime/Main", "Extension.OMLProcessor.model", "Extension.OMLProcessor.model$DeploymentZone_PivotalRec"], function (exports, OutSystems, Extension_OMLProcessorModel) {
var OS = OutSystems.Internal;
var DeploymentZone_PivotalRecord = (function (_super) {
__extends(DeploymentZone_PivotalRecord, _super);
function DeploymentZone_PivotalRecord(defaults) {
_super.apply(this, arguments);
}
DeploymentZone_PivotalRecord.attributesToDeclare = function () {
return [
this.attr("DeploymentZone_Pivotal", "deploymentZone_PivotalAttr", "DeploymentZone_Pivotal", false, false, OS.Types.Record, function () {
return OS.DataTypes.ImmutableBase.getData(new Extension_OMLProcessorModel.DeploymentZone_PivotalRec());
}, true, Extension_OMLProcessorModel.DeploymentZone_PivotalRec)
].concat(_super.attributesToDeclare.call(this));
};
DeploymentZone_PivotalRecord.fromStructure = function (str) {
return new DeploymentZone_PivotalRecord(new DeploymentZone_PivotalRecord.RecordClass({
deploymentZone_PivotalAttr: OS.DataTypes.ImmutableBase.getData(str)
}));
};
DeploymentZone_PivotalRecord._isAnonymousRecord = true;
DeploymentZone_PivotalRecord.UniqueId = "a2a60c94-c426-f71e-2b90-2304d74b358f";
DeploymentZone_PivotalRecord.init();
return DeploymentZone_PivotalRecord;
})(OS.DataTypes.GenericRecord);
Extension_OMLProcessorModel.DeploymentZone_PivotalRecord = DeploymentZone_PivotalRecord;

});
define("Extension.OMLProcessor.model$DeploymentZone_PivotalRecordList", ["exports", "OutSystems/ClientRuntime/Main", "Extension.OMLProcessor.model", "Extension.OMLProcessor.model$DeploymentZone_PivotalRecord"], function (exports, OutSystems, Extension_OMLProcessorModel) {
var OS = OutSystems.Internal;
var DeploymentZone_PivotalRecordList = (function (_super) {
__extends(DeploymentZone_PivotalRecordList, _super);
function DeploymentZone_PivotalRecordList(defaults) {
_super.apply(this, arguments);
}
DeploymentZone_PivotalRecordList.itemType = Extension_OMLProcessorModel.DeploymentZone_PivotalRecord;
return DeploymentZone_PivotalRecordList;
})(OS.DataTypes.GenericRecordList);
Extension_OMLProcessorModel.DeploymentZone_PivotalRecordList = DeploymentZone_PivotalRecordList;

});
define("Extension.OMLProcessor.model$PublicElementRecordList", ["exports", "OutSystems/ClientRuntime/Main", "Extension.OMLProcessor.model", "Extension.OMLProcessor.model$PublicElementRecord"], function (exports, OutSystems, Extension_OMLProcessorModel) {
var OS = OutSystems.Internal;
var PublicElementRecordList = (function (_super) {
__extends(PublicElementRecordList, _super);
function PublicElementRecordList(defaults) {
_super.apply(this, arguments);
}
PublicElementRecordList.itemType = Extension_OMLProcessorModel.PublicElementRecord;
return PublicElementRecordList;
})(OS.DataTypes.GenericRecordList);
Extension_OMLProcessorModel.PublicElementRecordList = PublicElementRecordList;

});
define("Extension.OMLProcessor.model$FeatureInfoRecordList", ["exports", "OutSystems/ClientRuntime/Main", "Extension.OMLProcessor.model", "Extension.OMLProcessor.model$FeatureInfoRecord"], function (exports, OutSystems, Extension_OMLProcessorModel) {
var OS = OutSystems.Internal;
var FeatureInfoRecordList = (function (_super) {
__extends(FeatureInfoRecordList, _super);
function FeatureInfoRecordList(defaults) {
_super.apply(this, arguments);
}
FeatureInfoRecordList.itemType = Extension_OMLProcessorModel.FeatureInfoRecord;
return FeatureInfoRecordList;
})(OS.DataTypes.GenericRecordList);
Extension_OMLProcessorModel.FeatureInfoRecordList = FeatureInfoRecordList;

});
define("Extension.OMLProcessor.model$EnvironmentInfoRec", ["exports", "OutSystems/ClientRuntime/Main", "Extension.OMLProcessor.model", "Extension.OMLProcessor.model$FeatureInfoRecordList"], function (exports, OutSystems, Extension_OMLProcessorModel) {
var OS = OutSystems.Internal;
var EnvironmentInfoRec = (function (_super) {
__extends(EnvironmentInfoRec, _super);
function EnvironmentInfoRec(defaults) {
_super.apply(this, arguments);
}
EnvironmentInfoRec.attributesToDeclare = function () {
return [
this.attr("Name", "nameAttr", "Name", true, false, OS.Types.Text, function () {
return "";
}, true), 
this.attr("Key", "keyAttr", "Key", true, false, OS.Types.Text, function () {
return "";
}, true), 
this.attr("Version", "versionAttr", "Version", true, false, OS.Types.Text, function () {
return "";
}, true), 
this.attr("Features", "featuresAttr", "Features", true, false, OS.Types.RecordList, function () {
return OS.DataTypes.ImmutableBase.getData(new Extension_OMLProcessorModel.FeatureInfoRecordList());
}, true, Extension_OMLProcessorModel.FeatureInfoRecordList)
].concat(_super.attributesToDeclare.call(this));
};
EnvironmentInfoRec.init();
return EnvironmentInfoRec;
})(OS.DataTypes.GenericRecord);
Extension_OMLProcessorModel.EnvironmentInfoRec = EnvironmentInfoRec;

});
define("Extension.OMLProcessor.model$BinaryDataRecord", ["exports", "OutSystems/ClientRuntime/Main", "Extension.OMLProcessor.model", "Extension.OMLProcessor.model$BinaryDataRec"], function (exports, OutSystems, Extension_OMLProcessorModel) {
var OS = OutSystems.Internal;
var BinaryDataRecord = (function (_super) {
__extends(BinaryDataRecord, _super);
function BinaryDataRecord(defaults) {
_super.apply(this, arguments);
}
BinaryDataRecord.attributesToDeclare = function () {
return [
this.attr("BinaryData", "binaryDataAttr", "BinaryData", false, false, OS.Types.Record, function () {
return OS.DataTypes.ImmutableBase.getData(new Extension_OMLProcessorModel.BinaryDataRec());
}, true, Extension_OMLProcessorModel.BinaryDataRec)
].concat(_super.attributesToDeclare.call(this));
};
BinaryDataRecord.fromStructure = function (str) {
return new BinaryDataRecord(new BinaryDataRecord.RecordClass({
binaryDataAttr: OS.DataTypes.ImmutableBase.getData(str)
}));
};
BinaryDataRecord._isAnonymousRecord = true;
BinaryDataRecord.UniqueId = "a8a54bd8-f519-0612-e397-79fd34e82713";
BinaryDataRecord.init();
return BinaryDataRecord;
})(OS.DataTypes.GenericRecord);
Extension_OMLProcessorModel.BinaryDataRecord = BinaryDataRecord;

});
define("Extension.OMLProcessor.model$BinaryDataRecordList", ["exports", "OutSystems/ClientRuntime/Main", "Extension.OMLProcessor.model", "Extension.OMLProcessor.model$BinaryDataRecord"], function (exports, OutSystems, Extension_OMLProcessorModel) {
var OS = OutSystems.Internal;
var BinaryDataRecordList = (function (_super) {
__extends(BinaryDataRecordList, _super);
function BinaryDataRecordList(defaults) {
_super.apply(this, arguments);
}
BinaryDataRecordList.itemType = Extension_OMLProcessorModel.BinaryDataRecord;
return BinaryDataRecordList;
})(OS.DataTypes.GenericRecordList);
Extension_OMLProcessorModel.BinaryDataRecordList = BinaryDataRecordList;

});
define("Extension.OMLProcessor.model$ActionInfoRecordList", ["exports", "OutSystems/ClientRuntime/Main", "Extension.OMLProcessor.model", "Extension.OMLProcessor.model$ActionInfoRecord"], function (exports, OutSystems, Extension_OMLProcessorModel) {
var OS = OutSystems.Internal;
var ActionInfoRecordList = (function (_super) {
__extends(ActionInfoRecordList, _super);
function ActionInfoRecordList(defaults) {
_super.apply(this, arguments);
}
ActionInfoRecordList.itemType = Extension_OMLProcessorModel.ActionInfoRecord;
return ActionInfoRecordList;
})(OS.DataTypes.GenericRecordList);
Extension_OMLProcessorModel.ActionInfoRecordList = ActionInfoRecordList;

});
define("Extension.OMLProcessor.model$SolutionPack_FileRec", ["exports", "OutSystems/ClientRuntime/Main", "Extension.OMLProcessor.model"], function (exports, OutSystems, Extension_OMLProcessorModel) {
var OS = OutSystems.Internal;
var SolutionPack_FileRec = (function (_super) {
__extends(SolutionPack_FileRec, _super);
function SolutionPack_FileRec(defaults) {
_super.apply(this, arguments);
}
SolutionPack_FileRec.attributesToDeclare = function () {
return [
this.attr("Filename", "filenameAttr", "Filename", true, false, OS.Types.Text, function () {
return "";
}, true), 
this.attr("Directory", "directoryAttr", "Directory", true, false, OS.Types.Boolean, function () {
return false;
}, true), 
this.attr("DateTime", "dateTimeAttr", "DateTime", true, false, OS.Types.DateTime, function () {
return OS.DataTypes.DateTime.defaultValue;
}, true), 
this.attr("Size", "sizeAttr", "Size", true, false, OS.Types.Integer, function () {
return 0;
}, true), 
this.attr("CompressedSize", "compressedSizeAttr", "CompressedSize", true, false, OS.Types.Integer, function () {
return 0;
}, true), 
this.attr("Content", "contentAttr", "Content", true, false, OS.Types.BinaryData, function () {
return OS.DataTypes.BinaryData.defaultValue;
}, true)
].concat(_super.attributesToDeclare.call(this));
};
SolutionPack_FileRec.init();
return SolutionPack_FileRec;
})(OS.DataTypes.GenericRecord);
Extension_OMLProcessorModel.SolutionPack_FileRec = SolutionPack_FileRec;

});
define("Extension.OMLProcessor.model$SolutionPack_FileRecord", ["exports", "OutSystems/ClientRuntime/Main", "Extension.OMLProcessor.model", "Extension.OMLProcessor.model$SolutionPack_FileRec"], function (exports, OutSystems, Extension_OMLProcessorModel) {
var OS = OutSystems.Internal;
var SolutionPack_FileRecord = (function (_super) {
__extends(SolutionPack_FileRecord, _super);
function SolutionPack_FileRecord(defaults) {
_super.apply(this, arguments);
}
SolutionPack_FileRecord.attributesToDeclare = function () {
return [
this.attr("SolutionPack_File", "solutionPack_FileAttr", "SolutionPack_File", false, false, OS.Types.Record, function () {
return OS.DataTypes.ImmutableBase.getData(new Extension_OMLProcessorModel.SolutionPack_FileRec());
}, true, Extension_OMLProcessorModel.SolutionPack_FileRec)
].concat(_super.attributesToDeclare.call(this));
};
SolutionPack_FileRecord.fromStructure = function (str) {
return new SolutionPack_FileRecord(new SolutionPack_FileRecord.RecordClass({
solutionPack_FileAttr: OS.DataTypes.ImmutableBase.getData(str)
}));
};
SolutionPack_FileRecord._isAnonymousRecord = true;
SolutionPack_FileRecord.UniqueId = "71e0d611-6660-61ba-d0a0-7aa1cc274ffc";
SolutionPack_FileRecord.init();
return SolutionPack_FileRecord;
})(OS.DataTypes.GenericRecord);
Extension_OMLProcessorModel.SolutionPack_FileRecord = SolutionPack_FileRecord;

});
define("Extension.OMLProcessor.model$SolutionPack_FileRecordList", ["exports", "OutSystems/ClientRuntime/Main", "Extension.OMLProcessor.model", "Extension.OMLProcessor.model$SolutionPack_FileRecord"], function (exports, OutSystems, Extension_OMLProcessorModel) {
var OS = OutSystems.Internal;
var SolutionPack_FileRecordList = (function (_super) {
__extends(SolutionPack_FileRecordList, _super);
function SolutionPack_FileRecordList(defaults) {
_super.apply(this, arguments);
}
SolutionPack_FileRecordList.itemType = Extension_OMLProcessorModel.SolutionPack_FileRecord;
return SolutionPack_FileRecordList;
})(OS.DataTypes.GenericRecordList);
Extension_OMLProcessorModel.SolutionPack_FileRecordList = SolutionPack_FileRecordList;

});
define("Extension.OMLProcessor.model$NativePluginConfigurationRecord", ["exports", "OutSystems/ClientRuntime/Main", "Extension.OMLProcessor.model", "Extension.OMLProcessor.model$NativePluginConfigurationRec"], function (exports, OutSystems, Extension_OMLProcessorModel) {
var OS = OutSystems.Internal;
var NativePluginConfigurationRecord = (function (_super) {
__extends(NativePluginConfigurationRecord, _super);
function NativePluginConfigurationRecord(defaults) {
_super.apply(this, arguments);
}
NativePluginConfigurationRecord.attributesToDeclare = function () {
return [
this.attr("NativePluginConfiguration", "nativePluginConfigurationAttr", "NativePluginConfiguration", false, false, OS.Types.Record, function () {
return OS.DataTypes.ImmutableBase.getData(new Extension_OMLProcessorModel.NativePluginConfigurationRec());
}, true, Extension_OMLProcessorModel.NativePluginConfigurationRec)
].concat(_super.attributesToDeclare.call(this));
};
NativePluginConfigurationRecord.fromStructure = function (str) {
return new NativePluginConfigurationRecord(new NativePluginConfigurationRecord.RecordClass({
nativePluginConfigurationAttr: OS.DataTypes.ImmutableBase.getData(str)
}));
};
NativePluginConfigurationRecord._isAnonymousRecord = true;
NativePluginConfigurationRecord.UniqueId = "66ce4eda-9f62-2887-36cd-44aed048343b";
NativePluginConfigurationRecord.init();
return NativePluginConfigurationRecord;
})(OS.DataTypes.GenericRecord);
Extension_OMLProcessorModel.NativePluginConfigurationRecord = NativePluginConfigurationRecord;

});
define("Extension.OMLProcessor.model$DALDBConfigProviderRec", ["exports", "OutSystems/ClientRuntime/Main", "Extension.OMLProcessor.model"], function (exports, OutSystems, Extension_OMLProcessorModel) {
var OS = OutSystems.Internal;
var DALDBConfigProviderRec = (function (_super) {
__extends(DALDBConfigProviderRec, _super);
function DALDBConfigProviderRec(defaults) {
_super.apply(this, arguments);
}
DALDBConfigProviderRec.attributesToDeclare = function () {
return [
this.attr("DisplayName", "displayNameAttr", "DisplayName", true, false, OS.Types.Text, function () {
return "";
}, true), 
this.attr("ProviderKey", "providerKeyAttr", "ProviderKey", true, false, OS.Types.Text, function () {
return "";
}, true)
].concat(_super.attributesToDeclare.call(this));
};
DALDBConfigProviderRec.init();
return DALDBConfigProviderRec;
})(OS.DataTypes.GenericRecord);
Extension_OMLProcessorModel.DALDBConfigProviderRec = DALDBConfigProviderRec;

});
define("Extension.OMLProcessor.model$DALDBConfigProviderRecord", ["exports", "OutSystems/ClientRuntime/Main", "Extension.OMLProcessor.model", "Extension.OMLProcessor.model$DALDBConfigProviderRec"], function (exports, OutSystems, Extension_OMLProcessorModel) {
var OS = OutSystems.Internal;
var DALDBConfigProviderRecord = (function (_super) {
__extends(DALDBConfigProviderRecord, _super);
function DALDBConfigProviderRecord(defaults) {
_super.apply(this, arguments);
}
DALDBConfigProviderRecord.attributesToDeclare = function () {
return [
this.attr("DALDBConfigProvider", "dALDBConfigProviderAttr", "DALDBConfigProvider", false, false, OS.Types.Record, function () {
return OS.DataTypes.ImmutableBase.getData(new Extension_OMLProcessorModel.DALDBConfigProviderRec());
}, true, Extension_OMLProcessorModel.DALDBConfigProviderRec)
].concat(_super.attributesToDeclare.call(this));
};
DALDBConfigProviderRecord.fromStructure = function (str) {
return new DALDBConfigProviderRecord(new DALDBConfigProviderRecord.RecordClass({
dALDBConfigProviderAttr: OS.DataTypes.ImmutableBase.getData(str)
}));
};
DALDBConfigProviderRecord._isAnonymousRecord = true;
DALDBConfigProviderRecord.UniqueId = "d175a9bf-6386-37d2-361d-b26a7fd7ea5e";
DALDBConfigProviderRecord.init();
return DALDBConfigProviderRecord;
})(OS.DataTypes.GenericRecord);
Extension_OMLProcessorModel.DALDBConfigProviderRecord = DALDBConfigProviderRecord;

});
define("Extension.OMLProcessor.model$DALDBConfigProviderRecordList", ["exports", "OutSystems/ClientRuntime/Main", "Extension.OMLProcessor.model", "Extension.OMLProcessor.model$DALDBConfigProviderRecord"], function (exports, OutSystems, Extension_OMLProcessorModel) {
var OS = OutSystems.Internal;
var DALDBConfigProviderRecordList = (function (_super) {
__extends(DALDBConfigProviderRecordList, _super);
function DALDBConfigProviderRecordList(defaults) {
_super.apply(this, arguments);
}
DALDBConfigProviderRecordList.itemType = Extension_OMLProcessorModel.DALDBConfigProviderRecord;
return DALDBConfigProviderRecordList;
})(OS.DataTypes.GenericRecordList);
Extension_OMLProcessorModel.DALDBConfigProviderRecordList = DALDBConfigProviderRecordList;

});
define("Extension.OMLProcessor.model$QueuedNativeBuildResultRecord", ["exports", "OutSystems/ClientRuntime/Main", "Extension.OMLProcessor.model", "Extension.OMLProcessor.model$QueuedNativeBuildResultRec"], function (exports, OutSystems, Extension_OMLProcessorModel) {
var OS = OutSystems.Internal;
var QueuedNativeBuildResultRecord = (function (_super) {
__extends(QueuedNativeBuildResultRecord, _super);
function QueuedNativeBuildResultRecord(defaults) {
_super.apply(this, arguments);
}
QueuedNativeBuildResultRecord.attributesToDeclare = function () {
return [
this.attr("QueuedNativeBuildResult", "queuedNativeBuildResultAttr", "QueuedNativeBuildResult", false, false, OS.Types.Record, function () {
return OS.DataTypes.ImmutableBase.getData(new Extension_OMLProcessorModel.QueuedNativeBuildResultRec());
}, true, Extension_OMLProcessorModel.QueuedNativeBuildResultRec)
].concat(_super.attributesToDeclare.call(this));
};
QueuedNativeBuildResultRecord.fromStructure = function (str) {
return new QueuedNativeBuildResultRecord(new QueuedNativeBuildResultRecord.RecordClass({
queuedNativeBuildResultAttr: OS.DataTypes.ImmutableBase.getData(str)
}));
};
QueuedNativeBuildResultRecord._isAnonymousRecord = true;
QueuedNativeBuildResultRecord.UniqueId = "6f7a3011-b543-3976-1ee9-f992611d775d";
QueuedNativeBuildResultRecord.init();
return QueuedNativeBuildResultRecord;
})(OS.DataTypes.GenericRecord);
Extension_OMLProcessorModel.QueuedNativeBuildResultRecord = QueuedNativeBuildResultRecord;

});
define("Extension.OMLProcessor.model$PTA_AreaRec", ["exports", "OutSystems/ClientRuntime/Main", "Extension.OMLProcessor.model"], function (exports, OutSystems, Extension_OMLProcessorModel) {
var OS = OutSystems.Internal;
var PTA_AreaRec = (function (_super) {
__extends(PTA_AreaRec, _super);
function PTA_AreaRec(defaults) {
_super.apply(this, arguments);
}
PTA_AreaRec.attributesToDeclare = function () {
return [
this.attr("Username", "usernameAttr", "Username", true, false, OS.Types.Text, function () {
return "";
}, true), 
this.attr("Name", "nameAttr", "Name", true, false, OS.Types.Text, function () {
return "";
}, true)
].concat(_super.attributesToDeclare.call(this));
};
PTA_AreaRec.init();
return PTA_AreaRec;
})(OS.DataTypes.GenericRecord);
Extension_OMLProcessorModel.PTA_AreaRec = PTA_AreaRec;

});
define("Extension.OMLProcessor.model$FeatureRec", ["exports", "OutSystems/ClientRuntime/Main", "Extension.OMLProcessor.model"], function (exports, OutSystems, Extension_OMLProcessorModel) {
var OS = OutSystems.Internal;
var FeatureRec = (function (_super) {
__extends(FeatureRec, _super);
function FeatureRec(defaults) {
_super.apply(this, arguments);
}
FeatureRec.attributesToDeclare = function () {
return [
this.attr("Id", "idAttr", "Id", true, false, OS.Types.Text, function () {
return "";
}, true)
].concat(_super.attributesToDeclare.call(this));
};
FeatureRec.fromStructure = function (str) {
return new FeatureRec(new FeatureRec.RecordClass({
idAttr: OS.DataTypes.ImmutableBase.getData(str)
}));
};
FeatureRec.init();
return FeatureRec;
})(OS.DataTypes.GenericRecord);
Extension_OMLProcessorModel.FeatureRec = FeatureRec;

});
define("Extension.OMLProcessor.model$FeatureRecord", ["exports", "OutSystems/ClientRuntime/Main", "Extension.OMLProcessor.model", "Extension.OMLProcessor.model$FeatureRec"], function (exports, OutSystems, Extension_OMLProcessorModel) {
var OS = OutSystems.Internal;
var FeatureRecord = (function (_super) {
__extends(FeatureRecord, _super);
function FeatureRecord(defaults) {
_super.apply(this, arguments);
}
FeatureRecord.attributesToDeclare = function () {
return [
this.attr("Feature", "featureAttr", "Feature", false, false, OS.Types.Record, function () {
return OS.DataTypes.ImmutableBase.getData(new Extension_OMLProcessorModel.FeatureRec());
}, true, Extension_OMLProcessorModel.FeatureRec)
].concat(_super.attributesToDeclare.call(this));
};
FeatureRecord.fromStructure = function (str) {
return new FeatureRecord(new FeatureRecord.RecordClass({
featureAttr: OS.DataTypes.ImmutableBase.getData(str)
}));
};
FeatureRecord._isAnonymousRecord = true;
FeatureRecord.UniqueId = "7478f1cf-1b27-04d2-366e-b309cbccfbe9";
FeatureRecord.init();
return FeatureRecord;
})(OS.DataTypes.GenericRecord);
Extension_OMLProcessorModel.FeatureRecord = FeatureRecord;

});
define("Extension.OMLProcessor.model$EntityDefinitionRecordList", ["exports", "OutSystems/ClientRuntime/Main", "Extension.OMLProcessor.model", "Extension.OMLProcessor.model$EntityDefinitionRecord"], function (exports, OutSystems, Extension_OMLProcessorModel) {
var OS = OutSystems.Internal;
var EntityDefinitionRecordList = (function (_super) {
__extends(EntityDefinitionRecordList, _super);
function EntityDefinitionRecordList(defaults) {
_super.apply(this, arguments);
}
EntityDefinitionRecordList.itemType = Extension_OMLProcessorModel.EntityDefinitionRecord;
return EntityDefinitionRecordList;
})(OS.DataTypes.GenericRecordList);
Extension_OMLProcessorModel.EntityDefinitionRecordList = EntityDefinitionRecordList;

});
define("Extension.OMLProcessor.model$QueuedNativeBuildResultRecordList", ["exports", "OutSystems/ClientRuntime/Main", "Extension.OMLProcessor.model", "Extension.OMLProcessor.model$QueuedNativeBuildResultRecord"], function (exports, OutSystems, Extension_OMLProcessorModel) {
var OS = OutSystems.Internal;
var QueuedNativeBuildResultRecordList = (function (_super) {
__extends(QueuedNativeBuildResultRecordList, _super);
function QueuedNativeBuildResultRecordList(defaults) {
_super.apply(this, arguments);
}
QueuedNativeBuildResultRecordList.itemType = Extension_OMLProcessorModel.QueuedNativeBuildResultRecord;
return QueuedNativeBuildResultRecordList;
})(OS.DataTypes.GenericRecordList);
Extension_OMLProcessorModel.QueuedNativeBuildResultRecordList = QueuedNativeBuildResultRecordList;

});
define("Extension.OMLProcessor.model$DeploymentZone_VMRec", ["exports", "OutSystems/ClientRuntime/Main", "Extension.OMLProcessor.model", "Extension.OMLProcessor.model$TextRecordList"], function (exports, OutSystems, Extension_OMLProcessorModel) {
var OS = OutSystems.Internal;
var DeploymentZone_VMRec = (function (_super) {
__extends(DeploymentZone_VMRec, _super);
function DeploymentZone_VMRec(defaults) {
_super.apply(this, arguments);
}
DeploymentZone_VMRec.attributesToDeclare = function () {
return [
this.attr("FEServerIds", "fEServerIdsAttr", "FEServerIds", true, false, OS.Types.RecordList, function () {
return OS.DataTypes.ImmutableBase.getData(new Extension_OMLProcessorModel.TextRecordList());
}, true, Extension_OMLProcessorModel.TextRecordList), 
this.attr("IncludesAllServers", "includesAllServersAttr", "IncludesAllServers", true, false, OS.Types.Boolean, function () {
return false;
}, true)
].concat(_super.attributesToDeclare.call(this));
};
DeploymentZone_VMRec.init();
return DeploymentZone_VMRec;
})(OS.DataTypes.GenericRecord);
Extension_OMLProcessorModel.DeploymentZone_VMRec = DeploymentZone_VMRec;

});
define("Extension.OMLProcessor.model$DeploymentZone_VMRecord", ["exports", "OutSystems/ClientRuntime/Main", "Extension.OMLProcessor.model", "Extension.OMLProcessor.model$DeploymentZone_VMRec"], function (exports, OutSystems, Extension_OMLProcessorModel) {
var OS = OutSystems.Internal;
var DeploymentZone_VMRecord = (function (_super) {
__extends(DeploymentZone_VMRecord, _super);
function DeploymentZone_VMRecord(defaults) {
_super.apply(this, arguments);
}
DeploymentZone_VMRecord.attributesToDeclare = function () {
return [
this.attr("DeploymentZone_VM", "deploymentZone_VMAttr", "DeploymentZone_VM", false, false, OS.Types.Record, function () {
return OS.DataTypes.ImmutableBase.getData(new Extension_OMLProcessorModel.DeploymentZone_VMRec());
}, true, Extension_OMLProcessorModel.DeploymentZone_VMRec)
].concat(_super.attributesToDeclare.call(this));
};
DeploymentZone_VMRecord.fromStructure = function (str) {
return new DeploymentZone_VMRecord(new DeploymentZone_VMRecord.RecordClass({
deploymentZone_VMAttr: OS.DataTypes.ImmutableBase.getData(str)
}));
};
DeploymentZone_VMRecord._isAnonymousRecord = true;
DeploymentZone_VMRecord.UniqueId = "eee227b7-e2cd-04fc-8fff-2b0c6e8ea86a";
DeploymentZone_VMRecord.init();
return DeploymentZone_VMRecord;
})(OS.DataTypes.GenericRecord);
Extension_OMLProcessorModel.DeploymentZone_VMRecord = DeploymentZone_VMRecord;

});
define("Extension.OMLProcessor.model$DeploymentZone_VMRecordList", ["exports", "OutSystems/ClientRuntime/Main", "Extension.OMLProcessor.model", "Extension.OMLProcessor.model$DeploymentZone_VMRecord"], function (exports, OutSystems, Extension_OMLProcessorModel) {
var OS = OutSystems.Internal;
var DeploymentZone_VMRecordList = (function (_super) {
__extends(DeploymentZone_VMRecordList, _super);
function DeploymentZone_VMRecordList(defaults) {
_super.apply(this, arguments);
}
DeploymentZone_VMRecordList.itemType = Extension_OMLProcessorModel.DeploymentZone_VMRecord;
return DeploymentZone_VMRecordList;
})(OS.DataTypes.GenericRecordList);
Extension_OMLProcessorModel.DeploymentZone_VMRecordList = DeploymentZone_VMRecordList;

});
define("Extension.OMLProcessor.model$PTA_AreaRecord", ["exports", "OutSystems/ClientRuntime/Main", "Extension.OMLProcessor.model", "Extension.OMLProcessor.model$PTA_AreaRec"], function (exports, OutSystems, Extension_OMLProcessorModel) {
var OS = OutSystems.Internal;
var PTA_AreaRecord = (function (_super) {
__extends(PTA_AreaRecord, _super);
function PTA_AreaRecord(defaults) {
_super.apply(this, arguments);
}
PTA_AreaRecord.attributesToDeclare = function () {
return [
this.attr("PTA_Area", "pTA_AreaAttr", "PTA_Area", false, false, OS.Types.Record, function () {
return OS.DataTypes.ImmutableBase.getData(new Extension_OMLProcessorModel.PTA_AreaRec());
}, true, Extension_OMLProcessorModel.PTA_AreaRec)
].concat(_super.attributesToDeclare.call(this));
};
PTA_AreaRecord.fromStructure = function (str) {
return new PTA_AreaRecord(new PTA_AreaRecord.RecordClass({
pTA_AreaAttr: OS.DataTypes.ImmutableBase.getData(str)
}));
};
PTA_AreaRecord._isAnonymousRecord = true;
PTA_AreaRecord.UniqueId = "c43cd7be-a08d-0277-ccc1-3f6ecbd2f65b";
PTA_AreaRecord.init();
return PTA_AreaRecord;
})(OS.DataTypes.GenericRecord);
Extension_OMLProcessorModel.PTA_AreaRecord = PTA_AreaRecord;

});
define("Extension.OMLProcessor.model$PTA_AreaRecordList", ["exports", "OutSystems/ClientRuntime/Main", "Extension.OMLProcessor.model", "Extension.OMLProcessor.model$PTA_AreaRecord"], function (exports, OutSystems, Extension_OMLProcessorModel) {
var OS = OutSystems.Internal;
var PTA_AreaRecordList = (function (_super) {
__extends(PTA_AreaRecordList, _super);
function PTA_AreaRecordList(defaults) {
_super.apply(this, arguments);
}
PTA_AreaRecordList.itemType = Extension_OMLProcessorModel.PTA_AreaRecord;
return PTA_AreaRecordList;
})(OS.DataTypes.GenericRecordList);
Extension_OMLProcessorModel.PTA_AreaRecordList = PTA_AreaRecordList;

});
define("Extension.OMLProcessor.model$ModuleVersionFeatureRecordList", ["exports", "OutSystems/ClientRuntime/Main", "Extension.OMLProcessor.model", "Extension.OMLProcessor.model$ModuleVersionFeatureRecord"], function (exports, OutSystems, Extension_OMLProcessorModel) {
var OS = OutSystems.Internal;
var ModuleVersionFeatureRecordList = (function (_super) {
__extends(ModuleVersionFeatureRecordList, _super);
function ModuleVersionFeatureRecordList(defaults) {
_super.apply(this, arguments);
}
ModuleVersionFeatureRecordList.itemType = Extension_OMLProcessorModel.ModuleVersionFeatureRecord;
return ModuleVersionFeatureRecordList;
})(OS.DataTypes.GenericRecordList);
Extension_OMLProcessorModel.ModuleVersionFeatureRecordList = ModuleVersionFeatureRecordList;

});
define("Extension.OMLProcessor.model$DALHighlightingDefinitionRec", ["exports", "OutSystems/ClientRuntime/Main", "Extension.OMLProcessor.model", "Extension.OMLProcessor.model$TextRecordList"], function (exports, OutSystems, Extension_OMLProcessorModel) {
var OS = OutSystems.Internal;
var DALHighlightingDefinitionRec = (function (_super) {
__extends(DALHighlightingDefinitionRec, _super);
function DALHighlightingDefinitionRec(defaults) {
_super.apply(this, arguments);
}
DALHighlightingDefinitionRec.attributesToDeclare = function () {
return [
this.attr("Hash", "hashAttr", "Hash", true, false, OS.Types.Text, function () {
return "";
}, true), 
this.attr("Keywords", "keywordsAttr", "Keywords", true, false, OS.Types.RecordList, function () {
return OS.DataTypes.ImmutableBase.getData(new Extension_OMLProcessorModel.TextRecordList());
}, true, Extension_OMLProcessorModel.TextRecordList), 
this.attr("Functions", "functionsAttr", "Functions", true, false, OS.Types.RecordList, function () {
return OS.DataTypes.ImmutableBase.getData(new Extension_OMLProcessorModel.TextRecordList());
}, true, Extension_OMLProcessorModel.TextRecordList), 
this.attr("Operators", "operatorsAttr", "Operators", true, false, OS.Types.RecordList, function () {
return OS.DataTypes.ImmutableBase.getData(new Extension_OMLProcessorModel.TextRecordList());
}, true, Extension_OMLProcessorModel.TextRecordList), 
this.attr("DataTypes", "dataTypesAttr", "DataTypes", true, false, OS.Types.RecordList, function () {
return OS.DataTypes.ImmutableBase.getData(new Extension_OMLProcessorModel.TextRecordList());
}, true, Extension_OMLProcessorModel.TextRecordList)
].concat(_super.attributesToDeclare.call(this));
};
DALHighlightingDefinitionRec.init();
return DALHighlightingDefinitionRec;
})(OS.DataTypes.GenericRecord);
Extension_OMLProcessorModel.DALHighlightingDefinitionRec = DALHighlightingDefinitionRec;

});
define("Extension.OMLProcessor.model$DALHighlightingDefinitionRecord", ["exports", "OutSystems/ClientRuntime/Main", "Extension.OMLProcessor.model", "Extension.OMLProcessor.model$DALHighlightingDefinitionRec"], function (exports, OutSystems, Extension_OMLProcessorModel) {
var OS = OutSystems.Internal;
var DALHighlightingDefinitionRecord = (function (_super) {
__extends(DALHighlightingDefinitionRecord, _super);
function DALHighlightingDefinitionRecord(defaults) {
_super.apply(this, arguments);
}
DALHighlightingDefinitionRecord.attributesToDeclare = function () {
return [
this.attr("DALHighlightingDefinition", "dALHighlightingDefinitionAttr", "DALHighlightingDefinition", false, false, OS.Types.Record, function () {
return OS.DataTypes.ImmutableBase.getData(new Extension_OMLProcessorModel.DALHighlightingDefinitionRec());
}, true, Extension_OMLProcessorModel.DALHighlightingDefinitionRec)
].concat(_super.attributesToDeclare.call(this));
};
DALHighlightingDefinitionRecord.fromStructure = function (str) {
return new DALHighlightingDefinitionRecord(new DALHighlightingDefinitionRecord.RecordClass({
dALHighlightingDefinitionAttr: OS.DataTypes.ImmutableBase.getData(str)
}));
};
DALHighlightingDefinitionRecord._isAnonymousRecord = true;
DALHighlightingDefinitionRecord.UniqueId = "7faf1a6d-fbb7-48f3-6d1e-e726338a60e3";
DALHighlightingDefinitionRecord.init();
return DALHighlightingDefinitionRecord;
})(OS.DataTypes.GenericRecord);
Extension_OMLProcessorModel.DALHighlightingDefinitionRecord = DALHighlightingDefinitionRecord;

});
define("Extension.OMLProcessor.model$DALDBConfigEnumValueRecord", ["exports", "OutSystems/ClientRuntime/Main", "Extension.OMLProcessor.model", "Extension.OMLProcessor.model$DALDBConfigEnumValueRec"], function (exports, OutSystems, Extension_OMLProcessorModel) {
var OS = OutSystems.Internal;
var DALDBConfigEnumValueRecord = (function (_super) {
__extends(DALDBConfigEnumValueRecord, _super);
function DALDBConfigEnumValueRecord(defaults) {
_super.apply(this, arguments);
}
DALDBConfigEnumValueRecord.attributesToDeclare = function () {
return [
this.attr("DALDBConfigEnumValue", "dALDBConfigEnumValueAttr", "DALDBConfigEnumValue", false, false, OS.Types.Record, function () {
return OS.DataTypes.ImmutableBase.getData(new Extension_OMLProcessorModel.DALDBConfigEnumValueRec());
}, true, Extension_OMLProcessorModel.DALDBConfigEnumValueRec)
].concat(_super.attributesToDeclare.call(this));
};
DALDBConfigEnumValueRecord.fromStructure = function (str) {
return new DALDBConfigEnumValueRecord(new DALDBConfigEnumValueRecord.RecordClass({
dALDBConfigEnumValueAttr: OS.DataTypes.ImmutableBase.getData(str)
}));
};
DALDBConfigEnumValueRecord._isAnonymousRecord = true;
DALDBConfigEnumValueRecord.UniqueId = "f383a9b4-4b6f-6b31-bd18-a7dbcc828b39";
DALDBConfigEnumValueRecord.init();
return DALDBConfigEnumValueRecord;
})(OS.DataTypes.GenericRecord);
Extension_OMLProcessorModel.DALDBConfigEnumValueRecord = DALDBConfigEnumValueRecord;

});
define("Extension.OMLProcessor.model$DALDBConfigEnumValueRecordList", ["exports", "OutSystems/ClientRuntime/Main", "Extension.OMLProcessor.model", "Extension.OMLProcessor.model$DALDBConfigEnumValueRecord"], function (exports, OutSystems, Extension_OMLProcessorModel) {
var OS = OutSystems.Internal;
var DALDBConfigEnumValueRecordList = (function (_super) {
__extends(DALDBConfigEnumValueRecordList, _super);
function DALDBConfigEnumValueRecordList(defaults) {
_super.apply(this, arguments);
}
DALDBConfigEnumValueRecordList.itemType = Extension_OMLProcessorModel.DALDBConfigEnumValueRecord;
return DALDBConfigEnumValueRecordList;
})(OS.DataTypes.GenericRecordList);
Extension_OMLProcessorModel.DALDBConfigEnumValueRecordList = DALDBConfigEnumValueRecordList;

});
define("Extension.OMLProcessor.model$GenericRecordDescriptionRecord", ["exports", "OutSystems/ClientRuntime/Main", "Extension.OMLProcessor.model", "Extension.OMLProcessor.model$GenericRecordDescriptionRec"], function (exports, OutSystems, Extension_OMLProcessorModel) {
var OS = OutSystems.Internal;
var GenericRecordDescriptionRecord = (function (_super) {
__extends(GenericRecordDescriptionRecord, _super);
function GenericRecordDescriptionRecord(defaults) {
_super.apply(this, arguments);
}
GenericRecordDescriptionRecord.attributesToDeclare = function () {
return [
this.attr("GenericRecordDescription", "genericRecordDescriptionAttr", "GenericRecordDescription", false, false, OS.Types.Record, function () {
return OS.DataTypes.ImmutableBase.getData(new Extension_OMLProcessorModel.GenericRecordDescriptionRec());
}, true, Extension_OMLProcessorModel.GenericRecordDescriptionRec)
].concat(_super.attributesToDeclare.call(this));
};
GenericRecordDescriptionRecord.fromStructure = function (str) {
return new GenericRecordDescriptionRecord(new GenericRecordDescriptionRecord.RecordClass({
genericRecordDescriptionAttr: OS.DataTypes.ImmutableBase.getData(str)
}));
};
GenericRecordDescriptionRecord._isAnonymousRecord = true;
GenericRecordDescriptionRecord.UniqueId = "80e9e74f-fc80-8705-a234-dcc5a8dd2f0b";
GenericRecordDescriptionRecord.init();
return GenericRecordDescriptionRecord;
})(OS.DataTypes.GenericRecord);
Extension_OMLProcessorModel.GenericRecordDescriptionRecord = GenericRecordDescriptionRecord;

});
define("Extension.OMLProcessor.model$SolutionPack_SolutionDBCatalogRecordList", ["exports", "OutSystems/ClientRuntime/Main", "Extension.OMLProcessor.model", "Extension.OMLProcessor.model$SolutionPack_SolutionDBCatalogRecord"], function (exports, OutSystems, Extension_OMLProcessorModel) {
var OS = OutSystems.Internal;
var SolutionPack_SolutionDBCatalogRecordList = (function (_super) {
__extends(SolutionPack_SolutionDBCatalogRecordList, _super);
function SolutionPack_SolutionDBCatalogRecordList(defaults) {
_super.apply(this, arguments);
}
SolutionPack_SolutionDBCatalogRecordList.itemType = Extension_OMLProcessorModel.SolutionPack_SolutionDBCatalogRecord;
return SolutionPack_SolutionDBCatalogRecordList;
})(OS.DataTypes.GenericRecordList);
Extension_OMLProcessorModel.SolutionPack_SolutionDBCatalogRecordList = SolutionPack_SolutionDBCatalogRecordList;

});
define("Extension.OMLProcessor.model$ApplicationModuleDetailsRecordList", ["exports", "OutSystems/ClientRuntime/Main", "Extension.OMLProcessor.model", "Extension.OMLProcessor.model$ApplicationModuleDetailsRecord"], function (exports, OutSystems, Extension_OMLProcessorModel) {
var OS = OutSystems.Internal;
var ApplicationModuleDetailsRecordList = (function (_super) {
__extends(ApplicationModuleDetailsRecordList, _super);
function ApplicationModuleDetailsRecordList(defaults) {
_super.apply(this, arguments);
}
ApplicationModuleDetailsRecordList.itemType = Extension_OMLProcessorModel.ApplicationModuleDetailsRecord;
return ApplicationModuleDetailsRecordList;
})(OS.DataTypes.GenericRecordList);
Extension_OMLProcessorModel.ApplicationModuleDetailsRecordList = ApplicationModuleDetailsRecordList;

});
define("Extension.OMLProcessor.model$DeploymentZone_DockerRec", ["exports", "OutSystems/ClientRuntime/Main", "Extension.OMLProcessor.model"], function (exports, OutSystems, Extension_OMLProcessorModel) {
var OS = OutSystems.Internal;
var DeploymentZone_DockerRec = (function (_super) {
__extends(DeploymentZone_DockerRec, _super);
function DeploymentZone_DockerRec(defaults) {
_super.apply(this, arguments);
}
DeploymentZone_DockerRec.attributesToDeclare = function () {
return [
this.attr("ResultPath", "resultPathAttr", "ResultPath", true, false, OS.Types.Text, function () {
return "";
}, true), 
this.attr("TargetPath", "targetPathAttr", "TargetPath", true, false, OS.Types.Text, function () {
return "";
}, true), 
this.attr("FromImageReference", "fromImageReferenceAttr", "FromImageReference", true, false, OS.Types.Text, function () {
return "";
}, true), 
this.attr("ConfigPath", "configPathAttr", "ConfigPath", true, false, OS.Types.Text, function () {
return "";
}, true), 
this.attr("OnPrepareDeployDone", "onPrepareDeployDoneAttr", "OnPrepareDeployDone", false, false, OS.Types.Text, function () {
return "";
}, true), 
this.attr("OnDeployDone", "onDeployDoneAttr", "OnDeployDone", false, false, OS.Types.Text, function () {
return "";
}, true), 
this.attr("OnUndeploy", "onUndeployAttr", "OnUndeploy", false, false, OS.Types.Text, function () {
return "";
}, true), 
this.attr("OnUpdateConfigurations", "onUpdateConfigurationsAttr", "OnUpdateConfigurations", false, false, OS.Types.Text, function () {
return "";
}, true), 
this.attr("ManualDeployTimeoutInMinutes", "manualDeployTimeoutInMinutesAttr", "ManualDeployTimeoutInMinutes", false, false, OS.Types.Integer, function () {
return 0;
}, true), 
this.attr("AutomaticDeployTimeoutInMinutes", "automaticDeployTimeoutInMinutesAttr", "AutomaticDeployTimeoutInMinutes", false, false, OS.Types.Integer, function () {
return 0;
}, true)
].concat(_super.attributesToDeclare.call(this));
};
DeploymentZone_DockerRec.init();
return DeploymentZone_DockerRec;
})(OS.DataTypes.GenericRecord);
Extension_OMLProcessorModel.DeploymentZone_DockerRec = DeploymentZone_DockerRec;

});
define("Extension.OMLProcessor.model$DeploymentZone_DockerRecord", ["exports", "OutSystems/ClientRuntime/Main", "Extension.OMLProcessor.model", "Extension.OMLProcessor.model$DeploymentZone_DockerRec"], function (exports, OutSystems, Extension_OMLProcessorModel) {
var OS = OutSystems.Internal;
var DeploymentZone_DockerRecord = (function (_super) {
__extends(DeploymentZone_DockerRecord, _super);
function DeploymentZone_DockerRecord(defaults) {
_super.apply(this, arguments);
}
DeploymentZone_DockerRecord.attributesToDeclare = function () {
return [
this.attr("DeploymentZone_Docker", "deploymentZone_DockerAttr", "DeploymentZone_Docker", false, false, OS.Types.Record, function () {
return OS.DataTypes.ImmutableBase.getData(new Extension_OMLProcessorModel.DeploymentZone_DockerRec());
}, true, Extension_OMLProcessorModel.DeploymentZone_DockerRec)
].concat(_super.attributesToDeclare.call(this));
};
DeploymentZone_DockerRecord.fromStructure = function (str) {
return new DeploymentZone_DockerRecord(new DeploymentZone_DockerRecord.RecordClass({
deploymentZone_DockerAttr: OS.DataTypes.ImmutableBase.getData(str)
}));
};
DeploymentZone_DockerRecord._isAnonymousRecord = true;
DeploymentZone_DockerRecord.UniqueId = "d64d30a7-5f67-e3b7-4bc5-092db5df27e7";
DeploymentZone_DockerRecord.init();
return DeploymentZone_DockerRecord;
})(OS.DataTypes.GenericRecord);
Extension_OMLProcessorModel.DeploymentZone_DockerRecord = DeploymentZone_DockerRecord;

});
define("Extension.OMLProcessor.model$GenericRecordDescriptionRecordList", ["exports", "OutSystems/ClientRuntime/Main", "Extension.OMLProcessor.model", "Extension.OMLProcessor.model$GenericRecordDescriptionRecord"], function (exports, OutSystems, Extension_OMLProcessorModel) {
var OS = OutSystems.Internal;
var GenericRecordDescriptionRecordList = (function (_super) {
__extends(GenericRecordDescriptionRecordList, _super);
function GenericRecordDescriptionRecordList(defaults) {
_super.apply(this, arguments);
}
GenericRecordDescriptionRecordList.itemType = Extension_OMLProcessorModel.GenericRecordDescriptionRecord;
return GenericRecordDescriptionRecordList;
})(OS.DataTypes.GenericRecordList);
Extension_OMLProcessorModel.GenericRecordDescriptionRecordList = GenericRecordDescriptionRecordList;

});
define("Extension.OMLProcessor.model$DeploymentZoneRec", ["exports", "OutSystems/ClientRuntime/Main", "Extension.OMLProcessor.model", "Extension.OMLProcessor.model$DeploymentZone_AmazonECSRecord", "Extension.OMLProcessor.model$DeploymentZone_AzureContainerServiceRecord", "Extension.OMLProcessor.model$DeploymentZone_DockerRecord", "Extension.OMLProcessor.model$DeploymentZone_PivotalRecord", "Extension.OMLProcessor.model$DeploymentZone_VMRecord", "Extension.OMLProcessor.model$GenericRecordDescriptionRecordList"], function (exports, OutSystems, Extension_OMLProcessorModel) {
var OS = OutSystems.Internal;
var DeploymentZoneRec = (function (_super) {
__extends(DeploymentZoneRec, _super);
function DeploymentZoneRec(defaults) {
_super.apply(this, arguments);
}
DeploymentZoneRec.attributesToDeclare = function () {
return [
this.attr("Key", "keyAttr", "Key", true, false, OS.Types.Text, function () {
return "";
}, true), 
this.attr("Name", "nameAttr", "Name", true, false, OS.Types.Text, function () {
return "";
}, true), 
this.attr("Description", "descriptionAttr", "Description", true, false, OS.Types.Text, function () {
return "";
}, true), 
this.attr("Address", "addressAttr", "Address", true, false, OS.Types.Text, function () {
return "";
}, true), 
this.attr("EnableHttps", "enableHttpsAttr", "EnableHttps", false, false, OS.Types.Boolean, function () {
return false;
}, true), 
this.attr("DeploymentTechKey", "deploymentTechKeyAttr", "DeploymentTechKey", true, false, OS.Types.Text, function () {
return "";
}, true), 
this.attr("IsDefault", "isDefaultAttr", "IsDefault", true, false, OS.Types.Boolean, function () {
return false;
}, true), 
this.attr("AmazonECSConfig", "amazonECSConfigAttr", "AmazonECSConfig", true, false, OS.Types.Record, function () {
return OS.DataTypes.ImmutableBase.getData(new Extension_OMLProcessorModel.DeploymentZone_AmazonECSRecord());
}, true, Extension_OMLProcessorModel.DeploymentZone_AmazonECSRecord), 
this.attr("AzureContainerServiceConfig", "azureContainerServiceConfigAttr", "AzureContainerServiceConfig", true, false, OS.Types.Record, function () {
return OS.DataTypes.ImmutableBase.getData(new Extension_OMLProcessorModel.DeploymentZone_AzureContainerServiceRecord());
}, true, Extension_OMLProcessorModel.DeploymentZone_AzureContainerServiceRecord), 
this.attr("DockerConfig", "dockerConfigAttr", "DockerConfig", true, false, OS.Types.Record, function () {
return OS.DataTypes.ImmutableBase.getData(new Extension_OMLProcessorModel.DeploymentZone_DockerRecord());
}, true, Extension_OMLProcessorModel.DeploymentZone_DockerRecord), 
this.attr("PivotalConfig", "pivotalConfigAttr", "PivotalConfig", true, false, OS.Types.Record, function () {
return OS.DataTypes.ImmutableBase.getData(new Extension_OMLProcessorModel.DeploymentZone_PivotalRecord());
}, true, Extension_OMLProcessorModel.DeploymentZone_PivotalRecord), 
this.attr("VMConfig", "vMConfigAttr", "VMConfig", true, false, OS.Types.Record, function () {
return OS.DataTypes.ImmutableBase.getData(new Extension_OMLProcessorModel.DeploymentZone_VMRecord());
}, true, Extension_OMLProcessorModel.DeploymentZone_VMRecord), 
this.attr("EspaceKeys", "espaceKeysAttr", "EspaceKeys", true, false, OS.Types.RecordList, function () {
return OS.DataTypes.ImmutableBase.getData(new Extension_OMLProcessorModel.GenericRecordDescriptionRecordList());
}, true, Extension_OMLProcessorModel.GenericRecordDescriptionRecordList)
].concat(_super.attributesToDeclare.call(this));
};
DeploymentZoneRec.init();
return DeploymentZoneRec;
})(OS.DataTypes.GenericRecord);
Extension_OMLProcessorModel.DeploymentZoneRec = DeploymentZoneRec;

});
define("Extension.OMLProcessor.model$QueueStatusRec", ["exports", "OutSystems/ClientRuntime/Main", "Extension.OMLProcessor.model"], function (exports, OutSystems, Extension_OMLProcessorModel) {
var OS = OutSystems.Internal;
var QueueStatusRec = (function (_super) {
__extends(QueueStatusRec, _super);
function QueueStatusRec(defaults) {
_super.apply(this, arguments);
}
QueueStatusRec.attributesToDeclare = function () {
return [
this.attr("QueueName", "queueNameAttr", "QueueName", false, false, OS.Types.Text, function () {
return "";
}, true), 
this.attr("QueueSize", "queueSizeAttr", "QueueSize", false, false, OS.Types.Integer, function () {
return 0;
}, true)
].concat(_super.attributesToDeclare.call(this));
};
QueueStatusRec.init();
return QueueStatusRec;
})(OS.DataTypes.GenericRecord);
Extension_OMLProcessorModel.QueueStatusRec = QueueStatusRec;

});
define("Extension.OMLProcessor.model$QueueStatusRecord", ["exports", "OutSystems/ClientRuntime/Main", "Extension.OMLProcessor.model", "Extension.OMLProcessor.model$QueueStatusRec"], function (exports, OutSystems, Extension_OMLProcessorModel) {
var OS = OutSystems.Internal;
var QueueStatusRecord = (function (_super) {
__extends(QueueStatusRecord, _super);
function QueueStatusRecord(defaults) {
_super.apply(this, arguments);
}
QueueStatusRecord.attributesToDeclare = function () {
return [
this.attr("QueueStatus", "queueStatusAttr", "QueueStatus", false, false, OS.Types.Record, function () {
return OS.DataTypes.ImmutableBase.getData(new Extension_OMLProcessorModel.QueueStatusRec());
}, true, Extension_OMLProcessorModel.QueueStatusRec)
].concat(_super.attributesToDeclare.call(this));
};
QueueStatusRecord.fromStructure = function (str) {
return new QueueStatusRecord(new QueueStatusRecord.RecordClass({
queueStatusAttr: OS.DataTypes.ImmutableBase.getData(str)
}));
};
QueueStatusRecord._isAnonymousRecord = true;
QueueStatusRecord.UniqueId = "95bb869c-875e-46f1-e7a5-83b985f67b5b";
QueueStatusRecord.init();
return QueueStatusRecord;
})(OS.DataTypes.GenericRecord);
Extension_OMLProcessorModel.QueueStatusRecord = QueueStatusRecord;

});
define("Extension.OMLProcessor.model$QueueStatusRecordList", ["exports", "OutSystems/ClientRuntime/Main", "Extension.OMLProcessor.model", "Extension.OMLProcessor.model$QueueStatusRecord"], function (exports, OutSystems, Extension_OMLProcessorModel) {
var OS = OutSystems.Internal;
var QueueStatusRecordList = (function (_super) {
__extends(QueueStatusRecordList, _super);
function QueueStatusRecordList(defaults) {
_super.apply(this, arguments);
}
QueueStatusRecordList.itemType = Extension_OMLProcessorModel.QueueStatusRecord;
return QueueStatusRecordList;
})(OS.DataTypes.GenericRecordList);
Extension_OMLProcessorModel.QueueStatusRecordList = QueueStatusRecordList;

});
define("Extension.OMLProcessor.model$ModuleDefinitionRecordList", ["exports", "OutSystems/ClientRuntime/Main", "Extension.OMLProcessor.model", "Extension.OMLProcessor.model$ModuleDefinitionRecord"], function (exports, OutSystems, Extension_OMLProcessorModel) {
var OS = OutSystems.Internal;
var ModuleDefinitionRecordList = (function (_super) {
__extends(ModuleDefinitionRecordList, _super);
function ModuleDefinitionRecordList(defaults) {
_super.apply(this, arguments);
}
ModuleDefinitionRecordList.itemType = Extension_OMLProcessorModel.ModuleDefinitionRecord;
return ModuleDefinitionRecordList;
})(OS.DataTypes.GenericRecordList);
Extension_OMLProcessorModel.ModuleDefinitionRecordList = ModuleDefinitionRecordList;

});
define("Extension.OMLProcessor.model$ProcessConfigRec", ["exports", "OutSystems/ClientRuntime/Main", "Extension.OMLProcessor.model"], function (exports, OutSystems, Extension_OMLProcessorModel) {
var OS = OutSystems.Internal;
var ProcessConfigRec = (function (_super) {
__extends(ProcessConfigRec, _super);
function ProcessConfigRec(defaults) {
_super.apply(this, arguments);
}
ProcessConfigRec.attributesToDeclare = function () {
return [
this.attr("ProcessName", "processNameAttr", "ProcessName", true, false, OS.Types.Text, function () {
return "";
}, true)
].concat(_super.attributesToDeclare.call(this));
};
ProcessConfigRec.fromStructure = function (str) {
return new ProcessConfigRec(new ProcessConfigRec.RecordClass({
processNameAttr: OS.DataTypes.ImmutableBase.getData(str)
}));
};
ProcessConfigRec.init();
return ProcessConfigRec;
})(OS.DataTypes.GenericRecord);
Extension_OMLProcessorModel.ProcessConfigRec = ProcessConfigRec;

});
define("Extension.OMLProcessor.model$ProcessConfigRecord", ["exports", "OutSystems/ClientRuntime/Main", "Extension.OMLProcessor.model", "Extension.OMLProcessor.model$ProcessConfigRec"], function (exports, OutSystems, Extension_OMLProcessorModel) {
var OS = OutSystems.Internal;
var ProcessConfigRecord = (function (_super) {
__extends(ProcessConfigRecord, _super);
function ProcessConfigRecord(defaults) {
_super.apply(this, arguments);
}
ProcessConfigRecord.attributesToDeclare = function () {
return [
this.attr("ProcessConfig", "processConfigAttr", "ProcessConfig", false, false, OS.Types.Record, function () {
return OS.DataTypes.ImmutableBase.getData(new Extension_OMLProcessorModel.ProcessConfigRec());
}, true, Extension_OMLProcessorModel.ProcessConfigRec)
].concat(_super.attributesToDeclare.call(this));
};
ProcessConfigRecord.fromStructure = function (str) {
return new ProcessConfigRecord(new ProcessConfigRecord.RecordClass({
processConfigAttr: OS.DataTypes.ImmutableBase.getData(str)
}));
};
ProcessConfigRecord._isAnonymousRecord = true;
ProcessConfigRecord.UniqueId = "8ab635ce-0629-cd5c-c32d-1df8b0eefa44";
ProcessConfigRecord.init();
return ProcessConfigRecord;
})(OS.DataTypes.GenericRecord);
Extension_OMLProcessorModel.ProcessConfigRecord = ProcessConfigRecord;

});
define("Extension.OMLProcessor.model$AbstractFieldRec", ["exports", "OutSystems/ClientRuntime/Main", "Extension.OMLProcessor.model"], function (exports, OutSystems, Extension_OMLProcessorModel) {
var OS = OutSystems.Internal;
var AbstractFieldRec = (function (_super) {
__extends(AbstractFieldRec, _super);
function AbstractFieldRec(defaults) {
_super.apply(this, arguments);
}
AbstractFieldRec.attributesToDeclare = function () {
return [
this.attr("Text", "textAttr", "Text", true, false, OS.Types.Text, function () {
return "";
}, true)
].concat(_super.attributesToDeclare.call(this));
};
AbstractFieldRec.fromStructure = function (str) {
return new AbstractFieldRec(new AbstractFieldRec.RecordClass({
textAttr: OS.DataTypes.ImmutableBase.getData(str)
}));
};
AbstractFieldRec.init();
return AbstractFieldRec;
})(OS.DataTypes.GenericRecord);
Extension_OMLProcessorModel.AbstractFieldRec = AbstractFieldRec;

});
define("Extension.OMLProcessor.model$AbstractFieldRecord", ["exports", "OutSystems/ClientRuntime/Main", "Extension.OMLProcessor.model", "Extension.OMLProcessor.model$AbstractFieldRec"], function (exports, OutSystems, Extension_OMLProcessorModel) {
var OS = OutSystems.Internal;
var AbstractFieldRecord = (function (_super) {
__extends(AbstractFieldRecord, _super);
function AbstractFieldRecord(defaults) {
_super.apply(this, arguments);
}
AbstractFieldRecord.attributesToDeclare = function () {
return [
this.attr("AbstractField", "abstractFieldAttr", "AbstractField", false, false, OS.Types.Record, function () {
return OS.DataTypes.ImmutableBase.getData(new Extension_OMLProcessorModel.AbstractFieldRec());
}, true, Extension_OMLProcessorModel.AbstractFieldRec)
].concat(_super.attributesToDeclare.call(this));
};
AbstractFieldRecord.fromStructure = function (str) {
return new AbstractFieldRecord(new AbstractFieldRecord.RecordClass({
abstractFieldAttr: OS.DataTypes.ImmutableBase.getData(str)
}));
};
AbstractFieldRecord._isAnonymousRecord = true;
AbstractFieldRecord.UniqueId = "fa4bd72d-79e6-ed1c-558c-67b823ae9dc0";
AbstractFieldRecord.init();
return AbstractFieldRecord;
})(OS.DataTypes.GenericRecord);
Extension_OMLProcessorModel.AbstractFieldRecord = AbstractFieldRecord;

});
define("Extension.OMLProcessor.model$AbstractFieldRecordList", ["exports", "OutSystems/ClientRuntime/Main", "Extension.OMLProcessor.model", "Extension.OMLProcessor.model$AbstractFieldRecord"], function (exports, OutSystems, Extension_OMLProcessorModel) {
var OS = OutSystems.Internal;
var AbstractFieldRecordList = (function (_super) {
__extends(AbstractFieldRecordList, _super);
function AbstractFieldRecordList(defaults) {
_super.apply(this, arguments);
}
AbstractFieldRecordList.itemType = Extension_OMLProcessorModel.AbstractFieldRecord;
return AbstractFieldRecordList;
})(OS.DataTypes.GenericRecordList);
Extension_OMLProcessorModel.AbstractFieldRecordList = AbstractFieldRecordList;

});
define("Extension.OMLProcessor.model$AbstractRecordRec", ["exports", "OutSystems/ClientRuntime/Main", "Extension.OMLProcessor.model", "Extension.OMLProcessor.model$AbstractFieldRecordList"], function (exports, OutSystems, Extension_OMLProcessorModel) {
var OS = OutSystems.Internal;
var AbstractRecordRec = (function (_super) {
__extends(AbstractRecordRec, _super);
function AbstractRecordRec(defaults) {
_super.apply(this, arguments);
}
AbstractRecordRec.attributesToDeclare = function () {
return [
this.attr("Fields", "fieldsAttr", "Fields", true, false, OS.Types.RecordList, function () {
return OS.DataTypes.ImmutableBase.getData(new Extension_OMLProcessorModel.AbstractFieldRecordList());
}, true, Extension_OMLProcessorModel.AbstractFieldRecordList), 
this.attr("Tag", "tagAttr", "Tag", true, false, OS.Types.Text, function () {
return "";
}, true)
].concat(_super.attributesToDeclare.call(this));
};
AbstractRecordRec.init();
return AbstractRecordRec;
})(OS.DataTypes.GenericRecord);
Extension_OMLProcessorModel.AbstractRecordRec = AbstractRecordRec;

});
define("Extension.OMLProcessor.model$ReferenceRec", ["exports", "OutSystems/ClientRuntime/Main", "Extension.OMLProcessor.model"], function (exports, OutSystems, Extension_OMLProcessorModel) {
var OS = OutSystems.Internal;
var ReferenceRec = (function (_super) {
__extends(ReferenceRec, _super);
function ReferenceRec(defaults) {
_super.apply(this, arguments);
}
ReferenceRec.attributesToDeclare = function () {
return [
this.attr("Name", "nameAttr", "Name", true, false, OS.Types.Text, function () {
return "";
}, true), 
this.attr("Key", "keyAttr", "Key", true, false, OS.Types.Text, function () {
return "";
}, true), 
this.attr("Kind", "kindAttr", "Kind", true, false, OS.Types.Text, function () {
return "";
}, true), 
this.attr("VersionId", "versionIdAttr", "VersionId", true, false, OS.Types.Integer, function () {
return 0;
}, true), 
this.attr("ComponentId", "componentIdAttr", "ComponentId", true, false, OS.Types.Integer, function () {
return 0;
}, true)
].concat(_super.attributesToDeclare.call(this));
};
ReferenceRec.init();
return ReferenceRec;
})(OS.DataTypes.GenericRecord);
Extension_OMLProcessorModel.ReferenceRec = ReferenceRec;

});
define("Extension.OMLProcessor.model$ReferenceRecord", ["exports", "OutSystems/ClientRuntime/Main", "Extension.OMLProcessor.model", "Extension.OMLProcessor.model$ReferenceRec"], function (exports, OutSystems, Extension_OMLProcessorModel) {
var OS = OutSystems.Internal;
var ReferenceRecord = (function (_super) {
__extends(ReferenceRecord, _super);
function ReferenceRecord(defaults) {
_super.apply(this, arguments);
}
ReferenceRecord.attributesToDeclare = function () {
return [
this.attr("Reference", "referenceAttr", "Reference", false, false, OS.Types.Record, function () {
return OS.DataTypes.ImmutableBase.getData(new Extension_OMLProcessorModel.ReferenceRec());
}, true, Extension_OMLProcessorModel.ReferenceRec)
].concat(_super.attributesToDeclare.call(this));
};
ReferenceRecord.fromStructure = function (str) {
return new ReferenceRecord(new ReferenceRecord.RecordClass({
referenceAttr: OS.DataTypes.ImmutableBase.getData(str)
}));
};
ReferenceRecord._isAnonymousRecord = true;
ReferenceRecord.UniqueId = "8ce90a09-ef40-6f56-10aa-6111c026fc21";
ReferenceRecord.init();
return ReferenceRecord;
})(OS.DataTypes.GenericRecord);
Extension_OMLProcessorModel.ReferenceRecord = ReferenceRecord;

});
define("Extension.OMLProcessor.model$AbstractRecordRecord", ["exports", "OutSystems/ClientRuntime/Main", "Extension.OMLProcessor.model", "Extension.OMLProcessor.model$AbstractRecordRec"], function (exports, OutSystems, Extension_OMLProcessorModel) {
var OS = OutSystems.Internal;
var AbstractRecordRecord = (function (_super) {
__extends(AbstractRecordRecord, _super);
function AbstractRecordRecord(defaults) {
_super.apply(this, arguments);
}
AbstractRecordRecord.attributesToDeclare = function () {
return [
this.attr("AbstractRecord", "abstractRecordAttr", "AbstractRecord", false, false, OS.Types.Record, function () {
return OS.DataTypes.ImmutableBase.getData(new Extension_OMLProcessorModel.AbstractRecordRec());
}, true, Extension_OMLProcessorModel.AbstractRecordRec)
].concat(_super.attributesToDeclare.call(this));
};
AbstractRecordRecord.fromStructure = function (str) {
return new AbstractRecordRecord(new AbstractRecordRecord.RecordClass({
abstractRecordAttr: OS.DataTypes.ImmutableBase.getData(str)
}));
};
AbstractRecordRecord._isAnonymousRecord = true;
AbstractRecordRecord.UniqueId = "910dc1eb-f4ae-53c5-5ed5-227e10d5339d";
AbstractRecordRecord.init();
return AbstractRecordRecord;
})(OS.DataTypes.GenericRecord);
Extension_OMLProcessorModel.AbstractRecordRecord = AbstractRecordRecord;

});
define("Extension.OMLProcessor.model$ActivationLicenseInformationRecordList", ["exports", "OutSystems/ClientRuntime/Main", "Extension.OMLProcessor.model", "Extension.OMLProcessor.model$ActivationLicenseInformationRecord"], function (exports, OutSystems, Extension_OMLProcessorModel) {
var OS = OutSystems.Internal;
var ActivationLicenseInformationRecordList = (function (_super) {
__extends(ActivationLicenseInformationRecordList, _super);
function ActivationLicenseInformationRecordList(defaults) {
_super.apply(this, arguments);
}
ActivationLicenseInformationRecordList.itemType = Extension_OMLProcessorModel.ActivationLicenseInformationRecord;
return ActivationLicenseInformationRecordList;
})(OS.DataTypes.GenericRecordList);
Extension_OMLProcessorModel.ActivationLicenseInformationRecordList = ActivationLicenseInformationRecordList;

});
define("Extension.OMLProcessor.model$DecimalRecordList", ["exports", "OutSystems/ClientRuntime/Main", "Extension.OMLProcessor.model", "Extension.OMLProcessor.model$DecimalRecord"], function (exports, OutSystems, Extension_OMLProcessorModel) {
var OS = OutSystems.Internal;
var DecimalRecordList = (function (_super) {
__extends(DecimalRecordList, _super);
function DecimalRecordList(defaults) {
_super.apply(this, arguments);
}
DecimalRecordList.itemType = Extension_OMLProcessorModel.DecimalRecord;
return DecimalRecordList;
})(OS.DataTypes.GenericRecordList);
Extension_OMLProcessorModel.DecimalRecordList = DecimalRecordList;

});
define("Extension.OMLProcessor.model$VersionComparisonRecordList", ["exports", "OutSystems/ClientRuntime/Main", "Extension.OMLProcessor.model", "Extension.OMLProcessor.model$VersionComparisonRecord"], function (exports, OutSystems, Extension_OMLProcessorModel) {
var OS = OutSystems.Internal;
var VersionComparisonRecordList = (function (_super) {
__extends(VersionComparisonRecordList, _super);
function VersionComparisonRecordList(defaults) {
_super.apply(this, arguments);
}
VersionComparisonRecordList.itemType = Extension_OMLProcessorModel.VersionComparisonRecord;
return VersionComparisonRecordList;
})(OS.DataTypes.GenericRecordList);
Extension_OMLProcessorModel.VersionComparisonRecordList = VersionComparisonRecordList;

});
define("Extension.OMLProcessor.model$DateTimeRecordList", ["exports", "OutSystems/ClientRuntime/Main", "Extension.OMLProcessor.model", "Extension.OMLProcessor.model$DateTimeRecord"], function (exports, OutSystems, Extension_OMLProcessorModel) {
var OS = OutSystems.Internal;
var DateTimeRecordList = (function (_super) {
__extends(DateTimeRecordList, _super);
function DateTimeRecordList(defaults) {
_super.apply(this, arguments);
}
DateTimeRecordList.itemType = Extension_OMLProcessorModel.DateTimeRecord;
return DateTimeRecordList;
})(OS.DataTypes.GenericRecordList);
Extension_OMLProcessorModel.DateTimeRecordList = DateTimeRecordList;

});
define("Extension.OMLProcessor.model$DeploymentZoneRecord", ["exports", "OutSystems/ClientRuntime/Main", "Extension.OMLProcessor.model", "Extension.OMLProcessor.model$DeploymentZoneRec"], function (exports, OutSystems, Extension_OMLProcessorModel) {
var OS = OutSystems.Internal;
var DeploymentZoneRecord = (function (_super) {
__extends(DeploymentZoneRecord, _super);
function DeploymentZoneRecord(defaults) {
_super.apply(this, arguments);
}
DeploymentZoneRecord.attributesToDeclare = function () {
return [
this.attr("DeploymentZone", "deploymentZoneAttr", "DeploymentZone", false, false, OS.Types.Record, function () {
return OS.DataTypes.ImmutableBase.getData(new Extension_OMLProcessorModel.DeploymentZoneRec());
}, true, Extension_OMLProcessorModel.DeploymentZoneRec)
].concat(_super.attributesToDeclare.call(this));
};
DeploymentZoneRecord.fromStructure = function (str) {
return new DeploymentZoneRecord(new DeploymentZoneRecord.RecordClass({
deploymentZoneAttr: OS.DataTypes.ImmutableBase.getData(str)
}));
};
DeploymentZoneRecord._isAnonymousRecord = true;
DeploymentZoneRecord.UniqueId = "a489420a-a0ce-3ed5-f7cd-4fdd82a88f40";
DeploymentZoneRecord.init();
return DeploymentZoneRecord;
})(OS.DataTypes.GenericRecord);
Extension_OMLProcessorModel.DeploymentZoneRecord = DeploymentZoneRecord;

});
define("Extension.OMLProcessor.model$AttributeDefinitionRecordList", ["exports", "OutSystems/ClientRuntime/Main", "Extension.OMLProcessor.model", "Extension.OMLProcessor.model$AttributeDefinitionRecord"], function (exports, OutSystems, Extension_OMLProcessorModel) {
var OS = OutSystems.Internal;
var AttributeDefinitionRecordList = (function (_super) {
__extends(AttributeDefinitionRecordList, _super);
function AttributeDefinitionRecordList(defaults) {
_super.apply(this, arguments);
}
AttributeDefinitionRecordList.itemType = Extension_OMLProcessorModel.AttributeDefinitionRecord;
return AttributeDefinitionRecordList;
})(OS.DataTypes.GenericRecordList);
Extension_OMLProcessorModel.AttributeDefinitionRecordList = AttributeDefinitionRecordList;

});
define("Extension.OMLProcessor.model$DeploymentTechnologyRec", ["exports", "OutSystems/ClientRuntime/Main", "Extension.OMLProcessor.model", "Extension.OMLProcessor.model$DeploymentZone_AmazonECSRecord", "Extension.OMLProcessor.model$DeploymentZone_AzureContainerServiceRecord", "Extension.OMLProcessor.model$DeploymentZone_DockerRecord", "Extension.OMLProcessor.model$DeploymentZone_PivotalRecord", "Extension.OMLProcessor.model$DeploymentZone_VMRecord"], function (exports, OutSystems, Extension_OMLProcessorModel) {
var OS = OutSystems.Internal;
var DeploymentTechnologyRec = (function (_super) {
__extends(DeploymentTechnologyRec, _super);
function DeploymentTechnologyRec(defaults) {
_super.apply(this, arguments);
}
DeploymentTechnologyRec.attributesToDeclare = function () {
return [
this.attr("Key", "keyAttr", "Key", true, false, OS.Types.Text, function () {
return "";
}, true), 
this.attr("Name", "nameAttr", "Name", true, false, OS.Types.Text, function () {
return "";
}, true), 
this.attr("Logo", "logoAttr", "Logo", true, false, OS.Types.BinaryData, function () {
return OS.DataTypes.BinaryData.defaultValue;
}, true), 
this.attr("AmazonECSConfig_Default", "amazonECSConfig_DefaultAttr", "AmazonECSConfig_Default", true, false, OS.Types.Record, function () {
return OS.DataTypes.ImmutableBase.getData(new Extension_OMLProcessorModel.DeploymentZone_AmazonECSRecord());
}, true, Extension_OMLProcessorModel.DeploymentZone_AmazonECSRecord), 
this.attr("AzureContainerServiceConfig_Default", "azureContainerServiceConfig_DefaultAttr", "AzureContainerServiceConfig_Default", true, false, OS.Types.Record, function () {
return OS.DataTypes.ImmutableBase.getData(new Extension_OMLProcessorModel.DeploymentZone_AzureContainerServiceRecord());
}, true, Extension_OMLProcessorModel.DeploymentZone_AzureContainerServiceRecord), 
this.attr("DockerConfig_Default", "dockerConfig_DefaultAttr", "DockerConfig_Default", true, false, OS.Types.Record, function () {
return OS.DataTypes.ImmutableBase.getData(new Extension_OMLProcessorModel.DeploymentZone_DockerRecord());
}, true, Extension_OMLProcessorModel.DeploymentZone_DockerRecord), 
this.attr("PivotalConfig_Default", "pivotalConfig_DefaultAttr", "PivotalConfig_Default", true, false, OS.Types.Record, function () {
return OS.DataTypes.ImmutableBase.getData(new Extension_OMLProcessorModel.DeploymentZone_PivotalRecord());
}, true, Extension_OMLProcessorModel.DeploymentZone_PivotalRecord), 
this.attr("VMConfig_Default", "vMConfig_DefaultAttr", "VMConfig_Default", true, false, OS.Types.Record, function () {
return OS.DataTypes.ImmutableBase.getData(new Extension_OMLProcessorModel.DeploymentZone_VMRecord());
}, true, Extension_OMLProcessorModel.DeploymentZone_VMRecord), 
this.attr("IsLicensed", "isLicensedAttr", "IsLicensed", true, false, OS.Types.Boolean, function () {
return false;
}, true)
].concat(_super.attributesToDeclare.call(this));
};
DeploymentTechnologyRec.init();
return DeploymentTechnologyRec;
})(OS.DataTypes.GenericRecord);
Extension_OMLProcessorModel.DeploymentTechnologyRec = DeploymentTechnologyRec;

});
define("Extension.OMLProcessor.model$DeploymentTechnologyRecord", ["exports", "OutSystems/ClientRuntime/Main", "Extension.OMLProcessor.model", "Extension.OMLProcessor.model$DeploymentTechnologyRec"], function (exports, OutSystems, Extension_OMLProcessorModel) {
var OS = OutSystems.Internal;
var DeploymentTechnologyRecord = (function (_super) {
__extends(DeploymentTechnologyRecord, _super);
function DeploymentTechnologyRecord(defaults) {
_super.apply(this, arguments);
}
DeploymentTechnologyRecord.attributesToDeclare = function () {
return [
this.attr("DeploymentTechnology", "deploymentTechnologyAttr", "DeploymentTechnology", false, false, OS.Types.Record, function () {
return OS.DataTypes.ImmutableBase.getData(new Extension_OMLProcessorModel.DeploymentTechnologyRec());
}, true, Extension_OMLProcessorModel.DeploymentTechnologyRec)
].concat(_super.attributesToDeclare.call(this));
};
DeploymentTechnologyRecord.fromStructure = function (str) {
return new DeploymentTechnologyRecord(new DeploymentTechnologyRecord.RecordClass({
deploymentTechnologyAttr: OS.DataTypes.ImmutableBase.getData(str)
}));
};
DeploymentTechnologyRecord._isAnonymousRecord = true;
DeploymentTechnologyRecord.UniqueId = "b14cadf1-8f1b-9fdd-8a5f-e8e021d7a231";
DeploymentTechnologyRecord.init();
return DeploymentTechnologyRecord;
})(OS.DataTypes.GenericRecord);
Extension_OMLProcessorModel.DeploymentTechnologyRecord = DeploymentTechnologyRecord;

});
define("Extension.OMLProcessor.model$DeploymentZoneRecordList", ["exports", "OutSystems/ClientRuntime/Main", "Extension.OMLProcessor.model", "Extension.OMLProcessor.model$DeploymentZoneRecord"], function (exports, OutSystems, Extension_OMLProcessorModel) {
var OS = OutSystems.Internal;
var DeploymentZoneRecordList = (function (_super) {
__extends(DeploymentZoneRecordList, _super);
function DeploymentZoneRecordList(defaults) {
_super.apply(this, arguments);
}
DeploymentZoneRecordList.itemType = Extension_OMLProcessorModel.DeploymentZoneRecord;
return DeploymentZoneRecordList;
})(OS.DataTypes.GenericRecordList);
Extension_OMLProcessorModel.DeploymentZoneRecordList = DeploymentZoneRecordList;

});
define("Extension.OMLProcessor.model$EntityRecordRecord", ["exports", "OutSystems/ClientRuntime/Main", "Extension.OMLProcessor.model", "Extension.OMLProcessor.model$EntityRecordRec"], function (exports, OutSystems, Extension_OMLProcessorModel) {
var OS = OutSystems.Internal;
var EntityRecordRecord = (function (_super) {
__extends(EntityRecordRecord, _super);
function EntityRecordRecord(defaults) {
_super.apply(this, arguments);
}
EntityRecordRecord.attributesToDeclare = function () {
return [
this.attr("EntityRecord", "entityRecordAttr", "EntityRecord", false, false, OS.Types.Record, function () {
return OS.DataTypes.ImmutableBase.getData(new Extension_OMLProcessorModel.EntityRecordRec());
}, true, Extension_OMLProcessorModel.EntityRecordRec)
].concat(_super.attributesToDeclare.call(this));
};
EntityRecordRecord.fromStructure = function (str) {
return new EntityRecordRecord(new EntityRecordRecord.RecordClass({
entityRecordAttr: OS.DataTypes.ImmutableBase.getData(str)
}));
};
EntityRecordRecord._isAnonymousRecord = true;
EntityRecordRecord.UniqueId = "b6edb46c-579b-18f9-5f69-237cb0c5779b";
EntityRecordRecord.init();
return EntityRecordRecord;
})(OS.DataTypes.GenericRecord);
Extension_OMLProcessorModel.EntityRecordRecord = EntityRecordRecord;

});
define("Extension.OMLProcessor.model$ESpaceDetailedInfoRecordList", ["exports", "OutSystems/ClientRuntime/Main", "Extension.OMLProcessor.model", "Extension.OMLProcessor.model$ESpaceDetailedInfoRecord"], function (exports, OutSystems, Extension_OMLProcessorModel) {
var OS = OutSystems.Internal;
var ESpaceDetailedInfoRecordList = (function (_super) {
__extends(ESpaceDetailedInfoRecordList, _super);
function ESpaceDetailedInfoRecordList(defaults) {
_super.apply(this, arguments);
}
ESpaceDetailedInfoRecordList.itemType = Extension_OMLProcessorModel.ESpaceDetailedInfoRecord;
return ESpaceDetailedInfoRecordList;
})(OS.DataTypes.GenericRecordList);
Extension_OMLProcessorModel.ESpaceDetailedInfoRecordList = ESpaceDetailedInfoRecordList;

});
define("Extension.OMLProcessor.model$EspacePublishAsyncResultRecord", ["exports", "OutSystems/ClientRuntime/Main", "Extension.OMLProcessor.model", "Extension.OMLProcessor.model$EspacePublishAsyncResultRec"], function (exports, OutSystems, Extension_OMLProcessorModel) {
var OS = OutSystems.Internal;
var EspacePublishAsyncResultRecord = (function (_super) {
__extends(EspacePublishAsyncResultRecord, _super);
function EspacePublishAsyncResultRecord(defaults) {
_super.apply(this, arguments);
}
EspacePublishAsyncResultRecord.attributesToDeclare = function () {
return [
this.attr("EspacePublishAsyncResult", "espacePublishAsyncResultAttr", "EspacePublishAsyncResult", false, false, OS.Types.Record, function () {
return OS.DataTypes.ImmutableBase.getData(new Extension_OMLProcessorModel.EspacePublishAsyncResultRec());
}, true, Extension_OMLProcessorModel.EspacePublishAsyncResultRec)
].concat(_super.attributesToDeclare.call(this));
};
EspacePublishAsyncResultRecord.fromStructure = function (str) {
return new EspacePublishAsyncResultRecord(new EspacePublishAsyncResultRecord.RecordClass({
espacePublishAsyncResultAttr: OS.DataTypes.ImmutableBase.getData(str)
}));
};
EspacePublishAsyncResultRecord._isAnonymousRecord = true;
EspacePublishAsyncResultRecord.UniqueId = "bd0f5fa3-986f-36d2-6f64-c96b0f81c167";
EspacePublishAsyncResultRecord.init();
return EspacePublishAsyncResultRecord;
})(OS.DataTypes.GenericRecord);
Extension_OMLProcessorModel.EspacePublishAsyncResultRecord = EspacePublishAsyncResultRecord;

});
define("Extension.OMLProcessor.model$UserWithUserPoolRecordList", ["exports", "OutSystems/ClientRuntime/Main", "Extension.OMLProcessor.model", "Extension.OMLProcessor.model$UserWithUserPoolRecord"], function (exports, OutSystems, Extension_OMLProcessorModel) {
var OS = OutSystems.Internal;
var UserWithUserPoolRecordList = (function (_super) {
__extends(UserWithUserPoolRecordList, _super);
function UserWithUserPoolRecordList(defaults) {
_super.apply(this, arguments);
}
UserWithUserPoolRecordList.itemType = Extension_OMLProcessorModel.UserWithUserPoolRecord;
return UserWithUserPoolRecordList;
})(OS.DataTypes.GenericRecordList);
Extension_OMLProcessorModel.UserWithUserPoolRecordList = UserWithUserPoolRecordList;

});
define("Extension.OMLProcessor.model$ReferenceableModuleRec", ["exports", "OutSystems/ClientRuntime/Main", "Extension.OMLProcessor.model"], function (exports, OutSystems, Extension_OMLProcessorModel) {
var OS = OutSystems.Internal;
var ReferenceableModuleRec = (function (_super) {
__extends(ReferenceableModuleRec, _super);
function ReferenceableModuleRec(defaults) {
_super.apply(this, arguments);
}
ReferenceableModuleRec.attributesToDeclare = function () {
return [
this.attr("Key", "keyAttr", "Key", true, false, OS.Types.Text, function () {
return "";
}, true), 
this.attr("Name", "nameAttr", "Name", true, false, OS.Types.Text, function () {
return "";
}, true), 
this.attr("Kind", "kindAttr", "Kind", true, false, OS.Types.Text, function () {
return "";
}, true), 
this.attr("Description", "descriptionAttr", "Description", true, false, OS.Types.Text, function () {
return "";
}, true), 
this.attr("SignatureHash", "signatureHashAttr", "SignatureHash", true, false, OS.Types.Text, function () {
return "";
}, true), 
this.attr("JQueryVersion", "jQueryVersionAttr", "JQueryVersion", true, false, OS.Types.Text, function () {
return "";
}, true), 
this.attr("ApplicationKey", "applicationKeyAttr", "ApplicationKey", true, false, OS.Types.Text, function () {
return "";
}, true), 
this.attr("RuntimeKind", "runtimeKindAttr", "RuntimeKind", true, false, OS.Types.Text, function () {
return "";
}, true), 
this.attr("ExtensibilityConfiguration", "extensibilityConfigurationAttr", "ExtensibilityConfiguration", true, false, OS.Types.Text, function () {
return "";
}, true), 
this.attr("Icon", "iconAttr", "Icon", true, false, OS.Types.BinaryData, function () {
return OS.DataTypes.BinaryData.defaultValue;
}, true)
].concat(_super.attributesToDeclare.call(this));
};
ReferenceableModuleRec.init();
return ReferenceableModuleRec;
})(OS.DataTypes.GenericRecord);
Extension_OMLProcessorModel.ReferenceableModuleRec = ReferenceableModuleRec;

});
define("Extension.OMLProcessor.model$ReferenceableModuleRecord", ["exports", "OutSystems/ClientRuntime/Main", "Extension.OMLProcessor.model", "Extension.OMLProcessor.model$ReferenceableModuleRec"], function (exports, OutSystems, Extension_OMLProcessorModel) {
var OS = OutSystems.Internal;
var ReferenceableModuleRecord = (function (_super) {
__extends(ReferenceableModuleRecord, _super);
function ReferenceableModuleRecord(defaults) {
_super.apply(this, arguments);
}
ReferenceableModuleRecord.attributesToDeclare = function () {
return [
this.attr("ReferenceableModule", "referenceableModuleAttr", "ReferenceableModule", false, false, OS.Types.Record, function () {
return OS.DataTypes.ImmutableBase.getData(new Extension_OMLProcessorModel.ReferenceableModuleRec());
}, true, Extension_OMLProcessorModel.ReferenceableModuleRec)
].concat(_super.attributesToDeclare.call(this));
};
ReferenceableModuleRecord.fromStructure = function (str) {
return new ReferenceableModuleRecord(new ReferenceableModuleRecord.RecordClass({
referenceableModuleAttr: OS.DataTypes.ImmutableBase.getData(str)
}));
};
ReferenceableModuleRecord._isAnonymousRecord = true;
ReferenceableModuleRecord.UniqueId = "c2d7720f-f4d7-4a5a-335e-9e9d8595926a";
ReferenceableModuleRecord.init();
return ReferenceableModuleRecord;
})(OS.DataTypes.GenericRecord);
Extension_OMLProcessorModel.ReferenceableModuleRecord = ReferenceableModuleRecord;

});
define("Extension.OMLProcessor.model$PTA_EspaceInfoRecordList", ["exports", "OutSystems/ClientRuntime/Main", "Extension.OMLProcessor.model", "Extension.OMLProcessor.model$PTA_EspaceInfoRecord"], function (exports, OutSystems, Extension_OMLProcessorModel) {
var OS = OutSystems.Internal;
var PTA_EspaceInfoRecordList = (function (_super) {
__extends(PTA_EspaceInfoRecordList, _super);
function PTA_EspaceInfoRecordList(defaults) {
_super.apply(this, arguments);
}
PTA_EspaceInfoRecordList.itemType = Extension_OMLProcessorModel.PTA_EspaceInfoRecord;
return PTA_EspaceInfoRecordList;
})(OS.DataTypes.GenericRecordList);
Extension_OMLProcessorModel.PTA_EspaceInfoRecordList = PTA_EspaceInfoRecordList;

});
define("Extension.OMLProcessor.model$BooleanRec", ["exports", "OutSystems/ClientRuntime/Main", "Extension.OMLProcessor.model"], function (exports, OutSystems, Extension_OMLProcessorModel) {
var OS = OutSystems.Internal;
var BooleanRec = (function (_super) {
__extends(BooleanRec, _super);
function BooleanRec(defaults) {
_super.apply(this, arguments);
}
BooleanRec.attributesToDeclare = function () {
return [
this.attr("Boolean", "booleanAttr", "Boolean", false, false, OS.Types.Boolean, function () {
return false;
}, true)
].concat(_super.attributesToDeclare.call(this));
};
BooleanRec.fromStructure = function (str) {
return new BooleanRec(new BooleanRec.RecordClass({
booleanAttr: OS.DataTypes.ImmutableBase.getData(str)
}));
};
BooleanRec.init();
return BooleanRec;
})(OS.DataTypes.GenericRecord);
Extension_OMLProcessorModel.BooleanRec = BooleanRec;

});
define("Extension.OMLProcessor.model$BooleanRecord", ["exports", "OutSystems/ClientRuntime/Main", "Extension.OMLProcessor.model", "Extension.OMLProcessor.model$BooleanRec"], function (exports, OutSystems, Extension_OMLProcessorModel) {
var OS = OutSystems.Internal;
var BooleanRecord = (function (_super) {
__extends(BooleanRecord, _super);
function BooleanRecord(defaults) {
_super.apply(this, arguments);
}
BooleanRecord.attributesToDeclare = function () {
return [
this.attr("Boolean", "booleanAttr", "Boolean", false, false, OS.Types.Record, function () {
return OS.DataTypes.ImmutableBase.getData(new Extension_OMLProcessorModel.BooleanRec());
}, true, Extension_OMLProcessorModel.BooleanRec)
].concat(_super.attributesToDeclare.call(this));
};
BooleanRecord.fromStructure = function (str) {
return new BooleanRecord(new BooleanRecord.RecordClass({
booleanAttr: OS.DataTypes.ImmutableBase.getData(str)
}));
};
BooleanRecord._isAnonymousRecord = true;
BooleanRecord.UniqueId = "d9f001ae-3de5-4c9f-9460-7e2bda71c207";
BooleanRecord.init();
return BooleanRecord;
})(OS.DataTypes.GenericRecord);
Extension_OMLProcessorModel.BooleanRecord = BooleanRecord;

});
define("Extension.OMLProcessor.model$BooleanRecordList", ["exports", "OutSystems/ClientRuntime/Main", "Extension.OMLProcessor.model", "Extension.OMLProcessor.model$BooleanRecord"], function (exports, OutSystems, Extension_OMLProcessorModel) {
var OS = OutSystems.Internal;
var BooleanRecordList = (function (_super) {
__extends(BooleanRecordList, _super);
function BooleanRecordList(defaults) {
_super.apply(this, arguments);
}
BooleanRecordList.itemType = Extension_OMLProcessorModel.BooleanRecord;
return BooleanRecordList;
})(OS.DataTypes.GenericRecordList);
Extension_OMLProcessorModel.BooleanRecordList = BooleanRecordList;

});
define("Extension.OMLProcessor.model$ExternalAuthentication_PluginAPIStatusRecord", ["exports", "OutSystems/ClientRuntime/Main", "Extension.OMLProcessor.model", "Extension.OMLProcessor.model$ExternalAuthentication_PluginAPIStatusRec"], function (exports, OutSystems, Extension_OMLProcessorModel) {
var OS = OutSystems.Internal;
var ExternalAuthentication_PluginAPIStatusRecord = (function (_super) {
__extends(ExternalAuthentication_PluginAPIStatusRecord, _super);
function ExternalAuthentication_PluginAPIStatusRecord(defaults) {
_super.apply(this, arguments);
}
ExternalAuthentication_PluginAPIStatusRecord.attributesToDeclare = function () {
return [
this.attr("ExternalAuthentication_PluginAPIStatus", "externalAuthentication_PluginAPIStatusAttr", "ExternalAuthentication_PluginAPIStatus", false, false, OS.Types.Record, function () {
return OS.DataTypes.ImmutableBase.getData(new Extension_OMLProcessorModel.ExternalAuthentication_PluginAPIStatusRec());
}, true, Extension_OMLProcessorModel.ExternalAuthentication_PluginAPIStatusRec)
].concat(_super.attributesToDeclare.call(this));
};
ExternalAuthentication_PluginAPIStatusRecord.fromStructure = function (str) {
return new ExternalAuthentication_PluginAPIStatusRecord(new ExternalAuthentication_PluginAPIStatusRecord.RecordClass({
externalAuthentication_PluginAPIStatusAttr: OS.DataTypes.ImmutableBase.getData(str)
}));
};
ExternalAuthentication_PluginAPIStatusRecord._isAnonymousRecord = true;
ExternalAuthentication_PluginAPIStatusRecord.UniqueId = "c92f7047-d198-da0a-e5db-64f7b4f443d6";
ExternalAuthentication_PluginAPIStatusRecord.init();
return ExternalAuthentication_PluginAPIStatusRecord;
})(OS.DataTypes.GenericRecord);
Extension_OMLProcessorModel.ExternalAuthentication_PluginAPIStatusRecord = ExternalAuthentication_PluginAPIStatusRecord;

});
define("Extension.OMLProcessor.model$ExternalAuthentication_PluginAPIStatusRecordList", ["exports", "OutSystems/ClientRuntime/Main", "Extension.OMLProcessor.model", "Extension.OMLProcessor.model$ExternalAuthentication_PluginAPIStatusRecord"], function (exports, OutSystems, Extension_OMLProcessorModel) {
var OS = OutSystems.Internal;
var ExternalAuthentication_PluginAPIStatusRecordList = (function (_super) {
__extends(ExternalAuthentication_PluginAPIStatusRecordList, _super);
function ExternalAuthentication_PluginAPIStatusRecordList(defaults) {
_super.apply(this, arguments);
}
ExternalAuthentication_PluginAPIStatusRecordList.itemType = Extension_OMLProcessorModel.ExternalAuthentication_PluginAPIStatusRecord;
return ExternalAuthentication_PluginAPIStatusRecordList;
})(OS.DataTypes.GenericRecordList);
Extension_OMLProcessorModel.ExternalAuthentication_PluginAPIStatusRecordList = ExternalAuthentication_PluginAPIStatusRecordList;

});
define("Extension.OMLProcessor.model$SolutionPack_SolutionVersionRecordList", ["exports", "OutSystems/ClientRuntime/Main", "Extension.OMLProcessor.model", "Extension.OMLProcessor.model$SolutionPack_SolutionVersionRecord"], function (exports, OutSystems, Extension_OMLProcessorModel) {
var OS = OutSystems.Internal;
var SolutionPack_SolutionVersionRecordList = (function (_super) {
__extends(SolutionPack_SolutionVersionRecordList, _super);
function SolutionPack_SolutionVersionRecordList(defaults) {
_super.apply(this, arguments);
}
SolutionPack_SolutionVersionRecordList.itemType = Extension_OMLProcessorModel.SolutionPack_SolutionVersionRecord;
return SolutionPack_SolutionVersionRecordList;
})(OS.DataTypes.GenericRecordList);
Extension_OMLProcessorModel.SolutionPack_SolutionVersionRecordList = SolutionPack_SolutionVersionRecordList;

});
define("Extension.OMLProcessor.model$ReferenceImageRecordList", ["exports", "OutSystems/ClientRuntime/Main", "Extension.OMLProcessor.model", "Extension.OMLProcessor.model$ReferenceImageRecord"], function (exports, OutSystems, Extension_OMLProcessorModel) {
var OS = OutSystems.Internal;
var ReferenceImageRecordList = (function (_super) {
__extends(ReferenceImageRecordList, _super);
function ReferenceImageRecordList(defaults) {
_super.apply(this, arguments);
}
ReferenceImageRecordList.itemType = Extension_OMLProcessorModel.ReferenceImageRecord;
return ReferenceImageRecordList;
})(OS.DataTypes.GenericRecordList);
Extension_OMLProcessorModel.ReferenceImageRecordList = ReferenceImageRecordList;

});
define("Extension.OMLProcessor.model$EntityDBProviderKeyRecord", ["exports", "OutSystems/ClientRuntime/Main", "Extension.OMLProcessor.model", "Extension.OMLProcessor.model$EntityDBProviderKeyRec"], function (exports, OutSystems, Extension_OMLProcessorModel) {
var OS = OutSystems.Internal;
var EntityDBProviderKeyRecord = (function (_super) {
__extends(EntityDBProviderKeyRecord, _super);
function EntityDBProviderKeyRecord(defaults) {
_super.apply(this, arguments);
}
EntityDBProviderKeyRecord.attributesToDeclare = function () {
return [
this.attr("EntityDBProviderKey", "entityDBProviderKeyAttr", "EntityDBProviderKey", false, false, OS.Types.Record, function () {
return OS.DataTypes.ImmutableBase.getData(new Extension_OMLProcessorModel.EntityDBProviderKeyRec());
}, true, Extension_OMLProcessorModel.EntityDBProviderKeyRec)
].concat(_super.attributesToDeclare.call(this));
};
EntityDBProviderKeyRecord.fromStructure = function (str) {
return new EntityDBProviderKeyRecord(new EntityDBProviderKeyRecord.RecordClass({
entityDBProviderKeyAttr: OS.DataTypes.ImmutableBase.getData(str)
}));
};
EntityDBProviderKeyRecord._isAnonymousRecord = true;
EntityDBProviderKeyRecord.UniqueId = "f5e3b8ac-5ca1-1133-451f-71578b29fe24";
EntityDBProviderKeyRecord.init();
return EntityDBProviderKeyRecord;
})(OS.DataTypes.GenericRecord);
Extension_OMLProcessorModel.EntityDBProviderKeyRecord = EntityDBProviderKeyRecord;

});
define("Extension.OMLProcessor.model$EntityDBProviderKeyRecordList", ["exports", "OutSystems/ClientRuntime/Main", "Extension.OMLProcessor.model", "Extension.OMLProcessor.model$EntityDBProviderKeyRecord"], function (exports, OutSystems, Extension_OMLProcessorModel) {
var OS = OutSystems.Internal;
var EntityDBProviderKeyRecordList = (function (_super) {
__extends(EntityDBProviderKeyRecordList, _super);
function EntityDBProviderKeyRecordList(defaults) {
_super.apply(this, arguments);
}
EntityDBProviderKeyRecordList.itemType = Extension_OMLProcessorModel.EntityDBProviderKeyRecord;
return EntityDBProviderKeyRecordList;
})(OS.DataTypes.GenericRecordList);
Extension_OMLProcessorModel.EntityDBProviderKeyRecordList = EntityDBProviderKeyRecordList;

});
define("Extension.OMLProcessor.model$EntityRecordRecordList", ["exports", "OutSystems/ClientRuntime/Main", "Extension.OMLProcessor.model", "Extension.OMLProcessor.model$EntityRecordRecord"], function (exports, OutSystems, Extension_OMLProcessorModel) {
var OS = OutSystems.Internal;
var EntityRecordRecordList = (function (_super) {
__extends(EntityRecordRecordList, _super);
function EntityRecordRecordList(defaults) {
_super.apply(this, arguments);
}
EntityRecordRecordList.itemType = Extension_OMLProcessorModel.EntityRecordRecord;
return EntityRecordRecordList;
})(OS.DataTypes.GenericRecordList);
Extension_OMLProcessorModel.EntityRecordRecordList = EntityRecordRecordList;

});
define("Extension.OMLProcessor.model$EnvironmentInfoRecord", ["exports", "OutSystems/ClientRuntime/Main", "Extension.OMLProcessor.model", "Extension.OMLProcessor.model$EnvironmentInfoRec"], function (exports, OutSystems, Extension_OMLProcessorModel) {
var OS = OutSystems.Internal;
var EnvironmentInfoRecord = (function (_super) {
__extends(EnvironmentInfoRecord, _super);
function EnvironmentInfoRecord(defaults) {
_super.apply(this, arguments);
}
EnvironmentInfoRecord.attributesToDeclare = function () {
return [
this.attr("EnvironmentInfo", "environmentInfoAttr", "EnvironmentInfo", false, false, OS.Types.Record, function () {
return OS.DataTypes.ImmutableBase.getData(new Extension_OMLProcessorModel.EnvironmentInfoRec());
}, true, Extension_OMLProcessorModel.EnvironmentInfoRec)
].concat(_super.attributesToDeclare.call(this));
};
EnvironmentInfoRecord.fromStructure = function (str) {
return new EnvironmentInfoRecord(new EnvironmentInfoRecord.RecordClass({
environmentInfoAttr: OS.DataTypes.ImmutableBase.getData(str)
}));
};
EnvironmentInfoRecord._isAnonymousRecord = true;
EnvironmentInfoRecord.UniqueId = "e6a9ec8c-f5c1-680a-7fee-feeb8dd95da9";
EnvironmentInfoRecord.init();
return EnvironmentInfoRecord;
})(OS.DataTypes.GenericRecord);
Extension_OMLProcessorModel.EnvironmentInfoRecord = EnvironmentInfoRecord;

});
define("Extension.OMLProcessor.model$EnvironmentInfoRecordList", ["exports", "OutSystems/ClientRuntime/Main", "Extension.OMLProcessor.model", "Extension.OMLProcessor.model$EnvironmentInfoRecord"], function (exports, OutSystems, Extension_OMLProcessorModel) {
var OS = OutSystems.Internal;
var EnvironmentInfoRecordList = (function (_super) {
__extends(EnvironmentInfoRecordList, _super);
function EnvironmentInfoRecordList(defaults) {
_super.apply(this, arguments);
}
EnvironmentInfoRecordList.itemType = Extension_OMLProcessorModel.EnvironmentInfoRecord;
return EnvironmentInfoRecordList;
})(OS.DataTypes.GenericRecordList);
Extension_OMLProcessorModel.EnvironmentInfoRecordList = EnvironmentInfoRecordList;

});
define("Extension.OMLProcessor.model$ProcessConfigRecordList", ["exports", "OutSystems/ClientRuntime/Main", "Extension.OMLProcessor.model", "Extension.OMLProcessor.model$ProcessConfigRecord"], function (exports, OutSystems, Extension_OMLProcessorModel) {
var OS = OutSystems.Internal;
var ProcessConfigRecordList = (function (_super) {
__extends(ProcessConfigRecordList, _super);
function ProcessConfigRecordList(defaults) {
_super.apply(this, arguments);
}
ProcessConfigRecordList.itemType = Extension_OMLProcessorModel.ProcessConfigRecord;
return ProcessConfigRecordList;
})(OS.DataTypes.GenericRecordList);
Extension_OMLProcessorModel.ProcessConfigRecordList = ProcessConfigRecordList;

});
define("Extension.OMLProcessor.model$DeploymentZone_DockerRecordList", ["exports", "OutSystems/ClientRuntime/Main", "Extension.OMLProcessor.model", "Extension.OMLProcessor.model$DeploymentZone_DockerRecord"], function (exports, OutSystems, Extension_OMLProcessorModel) {
var OS = OutSystems.Internal;
var DeploymentZone_DockerRecordList = (function (_super) {
__extends(DeploymentZone_DockerRecordList, _super);
function DeploymentZone_DockerRecordList(defaults) {
_super.apply(this, arguments);
}
DeploymentZone_DockerRecordList.itemType = Extension_OMLProcessorModel.DeploymentZone_DockerRecord;
return DeploymentZone_DockerRecordList;
})(OS.DataTypes.GenericRecordList);
Extension_OMLProcessorModel.DeploymentZone_DockerRecordList = DeploymentZone_DockerRecordList;

});
define("Extension.OMLProcessor.model$DALHighlightingDefinitionRecordList", ["exports", "OutSystems/ClientRuntime/Main", "Extension.OMLProcessor.model", "Extension.OMLProcessor.model$DALHighlightingDefinitionRecord"], function (exports, OutSystems, Extension_OMLProcessorModel) {
var OS = OutSystems.Internal;
var DALHighlightingDefinitionRecordList = (function (_super) {
__extends(DALHighlightingDefinitionRecordList, _super);
function DALHighlightingDefinitionRecordList(defaults) {
_super.apply(this, arguments);
}
DALHighlightingDefinitionRecordList.itemType = Extension_OMLProcessorModel.DALHighlightingDefinitionRecord;
return DALHighlightingDefinitionRecordList;
})(OS.DataTypes.GenericRecordList);
Extension_OMLProcessorModel.DALHighlightingDefinitionRecordList = DALHighlightingDefinitionRecordList;

});
define("Extension.OMLProcessor.model$ProcessMemDataRecordList", ["exports", "OutSystems/ClientRuntime/Main", "Extension.OMLProcessor.model", "Extension.OMLProcessor.model$ProcessMemDataRecord"], function (exports, OutSystems, Extension_OMLProcessorModel) {
var OS = OutSystems.Internal;
var ProcessMemDataRecordList = (function (_super) {
__extends(ProcessMemDataRecordList, _super);
function ProcessMemDataRecordList(defaults) {
_super.apply(this, arguments);
}
ProcessMemDataRecordList.itemType = Extension_OMLProcessorModel.ProcessMemDataRecord;
return ProcessMemDataRecordList;
})(OS.DataTypes.GenericRecordList);
Extension_OMLProcessorModel.ProcessMemDataRecordList = ProcessMemDataRecordList;

});
define("Extension.OMLProcessor.model$ReferenceableModuleRecordList", ["exports", "OutSystems/ClientRuntime/Main", "Extension.OMLProcessor.model", "Extension.OMLProcessor.model$ReferenceableModuleRecord"], function (exports, OutSystems, Extension_OMLProcessorModel) {
var OS = OutSystems.Internal;
var ReferenceableModuleRecordList = (function (_super) {
__extends(ReferenceableModuleRecordList, _super);
function ReferenceableModuleRecordList(defaults) {
_super.apply(this, arguments);
}
ReferenceableModuleRecordList.itemType = Extension_OMLProcessorModel.ReferenceableModuleRecord;
return ReferenceableModuleRecordList;
})(OS.DataTypes.GenericRecordList);
Extension_OMLProcessorModel.ReferenceableModuleRecordList = ReferenceableModuleRecordList;

});
define("Extension.OMLProcessor.model$AbstractRecordRecordList", ["exports", "OutSystems/ClientRuntime/Main", "Extension.OMLProcessor.model", "Extension.OMLProcessor.model$AbstractRecordRecord"], function (exports, OutSystems, Extension_OMLProcessorModel) {
var OS = OutSystems.Internal;
var AbstractRecordRecordList = (function (_super) {
__extends(AbstractRecordRecordList, _super);
function AbstractRecordRecordList(defaults) {
_super.apply(this, arguments);
}
AbstractRecordRecordList.itemType = Extension_OMLProcessorModel.AbstractRecordRecord;
return AbstractRecordRecordList;
})(OS.DataTypes.GenericRecordList);
Extension_OMLProcessorModel.AbstractRecordRecordList = AbstractRecordRecordList;

});
define("Extension.OMLProcessor.model$FeatureRecordList", ["exports", "OutSystems/ClientRuntime/Main", "Extension.OMLProcessor.model", "Extension.OMLProcessor.model$FeatureRecord"], function (exports, OutSystems, Extension_OMLProcessorModel) {
var OS = OutSystems.Internal;
var FeatureRecordList = (function (_super) {
__extends(FeatureRecordList, _super);
function FeatureRecordList(defaults) {
_super.apply(this, arguments);
}
FeatureRecordList.itemType = Extension_OMLProcessorModel.FeatureRecord;
return FeatureRecordList;
})(OS.DataTypes.GenericRecordList);
Extension_OMLProcessorModel.FeatureRecordList = FeatureRecordList;

});
define("Extension.OMLProcessor.model$DeploymentTechnologyRecordList", ["exports", "OutSystems/ClientRuntime/Main", "Extension.OMLProcessor.model", "Extension.OMLProcessor.model$DeploymentTechnologyRecord"], function (exports, OutSystems, Extension_OMLProcessorModel) {
var OS = OutSystems.Internal;
var DeploymentTechnologyRecordList = (function (_super) {
__extends(DeploymentTechnologyRecordList, _super);
function DeploymentTechnologyRecordList(defaults) {
_super.apply(this, arguments);
}
DeploymentTechnologyRecordList.itemType = Extension_OMLProcessorModel.DeploymentTechnologyRecord;
return DeploymentTechnologyRecordList;
})(OS.DataTypes.GenericRecordList);
Extension_OMLProcessorModel.DeploymentTechnologyRecordList = DeploymentTechnologyRecordList;

});
define("Extension.OMLProcessor.model$EspacePublishAsyncResultRecordList", ["exports", "OutSystems/ClientRuntime/Main", "Extension.OMLProcessor.model", "Extension.OMLProcessor.model$EspacePublishAsyncResultRecord"], function (exports, OutSystems, Extension_OMLProcessorModel) {
var OS = OutSystems.Internal;
var EspacePublishAsyncResultRecordList = (function (_super) {
__extends(EspacePublishAsyncResultRecordList, _super);
function EspacePublishAsyncResultRecordList(defaults) {
_super.apply(this, arguments);
}
EspacePublishAsyncResultRecordList.itemType = Extension_OMLProcessorModel.EspacePublishAsyncResultRecord;
return EspacePublishAsyncResultRecordList;
})(OS.DataTypes.GenericRecordList);
Extension_OMLProcessorModel.EspacePublishAsyncResultRecordList = EspacePublishAsyncResultRecordList;

});
define("Extension.OMLProcessor.model$ReferenceRecordList", ["exports", "OutSystems/ClientRuntime/Main", "Extension.OMLProcessor.model", "Extension.OMLProcessor.model$ReferenceRecord"], function (exports, OutSystems, Extension_OMLProcessorModel) {
var OS = OutSystems.Internal;
var ReferenceRecordList = (function (_super) {
__extends(ReferenceRecordList, _super);
function ReferenceRecordList(defaults) {
_super.apply(this, arguments);
}
ReferenceRecordList.itemType = Extension_OMLProcessorModel.ReferenceRecord;
return ReferenceRecordList;
})(OS.DataTypes.GenericRecordList);
Extension_OMLProcessorModel.ReferenceRecordList = ReferenceRecordList;

});
define("Extension.OMLProcessor.model$NativePluginConfigurationRecordList", ["exports", "OutSystems/ClientRuntime/Main", "Extension.OMLProcessor.model", "Extension.OMLProcessor.model$NativePluginConfigurationRecord"], function (exports, OutSystems, Extension_OMLProcessorModel) {
var OS = OutSystems.Internal;
var NativePluginConfigurationRecordList = (function (_super) {
__extends(NativePluginConfigurationRecordList, _super);
function NativePluginConfigurationRecordList(defaults) {
_super.apply(this, arguments);
}
NativePluginConfigurationRecordList.itemType = Extension_OMLProcessorModel.NativePluginConfigurationRecord;
return NativePluginConfigurationRecordList;
})(OS.DataTypes.GenericRecordList);
Extension_OMLProcessorModel.NativePluginConfigurationRecordList = NativePluginConfigurationRecordList;

});
define("Extension.OMLProcessor.model$FileRecord", ["exports", "OutSystems/ClientRuntime/Main", "Extension.OMLProcessor.model", "Extension.OMLProcessor.model$FileRec"], function (exports, OutSystems, Extension_OMLProcessorModel) {
var OS = OutSystems.Internal;
var FileRecord = (function (_super) {
__extends(FileRecord, _super);
function FileRecord(defaults) {
_super.apply(this, arguments);
}
FileRecord.attributesToDeclare = function () {
return [
this.attr("File", "fileAttr", "File", false, false, OS.Types.Record, function () {
return OS.DataTypes.ImmutableBase.getData(new Extension_OMLProcessorModel.FileRec());
}, true, Extension_OMLProcessorModel.FileRec)
].concat(_super.attributesToDeclare.call(this));
};
FileRecord.fromStructure = function (str) {
return new FileRecord(new FileRecord.RecordClass({
fileAttr: OS.DataTypes.ImmutableBase.getData(str)
}));
};
FileRecord._isAnonymousRecord = true;
FileRecord.UniqueId = "f62d0935-041c-a319-15c1-39c4b692d28e";
FileRecord.init();
return FileRecord;
})(OS.DataTypes.GenericRecord);
Extension_OMLProcessorModel.FileRecord = FileRecord;

});
define("Extension.OMLProcessor.model$IntegerRecordList", ["exports", "OutSystems/ClientRuntime/Main", "Extension.OMLProcessor.model", "Extension.OMLProcessor.model$IntegerRecord"], function (exports, OutSystems, Extension_OMLProcessorModel) {
var OS = OutSystems.Internal;
var IntegerRecordList = (function (_super) {
__extends(IntegerRecordList, _super);
function IntegerRecordList(defaults) {
_super.apply(this, arguments);
}
IntegerRecordList.itemType = Extension_OMLProcessorModel.IntegerRecord;
return IntegerRecordList;
})(OS.DataTypes.GenericRecordList);
Extension_OMLProcessorModel.IntegerRecordList = IntegerRecordList;

});
define("Extension.OMLProcessor.model$FileRecordList", ["exports", "OutSystems/ClientRuntime/Main", "Extension.OMLProcessor.model", "Extension.OMLProcessor.model$FileRecord"], function (exports, OutSystems, Extension_OMLProcessorModel) {
var OS = OutSystems.Internal;
var FileRecordList = (function (_super) {
__extends(FileRecordList, _super);
function FileRecordList(defaults) {
_super.apply(this, arguments);
}
FileRecordList.itemType = Extension_OMLProcessorModel.FileRecord;
return FileRecordList;
})(OS.DataTypes.GenericRecordList);
Extension_OMLProcessorModel.FileRecordList = FileRecordList;

});
define("Extension.OMLProcessor.model", ["exports", "OutSystems/ClientRuntime/Main"], function (exports, OutSystems) {
var OS = OutSystems.Internal;
var Extension_OMLProcessorModel = exports;
});
