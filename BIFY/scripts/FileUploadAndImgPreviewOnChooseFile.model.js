﻿define("FileUploadAndImgPreviewOnChooseFile.model$FileDataRec", ["exports", "OutSystems/ClientRuntime/Main", "FileUploadAndImgPreviewOnChooseFile.model"], function (exports, OutSystems, FileUploadAndImgPreviewOnChooseFileModel) {
var OS = OutSystems.Internal;
var FileDataRec = (function (_super) {
__extends(FileDataRec, _super);
function FileDataRec(defaults) {
_super.apply(this, arguments);
}
FileDataRec.attributesToDeclare = function () {
return [
this.attr("FileName", "fileNameAttr", "FileName", false, false, OS.Types.Text, function () {
return "";
}, true), 
this.attr("MimeType", "mimeTypeAttr", "MimeType", false, false, OS.Types.Text, function () {
return "";
}, true), 
this.attr("BinaryData", "binaryDataAttr", "BinaryData", false, false, OS.Types.BinaryData, function () {
return OS.DataTypes.BinaryData.defaultValue;
}, true)
].concat(_super.attributesToDeclare.call(this));
};
FileDataRec.init();
return FileDataRec;
})(OS.DataTypes.GenericRecord);
FileUploadAndImgPreviewOnChooseFileModel.FileDataRec = FileDataRec;

});
define("FileUploadAndImgPreviewOnChooseFile.model", ["exports", "OutSystems/ClientRuntime/Main"], function (exports, OutSystems) {
var OS = OutSystems.Internal;
var FileUploadAndImgPreviewOnChooseFileModel = exports;
});
