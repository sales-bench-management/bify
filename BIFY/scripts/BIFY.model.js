﻿define("BIFY.model$GiftListRec",["exports", "OutSystems/ClientRuntime/Main", "BIFY.model"], function(exports, OutSystems, BIFYModel) {
	var OS = OutSystems.Internal;
	var GiftListRec = (function(_super) {
		__extends(GiftListRec, _super);
		function GiftListRec(defaults) {
			_super.apply(this, arguments);
		}
		GiftListRec.attributesToDeclare = function() {
			return[
			this.attr("Id", "idAttr", "Id", true, false, OS.Types.LongInteger, function() {
				return OS.DataTypes.LongInteger.defaultValue;
			}
			, true),
			this.attr("GiftListName", "giftListNameAttr", "GiftListName", false, false, OS.Types.Text, function() {
				return "";
			}
			, true),
			this.attr("DateOfEvent", "dateOfEventAttr", "DateOfEvent", false, false, OS.Types.Date, function() {
				return OS.DataTypes.DateTime.defaultValue;
			}
			, true),
			this.attr("UserId_CreatedBy", "userId_CreatedByAttr", "UserId_CreatedBy", false, false, OS.Types.Integer, function() {
				return 0;
			}
			, true),
			this.attr("GiftListTypeId", "giftListTypeIdAttr", "GiftListTypeId", false, false, OS.Types.Integer, function() {
				return 0;
			}
			, true),
			this.attr("UserId_Added", "userId_AddedAttr", "UserId_Added", false, false, OS.Types.Integer, function() {
				return 0;
			}
			, true),
			this.attr("DateTimeCreated", "dateTimeCreatedAttr", "DateTimeCreated", false, false, OS.Types.DateTime, function() {
				return OS.DataTypes.DateTime.defaultValue;
			}
			, true),
			this.attr("Public", "publicAttr", "Public", false, false, OS.Types.Boolean, function() {
				return true;
			}
			, true)
			] .concat(_super.attributesToDeclare.call(this));
		};
		GiftListRec.init();
		return GiftListRec;
	}
	) (OS.DataTypes.GenericRecord);
	BIFYModel.GiftListRec = GiftListRec;

});
define("BIFY.model$FriendRequestStatusRec",["exports", "OutSystems/ClientRuntime/Main", "BIFY.model"], function(exports, OutSystems, BIFYModel) {
	var OS = OutSystems.Internal;
	var FriendRequestStatusRec = (function(_super) {
		__extends(FriendRequestStatusRec, _super);
		function FriendRequestStatusRec(defaults) {
			_super.apply(this, arguments);
		}
		FriendRequestStatusRec.attributesToDeclare = function() {
			return[
			this.attr("Id", "idAttr", "Id", true, false, OS.Types.Integer, function() {
				return 0;
			}
			, true),
			this.attr("Label", "labelAttr", "Label", true, false, OS.Types.Text, function() {
				return "";
			}
			, true),
			this.attr("Order", "orderAttr", "Order", true, false, OS.Types.Integer, function() {
				return 0;
			}
			, true),
			this.attr("Is_Active", "is_ActiveAttr", "Is_Active", true, false, OS.Types.Boolean, function() {
				return false;
			}
			, true)
			] .concat(_super.attributesToDeclare.call(this));
		};
		FriendRequestStatusRec.init();
		return FriendRequestStatusRec;
	}
	) (OS.DataTypes.GenericRecord);
	BIFYModel.FriendRequestStatusRec = FriendRequestStatusRec;

});
define("BIFY.model$MenuSubItemRec",["exports", "OutSystems/ClientRuntime/Main", "BIFY.model"], function(exports, OutSystems, BIFYModel) {
	var OS = OutSystems.Internal;
	var MenuSubItemRec = (function(_super) {
		__extends(MenuSubItemRec, _super);
		function MenuSubItemRec(defaults) {
			_super.apply(this, arguments);
		}
		MenuSubItemRec.attributesToDeclare = function() {
			return[
			this.attr("Id", "idAttr", "Id", true, false, OS.Types.Integer, function() {
				return 0;
			}
			, true),
			this.attr("Order", "orderAttr", "Order", true, false, OS.Types.Integer, function() {
				return 0;
			}
			, true),
			this.attr("Caption", "captionAttr", "Caption", true, false, OS.Types.Text, function() {
				return "";
			}
			, true),
			this.attr("MenuItemId", "menuItemIdAttr", "MenuItemId", false, false, OS.Types.Integer, function() {
				return 0;
			}
			, true)
			] .concat(_super.attributesToDeclare.call(this));
		};
		MenuSubItemRec.init();
		return MenuSubItemRec;
	}
	) (OS.DataTypes.GenericRecord);
	BIFYModel.MenuSubItemRec = MenuSubItemRec;

});
define("BIFY.model$FundRec",["exports", "OutSystems/ClientRuntime/Main", "BIFY.model"], function(exports, OutSystems, BIFYModel) {
	var OS = OutSystems.Internal;
	var FundRec = (function(_super) {
		__extends(FundRec, _super);
		function FundRec(defaults) {
			_super.apply(this, arguments);
		}
		FundRec.attributesToDeclare = function() {
			return[
			this.attr("Id", "idAttr", "Id", true, false, OS.Types.LongInteger, function() {
				return OS.DataTypes.LongInteger.defaultValue;
			}
			, true),
			this.attr("FundName", "fundNameAttr", "FundName", false, false, OS.Types.Text, function() {
				return "";
			}
			, true),
			this.attr("FundDetail", "fundDetailAttr", "FundDetail", false, false, OS.Types.Text, function() {
				return "";
			}
			, true),
			this.attr("UserId", "userIdAttr", "UserId", false, false, OS.Types.Integer, function() {
				return 0;
			}
			, true),
			this.attr("AccountNumber", "accountNumberAttr", "AccountNumber", false, false, OS.Types.Integer, function() {
				return 0;
			}
			, true),
			this.attr("SortCode", "sortCodeAttr", "SortCode", false, false, OS.Types.Integer, function() {
				return 0;
			}
			, true),
			this.attr("Target", "targetAttr", "Target", false, false, OS.Types.Currency, function() {
				return OS.DataTypes.Decimal.defaultValue;
			}
			, true),
			this.attr("StartDate", "startDateAttr", "StartDate", false, false, OS.Types.Date, function() {
				return OS.DataTypes.DateTime.defaultValue;
			}
			, true),
			this.attr("EndDate", "endDateAttr", "EndDate", false, false, OS.Types.Date, function() {
				return OS.DataTypes.DateTime.defaultValue;
			}
			, true),
			this.attr("DateCreated", "dateCreatedAttr", "DateCreated", false, false, OS.Types.DateTime, function() {
				return OS.BuiltinFunctions.currDateTime();
			}
			, true),
			this.attr("FundTypeId", "fundTypeIdAttr", "FundTypeId", false, false, OS.Types.Integer, function() {
				return 0;
			}
			, true),
			this.attr("PayPalEmail", "payPalEmailAttr", "PayPalEmail", false, false, OS.Types.Email, function() {
				return "";
			}
			, true),
			this.attr("Hash", "hashAttr", "Hash", false, false, OS.Types.Text, function() {
				return "";
			}
			, true)
			] .concat(_super.attributesToDeclare.call(this));
		};
		FundRec.init();
		return FundRec;
	}
	) (OS.DataTypes.GenericRecord);
	BIFYModel.FundRec = FundRec;

});
define("BIFY.model$GiftImagesRec",["exports", "OutSystems/ClientRuntime/Main", "BIFY.model"], function(exports, OutSystems, BIFYModel) {
	var OS = OutSystems.Internal;
	var GiftImagesRec = (function(_super) {
		__extends(GiftImagesRec, _super);
		function GiftImagesRec(defaults) {
			_super.apply(this, arguments);
		}
		GiftImagesRec.attributesToDeclare = function() {
			return[
			this.attr("Id", "idAttr", "Id", true, false, OS.Types.LongInteger, function() {
				return OS.DataTypes.LongInteger.defaultValue;
			}
			, true),
			this.attr("Image", "imageAttr", "Image", false, false, OS.Types.BinaryData, function() {
				return OS.DataTypes.BinaryData.defaultValue;
			}
			, true),
			this.attr("ContentType", "contentTypeAttr", "ContentType", false, false, OS.Types.Text, function() {
				return "";
			}
			, true),
			this.attr("GiftId", "giftIdAttr", "GiftId", false, false, OS.Types.LongInteger, function() {
				return OS.DataTypes.LongInteger.defaultValue;
			}
			, true),
			this.attr("Filename", "filenameAttr", "Filename", false, false, OS.Types.Text, function() {
				return "";
			}
			, true),
			this.attr("DateTimeCreated", "dateTimeCreatedAttr", "DateTimeCreated", false, false, OS.Types.DateTime, function() {
				return OS.BuiltinFunctions.currDateTime();
			}
			, true),
			this.attr("DateTimeModified", "dateTimeModifiedAttr", "DateTimeModified", false, false, OS.Types.DateTime, function() {
				return OS.BuiltinFunctions.currDateTime();
			}
			, true)
			] .concat(_super.attributesToDeclare.call(this));
		};
		GiftImagesRec.init();
		return GiftImagesRec;
	}
	) (OS.DataTypes.GenericRecord);
	BIFYModel.GiftImagesRec = GiftImagesRec;

});
define("BIFY.model$OrdersRec",["exports", "OutSystems/ClientRuntime/Main", "BIFY.model"], function(exports, OutSystems, BIFYModel) {
	var OS = OutSystems.Internal;
	var OrdersRec = (function(_super) {
		__extends(OrdersRec, _super);
		function OrdersRec(defaults) {
			_super.apply(this, arguments);
		}
		OrdersRec.attributesToDeclare = function() {
			return[
			this.attr("Id", "idAttr", "Id", true, false, OS.Types.LongInteger, function() {
				return OS.DataTypes.LongInteger.defaultValue;
			}
			, true),
			this.attr("GiftId", "giftIdAttr", "GiftId", false, false, OS.Types.LongInteger, function() {
				return OS.DataTypes.LongInteger.defaultValue;
			}
			, true),
			this.attr("UserIdTo", "userIdToAttr", "UserIdTo", false, false, OS.Types.Integer, function() {
				return 0;
			}
			, true),
			this.attr("UserIdFrom", "userIdFromAttr", "UserIdFrom", false, false, OS.Types.Integer, function() {
				return 0;
			}
			, true),
			this.attr("OrderStatusId", "orderStatusIdAttr", "OrderStatusId", false, false, OS.Types.Integer, function() {
				return 0;
			}
			, true),
			this.attr("RoundRobin", "roundRobinAttr", "RoundRobin", false, false, OS.Types.Boolean, function() {
				return false;
			}
			, true),
			this.attr("TrackingRef", "trackingRefAttr", "TrackingRef", false, false, OS.Types.Text, function() {
				return "";
			}
			, true),
			this.attr("DeliveryAddress", "deliveryAddressAttr", "DeliveryAddress", false, false, OS.Types.Text, function() {
				return "";
			}
			, true),
			this.attr("Personalisation_Note", "personalisation_NoteAttr", "Personalisation_Note", false, false, OS.Types.Text, function() {
				return "";
			}
			, true),
			this.attr("DateTimeCreated", "dateTimeCreatedAttr", "DateTimeCreated", false, false, OS.Types.DateTime, function() {
				return OS.BuiltinFunctions.currDateTime();
			}
			, true),
			this.attr("DateTimeModified", "dateTimeModifiedAttr", "DateTimeModified", false, false, OS.Types.DateTime, function() {
				return OS.BuiltinFunctions.currDateTime();
			}
			, true),
			this.attr("Notes", "notesAttr", "Notes", false, false, OS.Types.Text, function() {
				return "";
			}
			, true),
			this.attr("DeliveryCharges", "deliveryChargesAttr", "DeliveryCharges", false, false, OS.Types.Currency, function() {
				return OS.DataTypes.Decimal.defaultValue;
			}
			, true),
			this.attr("PaidDeliveryCharges", "paidDeliveryChargesAttr", "PaidDeliveryCharges", false, false, OS.Types.Boolean, function() {
				return false;
			}
			, true),
			this.attr("Returned", "returnedAttr", "Returned", false, false, OS.Types.Boolean, function() {
				return false;
			}
			, true),
			this.attr("ThankYouNote", "thankYouNoteAttr", "ThankYouNote", false, false, OS.Types.Text, function() {
				return "";
			}
			, true)
			] .concat(_super.attributesToDeclare.call(this));
		};
		OrdersRec.init();
		return OrdersRec;
	}
	) (OS.DataTypes.GenericRecord);
	BIFYModel.OrdersRec = OrdersRec;

});
define("BIFY.model$FriendsRec",["exports", "OutSystems/ClientRuntime/Main", "BIFY.model"], function(exports, OutSystems, BIFYModel) {
	var OS = OutSystems.Internal;
	var FriendsRec = (function(_super) {
		__extends(FriendsRec, _super);
		function FriendsRec(defaults) {
			_super.apply(this, arguments);
		}
		FriendsRec.attributesToDeclare = function() {
			return[
			this.attr("Id", "idAttr", "Id", true, false, OS.Types.LongInteger, function() {
				return OS.DataTypes.LongInteger.defaultValue;
			}
			, true),
			this.attr("User", "userAttr", "User", false, false, OS.Types.Integer, function() {
				return 0;
			}
			, true),
			this.attr("Friend", "friendAttr", "Friend", false, false, OS.Types.Integer, function() {
				return 0;
			}
			, true),
			this.attr("FriendRequestStatus", "friendRequestStatusAttr", "FriendRequestStatus", false, false, OS.Types.Integer, function() {
				return 0;
			}
			, true),
			this.attr("Seen", "seenAttr", "Seen", false, false, OS.Types.Boolean, function() {
				return false;
			}
			, true)
			] .concat(_super.attributesToDeclare.call(this));
		};
		FriendsRec.init();
		return FriendsRec;
	}
	) (OS.DataTypes.GenericRecord);
	BIFYModel.FriendsRec = FriendsRec;

});
define("BIFY.model$FundDepositsRec",["exports", "OutSystems/ClientRuntime/Main", "BIFY.model"], function(exports, OutSystems, BIFYModel) {
	var OS = OutSystems.Internal;
	var FundDepositsRec = (function(_super) {
		__extends(FundDepositsRec, _super);
		function FundDepositsRec(defaults) {
			_super.apply(this, arguments);
		}
		FundDepositsRec.attributesToDeclare = function() {
			return[
			this.attr("Id", "idAttr", "Id", true, false, OS.Types.LongInteger, function() {
				return OS.DataTypes.LongInteger.defaultValue;
			}
			, true),
			this.attr("UserIdTo", "userIdToAttr", "UserIdTo", false, false, OS.Types.Integer, function() {
				return 0;
			}
			, true),
			this.attr("UserIdFrom", "userIdFromAttr", "UserIdFrom", false, false, OS.Types.Integer, function() {
				return 0;
			}
			, true),
			this.attr("FundId", "fundIdAttr", "FundId", false, false, OS.Types.LongInteger, function() {
				return OS.DataTypes.LongInteger.defaultValue;
			}
			, true),
			this.attr("Amount", "amountAttr", "Amount", false, false, OS.Types.Currency, function() {
				return OS.DataTypes.Decimal.defaultValue;
			}
			, true),
			this.attr("Personalisation_Note", "personalisation_NoteAttr", "Personalisation_Note", false, false, OS.Types.Text, function() {
				return "";
			}
			, true),
			this.attr("DateTimeCreated", "dateTimeCreatedAttr", "DateTimeCreated", false, false, OS.Types.DateTime, function() {
				return OS.BuiltinFunctions.currDateTime();
			}
			, true),
			this.attr("DateTimeModified", "dateTimeModifiedAttr", "DateTimeModified", false, false, OS.Types.DateTime, function() {
				return OS.BuiltinFunctions.currDateTime();
			}
			, true),
			this.attr("DepositHash", "depositHashAttr", "DepositHash", false, false, OS.Types.Text, function() {
				return "";
			}
			, true),
			this.attr("Status", "statusAttr", "Status", false, false, OS.Types.Text, function() {
				return "Started";
			}
			, true),
			this.attr("ThankYouNote", "thankYouNoteAttr", "ThankYouNote", false, false, OS.Types.Text, function() {
				return "";
			}
			, true)
			] .concat(_super.attributesToDeclare.call(this));
		};
		FundDepositsRec.init();
		return FundDepositsRec;
	}
	) (OS.DataTypes.GenericRecord);
	BIFYModel.FundDepositsRec = FundDepositsRec;

});
define("BIFY.model$BlogRec",["exports", "OutSystems/ClientRuntime/Main", "BIFY.model"], function(exports, OutSystems, BIFYModel) {
	var OS = OutSystems.Internal;
	var BlogRec = (function(_super) {
		__extends(BlogRec, _super);
		function BlogRec(defaults) {
			_super.apply(this, arguments);
		}
		BlogRec.attributesToDeclare = function() {
			return[
			this.attr("Id", "idAttr", "Id", true, false, OS.Types.LongInteger, function() {
				return OS.DataTypes.LongInteger.defaultValue;
			}
			, true),
			this.attr("Title", "titleAttr", "Title", false, false, OS.Types.Text, function() {
				return "";
			}
			, true),
			this.attr("Detail", "detailAttr", "Detail", false, false, OS.Types.Text, function() {
				return "";
			}
			, true),
			this.attr("Image", "imageAttr", "Image", false, false, OS.Types.BinaryData, function() {
				return OS.DataTypes.BinaryData.defaultValue;
			}
			, true),
			this.attr("Date", "dateAttr", "Date", false, false, OS.Types.Date, function() {
				return OS.BuiltinFunctions.currDate();
			}
			, true)
			] .concat(_super.attributesToDeclare.call(this));
		};
		BlogRec.init();
		return BlogRec;
	}
	) (OS.DataTypes.GenericRecord);
	BIFYModel.BlogRec = BlogRec;

});
define("BIFY.model$GiftListTypeRec",["exports", "OutSystems/ClientRuntime/Main", "BIFY.model"], function(exports, OutSystems, BIFYModel) {
	var OS = OutSystems.Internal;
	var GiftListTypeRec = (function(_super) {
		__extends(GiftListTypeRec, _super);
		function GiftListTypeRec(defaults) {
			_super.apply(this, arguments);
		}
		GiftListTypeRec.attributesToDeclare = function() {
			return[
			this.attr("Id", "idAttr", "Id", true, false, OS.Types.Integer, function() {
				return 0;
			}
			, true),
			this.attr("Label", "labelAttr", "Label", true, false, OS.Types.Text, function() {
				return "";
			}
			, true),
			this.attr("Order", "orderAttr", "Order", true, false, OS.Types.Integer, function() {
				return 0;
			}
			, true),
			this.attr("Is_Active", "is_ActiveAttr", "Is_Active", true, false, OS.Types.Boolean, function() {
				return false;
			}
			, true)
			] .concat(_super.attributesToDeclare.call(this));
		};
		GiftListTypeRec.init();
		return GiftListTypeRec;
	}
	) (OS.DataTypes.GenericRecord);
	BIFYModel.GiftListTypeRec = GiftListTypeRec;

});
define("BIFY.model$RetailerFixesRec",["exports", "OutSystems/ClientRuntime/Main", "BIFY.model"], function(exports, OutSystems, BIFYModel) {
	var OS = OutSystems.Internal;
	var RetailerFixesRec = (function(_super) {
		__extends(RetailerFixesRec, _super);
		function RetailerFixesRec(defaults) {
			_super.apply(this, arguments);
		}
		RetailerFixesRec.attributesToDeclare = function() {
			return[
			this.attr("Id", "idAttr", "Id", true, false, OS.Types.LongInteger, function() {
				return OS.DataTypes.LongInteger.defaultValue;
			}
			, true),
			this.attr("Domain", "domainAttr", "Domain", false, false, OS.Types.Text, function() {
				return "";
			}
			, true),
			this.attr("ImageIndex", "imageIndexAttr", "ImageIndex", false, false, OS.Types.Integer, function() {
				return 0;
			}
			, true),
			this.attr("LoopImages", "loopImagesAttr", "LoopImages", false, false, OS.Types.Boolean, function() {
				return false;
			}
			, true),
			this.attr("IncludeHybridProduct", "includeHybridProductAttr", "IncludeHybridProduct", false, false, OS.Types.Boolean, function() {
				return false;
			}
			, true),
			this.attr("IncludeInfer", "includeInferAttr", "IncludeInfer", false, false, OS.Types.Boolean, function() {
				return false;
			}
			, true),
			this.attr("IncludeInferProduct", "includeInferProductAttr", "IncludeInferProduct", false, false, OS.Types.Boolean, function() {
				return false;
			}
			, true),
			this.attr("MinImgWidthHeight", "minImgWidthHeightAttr", "MinImgWidthHeight", false, false, OS.Types.Integer, function() {
				return 0;
			}
			, true),
			this.attr("FilterSizes", "filterSizesAttr", "FilterSizes", false, false, OS.Types.Boolean, function() {
				return true;
			}
			, true),
			this.attr("AllowPNG", "allowPNGAttr", "AllowPNG", false, false, OS.Types.Boolean, function() {
				return true;
			}
			, true),
			this.attr("AllowJPG", "allowJPGAttr", "AllowJPG", false, false, OS.Types.Boolean, function() {
				return true;
			}
			, true),
			this.attr("AllowSVG", "allowSVGAttr", "AllowSVG", false, false, OS.Types.Boolean, function() {
				return true;
			}
			, true),
			this.attr("IsActive", "isActiveAttr", "IsActive", false, false, OS.Types.Boolean, function() {
				return true;
			}
			, true),
			this.attr("IndexRelatesToProductImages", "indexRelatesToProductImagesAttr", "IndexRelatesToProductImages", false, false, OS.Types.Boolean, function() {
				return true;
			}
			, true),
			this.attr("ImageURLTemplate", "imageURLTemplateAttr", "ImageURLTemplate", false, false, OS.Types.Text, function() {
				return "";
			}
			, true),
			this.attr("SQLCodeToGetID", "sQLCodeToGetIDAttr", "SQLCodeToGetID", false, false, OS.Types.Text, function() {
				return "";
			}
			, true),
			this.attr("ForceProxy", "forceProxyAttr", "ForceProxy", false, false, OS.Types.Boolean, function() {
				return false;
			}
			, true)
			] .concat(_super.attributesToDeclare.call(this));
		};
		RetailerFixesRec.init();
		return RetailerFixesRec;
	}
	) (OS.DataTypes.GenericRecord);
	BIFYModel.RetailerFixesRec = RetailerFixesRec;

});
define("BIFY.model$NotificationRec",["exports", "OutSystems/ClientRuntime/Main", "BIFY.model"], function(exports, OutSystems, BIFYModel) {
	var OS = OutSystems.Internal;
	var NotificationRec = (function(_super) {
		__extends(NotificationRec, _super);
		function NotificationRec(defaults) {
			_super.apply(this, arguments);
		}
		NotificationRec.attributesToDeclare = function() {
			return[
			this.attr("Id", "idAttr", "Id", true, false, OS.Types.LongInteger, function() {
				return OS.DataTypes.LongInteger.defaultValue;
			}
			, true),
			this.attr("UserId", "userIdAttr", "UserId", false, false, OS.Types.Integer, function() {
				return 0;
			}
			, true),
			this.attr("Title", "titleAttr", "Title", false, false, OS.Types.Text, function() {
				return "";
			}
			, true),
			this.attr("Detail", "detailAttr", "Detail", false, false, OS.Types.Text, function() {
				return "";
			}
			, true),
			this.attr("ButtonLabel", "buttonLabelAttr", "ButtonLabel", false, false, OS.Types.Text, function() {
				return "";
			}
			, true),
			this.attr("ButtonURL", "buttonURLAttr", "ButtonURL", false, false, OS.Types.Text, function() {
				return "";
			}
			, true),
			this.attr("CreatedOn", "createdOnAttr", "CreatedOn", false, false, OS.Types.DateTime, function() {
				return OS.DataTypes.DateTime.defaultValue;
			}
			, true),
			this.attr("Seen", "seenAttr", "Seen", false, false, OS.Types.Boolean, function() {
				return false;
			}
			, true),
			this.attr("SeenOn", "seenOnAttr", "SeenOn", false, false, OS.Types.DateTime, function() {
				return OS.DataTypes.DateTime.defaultValue;
			}
			, true),
			this.attr("Cleared", "clearedAttr", "Cleared", false, false, OS.Types.Boolean, function() {
				return false;
			}
			, true),
			this.attr("ClearedOn", "clearedOnAttr", "ClearedOn", false, false, OS.Types.DateTime, function() {
				return OS.DataTypes.DateTime.defaultValue;
			}
			, true)
			] .concat(_super.attributesToDeclare.call(this));
		};
		NotificationRec.init();
		return NotificationRec;
	}
	) (OS.DataTypes.GenericRecord);
	BIFYModel.NotificationRec = NotificationRec;

});
define("BIFY.model$GiftsRec",["exports", "OutSystems/ClientRuntime/Main", "BIFY.model"], function(exports, OutSystems, BIFYModel) {
	var OS = OutSystems.Internal;
	var GiftsRec = (function(_super) {
		__extends(GiftsRec, _super);
		function GiftsRec(defaults) {
			_super.apply(this, arguments);
		}
		GiftsRec.attributesToDeclare = function() {
			return[
			this.attr("Id", "idAttr", "Id", true, false, OS.Types.LongInteger, function() {
				return OS.DataTypes.LongInteger.defaultValue;
			}
			, true),
			this.attr("Name", "nameAttr", "Name", false, false, OS.Types.Text, function() {
				return "";
			}
			, true),
			this.attr("YourGiftNotes", "yourGiftNotesAttr", "YourGiftNotes", false, false, OS.Types.Text, function() {
				return "";
			}
			, true),
			this.attr("ImageUrl", "imageUrlAttr", "ImageUrl", false, false, OS.Types.Text, function() {
				return "";
			}
			, true),
			this.attr("Tags", "tagsAttr", "Tags", false, false, OS.Types.Text, function() {
				return "";
			}
			, true),
			this.attr("Description", "descriptionAttr", "Description", false, false, OS.Types.Text, function() {
				return "";
			}
			, true),
			this.attr("UserId", "userIdAttr", "UserId", false, false, OS.Types.Integer, function() {
				return 0;
			}
			, true),
			this.attr("GiftTypeId", "giftTypeIdAttr", "GiftTypeId", false, false, OS.Types.Integer, function() {
				return 0;
			}
			, true),
			this.attr("Visible", "visibleAttr", "Visible", false, false, OS.Types.Boolean, function() {
				return true;
			}
			, true),
			this.attr("Price", "priceAttr", "Price", false, false, OS.Types.Currency, function() {
				return OS.DataTypes.Decimal.defaultValue;
			}
			, true),
			this.attr("CreatedOn", "createdOnAttr", "CreatedOn", false, false, OS.Types.DateTime, function() {
				return OS.BuiltinFunctions.currDateTime();
			}
			, true),
			this.attr("URL", "uRLAttr", "URL", false, false, OS.Types.Text, function() {
				return "";
			}
			, true),
			this.attr("URLInvalid", "uRLInvalidAttr", "URLInvalid", false, false, OS.Types.Boolean, function() {
				return false;
			}
			, true),
			this.attr("URLInvalidDate", "uRLInvalidDateAttr", "URLInvalidDate", false, false, OS.Types.Date, function() {
				return OS.DataTypes.DateTime.defaultValue;
			}
			, true),
			this.attr("Method", "methodAttr", "Method", false, false, OS.Types.Text, function() {
				return "URL";
			}
			, true),
			this.attr("GiftImageId", "giftImageIdAttr", "GiftImageId", false, false, OS.Types.LongInteger, function() {
				return OS.DataTypes.LongInteger.defaultValue;
			}
			, true),
			this.attr("WeddingGift", "weddingGiftAttr", "WeddingGift", false, false, OS.Types.Boolean, function() {
				return false;
			}
			, true),
			this.attr("GiftListId", "giftListIdAttr", "GiftListId", false, false, OS.Types.LongInteger, function() {
				return OS.DataTypes.LongInteger.defaultValue;
			}
			, true)
			] .concat(_super.attributesToDeclare.call(this));
		};
		GiftsRec.init();
		return GiftsRec;
	}
	) (OS.DataTypes.GenericRecord);
	BIFYModel.GiftsRec = GiftsRec;

});
define("BIFY.model$BifyUserRec",["exports", "OutSystems/ClientRuntime/Main", "BIFY.model"], function(exports, OutSystems, BIFYModel) {
	var OS = OutSystems.Internal;
	var BifyUserRec = (function(_super) {
		__extends(BifyUserRec, _super);
		function BifyUserRec(defaults) {
			_super.apply(this, arguments);
		}
		BifyUserRec.attributesToDeclare = function() {
			return[
			this.attr("Id", "idAttr", "Id", true, false, OS.Types.LongInteger, function() {
				return OS.DataTypes.LongInteger.defaultValue;
			}
			, true),
			this.attr("FirstName", "firstNameAttr", "FirstName", false, false, OS.Types.Text, function() {
				return "";
			}
			, true),
			this.attr("Surname", "surnameAttr", "Surname", false, false, OS.Types.Text, function() {
				return "";
			}
			, true),
			this.attr("DateOfBirth", "dateOfBirthAttr", "DateOfBirth", false, false, OS.Types.Date, function() {
				return OS.DataTypes.DateTime.defaultValue;
			}
			, true),
			this.attr("Gender", "genderAttr", "Gender", false, false, OS.Types.Text, function() {
				return "";
			}
			, true),
			this.attr("UserId", "userIdAttr", "UserId", false, false, OS.Types.Integer, function() {
				return 0;
			}
			, true),
			this.attr("SeenWelcomeMessage", "seenWelcomeMessageAttr", "SeenWelcomeMessage", false, false, OS.Types.Boolean, function() {
				return false;
			}
			, true),
			this.attr("Address1", "address1Attr", "Address1", false, false, OS.Types.Text, function() {
				return "";
			}
			, true),
			this.attr("Address2", "address2Attr", "Address2", false, false, OS.Types.Text, function() {
				return "";
			}
			, true),
			this.attr("Town", "townAttr", "Town", false, false, OS.Types.Text, function() {
				return "";
			}
			, true),
			this.attr("City", "cityAttr", "City", false, false, OS.Types.Text, function() {
				return "";
			}
			, true),
			this.attr("Postcode", "postcodeAttr", "Postcode", false, false, OS.Types.Text, function() {
				return "";
			}
			, true),
			this.attr("Country", "countryAttr", "Country", false, false, OS.Types.Text, function() {
				return "";
			}
			, true),
			this.attr("Bio", "bioAttr", "Bio", false, false, OS.Types.Text, function() {
				return "";
			}
			, true),
			this.attr("PublicProfile", "publicProfileAttr", "PublicProfile", false, false, OS.Types.Boolean, function() {
				return true;
			}
			, true),
			this.attr("ActivationCode", "activationCodeAttr", "ActivationCode", false, false, OS.Types.Text, function() {
				return "";
			}
			, true),
			this.attr("MobilePhone", "mobilePhoneAttr", "MobilePhone", false, false, OS.Types.PhoneNumber, function() {
				return "";
			}
			, true),
			this.attr("FacebookId", "facebookIdAttr", "FacebookId", false, false, OS.Types.Text, function() {
				return "";
			}
			, true)
			] .concat(_super.attributesToDeclare.call(this));
		};
		BifyUserRec.init();
		return BifyUserRec;
	}
	) (OS.DataTypes.GenericRecord);
	BIFYModel.BifyUserRec = BifyUserRec;

});
define("BIFY.model$MenuItemRec",["exports", "OutSystems/ClientRuntime/Main", "BIFY.model"], function(exports, OutSystems, BIFYModel) {
	var OS = OutSystems.Internal;
	var MenuItemRec = (function(_super) {
		__extends(MenuItemRec, _super);
		function MenuItemRec(defaults) {
			_super.apply(this, arguments);
		}
		MenuItemRec.attributesToDeclare = function() {
			return[
			this.attr("Id", "idAttr", "Id", true, false, OS.Types.Integer, function() {
				return 0;
			}
			, true),
			this.attr("Order", "orderAttr", "Order", true, false, OS.Types.Integer, function() {
				return 0;
			}
			, true),
			this.attr("Caption", "captionAttr", "Caption", true, false, OS.Types.Text, function() {
				return "";
			}
			, true)
			] .concat(_super.attributesToDeclare.call(this));
		};
		MenuItemRec.init();
		return MenuItemRec;
	}
	) (OS.DataTypes.GenericRecord);
	BIFYModel.MenuItemRec = MenuItemRec;

});
define("BIFY.model$FAQRec",["exports", "OutSystems/ClientRuntime/Main", "BIFY.model"], function(exports, OutSystems, BIFYModel) {
	var OS = OutSystems.Internal;
	var FAQRec = (function(_super) {
		__extends(FAQRec, _super);
		function FAQRec(defaults) {
			_super.apply(this, arguments);
		}
		FAQRec.attributesToDeclare = function() {
			return[
			this.attr("Id", "idAttr", "Id", true, false, OS.Types.LongInteger, function() {
				return OS.DataTypes.LongInteger.defaultValue;
			}
			, true),
			this.attr("Title", "titleAttr", "Title", false, false, OS.Types.Text, function() {
				return "";
			}
			, true),
			this.attr("Detail", "detailAttr", "Detail", false, false, OS.Types.Text, function() {
				return "";
			}
			, true)
			] .concat(_super.attributesToDeclare.call(this));
		};
		FAQRec.init();
		return FAQRec;
	}
	) (OS.DataTypes.GenericRecord);
	BIFYModel.FAQRec = FAQRec;

});
define("BIFY.model$FundImageRec",["exports", "OutSystems/ClientRuntime/Main", "BIFY.model"], function(exports, OutSystems, BIFYModel) {
	var OS = OutSystems.Internal;
	var FundImageRec = (function(_super) {
		__extends(FundImageRec, _super);
		function FundImageRec(defaults) {
			_super.apply(this, arguments);
		}
		FundImageRec.attributesToDeclare = function() {
			return[
			this.attr("FundId", "fundIdAttr", "FundId", true, false, OS.Types.LongInteger, function() {
				return OS.DataTypes.LongInteger.defaultValue;
			}
			, true),
			this.attr("Image", "imageAttr", "Image", false, false, OS.Types.BinaryData, function() {
				return OS.DataTypes.BinaryData.defaultValue;
			}
			, true)
			] .concat(_super.attributesToDeclare.call(this));
		};
		FundImageRec.init();
		return FundImageRec;
	}
	) (OS.DataTypes.GenericRecord);
	BIFYModel.FundImageRec = FundImageRec;

});
define("BIFY.model$OrderStatusRec",["exports", "OutSystems/ClientRuntime/Main", "BIFY.model"], function(exports, OutSystems, BIFYModel) {
	var OS = OutSystems.Internal;
	var OrderStatusRec = (function(_super) {
		__extends(OrderStatusRec, _super);
		function OrderStatusRec(defaults) {
			_super.apply(this, arguments);
		}
		OrderStatusRec.attributesToDeclare = function() {
			return[
			this.attr("Id", "idAttr", "Id", true, false, OS.Types.Integer, function() {
				return 0;
			}
			, true),
			this.attr("Label", "labelAttr", "Label", true, false, OS.Types.Text, function() {
				return "";
			}
			, true),
			this.attr("Order", "orderAttr", "Order", true, false, OS.Types.Integer, function() {
				return 0;
			}
			, true),
			this.attr("Is_Active", "is_ActiveAttr", "Is_Active", true, false, OS.Types.Boolean, function() {
				return false;
			}
			, true)
			] .concat(_super.attributesToDeclare.call(this));
		};
		OrderStatusRec.init();
		return OrderStatusRec;
	}
	) (OS.DataTypes.GenericRecord);
	BIFYModel.OrderStatusRec = OrderStatusRec;

});
define("BIFY.model$BifyUserImageRec",["exports", "OutSystems/ClientRuntime/Main", "BIFY.model"], function(exports, OutSystems, BIFYModel) {
	var OS = OutSystems.Internal;
	var BifyUserImageRec = (function(_super) {
		__extends(BifyUserImageRec, _super);
		function BifyUserImageRec(defaults) {
			_super.apply(this, arguments);
		}
		BifyUserImageRec.attributesToDeclare = function() {
			return[
			this.attr("Id", "idAttr", "Id", true, false, OS.Types.LongInteger, function() {
				return OS.DataTypes.LongInteger.defaultValue;
			}
			, true),
			this.attr("Image", "imageAttr", "Image", false, false, OS.Types.BinaryData, function() {
				return OS.DataTypes.BinaryData.defaultValue;
			}
			, true),
			this.attr("BifyUserId", "bifyUserIdAttr", "BifyUserId", false, false, OS.Types.LongInteger, function() {
				return OS.DataTypes.LongInteger.defaultValue;
			}
			, true),
			this.attr("UserId", "userIdAttr", "UserId", false, false, OS.Types.Integer, function() {
				return 0;
			}
			, true),
			this.attr("ContentType", "contentTypeAttr", "ContentType", false, false, OS.Types.Text, function() {
				return "";
			}
			, true),
			this.attr("Filename", "filenameAttr", "Filename", false, false, OS.Types.Text, function() {
				return "";
			}
			, true),
			this.attr("DateTimeCreated", "dateTimeCreatedAttr", "DateTimeCreated", false, false, OS.Types.DateTime, function() {
				return OS.BuiltinFunctions.currDateTime();
			}
			, true),
			this.attr("DateTimeModified", "dateTimeModifiedAttr", "DateTimeModified", false, false, OS.Types.DateTime, function() {
				return OS.BuiltinFunctions.currDateTime();
			}
			, true)
			] .concat(_super.attributesToDeclare.call(this));
		};
		BifyUserImageRec.init();
		return BifyUserImageRec;
	}
	) (OS.DataTypes.GenericRecord);
	BIFYModel.BifyUserImageRec = BifyUserImageRec;

});
define("BIFY.model$CroppedImageRec",["exports", "OutSystems/ClientRuntime/Main", "BIFY.model"], function(exports, OutSystems, BIFYModel) {
	var OS = OutSystems.Internal;
	var CroppedImageRec = (function(_super) {
		__extends(CroppedImageRec, _super);
		function CroppedImageRec(defaults) {
			_super.apply(this, arguments);
		}
		CroppedImageRec.attributesToDeclare = function() {
			return[
			this.attr("Id", "idAttr", "Id", true, false, OS.Types.Integer, function() {
				return 0;
			}
			, true),
			this.attr("Filename", "filenameAttr", "Filename", true, false, OS.Types.Text, function() {
				return "";
			}
			, true),
			this.attr("ContentType", "contentTypeAttr", "ContentType", true, false, OS.Types.Text, function() {
				return "";
			}
			, true),
			this.attr("Content", "contentAttr", "Content", false, false, OS.Types.BinaryData, function() {
				return OS.DataTypes.BinaryData.defaultValue;
			}
			, true),
			this.attr("CreatedAt", "createdAtAttr", "CreatedAt", true, false, OS.Types.DateTime, function() {
				return OS.BuiltinFunctions.currDateTime();
			}
			, true)
			] .concat(_super.attributesToDeclare.call(this));
		};
		CroppedImageRec.init();
		return CroppedImageRec;
	}
	) (OS.DataTypes.GenericRecord);
	BIFYModel.CroppedImageRec = CroppedImageRec;

});
define("BIFY.model$EventRec",["exports", "OutSystems/ClientRuntime/Main", "BIFY.model"], function(exports, OutSystems, BIFYModel) {
	var OS = OutSystems.Internal;
	var EventRec = (function(_super) {
		__extends(EventRec, _super);
		function EventRec(defaults) {
			_super.apply(this, arguments);
		}
		EventRec.attributesToDeclare = function() {
			return[
			this.attr("Id", "idAttr", "Id", true, false, OS.Types.LongInteger, function() {
				return OS.DataTypes.LongInteger.defaultValue;
			}
			, true),
			this.attr("EventName", "eventNameAttr", "EventName", false, false, OS.Types.Text, function() {
				return "";
			}
			, true),
			this.attr("EventDate", "eventDateAttr", "EventDate", false, false, OS.Types.Date, function() {
				return OS.DataTypes.DateTime.defaultValue;
			}
			, true),
			this.attr("UserId", "userIdAttr", "UserId", false, false, OS.Types.Integer, function() {
				return 0;
			}
			, true),
			this.attr("EventTypeId", "eventTypeIdAttr", "EventTypeId", false, false, OS.Types.Integer, function() {
				return 0;
			}
			, true),
			this.attr("Public", "publicAttr", "Public", false, false, OS.Types.Boolean, function() {
				return true;
			}
			, true),
			this.attr("Recurring", "recurringAttr", "Recurring", false, false, OS.Types.Boolean, function() {
				return false;
			}
			, true)
			] .concat(_super.attributesToDeclare.call(this));
		};
		EventRec.init();
		return EventRec;
	}
	) (OS.DataTypes.GenericRecord);
	BIFYModel.EventRec = EventRec;

});
define("BIFY.model$GiftTypeRec",["exports", "OutSystems/ClientRuntime/Main", "BIFY.model"], function(exports, OutSystems, BIFYModel) {
	var OS = OutSystems.Internal;
	var GiftTypeRec = (function(_super) {
		__extends(GiftTypeRec, _super);
		function GiftTypeRec(defaults) {
			_super.apply(this, arguments);
		}
		GiftTypeRec.attributesToDeclare = function() {
			return[
			this.attr("Id", "idAttr", "Id", true, false, OS.Types.Integer, function() {
				return 0;
			}
			, true),
			this.attr("Label", "labelAttr", "Label", true, false, OS.Types.Text, function() {
				return "";
			}
			, true),
			this.attr("Order", "orderAttr", "Order", true, false, OS.Types.Integer, function() {
				return 0;
			}
			, true),
			this.attr("Is_Active", "is_ActiveAttr", "Is_Active", true, false, OS.Types.Boolean, function() {
				return false;
			}
			, true)
			] .concat(_super.attributesToDeclare.call(this));
		};
		GiftTypeRec.init();
		return GiftTypeRec;
	}
	) (OS.DataTypes.GenericRecord);
	BIFYModel.GiftTypeRec = GiftTypeRec;

});
define("BIFY.model$EventTypeRec",["exports", "OutSystems/ClientRuntime/Main", "BIFY.model"], function(exports, OutSystems, BIFYModel) {
	var OS = OutSystems.Internal;
	var EventTypeRec = (function(_super) {
		__extends(EventTypeRec, _super);
		function EventTypeRec(defaults) {
			_super.apply(this, arguments);
		}
		EventTypeRec.attributesToDeclare = function() {
			return[
			this.attr("Id", "idAttr", "Id", true, false, OS.Types.Integer, function() {
				return 0;
			}
			, true),
			this.attr("Label", "labelAttr", "Label", true, false, OS.Types.Text, function() {
				return "";
			}
			, true),
			this.attr("Order", "orderAttr", "Order", true, false, OS.Types.Integer, function() {
				return 0;
			}
			, true),
			this.attr("Is_Active", "is_ActiveAttr", "Is_Active", true, false, OS.Types.Boolean, function() {
				return false;
			}
			, true)
			] .concat(_super.attributesToDeclare.call(this));
		};
		EventTypeRec.init();
		return EventTypeRec;
	}
	) (OS.DataTypes.GenericRecord);
	BIFYModel.EventTypeRec = EventTypeRec;

});
define("BIFY.model$FundTypeRec",["exports", "OutSystems/ClientRuntime/Main", "BIFY.model"], function(exports, OutSystems, BIFYModel) {
	var OS = OutSystems.Internal;
	var FundTypeRec = (function(_super) {
		__extends(FundTypeRec, _super);
		function FundTypeRec(defaults) {
			_super.apply(this, arguments);
		}
		FundTypeRec.attributesToDeclare = function() {
			return[
			this.attr("Id", "idAttr", "Id", true, false, OS.Types.Integer, function() {
				return 0;
			}
			, true),
			this.attr("Label", "labelAttr", "Label", true, false, OS.Types.Text, function() {
				return "";
			}
			, true),
			this.attr("Order", "orderAttr", "Order", true, false, OS.Types.Integer, function() {
				return 0;
			}
			, true),
			this.attr("Is_Active", "is_ActiveAttr", "Is_Active", true, false, OS.Types.Boolean, function() {
				return false;
			}
			, true)
			] .concat(_super.attributesToDeclare.call(this));
		};
		FundTypeRec.init();
		return FundTypeRec;
	}
	) (OS.DataTypes.GenericRecord);
	BIFYModel.FundTypeRec = FundTypeRec;

});
define("BIFY.model",["exports", "OutSystems/ClientRuntime/Main"], function(exports, OutSystems) {
	var OS = OutSystems.Internal;
	var BIFYModel = exports;
});
