﻿define("Extension.Authentication.model$SAMLValidationRec", ["exports", "OutSystems/ClientRuntime/Main", "Extension.Authentication.model"], function (exports, OutSystems, Extension_AuthenticationModel) {
var OS = OutSystems.Internal;
var SAMLValidationRec = (function (_super) {
__extends(SAMLValidationRec, _super);
function SAMLValidationRec(defaults) {
_super.apply(this, arguments);
}
SAMLValidationRec.attributesToDeclare = function () {
return [
this.attr("Success", "successAttr", "Success", true, false, OS.Types.Boolean, function () {
return false;
}, true), 
this.attr("Error", "errorAttr", "Error", true, false, OS.Types.Text, function () {
return "";
}, true)
].concat(_super.attributesToDeclare.call(this));
};
SAMLValidationRec.init();
return SAMLValidationRec;
})(OS.DataTypes.GenericRecord);
Extension_AuthenticationModel.SAMLValidationRec = SAMLValidationRec;

});
define("Extension.Authentication.model$SAMLValidationRecord", ["exports", "OutSystems/ClientRuntime/Main", "Extension.Authentication.model", "Extension.Authentication.model$SAMLValidationRec"], function (exports, OutSystems, Extension_AuthenticationModel) {
var OS = OutSystems.Internal;
var SAMLValidationRecord = (function (_super) {
__extends(SAMLValidationRecord, _super);
function SAMLValidationRecord(defaults) {
_super.apply(this, arguments);
}
SAMLValidationRecord.attributesToDeclare = function () {
return [
this.attr("SAMLValidation", "sAMLValidationAttr", "SAMLValidation", false, false, OS.Types.Record, function () {
return OS.DataTypes.ImmutableBase.getData(new Extension_AuthenticationModel.SAMLValidationRec());
}, true, Extension_AuthenticationModel.SAMLValidationRec)
].concat(_super.attributesToDeclare.call(this));
};
SAMLValidationRecord.fromStructure = function (str) {
return new SAMLValidationRecord(new SAMLValidationRecord.RecordClass({
sAMLValidationAttr: OS.DataTypes.ImmutableBase.getData(str)
}));
};
SAMLValidationRecord._isAnonymousRecord = true;
SAMLValidationRecord.UniqueId = "988ce50a-2b1f-92f3-f1da-068b7b7c0ecd";
SAMLValidationRecord.init();
return SAMLValidationRecord;
})(OS.DataTypes.GenericRecord);
Extension_AuthenticationModel.SAMLValidationRecord = SAMLValidationRecord;

});
define("Extension.Authentication.model$SAMLValidationRecordList", ["exports", "OutSystems/ClientRuntime/Main", "Extension.Authentication.model", "Extension.Authentication.model$SAMLValidationRecord"], function (exports, OutSystems, Extension_AuthenticationModel) {
var OS = OutSystems.Internal;
var SAMLValidationRecordList = (function (_super) {
__extends(SAMLValidationRecordList, _super);
function SAMLValidationRecordList(defaults) {
_super.apply(this, arguments);
}
SAMLValidationRecordList.itemType = Extension_AuthenticationModel.SAMLValidationRecord;
return SAMLValidationRecordList;
})(OS.DataTypes.GenericRecordList);
Extension_AuthenticationModel.SAMLValidationRecordList = SAMLValidationRecordList;

});
define("Extension.Authentication.model$LogoutResponseRec", ["exports", "OutSystems/ClientRuntime/Main", "Extension.Authentication.model"], function (exports, OutSystems, Extension_AuthenticationModel) {
var OS = OutSystems.Internal;
var LogoutResponseRec = (function (_super) {
__extends(LogoutResponseRec, _super);
function LogoutResponseRec(defaults) {
_super.apply(this, arguments);
}
LogoutResponseRec.attributesToDeclare = function () {
return [
this.attr("RequestId", "requestIdAttr", "RequestId", true, false, OS.Types.Text, function () {
return "";
}, true), 
this.attr("Success", "successAttr", "Success", true, false, OS.Types.Boolean, function () {
return false;
}, true), 
this.attr("Partial", "partialAttr", "Partial", true, false, OS.Types.Boolean, function () {
return false;
}, true), 
this.attr("ReasonError", "reasonErrorAttr", "ReasonError", true, false, OS.Types.Text, function () {
return "";
}, true), 
this.attr("IssueInstant", "issueInstantAttr", "IssueInstant", true, false, OS.Types.DateTime, function () {
return OS.DataTypes.DateTime.defaultValue;
}, true), 
this.attr("ResponseToMessageId", "responseToMessageIdAttr", "ResponseToMessageId", true, false, OS.Types.Text, function () {
return "";
}, true)
].concat(_super.attributesToDeclare.call(this));
};
LogoutResponseRec.init();
return LogoutResponseRec;
})(OS.DataTypes.GenericRecord);
Extension_AuthenticationModel.LogoutResponseRec = LogoutResponseRec;

});
define("Extension.Authentication.model$LogoutResponseRecord", ["exports", "OutSystems/ClientRuntime/Main", "Extension.Authentication.model", "Extension.Authentication.model$LogoutResponseRec"], function (exports, OutSystems, Extension_AuthenticationModel) {
var OS = OutSystems.Internal;
var LogoutResponseRecord = (function (_super) {
__extends(LogoutResponseRecord, _super);
function LogoutResponseRecord(defaults) {
_super.apply(this, arguments);
}
LogoutResponseRecord.attributesToDeclare = function () {
return [
this.attr("LogoutResponse", "logoutResponseAttr", "LogoutResponse", false, false, OS.Types.Record, function () {
return OS.DataTypes.ImmutableBase.getData(new Extension_AuthenticationModel.LogoutResponseRec());
}, true, Extension_AuthenticationModel.LogoutResponseRec)
].concat(_super.attributesToDeclare.call(this));
};
LogoutResponseRecord.fromStructure = function (str) {
return new LogoutResponseRecord(new LogoutResponseRecord.RecordClass({
logoutResponseAttr: OS.DataTypes.ImmutableBase.getData(str)
}));
};
LogoutResponseRecord._isAnonymousRecord = true;
LogoutResponseRecord.UniqueId = "04c0dc8d-a02d-1495-77c6-56def832c57e";
LogoutResponseRecord.init();
return LogoutResponseRecord;
})(OS.DataTypes.GenericRecord);
Extension_AuthenticationModel.LogoutResponseRecord = LogoutResponseRecord;

});
define("Extension.Authentication.model$AttributeRec", ["exports", "OutSystems/ClientRuntime/Main", "Extension.Authentication.model"], function (exports, OutSystems, Extension_AuthenticationModel) {
var OS = OutSystems.Internal;
var AttributeRec = (function (_super) {
__extends(AttributeRec, _super);
function AttributeRec(defaults) {
_super.apply(this, arguments);
}
AttributeRec.attributesToDeclare = function () {
return [
this.attr("Name", "nameAttr", "Name", true, false, OS.Types.Text, function () {
return "";
}, true), 
this.attr("Value", "valueAttr", "Value", true, false, OS.Types.Text, function () {
return "";
}, true)
].concat(_super.attributesToDeclare.call(this));
};
AttributeRec.init();
return AttributeRec;
})(OS.DataTypes.GenericRecord);
Extension_AuthenticationModel.AttributeRec = AttributeRec;

});
define("Extension.Authentication.model$AttributeRecord", ["exports", "OutSystems/ClientRuntime/Main", "Extension.Authentication.model", "Extension.Authentication.model$AttributeRec"], function (exports, OutSystems, Extension_AuthenticationModel) {
var OS = OutSystems.Internal;
var AttributeRecord = (function (_super) {
__extends(AttributeRecord, _super);
function AttributeRecord(defaults) {
_super.apply(this, arguments);
}
AttributeRecord.attributesToDeclare = function () {
return [
this.attr("Attribute", "attributeAttr", "Attribute", false, false, OS.Types.Record, function () {
return OS.DataTypes.ImmutableBase.getData(new Extension_AuthenticationModel.AttributeRec());
}, true, Extension_AuthenticationModel.AttributeRec)
].concat(_super.attributesToDeclare.call(this));
};
AttributeRecord.fromStructure = function (str) {
return new AttributeRecord(new AttributeRecord.RecordClass({
attributeAttr: OS.DataTypes.ImmutableBase.getData(str)
}));
};
AttributeRecord._isAnonymousRecord = true;
AttributeRecord.UniqueId = "0534485f-daa2-851c-1b0d-eb8acda7d17a";
AttributeRecord.init();
return AttributeRecord;
})(OS.DataTypes.GenericRecord);
Extension_AuthenticationModel.AttributeRecord = AttributeRecord;

});
define("Extension.Authentication.model$TextRec", ["exports", "OutSystems/ClientRuntime/Main", "Extension.Authentication.model"], function (exports, OutSystems, Extension_AuthenticationModel) {
var OS = OutSystems.Internal;
var TextRec = (function (_super) {
__extends(TextRec, _super);
function TextRec(defaults) {
_super.apply(this, arguments);
}
TextRec.attributesToDeclare = function () {
return [
this.attr("Value", "valueAttr", "Value", true, false, OS.Types.Text, function () {
return "";
}, true)
].concat(_super.attributesToDeclare.call(this));
};
TextRec.fromStructure = function (str) {
return new TextRec(new TextRec.RecordClass({
valueAttr: OS.DataTypes.ImmutableBase.getData(str)
}));
};
TextRec.init();
return TextRec;
})(OS.DataTypes.GenericRecord);
Extension_AuthenticationModel.TextRec = TextRec;

});
define("Extension.Authentication.model$TextRecord", ["exports", "OutSystems/ClientRuntime/Main", "Extension.Authentication.model", "Extension.Authentication.model$TextRec"], function (exports, OutSystems, Extension_AuthenticationModel) {
var OS = OutSystems.Internal;
var TextRecord = (function (_super) {
__extends(TextRecord, _super);
function TextRecord(defaults) {
_super.apply(this, arguments);
}
TextRecord.attributesToDeclare = function () {
return [
this.attr("Text", "textAttr", "Text", false, false, OS.Types.Record, function () {
return OS.DataTypes.ImmutableBase.getData(new Extension_AuthenticationModel.TextRec());
}, true, Extension_AuthenticationModel.TextRec)
].concat(_super.attributesToDeclare.call(this));
};
TextRecord.fromStructure = function (str) {
return new TextRecord(new TextRecord.RecordClass({
textAttr: OS.DataTypes.ImmutableBase.getData(str)
}));
};
TextRecord._isAnonymousRecord = true;
TextRecord.UniqueId = "0b862331-c103-5382-57a1-04a98dcc0831";
TextRecord.init();
return TextRecord;
})(OS.DataTypes.GenericRecord);
Extension_AuthenticationModel.TextRecord = TextRecord;

});
define("Extension.Authentication.model$HTTPHeaderRec", ["exports", "OutSystems/ClientRuntime/Main", "Extension.Authentication.model"], function (exports, OutSystems, Extension_AuthenticationModel) {
var OS = OutSystems.Internal;
var HTTPHeaderRec = (function (_super) {
__extends(HTTPHeaderRec, _super);
function HTTPHeaderRec(defaults) {
_super.apply(this, arguments);
}
HTTPHeaderRec.attributesToDeclare = function () {
return [
this.attr("Name", "nameAttr", "Name", true, false, OS.Types.Text, function () {
return "";
}, true), 
this.attr("Value", "valueAttr", "Value", true, false, OS.Types.Text, function () {
return "";
}, true)
].concat(_super.attributesToDeclare.call(this));
};
HTTPHeaderRec.init();
return HTTPHeaderRec;
})(OS.DataTypes.GenericRecord);
Extension_AuthenticationModel.HTTPHeaderRec = HTTPHeaderRec;

});
define("Extension.Authentication.model$HTTPHeaderRecord", ["exports", "OutSystems/ClientRuntime/Main", "Extension.Authentication.model", "Extension.Authentication.model$HTTPHeaderRec"], function (exports, OutSystems, Extension_AuthenticationModel) {
var OS = OutSystems.Internal;
var HTTPHeaderRecord = (function (_super) {
__extends(HTTPHeaderRecord, _super);
function HTTPHeaderRecord(defaults) {
_super.apply(this, arguments);
}
HTTPHeaderRecord.attributesToDeclare = function () {
return [
this.attr("HTTPHeader", "hTTPHeaderAttr", "HTTPHeader", false, false, OS.Types.Record, function () {
return OS.DataTypes.ImmutableBase.getData(new Extension_AuthenticationModel.HTTPHeaderRec());
}, true, Extension_AuthenticationModel.HTTPHeaderRec)
].concat(_super.attributesToDeclare.call(this));
};
HTTPHeaderRecord.fromStructure = function (str) {
return new HTTPHeaderRecord(new HTTPHeaderRecord.RecordClass({
hTTPHeaderAttr: OS.DataTypes.ImmutableBase.getData(str)
}));
};
HTTPHeaderRecord._isAnonymousRecord = true;
HTTPHeaderRecord.UniqueId = "e3fe1428-c399-2e4a-7d0c-afe4a6c3f967";
HTTPHeaderRecord.init();
return HTTPHeaderRecord;
})(OS.DataTypes.GenericRecord);
Extension_AuthenticationModel.HTTPHeaderRecord = HTTPHeaderRecord;

});
define("Extension.Authentication.model$HTTPHeaderRecordList", ["exports", "OutSystems/ClientRuntime/Main", "Extension.Authentication.model", "Extension.Authentication.model$HTTPHeaderRecord"], function (exports, OutSystems, Extension_AuthenticationModel) {
var OS = OutSystems.Internal;
var HTTPHeaderRecordList = (function (_super) {
__extends(HTTPHeaderRecordList, _super);
function HTTPHeaderRecordList(defaults) {
_super.apply(this, arguments);
}
HTTPHeaderRecordList.itemType = Extension_AuthenticationModel.HTTPHeaderRecord;
return HTTPHeaderRecordList;
})(OS.DataTypes.GenericRecordList);
Extension_AuthenticationModel.HTTPHeaderRecordList = HTTPHeaderRecordList;

});
define("Extension.Authentication.model$AttributeRecordList", ["exports", "OutSystems/ClientRuntime/Main", "Extension.Authentication.model", "Extension.Authentication.model$AttributeRecord"], function (exports, OutSystems, Extension_AuthenticationModel) {
var OS = OutSystems.Internal;
var AttributeRecordList = (function (_super) {
__extends(AttributeRecordList, _super);
function AttributeRecordList(defaults) {
_super.apply(this, arguments);
}
AttributeRecordList.itemType = Extension_AuthenticationModel.AttributeRecord;
return AttributeRecordList;
})(OS.DataTypes.GenericRecordList);
Extension_AuthenticationModel.AttributeRecordList = AttributeRecordList;

});
define("Extension.Authentication.model$DataRec", ["exports", "OutSystems/ClientRuntime/Main", "Extension.Authentication.model", "Extension.Authentication.model$AttributeRecordList"], function (exports, OutSystems, Extension_AuthenticationModel) {
var OS = OutSystems.Internal;
var DataRec = (function (_super) {
__extends(DataRec, _super);
function DataRec(defaults) {
_super.apply(this, arguments);
}
DataRec.attributesToDeclare = function () {
return [
this.attr("NameID", "nameIDAttr", "NameID", true, false, OS.Types.Text, function () {
return "";
}, true), 
this.attr("Audience", "audienceAttr", "Audience", true, false, OS.Types.Text, function () {
return "";
}, true), 
this.attr("AttributeStatement", "attributeStatementAttr", "AttributeStatement", true, false, OS.Types.RecordList, function () {
return OS.DataTypes.ImmutableBase.getData(new Extension_AuthenticationModel.AttributeRecordList());
}, true, Extension_AuthenticationModel.AttributeRecordList), 
this.attr("DestinationURL", "destinationURLAttr", "DestinationURL", true, false, OS.Types.Text, function () {
return "";
}, true), 
this.attr("RecipientURL", "recipientURLAttr", "RecipientURL", true, false, OS.Types.Text, function () {
return "";
}, true), 
this.attr("SessionIndex", "sessionIndexAttr", "SessionIndex", true, false, OS.Types.Text, function () {
return "";
}, true), 
this.attr("InResponseToMessageId", "inResponseToMessageIdAttr", "InResponseToMessageId", true, false, OS.Types.Text, function () {
return "";
}, true), 
this.attr("DecryptedAssertion", "decryptedAssertionAttr", "DecryptedAssertion", true, false, OS.Types.Text, function () {
return "";
}, true)
].concat(_super.attributesToDeclare.call(this));
};
DataRec.init();
return DataRec;
})(OS.DataTypes.GenericRecord);
Extension_AuthenticationModel.DataRec = DataRec;

});
define("Extension.Authentication.model$DataRecord", ["exports", "OutSystems/ClientRuntime/Main", "Extension.Authentication.model", "Extension.Authentication.model$DataRec"], function (exports, OutSystems, Extension_AuthenticationModel) {
var OS = OutSystems.Internal;
var DataRecord = (function (_super) {
__extends(DataRecord, _super);
function DataRecord(defaults) {
_super.apply(this, arguments);
}
DataRecord.attributesToDeclare = function () {
return [
this.attr("Data", "dataAttr", "Data", false, false, OS.Types.Record, function () {
return OS.DataTypes.ImmutableBase.getData(new Extension_AuthenticationModel.DataRec());
}, true, Extension_AuthenticationModel.DataRec)
].concat(_super.attributesToDeclare.call(this));
};
DataRecord.fromStructure = function (str) {
return new DataRecord(new DataRecord.RecordClass({
dataAttr: OS.DataTypes.ImmutableBase.getData(str)
}));
};
DataRecord._isAnonymousRecord = true;
DataRecord.UniqueId = "9aab80f9-e56e-2792-96ef-20dbbfc10548";
DataRecord.init();
return DataRecord;
})(OS.DataTypes.GenericRecord);
Extension_AuthenticationModel.DataRecord = DataRecord;

});
define("Extension.Authentication.model$DataRecordList", ["exports", "OutSystems/ClientRuntime/Main", "Extension.Authentication.model", "Extension.Authentication.model$DataRecord"], function (exports, OutSystems, Extension_AuthenticationModel) {
var OS = OutSystems.Internal;
var DataRecordList = (function (_super) {
__extends(DataRecordList, _super);
function DataRecordList(defaults) {
_super.apply(this, arguments);
}
DataRecordList.itemType = Extension_AuthenticationModel.DataRecord;
return DataRecordList;
})(OS.DataTypes.GenericRecordList);
Extension_AuthenticationModel.DataRecordList = DataRecordList;

});
define("Extension.Authentication.model$PropertyRec", ["exports", "OutSystems/ClientRuntime/Main", "Extension.Authentication.model"], function (exports, OutSystems, Extension_AuthenticationModel) {
var OS = OutSystems.Internal;
var PropertyRec = (function (_super) {
__extends(PropertyRec, _super);
function PropertyRec(defaults) {
_super.apply(this, arguments);
}
PropertyRec.attributesToDeclare = function () {
return [
this.attr("Name", "nameAttr", "Name", false, false, OS.Types.Text, function () {
return "";
}, true), 
this.attr("Value", "valueAttr", "Value", false, false, OS.Types.Text, function () {
return "";
}, true), 
this.attr("MoreValues", "moreValuesAttr", "MoreValues", false, false, OS.Types.Boolean, function () {
return false;
}, true)
].concat(_super.attributesToDeclare.call(this));
};
PropertyRec.init();
return PropertyRec;
})(OS.DataTypes.GenericRecord);
Extension_AuthenticationModel.PropertyRec = PropertyRec;

});
define("Extension.Authentication.model$PropertyRecord", ["exports", "OutSystems/ClientRuntime/Main", "Extension.Authentication.model", "Extension.Authentication.model$PropertyRec"], function (exports, OutSystems, Extension_AuthenticationModel) {
var OS = OutSystems.Internal;
var PropertyRecord = (function (_super) {
__extends(PropertyRecord, _super);
function PropertyRecord(defaults) {
_super.apply(this, arguments);
}
PropertyRecord.attributesToDeclare = function () {
return [
this.attr("Property", "propertyAttr", "Property", false, false, OS.Types.Record, function () {
return OS.DataTypes.ImmutableBase.getData(new Extension_AuthenticationModel.PropertyRec());
}, true, Extension_AuthenticationModel.PropertyRec)
].concat(_super.attributesToDeclare.call(this));
};
PropertyRecord.fromStructure = function (str) {
return new PropertyRecord(new PropertyRecord.RecordClass({
propertyAttr: OS.DataTypes.ImmutableBase.getData(str)
}));
};
PropertyRecord._isAnonymousRecord = true;
PropertyRecord.UniqueId = "2aecf770-0991-dc7a-4e48-23959f585367";
PropertyRecord.init();
return PropertyRecord;
})(OS.DataTypes.GenericRecord);
Extension_AuthenticationModel.PropertyRecord = PropertyRecord;

});
define("Extension.Authentication.model$ContactPersonRec", ["exports", "OutSystems/ClientRuntime/Main", "Extension.Authentication.model"], function (exports, OutSystems, Extension_AuthenticationModel) {
var OS = OutSystems.Internal;
var ContactPersonRec = (function (_super) {
__extends(ContactPersonRec, _super);
function ContactPersonRec(defaults) {
_super.apply(this, arguments);
}
ContactPersonRec.attributesToDeclare = function () {
return [
this.attr("Company", "companyAttr", "Company", true, false, OS.Types.Text, function () {
return "";
}, true), 
this.attr("GivenName", "givenNameAttr", "GivenName", true, false, OS.Types.Text, function () {
return "";
}, true), 
this.attr("SurName", "surNameAttr", "SurName", true, false, OS.Types.Text, function () {
return "";
}, true), 
this.attr("EmailAddress", "emailAddressAttr", "EmailAddress", true, false, OS.Types.Email, function () {
return "";
}, true), 
this.attr("TelephoneNumber", "telephoneNumberAttr", "TelephoneNumber", true, false, OS.Types.PhoneNumber, function () {
return "";
}, true)
].concat(_super.attributesToDeclare.call(this));
};
ContactPersonRec.init();
return ContactPersonRec;
})(OS.DataTypes.GenericRecord);
Extension_AuthenticationModel.ContactPersonRec = ContactPersonRec;

});
define("Extension.Authentication.model$ContactPersonRecord", ["exports", "OutSystems/ClientRuntime/Main", "Extension.Authentication.model", "Extension.Authentication.model$ContactPersonRec"], function (exports, OutSystems, Extension_AuthenticationModel) {
var OS = OutSystems.Internal;
var ContactPersonRecord = (function (_super) {
__extends(ContactPersonRecord, _super);
function ContactPersonRecord(defaults) {
_super.apply(this, arguments);
}
ContactPersonRecord.attributesToDeclare = function () {
return [
this.attr("ContactPerson", "contactPersonAttr", "ContactPerson", false, false, OS.Types.Record, function () {
return OS.DataTypes.ImmutableBase.getData(new Extension_AuthenticationModel.ContactPersonRec());
}, true, Extension_AuthenticationModel.ContactPersonRec)
].concat(_super.attributesToDeclare.call(this));
};
ContactPersonRecord.fromStructure = function (str) {
return new ContactPersonRecord(new ContactPersonRecord.RecordClass({
contactPersonAttr: OS.DataTypes.ImmutableBase.getData(str)
}));
};
ContactPersonRecord._isAnonymousRecord = true;
ContactPersonRecord.UniqueId = "3a024e7d-9c68-86c6-50e9-acf3999c524a";
ContactPersonRecord.init();
return ContactPersonRecord;
})(OS.DataTypes.GenericRecord);
Extension_AuthenticationModel.ContactPersonRecord = ContactPersonRecord;

});
define("Extension.Authentication.model$TextRecordList", ["exports", "OutSystems/ClientRuntime/Main", "Extension.Authentication.model", "Extension.Authentication.model$TextRecord"], function (exports, OutSystems, Extension_AuthenticationModel) {
var OS = OutSystems.Internal;
var TextRecordList = (function (_super) {
__extends(TextRecordList, _super);
function TextRecordList(defaults) {
_super.apply(this, arguments);
}
TextRecordList.itemType = Extension_AuthenticationModel.TextRecord;
return TextRecordList;
})(OS.DataTypes.GenericRecordList);
Extension_AuthenticationModel.TextRecordList = TextRecordList;

});
define("Extension.Authentication.model$LogoutRequestRec", ["exports", "OutSystems/ClientRuntime/Main", "Extension.Authentication.model", "Extension.Authentication.model$TextRecordList"], function (exports, OutSystems, Extension_AuthenticationModel) {
var OS = OutSystems.Internal;
var LogoutRequestRec = (function (_super) {
__extends(LogoutRequestRec, _super);
function LogoutRequestRec(defaults) {
_super.apply(this, arguments);
}
LogoutRequestRec.attributesToDeclare = function () {
return [
this.attr("IssueInstant", "issueInstantAttr", "IssueInstant", true, false, OS.Types.DateTime, function () {
return OS.DataTypes.DateTime.defaultValue;
}, true), 
this.attr("Issuer", "issuerAttr", "Issuer", true, false, OS.Types.Text, function () {
return "";
}, true), 
this.attr("NameID", "nameIDAttr", "NameID", true, false, OS.Types.Text, function () {
return "";
}, true), 
this.attr("SessionIndexes", "sessionIndexesAttr", "SessionIndexes", true, false, OS.Types.RecordList, function () {
return OS.DataTypes.ImmutableBase.getData(new Extension_AuthenticationModel.TextRecordList());
}, true, Extension_AuthenticationModel.TextRecordList), 
this.attr("RequestId", "requestIdAttr", "RequestId", true, false, OS.Types.Text, function () {
return "";
}, true)
].concat(_super.attributesToDeclare.call(this));
};
LogoutRequestRec.init();
return LogoutRequestRec;
})(OS.DataTypes.GenericRecord);
Extension_AuthenticationModel.LogoutRequestRec = LogoutRequestRec;

});
define("Extension.Authentication.model$LogoutRequestRecord", ["exports", "OutSystems/ClientRuntime/Main", "Extension.Authentication.model", "Extension.Authentication.model$LogoutRequestRec"], function (exports, OutSystems, Extension_AuthenticationModel) {
var OS = OutSystems.Internal;
var LogoutRequestRecord = (function (_super) {
__extends(LogoutRequestRecord, _super);
function LogoutRequestRecord(defaults) {
_super.apply(this, arguments);
}
LogoutRequestRecord.attributesToDeclare = function () {
return [
this.attr("LogoutRequest", "logoutRequestAttr", "LogoutRequest", false, false, OS.Types.Record, function () {
return OS.DataTypes.ImmutableBase.getData(new Extension_AuthenticationModel.LogoutRequestRec());
}, true, Extension_AuthenticationModel.LogoutRequestRec)
].concat(_super.attributesToDeclare.call(this));
};
LogoutRequestRecord.fromStructure = function (str) {
return new LogoutRequestRecord(new LogoutRequestRecord.RecordClass({
logoutRequestAttr: OS.DataTypes.ImmutableBase.getData(str)
}));
};
LogoutRequestRecord._isAnonymousRecord = true;
LogoutRequestRecord.UniqueId = "ffa30841-ee44-1ce7-5c01-4781d925967c";
LogoutRequestRecord.init();
return LogoutRequestRecord;
})(OS.DataTypes.GenericRecord);
Extension_AuthenticationModel.LogoutRequestRecord = LogoutRequestRecord;

});
define("Extension.Authentication.model$LogoutRequestRecordList", ["exports", "OutSystems/ClientRuntime/Main", "Extension.Authentication.model", "Extension.Authentication.model$LogoutRequestRecord"], function (exports, OutSystems, Extension_AuthenticationModel) {
var OS = OutSystems.Internal;
var LogoutRequestRecordList = (function (_super) {
__extends(LogoutRequestRecordList, _super);
function LogoutRequestRecordList(defaults) {
_super.apply(this, arguments);
}
LogoutRequestRecordList.itemType = Extension_AuthenticationModel.LogoutRequestRecord;
return LogoutRequestRecordList;
})(OS.DataTypes.GenericRecordList);
Extension_AuthenticationModel.LogoutRequestRecordList = LogoutRequestRecordList;

});
define("Extension.Authentication.model$NodeRec", ["exports", "OutSystems/ClientRuntime/Main", "Extension.Authentication.model"], function (exports, OutSystems, Extension_AuthenticationModel) {
var OS = OutSystems.Internal;
var NodeRec = (function (_super) {
__extends(NodeRec, _super);
function NodeRec(defaults) {
_super.apply(this, arguments);
}
NodeRec.attributesToDeclare = function () {
return [
this.attr("Path", "pathAttr", "Path", true, false, OS.Types.Text, function () {
return "";
}, true)
].concat(_super.attributesToDeclare.call(this));
};
NodeRec.fromStructure = function (str) {
return new NodeRec(new NodeRec.RecordClass({
pathAttr: OS.DataTypes.ImmutableBase.getData(str)
}));
};
NodeRec.init();
return NodeRec;
})(OS.DataTypes.GenericRecord);
Extension_AuthenticationModel.NodeRec = NodeRec;

});
define("Extension.Authentication.model$LogoutResponseRecordList", ["exports", "OutSystems/ClientRuntime/Main", "Extension.Authentication.model", "Extension.Authentication.model$LogoutResponseRecord"], function (exports, OutSystems, Extension_AuthenticationModel) {
var OS = OutSystems.Internal;
var LogoutResponseRecordList = (function (_super) {
__extends(LogoutResponseRecordList, _super);
function LogoutResponseRecordList(defaults) {
_super.apply(this, arguments);
}
LogoutResponseRecordList.itemType = Extension_AuthenticationModel.LogoutResponseRecord;
return LogoutResponseRecordList;
})(OS.DataTypes.GenericRecordList);
Extension_AuthenticationModel.LogoutResponseRecordList = LogoutResponseRecordList;

});
define("Extension.Authentication.model$NodeRecord", ["exports", "OutSystems/ClientRuntime/Main", "Extension.Authentication.model", "Extension.Authentication.model$NodeRec"], function (exports, OutSystems, Extension_AuthenticationModel) {
var OS = OutSystems.Internal;
var NodeRecord = (function (_super) {
__extends(NodeRecord, _super);
function NodeRecord(defaults) {
_super.apply(this, arguments);
}
NodeRecord.attributesToDeclare = function () {
return [
this.attr("Node", "nodeAttr", "Node", false, false, OS.Types.Record, function () {
return OS.DataTypes.ImmutableBase.getData(new Extension_AuthenticationModel.NodeRec());
}, true, Extension_AuthenticationModel.NodeRec)
].concat(_super.attributesToDeclare.call(this));
};
NodeRecord.fromStructure = function (str) {
return new NodeRecord(new NodeRecord.RecordClass({
nodeAttr: OS.DataTypes.ImmutableBase.getData(str)
}));
};
NodeRecord._isAnonymousRecord = true;
NodeRecord.UniqueId = "98d9cf11-7a35-73d3-8fae-41966ca62bb9";
NodeRecord.init();
return NodeRecord;
})(OS.DataTypes.GenericRecord);
Extension_AuthenticationModel.NodeRecord = NodeRecord;

});
define("Extension.Authentication.model$NodeRecordList", ["exports", "OutSystems/ClientRuntime/Main", "Extension.Authentication.model", "Extension.Authentication.model$NodeRecord"], function (exports, OutSystems, Extension_AuthenticationModel) {
var OS = OutSystems.Internal;
var NodeRecordList = (function (_super) {
__extends(NodeRecordList, _super);
function NodeRecordList(defaults) {
_super.apply(this, arguments);
}
NodeRecordList.itemType = Extension_AuthenticationModel.NodeRecord;
return NodeRecordList;
})(OS.DataTypes.GenericRecordList);
Extension_AuthenticationModel.NodeRecordList = NodeRecordList;

});
define("Extension.Authentication.model$GroupRec", ["exports", "OutSystems/ClientRuntime/Main", "Extension.Authentication.model"], function (exports, OutSystems, Extension_AuthenticationModel) {
var OS = OutSystems.Internal;
var GroupRec = (function (_super) {
__extends(GroupRec, _super);
function GroupRec(defaults) {
_super.apply(this, arguments);
}
GroupRec.attributesToDeclare = function () {
return [
this.attr("Name", "nameAttr", "Name", true, false, OS.Types.Text, function () {
return "";
}, true)
].concat(_super.attributesToDeclare.call(this));
};
GroupRec.fromStructure = function (str) {
return new GroupRec(new GroupRec.RecordClass({
nameAttr: OS.DataTypes.ImmutableBase.getData(str)
}));
};
GroupRec.init();
return GroupRec;
})(OS.DataTypes.GenericRecord);
Extension_AuthenticationModel.GroupRec = GroupRec;

});
define("Extension.Authentication.model$GroupRecord", ["exports", "OutSystems/ClientRuntime/Main", "Extension.Authentication.model", "Extension.Authentication.model$GroupRec"], function (exports, OutSystems, Extension_AuthenticationModel) {
var OS = OutSystems.Internal;
var GroupRecord = (function (_super) {
__extends(GroupRecord, _super);
function GroupRecord(defaults) {
_super.apply(this, arguments);
}
GroupRecord.attributesToDeclare = function () {
return [
this.attr("Group", "groupAttr", "Group", false, false, OS.Types.Record, function () {
return OS.DataTypes.ImmutableBase.getData(new Extension_AuthenticationModel.GroupRec());
}, true, Extension_AuthenticationModel.GroupRec)
].concat(_super.attributesToDeclare.call(this));
};
GroupRecord.fromStructure = function (str) {
return new GroupRecord(new GroupRecord.RecordClass({
groupAttr: OS.DataTypes.ImmutableBase.getData(str)
}));
};
GroupRecord._isAnonymousRecord = true;
GroupRecord.UniqueId = "57010155-1e66-edd3-beb2-a1c7247ad958";
GroupRecord.init();
return GroupRecord;
})(OS.DataTypes.GenericRecord);
Extension_AuthenticationModel.GroupRecord = GroupRecord;

});
define("Extension.Authentication.model$ContactPersonRecordList", ["exports", "OutSystems/ClientRuntime/Main", "Extension.Authentication.model", "Extension.Authentication.model$ContactPersonRecord"], function (exports, OutSystems, Extension_AuthenticationModel) {
var OS = OutSystems.Internal;
var ContactPersonRecordList = (function (_super) {
__extends(ContactPersonRecordList, _super);
function ContactPersonRecordList(defaults) {
_super.apply(this, arguments);
}
ContactPersonRecordList.itemType = Extension_AuthenticationModel.ContactPersonRecord;
return ContactPersonRecordList;
})(OS.DataTypes.GenericRecordList);
Extension_AuthenticationModel.ContactPersonRecordList = ContactPersonRecordList;

});
define("Extension.Authentication.model$ValidationRec", ["exports", "OutSystems/ClientRuntime/Main", "Extension.Authentication.model"], function (exports, OutSystems, Extension_AuthenticationModel) {
var OS = OutSystems.Internal;
var ValidationRec = (function (_super) {
__extends(ValidationRec, _super);
function ValidationRec(defaults) {
_super.apply(this, arguments);
}
ValidationRec.attributesToDeclare = function () {
return [
this.attr("ResponseId", "responseIdAttr", "ResponseId", true, false, OS.Types.Text, function () {
return "";
}, true), 
this.attr("IsValidSignature", "isValidSignatureAttr", "IsValidSignature", true, false, OS.Types.Boolean, function () {
return false;
}, true), 
this.attr("IsValidCertificate", "isValidCertificateAttr", "IsValidCertificate", true, false, OS.Types.Boolean, function () {
return false;
}, true), 
this.attr("IsValidTimestamp", "isValidTimestampAttr", "IsValidTimestamp", true, false, OS.Types.Boolean, function () {
return false;
}, true), 
this.attr("isValidStatusCode", "isValidStatusCodeAttr", "isValidStatusCode", true, false, OS.Types.Boolean, function () {
return false;
}, true), 
this.attr("IsValidIssuer", "isValidIssuerAttr", "IsValidIssuer", true, false, OS.Types.Boolean, function () {
return false;
}, true), 
this.attr("StatusCode", "statusCodeAttr", "StatusCode", true, false, OS.Types.Text, function () {
return "";
}, true), 
this.attr("Issuer", "issuerAttr", "Issuer", true, false, OS.Types.Text, function () {
return "";
}, true), 
this.attr("NotBefore", "notBeforeAttr", "NotBefore", true, false, OS.Types.DateTime, function () {
return OS.DataTypes.DateTime.defaultValue;
}, true), 
this.attr("NotOnOrAfter", "notOnOrAfterAttr", "NotOnOrAfter", true, false, OS.Types.DateTime, function () {
return OS.DataTypes.DateTime.defaultValue;
}, true)
].concat(_super.attributesToDeclare.call(this));
};
ValidationRec.init();
return ValidationRec;
})(OS.DataTypes.GenericRecord);
Extension_AuthenticationModel.ValidationRec = ValidationRec;

});
define("Extension.Authentication.model$PropertyRecordList", ["exports", "OutSystems/ClientRuntime/Main", "Extension.Authentication.model", "Extension.Authentication.model$PropertyRecord"], function (exports, OutSystems, Extension_AuthenticationModel) {
var OS = OutSystems.Internal;
var PropertyRecordList = (function (_super) {
__extends(PropertyRecordList, _super);
function PropertyRecordList(defaults) {
_super.apply(this, arguments);
}
PropertyRecordList.itemType = Extension_AuthenticationModel.PropertyRecord;
return PropertyRecordList;
})(OS.DataTypes.GenericRecordList);
Extension_AuthenticationModel.PropertyRecordList = PropertyRecordList;

});
define("Extension.Authentication.model$GroupRecordList", ["exports", "OutSystems/ClientRuntime/Main", "Extension.Authentication.model", "Extension.Authentication.model$GroupRecord"], function (exports, OutSystems, Extension_AuthenticationModel) {
var OS = OutSystems.Internal;
var GroupRecordList = (function (_super) {
__extends(GroupRecordList, _super);
function GroupRecordList(defaults) {
_super.apply(this, arguments);
}
GroupRecordList.itemType = Extension_AuthenticationModel.GroupRecord;
return GroupRecordList;
})(OS.DataTypes.GenericRecordList);
Extension_AuthenticationModel.GroupRecordList = GroupRecordList;

});
define("Extension.Authentication.model$ValidationRecord", ["exports", "OutSystems/ClientRuntime/Main", "Extension.Authentication.model", "Extension.Authentication.model$ValidationRec"], function (exports, OutSystems, Extension_AuthenticationModel) {
var OS = OutSystems.Internal;
var ValidationRecord = (function (_super) {
__extends(ValidationRecord, _super);
function ValidationRecord(defaults) {
_super.apply(this, arguments);
}
ValidationRecord.attributesToDeclare = function () {
return [
this.attr("Validation", "validationAttr", "Validation", false, false, OS.Types.Record, function () {
return OS.DataTypes.ImmutableBase.getData(new Extension_AuthenticationModel.ValidationRec());
}, true, Extension_AuthenticationModel.ValidationRec)
].concat(_super.attributesToDeclare.call(this));
};
ValidationRecord.fromStructure = function (str) {
return new ValidationRecord(new ValidationRecord.RecordClass({
validationAttr: OS.DataTypes.ImmutableBase.getData(str)
}));
};
ValidationRecord._isAnonymousRecord = true;
ValidationRecord.UniqueId = "ce236c29-218e-9220-a147-4ceb89717a1d";
ValidationRecord.init();
return ValidationRecord;
})(OS.DataTypes.GenericRecord);
Extension_AuthenticationModel.ValidationRecord = ValidationRecord;

});
define("Extension.Authentication.model$ValidationRecordList", ["exports", "OutSystems/ClientRuntime/Main", "Extension.Authentication.model", "Extension.Authentication.model$ValidationRecord"], function (exports, OutSystems, Extension_AuthenticationModel) {
var OS = OutSystems.Internal;
var ValidationRecordList = (function (_super) {
__extends(ValidationRecordList, _super);
function ValidationRecordList(defaults) {
_super.apply(this, arguments);
}
ValidationRecordList.itemType = Extension_AuthenticationModel.ValidationRecord;
return ValidationRecordList;
})(OS.DataTypes.GenericRecordList);
Extension_AuthenticationModel.ValidationRecordList = ValidationRecordList;

});
define("Extension.Authentication.model", ["exports", "OutSystems/ClientRuntime/Main"], function (exports, OutSystems) {
var OS = OutSystems.Internal;
var Extension_AuthenticationModel = exports;
});
