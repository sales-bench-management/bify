﻿define("ProgressCircle.model$MenuSubItemRec", ["exports", "OutSystems/ClientRuntime/Main", "ProgressCircle.model"], function (exports, OutSystems, ProgressCircleModel) {
var OS = OutSystems.Internal;
var MenuSubItemRec = (function (_super) {
__extends(MenuSubItemRec, _super);
function MenuSubItemRec(defaults) {
_super.apply(this, arguments);
}
MenuSubItemRec.attributesToDeclare = function () {
return [
this.attr("Id", "idAttr", "Id", true, false, OS.Types.Integer, function () {
return 0;
}, true), 
this.attr("Order", "orderAttr", "Order", true, false, OS.Types.Integer, function () {
return 0;
}, true), 
this.attr("Caption", "captionAttr", "Caption", true, false, OS.Types.Text, function () {
return "";
}, true), 
this.attr("MenuItemId", "menuItemIdAttr", "MenuItemId", false, false, OS.Types.Integer, function () {
return 0;
}, true)
].concat(_super.attributesToDeclare.call(this));
};
MenuSubItemRec.init();
return MenuSubItemRec;
})(OS.DataTypes.GenericRecord);
ProgressCircleModel.MenuSubItemRec = MenuSubItemRec;

});
define("ProgressCircle.model$MenuItemRec", ["exports", "OutSystems/ClientRuntime/Main", "ProgressCircle.model"], function (exports, OutSystems, ProgressCircleModel) {
var OS = OutSystems.Internal;
var MenuItemRec = (function (_super) {
__extends(MenuItemRec, _super);
function MenuItemRec(defaults) {
_super.apply(this, arguments);
}
MenuItemRec.attributesToDeclare = function () {
return [
this.attr("Id", "idAttr", "Id", true, false, OS.Types.Integer, function () {
return 0;
}, true), 
this.attr("Order", "orderAttr", "Order", true, false, OS.Types.Integer, function () {
return 0;
}, true), 
this.attr("Caption", "captionAttr", "Caption", true, false, OS.Types.Text, function () {
return "";
}, true)
].concat(_super.attributesToDeclare.call(this));
};
MenuItemRec.init();
return MenuItemRec;
})(OS.DataTypes.GenericRecord);
ProgressCircleModel.MenuItemRec = MenuItemRec;

});
define("ProgressCircle.model", ["exports", "OutSystems/ClientRuntime/Main"], function (exports, OutSystems) {
var OS = OutSystems.Internal;
var ProgressCircleModel = exports;
});
