﻿define("RichWidgets.model$IconSizeRec", ["exports", "OutSystems/ClientRuntime/Main", "RichWidgets.model"], function (exports, OutSystems, RichWidgetsModel) {
var OS = OutSystems.Internal;
var IconSizeRec = (function (_super) {
__extends(IconSizeRec, _super);
function IconSizeRec(defaults) {
_super.apply(this, arguments);
}
IconSizeRec.attributesToDeclare = function () {
return [
this.attr("Class", "classAttr", "Class", true, false, OS.Types.Text, function () {
return "";
}, true)
].concat(_super.attributesToDeclare.call(this));
};
IconSizeRec.fromStructure = function (str) {
return new IconSizeRec(new IconSizeRec.RecordClass({
classAttr: OS.DataTypes.ImmutableBase.getData(str)
}));
};
IconSizeRec.init();
return IconSizeRec;
})(OS.DataTypes.GenericRecord);
RichWidgetsModel.IconSizeRec = IconSizeRec;

});
define("RichWidgets.model$Input_AutoComplete_ListEntryRec", ["exports", "OutSystems/ClientRuntime/Main", "RichWidgets.model"], function (exports, OutSystems, RichWidgetsModel) {
var OS = OutSystems.Internal;
var Input_AutoComplete_ListEntryRec = (function (_super) {
__extends(Input_AutoComplete_ListEntryRec, _super);
function Input_AutoComplete_ListEntryRec(defaults) {
_super.apply(this, arguments);
}
Input_AutoComplete_ListEntryRec.attributesToDeclare = function () {
return [
this.attr("Label", "labelAttr", "Label", false, false, OS.Types.Text, function () {
return "";
}, true), 
this.attr("Identifier", "identifierAttr", "Identifier", false, false, OS.Types.Text, function () {
return "";
}, true)
].concat(_super.attributesToDeclare.call(this));
};
Input_AutoComplete_ListEntryRec.init();
return Input_AutoComplete_ListEntryRec;
})(OS.DataTypes.GenericRecord);
RichWidgetsModel.Input_AutoComplete_ListEntryRec = Input_AutoComplete_ListEntryRec;

});
define("RichWidgets.model$UploadedFileRec", ["exports", "OutSystems/ClientRuntime/Main", "RichWidgets.model"], function (exports, OutSystems, RichWidgetsModel) {
var OS = OutSystems.Internal;
var UploadedFileRec = (function (_super) {
__extends(UploadedFileRec, _super);
function UploadedFileRec(defaults) {
_super.apply(this, arguments);
}
UploadedFileRec.attributesToDeclare = function () {
return [
this.attr("Content", "contentAttr", "Content", true, false, OS.Types.BinaryData, function () {
return OS.DataTypes.BinaryData.defaultValue;
}, true), 
this.attr("Filename", "filenameAttr", "Filename", true, false, OS.Types.Text, function () {
return "";
}, true), 
this.attr("Filetype", "filetypeAttr", "Filetype", true, false, OS.Types.Text, function () {
return "";
}, true)
].concat(_super.attributesToDeclare.call(this));
};
UploadedFileRec.init();
return UploadedFileRec;
})(OS.DataTypes.GenericRecord);
RichWidgetsModel.UploadedFileRec = UploadedFileRec;

});
define("RichWidgets.model$BooleanRec", ["exports", "OutSystems/ClientRuntime/Main", "RichWidgets.model"], function (exports, OutSystems, RichWidgetsModel) {
var OS = OutSystems.Internal;
var BooleanRec = (function (_super) {
__extends(BooleanRec, _super);
function BooleanRec(defaults) {
_super.apply(this, arguments);
}
BooleanRec.attributesToDeclare = function () {
return [
this.attr("Value", "valueAttr", "Value", false, false, OS.Types.Boolean, function () {
return false;
}, true)
].concat(_super.attributesToDeclare.call(this));
};
BooleanRec.fromStructure = function (str) {
return new BooleanRec(new BooleanRec.RecordClass({
valueAttr: OS.DataTypes.ImmutableBase.getData(str)
}));
};
BooleanRec.init();
return BooleanRec;
})(OS.DataTypes.GenericRecord);
RichWidgetsModel.BooleanRec = BooleanRec;

});
define("RichWidgets.model$MessageTypeRec", ["exports", "OutSystems/ClientRuntime/Main", "RichWidgets.model"], function (exports, OutSystems, RichWidgetsModel) {
var OS = OutSystems.Internal;
var MessageTypeRec = (function (_super) {
__extends(MessageTypeRec, _super);
function MessageTypeRec(defaults) {
_super.apply(this, arguments);
}
MessageTypeRec.attributesToDeclare = function () {
return [
this.attr("Id", "idAttr", "Id", true, false, OS.Types.Integer, function () {
return 0;
}, true), 
this.attr("Label", "labelAttr", "Label", true, false, OS.Types.Text, function () {
return "";
}, true)
].concat(_super.attributesToDeclare.call(this));
};
MessageTypeRec.init();
return MessageTypeRec;
})(OS.DataTypes.GenericRecord);
RichWidgetsModel.MessageTypeRec = MessageTypeRec;

});
define("RichWidgets.model$IconNameRec", ["exports", "OutSystems/ClientRuntime/Main", "RichWidgets.model"], function (exports, OutSystems, RichWidgetsModel) {
var OS = OutSystems.Internal;
var IconNameRec = (function (_super) {
__extends(IconNameRec, _super);
function IconNameRec(defaults) {
_super.apply(this, arguments);
}
IconNameRec.attributesToDeclare = function () {
return [
this.attr("Class", "classAttr", "Class", true, false, OS.Types.Text, function () {
return "";
}, true)
].concat(_super.attributesToDeclare.call(this));
};
IconNameRec.fromStructure = function (str) {
return new IconNameRec(new IconNameRec.RecordClass({
classAttr: OS.DataTypes.ImmutableBase.getData(str)
}));
};
IconNameRec.init();
return IconNameRec;
})(OS.DataTypes.GenericRecord);
RichWidgetsModel.IconNameRec = IconNameRec;

});
define("RichWidgets.model", ["exports", "OutSystems/ClientRuntime/Main"], function (exports, OutSystems) {
var OS = OutSystems.Internal;
var RichWidgetsModel = exports;
});
