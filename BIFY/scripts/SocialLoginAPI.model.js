﻿define("SocialLoginAPI.model$GoogleScopeRec", ["exports", "OutSystems/ClientRuntime/Main", "SocialLoginAPI.model"], function (exports, OutSystems, SocialLoginAPIModel) {
var OS = OutSystems.Internal;
var GoogleScopeRec = (function (_super) {
__extends(GoogleScopeRec, _super);
function GoogleScopeRec(defaults) {
_super.apply(this, arguments);
}
GoogleScopeRec.attributesToDeclare = function () {
return [
this.attr("Id", "idAttr", "Id", true, false, OS.Types.Integer, function () {
return 0;
}, true), 
this.attr("Label", "labelAttr", "Label", true, false, OS.Types.Text, function () {
return "";
}, true), 
this.attr("Value", "valueAttr", "Value", false, false, OS.Types.Text, function () {
return "";
}, true), 
this.attr("Order", "orderAttr", "Order", true, false, OS.Types.Integer, function () {
return 0;
}, true), 
this.attr("Is_Active", "is_ActiveAttr", "Is_Active", true, false, OS.Types.Boolean, function () {
return false;
}, true)
].concat(_super.attributesToDeclare.call(this));
};
GoogleScopeRec.init();
return GoogleScopeRec;
})(OS.DataTypes.GenericRecord);
SocialLoginAPIModel.GoogleScopeRec = GoogleScopeRec;

});
define("SocialLoginAPI.model$InstagramScopeRec", ["exports", "OutSystems/ClientRuntime/Main", "SocialLoginAPI.model"], function (exports, OutSystems, SocialLoginAPIModel) {
var OS = OutSystems.Internal;
var InstagramScopeRec = (function (_super) {
__extends(InstagramScopeRec, _super);
function InstagramScopeRec(defaults) {
_super.apply(this, arguments);
}
InstagramScopeRec.attributesToDeclare = function () {
return [
this.attr("Id", "idAttr", "Id", true, false, OS.Types.Integer, function () {
return 0;
}, true), 
this.attr("Label", "labelAttr", "Label", true, false, OS.Types.Text, function () {
return "";
}, true), 
this.attr("Value", "valueAttr", "Value", false, false, OS.Types.Text, function () {
return "";
}, true), 
this.attr("Order", "orderAttr", "Order", true, false, OS.Types.Integer, function () {
return 0;
}, true), 
this.attr("Is_Active", "is_ActiveAttr", "Is_Active", true, false, OS.Types.Boolean, function () {
return false;
}, true)
].concat(_super.attributesToDeclare.call(this));
};
InstagramScopeRec.init();
return InstagramScopeRec;
})(OS.DataTypes.GenericRecord);
SocialLoginAPIModel.InstagramScopeRec = InstagramScopeRec;

});
define("SocialLoginAPI.model$SocialLoginUserRec", ["exports", "OutSystems/ClientRuntime/Main", "SocialLoginAPI.model"], function (exports, OutSystems, SocialLoginAPIModel) {
var OS = OutSystems.Internal;
var SocialLoginUserRec = (function (_super) {
__extends(SocialLoginUserRec, _super);
function SocialLoginUserRec(defaults) {
_super.apply(this, arguments);
}
SocialLoginUserRec.attributesToDeclare = function () {
return [
this.attr("Id", "idAttr", "Id", false, false, OS.Types.Text, function () {
return "";
}, true), 
this.attr("Name", "nameAttr", "Name", false, false, OS.Types.Text, function () {
return "";
}, true), 
this.attr("Email", "emailAttr", "Email", false, false, OS.Types.Text, function () {
return "";
}, true), 
this.attr("HasErrors", "hasErrorsAttr", "HasErrors", false, false, OS.Types.Boolean, function () {
return false;
}, true), 
this.attr("message", "messageAttr", "message", false, false, OS.Types.Text, function () {
return "";
}, true), 
this.attr("error", "errorAttr", "error", false, false, OS.Types.Text, function () {
return "";
}, true), 
this.attr("error_message", "error_messageAttr", "error_message", false, false, OS.Types.Text, function () {
return "";
}, true), 
this.attr("error_reason", "error_reasonAttr", "error_reason", false, false, OS.Types.Text, function () {
return "";
}, true), 
this.attr("error_code", "error_codeAttr", "error_code", false, false, OS.Types.Text, function () {
return "";
}, true), 
this.attr("error_description", "error_descriptionAttr", "error_description", false, false, OS.Types.Text, function () {
return "";
}, true), 
this.attr("error_uri", "error_uriAttr", "error_uri", false, false, OS.Types.Text, function () {
return "";
}, true), 
this.attr("documentation_url", "documentation_urlAttr", "documentation_url", false, false, OS.Types.Text, function () {
return "";
}, true)
].concat(_super.attributesToDeclare.call(this));
};
SocialLoginUserRec.init();
return SocialLoginUserRec;
})(OS.DataTypes.GenericRecord);
SocialLoginAPIModel.SocialLoginUserRec = SocialLoginUserRec;

});
define("SocialLoginAPI.model$GitHubScopeRec", ["exports", "OutSystems/ClientRuntime/Main", "SocialLoginAPI.model"], function (exports, OutSystems, SocialLoginAPIModel) {
var OS = OutSystems.Internal;
var GitHubScopeRec = (function (_super) {
__extends(GitHubScopeRec, _super);
function GitHubScopeRec(defaults) {
_super.apply(this, arguments);
}
GitHubScopeRec.attributesToDeclare = function () {
return [
this.attr("Id", "idAttr", "Id", true, false, OS.Types.Integer, function () {
return 0;
}, true), 
this.attr("Label", "labelAttr", "Label", true, false, OS.Types.Text, function () {
return "";
}, true), 
this.attr("Value", "valueAttr", "Value", false, false, OS.Types.Text, function () {
return "";
}, true), 
this.attr("Order", "orderAttr", "Order", true, false, OS.Types.Integer, function () {
return 0;
}, true), 
this.attr("Is_Active", "is_ActiveAttr", "Is_Active", true, false, OS.Types.Boolean, function () {
return false;
}, true)
].concat(_super.attributesToDeclare.call(this));
};
GitHubScopeRec.init();
return GitHubScopeRec;
})(OS.DataTypes.GenericRecord);
SocialLoginAPIModel.GitHubScopeRec = GitHubScopeRec;

});
define("SocialLoginAPI.model$FacebookScopeRec", ["exports", "OutSystems/ClientRuntime/Main", "SocialLoginAPI.model"], function (exports, OutSystems, SocialLoginAPIModel) {
var OS = OutSystems.Internal;
var FacebookScopeRec = (function (_super) {
__extends(FacebookScopeRec, _super);
function FacebookScopeRec(defaults) {
_super.apply(this, arguments);
}
FacebookScopeRec.attributesToDeclare = function () {
return [
this.attr("Id", "idAttr", "Id", true, false, OS.Types.Integer, function () {
return 0;
}, true), 
this.attr("Label", "labelAttr", "Label", true, false, OS.Types.Text, function () {
return "";
}, true), 
this.attr("Value", "valueAttr", "Value", false, false, OS.Types.Text, function () {
return "";
}, true), 
this.attr("Order", "orderAttr", "Order", true, false, OS.Types.Integer, function () {
return 0;
}, true), 
this.attr("Is_Active", "is_ActiveAttr", "Is_Active", true, false, OS.Types.Boolean, function () {
return false;
}, true)
].concat(_super.attributesToDeclare.call(this));
};
FacebookScopeRec.init();
return FacebookScopeRec;
})(OS.DataTypes.GenericRecord);
SocialLoginAPIModel.FacebookScopeRec = FacebookScopeRec;

});
define("SocialLoginAPI.model$LinkedinScopeRec", ["exports", "OutSystems/ClientRuntime/Main", "SocialLoginAPI.model"], function (exports, OutSystems, SocialLoginAPIModel) {
var OS = OutSystems.Internal;
var LinkedinScopeRec = (function (_super) {
__extends(LinkedinScopeRec, _super);
function LinkedinScopeRec(defaults) {
_super.apply(this, arguments);
}
LinkedinScopeRec.attributesToDeclare = function () {
return [
this.attr("Id", "idAttr", "Id", true, false, OS.Types.Integer, function () {
return 0;
}, true), 
this.attr("Label", "labelAttr", "Label", true, false, OS.Types.Text, function () {
return "";
}, true), 
this.attr("Value", "valueAttr", "Value", false, false, OS.Types.Text, function () {
return "";
}, true), 
this.attr("Order", "orderAttr", "Order", true, false, OS.Types.Integer, function () {
return 0;
}, true), 
this.attr("Is_Active", "is_ActiveAttr", "Is_Active", true, false, OS.Types.Boolean, function () {
return false;
}, true)
].concat(_super.attributesToDeclare.call(this));
};
LinkedinScopeRec.init();
return LinkedinScopeRec;
})(OS.DataTypes.GenericRecord);
SocialLoginAPIModel.LinkedinScopeRec = LinkedinScopeRec;

});
define("SocialLoginAPI.model$AuthProviderRec", ["exports", "OutSystems/ClientRuntime/Main", "SocialLoginAPI.model"], function (exports, OutSystems, SocialLoginAPIModel) {
var OS = OutSystems.Internal;
var AuthProviderRec = (function (_super) {
__extends(AuthProviderRec, _super);
function AuthProviderRec(defaults) {
_super.apply(this, arguments);
}
AuthProviderRec.attributesToDeclare = function () {
return [
this.attr("Id", "idAttr", "Id", true, false, OS.Types.Integer, function () {
return 0;
}, true), 
this.attr("Label", "labelAttr", "Label", true, false, OS.Types.Text, function () {
return "";
}, true), 
this.attr("Order", "orderAttr", "Order", true, false, OS.Types.Integer, function () {
return 0;
}, true), 
this.attr("Is_Active", "is_ActiveAttr", "Is_Active", true, false, OS.Types.Boolean, function () {
return false;
}, true)
].concat(_super.attributesToDeclare.call(this));
};
AuthProviderRec.init();
return AuthProviderRec;
})(OS.DataTypes.GenericRecord);
SocialLoginAPIModel.AuthProviderRec = AuthProviderRec;

});
define("SocialLoginAPI.model", ["exports", "OutSystems/ClientRuntime/Main"], function (exports, OutSystems) {
var OS = OutSystems.Internal;
var SocialLoginAPIModel = exports;
});
