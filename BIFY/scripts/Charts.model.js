﻿define("Charts.model$ChartFormatRec", ["exports", "OutSystems/ClientRuntime/Main", "Charts.model"], function (exports, OutSystems, ChartsModel) {
var OS = OutSystems.Internal;
var ChartFormatRec = (function (_super) {
__extends(ChartFormatRec, _super);
function ChartFormatRec(defaults) {
_super.apply(this, arguments);
}
ChartFormatRec.attributesToDeclare = function () {
return [
this.attr("ShowDataPointValues", "showDataPointValuesAttr", "ShowDataPointValues", false, false, OS.Types.Boolean, function () {
return false;
}, true), 
this.attr("UseAnimation", "useAnimationAttr", "UseAnimation", false, false, OS.Types.Boolean, function () {
return false;
}, true)
].concat(_super.attributesToDeclare.call(this));
};
ChartFormatRec.init();
return ChartFormatRec;
})(OS.DataTypes.GenericRecord);
ChartsModel.ChartFormatRec = ChartFormatRec;

});
define("Charts.model$XAxisValuesTypeRec", ["exports", "OutSystems/ClientRuntime/Main", "Charts.model"], function (exports, OutSystems, ChartsModel) {
var OS = OutSystems.Internal;
var XAxisValuesTypeRec = (function (_super) {
__extends(XAxisValuesTypeRec, _super);
function XAxisValuesTypeRec(defaults) {
_super.apply(this, arguments);
}
XAxisValuesTypeRec.attributesToDeclare = function () {
return [
this.attr("Id", "idAttr", "Id", true, false, OS.Types.Integer, function () {
return 0;
}, true), 
this.attr("Label", "labelAttr", "Label", true, false, OS.Types.Text, function () {
return "";
}, true), 
this.attr("Order", "orderAttr", "Order", false, false, OS.Types.Integer, function () {
return 0;
}, true)
].concat(_super.attributesToDeclare.call(this));
};
XAxisValuesTypeRec.init();
return XAxisValuesTypeRec;
})(OS.DataTypes.GenericRecord);
ChartsModel.XAxisValuesTypeRec = XAxisValuesTypeRec;

});
define("Charts.model$DataPointRec", ["exports", "OutSystems/ClientRuntime/Main", "Charts.model"], function (exports, OutSystems, ChartsModel) {
var OS = OutSystems.Internal;
var DataPointRec = (function (_super) {
__extends(DataPointRec, _super);
function DataPointRec(defaults) {
_super.apply(this, arguments);
}
DataPointRec.attributesToDeclare = function () {
return [
this.attr("Label", "labelAttr", "Label", true, false, OS.Types.Text, function () {
return "";
}, true), 
this.attr("Value", "valueAttr", "Value", true, false, OS.Types.Decimal, function () {
return OS.DataTypes.Decimal.defaultValue;
}, true), 
this.attr("DataSeriesName", "dataSeriesNameAttr", "DataSeriesName", false, false, OS.Types.Text, function () {
return "";
}, true), 
this.attr("Tooltip", "tooltipAttr", "Tooltip", false, false, OS.Types.Text, function () {
return "";
}, true), 
this.attr("Color", "colorAttr", "Color", false, false, OS.Types.Text, function () {
return "";
}, true)
].concat(_super.attributesToDeclare.call(this));
};
DataPointRec.init();
return DataPointRec;
})(OS.DataTypes.GenericRecord);
ChartsModel.DataPointRec = DataPointRec;

});
define("Charts.model$DataPointRecord", ["exports", "OutSystems/ClientRuntime/Main", "Charts.model", "Charts.model$DataPointRec"], function (exports, OutSystems, ChartsModel) {
var OS = OutSystems.Internal;
var DataPointRecord = (function (_super) {
__extends(DataPointRecord, _super);
function DataPointRecord(defaults) {
_super.apply(this, arguments);
}
DataPointRecord.attributesToDeclare = function () {
return [
this.attr("DataPoint", "dataPointAttr", "DataPoint", false, false, OS.Types.Record, function () {
return OS.DataTypes.ImmutableBase.getData(new ChartsModel.DataPointRec());
}, true, ChartsModel.DataPointRec)
].concat(_super.attributesToDeclare.call(this));
};
DataPointRecord.fromStructure = function (str) {
return new DataPointRecord(new DataPointRecord.RecordClass({
dataPointAttr: OS.DataTypes.ImmutableBase.getData(str)
}));
};
DataPointRecord._isAnonymousRecord = true;
DataPointRecord.UniqueId = "5c998150-f80a-9320-b45b-51a11cba92ed";
DataPointRecord.init();
return DataPointRecord;
})(OS.DataTypes.GenericRecord);
ChartsModel.DataPointRecord = DataPointRecord;

});
define("Charts.model$AdvancedDataPointFormatRec", ["exports", "OutSystems/ClientRuntime/Main", "Charts.model", "Charts.model$DataPointRecord"], function (exports, OutSystems, ChartsModel) {
var OS = OutSystems.Internal;
var AdvancedDataPointFormatRec = (function (_super) {
__extends(AdvancedDataPointFormatRec, _super);
function AdvancedDataPointFormatRec(defaults) {
_super.apply(this, arguments);
}
AdvancedDataPointFormatRec.attributesToDeclare = function () {
return [
this.attr("DataPoint", "dataPointAttr", "DataPoint", false, false, OS.Types.Record, function () {
return OS.DataTypes.ImmutableBase.getData(new ChartsModel.DataPointRecord());
}, true, ChartsModel.DataPointRecord), 
this.attr("DataPointJSON", "dataPointJSONAttr", "DataPointJSON", false, false, OS.Types.Text, function () {
return "";
}, true)
].concat(_super.attributesToDeclare.call(this));
};
AdvancedDataPointFormatRec.init();
return AdvancedDataPointFormatRec;
})(OS.DataTypes.GenericRecord);
ChartsModel.AdvancedDataPointFormatRec = AdvancedDataPointFormatRec;

});
define("Charts.model$YAxisFormatRec", ["exports", "OutSystems/ClientRuntime/Main", "Charts.model"], function (exports, OutSystems, ChartsModel) {
var OS = OutSystems.Internal;
var YAxisFormatRec = (function (_super) {
__extends(YAxisFormatRec, _super);
function YAxisFormatRec(defaults) {
_super.apply(this, arguments);
}
YAxisFormatRec.attributesToDeclare = function () {
return [
this.attr("Title", "titleAttr", "Title", false, false, OS.Types.Text, function () {
return "";
}, true), 
this.attr("MinValue", "minValueAttr", "MinValue", false, false, OS.Types.Decimal, function () {
return OS.BuiltinFunctions.integerToDecimal(-2147483647);
}, true), 
this.attr("MaxValue", "maxValueAttr", "MaxValue", false, false, OS.Types.Decimal, function () {
return OS.BuiltinFunctions.integerToDecimal(-2147483647);
}, true), 
this.attr("ValuesPrefix", "valuesPrefixAttr", "ValuesPrefix", false, false, OS.Types.Text, function () {
return "";
}, true), 
this.attr("ValuesSuffix", "valuesSuffixAttr", "ValuesSuffix", false, false, OS.Types.Text, function () {
return "";
}, true), 
this.attr("GridLineStep", "gridLineStepAttr", "GridLineStep", false, false, OS.Types.Decimal, function () {
return OS.BuiltinFunctions.integerToDecimal(-2147483647);
}, true)
].concat(_super.attributesToDeclare.call(this));
};
YAxisFormatRec.init();
return YAxisFormatRec;
})(OS.DataTypes.GenericRecord);
ChartsModel.YAxisFormatRec = YAxisFormatRec;

});
define("Charts.model$StackingTypeRec", ["exports", "OutSystems/ClientRuntime/Main", "Charts.model"], function (exports, OutSystems, ChartsModel) {
var OS = OutSystems.Internal;
var StackingTypeRec = (function (_super) {
__extends(StackingTypeRec, _super);
function StackingTypeRec(defaults) {
_super.apply(this, arguments);
}
StackingTypeRec.attributesToDeclare = function () {
return [
this.attr("Id", "idAttr", "Id", true, false, OS.Types.Integer, function () {
return 0;
}, true), 
this.attr("Order", "orderAttr", "Order", true, false, OS.Types.Integer, function () {
return 0;
}, true), 
this.attr("Is_Active", "is_ActiveAttr", "Is_Active", true, false, OS.Types.Boolean, function () {
return false;
}, true)
].concat(_super.attributesToDeclare.call(this));
};
StackingTypeRec.init();
return StackingTypeRec;
})(OS.DataTypes.GenericRecord);
ChartsModel.StackingTypeRec = StackingTypeRec;

});
define("Charts.model$LegendPositionRec", ["exports", "OutSystems/ClientRuntime/Main", "Charts.model"], function (exports, OutSystems, ChartsModel) {
var OS = OutSystems.Internal;
var LegendPositionRec = (function (_super) {
__extends(LegendPositionRec, _super);
function LegendPositionRec(defaults) {
_super.apply(this, arguments);
}
LegendPositionRec.attributesToDeclare = function () {
return [
this.attr("Id", "idAttr", "Id", true, false, OS.Types.Integer, function () {
return 0;
}, true), 
this.attr("Order", "orderAttr", "Order", true, false, OS.Types.Integer, function () {
return 0;
}, true), 
this.attr("Is_Active", "is_ActiveAttr", "Is_Active", true, false, OS.Types.Boolean, function () {
return false;
}, true)
].concat(_super.attributesToDeclare.call(this));
};
LegendPositionRec.init();
return LegendPositionRec;
})(OS.DataTypes.GenericRecord);
ChartsModel.LegendPositionRec = LegendPositionRec;

});
define("Charts.model$AdvancedDataPointFormatRecord", ["exports", "OutSystems/ClientRuntime/Main", "Charts.model", "Charts.model$AdvancedDataPointFormatRec"], function (exports, OutSystems, ChartsModel) {
var OS = OutSystems.Internal;
var AdvancedDataPointFormatRecord = (function (_super) {
__extends(AdvancedDataPointFormatRecord, _super);
function AdvancedDataPointFormatRecord(defaults) {
_super.apply(this, arguments);
}
AdvancedDataPointFormatRecord.attributesToDeclare = function () {
return [
this.attr("AdvancedDataPointFormat", "advancedDataPointFormatAttr", "AdvancedDataPointFormat", false, false, OS.Types.Record, function () {
return OS.DataTypes.ImmutableBase.getData(new ChartsModel.AdvancedDataPointFormatRec());
}, true, ChartsModel.AdvancedDataPointFormatRec)
].concat(_super.attributesToDeclare.call(this));
};
AdvancedDataPointFormatRecord.fromStructure = function (str) {
return new AdvancedDataPointFormatRecord(new AdvancedDataPointFormatRecord.RecordClass({
advancedDataPointFormatAttr: OS.DataTypes.ImmutableBase.getData(str)
}));
};
AdvancedDataPointFormatRecord._isAnonymousRecord = true;
AdvancedDataPointFormatRecord.UniqueId = "9377da45-6962-44c4-a935-6591b226316d";
AdvancedDataPointFormatRecord.init();
return AdvancedDataPointFormatRecord;
})(OS.DataTypes.GenericRecord);
ChartsModel.AdvancedDataPointFormatRecord = AdvancedDataPointFormatRecord;

});
define("Charts.model$AdvancedDataPointFormatRecordList", ["exports", "OutSystems/ClientRuntime/Main", "Charts.model", "Charts.model$AdvancedDataPointFormatRecord"], function (exports, OutSystems, ChartsModel) {
var OS = OutSystems.Internal;
var AdvancedDataPointFormatRecordList = (function (_super) {
__extends(AdvancedDataPointFormatRecordList, _super);
function AdvancedDataPointFormatRecordList(defaults) {
_super.apply(this, arguments);
}
AdvancedDataPointFormatRecordList.itemType = ChartsModel.AdvancedDataPointFormatRecord;
return AdvancedDataPointFormatRecordList;
})(OS.DataTypes.GenericRecordList);
ChartsModel.AdvancedDataPointFormatRecordList = AdvancedDataPointFormatRecordList;

});
define("Charts.model$AdvancedDataSeriesFormatRec", ["exports", "OutSystems/ClientRuntime/Main", "Charts.model"], function (exports, OutSystems, ChartsModel) {
var OS = OutSystems.Internal;
var AdvancedDataSeriesFormatRec = (function (_super) {
__extends(AdvancedDataSeriesFormatRec, _super);
function AdvancedDataSeriesFormatRec(defaults) {
_super.apply(this, arguments);
}
AdvancedDataSeriesFormatRec.attributesToDeclare = function () {
return [
this.attr("DataSeriesName", "dataSeriesNameAttr", "DataSeriesName", false, false, OS.Types.Text, function () {
return "";
}, true), 
this.attr("DataSeriesJSON", "dataSeriesJSONAttr", "DataSeriesJSON", false, false, OS.Types.Text, function () {
return "";
}, true)
].concat(_super.attributesToDeclare.call(this));
};
AdvancedDataSeriesFormatRec.init();
return AdvancedDataSeriesFormatRec;
})(OS.DataTypes.GenericRecord);
ChartsModel.AdvancedDataSeriesFormatRec = AdvancedDataSeriesFormatRec;

});
define("Charts.model$AdvancedDataSeriesFormatRecord", ["exports", "OutSystems/ClientRuntime/Main", "Charts.model", "Charts.model$AdvancedDataSeriesFormatRec"], function (exports, OutSystems, ChartsModel) {
var OS = OutSystems.Internal;
var AdvancedDataSeriesFormatRecord = (function (_super) {
__extends(AdvancedDataSeriesFormatRecord, _super);
function AdvancedDataSeriesFormatRecord(defaults) {
_super.apply(this, arguments);
}
AdvancedDataSeriesFormatRecord.attributesToDeclare = function () {
return [
this.attr("AdvancedDataSeriesFormat", "advancedDataSeriesFormatAttr", "AdvancedDataSeriesFormat", false, false, OS.Types.Record, function () {
return OS.DataTypes.ImmutableBase.getData(new ChartsModel.AdvancedDataSeriesFormatRec());
}, true, ChartsModel.AdvancedDataSeriesFormatRec)
].concat(_super.attributesToDeclare.call(this));
};
AdvancedDataSeriesFormatRecord.fromStructure = function (str) {
return new AdvancedDataSeriesFormatRecord(new AdvancedDataSeriesFormatRecord.RecordClass({
advancedDataSeriesFormatAttr: OS.DataTypes.ImmutableBase.getData(str)
}));
};
AdvancedDataSeriesFormatRecord._isAnonymousRecord = true;
AdvancedDataSeriesFormatRecord.UniqueId = "0d827485-a505-6429-f24e-1c2b11aa2953";
AdvancedDataSeriesFormatRecord.init();
return AdvancedDataSeriesFormatRecord;
})(OS.DataTypes.GenericRecord);
ChartsModel.AdvancedDataSeriesFormatRecord = AdvancedDataSeriesFormatRecord;

});
define("Charts.model$AdvancedDataSeriesFormatRecordList", ["exports", "OutSystems/ClientRuntime/Main", "Charts.model", "Charts.model$AdvancedDataSeriesFormatRecord"], function (exports, OutSystems, ChartsModel) {
var OS = OutSystems.Internal;
var AdvancedDataSeriesFormatRecordList = (function (_super) {
__extends(AdvancedDataSeriesFormatRecordList, _super);
function AdvancedDataSeriesFormatRecordList(defaults) {
_super.apply(this, arguments);
}
AdvancedDataSeriesFormatRecordList.itemType = ChartsModel.AdvancedDataSeriesFormatRecord;
return AdvancedDataSeriesFormatRecordList;
})(OS.DataTypes.GenericRecordList);
ChartsModel.AdvancedDataSeriesFormatRecordList = AdvancedDataSeriesFormatRecordList;

});
define("Charts.model$AdvancedFormatRec", ["exports", "OutSystems/ClientRuntime/Main", "Charts.model", "Charts.model$AdvancedDataPointFormatRecordList", "Charts.model$AdvancedDataSeriesFormatRecordList"], function (exports, OutSystems, ChartsModel) {
var OS = OutSystems.Internal;
var AdvancedFormatRec = (function (_super) {
__extends(AdvancedFormatRec, _super);
function AdvancedFormatRec(defaults) {
_super.apply(this, arguments);
}
AdvancedFormatRec.attributesToDeclare = function () {
return [
this.attr("DataPointFormats", "dataPointFormatsAttr", "DataPointFormats", false, false, OS.Types.RecordList, function () {
return OS.DataTypes.ImmutableBase.getData(new ChartsModel.AdvancedDataPointFormatRecordList());
}, true, ChartsModel.AdvancedDataPointFormatRecordList), 
this.attr("DataSeriesFormats", "dataSeriesFormatsAttr", "DataSeriesFormats", false, false, OS.Types.RecordList, function () {
return OS.DataTypes.ImmutableBase.getData(new ChartsModel.AdvancedDataSeriesFormatRecordList());
}, true, ChartsModel.AdvancedDataSeriesFormatRecordList), 
this.attr("XAxisJSON", "xAxisJSONAttr", "XAxisJSON", false, false, OS.Types.Text, function () {
return "";
}, true), 
this.attr("YAxisJSON", "yAxisJSONAttr", "YAxisJSON", false, false, OS.Types.Text, function () {
return "";
}, true), 
this.attr("HighchartsJSON", "highchartsJSONAttr", "HighchartsJSON", false, false, OS.Types.Text, function () {
return "";
}, true)
].concat(_super.attributesToDeclare.call(this));
};
AdvancedFormatRec.init();
return AdvancedFormatRec;
})(OS.DataTypes.GenericRecord);
ChartsModel.AdvancedFormatRec = AdvancedFormatRec;

});
define("Charts.model$XAxisFormatRec", ["exports", "OutSystems/ClientRuntime/Main", "Charts.model"], function (exports, OutSystems, ChartsModel) {
var OS = OutSystems.Internal;
var XAxisFormatRec = (function (_super) {
__extends(XAxisFormatRec, _super);
function XAxisFormatRec(defaults) {
_super.apply(this, arguments);
}
XAxisFormatRec.attributesToDeclare = function () {
return [
this.attr("Title", "titleAttr", "Title", false, false, OS.Types.Text, function () {
return "";
}, true), 
this.attr("LabelsRotation", "labelsRotationAttr", "LabelsRotation", false, false, OS.Types.Integer, function () {
return 0;
}, true), 
this.attr("MinValue", "minValueAttr", "MinValue", false, false, OS.Types.Text, function () {
return "";
}, true), 
this.attr("MaxValue", "maxValueAttr", "MaxValue", false, false, OS.Types.Text, function () {
return "";
}, true), 
this.attr("ValuesType", "valuesTypeAttr", "ValuesType", false, false, OS.Types.Integer, function () {
return ChartsModel.staticEntities.xAxisValuesType.auto;
}, true)
].concat(_super.attributesToDeclare.call(this));
};
XAxisFormatRec.init();
return XAxisFormatRec;
})(OS.DataTypes.GenericRecord);
ChartsModel.XAxisFormatRec = XAxisFormatRec;

});
define("Charts.model", ["exports", "OutSystems/ClientRuntime/Main"], function (exports, OutSystems) {
var OS = OutSystems.Internal;
var ChartsModel = exports;
});
