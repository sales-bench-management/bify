﻿/* 
 This source code (the "Generated Software") is generated by the OutSystems Platform 
 and is licensed by OutSystems (http://www.outsystems.com) to You solely for testing and evaluation 
 purposes, unless You and OutSystems have executed a specific agreement covering the use terms and 
 conditions of the Generated Software, in which case such agreement shall apply. 
*/

using System;
using System.Threading;
using System.Collections;
using System.Data;
using System.Web;
using OutSystems.HubEdition.RuntimePlatform;
using OutSystems.HubEdition.RuntimePlatform.Db;
using OutSystems.Internal.Db;
using OutSystems.HubEdition.RuntimePlatform.Log;

namespace ssBIFY {
	public partial class Roles {

		private static int? __ssProgressCircleUser = null;
		public static int ssProgressCircleUser {
			get {
				if (__ssProgressCircleUser == null) {
					using(Transaction trans = DatabaseAccess.ForRuntimeDatabase.GetRequestTransaction()) {
						__ssProgressCircleUser = DBRuntimePlatform.Instance.GetRoleId(trans, "f070a127-3ccf-4482-95a7-0a7a05969d82", "d13b7783-9664-44a6-aec7-55cd5b7155b4");
					}
				}
				if (__ssProgressCircleUser == null) {
					throw new Exception("Unable to find 'ProgressCircleUser' Role in meta-model");
				}
				return (int) __ssProgressCircleUser;
			}
		}
	}
}
