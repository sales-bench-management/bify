﻿/* 
 This source code (the "Generated Software") is generated by the OutSystems Platform 
 and is licensed by OutSystems (http://www.outsystems.com) to You solely for testing and evaluation 
 purposes, unless You and OutSystems have executed a specific agreement covering the use terms and 
 conditions of the Generated Software, in which case such agreement shall apply. 
*/

using System;
using System.Data;
using System.Runtime.Serialization;
using System.Reflection;
using System.Xml;
using OutSystems.ObjectKeys;
using OutSystems.RuntimeCommon;
using OutSystems.HubEdition.RuntimePlatform;
using OutSystems.HubEdition.RuntimePlatform.Db;
using OutSystems.Internal.Db;
using System.Xml.Serialization;
using System.Collections;
using OutSystems.HubEdition.RuntimePlatform.NewRuntime;

namespace ssBIFY {
	/// <summary>
	/// Structure <code>STCurrencyStructure</code> that represents the Service Studio structure
	///  <code>Currency</code> <p> Description: </p>
	/// </summary>
	[Serializable()]
	public partial struct STCurrencyStructure: ISerializable, ITypedRecord<STCurrencyStructure>, ISimpleRecord {
		internal static readonly GlobalObjectKey IdCurrency = GlobalObjectKey.Parse("1WMl+O7_wkOQDYQl1Ck5zQ*N5SiuFDy1k6gFn8PSbwX9w");

		public static void EnsureInitialized() {}
		[System.Xml.Serialization.XmlElement("Currency")]
		public decimal ssCurrency;


		public BitArray OptimizedAttributes;

		public STCurrencyStructure(params string[] dummy) {
			OptimizedAttributes = null;
			ssCurrency = 0.0M;
		}

		public BitArray[] GetDefaultOptimizedValues() {
			BitArray[] all = new BitArray[0];
			return all;
		}

		public BitArray[] AllOptimizedAttributes {
			set {
				if (value == null) {
				} else {
				}
			}
			get {
				BitArray[] all = new BitArray[0];
				return all;
			}
		}

		/// <summary>
		/// Read a record from database
		/// </summary>
		/// <param name="r"> Data base reader</param>
		/// <param name="index"> index</param>
		public void Read(IDataReader r, ref int index) {
			ssCurrency = r.ReadCurrency(index++, "Currency.Currency", 0.0M);
		}
		/// <summary>
		/// Read from database
		/// </summary>
		/// <param name="r"> Data reader</param>
		public void ReadDB(IDataReader r) {
			int index = 0;
			Read(r, ref index);
		}

		/// <summary>
		/// Read from record
		/// </summary>
		/// <param name="r"> Record</param>
		public void ReadIM(STCurrencyStructure r) {
			this = r;
		}


		public static bool operator == (STCurrencyStructure a, STCurrencyStructure b) {
			if (a.ssCurrency != b.ssCurrency) return false;
			return true;
		}

		public static bool operator != (STCurrencyStructure a, STCurrencyStructure b) {
			return !(a==b);
		}

		public override bool Equals(object o) {
			if (o.GetType() != typeof(STCurrencyStructure)) return false;
			return (this == (STCurrencyStructure) o);
		}

		public override int GetHashCode() {
			try {
				return base.GetHashCode()
				^ ssCurrency.GetHashCode()
				;
			} catch {
				return base.GetHashCode();
			}
		}

		public void GetObjectData(SerializationInfo info, StreamingContext context) {
			Type objInfo = this.GetType();
			FieldInfo[] fields;
			fields = objInfo.GetFields(BindingFlags.NonPublic | BindingFlags.Instance | BindingFlags.Public);
			for (int i = 0; i < fields.Length; i++)
			if (fields[i] .FieldType.IsSerializable)
			info.AddValue(fields[i] .Name, fields[i] .GetValue(this));
		}

		public STCurrencyStructure(SerializationInfo info, StreamingContext context) {
			OptimizedAttributes = null;
			ssCurrency = 0.0M;
			Type objInfo = this.GetType();
			FieldInfo fieldInfo = null;
			fieldInfo = objInfo.GetField("ssCurrency", BindingFlags.NonPublic | BindingFlags.Instance | BindingFlags.Public);
			if (fieldInfo == null) {
				throw new Exception("The field named 'ssCurrency' was not found.");
			}
			if (fieldInfo.FieldType.IsSerializable) {
				ssCurrency = (decimal) info.GetValue(fieldInfo.Name, fieldInfo.FieldType);
			}
		}

		public void RecursiveReset() {
		}

		public void InternalRecursiveSave() {
		}


		public STCurrencyStructure Duplicate() {
			STCurrencyStructure t;
			t.ssCurrency = this.ssCurrency;
			t.OptimizedAttributes = null;
			return t;
		}

		IRecord IRecord.Duplicate() {
			return Duplicate();
		}

		public void ToXml(Object parent, System.Xml.XmlElement baseElem, String fieldName, int detailLevel) {
			System.Xml.XmlElement recordElem = VarValue.AppendChild(baseElem, "Structure");
			if (fieldName != null) {
				VarValue.AppendAttribute(recordElem, "debug.field", fieldName);
				fieldName = fieldName.ToLowerInvariant();
			}
			if (detailLevel > 0) {
				if (!VarValue.FieldIsOptimized(parent, fieldName + ".Currency")) VarValue.AppendAttribute(recordElem, "Currency", ssCurrency, detailLevel, TypeKind.Currency); else VarValue.AppendOptimizedAttribute(recordElem, "Currency");
			} else {
				VarValue.AppendDeferredEvaluationElement(recordElem);
			}
		}

		public void EvaluateFields(VarValue variable, Object parent, String baseName, String fields) {
			String head = VarValue.GetHead(fields);
			String tail = VarValue.GetTail(fields);
			variable.Found = false;
			if (head == "currency") {
				if (!VarValue.FieldIsOptimized(parent, baseName + ".Currency")) variable.Value = ssCurrency; else variable.Optimized = true;
			}
			if (variable.Found && tail != null) variable.EvaluateFields(this, head, tail);
		}

		public bool ChangedAttributeGet(GlobalObjectKey key) {
			throw new Exception("Method not Supported");
		}

		public bool OptimizedAttributeGet(GlobalObjectKey key) {
			throw new Exception("Method not Supported");
		}

		public object AttributeGet(GlobalObjectKey key) {
			if (key == IdCurrency) {
				return ssCurrency;
			} else {
				throw new Exception("Invalid key");
			}
		}
		public void FillFromOther(IRecord other) {
			if (other == null) return;
			ssCurrency = (decimal) other.AttributeGet(IdCurrency);
		}
	} // STCurrencyStructure
	/// <summary>
	/// Structure <code>RCCurrencyRecord</code>
	/// </summary>
	[Serializable()]
	public partial struct RCCurrencyRecord: ISerializable, ITypedRecord<RCCurrencyRecord> {
		internal static readonly GlobalObjectKey IdCurrency = GlobalObjectKey.Parse("2UmDmepsh0WSfJ_D1JexCA*xw5B5D3a+AL68_k5TBbnnw");

		public static void EnsureInitialized() {}
		[System.Xml.Serialization.XmlElement("Currency")]
		public STCurrencyStructure ssSTCurrency;


		public static implicit operator STCurrencyStructure(RCCurrencyRecord r) {
			return r.ssSTCurrency;
		}

		public static implicit operator RCCurrencyRecord(STCurrencyStructure r) {
			RCCurrencyRecord res = new RCCurrencyRecord(null);
			res.ssSTCurrency = r;
			return res;
		}

		public BitArray OptimizedAttributes;

		public RCCurrencyRecord(params string[] dummy) {
			OptimizedAttributes = null;
			ssSTCurrency = new STCurrencyStructure(null);
		}

		public BitArray[] GetDefaultOptimizedValues() {
			BitArray[] all = new BitArray[1];
			all[0] = null;
			return all;
		}

		public BitArray[] AllOptimizedAttributes {
			set {
				if (value == null) {
				} else {
					ssSTCurrency.OptimizedAttributes = value[0];
				}
			}
			get {
				BitArray[] all = new BitArray[1];
				all[0] = null;
				return all;
			}
		}

		/// <summary>
		/// Read a record from database
		/// </summary>
		/// <param name="r"> Data base reader</param>
		/// <param name="index"> index</param>
		public void Read(IDataReader r, ref int index) {
			ssSTCurrency.Read(r, ref index);
		}
		/// <summary>
		/// Read from database
		/// </summary>
		/// <param name="r"> Data reader</param>
		public void ReadDB(IDataReader r) {
			int index = 0;
			Read(r, ref index);
		}

		/// <summary>
		/// Read from record
		/// </summary>
		/// <param name="r"> Record</param>
		public void ReadIM(RCCurrencyRecord r) {
			this = r;
		}


		public static bool operator == (RCCurrencyRecord a, RCCurrencyRecord b) {
			if (a.ssSTCurrency != b.ssSTCurrency) return false;
			return true;
		}

		public static bool operator != (RCCurrencyRecord a, RCCurrencyRecord b) {
			return !(a==b);
		}

		public override bool Equals(object o) {
			if (o.GetType() != typeof(RCCurrencyRecord)) return false;
			return (this == (RCCurrencyRecord) o);
		}

		public override int GetHashCode() {
			try {
				return base.GetHashCode()
				^ ssSTCurrency.GetHashCode()
				;
			} catch {
				return base.GetHashCode();
			}
		}

		public void GetObjectData(SerializationInfo info, StreamingContext context) {
			Type objInfo = this.GetType();
			FieldInfo[] fields;
			fields = objInfo.GetFields(BindingFlags.NonPublic | BindingFlags.Instance | BindingFlags.Public);
			for (int i = 0; i < fields.Length; i++)
			if (fields[i] .FieldType.IsSerializable)
			info.AddValue(fields[i] .Name, fields[i] .GetValue(this));
		}

		public RCCurrencyRecord(SerializationInfo info, StreamingContext context) {
			OptimizedAttributes = null;
			ssSTCurrency = new STCurrencyStructure(null);
			Type objInfo = this.GetType();
			FieldInfo fieldInfo = null;
			fieldInfo = objInfo.GetField("ssSTCurrency", BindingFlags.NonPublic | BindingFlags.Instance | BindingFlags.Public);
			if (fieldInfo == null) {
				throw new Exception("The field named 'ssSTCurrency' was not found.");
			}
			if (fieldInfo.FieldType.IsSerializable) {
				ssSTCurrency = (STCurrencyStructure) info.GetValue(fieldInfo.Name, fieldInfo.FieldType);
			}
		}

		public void RecursiveReset() {
			ssSTCurrency.RecursiveReset();
		}

		public void InternalRecursiveSave() {
			ssSTCurrency.InternalRecursiveSave();
		}


		public RCCurrencyRecord Duplicate() {
			RCCurrencyRecord t;
			t.ssSTCurrency = (STCurrencyStructure) this.ssSTCurrency.Duplicate();
			t.OptimizedAttributes = null;
			return t;
		}

		IRecord IRecord.Duplicate() {
			return Duplicate();
		}

		public void ToXml(Object parent, System.Xml.XmlElement baseElem, String fieldName, int detailLevel) {
			System.Xml.XmlElement recordElem = VarValue.AppendChild(baseElem, "Record");
			if (fieldName != null) {
				VarValue.AppendAttribute(recordElem, "debug.field", fieldName);
			}
			if (detailLevel > 0) {
				ssSTCurrency.ToXml(this, recordElem, "Currency", detailLevel - 1);
			} else {
				VarValue.AppendDeferredEvaluationElement(recordElem);
			}
		}

		public void EvaluateFields(VarValue variable, Object parent, String baseName, String fields) {
			String head = VarValue.GetHead(fields);
			String tail = VarValue.GetTail(fields);
			variable.Found = false;
			if (head == "currency") {
				if (!VarValue.FieldIsOptimized(parent, baseName + ".Currency")) variable.Value = ssSTCurrency; else variable.Optimized = true;
				variable.SetFieldName("currency");
			}
			if (variable.Found && tail != null) variable.EvaluateFields(this, head, tail);
		}

		public bool ChangedAttributeGet(GlobalObjectKey key) {
			throw new Exception("Method not Supported");
		}

		public bool OptimizedAttributeGet(GlobalObjectKey key) {
			throw new Exception("Method not Supported");
		}

		public object AttributeGet(GlobalObjectKey key) {
			if (key == IdCurrency) {
				return ssSTCurrency;
			} else {
				throw new Exception("Invalid key");
			}
		}
		public void FillFromOther(IRecord other) {
			if (other == null) return;
			ssSTCurrency.FillFromOther((IRecord) other.AttributeGet(IdCurrency));
		}
	} // RCCurrencyRecord
	/// <summary>
	/// RecordList type <code>RLCurrencyRecordList</code> that represents a record list of
	///  <code>Currency</code>
	/// </summary>
	[Serializable()]
	public partial class RLCurrencyRecordList: GenericRecordList<RCCurrencyRecord>, IEnumerable, IEnumerator, ISerializable {
		public static void EnsureInitialized() {}

		protected override RCCurrencyRecord GetElementDefaultValue() {
			return new RCCurrencyRecord("");
		}

		public T[] ToArray<T>(Func<RCCurrencyRecord, T> converter) {
			return ToArray(this, converter);
		}

		public static T[] ToArray<T>(RLCurrencyRecordList recordlist, Func<RCCurrencyRecord, T> converter) {
			return InnerToArray(recordlist, converter);
		}
		public static implicit operator RLCurrencyRecordList(RCCurrencyRecord[] array) {
			RLCurrencyRecordList result = new RLCurrencyRecordList();
			result.InnerFromArray(array);
			return result;
		}

		public static RLCurrencyRecordList ToList<T>(T[] array, Func <T, RCCurrencyRecord> converter) {
			RLCurrencyRecordList result = new RLCurrencyRecordList();
			result.InnerFromArray(array, converter);
			return result;
		}

		public static RLCurrencyRecordList FromRestList<T>(RestList<T> restList, Func <T, RCCurrencyRecord> converter) {
			RLCurrencyRecordList result = new RLCurrencyRecordList();
			result.InnerFromRestList(restList, converter);
			return result;
		}
		/// <summary>
		/// Default Constructor
		/// </summary>
		public RLCurrencyRecordList(): base() {
		}

		/// <summary>
		/// Constructor with transaction parameter
		/// </summary>
		/// <param name="trans"> IDbTransaction Parameter</param>
		[Obsolete("Use the Default Constructor and set the Transaction afterwards.")]
		public RLCurrencyRecordList(IDbTransaction trans): base(trans) {
		}

		/// <summary>
		/// Constructor with transaction parameter and alternate read method
		/// </summary>
		/// <param name="trans"> IDbTransaction Parameter</param>
		/// <param name="alternateReadDBMethod"> Alternate Read Method</param>
		[Obsolete("Use the Default Constructor and set the Transaction afterwards.")]
		public RLCurrencyRecordList(IDbTransaction trans, ReadDBMethodDelegate alternateReadDBMethod): this(trans) {
			this.alternateReadDBMethod = alternateReadDBMethod;
		}

		/// <summary>
		/// Constructor declaration for serialization
		/// </summary>
		/// <param name="info"> SerializationInfo</param>
		/// <param name="context"> StreamingContext</param>
		public RLCurrencyRecordList(SerializationInfo info, StreamingContext context): base(info, context) {
		}

		public override BitArray[] GetDefaultOptimizedValues() {
			BitArray[] def = new BitArray[1];
			def[0] = null;
			return def;
		}
		/// <summary>
		/// Create as new list
		/// </summary>
		/// <returns>The new record list</returns>
		protected override OSList<RCCurrencyRecord> NewList() {
			return new RLCurrencyRecordList();
		}


	} // RLCurrencyRecordList
	/// <summary>
	/// RecordList type <code>RLCurrencyList</code> that represents a record list of <code>Currency</code>
	/// </summary>
	[Serializable()]
	public partial class RLCurrencyList: GenericRecordList<STCurrencyStructure>, IEnumerable, IEnumerator, ISerializable {
		public static void EnsureInitialized() {}

		protected override STCurrencyStructure GetElementDefaultValue() {
			return new STCurrencyStructure("");
		}

		public T[] ToArray<T>(Func<STCurrencyStructure, T> converter) {
			return ToArray(this, converter);
		}

		public static T[] ToArray<T>(RLCurrencyList recordlist, Func<STCurrencyStructure, T> converter) {
			return InnerToArray(recordlist, converter);
		}
		public static implicit operator RLCurrencyList(STCurrencyStructure[] array) {
			RLCurrencyList result = new RLCurrencyList();
			result.InnerFromArray(array);
			return result;
		}

		public static RLCurrencyList ToList<T>(T[] array, Func <T, STCurrencyStructure> converter) {
			RLCurrencyList result = new RLCurrencyList();
			result.InnerFromArray(array, converter);
			return result;
		}

		public static RLCurrencyList FromRestList<T>(RestList<T> restList, Func <T, STCurrencyStructure> converter) {
			RLCurrencyList result = new RLCurrencyList();
			result.InnerFromRestList(restList, converter);
			return result;
		}
		/// <summary>
		/// Default Constructor
		/// </summary>
		public RLCurrencyList(): base() {
		}

		/// <summary>
		/// Constructor with transaction parameter
		/// </summary>
		/// <param name="trans"> IDbTransaction Parameter</param>
		[Obsolete("Use the Default Constructor and set the Transaction afterwards.")]
		public RLCurrencyList(IDbTransaction trans): base(trans) {
		}

		/// <summary>
		/// Constructor with transaction parameter and alternate read method
		/// </summary>
		/// <param name="trans"> IDbTransaction Parameter</param>
		/// <param name="alternateReadDBMethod"> Alternate Read Method</param>
		[Obsolete("Use the Default Constructor and set the Transaction afterwards.")]
		public RLCurrencyList(IDbTransaction trans, ReadDBMethodDelegate alternateReadDBMethod): this(trans) {
			this.alternateReadDBMethod = alternateReadDBMethod;
		}

		/// <summary>
		/// Constructor declaration for serialization
		/// </summary>
		/// <param name="info"> SerializationInfo</param>
		/// <param name="context"> StreamingContext</param>
		public RLCurrencyList(SerializationInfo info, StreamingContext context): base(info, context) {
		}

		public override BitArray[] GetDefaultOptimizedValues() {
			BitArray[] def = new BitArray[0];
			return def;
		}
		/// <summary>
		/// Create as new list
		/// </summary>
		/// <returns>The new record list</returns>
		protected override OSList<STCurrencyStructure> NewList() {
			return new RLCurrencyList();
		}


	} // RLCurrencyList
}

namespace ssBIFY {
	[XmlType("Currency")]
	public class WORCCurrencyRecord {
		[System.Xml.Serialization.XmlElement("Currency")]
		public decimal varWSCurrency;

		public WORCCurrencyRecord() {
			varWSCurrency = (decimal) 0.0M;
		}

		public WORCCurrencyRecord(STCurrencyStructure r) {
			varWSCurrency = (decimal) r.ssCurrency;
		}

		public static RLCurrencyList ToRecordList(WORCCurrencyRecord[] array) {
			RLCurrencyList rl = new RLCurrencyList();
			if (array != null) {
				foreach(WORCCurrencyRecord val in array) {
					rl.Append(val);
				}
			}
			return rl;
		}

		public static WORCCurrencyRecord[] FromRecordList(RLCurrencyList rl) {
			WORCCurrencyRecord[] array = new WORCCurrencyRecord[rl == null ? 0: rl.Length];
			for (int i = 0; i < array.Length; i++) {
				array[i] = rl.Data[i];
			}
			return array;
		}
	}
}

namespace ssBIFY {
	partial struct RCCurrencyRecord {
		public static implicit operator WORCCurrencyRecord(RCCurrencyRecord r) {
			return new WORCCurrencyRecord(r.ssSTCurrency);
		}

		public static implicit operator RCCurrencyRecord(WORCCurrencyRecord w) {
			RCCurrencyRecord r = new RCCurrencyRecord("");
			if (w != null) {
				r.ssSTCurrency = w;
			}
			return r;
		}

	}

	partial struct STCurrencyStructure {
		public static implicit operator WORCCurrencyRecord(STCurrencyStructure r) {
			return new WORCCurrencyRecord(r);
		}

		public static implicit operator STCurrencyStructure(WORCCurrencyRecord w) {
			STCurrencyStructure r = new STCurrencyStructure("");
			if (w != null) {
				r.ssCurrency = (decimal) w.varWSCurrency;
			}
			return r;
		}

	}
}


namespace ssBIFY {
	[Serializable()]
	public partial class WORLCurrencyRecordList {
		public WORCCurrencyRecord[] Array;


		public WORLCurrencyRecordList(WORCCurrencyRecord[] r) {
			if (r == null)
			Array = new WORCCurrencyRecord[0];
			else
			Array = r;
		}
		public WORLCurrencyRecordList() {
			Array = new WORCCurrencyRecord[0];
		}

		public WORLCurrencyRecordList(RLCurrencyRecordList rl) {
			rl=(RLCurrencyRecordList) rl.Duplicate();
			Array = new WORCCurrencyRecord[rl.Length];
			while (!rl.Eof) {
				Array[rl.CurrentRowNumber] = new WORCCurrencyRecord(rl.CurrentRec);
				rl.Advance();
			}
		}

	}
}

namespace ssBIFY {
	partial class RLCurrencyRecordList {
		public static implicit operator RLCurrencyRecordList(WORCCurrencyRecord[] array) {
			RLCurrencyRecordList rl = new RLCurrencyRecordList();
			if (array != null) {
				foreach(WORCCurrencyRecord val in array) {
					rl.Append((RCCurrencyRecord) val);
				}
			}
			return rl;
		}
		public static implicit operator WORCCurrencyRecord[](RLCurrencyRecordList rl) {
			WORCCurrencyRecord[] array = new WORCCurrencyRecord[rl == null ? 0: rl.Length];
			for (int i = 0; i < array.Length; i++) {
				array[i] = (RCCurrencyRecord) rl.Data[i];
			}
			return array;
		}
	}
}

namespace ssBIFY {
	partial class WORLCurrencyRecordList {
		public static implicit operator RLCurrencyRecordList(WORLCurrencyRecordList w) {
			return w.Array;
		}
		public static implicit operator WORLCurrencyRecordList(RLCurrencyRecordList rl) {
			return new WORLCurrencyRecordList(rl);
		}
		public static implicit operator WORCCurrencyRecord[](WORLCurrencyRecordList w) {
			if (w != null) {
				return w.Array;
			}
			return null;
		}
		public static implicit operator WORLCurrencyRecordList(WORCCurrencyRecord[] array) {
			return new WORLCurrencyRecordList(array);
		}
	}
}

namespace ssBIFY {
	[Serializable()]
	public partial class WORLCurrencyList {
		public WORCCurrencyRecord[] Array;


		public WORLCurrencyList(WORCCurrencyRecord[] r) {
			if (r == null)
			Array = new WORCCurrencyRecord[0];
			else
			Array = r;
		}
		public WORLCurrencyList() {
			Array = new WORCCurrencyRecord[0];
		}

		public WORLCurrencyList(RLCurrencyList rl) {
			rl=(RLCurrencyList) rl.Duplicate();
			Array = new WORCCurrencyRecord[rl.Length];
			while (!rl.Eof) {
				Array[rl.CurrentRowNumber] = rl.CurrentRec.Duplicate();
				rl.Advance();
			}
		}

	}
}

namespace ssBIFY {
	partial class RLCurrencyList {
		public static implicit operator RLCurrencyList(WORCCurrencyRecord[] array) {
			RLCurrencyList rl = new RLCurrencyList();
			if (array != null) {
				foreach(WORCCurrencyRecord val in array) {
					rl.Append((STCurrencyStructure) val);
				}
			}
			return rl;
		}
		public static implicit operator WORCCurrencyRecord[](RLCurrencyList rl) {
			WORCCurrencyRecord[] array = new WORCCurrencyRecord[rl == null ? 0: rl.Length];
			for (int i = 0; i < array.Length; i++) {
				array[i] = (STCurrencyStructure) rl.Data[i];
			}
			return array;
		}
	}
}

namespace ssBIFY {
	partial class WORLCurrencyList {
		public static implicit operator RLCurrencyList(WORLCurrencyList w) {
			return w.Array;
		}
		public static implicit operator WORLCurrencyList(RLCurrencyList rl) {
			return new WORLCurrencyList(rl);
		}
		public static implicit operator WORCCurrencyRecord[](WORLCurrencyList w) {
			if (w != null) {
				return w.Array;
			}
			return null;
		}
		public static implicit operator WORLCurrencyList(WORCCurrencyRecord[] array) {
			return new WORLCurrencyList(array);
		}
	}
}

