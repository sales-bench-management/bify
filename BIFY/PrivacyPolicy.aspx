﻿<%@ Page Language="c#" Codebehind="PrivacyPolicy.aspx.cs" AutoEventWireup="false" Inherits="ssBIFY.Flows.FlowNotLoggedIn.ScrnPrivacyPolicy" %>
<%@ Register TagPrefix="osweb" Namespace="OutSystems.HubEdition.WebWidgets" Assembly="OutSystems.HubEdition.WebWidgets" %>
<%@ Import namespace="ssBIFY" %>
<%@ Import namespace="OutSystems.HubEdition.RuntimePlatform" %>
<%@ Assembly Name="OutSystems.WidgetsRuntimeAPI" %>
<%@ Register TagPrefix="widgets" TagName="Kz3isd_zy0kiORsLuRetsJw" Src="Blocks\BIFY\Common\LayoutHomePage.ascx" %>
<%@ Register TagPrefix="widgets" TagName="KXIDBGmiH30CQls7epjSjoQ" Src="Blocks\BIFY\Common\ApplicationTitle.ascx" %>
<%@ Register TagPrefix="widgets" TagName="KvjOOjGhXMUi5hSbN9Ooc0g" Src="Blocks\BIFY\Weblocks\LoginRegister.ascx" %>
<%@ Register TagPrefix="widgets" TagName="KnfUSiLskTUmjVXcue64Gkg" Src="Blocks\BIFY\Weblocks\LoginRegisterMobile.ascx" %>
<%@ Register TagPrefix="widgets" TagName="KVlxoCzEyUUmJdaTCoz8lHA" Src="Blocks\BIFY\Weblocks\HeaderCenter.ascx" %>
<%@ Register TagPrefix="widgets" TagName="Kz0Pe8bEM1keIAmKQ6Kxphg" Src="Blocks\BIFY\Weblocks\FooterNotLoggedIn.ascx" %>

<%= BIFY_Properties.DocType %>
<html xmlns="http://www.w3.org/1999/xhtml" >
  <head runat="server"><%= GetHeadTopJavaScript() %>
	<title><%= HttpUtility.HtmlEncode (Title) %></title>
    <meta http-equiv="Content-Type" content="<%= "text/html; charset=" + Response.ContentEncoding.WebName %>" />
    <meta http-equiv="Content-Script-Type" content="text/javascript" />
    <meta http-equiv="Content-Style-Type" content="text/css" />
<%= "\n" + GetStyleSheetIncludes() %><%= GetRequestInfoJavaScript() + GetJavaScriptIncludes() + GetHeadBottomJavaScript() %>
  </head>
  <osweb:Body runat="server"><%= GetBodyTopJavaScript() %>
    <osweb:Form id="WebForm1" method="post"  action="<%# GetFormAction() %>" runat="server">
<widgets:Kz3isd_zy0kiORsLuRetsJw runat="server" id="wt_WebBlockInstance17" OnEvaluateParameters="webBlck_WebBlockInstance17_onDataBinding" InstanceID="_WebBlockInstance17"><phLeft><widgets:KXIDBGmiH30CQls7epjSjoQ runat="server" id="wt_WebBlockInstance14" OnEvaluateParameters="webBlck_WebBlockInstance14_onDataBinding" InstanceID="_WebBlockInstance14"></widgets:KXIDBGmiH30CQls7epjSjoQ></phLeft><phCenter></phCenter><phRight><widgets:KvjOOjGhXMUi5hSbN9Ooc0g runat="server" id="wt_WebBlockInstance4" OnEvaluateParameters="webBlck_WebBlockInstance4_onDataBinding" InstanceID="_WebBlockInstance4"></widgets:KvjOOjGhXMUi5hSbN9Ooc0g></phRight><phHeaderContent><osweb:Container runat="server" id="wt_Container21" anonymous="true" onDataBinding="cnt_Container21_onDataBinding" cssClass="mobile_login_register" align="center"><widgets:KnfUSiLskTUmjVXcue64Gkg runat="server" id="wt_WebBlockInstance19" OnEvaluateParameters="webBlck_WebBlockInstance19_onDataBinding" InstanceID="_WebBlockInstance19"></widgets:KnfUSiLskTUmjVXcue64Gkg></osweb:Container></phHeaderContent><phMainContent><widgets:KVlxoCzEyUUmJdaTCoz8lHA runat="server" id="wt_WebBlockInstance13" OnEvaluateParameters="webBlck_WebBlockInstance13_onDataBinding" InstanceID="_WebBlockInstance13"><phTitle><osweb:PlaceHolder runat="server"><%# "Privacy" %></osweb:PlaceHolder></phTitle><phTitle2></phTitle2><phButton></phButton><phImage><osweb:DynamicImage runat="server" id="wt_Image1" anonymous="true" StaticSource="<%# Images.hero_Source() %>" ImageType="Static" StaticPath="<%# AppUtils.Instance.getImagePath(skipSeo: this.Page is OutSystems.HubEdition.RuntimePlatform.Web.IEmailScreen) %>" alt=""/></phImage></widgets:KVlxoCzEyUUmJdaTCoz8lHA><osweb:Container runat="server" id="wt_Container15" anonymous="true" onDataBinding="cnt_Container15_onDataBinding" cssClass="padding-xl"><osweb:PlaceHolder runat="server"><%# expression_InlineExpression6() %></osweb:PlaceHolder></osweb:Container><widgets:Kz0Pe8bEM1keIAmKQ6Kxphg runat="server" id="wt_WebBlockInstance11" OnEvaluateParameters="webBlck_WebBlockInstance11_onDataBinding" InstanceID="_WebBlockInstance11"></widgets:Kz0Pe8bEM1keIAmKQ6Kxphg></phMainContent><phFooter></phFooter></widgets:Kz3isd_zy0kiORsLuRetsJw><osweb:DummySubmitLink runat="server" id="Dummy_Submit_Link"/>
    <%= OutSystems.HubEdition.RuntimePlatform.AppInfo.GetAppInfo().GetWatermark() %><%= GetCallbackDebug()	
%><%= GetVisitCode() %></osweb:Form><%= GetEndPageJavaScript() + GetBodyBottomJavaScript() %>
  </osweb:Body>
</html>
