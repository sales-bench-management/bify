﻿/* 
 This source code (the "Generated Software") is generated by the OutSystems Platform 
 and is licensed by OutSystems (http://www.outsystems.com) to You solely for testing and evaluation 
 purposes, unless You and OutSystems have executed a specific agreement covering the use terms and 
 conditions of the Generated Software, in which case such agreement shall apply. 
*/

using System;
using System.Linq;
using OutSystems.HubEdition.RuntimePlatform;
using OutSystems.HubEdition.RuntimePlatform.Db;
using OutSystems.Internal.Db;
using OutSystems.RuntimeCommon;
using OutSystems.RuntimeCommon.Caching;

using System.Text;
using System.Data;
using System.Collections.Generic;
using Newtonsoft.Json;
using OutSystems.RESTService;
using OutSystems.RESTService.Behaviors;
using OutSystems.RESTService.Controllers;
using OutSystems.ObjectKeys;
using OutSystems.HubEdition.RuntimePlatform.NewRuntime;


namespace ssBIFY.RestRecords {

	public class JSONSTShopping_resultItemStructure: AbstractRESTStructure<STShopping_resultItemStructure> {
		[JsonProperty("position")]
		public long? AttrPosition;

		[JsonProperty("title")]
		public string AttrTitle;

		[JsonProperty("link")]
		public string AttrLink;

		[JsonProperty("source")]
		public string AttrSource;

		[JsonProperty("price")]
		public string AttrPrice;

		[JsonProperty("snippet")]
		public string AttrSnippet;

		[JsonProperty("thumbnail")]
		public string AttrThumbnail;

		public JSONSTShopping_resultItemStructure() {}

		public JSONSTShopping_resultItemStructure(STShopping_resultItemStructure s, IBehaviorsConfiguration config) {
			if (config.DefaultValuesBehavior == DefaultValuesBehavior.DontSend) {
				AttrPosition = ConvertToRestWithoutDefaults(s.ssPosition, 0L);
				AttrTitle = ConvertToRestWithoutDefaults(s.ssTitle, "");
				AttrLink = ConvertToRestWithoutDefaults(s.ssLink, "");
				AttrSource = ConvertToRestWithoutDefaults(s.ssSource, "");
				AttrPrice = ConvertToRestWithoutDefaults(s.ssPrice, "");
				AttrSnippet = ConvertToRestWithoutDefaults(s.ssSnippet, "");
				AttrThumbnail = ConvertToRestWithoutDefaults(s.ssThumbnail, "");
			} else {
				AttrPosition = (long?) s.ssPosition;
				AttrTitle = s.ssTitle;
				AttrLink = s.ssLink;
				AttrSource = s.ssSource;
				AttrPrice = s.ssPrice;
				AttrSnippet = s.ssSnippet;
				AttrThumbnail = s.ssThumbnail;
			}
		}

		public static Func<ssBIFY.RestRecords.JSONSTShopping_resultItemStructure, STShopping_resultItemStructure> ToStructureDelegate(IBehaviorsConfiguration config) {
			return (ssBIFY.RestRecords.JSONSTShopping_resultItemStructure s) => ToStructure(s, config);
		}
		public static STShopping_resultItemStructure ToStructure(ssBIFY.RestRecords.JSONSTShopping_resultItemStructure obj, IBehaviorsConfiguration config) {
			STShopping_resultItemStructure s = new STShopping_resultItemStructure(null);
			if (obj != null) {
				s.ssPosition = obj.AttrPosition == null ? 0L: obj.AttrPosition.Value;
				s.ssTitle = obj.AttrTitle == null ? "": obj.AttrTitle;
				s.ssLink = obj.AttrLink == null ? "": obj.AttrLink;
				s.ssSource = obj.AttrSource == null ? "": obj.AttrSource;
				s.ssPrice = obj.AttrPrice == null ? "": obj.AttrPrice;
				s.ssSnippet = obj.AttrSnippet == null ? "": obj.AttrSnippet;
				s.ssThumbnail = obj.AttrThumbnail == null ? "": obj.AttrThumbnail;
			}
			return s;
		}

		public static Func<STShopping_resultItemStructure, ssBIFY.RestRecords.JSONSTShopping_resultItemStructure> FromStructureDelegate(IBehaviorsConfiguration config) {
			return (STShopping_resultItemStructure s) => FromStructure(s, config);
		}
		public static ssBIFY.RestRecords.JSONSTShopping_resultItemStructure FromStructure(STShopping_resultItemStructure s, IBehaviorsConfiguration config) {
			return new ssBIFY.RestRecords.JSONSTShopping_resultItemStructure(s, config);
		}

	}



}