﻿/* 
 This source code (the "Generated Software") is generated by the OutSystems Platform 
 and is licensed by OutSystems (http://www.outsystems.com) to You solely for testing and evaluation 
 purposes, unless You and OutSystems have executed a specific agreement covering the use terms and 
 conditions of the Generated Software, in which case such agreement shall apply. 
*/

using System;
using System.Data;
using System.Runtime.Serialization;
using System.Reflection;
using System.Xml;
using OutSystems.ObjectKeys;
using OutSystems.RuntimeCommon;
using OutSystems.HubEdition.RuntimePlatform;
using OutSystems.HubEdition.RuntimePlatform.Db;
using OutSystems.Internal.Db;
using System.Xml.Serialization;
using System.Collections;
using OutSystems.HubEdition.RuntimePlatform.NewRuntime;

namespace ssBIFY {
	/// <summary>
	/// Structure <code>STHTTPHeader3Structure</code> that represents the Service Studio structure
	///  <code>HTTPHeader3</code> <p> Description: </p>
	/// </summary>
	[Serializable()]
	public partial struct STHTTPHeader3Structure: ISerializable, ITypedRecord<STHTTPHeader3Structure>, ISimpleRecord {
		internal static readonly GlobalObjectKey IdName = GlobalObjectKey.Parse("1WMl+O7_wkOQDYQl1Ck5zQ*1+iM5qY4BkGwCIqEe69ceg");
		internal static readonly GlobalObjectKey IdValue = GlobalObjectKey.Parse("1WMl+O7_wkOQDYQl1Ck5zQ*51Rv3DhiY0+pSArco94xjQ");

		public static void EnsureInitialized() {}
		[System.Xml.Serialization.XmlElement("Name")]
		public string ssName;

		[System.Xml.Serialization.XmlElement("Value")]
		public string ssValue;


		public BitArray OptimizedAttributes;

		public STHTTPHeader3Structure(params string[] dummy) {
			OptimizedAttributes = null;
			ssName = "";
			ssValue = "";
		}

		public BitArray[] GetDefaultOptimizedValues() {
			BitArray[] all = new BitArray[0];
			return all;
		}

		public BitArray[] AllOptimizedAttributes {
			set {
				if (value == null) {
				} else {
				}
			}
			get {
				BitArray[] all = new BitArray[0];
				return all;
			}
		}

		/// <summary>
		/// Read a record from database
		/// </summary>
		/// <param name="r"> Data base reader</param>
		/// <param name="index"> index</param>
		public void Read(IDataReader r, ref int index) {
			ssName = r.ReadText(index++, "HTTPHeader3.Name", "");
			ssValue = r.ReadText(index++, "HTTPHeader3.Value", "");
		}
		/// <summary>
		/// Read from database
		/// </summary>
		/// <param name="r"> Data reader</param>
		public void ReadDB(IDataReader r) {
			int index = 0;
			Read(r, ref index);
		}

		/// <summary>
		/// Read from record
		/// </summary>
		/// <param name="r"> Record</param>
		public void ReadIM(STHTTPHeader3Structure r) {
			this = r;
		}


		public static bool operator == (STHTTPHeader3Structure a, STHTTPHeader3Structure b) {
			if (a.ssName != b.ssName) return false;
			if (a.ssValue != b.ssValue) return false;
			return true;
		}

		public static bool operator != (STHTTPHeader3Structure a, STHTTPHeader3Structure b) {
			return !(a==b);
		}

		public override bool Equals(object o) {
			if (o.GetType() != typeof(STHTTPHeader3Structure)) return false;
			return (this == (STHTTPHeader3Structure) o);
		}

		public override int GetHashCode() {
			try {
				return base.GetHashCode()
				^ ssName.GetHashCode()
				^ ssValue.GetHashCode()
				;
			} catch {
				return base.GetHashCode();
			}
		}

		public void GetObjectData(SerializationInfo info, StreamingContext context) {
			Type objInfo = this.GetType();
			FieldInfo[] fields;
			fields = objInfo.GetFields(BindingFlags.NonPublic | BindingFlags.Instance | BindingFlags.Public);
			for (int i = 0; i < fields.Length; i++)
			if (fields[i] .FieldType.IsSerializable)
			info.AddValue(fields[i] .Name, fields[i] .GetValue(this));
		}

		public STHTTPHeader3Structure(SerializationInfo info, StreamingContext context) {
			OptimizedAttributes = null;
			ssName = "";
			ssValue = "";
			Type objInfo = this.GetType();
			FieldInfo fieldInfo = null;
			fieldInfo = objInfo.GetField("ssName", BindingFlags.NonPublic | BindingFlags.Instance | BindingFlags.Public);
			if (fieldInfo == null) {
				throw new Exception("The field named 'ssName' was not found.");
			}
			if (fieldInfo.FieldType.IsSerializable) {
				ssName = (string) info.GetValue(fieldInfo.Name, fieldInfo.FieldType);
			}
			fieldInfo = objInfo.GetField("ssValue", BindingFlags.NonPublic | BindingFlags.Instance | BindingFlags.Public);
			if (fieldInfo == null) {
				throw new Exception("The field named 'ssValue' was not found.");
			}
			if (fieldInfo.FieldType.IsSerializable) {
				ssValue = (string) info.GetValue(fieldInfo.Name, fieldInfo.FieldType);
			}
		}

		public void RecursiveReset() {
		}

		public void InternalRecursiveSave() {
		}


		public STHTTPHeader3Structure Duplicate() {
			STHTTPHeader3Structure t;
			t.ssName = this.ssName;
			t.ssValue = this.ssValue;
			t.OptimizedAttributes = null;
			return t;
		}

		IRecord IRecord.Duplicate() {
			return Duplicate();
		}

		public void ToXml(Object parent, System.Xml.XmlElement baseElem, String fieldName, int detailLevel) {
			System.Xml.XmlElement recordElem = VarValue.AppendChild(baseElem, "Structure");
			if (fieldName != null) {
				VarValue.AppendAttribute(recordElem, "debug.field", fieldName);
				fieldName = fieldName.ToLowerInvariant();
			}
			if (detailLevel > 0) {
				if (!VarValue.FieldIsOptimized(parent, fieldName + ".Name")) VarValue.AppendAttribute(recordElem, "Name", ssName, detailLevel, TypeKind.Text); else VarValue.AppendOptimizedAttribute(recordElem, "Name");
				if (!VarValue.FieldIsOptimized(parent, fieldName + ".Value")) VarValue.AppendAttribute(recordElem, "Value", ssValue, detailLevel, TypeKind.Text); else VarValue.AppendOptimizedAttribute(recordElem, "Value");
			} else {
				VarValue.AppendDeferredEvaluationElement(recordElem);
			}
		}

		public void EvaluateFields(VarValue variable, Object parent, String baseName, String fields) {
			String head = VarValue.GetHead(fields);
			String tail = VarValue.GetTail(fields);
			variable.Found = false;
			if (head == "name") {
				if (!VarValue.FieldIsOptimized(parent, baseName + ".Name")) variable.Value = ssName; else variable.Optimized = true;
			} else if (head == "value") {
				if (!VarValue.FieldIsOptimized(parent, baseName + ".Value")) variable.Value = ssValue; else variable.Optimized = true;
			}
			if (variable.Found && tail != null) variable.EvaluateFields(this, head, tail);
		}

		public bool ChangedAttributeGet(GlobalObjectKey key) {
			throw new Exception("Method not Supported");
		}

		public bool OptimizedAttributeGet(GlobalObjectKey key) {
			throw new Exception("Method not Supported");
		}

		public object AttributeGet(GlobalObjectKey key) {
			if (key == IdName) {
				return ssName;
			} else if (key == IdValue) {
				return ssValue;
			} else {
				throw new Exception("Invalid key");
			}
		}
		public void FillFromOther(IRecord other) {
			if (other == null) return;
			ssName = (string) other.AttributeGet(IdName);
			ssValue = (string) other.AttributeGet(IdValue);
		}
	} // STHTTPHeader3Structure
	/// <summary>
	/// Structure <code>RCHTTPHeader3Record</code>
	/// </summary>
	[Serializable()]
	public partial struct RCHTTPHeader3Record: ISerializable, ITypedRecord<RCHTTPHeader3Record> {
		internal static readonly GlobalObjectKey IdHTTPHeader3 = GlobalObjectKey.Parse("2UmDmepsh0WSfJ_D1JexCA*hq_Ykwsr8U2GwdcW7LCMaQ");

		public static void EnsureInitialized() {}
		[System.Xml.Serialization.XmlElement("HTTPHeader3")]
		public STHTTPHeader3Structure ssSTHTTPHeader3;


		public static implicit operator STHTTPHeader3Structure(RCHTTPHeader3Record r) {
			return r.ssSTHTTPHeader3;
		}

		public static implicit operator RCHTTPHeader3Record(STHTTPHeader3Structure r) {
			RCHTTPHeader3Record res = new RCHTTPHeader3Record(null);
			res.ssSTHTTPHeader3 = r;
			return res;
		}

		public BitArray OptimizedAttributes;

		public RCHTTPHeader3Record(params string[] dummy) {
			OptimizedAttributes = null;
			ssSTHTTPHeader3 = new STHTTPHeader3Structure(null);
		}

		public BitArray[] GetDefaultOptimizedValues() {
			BitArray[] all = new BitArray[1];
			all[0] = null;
			return all;
		}

		public BitArray[] AllOptimizedAttributes {
			set {
				if (value == null) {
				} else {
					ssSTHTTPHeader3.OptimizedAttributes = value[0];
				}
			}
			get {
				BitArray[] all = new BitArray[1];
				all[0] = null;
				return all;
			}
		}

		/// <summary>
		/// Read a record from database
		/// </summary>
		/// <param name="r"> Data base reader</param>
		/// <param name="index"> index</param>
		public void Read(IDataReader r, ref int index) {
			ssSTHTTPHeader3.Read(r, ref index);
		}
		/// <summary>
		/// Read from database
		/// </summary>
		/// <param name="r"> Data reader</param>
		public void ReadDB(IDataReader r) {
			int index = 0;
			Read(r, ref index);
		}

		/// <summary>
		/// Read from record
		/// </summary>
		/// <param name="r"> Record</param>
		public void ReadIM(RCHTTPHeader3Record r) {
			this = r;
		}


		public static bool operator == (RCHTTPHeader3Record a, RCHTTPHeader3Record b) {
			if (a.ssSTHTTPHeader3 != b.ssSTHTTPHeader3) return false;
			return true;
		}

		public static bool operator != (RCHTTPHeader3Record a, RCHTTPHeader3Record b) {
			return !(a==b);
		}

		public override bool Equals(object o) {
			if (o.GetType() != typeof(RCHTTPHeader3Record)) return false;
			return (this == (RCHTTPHeader3Record) o);
		}

		public override int GetHashCode() {
			try {
				return base.GetHashCode()
				^ ssSTHTTPHeader3.GetHashCode()
				;
			} catch {
				return base.GetHashCode();
			}
		}

		public void GetObjectData(SerializationInfo info, StreamingContext context) {
			Type objInfo = this.GetType();
			FieldInfo[] fields;
			fields = objInfo.GetFields(BindingFlags.NonPublic | BindingFlags.Instance | BindingFlags.Public);
			for (int i = 0; i < fields.Length; i++)
			if (fields[i] .FieldType.IsSerializable)
			info.AddValue(fields[i] .Name, fields[i] .GetValue(this));
		}

		public RCHTTPHeader3Record(SerializationInfo info, StreamingContext context) {
			OptimizedAttributes = null;
			ssSTHTTPHeader3 = new STHTTPHeader3Structure(null);
			Type objInfo = this.GetType();
			FieldInfo fieldInfo = null;
			fieldInfo = objInfo.GetField("ssSTHTTPHeader3", BindingFlags.NonPublic | BindingFlags.Instance | BindingFlags.Public);
			if (fieldInfo == null) {
				throw new Exception("The field named 'ssSTHTTPHeader3' was not found.");
			}
			if (fieldInfo.FieldType.IsSerializable) {
				ssSTHTTPHeader3 = (STHTTPHeader3Structure) info.GetValue(fieldInfo.Name, fieldInfo.FieldType);
			}
		}

		public void RecursiveReset() {
			ssSTHTTPHeader3.RecursiveReset();
		}

		public void InternalRecursiveSave() {
			ssSTHTTPHeader3.InternalRecursiveSave();
		}


		public RCHTTPHeader3Record Duplicate() {
			RCHTTPHeader3Record t;
			t.ssSTHTTPHeader3 = (STHTTPHeader3Structure) this.ssSTHTTPHeader3.Duplicate();
			t.OptimizedAttributes = null;
			return t;
		}

		IRecord IRecord.Duplicate() {
			return Duplicate();
		}

		public void ToXml(Object parent, System.Xml.XmlElement baseElem, String fieldName, int detailLevel) {
			System.Xml.XmlElement recordElem = VarValue.AppendChild(baseElem, "Record");
			if (fieldName != null) {
				VarValue.AppendAttribute(recordElem, "debug.field", fieldName);
			}
			if (detailLevel > 0) {
				ssSTHTTPHeader3.ToXml(this, recordElem, "HTTPHeader3", detailLevel - 1);
			} else {
				VarValue.AppendDeferredEvaluationElement(recordElem);
			}
		}

		public void EvaluateFields(VarValue variable, Object parent, String baseName, String fields) {
			String head = VarValue.GetHead(fields);
			String tail = VarValue.GetTail(fields);
			variable.Found = false;
			if (head == "httpheader3") {
				if (!VarValue.FieldIsOptimized(parent, baseName + ".HTTPHeader3")) variable.Value = ssSTHTTPHeader3; else variable.Optimized = true;
				variable.SetFieldName("httpheader3");
			}
			if (variable.Found && tail != null) variable.EvaluateFields(this, head, tail);
		}

		public bool ChangedAttributeGet(GlobalObjectKey key) {
			throw new Exception("Method not Supported");
		}

		public bool OptimizedAttributeGet(GlobalObjectKey key) {
			throw new Exception("Method not Supported");
		}

		public object AttributeGet(GlobalObjectKey key) {
			if (key == IdHTTPHeader3) {
				return ssSTHTTPHeader3;
			} else {
				throw new Exception("Invalid key");
			}
		}
		public void FillFromOther(IRecord other) {
			if (other == null) return;
			ssSTHTTPHeader3.FillFromOther((IRecord) other.AttributeGet(IdHTTPHeader3));
		}
	} // RCHTTPHeader3Record
	/// <summary>
	/// RecordList type <code>RLHTTPHeader3RecordList</code> that represents a record list of
	///  <code>HTTPHeader3</code>
	/// </summary>
	[Serializable()]
	public partial class RLHTTPHeader3RecordList: GenericRecordList<RCHTTPHeader3Record>, IEnumerable, IEnumerator, ISerializable {
		public static void EnsureInitialized() {}

		protected override RCHTTPHeader3Record GetElementDefaultValue() {
			return new RCHTTPHeader3Record("");
		}

		public T[] ToArray<T>(Func<RCHTTPHeader3Record, T> converter) {
			return ToArray(this, converter);
		}

		public static T[] ToArray<T>(RLHTTPHeader3RecordList recordlist, Func<RCHTTPHeader3Record, T> converter) {
			return InnerToArray(recordlist, converter);
		}
		public static implicit operator RLHTTPHeader3RecordList(RCHTTPHeader3Record[] array) {
			RLHTTPHeader3RecordList result = new RLHTTPHeader3RecordList();
			result.InnerFromArray(array);
			return result;
		}

		public static RLHTTPHeader3RecordList ToList<T>(T[] array, Func <T, RCHTTPHeader3Record> converter) {
			RLHTTPHeader3RecordList result = new RLHTTPHeader3RecordList();
			result.InnerFromArray(array, converter);
			return result;
		}

		public static RLHTTPHeader3RecordList FromRestList<T>(RestList<T> restList, Func <T, RCHTTPHeader3Record> converter) {
			RLHTTPHeader3RecordList result = new RLHTTPHeader3RecordList();
			result.InnerFromRestList(restList, converter);
			return result;
		}
		/// <summary>
		/// Default Constructor
		/// </summary>
		public RLHTTPHeader3RecordList(): base() {
		}

		/// <summary>
		/// Constructor with transaction parameter
		/// </summary>
		/// <param name="trans"> IDbTransaction Parameter</param>
		[Obsolete("Use the Default Constructor and set the Transaction afterwards.")]
		public RLHTTPHeader3RecordList(IDbTransaction trans): base(trans) {
		}

		/// <summary>
		/// Constructor with transaction parameter and alternate read method
		/// </summary>
		/// <param name="trans"> IDbTransaction Parameter</param>
		/// <param name="alternateReadDBMethod"> Alternate Read Method</param>
		[Obsolete("Use the Default Constructor and set the Transaction afterwards.")]
		public RLHTTPHeader3RecordList(IDbTransaction trans, ReadDBMethodDelegate alternateReadDBMethod): this(trans) {
			this.alternateReadDBMethod = alternateReadDBMethod;
		}

		/// <summary>
		/// Constructor declaration for serialization
		/// </summary>
		/// <param name="info"> SerializationInfo</param>
		/// <param name="context"> StreamingContext</param>
		public RLHTTPHeader3RecordList(SerializationInfo info, StreamingContext context): base(info, context) {
		}

		public override BitArray[] GetDefaultOptimizedValues() {
			BitArray[] def = new BitArray[1];
			def[0] = null;
			return def;
		}
		/// <summary>
		/// Create as new list
		/// </summary>
		/// <returns>The new record list</returns>
		protected override OSList<RCHTTPHeader3Record> NewList() {
			return new RLHTTPHeader3RecordList();
		}


	} // RLHTTPHeader3RecordList
	/// <summary>
	/// RecordList type <code>RLHTTPHeader3List</code> that represents a record list of
	///  <code>HTTPHeader3</code>
	/// </summary>
	[Serializable()]
	public partial class RLHTTPHeader3List: GenericRecordList<STHTTPHeader3Structure>, IEnumerable, IEnumerator, ISerializable {
		public static void EnsureInitialized() {}

		protected override STHTTPHeader3Structure GetElementDefaultValue() {
			return new STHTTPHeader3Structure("");
		}

		public T[] ToArray<T>(Func<STHTTPHeader3Structure, T> converter) {
			return ToArray(this, converter);
		}

		public static T[] ToArray<T>(RLHTTPHeader3List recordlist, Func<STHTTPHeader3Structure, T> converter) {
			return InnerToArray(recordlist, converter);
		}
		public static implicit operator RLHTTPHeader3List(STHTTPHeader3Structure[] array) {
			RLHTTPHeader3List result = new RLHTTPHeader3List();
			result.InnerFromArray(array);
			return result;
		}

		public static RLHTTPHeader3List ToList<T>(T[] array, Func <T, STHTTPHeader3Structure> converter) {
			RLHTTPHeader3List result = new RLHTTPHeader3List();
			result.InnerFromArray(array, converter);
			return result;
		}

		public static RLHTTPHeader3List FromRestList<T>(RestList<T> restList, Func <T, STHTTPHeader3Structure> converter) {
			RLHTTPHeader3List result = new RLHTTPHeader3List();
			result.InnerFromRestList(restList, converter);
			return result;
		}
		/// <summary>
		/// Default Constructor
		/// </summary>
		public RLHTTPHeader3List(): base() {
		}

		/// <summary>
		/// Constructor with transaction parameter
		/// </summary>
		/// <param name="trans"> IDbTransaction Parameter</param>
		[Obsolete("Use the Default Constructor and set the Transaction afterwards.")]
		public RLHTTPHeader3List(IDbTransaction trans): base(trans) {
		}

		/// <summary>
		/// Constructor with transaction parameter and alternate read method
		/// </summary>
		/// <param name="trans"> IDbTransaction Parameter</param>
		/// <param name="alternateReadDBMethod"> Alternate Read Method</param>
		[Obsolete("Use the Default Constructor and set the Transaction afterwards.")]
		public RLHTTPHeader3List(IDbTransaction trans, ReadDBMethodDelegate alternateReadDBMethod): this(trans) {
			this.alternateReadDBMethod = alternateReadDBMethod;
		}

		/// <summary>
		/// Constructor declaration for serialization
		/// </summary>
		/// <param name="info"> SerializationInfo</param>
		/// <param name="context"> StreamingContext</param>
		public RLHTTPHeader3List(SerializationInfo info, StreamingContext context): base(info, context) {
		}

		public override BitArray[] GetDefaultOptimizedValues() {
			BitArray[] def = new BitArray[0];
			return def;
		}
		/// <summary>
		/// Create as new list
		/// </summary>
		/// <returns>The new record list</returns>
		protected override OSList<STHTTPHeader3Structure> NewList() {
			return new RLHTTPHeader3List();
		}


	} // RLHTTPHeader3List
}

namespace ssBIFY {
	[XmlType("HTTPHeader3")]
	public class WORCHTTPHeader3Record {
		[System.Xml.Serialization.XmlElement("Name")]
		public string varWSName;

		[System.Xml.Serialization.XmlElement("Value")]
		public string varWSValue;

		public WORCHTTPHeader3Record() {
			varWSName = (string) "";
			varWSValue = (string) "";
		}

		public WORCHTTPHeader3Record(STHTTPHeader3Structure r) {
			varWSName = BaseAppUtils.RemoveControlChars(r.ssName);
			varWSValue = BaseAppUtils.RemoveControlChars(r.ssValue);
		}

		public static RLHTTPHeader3List ToRecordList(WORCHTTPHeader3Record[] array) {
			RLHTTPHeader3List rl = new RLHTTPHeader3List();
			if (array != null) {
				foreach(WORCHTTPHeader3Record val in array) {
					rl.Append(val);
				}
			}
			return rl;
		}

		public static WORCHTTPHeader3Record[] FromRecordList(RLHTTPHeader3List rl) {
			WORCHTTPHeader3Record[] array = new WORCHTTPHeader3Record[rl == null ? 0: rl.Length];
			for (int i = 0; i < array.Length; i++) {
				array[i] = rl.Data[i];
			}
			return array;
		}
	}
}

namespace ssBIFY {
	partial struct RCHTTPHeader3Record {
		public static implicit operator WORCHTTPHeader3Record(RCHTTPHeader3Record r) {
			return new WORCHTTPHeader3Record(r.ssSTHTTPHeader3);
		}

		public static implicit operator RCHTTPHeader3Record(WORCHTTPHeader3Record w) {
			RCHTTPHeader3Record r = new RCHTTPHeader3Record("");
			if (w != null) {
				r.ssSTHTTPHeader3 = w;
			}
			return r;
		}

	}

	partial struct STHTTPHeader3Structure {
		public static implicit operator WORCHTTPHeader3Record(STHTTPHeader3Structure r) {
			return new WORCHTTPHeader3Record(r);
		}

		public static implicit operator STHTTPHeader3Structure(WORCHTTPHeader3Record w) {
			STHTTPHeader3Structure r = new STHTTPHeader3Structure("");
			if (w != null) {
				r.ssName = ((string) w.varWSName ?? "");
				r.ssValue = ((string) w.varWSValue ?? "");
			}
			return r;
		}

	}
}


namespace ssBIFY {
	[Serializable()]
	public partial class WORLHTTPHeader3RecordList {
		public WORCHTTPHeader3Record[] Array;


		public WORLHTTPHeader3RecordList(WORCHTTPHeader3Record[] r) {
			if (r == null)
			Array = new WORCHTTPHeader3Record[0];
			else
			Array = r;
		}
		public WORLHTTPHeader3RecordList() {
			Array = new WORCHTTPHeader3Record[0];
		}

		public WORLHTTPHeader3RecordList(RLHTTPHeader3RecordList rl) {
			rl=(RLHTTPHeader3RecordList) rl.Duplicate();
			Array = new WORCHTTPHeader3Record[rl.Length];
			while (!rl.Eof) {
				Array[rl.CurrentRowNumber] = new WORCHTTPHeader3Record(rl.CurrentRec);
				rl.Advance();
			}
		}

	}
}

namespace ssBIFY {
	partial class RLHTTPHeader3RecordList {
		public static implicit operator RLHTTPHeader3RecordList(WORCHTTPHeader3Record[] array) {
			RLHTTPHeader3RecordList rl = new RLHTTPHeader3RecordList();
			if (array != null) {
				foreach(WORCHTTPHeader3Record val in array) {
					rl.Append((RCHTTPHeader3Record) val);
				}
			}
			return rl;
		}
		public static implicit operator WORCHTTPHeader3Record[](RLHTTPHeader3RecordList rl) {
			WORCHTTPHeader3Record[] array = new WORCHTTPHeader3Record[rl == null ? 0: rl.Length];
			for (int i = 0; i < array.Length; i++) {
				array[i] = (RCHTTPHeader3Record) rl.Data[i];
			}
			return array;
		}
	}
}

namespace ssBIFY {
	partial class WORLHTTPHeader3RecordList {
		public static implicit operator RLHTTPHeader3RecordList(WORLHTTPHeader3RecordList w) {
			return w.Array;
		}
		public static implicit operator WORLHTTPHeader3RecordList(RLHTTPHeader3RecordList rl) {
			return new WORLHTTPHeader3RecordList(rl);
		}
		public static implicit operator WORCHTTPHeader3Record[](WORLHTTPHeader3RecordList w) {
			if (w != null) {
				return w.Array;
			}
			return null;
		}
		public static implicit operator WORLHTTPHeader3RecordList(WORCHTTPHeader3Record[] array) {
			return new WORLHTTPHeader3RecordList(array);
		}
	}
}

namespace ssBIFY {
	[Serializable()]
	public partial class WORLHTTPHeader3List {
		public WORCHTTPHeader3Record[] Array;


		public WORLHTTPHeader3List(WORCHTTPHeader3Record[] r) {
			if (r == null)
			Array = new WORCHTTPHeader3Record[0];
			else
			Array = r;
		}
		public WORLHTTPHeader3List() {
			Array = new WORCHTTPHeader3Record[0];
		}

		public WORLHTTPHeader3List(RLHTTPHeader3List rl) {
			rl=(RLHTTPHeader3List) rl.Duplicate();
			Array = new WORCHTTPHeader3Record[rl.Length];
			while (!rl.Eof) {
				Array[rl.CurrentRowNumber] = rl.CurrentRec.Duplicate();
				rl.Advance();
			}
		}

	}
}

namespace ssBIFY {
	partial class RLHTTPHeader3List {
		public static implicit operator RLHTTPHeader3List(WORCHTTPHeader3Record[] array) {
			RLHTTPHeader3List rl = new RLHTTPHeader3List();
			if (array != null) {
				foreach(WORCHTTPHeader3Record val in array) {
					rl.Append((STHTTPHeader3Structure) val);
				}
			}
			return rl;
		}
		public static implicit operator WORCHTTPHeader3Record[](RLHTTPHeader3List rl) {
			WORCHTTPHeader3Record[] array = new WORCHTTPHeader3Record[rl == null ? 0: rl.Length];
			for (int i = 0; i < array.Length; i++) {
				array[i] = (STHTTPHeader3Structure) rl.Data[i];
			}
			return array;
		}
	}
}

namespace ssBIFY {
	partial class WORLHTTPHeader3List {
		public static implicit operator RLHTTPHeader3List(WORLHTTPHeader3List w) {
			return w.Array;
		}
		public static implicit operator WORLHTTPHeader3List(RLHTTPHeader3List rl) {
			return new WORLHTTPHeader3List(rl);
		}
		public static implicit operator WORCHTTPHeader3Record[](WORLHTTPHeader3List w) {
			if (w != null) {
				return w.Array;
			}
			return null;
		}
		public static implicit operator WORLHTTPHeader3List(WORCHTTPHeader3Record[] array) {
			return new WORLHTTPHeader3List(array);
		}
	}
}

