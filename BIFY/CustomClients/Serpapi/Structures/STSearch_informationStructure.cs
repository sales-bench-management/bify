﻿/* 
 This source code (the "Generated Software") is generated by the OutSystems Platform 
 and is licensed by OutSystems (http://www.outsystems.com) to You solely for testing and evaluation 
 purposes, unless You and OutSystems have executed a specific agreement covering the use terms and 
 conditions of the Generated Software, in which case such agreement shall apply. 
*/

using System;
using System.Data;
using System.Runtime.Serialization;
using System.Reflection;
using System.Xml;
using OutSystems.ObjectKeys;
using OutSystems.RuntimeCommon;
using OutSystems.HubEdition.RuntimePlatform;
using OutSystems.HubEdition.RuntimePlatform.Db;
using OutSystems.Internal.Db;
using System.Xml.Serialization;
using System.Collections;
using OutSystems.HubEdition.RuntimePlatform.NewRuntime;

namespace ssBIFY {
	/// <summary>
	/// Structure <code>STSearch_informationStructure</code> that represents the Service Studio structure
	///  <code>Search_information</code> <p> Description: </p>
	/// </summary>
	[Serializable()]
	public partial struct STSearch_informationStructure: ISerializable, ITypedRecord<STSearch_informationStructure>, ISimpleRecord {
		internal static readonly GlobalObjectKey IdQuery_displayed = GlobalObjectKey.Parse("1WMl+O7_wkOQDYQl1Ck5zQ*6y3pUi9R3kauk1ZT7gym+w");

		public static void EnsureInitialized() {}
		[System.Xml.Serialization.XmlElement("Query_displayed")]
		public string ssQuery_displayed;


		public BitArray OptimizedAttributes;

		public STSearch_informationStructure(params string[] dummy) {
			OptimizedAttributes = null;
			ssQuery_displayed = "";
		}

		public BitArray[] GetDefaultOptimizedValues() {
			BitArray[] all = new BitArray[0];
			return all;
		}

		public BitArray[] AllOptimizedAttributes {
			set {
				if (value == null) {
				} else {
				}
			}
			get {
				BitArray[] all = new BitArray[0];
				return all;
			}
		}

		/// <summary>
		/// Read a record from database
		/// </summary>
		/// <param name="r"> Data base reader</param>
		/// <param name="index"> index</param>
		public void Read(IDataReader r, ref int index) {
			ssQuery_displayed = r.ReadText(index++, "Search_information.Query_displayed", "");
		}
		/// <summary>
		/// Read from database
		/// </summary>
		/// <param name="r"> Data reader</param>
		public void ReadDB(IDataReader r) {
			int index = 0;
			Read(r, ref index);
		}

		/// <summary>
		/// Read from record
		/// </summary>
		/// <param name="r"> Record</param>
		public void ReadIM(STSearch_informationStructure r) {
			this = r;
		}


		public static bool operator == (STSearch_informationStructure a, STSearch_informationStructure b) {
			if (a.ssQuery_displayed != b.ssQuery_displayed) return false;
			return true;
		}

		public static bool operator != (STSearch_informationStructure a, STSearch_informationStructure b) {
			return !(a==b);
		}

		public override bool Equals(object o) {
			if (o.GetType() != typeof(STSearch_informationStructure)) return false;
			return (this == (STSearch_informationStructure) o);
		}

		public override int GetHashCode() {
			try {
				return base.GetHashCode()
				^ ssQuery_displayed.GetHashCode()
				;
			} catch {
				return base.GetHashCode();
			}
		}

		public void GetObjectData(SerializationInfo info, StreamingContext context) {
			Type objInfo = this.GetType();
			FieldInfo[] fields;
			fields = objInfo.GetFields(BindingFlags.NonPublic | BindingFlags.Instance | BindingFlags.Public);
			for (int i = 0; i < fields.Length; i++)
			if (fields[i] .FieldType.IsSerializable)
			info.AddValue(fields[i] .Name, fields[i] .GetValue(this));
		}

		public STSearch_informationStructure(SerializationInfo info, StreamingContext context) {
			OptimizedAttributes = null;
			ssQuery_displayed = "";
			Type objInfo = this.GetType();
			FieldInfo fieldInfo = null;
			fieldInfo = objInfo.GetField("ssQuery_displayed", BindingFlags.NonPublic | BindingFlags.Instance | BindingFlags.Public);
			if (fieldInfo == null) {
				throw new Exception("The field named 'ssQuery_displayed' was not found.");
			}
			if (fieldInfo.FieldType.IsSerializable) {
				ssQuery_displayed = (string) info.GetValue(fieldInfo.Name, fieldInfo.FieldType);
			}
		}

		public void RecursiveReset() {
		}

		public void InternalRecursiveSave() {
		}


		public STSearch_informationStructure Duplicate() {
			STSearch_informationStructure t;
			t.ssQuery_displayed = this.ssQuery_displayed;
			t.OptimizedAttributes = null;
			return t;
		}

		IRecord IRecord.Duplicate() {
			return Duplicate();
		}

		public void ToXml(Object parent, System.Xml.XmlElement baseElem, String fieldName, int detailLevel) {
			System.Xml.XmlElement recordElem = VarValue.AppendChild(baseElem, "Structure");
			if (fieldName != null) {
				VarValue.AppendAttribute(recordElem, "debug.field", fieldName);
				fieldName = fieldName.ToLowerInvariant();
			}
			if (detailLevel > 0) {
				if (!VarValue.FieldIsOptimized(parent, fieldName + ".Query_displayed")) VarValue.AppendAttribute(recordElem, "Query_displayed", ssQuery_displayed, detailLevel, TypeKind.Text); else VarValue.AppendOptimizedAttribute(recordElem, "Query_displayed");
			} else {
				VarValue.AppendDeferredEvaluationElement(recordElem);
			}
		}

		public void EvaluateFields(VarValue variable, Object parent, String baseName, String fields) {
			String head = VarValue.GetHead(fields);
			String tail = VarValue.GetTail(fields);
			variable.Found = false;
			if (head == "query_displayed") {
				if (!VarValue.FieldIsOptimized(parent, baseName + ".Query_displayed")) variable.Value = ssQuery_displayed; else variable.Optimized = true;
			}
			if (variable.Found && tail != null) variable.EvaluateFields(this, head, tail);
		}

		public bool ChangedAttributeGet(GlobalObjectKey key) {
			throw new Exception("Method not Supported");
		}

		public bool OptimizedAttributeGet(GlobalObjectKey key) {
			throw new Exception("Method not Supported");
		}

		public object AttributeGet(GlobalObjectKey key) {
			if (key == IdQuery_displayed) {
				return ssQuery_displayed;
			} else {
				throw new Exception("Invalid key");
			}
		}
		public void FillFromOther(IRecord other) {
			if (other == null) return;
			ssQuery_displayed = (string) other.AttributeGet(IdQuery_displayed);
		}
	} // STSearch_informationStructure
	/// <summary>
	/// Structure <code>RCSearch_informationRecord</code>
	/// </summary>
	[Serializable()]
	public partial struct RCSearch_informationRecord: ISerializable, ITypedRecord<RCSearch_informationRecord> {
		internal static readonly GlobalObjectKey IdSearch_information = GlobalObjectKey.Parse("2UmDmepsh0WSfJ_D1JexCA*zQLAQFqakqp3fnfcKWTtOA");

		public static void EnsureInitialized() {}
		[System.Xml.Serialization.XmlElement("Search_information")]
		public STSearch_informationStructure ssSTSearch_information;


		public static implicit operator STSearch_informationStructure(RCSearch_informationRecord r) {
			return r.ssSTSearch_information;
		}

		public static implicit operator RCSearch_informationRecord(STSearch_informationStructure r) {
			RCSearch_informationRecord res = new RCSearch_informationRecord(null);
			res.ssSTSearch_information = r;
			return res;
		}

		public BitArray OptimizedAttributes;

		public RCSearch_informationRecord(params string[] dummy) {
			OptimizedAttributes = null;
			ssSTSearch_information = new STSearch_informationStructure(null);
		}

		public BitArray[] GetDefaultOptimizedValues() {
			BitArray[] all = new BitArray[1];
			all[0] = null;
			return all;
		}

		public BitArray[] AllOptimizedAttributes {
			set {
				if (value == null) {
				} else {
					ssSTSearch_information.OptimizedAttributes = value[0];
				}
			}
			get {
				BitArray[] all = new BitArray[1];
				all[0] = null;
				return all;
			}
		}

		/// <summary>
		/// Read a record from database
		/// </summary>
		/// <param name="r"> Data base reader</param>
		/// <param name="index"> index</param>
		public void Read(IDataReader r, ref int index) {
			ssSTSearch_information.Read(r, ref index);
		}
		/// <summary>
		/// Read from database
		/// </summary>
		/// <param name="r"> Data reader</param>
		public void ReadDB(IDataReader r) {
			int index = 0;
			Read(r, ref index);
		}

		/// <summary>
		/// Read from record
		/// </summary>
		/// <param name="r"> Record</param>
		public void ReadIM(RCSearch_informationRecord r) {
			this = r;
		}


		public static bool operator == (RCSearch_informationRecord a, RCSearch_informationRecord b) {
			if (a.ssSTSearch_information != b.ssSTSearch_information) return false;
			return true;
		}

		public static bool operator != (RCSearch_informationRecord a, RCSearch_informationRecord b) {
			return !(a==b);
		}

		public override bool Equals(object o) {
			if (o.GetType() != typeof(RCSearch_informationRecord)) return false;
			return (this == (RCSearch_informationRecord) o);
		}

		public override int GetHashCode() {
			try {
				return base.GetHashCode()
				^ ssSTSearch_information.GetHashCode()
				;
			} catch {
				return base.GetHashCode();
			}
		}

		public void GetObjectData(SerializationInfo info, StreamingContext context) {
			Type objInfo = this.GetType();
			FieldInfo[] fields;
			fields = objInfo.GetFields(BindingFlags.NonPublic | BindingFlags.Instance | BindingFlags.Public);
			for (int i = 0; i < fields.Length; i++)
			if (fields[i] .FieldType.IsSerializable)
			info.AddValue(fields[i] .Name, fields[i] .GetValue(this));
		}

		public RCSearch_informationRecord(SerializationInfo info, StreamingContext context) {
			OptimizedAttributes = null;
			ssSTSearch_information = new STSearch_informationStructure(null);
			Type objInfo = this.GetType();
			FieldInfo fieldInfo = null;
			fieldInfo = objInfo.GetField("ssSTSearch_information", BindingFlags.NonPublic | BindingFlags.Instance | BindingFlags.Public);
			if (fieldInfo == null) {
				throw new Exception("The field named 'ssSTSearch_information' was not found.");
			}
			if (fieldInfo.FieldType.IsSerializable) {
				ssSTSearch_information = (STSearch_informationStructure) info.GetValue(fieldInfo.Name, fieldInfo.FieldType);
			}
		}

		public void RecursiveReset() {
			ssSTSearch_information.RecursiveReset();
		}

		public void InternalRecursiveSave() {
			ssSTSearch_information.InternalRecursiveSave();
		}


		public RCSearch_informationRecord Duplicate() {
			RCSearch_informationRecord t;
			t.ssSTSearch_information = (STSearch_informationStructure) this.ssSTSearch_information.Duplicate();
			t.OptimizedAttributes = null;
			return t;
		}

		IRecord IRecord.Duplicate() {
			return Duplicate();
		}

		public void ToXml(Object parent, System.Xml.XmlElement baseElem, String fieldName, int detailLevel) {
			System.Xml.XmlElement recordElem = VarValue.AppendChild(baseElem, "Record");
			if (fieldName != null) {
				VarValue.AppendAttribute(recordElem, "debug.field", fieldName);
			}
			if (detailLevel > 0) {
				ssSTSearch_information.ToXml(this, recordElem, "Search_information", detailLevel - 1);
			} else {
				VarValue.AppendDeferredEvaluationElement(recordElem);
			}
		}

		public void EvaluateFields(VarValue variable, Object parent, String baseName, String fields) {
			String head = VarValue.GetHead(fields);
			String tail = VarValue.GetTail(fields);
			variable.Found = false;
			if (head == "search_information") {
				if (!VarValue.FieldIsOptimized(parent, baseName + ".Search_information")) variable.Value = ssSTSearch_information; else variable.Optimized = true;
				variable.SetFieldName("search_information");
			}
			if (variable.Found && tail != null) variable.EvaluateFields(this, head, tail);
		}

		public bool ChangedAttributeGet(GlobalObjectKey key) {
			throw new Exception("Method not Supported");
		}

		public bool OptimizedAttributeGet(GlobalObjectKey key) {
			throw new Exception("Method not Supported");
		}

		public object AttributeGet(GlobalObjectKey key) {
			if (key == IdSearch_information) {
				return ssSTSearch_information;
			} else {
				throw new Exception("Invalid key");
			}
		}
		public void FillFromOther(IRecord other) {
			if (other == null) return;
			ssSTSearch_information.FillFromOther((IRecord) other.AttributeGet(IdSearch_information));
		}
	} // RCSearch_informationRecord
	/// <summary>
	/// RecordList type <code>RLSearch_informationRecordList</code> that represents a record list of
	///  <code>Search_information</code>
	/// </summary>
	[Serializable()]
	public partial class RLSearch_informationRecordList: GenericRecordList<RCSearch_informationRecord>, IEnumerable, IEnumerator, ISerializable {
		public static void EnsureInitialized() {}

		protected override RCSearch_informationRecord GetElementDefaultValue() {
			return new RCSearch_informationRecord("");
		}

		public T[] ToArray<T>(Func<RCSearch_informationRecord, T> converter) {
			return ToArray(this, converter);
		}

		public static T[] ToArray<T>(RLSearch_informationRecordList recordlist, Func<RCSearch_informationRecord, T> converter) {
			return InnerToArray(recordlist, converter);
		}
		public static implicit operator RLSearch_informationRecordList(RCSearch_informationRecord[] array) {
			RLSearch_informationRecordList result = new RLSearch_informationRecordList();
			result.InnerFromArray(array);
			return result;
		}

		public static RLSearch_informationRecordList ToList<T>(T[] array, Func <T, RCSearch_informationRecord> converter) {
			RLSearch_informationRecordList result = new RLSearch_informationRecordList();
			result.InnerFromArray(array, converter);
			return result;
		}

		public static RLSearch_informationRecordList FromRestList<T>(RestList<T> restList, Func <T, RCSearch_informationRecord> converter) {
			RLSearch_informationRecordList result = new RLSearch_informationRecordList();
			result.InnerFromRestList(restList, converter);
			return result;
		}
		/// <summary>
		/// Default Constructor
		/// </summary>
		public RLSearch_informationRecordList(): base() {
		}

		/// <summary>
		/// Constructor with transaction parameter
		/// </summary>
		/// <param name="trans"> IDbTransaction Parameter</param>
		[Obsolete("Use the Default Constructor and set the Transaction afterwards.")]
		public RLSearch_informationRecordList(IDbTransaction trans): base(trans) {
		}

		/// <summary>
		/// Constructor with transaction parameter and alternate read method
		/// </summary>
		/// <param name="trans"> IDbTransaction Parameter</param>
		/// <param name="alternateReadDBMethod"> Alternate Read Method</param>
		[Obsolete("Use the Default Constructor and set the Transaction afterwards.")]
		public RLSearch_informationRecordList(IDbTransaction trans, ReadDBMethodDelegate alternateReadDBMethod): this(trans) {
			this.alternateReadDBMethod = alternateReadDBMethod;
		}

		/// <summary>
		/// Constructor declaration for serialization
		/// </summary>
		/// <param name="info"> SerializationInfo</param>
		/// <param name="context"> StreamingContext</param>
		public RLSearch_informationRecordList(SerializationInfo info, StreamingContext context): base(info, context) {
		}

		public override BitArray[] GetDefaultOptimizedValues() {
			BitArray[] def = new BitArray[1];
			def[0] = null;
			return def;
		}
		/// <summary>
		/// Create as new list
		/// </summary>
		/// <returns>The new record list</returns>
		protected override OSList<RCSearch_informationRecord> NewList() {
			return new RLSearch_informationRecordList();
		}


	} // RLSearch_informationRecordList
	/// <summary>
	/// RecordList type <code>RLSearch_informationList</code> that represents a record list of
	///  <code>Search_information</code>
	/// </summary>
	[Serializable()]
	public partial class RLSearch_informationList: GenericRecordList<STSearch_informationStructure>, IEnumerable, IEnumerator, ISerializable {
		public static void EnsureInitialized() {}

		protected override STSearch_informationStructure GetElementDefaultValue() {
			return new STSearch_informationStructure("");
		}

		public T[] ToArray<T>(Func<STSearch_informationStructure, T> converter) {
			return ToArray(this, converter);
		}

		public static T[] ToArray<T>(RLSearch_informationList recordlist, Func<STSearch_informationStructure, T> converter) {
			return InnerToArray(recordlist, converter);
		}
		public static implicit operator RLSearch_informationList(STSearch_informationStructure[] array) {
			RLSearch_informationList result = new RLSearch_informationList();
			result.InnerFromArray(array);
			return result;
		}

		public static RLSearch_informationList ToList<T>(T[] array, Func <T, STSearch_informationStructure> converter) {
			RLSearch_informationList result = new RLSearch_informationList();
			result.InnerFromArray(array, converter);
			return result;
		}

		public static RLSearch_informationList FromRestList<T>(RestList<T> restList, Func <T, STSearch_informationStructure> converter) {
			RLSearch_informationList result = new RLSearch_informationList();
			result.InnerFromRestList(restList, converter);
			return result;
		}
		/// <summary>
		/// Default Constructor
		/// </summary>
		public RLSearch_informationList(): base() {
		}

		/// <summary>
		/// Constructor with transaction parameter
		/// </summary>
		/// <param name="trans"> IDbTransaction Parameter</param>
		[Obsolete("Use the Default Constructor and set the Transaction afterwards.")]
		public RLSearch_informationList(IDbTransaction trans): base(trans) {
		}

		/// <summary>
		/// Constructor with transaction parameter and alternate read method
		/// </summary>
		/// <param name="trans"> IDbTransaction Parameter</param>
		/// <param name="alternateReadDBMethod"> Alternate Read Method</param>
		[Obsolete("Use the Default Constructor and set the Transaction afterwards.")]
		public RLSearch_informationList(IDbTransaction trans, ReadDBMethodDelegate alternateReadDBMethod): this(trans) {
			this.alternateReadDBMethod = alternateReadDBMethod;
		}

		/// <summary>
		/// Constructor declaration for serialization
		/// </summary>
		/// <param name="info"> SerializationInfo</param>
		/// <param name="context"> StreamingContext</param>
		public RLSearch_informationList(SerializationInfo info, StreamingContext context): base(info, context) {
		}

		public override BitArray[] GetDefaultOptimizedValues() {
			BitArray[] def = new BitArray[0];
			return def;
		}
		/// <summary>
		/// Create as new list
		/// </summary>
		/// <returns>The new record list</returns>
		protected override OSList<STSearch_informationStructure> NewList() {
			return new RLSearch_informationList();
		}


	} // RLSearch_informationList
}

namespace ssBIFY {
	[XmlType("Search_information")]
	public class WORCSearch_informationRecord {
		[System.Xml.Serialization.XmlElement("Query_displayed")]
		public string varWSQuery_displayed;

		public WORCSearch_informationRecord() {
			varWSQuery_displayed = (string) "";
		}

		public WORCSearch_informationRecord(STSearch_informationStructure r) {
			varWSQuery_displayed = BaseAppUtils.RemoveControlChars(r.ssQuery_displayed);
		}

		public static RLSearch_informationList ToRecordList(WORCSearch_informationRecord[] array) {
			RLSearch_informationList rl = new RLSearch_informationList();
			if (array != null) {
				foreach(WORCSearch_informationRecord val in array) {
					rl.Append(val);
				}
			}
			return rl;
		}

		public static WORCSearch_informationRecord[] FromRecordList(RLSearch_informationList rl) {
			WORCSearch_informationRecord[] array = new WORCSearch_informationRecord[rl == null ? 0: rl.Length];
			for (int i = 0; i < array.Length; i++) {
				array[i] = rl.Data[i];
			}
			return array;
		}
	}
}

namespace ssBIFY {
	partial struct RCSearch_informationRecord {
		public static implicit operator WORCSearch_informationRecord(RCSearch_informationRecord r) {
			return new WORCSearch_informationRecord(r.ssSTSearch_information);
		}

		public static implicit operator RCSearch_informationRecord(WORCSearch_informationRecord w) {
			RCSearch_informationRecord r = new RCSearch_informationRecord("");
			if (w != null) {
				r.ssSTSearch_information = w;
			}
			return r;
		}

	}

	partial struct STSearch_informationStructure {
		public static implicit operator WORCSearch_informationRecord(STSearch_informationStructure r) {
			return new WORCSearch_informationRecord(r);
		}

		public static implicit operator STSearch_informationStructure(WORCSearch_informationRecord w) {
			STSearch_informationStructure r = new STSearch_informationStructure("");
			if (w != null) {
				r.ssQuery_displayed = ((string) w.varWSQuery_displayed ?? "");
			}
			return r;
		}

	}
}


namespace ssBIFY {
	[Serializable()]
	public partial class WORLSearch_informationRecordList {
		public WORCSearch_informationRecord[] Array;


		public WORLSearch_informationRecordList(WORCSearch_informationRecord[] r) {
			if (r == null)
			Array = new WORCSearch_informationRecord[0];
			else
			Array = r;
		}
		public WORLSearch_informationRecordList() {
			Array = new WORCSearch_informationRecord[0];
		}

		public WORLSearch_informationRecordList(RLSearch_informationRecordList rl) {
			rl=(RLSearch_informationRecordList) rl.Duplicate();
			Array = new WORCSearch_informationRecord[rl.Length];
			while (!rl.Eof) {
				Array[rl.CurrentRowNumber] = new WORCSearch_informationRecord(rl.CurrentRec);
				rl.Advance();
			}
		}

	}
}

namespace ssBIFY {
	partial class RLSearch_informationRecordList {
		public static implicit operator RLSearch_informationRecordList(WORCSearch_informationRecord[] array) {
			RLSearch_informationRecordList rl = new RLSearch_informationRecordList();
			if (array != null) {
				foreach(WORCSearch_informationRecord val in array) {
					rl.Append((RCSearch_informationRecord) val);
				}
			}
			return rl;
		}
		public static implicit operator WORCSearch_informationRecord[](RLSearch_informationRecordList rl) {
			WORCSearch_informationRecord[] array = new WORCSearch_informationRecord[rl == null ? 0: rl.Length];
			for (int i = 0; i < array.Length; i++) {
				array[i] = (RCSearch_informationRecord) rl.Data[i];
			}
			return array;
		}
	}
}

namespace ssBIFY {
	partial class WORLSearch_informationRecordList {
		public static implicit operator RLSearch_informationRecordList(WORLSearch_informationRecordList w) {
			return w.Array;
		}
		public static implicit operator WORLSearch_informationRecordList(RLSearch_informationRecordList rl) {
			return new WORLSearch_informationRecordList(rl);
		}
		public static implicit operator WORCSearch_informationRecord[](WORLSearch_informationRecordList w) {
			if (w != null) {
				return w.Array;
			}
			return null;
		}
		public static implicit operator WORLSearch_informationRecordList(WORCSearch_informationRecord[] array) {
			return new WORLSearch_informationRecordList(array);
		}
	}
}

namespace ssBIFY {
	[Serializable()]
	public partial class WORLSearch_informationList {
		public WORCSearch_informationRecord[] Array;


		public WORLSearch_informationList(WORCSearch_informationRecord[] r) {
			if (r == null)
			Array = new WORCSearch_informationRecord[0];
			else
			Array = r;
		}
		public WORLSearch_informationList() {
			Array = new WORCSearch_informationRecord[0];
		}

		public WORLSearch_informationList(RLSearch_informationList rl) {
			rl=(RLSearch_informationList) rl.Duplicate();
			Array = new WORCSearch_informationRecord[rl.Length];
			while (!rl.Eof) {
				Array[rl.CurrentRowNumber] = rl.CurrentRec.Duplicate();
				rl.Advance();
			}
		}

	}
}

namespace ssBIFY {
	partial class RLSearch_informationList {
		public static implicit operator RLSearch_informationList(WORCSearch_informationRecord[] array) {
			RLSearch_informationList rl = new RLSearch_informationList();
			if (array != null) {
				foreach(WORCSearch_informationRecord val in array) {
					rl.Append((STSearch_informationStructure) val);
				}
			}
			return rl;
		}
		public static implicit operator WORCSearch_informationRecord[](RLSearch_informationList rl) {
			WORCSearch_informationRecord[] array = new WORCSearch_informationRecord[rl == null ? 0: rl.Length];
			for (int i = 0; i < array.Length; i++) {
				array[i] = (STSearch_informationStructure) rl.Data[i];
			}
			return array;
		}
	}
}

namespace ssBIFY {
	partial class WORLSearch_informationList {
		public static implicit operator RLSearch_informationList(WORLSearch_informationList w) {
			return w.Array;
		}
		public static implicit operator WORLSearch_informationList(RLSearch_informationList rl) {
			return new WORLSearch_informationList(rl);
		}
		public static implicit operator WORCSearch_informationRecord[](WORLSearch_informationList w) {
			if (w != null) {
				return w.Array;
			}
			return null;
		}
		public static implicit operator WORLSearch_informationList(WORCSearch_informationRecord[] array) {
			return new WORLSearch_informationList(array);
		}
	}
}


namespace ssBIFY {
	using System.IO;
	using System.Net;
	using System.Text;
	using System.Data;
	using System.Collections.Generic;
	using OutSystems.RESTService;
	using Newtonsoft.Json;
	public static partial class CcSerpapi {
		public class RESTSearch_information {
			[JsonProperty("query_displayed")]
			public string restQuery_displayed;

			public RESTSearch_information() {}

			public RESTSearch_information(STSearch_informationStructure s) {
				restQuery_displayed = s.ssQuery_displayed == "" ? null: s.ssQuery_displayed;
			}

			public static STSearch_informationStructure ToStructure(RESTSearch_information obj) {
				STSearch_informationStructure s = new STSearch_informationStructure(null);
				if (obj != null) {
					s.ssQuery_displayed = obj.restQuery_displayed == null ? "": obj.restQuery_displayed;
				}
				return s;
			}

			public static RESTSearch_information FromStructure(STSearch_informationStructure s) {
				return new RESTSearch_information(s);
			}

		}
	}
}
