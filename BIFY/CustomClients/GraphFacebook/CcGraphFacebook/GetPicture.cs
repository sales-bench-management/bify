﻿/* 
 This source code (the "Generated Software") is generated by the OutSystems Platform 
 and is licensed by OutSystems (http://www.outsystems.com) to You solely for testing and evaluation 
 purposes, unless You and OutSystems have executed a specific agreement covering the use terms and 
 conditions of the Generated Software, in which case such agreement shall apply. 
*/

using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Reflection;
using System.Text.RegularExpressions;
using OutSystems.HubEdition.RuntimePlatform;
using OutSystems.HubEdition.RuntimePlatform.Log;
using OutSystems.HubEdition.RuntimePlatform.Db;
using OutSystems.HubEdition.RuntimePlatform.NewRuntime;
using OutSystems.Internal.Db;
using OutSystems.HubEdition.RuntimePlatform.Email;
using OutSystems.RuntimeCommon;
using OutSystems.RuntimeCommon.Caching;
using OutSystems.ObjectKeys;
using System.Resources;
using System.IO;
using System.Net;
using System.Text;
using System.Data;
using System.Globalization;
using OutSystems.RESTService;
using OutSystems.RESTService.Behaviors;
using OutSystems.REST;
using Newtonsoft.Json;

namespace ssBIFY {
	public static partial class CcGraphFacebook {
		/// <summary>
		/// Action: GetPicture
		/// </summary>

		public static void ActionGetPicture(HeContext heContext, ICcGraphFacebookCallbacks _callbacks, string inParamuserid, out STGetPictureResponseStructure outParamResponse) {
			outParamResponse = new STGetPictureResponseStructure(null);

			String __url = "";
			String __errorLogId = "";
			String __endpoint = null;
			String __message = null;
			String __detail = null;
			String __detailLabel = null;
			DateTime __startExecution = DateTime.Now;
			try {
				OutSystems.REST.Configuration config = OutSystems.REST.Configuration.GetClientConfiguration("b27b5e73-37fb-45ac-ae83-706a4f540559", 37, "f82563d5-ffee-43c2-900d-8425d42939cd", defaultUrlOverride: "https://graph.facebook.com");
				StringBuilder httpTrace = new StringBuilder();
				bool failedRequest = false;
				try {
					string baseURL = config.Url.TrimEnd('/');
					string relativeUrl = "" + HttpHelper.UrlEncode("") + "/" + HttpHelper.UrlEncode("v3.2") + "/" + HttpHelper.UrlEncode("" + inParamuserid + "") + "/" + HttpHelper.UrlEncode("picture") + "";
					string httpMethod = "GET";
					var headers = new Dictionary<string, RestPluginRuntimeUtils.NameValue>();
					headers.Add("User-Agent", new RestPluginRuntimeUtils.NameValue("User-Agent", "OutSystemsPlatform"));
					string username = config.Username;
					string password = config.Password;
					if (username != "" || password != "") {
						headers.Add("Authorization", new RestPluginRuntimeUtils.NameValue("Authorization", "Basic " + Convert.ToBase64String(Encoding.ASCII.GetBytes(username + ":" + password))));
					}
					var queryParams = new List<RestPluginRuntimeUtils.NameValue>();
					queryParams.Add(new RestPluginRuntimeUtils.NameValue("redirect", "false"));
					queryParams.Add(new RestPluginRuntimeUtils.NameValue("height", "300"));
					String url = baseURL + relativeUrl;
					for (int i = 0; i < queryParams.Count; i++) {
						url += (i == 0) ? "?": "&";
						url += HttpHelper.UrlQueryEncode(queryParams[i] .Name) + "=" + HttpHelper.UrlQueryEncode(queryParams[i] .Value);
					}
					__endpoint = baseURL + relativeUrl;
					var request = (HttpWebRequest) HttpWebRequest.Create(url);
					request.Method = httpMethod;
					request.Timeout = 100000;
					foreach(var header in headers.Values) {
						HttpHelper.SetRequestHeader(request, header.Name, header.Value);
					}
					if (request.CookieContainer == null && request.Headers[HttpRequestHeader.Cookie] == null) {
						request.CookieContainer = new CookieContainer();
					}
					if (config.Trace) {
						httpTrace.AppendLine(request.Method + " " + request.RequestUri.ToString() + " HTTP/" + request.ProtocolVersion.ToString());
						foreach(var header in request.Headers.AllKeys) {
							httpTrace.AppendLine(header + ": " + request.Headers[header]);
						}
						if (request.ContentLength != -1) {
							httpTrace.AppendLine("Content-Length: " + request.ContentLength);
						}
						httpTrace.AppendLine();
						httpTrace.AppendLine();
					}

					HttpWebResponse response;

					try {
						response = (HttpWebResponse) request.GetResponse();
					} catch (WebException we) {
						response = (HttpWebResponse) we.Response;
						if (response == null) {
							throw;
						}
					}

					String responseText = String.Empty;
					byte[] responseArray = new byte[0];
					int statusCode = (int) response.StatusCode;
					String statusLine = statusCode + " " + response.StatusDescription;
					var headersAux = response.Headers;
					using(var responseContext = new OutSystems.Internal.REST.ResponseContext("GetPicture", response)) {
						responseText = responseContext.GetText();
						responseArray = responseContext.GetBinary();
						if (config.Trace) {
							httpTrace.AppendLine("HTTP/" + response.ProtocolVersion + " " + ((int) response.StatusCode) + " " + response.StatusDescription);
							foreach(var headerName in response.Headers.AllKeys) {
								httpTrace.AppendLine(headerName + ": " + response.Headers[headerName]);
							}
							httpTrace.AppendLine();
							httpTrace.Append(responseText);
						}

					}

					var responseHeaders = new List<RestPluginRuntimeUtils.NameValue>();
					foreach(var headerName in headersAux.AllKeys) {
						responseHeaders.Add(new RestPluginRuntimeUtils.NameValue(headerName, headersAux[headerName]));
					}

					if (statusCode < 200 || statusCode >= 400) {
						throw new Exception(statusLine);
					}

					var deserializationMessages = new List<String>();

					RESTGetPictureResponse responseHolder = null;

					try {
						responseHolder = JsonConvert.DeserializeObject<RESTGetPictureResponse>(responseText, new JsonSerializerSettings {
							DefaultValueHandling = DefaultValueHandling.Ignore, DateTimeZoneHandling = DateTimeZoneHandling.Local, Error = (sender, args) => {
								String message = "Parsing '" + args.ErrorContext.Path + "': " + args.ErrorContext.Error.Message;
								if (!deserializationMessages.Contains(message)) {
									deserializationMessages.Add(message);
								}
								args.ErrorContext.Handled = true;
							}
						}
						);
						if (responseHolder != null) {
							outParamResponse = RESTGetPictureResponse.ToStructure(responseHolder);
						}
					} catch (Exception e) {
						deserializationMessages.Add(e.Message);
					}

					if (deserializationMessages.Any()) {
						var detail = new StringBuilder();
						detail.Append("Failed to parse response of the method 'GetPicture' of the 'GraphFacebook' REST API:\n");
						for (int maxErrorMessagesToShow = 3, currentPosition = 0; maxErrorMessagesToShow > 0 && currentPosition < deserializationMessages.Count; --maxErrorMessagesToShow, ++currentPosition) {
							detail.Append(deserializationMessages[currentPosition]);
							detail.Append("\n");
						}

						throw new Exception(detail.ToString());
					}
				} catch {
					failedRequest = true;
					throw;
				} finally {
					if (config.TraceAll || (config.TraceErrors && failedRequest)) {
						__message = "";
						__detail = httpTrace.ToString();
						__detailLabel = "HTTP Trace";
					}
				}
			} catch (Exception __ex) {
				__errorLogId = ErrorLog.LogApplicationError(__ex, heContext, "REST (Consume)");
				throw __ex;
			} finally {
				DateTime __instant = DateTime.Now;
				TimeSpan __duration = __instant.Subtract(__startExecution);
				if (heContext.RequestTracer != null) {
					heContext.RequestTracer.RegisterConsumedIntegrationExecuted("GraphFacebook_GetPicture", "305ff453-60dc-4988-8cf9-c556bedb53e1",
					 "f82563d5-ffee-43c2-900d-8425d42939cd", "BIFY",
					 "REST (Consume)", (int) __duration.TotalMilliseconds, __instant,
					__endpoint); 
				}
				if (!String.IsNullOrEmpty(__errorLogId) ||
				(heContext.AppInfo != null && (!heContext.AppInfo.SelectiveLoggingEnabled || heContext.AppInfo.Properties.AllowLogging))) {
					String __integrationLogId = IntegrationLog.StaticWrite(
					heContext.AppInfo,
					__instant,
					(int) __duration.TotalMilliseconds,
					null,
					__endpoint,
					 "GraphFacebook.GetPicture",
					 "REST (Consume)",
					__errorLogId,
					false);
					if (!String.IsNullOrEmpty(__detail) ||
					!String.IsNullOrEmpty(__message)) {
						IntDetailLog.StaticWrite(__integrationLogId,
						__instant,
						(heContext.AppInfo != null && heContext.AppInfo.Tenant != null ? heContext.AppInfo.Tenant.Id: 0),
						__message,
						__detail,
						__detailLabel);
					}
				}
			}
		}
	}
}
