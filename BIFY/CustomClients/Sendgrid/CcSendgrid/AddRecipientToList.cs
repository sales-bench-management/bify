﻿/* 
 This source code (the "Generated Software") is generated by the OutSystems Platform 
 and is licensed by OutSystems (http://www.outsystems.com) to You solely for testing and evaluation 
 purposes, unless You and OutSystems have executed a specific agreement covering the use terms and 
 conditions of the Generated Software, in which case such agreement shall apply. 
*/

using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Reflection;
using System.Text.RegularExpressions;
using OutSystems.HubEdition.RuntimePlatform;
using OutSystems.HubEdition.RuntimePlatform.Log;
using OutSystems.HubEdition.RuntimePlatform.Db;
using OutSystems.HubEdition.RuntimePlatform.NewRuntime;
using OutSystems.Internal.Db;
using OutSystems.HubEdition.RuntimePlatform.Email;
using OutSystems.RuntimeCommon;
using OutSystems.RuntimeCommon.Caching;
using OutSystems.ObjectKeys;
using System.Resources;
using System.IO;
using System.Net;
using System.Text;
using System.Data;
using System.Globalization;
using OutSystems.RESTService;
using OutSystems.RESTService.Behaviors;
using OutSystems.REST;
using Newtonsoft.Json;

namespace ssBIFY {
	public static partial class CcSendgrid {
		/// <summary>
		/// Action: AddRecipientToList
		/// </summary>

		public static void ActionAddRecipientToList(HeContext heContext, ICcSendgridCallbacks _callbacks, string inParamlist_id, string inParamrecipient_id) {
			String __url = "";
			String __errorLogId = "";
			String __endpoint = null;
			String __message = null;
			String __detail = null;
			String __detailLabel = null;
			DateTime __startExecution = DateTime.Now;
			try {
				OutSystems.REST.Configuration config = OutSystems.REST.Configuration.GetClientConfiguration("0d9af1b6-9dcf-4728-9a66-98d91f58f42f", 37, "f82563d5-ffee-43c2-900d-8425d42939cd", defaultUrlOverride: "https://api.sendgrid.com");
				StringBuilder httpTrace = new StringBuilder();
				bool failedRequest = false;
				try {
					string baseURL = config.Url.TrimEnd('/');
					string relativeUrl = "" + HttpHelper.UrlEncode("") + "/" + HttpHelper.UrlEncode("v3") + "/" + HttpHelper.UrlEncode("contactdb") + "/" + HttpHelper.UrlEncode("lists") + "/" + HttpHelper.UrlEncode("" + inParamlist_id + "") + "/" + HttpHelper.UrlEncode("recipients") + "/" + HttpHelper.UrlEncode("" + inParamrecipient_id + "") + "";
					string httpMethod = "POST";
					var headers = new Dictionary<string, RestPluginRuntimeUtils.NameValue>();
					headers.Add("User-Agent", new RestPluginRuntimeUtils.NameValue("User-Agent", "OutSystemsPlatform"));
					string username = config.Username;
					string password = config.Password;
					if (username != "" || password != "") {
						headers.Add("Authorization", new RestPluginRuntimeUtils.NameValue("Authorization", "Basic " + Convert.ToBase64String(Encoding.ASCII.GetBytes(username + ":" + password))));
					}
					headers["Authorization"] = new RestPluginRuntimeUtils.NameValue("Authorization", _callbacks.EvaluateExtendedProperty(heContext, "Authorization"));
					headers["Content-Type"] = new RestPluginRuntimeUtils.NameValue("Content-Type", "application/json");
					var queryParams = new List<RestPluginRuntimeUtils.NameValue>();
					String url = baseURL + relativeUrl;
					for (int i = 0; i < queryParams.Count; i++) {
						url += (i == 0) ? "?": "&";
						url += HttpHelper.UrlQueryEncode(queryParams[i] .Name) + "=" + HttpHelper.UrlQueryEncode(queryParams[i] .Value);
					}
					__endpoint = baseURL + relativeUrl;
					var request = (HttpWebRequest) HttpWebRequest.Create(url);
					request.Method = httpMethod;
					request.Timeout = 100000;
					foreach(var header in headers.Values) {
						HttpHelper.SetRequestHeader(request, header.Name, header.Value);
					}
					if (request.CookieContainer == null && request.Headers[HttpRequestHeader.Cookie] == null) {
						request.CookieContainer = new CookieContainer();
					}
					if (config.Trace) {
						httpTrace.AppendLine(request.Method + " " + request.RequestUri.ToString() + " HTTP/" + request.ProtocolVersion.ToString());
						foreach(var header in request.Headers.AllKeys) {
							httpTrace.AppendLine(header + ": " + request.Headers[header]);
						}
						if (request.ContentLength != -1) {
							httpTrace.AppendLine("Content-Length: " + request.ContentLength);
						}
						httpTrace.AppendLine();
						httpTrace.AppendLine();
					}

					if (request.ContentLength == -1) {
						request.ContentLength = 0;
					}
					HttpWebResponse response;

					try {
						response = (HttpWebResponse) request.GetResponse();
					} catch (WebException we) {
						response = (HttpWebResponse) we.Response;
						if (response == null) {
							throw;
						}
					}

					String responseText = String.Empty;
					byte[] responseArray = new byte[0];
					int statusCode = (int) response.StatusCode;
					String statusLine = statusCode + " " + response.StatusDescription;
					var headersAux = response.Headers;
					using(var responseContext = new OutSystems.Internal.REST.ResponseContext("AddRecipientToList", response)) {
						responseText = responseContext.GetText();
						responseArray = responseContext.GetBinary();
						if (config.Trace) {
							httpTrace.AppendLine("HTTP/" + response.ProtocolVersion + " " + ((int) response.StatusCode) + " " + response.StatusDescription);
							foreach(var headerName in response.Headers.AllKeys) {
								httpTrace.AppendLine(headerName + ": " + response.Headers[headerName]);
							}
							httpTrace.AppendLine();
						}

					}

					var responseHeaders = new List<RestPluginRuntimeUtils.NameValue>();
					foreach(var headerName in headersAux.AllKeys) {
						responseHeaders.Add(new RestPluginRuntimeUtils.NameValue(headerName, headersAux[headerName]));
					}

					STHTTPResponseStructure inHTTPResponse = new STHTTPResponseStructure();
					inHTTPResponse.ssStatusCode = statusCode;
					inHTTPResponse.ssStatusLine = statusLine;
					RLHTTPHeaderList responseHeadersRecordList = RLHTTPHeaderList.ToList<RestPluginRuntimeUtils.NameValue>(responseHeaders.ToArray(), RestPluginRuntimeUtils.NameValue.ToHeaderRecord);
					inHTTPResponse.ssHeaders = responseHeadersRecordList;
					inHTTPResponse.ssResponseText = responseText;
					inHTTPResponse.ssResponseBinary = responseArray;
					STHTTPResponseStructure outHTTPResponse;
					_callbacks.FlowSendgridActionOnAfterResponse(heContext, inHTTPResponse, out outHTTPResponse);
					if (!(new STHTTPResponseStructure(null).Equals(outHTTPResponse))) {
						statusCode = outHTTPResponse.ssStatusCode;
						statusLine = outHTTPResponse.ssStatusLine;
						responseHeadersRecordList = outHTTPResponse.ssHeaders;
						responseHeaders = RLHTTPHeaderList.ToArray<RestPluginRuntimeUtils.NameValue>(responseHeadersRecordList, RestPluginRuntimeUtils.NameValue.FromHeaderRecord).ToList();
						responseText = outHTTPResponse.ssResponseText;
						responseArray = outHTTPResponse.ssResponseBinary;
					}

					if (statusCode < 200 || statusCode >= 400) {
						throw new Exception(statusLine);
					}

					var deserializationMessages = new List<String>();

					if (deserializationMessages.Any()) {
						var detail = new StringBuilder();
						detail.Append("Failed to parse response of the method 'AddRecipientToList' of the 'Sendgrid' REST API:\n");
						for (int maxErrorMessagesToShow = 3, currentPosition = 0; maxErrorMessagesToShow > 0 && currentPosition < deserializationMessages.Count; --maxErrorMessagesToShow, ++currentPosition) {
							detail.Append(deserializationMessages[currentPosition]);
							detail.Append("\n");
						}

						throw new Exception(detail.ToString());
					}
				} catch {
					failedRequest = true;
					throw;
				} finally {
					if (config.TraceAll || (config.TraceErrors && failedRequest)) {
						__message = "";
						__detail = httpTrace.ToString();
						__detailLabel = "HTTP Trace";
					}
				}
			} catch (Exception __ex) {
				__errorLogId = ErrorLog.LogApplicationError(__ex, heContext, "REST (Consume)");
				throw __ex;
			} finally {
				DateTime __instant = DateTime.Now;
				TimeSpan __duration = __instant.Subtract(__startExecution);
				if (heContext.RequestTracer != null) {
					heContext.RequestTracer.RegisterConsumedIntegrationExecuted("Sendgrid_AddRecipientToList", "116467eb-929a-4d4b-9d4f-d8c0a104e3c3",
					 "f82563d5-ffee-43c2-900d-8425d42939cd", "BIFY",
					 "REST (Consume)", (int) __duration.TotalMilliseconds, __instant,
					__endpoint); 
				}
				if (!String.IsNullOrEmpty(__errorLogId) ||
				(heContext.AppInfo != null && (!heContext.AppInfo.SelectiveLoggingEnabled || heContext.AppInfo.Properties.AllowLogging))) {
					String __integrationLogId = IntegrationLog.StaticWrite(
					heContext.AppInfo,
					__instant,
					(int) __duration.TotalMilliseconds,
					null,
					__endpoint,
					 "Sendgrid.AddRecipientToList",
					 "REST (Consume)",
					__errorLogId,
					false);
					if (!String.IsNullOrEmpty(__detail) ||
					!String.IsNullOrEmpty(__message)) {
						IntDetailLog.StaticWrite(__integrationLogId,
						__instant,
						(heContext.AppInfo != null && heContext.AppInfo.Tenant != null ? heContext.AppInfo.Tenant.Id: 0),
						__message,
						__detail,
						__detailLabel);
					}
				}
			}
		}
	}
}
