/* 
 This source code (the "Generated Software") is generated by the OutSystems Platform 
 and is licensed by OutSystems (http://www.outsystems.com) to You solely for testing and evaluation 
 purposes, unless You and OutSystems have executed a specific agreement covering the use terms and 
 conditions of the Generated Software, in which case such agreement shall apply. 
*/

using System;
using System.Web;
using OutSystems.Application.Core;
using OutSystems.Application.Core.Http;
using OutSystems.Application.Session.Abstractions;
using OutSystems.Application.Session.Persistent.Cookies;
using OutSystems.Application.Session.Persistent.Data;
using OutSystems.Application.Session.Persistent.Interface;
using OutSystems.HubEdition.Extensibility.Data.Platform.Session;
using OutSystems.Internal.Db;
using OutSystems.RuntimeCommon.Settings;

namespace OutSystems.Application.Session.Persistent {

    static class ASPSessionId {
        public static string Value {
            get {
                return HttpContext.Current?.Session?.SessionID ?? "No session";
            }
        }
    }

    public class PersistentSessionInfo : BaseSessionInfo {
        private const string SESSION_FIXATION_PROTECTION = "SessionFixationProtection";
        private const string SESSION_FIXATION_PROTECTION_OLD = "SessionFixationProtectionOld";
        private const string SESSION_FIXATION_PROTECTION_EXPIRATION_INSTANT = "SessionFixationProtectionExpirationInstant";
        public const string SESSION_FIXATION_EXCEPTION = "SessionFixationException";

        public override string SessionId => ASPSessionId.Value;

        private const string CookieNameSeparationChar = ".";

        private const string SFPSuffix = "sid";

        public string PersistentLoginCookieName {
            get {
                return GetPersistentLoginCookieName(ModuleInfo, TenantInfo);
            }
        }

        internal string SessionFixationProtectionCookieName {
            get {
                return GetSessionFixationProtectionCookieName(PersistentLoginCookieName);
            }
        }

        private CookieActions CookieActions { get; set; }

        internal bool IsReadOnlySession {
            get => HttpContext.Current?.Session?.IsReadOnly ?? true;
        }

        public static string GetPersistentLoginCookieName(string userProviderName, string espaceName, bool isMultiTenant, string tenantName) {
            string cookieName = string.Empty;

            if (!string.IsNullOrEmpty(userProviderName)) {
                cookieName = userProviderName;
            } else {
                cookieName = espaceName;
                if (isMultiTenant) {
                    cookieName += CookieNameSeparationChar + tenantName;
                }
            }
            return cookieName;
        }

        public static string GetPersistentLoginCookieName(IModuleInfo moduleInfo, ITenantInfo tenantInfo) {
            return GetPersistentLoginCookieName(moduleInfo.UserProvider.Name, moduleInfo.Name, tenantInfo.IsMultiTenant, tenantInfo.Name);
        }

        public static string GetSessionFixationProtectionCookieName(string persistentLoginCookieName) {
            string cookieName = persistentLoginCookieName;
            cookieName += CookieNameSeparationChar + SFPSuffix;
            return cookieName;
        }

        internal string SessionFixationProtection {
            get {
                return (string) SessionHashtable[SESSION_FIXATION_PROTECTION];
            }
            set {
                SessionHashtable[SESSION_FIXATION_PROTECTION] = value;
            }
        }

        internal string SessionFixationProtectionOld {
            get {
                return (string) SessionHashtable[SESSION_FIXATION_PROTECTION_OLD] ?? "";
            }
            set {
                SessionHashtable[SESSION_FIXATION_PROTECTION_OLD] = value;
            }
        }

        internal DateTime SessionFixationProtectionExpirationInstant {
            get {
                return SessionHashtable[SESSION_FIXATION_PROTECTION_EXPIRATION_INSTANT] == null ? DateTime.MinValue : Convert.ToDateTime(SessionHashtable[SESSION_FIXATION_PROTECTION_EXPIRATION_INSTANT]);
            }
            set {
                SessionHashtable[SESSION_FIXATION_PROTECTION_EXPIRATION_INSTANT] = value;
            }

        }

        IPlatformSessionService SessionService { get; }
        ITenantInfo TenantInfo { get; }
        IHttpContext HttpContextAccessor { get; }

        DatabaseAccessWorkarounds DatabaseWorkarounds { get; }

        public PersistentSessionInfo(
            ISettingsProvider settingsProvider,
            IHttpContext httpContext,
            IModuleInfo moduleInfo,
            ITenantInfo tenantInfo,
            IDatabaseAccessProvider runtimeDatabaseAccess,
            IPlatformSessionService sessionService,
            ISessionMetadata sessionMetadata,
            IPersistentSessionHashtable sessionData,
            ILogService logger) : base(settingsProvider, moduleInfo, runtimeDatabaseAccess, sessionMetadata, sessionData, logger) {

            HttpContextAccessor = httpContext;
            CookieActions = new CookieActions(SettingsProvider, httpContext, RuntimeDatabaseAccess, SessionMetadata, logger);
            SessionService = sessionService;
            TenantInfo = tenantInfo;
            PendingSessionProtectionValidation = true;
            DatabaseWorkarounds = new DatabaseAccessWorkarounds();
        }

        public override void Validate() {
            CookieActions.ValidateSessionFixationCookieAgainstSession(ModuleInfo, this);
        }

        protected override void FullClearSession() {
            try {
                Func<int> ExecuteDeleteModuleItems = () => {
                    SessionService.DeleteModuleItems(SessionIdParser.Parse(SessionId, SettingsProvider));
                    return 0;
                };
                DatabaseWorkarounds.ExecuteWithoutRequestTimeout(ExecuteDeleteModuleItems);
            } finally {
                HttpContext.Current?.Session?.Clear();
                base.FullClearSession();
                if (HttpContext.Current != null) {
                    HttpContext.Current.Session[SESSION_OBJECT_KEY] = SessionHashtable;
                }
            }
        }

        public override void RecreateHashtable() {
            SessionHashtable = new PersistentSessionHashtable(ModuleInfo, SessionId, SessionService, SettingsProvider, Logger);
        }

        private static string SESSION_OBJECT_KEY = "OutSystems.Session.PersistentData";
        private static string SESSION_OBJECT_KEY_LEGACY = "OutSystems.UnifiedSession";
        internal bool PendingSessionProtectionValidation { get; set; }

        static bool HasLegacySession(HttpContext context) {

            if (context == null) {
                return false;
            }

            try {
                for (int i = 0; i < context.Session.Keys.Count; ++i) {
                    if (context.Session.Keys[i] == SESSION_OBJECT_KEY_LEGACY) {
                        return true;
                    }
                }
                return false;
            } catch {
                return false;
            }
        }

        public static PersistentSessionInfo RecreateFromSessionOrCreateNew(
                                                                ISettingsProvider settingsProvider,
                                                                IHttpContext httpContext,
                                                                IModuleInfo moduleInfo,
                                                                ITenantInfo tenantInfo,
                                                                IDatabaseAccessProvider runtimeDatabaseAccess,
                                                                IPlatformSessionService sessionService,
                                                                ISessionMetadata sessionMetadata,
                                                                ILogService logger,
                                                                Action onSessionStart,
                                                                Action onLogout,
                                                                Action onLogin,
                                                                Action<PersistentSessionInfo> storeSession) {

            PersistentSessionHashtable sessionData = null;
            try {
                sessionData = (PersistentSessionHashtable) HttpContext.Current.Session[SESSION_OBJECT_KEY];
            } catch (Exception ex) {
                try {
                    logger.LogError("Error obtaining session data.", "Session", ex);
                } catch { }
            }

            if (sessionData == null) {
                sessionData = new PersistentSessionHashtable(moduleInfo, ASPSessionId.Value, sessionService, settingsProvider, logger);
                HttpContext.Current.Session[SESSION_OBJECT_KEY] = sessionData;
            } else {
                // We have to restore the services that are not serialized with session data
                sessionData.Initialize(moduleInfo, sessionService, settingsProvider, logger);
            }

            PersistentSessionInfo session = new PersistentSessionInfo(settingsProvider, httpContext, moduleInfo, tenantInfo, runtimeDatabaseAccess, sessionService, sessionMetadata, sessionData, logger);

            // if we still have an old sesison, it means that we are still running the upgrade process
            // we won't maintain sessions synced with old versions of applications, but we need to delete them
            if (HasLegacySession(HttpContext.Current)) {
                // set session fixation fields to null so we can skip validation until a new login occurs
                session.SessionFixationProtection = null;
                session.SessionFixationProtectionOld = null;
                // remove old sesison so old application don't throw sesison fixation errors
                HttpContext.Current.Session.Remove(SESSION_OBJECT_KEY_LEGACY);
            }

            session.OnSessionStart += onSessionStart;
            session.OnLogout += onLogout;
            session.OnLogin += onLogin;

            storeSession(session);

            session.AutoLoginFromPersistentCookies();
            session.TriggerSessionStart();

            return session;
        }

        private void Log(string message) {
            if (SettingsProvider.Get(SessionSettings.DebugAutoLogin)) {
                Logger.LogGeneral(message, "INFO", "LOGIN");
            }
        }

        private void AutoLoginFromPersistentCookies() {
            if (UserId != 0) {
                return;
            }

            var persistentLoginValue = CookieActions.GetPersistentLoginValue(this);
            if (!string.IsNullOrEmpty(persistentLoginValue)) {

                using (Transaction trans = RuntimeDatabaseAccess.GetRequestTransaction()) {

                    int userId = 0;
                    int tenantId = 0;
                    int existingPersistentLoginId = 0;
                    DateTime expirationDateTime;

                    CookieActions.GetPersistentLoginInfo(trans, ModuleInfo, persistentLoginValue, out userId, out tenantId, out existingPersistentLoginId, out expirationDateTime, "AutoLogin");

                    if (userId == 0 || existingPersistentLoginId == 0) {
                        Log($"Failed to auto-login. (id: {userId}, tenant id: {tenantId}, persistent login id: {existingPersistentLoginId})");
                        CookieActions.DeletePersistentLoginCookie(this);
                    } else {
                        if (!this.SessionMetadata.IsUserActive(trans, userId, tenantId, out var username)) {
                            return;
                        }
                        TenantId = tenantId;
                        Log($"Performing auto-login for user {username} (id: {userId}, tenant id: {tenantId}, persistent login id: {existingPersistentLoginId}))");
                        Login(userId, tenantId, username, true);
                    }
                }
            }
        }


        protected override void InnerLogin(int userId, int tenantId, string username, bool persistent) {
            if (!IsReadOnlySession) {
                CookieActions.AddSessionFixationProtectionCookie(this);

                // if it is a persistent login, refresh the entry and refresh the cookie
                if (persistent) {
                    CookieActions.AddOrRefreshPersistentLoginCookie(ModuleInfo, this, userId, "InnerLogin");
                } else {
                    // always delete any persistent login entries, if a persistent cookie was sent and we did a regular login
                    CookieActions.DeletePersistentLoginEntryAndCookie(ModuleInfo, this, "InnerLogin");
                }
            }
        }

        protected override void InnerLogout(int userId) {
            base.InnerLogout(userId);
            if (userId != 0) {
                CookieActions.DeleteAllPersistentLoginEntries(userId, "Logout");
            }
        }
    }
}
