/* 
 This source code (the "Generated Software") is generated by the OutSystems Platform 
 and is licensed by OutSystems (http://www.outsystems.com) to You solely for testing and evaluation 
 purposes, unless You and OutSystems have executed a specific agreement covering the use terms and 
 conditions of the Generated Software, in which case such agreement shall apply. 
*/


using OutSystems.RuntimeCommon.Settings;

namespace OutSystems.Application.Core.Http {

    public abstract class AbstractHttpRequest : IHttpRequest {
        public IHttpContext HttpContext { get; }
        protected ISettingsProvider SettingsProvider { get; }

        public AbstractHttpRequest(IHttpContext httpContext, ISettingsProvider settingsProvider) {
            HttpContext = httpContext;
            SettingsProvider = settingsProvider;
        }

        public abstract IRequestCookies Cookies { get; }
        public abstract IRequestHeaders Headers { get; }
    }
}
