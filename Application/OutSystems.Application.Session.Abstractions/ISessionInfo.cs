/* 
 This source code (the "Generated Software") is generated by the OutSystems Platform 
 and is licensed by OutSystems (http://www.outsystems.com) to You solely for testing and evaluation 
 purposes, unless You and OutSystems have executed a specific agreement covering the use terms and 
 conditions of the Generated Software, in which case such agreement shall apply. 
*/

using System;
using System.Collections.Generic;
using System.Text;

namespace OutSystems.Application.Session.Abstractions {

    public interface ISessionInfo {
        [Obsolete("TerminalType is always 'WEB'")]
        string TerminalType { get; set; }
        object this[string name] { get; set; }
        int UserId { get; set; }
        string UserName { get; set; }
        int TenantId { get; set; }
        string SessionGuid { get; }
        string SessionId { get; }
        string CurrentLocale { get; set; }

        event Action OnSessionStart;
        event Action OnLogout;
        event Action OnLogin;

        void Validate(); //Throws or clears session for instance on session fixation attacks 

        IEnumerable<string> GetAllRoles();
        bool CheckRole(int roleId);
        void AddRole(int roleId, string roleSSkey);
        void RemoveRole(int roleId);
        void ClearRoles();

        bool TestAndSetNeedsSessionStart(string eSpaceName);
        bool NeedsSessionStart(string eSpaceName);
        void Clear();
        void Login(int userId, int tenantId, string username, bool persistent);
        void Logout();
    }
}
