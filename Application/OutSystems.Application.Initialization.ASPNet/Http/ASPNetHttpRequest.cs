/* 
 This source code (the "Generated Software") is generated by the OutSystems Platform 
 and is licensed by OutSystems (http://www.outsystems.com) to You solely for testing and evaluation 
 purposes, unless You and OutSystems have executed a specific agreement covering the use terms and 
 conditions of the Generated Software, in which case such agreement shall apply. 
*/


using OutSystems.Application.Core.Http;
using OutSystems.RuntimeCommon.Settings;

namespace OutSystems.Application.Initialization.ASPNet.Http {

    public class ASPNetHttpRequest : AbstractHttpRequest {

        protected System.Web.HttpRequest WrappedRequest { get; }

        public ASPNetHttpRequest(IHttpContext httpContext, ISettingsProvider settingsProvider, System.Web.HttpRequest request)
            : base(httpContext, settingsProvider) {

            WrappedRequest = request;
        }

        public override IRequestCookies Cookies => new ASPNetRequestCookies(WrappedRequest);

        public override IRequestHeaders Headers => new ASPNetRequestHeaders(WrappedRequest.Headers);
    }
}
