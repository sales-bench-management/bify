/* 
 This source code (the "Generated Software") is generated by the OutSystems Platform 
 and is licensed by OutSystems (http://www.outsystems.com) to You solely for testing and evaluation 
 purposes, unless You and OutSystems have executed a specific agreement covering the use terms and 
 conditions of the Generated Software, in which case such agreement shall apply. 
*/

using System.Linq;
using OutSystems.Application.Core.Http;
using OutSystems.RuntimeCommon.Settings;

namespace OutSystems.Application.Initialization.ASPNet.Http {

    public class ASPNetResponseCookies : AbstractResponseCookies {

        protected System.Web.HttpResponse WrappedResponse { get; }

        public ASPNetResponseCookies(IHttpResponse httpResponse, ISettingsProvider settingsProvider, System.Web.HttpResponse wrappedResponse)
            : base(httpResponse, settingsProvider) {
            WrappedResponse = wrappedResponse;
        }
        
        protected System.Web.HttpCookie CreateCookie(string name, string value, CookieOptions options) {
            var cookie = new System.Web.HttpCookie(name, value);
            // only touch the fields that are set in the Options, to avoid overriding system defaults

            if (options.Domain != null) {
                cookie.Domain = options.Domain;
            }

            if (options.Path != null) {
                cookie.Path = options.Path;
            }

            if (options.HttpOnly != null) {
                cookie.HttpOnly = options.HttpOnly.Value;
            }

            if (options.Secure != null) {
                cookie.Secure = options.Secure.Value;
            }

            if (options.Expires != null) {
                cookie.Expires = options.Expires.Value;
            }

            if (options.MaxAge != null) {
                cookie.Value += "; Max-Age=" + (long)(options.MaxAge.Value.TotalSeconds);
            }

            if (options.SameSite != null) {
                switch (options.SameSite.Value) {
                    case CookieOptions.SameSiteMode.Unspecified:

                        // Better not do anything here
                        // If cookie.SameSite = -1 , it's already ok
                        // If cookie.SameSite = None , it's probably a .Net Framework version that may not support Unspecified, leaving it as None is safer
                        // If cookie.SameSite = Lax or Strict , there is a override in either machine.config or web.config for the HttpCookies section to force a value.

                        //// Could be a more aggressive on this, but would at least need to protect the cookie.SameSite = None case
                        //if (cookie.SameSite != System.Web.SameSiteMode.None) {
                        //    cookie.SameSite = (System.Web.SameSiteMode)(-1) /* Unspecified */;
                        //}

                        break;
                    case CookieOptions.SameSiteMode.None:
                        cookie.SameSite = System.Web.SameSiteMode.None;
                        break;
                    case CookieOptions.SameSiteMode.Lax:
                        cookie.SameSite = System.Web.SameSiteMode.Lax;
                        break;
                    case CookieOptions.SameSiteMode.Strict:
                        cookie.SameSite = System.Web.SameSiteMode.Strict;
                        break;
                    default:
                        break;
                }
            }

            return cookie;
        }

        /// <summary>
        /// Adds a cookie. 
        /// Even if there are already cookies with the same name, this method will add a new one.
        /// It's specially helpful to add multiple cookies with the same name but different options like Path and Domain.
        /// </summary>
        protected override void InnerAdd(string name, string value, CookieOptions options) {
            WrappedResponse.Cookies.Add(CreateCookie(name, value, options));
        }

        /// <summary>
        /// Sets a cookie, removing any existing cookie with the same name.
        /// It does not make any distinction about cookies with different options.
        /// </summary>
        protected override void InnerSet(string name, string value, CookieOptions options) {
            // Set only needs to allow one cookie in the collection per name.
            if (WrappedResponse.Cookies.AllKeys.Any(key => key == name)) {
                // Force the removal of the previous cookie (instead of using .Set) to avoid .Net Framework issue that duplicated cookies on response
                WrappedResponse.Cookies.Remove(name);
            }
            // Still using the Set instead of the Add (even though the previous cookie was removed) to avoid the Add internal semantics
            WrappedResponse.Cookies.Set(CreateCookie(name, value, options));
        }
    }
}
