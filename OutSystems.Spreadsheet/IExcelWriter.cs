/* 
 This source code (the "Generated Software") is generated by the OutSystems Platform 
 and is licensed by OutSystems (http://www.outsystems.com) to You solely for testing and evaluation 
 purposes, unless You and OutSystems have executed a specific agreement covering the use terms and 
 conditions of the Generated Software, in which case such agreement shall apply. 
*/

namespace OutSystems.Spreadsheet {

    public interface IExcelWriter {

        void GetCellPosition(string content, int row, out int column);
        void GetCellPosition(string content, out int row, out int column);
        void CreateRow(int index);
        void CreateRow(int index, int colOffset);
        void SkipCell();
        void CreateCell(object data);
        void CreateCellTime(object data);
        void CreateCellDate(object data);
        void CreateCellDateTime(object data);
        void DoubleDefaultColumnWidth();
        void CopyColumnWidth(int index, int width);
        void CopyStyle(int targetRow, int targetCol, int sourceRow, int sourceCol);
        void DeleteColumn(int index);
        byte[] Save();
        void Write(string filename);
    }
}
