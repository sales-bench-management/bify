/* 
 This source code (the "Generated Software") is generated by the OutSystems Platform 
 and is licensed by OutSystems (http://www.outsystems.com) to You solely for testing and evaluation 
 purposes, unless You and OutSystems have executed a specific agreement covering the use terms and 
 conditions of the Generated Software, in which case such agreement shall apply. 
*/

using OutSystems.HubEdition.Extensibility.Data;
using OutSystems.HubEdition.Extensibility.Data.Platform;
using OutSystems.Internal.Db;

namespace OutSystems.HubEdition.RuntimePlatform {

    public static partial class RuntimePlatformServices {

        internal class DefaultDatabaseAccessService : IDatabaseAccessService {
            public DefaultDatabaseAccessService() { }

            public virtual IDatabaseAccessProvider<IPlatformDatabaseServices> ForCurrentDatabase() => DatabaseAccess.ForCurrentDatabase;
            public virtual IDatabaseAccessProvider<IPlatformDatabaseServices> ForRuntimeDatabase() => DatabaseAccess.ForRuntimeDatabase;
            public virtual IDatabaseAccessProvider<IPlatformDatabaseServices> ForSystemDatabase() => DatabaseAccess.ForSystemDatabase;
            public virtual IDatabaseAccessProvider<IPlatformDatabaseServices> ForDatabase(string databaseName) => DatabaseAccess.ForDatabase(databaseName);
            public virtual IDatabaseAccessProvider<IDatabaseServices> ForDBConnection(string connectionName) => DatabaseAccess.ForDBConnection(connectionName);

            public virtual IDatabaseAccessProvider<IPlatformDatabaseServices> ForEspaceDatabase(string eSpaceKey) => DatabaseAccess.ForEspaceDatabase(eSpaceKey);
            public virtual IDatabaseAccessProvider<IPlatformDatabaseServices> ForLogging() => DatabaseAccess.ForLogging;
            public virtual IDatabaseAccessProvider<IPlatformDatabaseServices> ForSession() => DatabaseAccess.ForSession;

            public virtual void CommitAllTransactions() => DatabaseAccess.CommitAllTransactions();
            public virtual void RollbackAllTransactions() => DatabaseAccess.RollbackAllTransactions();
            public virtual void FreeupResources(bool commit) => DatabaseAccess.FreeupResources(commit);
        }
    }
}
