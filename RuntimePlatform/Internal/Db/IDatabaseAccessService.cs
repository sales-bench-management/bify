/* 
 This source code (the "Generated Software") is generated by the OutSystems Platform 
 and is licensed by OutSystems (http://www.outsystems.com) to You solely for testing and evaluation 
 purposes, unless You and OutSystems have executed a specific agreement covering the use terms and 
 conditions of the Generated Software, in which case such agreement shall apply. 
*/

using OutSystems.HubEdition.Extensibility.Data;
using OutSystems.HubEdition.Extensibility.Data.Platform;

namespace OutSystems.Internal.Db {

    internal interface IDatabaseAccessService {

        IDatabaseAccessProvider<IPlatformDatabaseServices> ForCurrentDatabase();
        IDatabaseAccessProvider<IPlatformDatabaseServices> ForRuntimeDatabase();
        IDatabaseAccessProvider<IPlatformDatabaseServices> ForSystemDatabase();
        IDatabaseAccessProvider<IPlatformDatabaseServices> ForDatabase(string databaseName);
        IDatabaseAccessProvider<IDatabaseServices> ForDBConnection(string connectionName);

        IDatabaseAccessProvider<IPlatformDatabaseServices> ForEspaceDatabase(string eSpaceKey);
        IDatabaseAccessProvider<IPlatformDatabaseServices> ForLogging();
        IDatabaseAccessProvider<IPlatformDatabaseServices> ForSession();

        void CommitAllTransactions();
        void RollbackAllTransactions();
        void FreeupResources(bool commit);
    }

}
