/* 
 This source code (the "Generated Software") is generated by the OutSystems Platform 
 and is licensed by OutSystems (http://www.outsystems.com) to You solely for testing and evaluation 
 purposes, unless You and OutSystems have executed a specific agreement covering the use terms and 
 conditions of the Generated Software, in which case such agreement shall apply. 
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OutSystems.ObjectKeys;

namespace OutSystems.HubEdition.RuntimePlatform.NewRuntime {
    public class StaticEntity {
        public GlobalObjectKey EntityGlobalKey { get; }
        public Dictionary<ObjectKey, object> StaticRecords { get; }
        public bool IsInMemory { get; }

        private StaticEntity(GlobalObjectKey globalKey, Dictionary<ObjectKey, object> records, bool inMemory) {
            this.EntityGlobalKey = globalKey;
            this.StaticRecords = records;
            this.IsInMemory = inMemory;
        }

        public StaticEntity(GlobalObjectKey globalKey, Dictionary<ObjectKey, object> records) : this(globalKey, records, inMemory: true) { }

        public StaticEntity(GlobalObjectKey globalKey) : this(globalKey, null, inMemory: false) { }

        public ObjectKey EspaceKey => EntityGlobalKey.OwnerKey;

        public ObjectKey EntityKey => EntityGlobalKey.Key;
        
        public static bool operator ==(StaticEntity a, StaticEntity b) {
            if (ReferenceEquals(a, null)) {
                return ReferenceEquals(b, null);
            }
            return a.Equals(b);
        }

        public static bool operator !=(StaticEntity a, StaticEntity b) {
            if (ReferenceEquals(a, null)) {
                return !ReferenceEquals(b, null);
            }
            return !a.Equals(b);
        }

        public override bool Equals(object obj) {
            var entity = obj as StaticEntity;
            return entity != null &&
                   EqualityComparer<GlobalObjectKey>.Default.Equals(EntityGlobalKey, entity.EntityGlobalKey);
        }

        public override int GetHashCode() {
            return 1402306252 + EqualityComparer<GlobalObjectKey>.Default.GetHashCode(EntityGlobalKey);
        }
    }
}
