/* 
 This source code (the "Generated Software") is generated by the OutSystems Platform 
 and is licensed by OutSystems (http://www.outsystems.com) to You solely for testing and evaluation 
 purposes, unless You and OutSystems have executed a specific agreement covering the use terms and 
 conditions of the Generated Software, in which case such agreement shall apply. 
*/

using System.Collections.Generic;
using OutSystems.ModuleServices;
using OutSystems.RuntimeCommon.Settings;

namespace OutSystems.HubEdition.RuntimePlatform.Processes {
    public class ActivitiesClientFactory : AbstractCommunicationClientFactory<IActivitiesClient, ActivitiesRestClient> {
        private ISettingsProvider SettingsProvider { get; }
        private int TenantId { get; }
        private int UserId { get; }
        private string ConsumerKey { get; }
        private string ProducerKey { get; }

        public ActivitiesClientFactory(ISettingsProvider settingsProvider, string moduleUrl, int tenantId, int userId, string consumerKey, string producerKey) : base(moduleUrl) {
            SettingsProvider = settingsProvider;
            TenantId = tenantId;
            UserId = userId;
            ConsumerKey = consumerKey;
            ProducerKey = producerKey;
        }

        public ActivitiesClientFactory(string moduleUrl, int tenantId, int userId, string consumerKey, string producerKey) :
            this(RuntimeSettingsProvider.Instance, moduleUrl, tenantId, userId, consumerKey, producerKey) { }


        protected override ActivitiesRestClient CreateRestClient() {
            return new ActivitiesRestClient(SettingsProvider, ModuleUrl, TenantId, UserId, ConsumerKey, ProducerKey);
        }

        protected override IActivitiesClient CreateSoapClient() {
            return new ActivityHandler(SettingsProvider, ModuleUrl, TenantId, UserId, ConsumerKey, ProducerKey);
        }

        protected override bool IsRestEndpointEnabled() {
            return SettingsProvider.Get(RuntimePlatformSettings.Misc.EnableCoreRestEndpoints);
        }
    }
}
