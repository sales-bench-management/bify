/* 
 This source code (the "Generated Software") is generated by the OutSystems Platform 
 and is licensed by OutSystems (http://www.outsystems.com) to You solely for testing and evaluation 
 purposes, unless You and OutSystems have executed a specific agreement covering the use terms and 
 conditions of the Generated Software, in which case such agreement shall apply. 
*/

using System;

namespace OutSystems.RuntimePublic.Users {
    public class LocalGroup : ILocalGroup {
        public int Id { get; set; }

        public string Name { get; set; }

        public string Description { get; set; }

        public int Created_By { get; set; }

        public DateTime Creation_Date { get; set; }

        public bool Has_Custom_Management { get; set; }
    }
}
